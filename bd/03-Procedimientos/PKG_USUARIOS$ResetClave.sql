USE CSGPI 
GO

IF EXISTS (SELECT 1 FROM DBO.SYSOBJECTS WHERE ID = OBJECT_ID(N'[DBO].[PKG_USUARIOS$ResetClave]') AND OBJECTPROPERTY(ID, N'ISPROCEDURE') = 1)
	DROP PROCEDURE PKG_USUARIOS$ResetClave
GO
CREATE  PROCEDURE [dbo].[PKG_USUARIOS$ResetClave]
	@PId_Usuario		 FLOAT(53) 
AS
BEGIN

	UPDATE	USUARIOS
	SET		COD_ESTADO='PWD'
	WHERE	Id_Usuario = @PId_Usuario

END
GO
GRANT EXECUTE ON PKG_USUARIOS$ResetClave TO DB_EXECUTESP
GO


