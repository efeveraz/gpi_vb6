USE [CSGPI]
GO
/****** Object:  StoredProcedure [dbo].[Sis_HistClave_Consultar]    Script Date: 04/01/2021 10:15:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[Sis_HistClave_Consultar]
(
	@pIdUsuario	NUMERIC(10),
	@pResultado			VARCHAR(2000)
)
AS
BEGIN TRY

	SET @pResultado	 = 'OK'

	SELECT	TOP 3 ID_NEGOCIO,
			ID_USUARIO,
			CLAVE,
			FECHA,
			ESTADO
	FROM	SIS_HIST_CLAVE
	WHERE   ID_USUARIO = @pIdUsuario
	ORDER BY FECHA DESC
	
END TRY
BEGIN CATCH
	SET @pResultado =  ERROR_MESSAGE()	+ ' procedimiento: ' + ERROR_PROCEDURE() + ' línea: ' + CONVERT(VARCHAR,ERROR_LINE())
END CATCH
