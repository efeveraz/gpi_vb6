USE CSGPI 
GO

IF EXISTS (SELECT 1 FROM DBO.SYSOBJECTS WHERE ID = OBJECT_ID(N'[DBO].[PKG_USUARIOS$ActualizaClave]') AND OBJECTPROPERTY(ID, N'ISPROCEDURE') = 1)
	DROP PROCEDURE PKG_USUARIOS$ActualizaClave
GO
CREATE  PROCEDURE [dbo].[PKG_USUARIOS$ActualizaClave]
	@pIdUsuario		FLOAT(10),
	@pClave			VARCHAR(200),
	@pEstado		VARCHAR(3),
	@pResultado		Varchar(2000) output
AS
BEGIN
	BEGIN TRY

		declare @vDias				integer,
				@vFechaActualizada	datetime

		SET @pResultado	= 'OK'

		SELECT	@vDias = valor 
		FROM	sis_parametros with (nolock)
		WHERE	CODIGO = 'N_DIAS_VENCI'

		SET @vFechaActualizada  = DATEADD(day, @vDias , getdate())

		DECLARE @vIdNegocio NUMERIC(10),
				@vIdUsuario	NUMERIC(10),
				@vClave		VARCHAR(500),
				@vEstado	VARCHAR(10),
				@vResultado	VARCHAR(2000)

		SELECT	@vIdNegocio = ID_EMPRESA,
				@vIdUsuario = Id_Usuario,
				@vClave = PASSWORD,
				@vEstado = COD_ESTADO
		FROM	USUARIOS with (nolock)
		WHERE	Id_Usuario = @pIdUsuario

		EXEC Sis_HistClave_Insertar
			@pIdNegocio = @vIdNegocio,
			@pIdUsuario	= @vIdUsuario,
			@pClave		= @vClave,
			@pEstado	= @vEstado,
			@pResultado	= null

		UPDATE	USUARIOS
		SET		PASSWORD = @pClave,
				FECHA_VIGENCIA = @vFechaActualizada,
				COD_ESTADO = @pEstado
		WHERE	Id_Usuario = @pIdUsuario

	END TRY
	BEGIN CATCH
		SET @pResultado ='ERROR'
	END CATCH

END
GO
GRANT EXECUTE ON PKG_USUARIOS$ActualizaClave TO DB_EXECUTESP
GO


