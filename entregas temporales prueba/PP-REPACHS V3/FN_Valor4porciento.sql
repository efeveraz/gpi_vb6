USE [CSGPI] 
GO

IF EXISTS(SELECT 1 FROM SYSOBJECTS WHERE name = 'FN_Valor4porciento' AND xtype = 'FN')
	DROP FUNCTION dbo.FN_Valor4porciento
GO
CREATE FUNCTION FN_Valor4porciento
(
	@pFechaConsulta datetime,--VARCHAR(10),
	@pNemotecnico	VARCHAR(200),
	@pValorTotal	NUMERIC(24,6)
)
RETURNS INT
AS
BEGIN
	DECLARE	@vFechaConsulta		DATETIME,
			@vFechaInicioCupon	DATETIME,
			@vFechaSigCupon		DATETIME,
			@vCupo				NUMERIC(10),
			@vMaxcupo			NUMERIC(10),
			@vDiasDevengo		NUMERIC(10),
			@vPeriodoCupon		NUMERIC(10)

	--SELECT @vFechaConsulta = CONVERT(DATETIME,@pFechaConsulta,103)
	SET @vFechaConsulta = @pFechaConsulta
	-- obtengo la fecha maxima segun la fecha de consulta del ultimo cupon ejecutado
	SELECT	@vFechaInicioCupon = max(fecha_cupon)
	FROM	VIEW_CUPONES_RF 
	WHERE	NEMOTECNICO = @pNemotecnico
	AND		FECHA_CUPON <= @vFechaConsulta 
	
	SELECT	@vcupo = nrocupon
	FROM	VIEW_CUPONES_RF
	WHERE	NEMOTECNICO = @pNemotecnico
	AND		fecha_cupon = @vFechaInicioCupon

	-- obtengo el maximo de cupones del nemotecnico
	SELECT	@vMaxcupo = MAX(NROCUPON) 
	FROM	VIEW_CUPONES_RF
	WHERE	NEMOTECNICO = @pNemotecnico

	-- valido que el cupon que estoy viendo, no sea el ultimo
	IF @vcupo<@vMaxcupo
	BEGIN 
		SELECT	@vFechaSigCupon = FECHA_CUPON
		FROM	VIEW_CUPONES_RF
		WHERE	NROCUPON = (@vcupo + 1)
		AND		NEMOTECNICO = @pNemotecnico
	END
	ELSE
	BEGIN
		SET @vFechaSigCupon = @vFechaInicioCupon
	END

	SET @vDiasDevengo = DATEDIFF(DAY,@vFechaInicioCupon, @vFechaConsulta)
	SET @vPeriodoCupon = DATEDIFF(DAY,@vFechaInicioCupon,@vFechaSigCupon)

	RETURN	ROUND(@pValorTotal * (
									(	CASE
											WHEN @vDiasDevengo = 0 or  @vDiasDevengo is null THEN
												1
											ELSE
												@vDiasDevengo
										END)
									/ 
									(	CASE 
											WHEN @vPeriodoCupon is null or @vPeriodoCupon = 0 then
												1
											ELSE
												@vPeriodoCupon
										END)
								),2)
END




