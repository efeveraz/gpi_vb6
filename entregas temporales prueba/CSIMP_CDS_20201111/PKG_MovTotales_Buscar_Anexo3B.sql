USE [CSGPI] 
GO

IF EXISTS (SELECT 1 FROM SYSOBJECTS WHERE name = 'PKG_MovTotales_Buscar_Anexo3B' AND xtype = 'P')
	DROP PROCEDURE [dbo].[PKG_MovTotales_Buscar_Anexo3B]
GO

CREATE PROCEDURE [dbo].[PKG_MovTotales_Buscar_Anexo3B](
	@PID_CUENTA NUMERIC,
	@PFECHA_INI DATETIME,
	@PFECHA_TER DATETIME
)
AS
SET NOCOUNT ON
BEGIN

	DECLARE @ID_ORIGEN			NUMERIC, 
			@ID_TIPO_CONVERSION NUMERIC

	SELECT	@ID_ORIGEN = ID_ORIGEN 
	FROM	ORIGENES 
	WHERE	COD_ORIGEN = 'SEIL'

	SELECT	@ID_TIPO_CONVERSION = ID_TIPO_CONVERSION 
	FROM	TIPOS_CONVERSION 
	WHERE	TABLA = 'MONEDAS'

    DECLARE @TBLANEXO3A TABLE(
		TIPO_FONDO           VARCHAR(1),
		FOLIO_OPERACION      NUMERIC(10),
		FECHA_OPERACION      DATETIME,
		TIPO_OPERACION       VARCHAR(2),
		RUT_EMISOR           VARCHAR(12),
		TIPO_INSTRUMENTO     VARCHAR(12),
		NEMOTECNICO          VARCHAR(50),
		SERIE                VARCHAR(50),
		FECHA_EMISION        DATETIME,
		UNIDADES_NOMINALES   NUMERIC(18,4),
		UNIDAD_MONETARIA     VARCHAR(50),
		PROHIBICION          VARCHAR(1),
		CON_DESCRIPTOR       VARCHAR(1),
		RELACIONADO          VARCHAR(2),
		CLASIFICACION_RIESGO VARCHAR(10),
		VALOR_PAR            NUMERIC(18,4),
		TIR_OPERACION        NUMERIC(18,4),
		VALOR_PRESENTE       NUMERIC(18,4),
		VALOR_OPERACION      NUMERIC(18,4),
		FECHA_VENCIMIENTO    DATETIME,
		FECHA_PAGO           DATETIME,
		TIPO_NOMINALES       VARCHAR(1),
		TASA_EMISION         NUMERIC(18,4),
		TASA_BASE            NUMERIC(18,4)
	)

	INSERT INTO @TBLANEXO3A
    SELECT	CASE
				WHEN ((SELECT ISNULL(TIPO_FONDO,'') FROM CUENTAS_ACHS WITH (NOLOCK) WHERE ID_CUENTA = OP.ID_CUENTA)  <> '')THEN 
					(
						SELECT	TIPO_FONDO 
						FROM	CUENTAS_ACHS WITH (NOLOCK) 
						WHERE	ID_CUENTA = OP.ID_CUENTA
					)
				ELSE 
					'O'
			END AS TIPO_FONDO,
            OP.ID_OPERACION AS FOLIO_OPERACION,
			OP.FECHA_OPERACION,
			CASE  OP.FLG_TIPO_MOVIMIENTO
				WHEN 'I' THEN 'OC'
				WHEN 'E' THEN 'OV'
			END AS TIPO_OPERACION,
            EE.RUT AS RUT_EMISOR,
			CASE 
				WHEN VN.COD_SUBFAMILIA = 'DP$' OR VN.COD_SUBFAMILIA ='DPUF' OR VN.COD_SUBFAMILIA = 'DPUS$' OR VN.COD_SUBFAMILIA = 'DPE' then
					CASE
						WHEN (DATEDIFF(D, @PFECHA_INI, VN.FECHA_VENCIMIENTO) <= 365) then
							'DPC'
						ELSE
							'DPL'
					END 
				ELSE
					VN.COD_SUBFAMILIA
			END AS TIPO_INSTRUMENTO,
			VN.NEMOTECNICO,
			(
				SELECT	ISNULL(COD_ACHS,'')
				FROM	REL_NEMOTECNICO_ATRIBUTOS  WITH (NOLOCK)
				WHERE	ID_NEMOTECNICO = VN.ID_NEMOTECNICO
			) AS SERIE,
			VN.FECHA_EMISION AS FECHA_EMISION,
			OPD.CANTIDAD AS UNIDADES_NOMINALES,
			(
				SELECT	VALOR
				FROM	REL_CONVERSIONES WITH (NOLOCK)
				WHERE	ID_ENTIDAD =VN.ID_MONEDA
				AND		ID_ORIGEN = @ID_ORIGEN
				AND		ID_TIPO_CONVERSION = @ID_TIPO_CONVERSION
			) AS UNIDAD_MONETARIA,
			'N' AS PROHIBICION,
            CASE
				WHEN (VN.COD_INSTRUMENTO = dbo.Pkg_Global$gcINST_DEPOSITOS_NAC()) THEN 
					'U'
				ELSE 
					'S'
            END AS CON_DESCRIPTOR,
            'NR' AS RELACIONADO,
			ISNULL(
					(	SELECT	COD_VALOR_CLASIFICACION
						FROM	REL_NEMOTECNICO_VALOR_CLASIFIC WITH (NOLOCK)
						WHERE	ID_NEMOTECNICO = VN.ID_NEMOTECNICO
					), 'S/C'
			) AS CLASIFICACION_RIESGO,
            CASE
				WHEN (OP.FLG_TIPO_MOVIMIENTO = 'I') THEN
					(
						SELECT	SA.VALOR_PAR
						FROM	SALDOS_ACTIVOS SA WITH (NOLOCK),
								MOV_ACTIVOS MAC   WITH (NOLOCK)
						WHERE	SA.ID_SALDO_ACTIVO = MAC.ID_SALDO_ACTIVO
						AND		MAC.ID_OPERACION_DETALLE = OPD.ID_OPERACION_DETALLE
						AND		SA.ID_CUENTA = OP.ID_CUENTA
						AND		SA.ID_NEMOTECNICO = OPD.ID_NEMOTECNICO
					)
				WHEN (OP.FLG_TIPO_MOVIMIENTO = 'E') THEN
					(
						SELECT	SA.VALOR_PAR
						FROM	SALDOS_ACTIVOS SA WITH (NOLOCK),
								MOV_ACTIVOS MAC   WITH (NOLOCK)
						WHERE	SA.ID_SALDO_ACTIVO = MAC.ID_SALDO_ACTIVO
						AND		MAC.ID_MOV_ACTIVO = MA.ID_MOV_ACTIVO_COMPRA
						AND		SA.ID_CUENTA = OP.ID_CUENTA
						AND		SA.ID_NEMOTECNICO = OPD.ID_NEMOTECNICO)
			END AS VALOR_PAR,
			OPD.PRECIO AS TIR_OPERACION,
			OPD.MONTO_PAGO AS VALOR_PRESENTE,
			OPD.MONTO_PAGO AS VALOR_OPERACION,
            VN.FECHA_VENCIMIENTO,
            CASE
				WHEN (OP.FLG_TIPO_MOVIMIENTO = 'I') THEN OP.FECHA_LIQUIDACION
				WHEN (OP.FLG_TIPO_MOVIMIENTO = 'E') THEN MA.FECHA_CONFIRMACION
            END AS FECHA_PAGO,
            CASE
				WHEN (VN.COD_INSTRUMENTO = dbo.Pkg_Global$gcINST_DEPOSITOS_NAC()) THEN 'F'
				ELSE 'I'
            END AS TIPO_NOMINALES,
            CASE
				WHEN (VN.COD_INSTRUMENTO = dbo.Pkg_Global$gcINST_DEPOSITOS_NAC()) THEN OPD.PRECIO
				ELSE VN.TASA_EMISION
            END AS TASA_EMISION,
            NULL AS TASA_BASE
	FROM	OPERACIONES         OP  WITH (NOLOCK),
			OPERACIONES_DETALLE OPD WITH (NOLOCK),
			MOV_ACTIVOS         MA  WITH (NOLOCK),
			VIEW_NEMOTECNICOS   VN  WITH (NOLOCK),
			EMISORES_ESPECIFICO EE  WITH (NOLOCK)
	WHERE	OPD.ID_OPERACION          = OP.ID_OPERACION
    AND		OP.ID_CUENTA              = @PID_CUENTA
    AND		OPD.ID_OPERACION_DETALLE  = MA.ID_OPERACION_DETALLE
    AND		OP.COD_ESTADO             <> 'A'
    AND		OP.FECHA_OPERACION        BETWEEN @PFECHA_INI AND @PFECHA_TER
    AND		VN.ID_NEMOTECNICO         = OPD.ID_NEMOTECNICO
    AND		EE.ID_EMISOR_ESPECIFICO   = VN.ID_EMISOR_ESPECIFICO
    AND		VN.COD_PRODUCTO           IN (dbo.Pkg_Global$gcPROD_RF_NAC())
    AND		NOT OP.COD_TIPO_OPERACION IN ('SORTEOLET', 'VENC_IIF', 'VENC_RF')

	INSERT INTO @TBLANEXO3A
	SELECT
			CASE
				WHEN ((SELECT isnull(TIPO_FONDO,'') FROM CUENTAS_ACHS WITH (NOLOCK) WHERE ID_CUENTA = OP.ID_CUENTA)  <> '') THEN 
					(SELECT TIPO_FONDO FROM CUENTAS_ACHS WITH (NOLOCK) WHERE ID_CUENTA = OP.ID_CUENTA)
				ELSE 
					'O'
			END AS TIPO_FONDO,
            OP.ID_OPERACION AS FOLIO_OPERACION,
			OP.FECHA_OPERACION,
			'OF' AS TIPO_OPERACION,
            EE.RUT AS RUT_EMISOR,
            CASE
				WHEN ((VN.COD_INSTRUMENTO = dbo.Pkg_Global$gcINST_DEPOSITOS_NAC()) AND (DATEDIFF(D, @PFECHA_INI, VN.FECHA_VENCIMIENTO) <= 365)) THEN
					'DPC'
				WHEN ((VN.COD_INSTRUMENTO = dbo.Pkg_Global$gcINST_DEPOSITOS_NAC()) AND (DATEDIFF(D, @PFECHA_INI, VN.FECHA_VENCIMIENTO) > 365)) THEN
					'DPL'
            ELSE
				(
					SELECT	COD_SUBFAMILIA
					FROM	SUBFAMILIAS  WITH (NOLOCK)
					WHERE	ID_SUBFAMILIA = VN.ID_SUBFAMILIA)
			END AS TIPO_INSTRUMENTO,
			VN.NEMOTECNICO,
			(
				SELECT	ISNULL(COD_ACHS,'')
				FROM	REL_NEMOTECNICO_ATRIBUTOS WITH (NOLOCK)
				WHERE	ID_NEMOTECNICO = VN.ID_NEMOTECNICO) AS SERIE,
			VN.FECHA_EMISION AS FECHA_EMISION,
			OPD.CANTIDAD AS UNIDADES_NOMINALES,
			(
				SELECT	VALOR
				FROM	REL_CONVERSIONES WITH (NOLOCK)
				WHERE	ID_ENTIDAD =VN.ID_MONEDA
				AND		ID_ORIGEN = @ID_ORIGEN
				AND		ID_TIPO_CONVERSION = @ID_TIPO_CONVERSION
			) AS UNIDAD_MONETARIA,
			'N' AS PROHIBICION,
			CASE
				WHEN (VN.COD_INSTRUMENTO = dbo.Pkg_Global$gcINST_DEPOSITOS_NAC()) THEN 'U'
				ELSE 'S'
            END AS CON_DESCRIPTOR,
            'NR' AS RELACIONADO,
			ISNULL(
					(
						SELECT	COD_VALOR_CLASIFICACION
						FROM	REL_NEMOTECNICO_VALOR_CLASIFIC WITH (NOLOCK)
						WHERE	ID_NEMOTECNICO = VN.ID_NEMOTECNICO
					), 'S/C'
			) AS CLASIFICACION_RIESGO,
            CASE
				WHEN (OP.FLG_TIPO_MOVIMIENTO = 'I') THEN
					(
						SELECT	SA.VALOR_PAR
						FROM	SALDOS_ACTIVOS SA WITH (NOLOCK),
								MOV_ACTIVOS MAC   WITH (NOLOCK)
						WHERE	SA.ID_SALDO_ACTIVO = MAC.ID_SALDO_ACTIVO
						AND		MAC.ID_OPERACION_DETALLE = OPD.ID_OPERACION_DETALLE
						AND		SA.ID_CUENTA = OP.ID_CUENTA
						AND		SA.ID_NEMOTECNICO = OPD.ID_NEMOTECNICO
					)
				WHEN (OP.FLG_TIPO_MOVIMIENTO = 'E') THEN
					(
						SELECT	SA.VALOR_PAR
						FROM	SALDOS_ACTIVOS SA WITH (NOLOCK),
								MOV_ACTIVOS MAC   WITH (NOLOCK)
						WHERE	SA.ID_SALDO_ACTIVO = MAC.ID_SALDO_ACTIVO
						AND		MAC.ID_MOV_ACTIVO = MA.ID_MOV_ACTIVO_COMPRA
						AND		SA.ID_CUENTA = OP.ID_CUENTA
						AND		SA.ID_NEMOTECNICO = OPD.ID_NEMOTECNICO
					)
			END AS VALOR_PAR,
            OPD.PRECIO AS TIR_OPERACION,
            OPD.MONTO_PAGO AS VALOR_PRESENTE,
            OPD.MONTO_PAGO AS VALOR_OPERACION,
            VN.FECHA_VENCIMIENTO,
            CASE
				WHEN (OP.FLG_TIPO_MOVIMIENTO = 'I') THEN OP.FECHA_LIQUIDACION
				WHEN (OP.FLG_TIPO_MOVIMIENTO = 'E') THEN MA.FECHA_CONFIRMACION
            END AS FECHA_PAGO,
            CASE
				WHEN (VN.COD_INSTRUMENTO = dbo.Pkg_Global$gcINST_DEPOSITOS_NAC()) THEN 'F'
				ELSE 'I'
            END AS TIPO_NOMINALES,
            CASE
				WHEN (VN.COD_INSTRUMENTO = dbo.Pkg_Global$gcINST_DEPOSITOS_NAC()) THEN OPD.PRECIO
				ELSE VN.TASA_EMISION
            END AS TASA_EMISION,
            NULL AS TASA_BASE
     FROM	OPERACIONES         OP  WITH (NOLOCK),
			OPERACIONES_DETALLE OPD WITH (NOLOCK),
			MOV_ACTIVOS         MA  WITH (NOLOCK),
			VIEW_NEMOTECNICOS   VN  WITH (NOLOCK),
			EMISORES_ESPECIFICO EE  WITH (NOLOCK)
	WHERE	OPD.ID_OPERACION         = OP.ID_OPERACION
    AND		OPD.ID_OPERACION_DETALLE = MA.ID_OPERACION_DETALLE
    AND		OP.ID_CUENTA             = @PID_CUENTA
    AND		OP.COD_ESTADO			 <> 'A'
    AND		VN.ID_NEMOTECNICO        = OPD.ID_NEMOTECNICO
    AND		EE.ID_EMISOR_ESPECIFICO  = VN.ID_EMISOR_ESPECIFICO
    AND		OP.FECHA_OPERACION       BETWEEN @PFECHA_INI AND @PFECHA_TER
    AND		VN.COD_PRODUCTO          IN (dbo.Pkg_Global$gcPROD_RF_NAC())
    AND		OP.COD_TIPO_OPERACION    IN ('VENC_IIF', 'VENC_RF')

    INSERT INTO @TBLANEXO3A
	SELECT
			CASE
				WHEN ((SELECT isnull(TIPO_FONDO,'') FROM CUENTAS_ACHS WITH (NOLOCK) WHERE ID_CUENTA = OP.ID_CUENTA)  <> '')THEN 
					(	
						SELECT	TIPO_FONDO 
						FROM	CUENTAS_ACHS WITH (NOLOCK) 
						WHERE ID_CUENTA = OP.ID_CUENTA
					)
				ELSE 
					'O'
            END AS TIPO_FONDO,
            OP.ID_OPERACION AS FOLIO_OPERACION,
			OP.FECHA_OPERACION,
			'OS' AS TIPO_OPERACION,
            EE.RUT AS RUT_EMISOR,
            CASE
				WHEN ((VN.COD_INSTRUMENTO = dbo.Pkg_Global$gcINST_DEPOSITOS_NAC()) AND (DATEDIFF(D, @PFECHA_INI, VN.FECHA_VENCIMIENTO) <= 365)) THEN
					'DPC'
				WHEN ((VN.COD_INSTRUMENTO = dbo.Pkg_Global$gcINST_DEPOSITOS_NAC()) AND (DATEDIFF(D, @PFECHA_INI, VN.FECHA_VENCIMIENTO) > 365)) THEN
					'DPL'
				ELSE
					(
						SELECT	COD_SUBFAMILIA
						FROM	SUBFAMILIAS WITH (NOLOCK)
						WHERE	ID_SUBFAMILIA = VN.ID_SUBFAMILIA)
            END AS TIPO_INSTRUMENTO,
			VN.NEMOTECNICO,
			(
				SELECT	ISNULL(COD_ACHS,'')
				FROM	REL_NEMOTECNICO_ATRIBUTOS WITH (NOLOCK)
				WHERE	ID_NEMOTECNICO = VN.ID_NEMOTECNICO
			) AS SERIE,
			VN.FECHA_EMISION AS FECHA_EMISION,
			OPD.CANTIDAD AS UNIDADES_NOMINALES,
			(
				SELECT	VALOR
				FROM	REL_CONVERSIONES WITH (NOLOCK)
				WHERE	ID_ENTIDAD =VN.ID_MONEDA
				AND		ID_ORIGEN = @ID_ORIGEN
				AND		ID_TIPO_CONVERSION = @ID_TIPO_CONVERSION
			) AS UNIDAD_MONETARIA,
			'N' AS PROHIBICION,
            CASE
				WHEN (VN.COD_INSTRUMENTO = dbo.Pkg_Global$gcINST_DEPOSITOS_NAC()) THEN 'U'
				ELSE 'S'
            END AS CON_DESCRIPTOR,
            'NR' AS RELACIONADO,
			ISNULL(
					(
						SELECT	COD_VALOR_CLASIFICACION
						FROM	REL_NEMOTECNICO_VALOR_CLASIFIC WITH (NOLOCK)
						WHERE	ID_NEMOTECNICO = VN.ID_NEMOTECNICO
					), 'S/C'
			) AS CLASIFICACION_RIESGO,
            CASE
				WHEN (OP.FLG_TIPO_MOVIMIENTO = 'I') THEN
					(
						SELECT	SA.VALOR_PAR
						FROM	SALDOS_ACTIVOS SA WITH (NOLOCK),
								MOV_ACTIVOS MAC   WITH (NOLOCK)
						WHERE	SA.ID_SALDO_ACTIVO = MAC.ID_SALDO_ACTIVO
						AND		MAC.ID_OPERACION_DETALLE = OPD.ID_OPERACION_DETALLE
						AND		SA.ID_CUENTA = OP.ID_CUENTA
						AND		SA.ID_NEMOTECNICO = OPD.ID_NEMOTECNICO)
				WHEN (OP.FLG_TIPO_MOVIMIENTO = 'E') THEN
					(
						SELECT	SA.VALOR_PAR
						FROM	SALDOS_ACTIVOS SA WITH (NOLOCK),
								MOV_ACTIVOS MAC   WITH (NOLOCK)
						WHERE	SA.ID_SALDO_ACTIVO = MAC.ID_SALDO_ACTIVO
						AND		MAC.ID_MOV_ACTIVO = MA.ID_MOV_ACTIVO_COMPRA
						AND		SA.ID_CUENTA = OP.ID_CUENTA
						AND		SA.ID_NEMOTECNICO = OPD.ID_NEMOTECNICO)
			END AS VALOR_PAR,
			OPD.PRECIO AS TIR_OPERACION,
            OPD.MONTO_PAGO AS VALOR_PRESENTE,
            OPD.MONTO_PAGO AS VALOR_OPERACION,
            VN.FECHA_VENCIMIENTO,
            CASE
				WHEN (OP.FLG_TIPO_MOVIMIENTO = 'I') THEN OP.FECHA_LIQUIDACION
				WHEN (OP.FLG_TIPO_MOVIMIENTO = 'E') THEN MA.FECHA_CONFIRMACION
            END AS FECHA_PAGO,
            CASE
				WHEN (VN.COD_INSTRUMENTO = dbo.Pkg_Global$gcINST_DEPOSITOS_NAC()) THEN 'F'
				ELSE 'I'
            END AS TIPO_NOMINALES,
            CASE
				WHEN (VN.COD_INSTRUMENTO = dbo.Pkg_Global$gcINST_DEPOSITOS_NAC()) THEN OPD.PRECIO
				ELSE VN.TASA_EMISION
            END AS TASA_EMISION,
            NULL AS TASA_BASE
	FROM	OPERACIONES         OP  WITH (NOLOCK),
			OPERACIONES_DETALLE OPD WITH (NOLOCK),
			MOV_ACTIVOS         MA  WITH (NOLOCK),
			VIEW_NEMOTECNICOS   VN  WITH (NOLOCK),
			EMISORES_ESPECIFICO EE  WITH (NOLOCK)
	WHERE	OPD.ID_OPERACION         = OP.ID_OPERACION
    AND		OPD.ID_OPERACION_DETALLE = MA.ID_OPERACION_DETALLE
    AND		OP.ID_CUENTA             = @PID_CUENTA
    AND		OP.COD_ESTADO            <> 'A'
    AND		OP.FECHA_OPERACION       BETWEEN @PFECHA_INI AND @PFECHA_TER
    AND		VN.ID_NEMOTECNICO        = OPD.ID_NEMOTECNICO
    AND		EE.ID_EMISOR_ESPECIFICO  = VN.ID_EMISOR_ESPECIFICO
    AND		VN.COD_PRODUCTO          IN (dbo.Pkg_Global$gcPROD_RF_NAC())
    AND		OP.COD_TIPO_OPERACION    IN ('SORTEOLET')


    SELECT  TIPO_FONDO,
			FOLIO_OPERACION,
			FECHA_OPERACION,
            TIPO_OPERACION,
			RUT_EMISOR,
			TIPO_INSTRUMENTO,
            NEMOTECNICO,
			SERIE,
			FECHA_EMISION,
            UNIDADES_NOMINALES,
			(
				SELECT	CASE
							WHEN DSC_MONEDA ='PESOS' then
								'CLP'
							ELSE
								DSC_MONEDA 
						END 
				FROM	MONEDAS
				WHERE	COD_MONEDA = UNIDAD_MONETARIA
			) AS UNIDAD_MONETARIA,
			PROHIBICION,
            CON_DESCRIPTOR,
			RELACIONADO,
			CLASIFICACION_RIESGO,
            VALOR_PAR,
			TIR_OPERACION,
			VALOR_PRESENTE,
            VALOR_OPERACION,
			FECHA_VENCIMIENTO,
			FECHA_PAGO,
            TIPO_NOMINALES,
			TASA_EMISION,
			TASA_BASE
	FROM	@TBLANEXO3A 
	ORDER BY 
			FECHA_OPERACION, 
			FOLIO_OPERACION

	DROP table #TBLANEXO3A

END
SET NOCOUNT OFF
GO
GRANT EXECUTE ON [PKG_MovTotales_Buscar_Anexo3B] TO DB_EXECUTESP
GO