IF EXISTS (SELECT 1 FROM SYSOBJECTS WHERE name = 'PKG_MovTotales_Buscar_Buscar_AD5' AND xtype = 'P')
	DROP PROCEDURE [dbo].[PKG_MovTotales_Buscar_Buscar_AD5]
GO
CREATE PROCEDURE [dbo].[PKG_MovTotales_Buscar_Buscar_AD5](
	@PID_CUENTA NUMERIC(10),
	@PFECHA     DATETIME
)
AS
SET NOCOUNT ON
BEGIN

	DECLARE  @TMP_SALIDA TABLE(
		INSTRUMENTO			NUMERIC(10), 
		DESCRIPCION_EMISOR	VARCHAR(100),
		CANTIDAD			NUMERIC(24,6),
		VALORPRESENTE		NUMERIC(24,6),
		VALOR_CARTERA		NUMERIC(24,6)
	)

	INSERT INTO @TMP_SALIDA
	SELECT	1 AS INSTRUMENTO,
			EE.DSC_EMISOR_ESPECIFICO,
			SUM(ISNULL(SA.CANTIDAD,0)),
			SUM(ISNULL(MONTO_VALOR_COMPRA,0)),
			CASE
				WHEN (SA.ID_CUENTA IN (SELECT ID_CUENTA FROM CUENTAS_ACHS WITH (NOLOCK) WHERE TIPO_VALORIZACION = 'Compra' )) THEN
					SUM(DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(SA.ID_CUENTA,
                                                          SA.MONTO_VALOR_COMPRA,
                                                          SA.ID_MONEDA_CTA,
                                                          DBO.FNT_DAMEIDMONEDA(DBO.PKG_GLOBAL$cMoneda_Cod_Peso()),
                                                          SA.FECHA_CIERRE))
				WHEN (SA.ID_CUENTA IN (SELECT ID_CUENTA FROM CUENTAS_ACHS WITH (NOLOCK) WHERE TIPO_VALORIZACION = 'Mercado' )) THEN
				SUM(DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(SA.ID_CUENTA,
                                                          SA.MONTO_MON_CTA,
                                                          SA.ID_MONEDA_CTA,
                                                          DBO.FNT_DAMEIDMONEDA(DBO.PKG_GLOBAL$cMoneda_Cod_Peso()),
                                                          SA.FECHA_CIERRE))
			END AS VALOR_CARTERA
	FROM SALDOS_ACTIVOS SA WITH (NOLOCK),
         NEMOTECNICOS N    WITH (NOLOCK),
         EMISORES_ESPECIFICO EE WITH (NOLOCK)
	WHERE SA.ID_NEMOTECNICO      = N.ID_NEMOTECNICO
    AND N.ID_EMISOR_ESPECIFICO = EE.ID_EMISOR_ESPECIFICO
    AND EE.COD_CLASIFICADOR    = 'M'
    AND SA.FECHA_CIERRE        = @PFECHA
    AND SA.ID_CUENTA           = @PID_CUENTA
	GROUP BY SA.ID_CUENTA, EE.DSC_EMISOR_ESPECIFICO

	INSERT INTO @TMP_SALIDA
	SELECT	2 AS INSTRUMENTO,
			EE.DSC_EMISOR_ESPECIFICO,
			SUM(ISNULL(SA.CANTIDAD,0)),
			SUM(ISNULL(MONTO_VALOR_COMPRA,0)),
			CASE
				WHEN (SA.ID_CUENTA IN (SELECT ID_CUENTA FROM CUENTAS_ACHS WITH (NOLOCK) WHERE TIPO_VALORIZACION = 'Compra' )) THEN
					SUM(DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(SA.ID_CUENTA,
                                                          SA.MONTO_VALOR_COMPRA,
                                                          SA.ID_MONEDA_CTA,
                                                          DBO.FNT_DAMEIDMONEDA(DBO.PKG_GLOBAL$cMoneda_Cod_Peso()),
                                                          SA.FECHA_CIERRE))
				WHEN (SA.ID_CUENTA IN (SELECT ID_CUENTA FROM CUENTAS_ACHS WITH (NOLOCK) WHERE TIPO_VALORIZACION = 'Mercado' )) THEN
				SUM(DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(SA.ID_CUENTA,
                                                          SA.MONTO_MON_CTA,
                                                          SA.ID_MONEDA_CTA,
                                                          DBO.FNT_DAMEIDMONEDA(DBO.PKG_GLOBAL$cMoneda_Cod_Peso()),
                                                          SA.FECHA_CIERRE))
			END AS VALOR_CARTERA
    FROM SALDOS_ACTIVOS SA WITH (NOLOCK),
         NEMOTECNICOS N WITH (NOLOCK),
         EMISORES_ESPECIFICO EE WITH (NOLOCK)
	WHERE SA.ID_NEMOTECNICO      = N.ID_NEMOTECNICO
     AND N.ID_EMISOR_ESPECIFICO = EE.ID_EMISOR_ESPECIFICO
     AND EE.COD_CLASIFICADOR    = 'I'
     AND SA.FECHA_CIERRE        = @PFECHA
     AND SA.ID_CUENTA           = @PID_CUENTA
	GROUP BY SA.ID_CUENTA, EE.DSC_EMISOR_ESPECIFICO

	INSERT INTO @TMP_SALIDA
	SELECT	3 AS INSTRUMENTO,
			EE.DSC_EMISOR_ESPECIFICO,
			SUM(ISNULL(SA.CANTIDAD,0)),
			SUM(ISNULL(MONTO_VALOR_COMPRA,0)),
			CASE
				WHEN (SA.ID_CUENTA IN (SELECT ID_CUENTA FROM CUENTAS_ACHS WITH (NOLOCK) WHERE TIPO_VALORIZACION = 'Compra' )) THEN
					SUM(DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(SA.ID_CUENTA,
                                                          SA.MONTO_VALOR_COMPRA,
                                                          SA.ID_MONEDA_CTA,
                                                          DBO.FNT_DAMEIDMONEDA(DBO.PKG_GLOBAL$cMoneda_Cod_Peso()),
                                                          SA.FECHA_CIERRE))
				WHEN (SA.ID_CUENTA IN (SELECT ID_CUENTA FROM CUENTAS_ACHS WITH (NOLOCK) WHERE TIPO_VALORIZACION = 'Mercado' )) THEN
					SUM(DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(SA.ID_CUENTA,
                                                          SA.MONTO_MON_CTA,
                                                          SA.ID_MONEDA_CTA,
                                                          DBO.FNT_DAMEIDMONEDA(DBO.PKG_GLOBAL$cMoneda_Cod_Peso()),
                                                          SA.FECHA_CIERRE))
			END AS VALOR_CARTERA
    FROM SALDOS_ACTIVOS SA  WITH (NOLOCK),
         NEMOTECNICOS N   WITH (NOLOCK),
         EMISORES_ESPECIFICO EE  WITH (NOLOCK)
	WHERE SA.ID_NEMOTECNICO      = N.ID_NEMOTECNICO
    AND N.ID_EMISOR_ESPECIFICO = EE.ID_EMISOR_ESPECIFICO
    AND EE.COD_CLASIFICADOR    = 'E'
    AND SA.FECHA_CIERRE        = @PFECHA
    AND SA.ID_CUENTA           = @PID_CUENTA
	GROUP BY SA.ID_CUENTA, EE.DSC_EMISOR_ESPECIFICO


	SELECT	INSTRUMENTO,
			DESCRIPCION_EMISOR,
			CANTIDAD,
			VALORPRESENTE,
			VALOR_CARTERA
	FROM		@TMP_SALIDA 
	ORDER BY INSTRUMENTO

END
SET NOCOUNT OFF
