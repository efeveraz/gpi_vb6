USE [CSGPI] 
GO

IF EXISTS (SELECT 1 FROM SYSOBJECTS WHERE name = 'PKG_MovTotales_Buscar_AD4' AND xtype = 'P')
	DROP PROCEDURE PKG_MovTotales_Buscar_AD4
GO
CREATE PROCEDURE PKG_MovTotales_Buscar_AD4( 
	@PID_CUENTA NUMERIC,
	@PFECHA     DATETIME
)
AS
SET NOCOUNT ON
BEGIN

	DECLARE @MONTO			NUMERIC(24,6),
			@vCantidad		NUMERIC(24,6),
			@vValorPresente NUMERIC(24,6)

    DECLARE @TMP_SALIDA TABLE (
		INSTRUMENTO		NUMERIC(10), 
		Cantidad		NUMERIC(24,6),
		ValorPresente	NUMERIC(24,6),
		ValorCartera	NUMERIC(24,6)
	)

	IF (	SELECT	COUNT(SA.MONTO_VALOR_COMPRA)
			FROM	SALDOS_ACTIVOS SA WITH (NOLOCK),
					NEMOTECNICOS N    WITH (NOLOCK),
					EMISORES_ESPECIFICO EE WITH (NOLOCK)
			WHERE	SA.ID_NEMOTECNICO      = N.ID_NEMOTECNICO
			AND		N.ID_EMISOR_ESPECIFICO = EE.ID_EMISOR_ESPECIFICO
			AND		EE.RUT                 IN ('60805000-0', '97029000-1', '61801000-7', '61533000-0')
			AND		EE.COD_CLASIFICADOR    = 'E'
			AND		SA.FECHA_CIERRE        = @PFECHA
			AND		SA.ID_CUENTA           = @PID_CUENTA) = 0
    BEGIN

		INSERT INTO @TMP_SALIDA
        SELECT	1,
				0,
				0,
				0
    END
    ELSE
    BEGIN
		INSERT INTO @TMP_SALIDA
        SELECT	1 AS INSTRUMENTO,
				sum(SA.Cantidad),
				sum(SA.MONTO_VALOR_COMPRA),
				CASE
					WHEN (SA.ID_CUENTA IN (SELECT ID_CUENTA FROM CUENTAS_ACHS WITH (NOLOCK) WHERE TIPO_VALORIZACION = 'Compra')) THEN
						SUM(DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD( SA.ID_CUENTA
                                                            , SA.MONTO_VALOR_COMPRA
                                                            , SA.ID_MONEDA_CTA
                                                            , DBO.FNT_DAMEIDMONEDA(DBO.PKG_GLOBAL$cMoneda_Cod_Peso())
                                                            , SA.FECHA_CIERRE))
					WHEN (SA.ID_CUENTA IN (SELECT ID_CUENTA FROM CUENTAS_ACHS WITH (NOLOCK) WHERE TIPO_VALORIZACION = 'Mercado')) THEN
						SUM(DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD( SA.ID_CUENTA
                                                            , SA.MONTO_MON_CTA
                                                            , SA.ID_MONEDA_CTA
                                                            , DBO.FNT_DAMEIDMONEDA(DBO.PKG_GLOBAL$cMoneda_Cod_Peso())
                                                            , SA.FECHA_CIERRE))
				END AS VALOR_CARTERA
		FROM	SALDOS_ACTIVOS SA WITH (NOLOCK),
				NEMOTECNICOS N    WITH (NOLOCK),
				EMISORES_ESPECIFICO EE  WITH (NOLOCK)
		WHERE	SA.ID_NEMOTECNICO      = N.ID_NEMOTECNICO
        AND		N.ID_EMISOR_ESPECIFICO = EE.ID_EMISOR_ESPECIFICO
        AND		EE.RUT                 IN ('60805000-0', '97029000-1', '61801000-7', '61533000-0')
        AND		EE.COD_CLASIFICADOR    = 'E'
        AND		SA.FECHA_CIERRE        = @PFECHA
        AND		SA.ID_CUENTA           = @PID_CUENTA
		GROUP BY SA.ID_CUENTA
    END

    IF (	SELECT	COUNT(SA.MONTO_VALOR_COMPRA)
			FROM	SALDOS_ACTIVOS SA WITH (NOLOCK),
					NEMOTECNICOS N    WITH (NOLOCK),
					SUBFAMILIAS S     WITH (NOLOCK),
					EMISORES_ESPECIFICO EE WITH (NOLOCK)
			WHERE SA.ID_NEMOTECNICO      = N.ID_NEMOTECNICO
			AND N.ID_EMISOR_ESPECIFICO = EE.ID_EMISOR_ESPECIFICO
			AND N.ID_SUBFAMILIA        = S.ID_SUBFAMILIA
			AND NOT ISNULL(EE.RUT,'')  IN ('60805000-0', '97029000-1', '61801000-7', '61533000-0')
			AND EE.COD_CLASIFICADOR    = 'I'
			AND NOT S.COD_SUBFAMILIA   = 'LH'
			AND SA.FECHA_CIERRE        = @PFECHA
			AND SA.ID_CUENTA           = @PID_CUENTA) = 0
    BEGIN
        INSERT INTO @TMP_SALIDA
        SELECT	2 AS INSTRUMENTO,
				0 AS CANTIDAD,
				0 AS VALORPRESENTE,
				0 AS VALOR_CARTERA
    END
    ELSE
    BEGIN

		SELECT	@MONTO=isnull((	CASE
								WHEN (SA.ID_CUENTA IN (SELECT ID_CUENTA FROM CUENTAS_ACHS WITH (NOLOCK) WHERE TIPO_VALORIZACION = 'Compra')) THEN
									SUM(DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD( SA.ID_CUENTA
                                                                          , SA.MONTO_VALOR_COMPRA
                                                                          , SA.ID_MONEDA_CTA
                                                                          , DBO.FNT_DAMEIDMONEDA(DBO.PKG_GLOBAL$cMoneda_Cod_Peso())
                                                                          , SA.FECHA_CIERRE))
								WHEN (SA.ID_CUENTA IN (SELECT ID_CUENTA FROM CUENTAS_ACHS WITH (NOLOCK) WHERE TIPO_VALORIZACION = 'Mercado')) THEN
									SUM(DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD( SA.ID_CUENTA
                                                                          , SA.MONTO_MON_CTA
                                                                          , SA.ID_MONEDA_CTA
                                                                          , DBO.FNT_DAMEIDMONEDA(DBO.PKG_GLOBAL$cMoneda_Cod_Peso())
                                                                          , SA.FECHA_CIERRE))
								END),0),
				@vCantidad = SUM(ISNULL(SA.CANTIDAD,0)),
				@vValorPresente = SUM(ISNULL(SA.MONTO_VALOR_COMPRA,0))
		FROM	SALDOS_ACTIVOS SA WITH (NOLOCK),
				NEMOTECNICOS N    WITH (NOLOCK),
				SUBFAMILIAS S     WITH (NOLOCK),
				EMISORES_ESPECIFICO EE WITH (NOLOCK)
		WHERE	SA.ID_NEMOTECNICO      = N.ID_NEMOTECNICO
        AND		N.ID_EMISOR_ESPECIFICO = EE.ID_EMISOR_ESPECIFICO
        AND		N.ID_SUBFAMILIA        = S.ID_SUBFAMILIA
        AND		NOT ISNULL(EE.RUT,'')  IN ('60805000-0', '97029000-1', '61801000-7', '61533000-0')
        AND		EE.COD_CLASIFICADOR    = 'I'
        AND		NOT S.COD_SUBFAMILIA   = 'LH'
        AND		SA.FECHA_CIERRE        = @PFECHA
        AND		SA.ID_CUENTA           = @PID_CUENTA
		GROUP BY SA.ID_CUENTA

		SELECT	@MONTO = @MONTO + isnull(CASE
									WHEN (SA.ID_CUENTA IN (SELECT ID_CUENTA FROM CUENTAS_ACHS WITH (NOLOCK) WHERE TIPO_VALORIZACION = 'Compra')) THEN
										SUM(DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD( SA.ID_CUENTA
																				, SA.MONTO_VALOR_COMPRA
																				, SA.ID_MONEDA_CTA
																				, DBO.FNT_DAMEIDMONEDA(DBO.PKG_GLOBAL$cMoneda_Cod_Peso())
																				, SA.FECHA_CIERRE))
									WHEN (SA.ID_CUENTA IN (SELECT ID_CUENTA FROM CUENTAS_ACHS WITH (NOLOCK) WHERE TIPO_VALORIZACION = 'Mercado')) THEN
										SUM(DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD( SA.ID_CUENTA
																				, SA.MONTO_MON_CTA
																				, SA.ID_MONEDA_CTA
																				, DBO.FNT_DAMEIDMONEDA(DBO.PKG_GLOBAL$cMoneda_Cod_Peso())
																				, SA.FECHA_CIERRE))
									END,0),
				@vCantidad = @vCantidad + SUM(ISNULL(SA.CANTIDAD,0)),
				@vValorPresente = @vValorPresente + SUM(ISNULL(SA.MONTO_VALOR_COMPRA,0))
		FROM	SALDOS_ACTIVOS SA WITH (NOLOCK),
				NEMOTECNICOS N    WITH (NOLOCK),
				EMISORES_ESPECIFICO EE WITH (NOLOCK)
		WHERE	SA.ID_NEMOTECNICO      = N.ID_NEMOTECNICO
        AND		N.ID_EMISOR_ESPECIFICO = EE.ID_EMISOR_ESPECIFICO
        AND		N.ID_SUBFAMILIA        IS NULL
        AND		NOT ISNULL(EE.RUT,'')  IN ('60805000-0', '97029000-1', '61801000-7', '61533000-0')
        AND		EE.COD_CLASIFICADOR    = 'I'
        AND		N.COD_INSTRUMENTO      = 'DEPOSITO_NAC'
        AND		SA.FECHA_CIERRE        = @PFECHA
        AND		SA.ID_CUENTA           = @PID_CUENTA
		GROUP BY SA.ID_CUENTA


		--SET @MONTO = (
		--				SELECT	CASE
		--						WHEN (SA.ID_CUENTA IN (SELECT ID_CUENTA FROM CUENTAS_ACHS WITH (NOLOCK) WHERE TIPO_VALORIZACION = 'Compra')) THEN
		--							SUM(DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD( SA.ID_CUENTA
  --                                                                        , SA.MONTO_VALOR_COMPRA
  --                                                                        , SA.ID_MONEDA_CTA
  --                                                                        , DBO.FNT_DAMEIDMONEDA(DBO.PKG_GLOBAL$cMoneda_Cod_Peso())
  --                                                                        , SA.FECHA_CIERRE))
		--						WHEN (SA.ID_CUENTA IN (SELECT ID_CUENTA FROM CUENTAS_ACHS WITH (NOLOCK) WHERE TIPO_VALORIZACION = 'Mercado')) THEN
		--							SUM(DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD( SA.ID_CUENTA
  --                                                                        , SA.MONTO_MON_CTA
  --                                                                        , SA.ID_MONEDA_CTA
  --                                                                        , DBO.FNT_DAMEIDMONEDA(DBO.PKG_GLOBAL$cMoneda_Cod_Peso())
  --                                                                        , SA.FECHA_CIERRE))
		--						END AS VALOR_CARTERA
		--				FROM	SALDOS_ACTIVOS SA WITH (NOLOCK),
		--						NEMOTECNICOS N    WITH (NOLOCK),
		--						SUBFAMILIAS S     WITH (NOLOCK),
		--						EMISORES_ESPECIFICO EE WITH (NOLOCK)
		--				WHERE	SA.ID_NEMOTECNICO      = N.ID_NEMOTECNICO
  --                      AND		N.ID_EMISOR_ESPECIFICO = EE.ID_EMISOR_ESPECIFICO
  --                      AND		N.ID_SUBFAMILIA        = S.ID_SUBFAMILIA
  --                      AND		NOT ISNULL(EE.RUT,'')  IN ('60805000-0', '97029000-1', '61801000-7', '61533000-0')
  --                      AND		EE.COD_CLASIFICADOR    = 'I'
  --                      AND		NOT S.COD_SUBFAMILIA   = 'LH'
  --                      AND		SA.FECHA_CIERRE        = @PFECHA
  --                      AND		SA.ID_CUENTA           = @PID_CUENTA
		--				GROUP BY SA.ID_CUENTA)

		--SET @MONTO = @MONTO + ISNULL((	SELECT	CASE
		--										WHEN (SA.ID_CUENTA IN (SELECT ID_CUENTA FROM CUENTAS_ACHS WITH (NOLOCK) WHERE TIPO_VALORIZACION = 'Compra')) THEN
		--											SUM(DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD( SA.ID_CUENTA
  --                                                                                        , SA.MONTO_VALOR_COMPRA
  --                                                                                        , SA.ID_MONEDA_CTA
  --                                                                                        , DBO.FNT_DAMEIDMONEDA(DBO.PKG_GLOBAL$cMoneda_Cod_Peso())
  --                                                                                        , SA.FECHA_CIERRE))
		--										WHEN (SA.ID_CUENTA IN (SELECT ID_CUENTA FROM CUENTAS_ACHS WITH (NOLOCK) WHERE TIPO_VALORIZACION = 'Mercado')) THEN
		--											SUM(DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD( SA.ID_CUENTA
  --                                                                                        , SA.MONTO_MON_CTA
  --                                                                                        , SA.ID_MONEDA_CTA
  --                                                                                        , DBO.FNT_DAMEIDMONEDA(DBO.PKG_GLOBAL$cMoneda_Cod_Peso())
  --                                                                                        , SA.FECHA_CIERRE))
		--										END AS VALOR_CARTERA
		--								FROM	SALDOS_ACTIVOS SA WITH (NOLOCK),
		--										NEMOTECNICOS N    WITH (NOLOCK),
		--										EMISORES_ESPECIFICO EE WITH (NOLOCK)
		--								WHERE	SA.ID_NEMOTECNICO      = N.ID_NEMOTECNICO
  --                                      AND		N.ID_EMISOR_ESPECIFICO = EE.ID_EMISOR_ESPECIFICO
  --                                      AND		N.ID_SUBFAMILIA        IS NULL
  --                                      AND		NOT ISNULL(EE.RUT,'')  IN ('60805000-0', '97029000-1', '61801000-7', '61533000-0')
  --                                      AND		EE.COD_CLASIFICADOR    = 'I'
  --                                      AND		N.COD_INSTRUMENTO      = 'DEPOSITO_NAC'
  --                                      AND		SA.FECHA_CIERRE        = @PFECHA
  --                                      AND		SA.ID_CUENTA           = @PID_CUENTA
		--								GROUP BY SA.ID_CUENTA), 0)

			

        INSERT INTO @TMP_SALIDA
        SELECT	2 AS INSTRUMENTO,
				@vCantidad,
				@vValorPresente,
				@MONTO AS VALOR_CARTERA
    END

	IF (	SELECT	COUNT(SA.MONTO_VALOR_COMPRA)
			FROM	SALDOS_ACTIVOS SA WITH (NOLOCK),
					NEMOTECNICOS N    WITH (NOLOCK),
					SUBFAMILIAS S     WITH (NOLOCK),
					EMISORES_ESPECIFICO EE WITH (NOLOCK)
			WHERE SA.ID_NEMOTECNICO      = N.ID_NEMOTECNICO
			AND N.ID_EMISOR_ESPECIFICO = EE.ID_EMISOR_ESPECIFICO
			AND N.ID_SUBFAMILIA        = S.ID_SUBFAMILIA
			AND NOT ISNULL(EE.RUT,'')  IN ('60805000-0', '97029000-1', '61801000-7', '61533000-0')
			AND EE.COD_CLASIFICADOR    = 'I'
			AND N.COD_INSTRUMENTO      IN ('BONOS_NAC')
			AND S.COD_SUBFAMILIA       = 'LH'
			AND SA.FECHA_CIERRE        = @PFECHA
			AND SA.ID_CUENTA           = @PID_CUENTA) = 0
    BEGIN
        INSERT INTO @TMP_SALIDA
        SELECT	3 AS INSTRUMENTO,
				0,
				0,
				0 AS VALOR_CARTERA
    END
    ELSE
    BEGIN

		select	@vCantidad = 0,
				@vValorPresente = 0,
				@MONTO =0

        INSERT INTO @TMP_SALIDA
        SELECT	3 AS INSTRUMENTO,
				SUM(ISNULL(SA.CANTIDAD,0)),
				SUM(ISNULL(SA.MONTO_VALOR_COMPRA,0)),
				isnull(CASE
					WHEN (SA.ID_CUENTA IN (SELECT ID_CUENTA FROM CUENTAS_ACHS WITH (NOLOCK) WHERE TIPO_VALORIZACION = 'Compra'))  THEN
						SUM(DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD( SA.ID_CUENTA
                                                            , SA.MONTO_VALOR_COMPRA
                                                            , SA.ID_MONEDA_CTA
                                                            , DBO.FNT_DAMEIDMONEDA(DBO.PKG_GLOBAL$cMoneda_Cod_Peso())
                                                            , SA.FECHA_CIERRE))
					WHEN (SA.ID_CUENTA IN (SELECT ID_CUENTA FROM CUENTAS_ACHS WITH (NOLOCK) WHERE TIPO_VALORIZACION = 'Mercado')) THEN
						SUM(DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD( SA.ID_CUENTA
                                                            , SA.MONTO_MON_CTA
                                                            , SA.ID_MONEDA_CTA
                                                            , DBO.FNT_DAMEIDMONEDA(DBO.PKG_GLOBAL$cMoneda_Cod_Peso())
                                                            , SA.FECHA_CIERRE))
               END,0) AS VALOR_CARTERA
          FROM SALDOS_ACTIVOS SA  WITH (NOLOCK)
             , NEMOTECNICOS N     WITH (NOLOCK)
             , SUBFAMILIAS S      WITH (NOLOCK)
             , EMISORES_ESPECIFICO EE   WITH (NOLOCK)
         WHERE SA.ID_NEMOTECNICO      = N.ID_NEMOTECNICO
           AND N.ID_EMISOR_ESPECIFICO = EE.ID_EMISOR_ESPECIFICO
           AND N.ID_SUBFAMILIA        = S.ID_SUBFAMILIA
           AND NOT ISNULL(EE.RUT,'')  IN ('60805000-0', '97029000-1', '61801000-7', '61533000-0')
           AND EE.COD_CLASIFICADOR    = 'I'
           AND N.COD_INSTRUMENTO      IN ('BONOS_NAC')
           AND S.COD_SUBFAMILIA       = 'LH'
           AND SA.FECHA_CIERRE        = @PFECHA
           AND SA.ID_CUENTA           = @PID_CUENTA
      GROUP BY SA.ID_CUENTA
    END
	
	
	-- parte 4

	SELECT	@MONTO = ISNULL(CASE
							WHEN (SA.ID_CUENTA IN (SELECT ID_CUENTA FROM CUENTAS_ACHS WITH (NOLOCK) WHERE TIPO_VALORIZACION = 'Compra')) THEN
								SUM(DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD( SA.ID_CUENTA
																	, SA.MONTO_VALOR_COMPRA
																	, SA.ID_MONEDA_CTA
																	, DBO.FNT_DAMEIDMONEDA(DBO.PKG_GLOBAL$cMoneda_Cod_Peso())
																	, SA.FECHA_CIERRE))
							WHEN (SA.ID_CUENTA IN (SELECT ID_CUENTA FROM CUENTAS_ACHS WITH (NOLOCK) WHERE TIPO_VALORIZACION = 'Mercado')) THEN
								SUM(DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD( SA.ID_CUENTA
																	, SA.MONTO_MON_CTA
																	, SA.ID_MONEDA_CTA
																	, DBO.FNT_DAMEIDMONEDA(DBO.PKG_GLOBAL$cMoneda_Cod_Peso())
																	, SA.FECHA_CIERRE))
						END,0),
			@vCantidad = SUM(ISNULL(SA.CANTIDAD,0)),
			--@vValorPresente = SUM(ISNULL(SA.MONTO_VALOR_COMPRA,0))

			@vValorPresente =( CASE
				WHEN (DBO.FNT_DAMEIDMONEDA(DBO.PKG_GLOBAL$cMoneda_Cod_Peso()) = SA.ID_MONEDA_CTA) THEN 
					SA.MONTO_VALOR_COMPRA
				ELSE 
					DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(	SA.ID_CUENTA,
															SA.MONTO_VALOR_COMPRA,
															SA.ID_MONEDA_CTA,
															DBO.FNT_DAMEIDMONEDA(DBO.PKG_GLOBAL$cMoneda_Cod_Peso()),
															SA.FECHA_CIERRE
														)
            END)

    FROM SALDOS_ACTIVOS SA  WITH (NOLOCK)
        , NEMOTECNICOS N     WITH (NOLOCK)
        , SUBFAMILIAS S      WITH (NOLOCK)
        , EMISORES_ESPECIFICO EE  WITH (NOLOCK)
    WHERE SA.ID_NEMOTECNICO = N.ID_NEMOTECNICO
        AND N.ID_EMISOR_ESPECIFICO = EE.ID_EMISOR_ESPECIFICO
        AND N.ID_SUBFAMILIA        = S.ID_SUBFAMILIA
        AND NOT ISNULL(EE.RUT,'')  IN ('60805000-0', '97029000-1', '61801000-7', '61533000-0')
        AND EE.COD_CLASIFICADOR    = 'M'
        AND NOT S.COD_SUBFAMILIA   IN ('PRC', 'PRD', 'LH')
        AND SA.FECHA_CIERRE        = @PFECHA
        AND SA.ID_CUENTA           = @PID_CUENTA
	GROUP BY 
			SA.ID_CUENTA,
			SA.ID_MONEDA_CTA,
			SA.MONTO_VALOR_COMPRA,
			SA.FECHA_CIERRE


	SELECT	@MONTO = @MONTO + isnull(CASE
                                         WHEN (SA.ID_CUENTA IN (SELECT ID_CUENTA FROM CUENTAS_ACHS WITH (NOLOCK) WHERE TIPO_VALORIZACION = 'Compra')) THEN
                                             SUM(DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD( SA.ID_CUENTA,
                                                                                        SA.MONTO_VALOR_COMPRA,
                                                                                        SA.ID_MONEDA_CTA,
                                                                                        DBO.FNT_DAMEIDMONEDA(DBO.PKG_GLOBAL$cMoneda_Cod_Peso()),
                                                                                        SA.FECHA_CIERRE))
                                         WHEN (SA.ID_CUENTA IN (SELECT ID_CUENTA FROM CUENTAS_ACHS WITH (NOLOCK) WHERE TIPO_VALORIZACION = 'Mercado')) THEN
                                             SUM(DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD( SA.ID_CUENTA,
                                                                                         SA.MONTO_MON_CTA,
                                                                                         SA.ID_MONEDA_CTA,
                                                                                         DBO.FNT_DAMEIDMONEDA(DBO.PKG_GLOBAL$cMoneda_Cod_Peso()),
                                                                                         SA.FECHA_CIERRE))
                                         END,0),
			@vCantidad = @vCantidad + SUM(ISNULL(SA.CANTIDAD,0)),
			--@vValorPresente = @vValorPresente + SUM(ISNULL(SA.MONTO_VALOR_COMPRA,0))

			@vValorPresente = @vValorPresente +(CASE
												WHEN (DBO.FNT_DAMEIDMONEDA(DBO.PKG_GLOBAL$cMoneda_Cod_Peso()) = SA.ID_MONEDA_CTA) THEN 
													SA.MONTO_VALOR_COMPRA
												ELSE 
													DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(	SA.ID_CUENTA,
																							SA.MONTO_VALOR_COMPRA,
																							SA.ID_MONEDA_CTA,
																							DBO.FNT_DAMEIDMONEDA(DBO.PKG_GLOBAL$cMoneda_Cod_Peso()),
																							SA.FECHA_CIERRE
																						)
												END)

	FROM	SALDOS_ACTIVOS SA    WITH (NOLOCK),
			VIEW_NEMOTECNICOS VN WITH (NOLOCK)
	WHERE SA.ID_NEMOTECNICO             = VN.ID_NEMOTECNICO
	AND VN.COD_INSTRUMENTO            = dbo.Pkg_Global$gcINST_DEPOSITOS_NAC()
	AND SUBSTRING(VN.NEMOTECNICO,1,1) = 'S'
	AND SA.FECHA_CIERRE               = @PFECHA
	AND SA.ID_CUENTA                  = @PID_CUENTA
	GROUP BY 
			SA.ID_CUENTA,
			SA.ID_MONEDA_CTA,
			SA.MONTO_VALOR_COMPRA,
			SA.FECHA_CIERRE


    INSERT INTO @TMP_SALIDA
    SELECT	4 AS INSTRUMENTO,
			@vCantidad,
			@vValorPresente,
			@MONTO


	INSERT INTO @TMP_SALIDA
    SELECT	5 AS INSTRUMENTO,
			SUM(ISNULL(SA.CANTIDAD,0)),
			SUM(ISNULL(SA.MONTO_VALOR_COMPRA,0)),
			ISNULL(	CASE
						WHEN (SA.ID_CUENTA IN (SELECT ID_CUENTA FROM CUENTAS_ACHS WITH (NOLOCK) WHERE TIPO_VALORIZACION = 'Compra')) THEN
							SUM(DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD( SA.ID_CUENTA
																, SA.MONTO_VALOR_COMPRA
																, SA.ID_MONEDA_CTA
													   , DBO.FNT_DAMEIDMONEDA(DBO.PKG_GLOBAL$cMoneda_Cod_Peso())
																, SA.FECHA_CIERRE))
						WHEN (SA.ID_CUENTA IN (SELECT ID_CUENTA FROM CUENTAS_ACHS WITH (NOLOCK) WHERE TIPO_VALORIZACION = 'Mercado')) THEN
							SUM(DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD( SA.ID_CUENTA
																, SA.MONTO_MON_CTA
																, SA.ID_MONEDA_CTA
																, DBO.FNT_DAMEIDMONEDA(DBO.PKG_GLOBAL$cMoneda_Cod_Peso())
																, SA.FECHA_CIERRE))
				   END,0) AS VALOR_CARTERA
      FROM SALDOS_ACTIVOS SA  WITH (NOLOCK)
         , NEMOTECNICOS N     WITH (NOLOCK)
         , EMISORES_ESPECIFICO EE WITH (NOLOCK)
     WHERE SA.ID_NEMOTECNICO = N.ID_NEMOTECNICO
    AND		N.ID_EMISOR_ESPECIFICO = EE.ID_EMISOR_ESPECIFICO
    AND NOT ISNULL(EE.RUT,'')  IN ('60805000-0', '97029000-1', '61801000-7', '61533000-0')
    AND NOT N.COD_INSTRUMENTO  IN ('BONOS_NAC', 'DEPOSITO_NAC', 'PACTOS_NAC')
    AND		SA.FECHA_CIERRE        = @PFECHA
    AND		SA.ID_CUENTA           = @PID_CUENTA
	GROUP BY SA.ID_CUENTA

	SELECT	INSTRUMENTO,
			Cantidad,
			ValorPresente,
			ValorCartera
	FROM	@TMP_SALIDA 
	ORDER BY INSTRUMENTO

  

END
SET NOCOUNT OFF
GO
GRANT EXECUTE ON [PKG_MovTotales_Buscar_AD4] TO DB_EXECUTESP
GO