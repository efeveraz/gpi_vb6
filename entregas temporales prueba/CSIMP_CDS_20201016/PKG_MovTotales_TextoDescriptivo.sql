IF EXISTS (SELECT 1 FROM SYSOBJECTS WHERE name = 'PKG_MovTotales_TextoDescriptivo' AND xtype = 'P')
	DROP PROCEDURE [dbo].[PKG_MovTotales_TextoDescriptivo]
GO

CREATE PROCEDURE [dbo].[PKG_MovTotales_TextoDescriptivo]
AS
BEGIN
SET NOCOUNT ON
	DECLARE @TMP_Parametro_ExcelExplicativo TABLE (
		ID				NUMERIC(10) identity(1,1),
		Titulo			VARCHAR(200),
		Descripccion	VARCHAR(500)
	)

	INSERT INTO @TMP_Parametro_ExcelExplicativo 
	SELECT 'Explicaciones','' union all
	SELECT 'Folio Compra/Venta:','Folio inerno de la administradora para identificar la transaccion' union all
	SELECT 'Fecha Movto:','Fecha en que se lleva a cabo la transaccion' union all
	SELECT 'Tipo de Pago:','PM = Pagadero Ma�ana' union all
	SELECT 'Tipo de Pago:','PH = Pagadero Hoy' union all
	SELECT 'Tipo Instrumento:','Nombre Oficial del Instrumento en la Bolsa de Valores.' union all
	SELECT 'Plazo:','L = Largo Plazo' union all
	SELECT 'Plazo:','C = Corto Plazo' union all
	SELECT 'Fecha Emision:','Fecha en que se emite el instrumento' union all
	SELECT 'TIR:','TIR del instrumento al momento de la transaccion' union all
	SELECT 'R:','Moneda del Instrumento (UF -CLP)' union all
	SELECT 'Utilidad:','Diferencia producida por variaci�n de TIR compra con venta' union all
	SELECT 'Fecha Vcto:','Fecha de vencimiento del instrumento' union all
	SELECT 'Tipo de Operacion:','CPA = Compra' union all
	SELECT 'Tipo de Operacion:','VTA = Venta' union all
	SELECT 'Tipo de Operacion:','VCT = Vencimiento' union all
	SELECT 'Tipo de Operacion:','CCP = Corte de Cup�n' union all
	SELECT 'Tipo de Operacion:','SOR = Sorteo de Letras'


	SELECT	Titulo,
			Descripccion
	FROM	@TMP_Parametro_ExcelExplicativo

SET NOCOUNT OFF
END