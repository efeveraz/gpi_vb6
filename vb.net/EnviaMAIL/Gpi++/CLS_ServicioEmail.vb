﻿Imports Microsoft.Win32
Imports System.Data.SqlClient

Module CLS_ServicioEmail
    Dim lobjConexion As Cls_Conexion = New Cls_Conexion


    Public Sub Main(ByVal args() As String)
        Try
            Dim lstrPathArchivoIni As String = Application.StartupPath
            Dim lintIndice As Integer = lstrPathArchivoIni.IndexOf("\bin")
            If lintIndice > -1 Then lstrPathArchivoIni = lstrPathArchivoIni.Substring(0, lintIndice)
            gPathApp = lstrPathArchivoIni
            lstrPathArchivoIni = lstrPathArchivoIni & "\GPI_CONFIG.INI"
            gstrPathArchivoIni = lstrPathArchivoIni
            gLogoCliente = ObtieneValor_ArchivoIni(lstrPathArchivoIni, "EMPRESA", "Cod_Empresa", "")
            gtxtAmbienteSistema = ObtieneValor_ArchivoIni(lstrPathArchivoIni, "AMBIENTEBD", "Ambiente", "")
            'gtxtPath_Excel = ObtieneValor_ArchivoIni(lstrPathArchivoIni, "REPOSITORIOS", "Path_Excel", "C:\GPIMAS\Excel")
            'gtxtPath_Txt = ObtieneValor_ArchivoIni(lstrPathArchivoIni, "REPOSITORIOS", "Path_Txt", "C:\GPIMAS\Txt")
            gServer = lobjConexion.GetStringDB(lstrPathArchivoIni, "Data Source")
            gDB = lobjConexion.GetStringDB(lstrPathArchivoIni, "Initial Catalog")
            gModoAutenticacion = ObtieneValor_ArchivoIni(lstrPathArchivoIni, "MODO_AUTENTICACION", "Modo_autenticacion", "SQL SERVER")
            gstrCadenaConexion = lobjConexion.GetConnectionString(lstrPathArchivoIni)
            'gstrCadenaConexion_ITZ = lobjConexion_ITZ.GetConnectionString_ITZ(lstrPathArchivoIni)
            'gstrCadenaConexionGF = lobjConexionGF.GetConnectionString(lstrPathArchivoIni)
            'gstrCadenaConexionConc = lobjConexionConc.GetConnectionString(lstrPathArchivoIni)
            'gstrCadenaConexionCLTE = lobjConexionCLTE.GetConnectionString(lstrPathArchivoIni)
            'gstrCadenaConexionMoneda = lobjConexion_Moneda.GetConnectionString_Moneda(lstrPathArchivoIni)

            'tslbl_InfoServer.Text = " SVR:" & gServer
            'tslbl_InfoDB.Text = "DB:" & gtxtAmbienteSistema
            'tslbl_InfoVersion.Text = "V:" & gtxtVersionSistema
            'tslbl_InfoFecha.Text = Now

        Catch ex As Exception
            Console.WriteLine("No se puede conectar a BD" & vbCrLf & ex.Message, MsgBoxStyle.Critical, gtxtNombreSistema & " Indetificación")
            End
        End Try

        Try
            'vc Dim lstrAmbienteConexion = gFunExisteAmbienteConexion()
            'Dim lstrAmbienteConexion As String
            'lstrAmbienteConexion = "OK"
            Dim lstrAmbienteConexion = gFunExisteAmbienteConexion()
            If lstrAmbienteConexion <> "OK" Then
                Console.WriteLine(lstrAmbienteConexion, MsgBoxStyle.Critical, gtxtNombreSistema & " Identificación")
                End
            End If

            Dim bmp As Bitmap = Nothing

            'If gstrAmbienteBD = "PRODUCCION" Then
            '    'Me.BackgroundImage = My.Resources.Resources.Fondo_Login
            '    Me.BackColor = Color.SteelBlue
            'Else
            '    'Me.BackgroundImage = My.Resources.Resources.Fondo_Login_Desarrollo
            '    Me.BackColor = Color.White
            'End If

        Catch ex As Exception
            Console.WriteLine("Error : NO se  pudo establecer conexión con la Base de Datos...// ")
        End Try

        Try
            Dim usuario As String
            Dim Negocio As Integer
            'usuario = "adm"
            'Negocio = 1

            Dim parametros As String = ""
            'Dim value As System.Collections.ObjectModel.ReadOnlyCollection(Of String) = My.Application.CommandLineArgs
            parametros = args(0).ToString
            'parametros = "adm|1"

            'parametros = usuario & "|" & Negocio

            usuario = Mid(parametros, 1, InStr(1, parametros, "|", 0) - 1)
            Negocio = Mid(parametros, InStr(1, parametros, "|", 0) + 1, Len(parametros) - Len(usuario))

            ResetPassWord(usuario, Negocio)

            Console.WriteLine("Email enviado Correctamente")
        Catch ex As Exception
            Console.WriteLine("Error en el envio: " & ex.Message)
            End
        End Try

        End
    End Sub
    Public Sub ResetPassWord(ByVal Usuario As String, ByVal lintIdNegocio As Integer)
        Dim lintIdUsuario As Integer
        Dim lstrPassword, lstrPassEncriptada As String
        Dim strRetorno As String
        Dim strRetorno2 As String

        Dim DS_Usuario As New DataSet


        lstrPassword = ClsSeguridad.GenerarPasswordRandom()
        lstrPassEncriptada = ClsEncriptacion.Encriptar(lstrPassword)

        DS_Usuario = TraerUsuario(strRetorno, Usuario, lintIdNegocio)


        ResetearPassword(strRetorno2, DS_Usuario.Tables("USUARIO").Rows(0).Item("ID_USUARIO").ToString.Trim, lstrPassEncriptada, "PWD")

        Dim resultadoEmail = "OK"
        With DS_Usuario.Tables("USUARIO").Rows(0)
            resultadoEmail = ClsSeguridad.EnviarMailPassword(
                .Item("DSC_USUARIO").ToString.Trim,
                .Item("DSC_USUARIO").ToString.Trim,
                .Item("EMAIL_USUARIO").ToString.Trim,
                lstrPassword)
        End With

        If resultadoEmail = "OK" Then
            Console.WriteLine("Se envio el correo correctamente")
            'MsgBoxStyle.Information,
            '"Servicio de Envio de Email")
        Else
            Console.WriteLine("Ocurrio un error en el envio")
            'MsgBoxStyle.Information,
            '"Servicio de Envio de Email")
        End If
        'If ltxtResultadoTransaccion = "OK" Then
        '    MsgBox("La Contraseña ha sido cambiada correctamente" &
        '           IIf(Not resultadoEmail.Equals("OK"), ", pero el email no pudo ser enviado, debido al siguiente problema: " & vbCrLf & resultadoEmail, "."),
        '           MsgBoxStyle.Information,
        '           gtxtNombreSistema & Me.Text)


        'End If

    End Sub

    Public Function TraerUsuario(ByRef strRetorno As String,
                                 ByVal strNombreUsuario As String,
                                 ByVal IdNegocio As Integer) As DataSet

        Dim lDS_Usuario As New DataSet
        Dim lDS_Usuario_Aux As New DataSet

        Dim Sqlcommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim Lstr_Procedimiento As String = ""
        Dim Lstr_NombreTabla As String = ""

        'Lstr_Procedimiento = "SGC_SP_VALIDA_USUARIO"
        Lstr_Procedimiento = "PKG_USUARIOS$Buscar"
        Lstr_NombreTabla = "USUARIO"
        Connect.Abrir()

        Try
            Sqlcommand = New SqlCommand(Lstr_Procedimiento, Connect.Conexion)

            Sqlcommand.CommandType = CommandType.StoredProcedure
            Sqlcommand.CommandText = Lstr_Procedimiento
            Sqlcommand.Parameters.Clear()

            Sqlcommand.Parameters.Add("@pDsc_Usuario", SqlDbType.Char, 50).Value = strNombreUsuario
            Sqlcommand.Parameters.Add("@pId_Empresa", SqlDbType.Char, 50).Value = IdNegocio
            Sqlcommand.Parameters.Add("@pId_asesor", SqlDbType.Char, 50).Value = DBNull.Value

            Dim oParamSal As New SqlClient.SqlParameter("@pId_Usuario", SqlDbType.Char, 60)
            oParamSal.Direction = ParameterDirection.Output
            Sqlcommand.Parameters.Add(oParamSal)

            SQLDataAdapter.SelectCommand = Sqlcommand
            SQLDataAdapter.Fill(lDS_Usuario, Lstr_NombreTabla)

            lDS_Usuario_Aux = lDS_Usuario
            strRetorno = "OK"
            Return lDS_Usuario_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function ResetearPassword(ByRef strRetorno2 As String,
                                     ByVal lngIdUsuario As Long,
                                     ByVal strClaveUsuario As String,
                                     ByVal strEstadoUsuario As String)

        Dim lDS_Usuario As New DataSet
        Dim lDS_Usuario_Aux As New DataSet

        Dim Sqlcommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim Lstr_Procedimiento As String = ""
        Dim Lstr_NombreTabla As String = ""

        'Lstr_Procedimiento = "SGC_SP_MANTENCION_USUARIO"
        Lstr_Procedimiento = "PKG_USUARIOS$ActualizaClave"
        Lstr_NombreTabla = "USUARIO"
        Connect.Abrir()

        Try
            Sqlcommand = New SqlCommand(Lstr_Procedimiento, Connect.Conexion)

            Sqlcommand.CommandType = CommandType.StoredProcedure
            Sqlcommand.CommandText = Lstr_Procedimiento
            Sqlcommand.Parameters.Clear()

            Sqlcommand.Parameters.Add("@pIdUsuario", SqlDbType.Int).Value = lngIdUsuario
            Sqlcommand.Parameters.Add("@pClave", SqlDbType.Char, 200).Value = strClaveUsuario
            Sqlcommand.Parameters.Add("@pEstado", SqlDbType.Char, 3).Value = strEstadoUsuario


            Dim oParamSal As New SqlClient.SqlParameter("@pResultado", SqlDbType.Char, 60)
            oParamSal.Direction = ParameterDirection.Output
            Sqlcommand.Parameters.Add(oParamSal)

            SQLDataAdapter.SelectCommand = Sqlcommand
            SQLDataAdapter.Fill(lDS_Usuario, Lstr_NombreTabla)

            lDS_Usuario_Aux = lDS_Usuario

            strRetorno2 = "OK"
            Return Nothing
            'Return lDS_Usuario_Aux

        Catch ex As Exception
            strRetorno2 = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function
End Module
