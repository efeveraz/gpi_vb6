﻿Public Module gEnumeracionesSeguridad
    Public Enum TipoRegistroLog As Integer
        LogSistema = 1              '   SISTEMA
        LogAplicacion = 2           '   APLICACION
        LogTransacciones = 3        '   TRANSACCIONES
        LogEntradaSalida = 4        '   ENTRADA Y SALIDAS APP
        LogCargaYProcesos = 5       '   CARGAS Y PROCESO
        LogBaseDeDatos = 6          '   BASE DE DATOS
        LogProcesosAutomaticos = 7  '   PROCESOS AUTOMATICOS
        LogProcesosUsuario = 8      '   PROCESOS DE USUARIO
    End Enum
End Module
