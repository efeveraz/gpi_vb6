﻿
Public Class ClsPerfilUsuario
    Public Function RetornaPerfiles(ByRef strRetorno As String, _
                                    ByVal intIdPerfilUsuario As Integer, _
                                    ByVal strNombrePerfil As String, _
                                    ByVal strNombreCortoPerfil As String, _
                                    ByVal strCodigoExternoPerfil As String, _
                                    ByVal strEstadoPerfil As String) As DataSet

        Dim lDS_Perfiles As New DataSet
        Dim lDS_Perfiles_Aux As New DataSet

        Dim Sqlcommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim Lstr_Procedimiento As String = ""
        Dim Lstr_NombreTabla As String = ""


        'Lstr_Procedimiento = "SGC_SP_MANTENCION_C_PERFIL"
        Lstr_Procedimiento = "Sis_PerfilUsuario_Mantencion"
        Lstr_NombreTabla = "PERFIL_USUARIO"
        Connect.Abrir()

        Try
            Sqlcommand = New SqlClient.SqlCommand(Lstr_Procedimiento, Connect.Conexion)

            Sqlcommand.CommandType = CommandType.StoredProcedure
            Sqlcommand.CommandText = Lstr_Procedimiento
            Sqlcommand.Parameters.Clear()

            Sqlcommand.Parameters.Add("@pCodigoOperacion", SqlDbType.Char, 10).Value = "BUSCAR"
            Sqlcommand.Parameters.Add("@pIdPerfilUsuario", SqlDbType.Int).Value = IIf(intIdPerfilUsuario = 0, DBNull.Value, intIdPerfilUsuario)
            Sqlcommand.Parameters.Add("@pNombrePerfil", SqlDbType.Char, 50).Value = IIf(strNombrePerfil = "", DBNull.Value, strNombrePerfil)
            Sqlcommand.Parameters.Add("@pNombreCortoPerfil", SqlDbType.Char, 20).Value = IIf(strNombreCortoPerfil = "", DBNull.Value, strNombreCortoPerfil)
            Sqlcommand.Parameters.Add("@pCodigoExternoPerfil", SqlDbType.Char, 50).Value = IIf(strCodigoExternoPerfil = "", DBNull.Value, strCodigoExternoPerfil)
            Sqlcommand.Parameters.Add("@pEstadoPerfil", SqlDbType.Char, 3).Value = IIf(strEstadoPerfil = "", DBNull.Value, strEstadoPerfil)

            Dim oParamSal As New SqlClient.SqlParameter("@pMsjRetorno", SqlDbType.Char, 60)
            oParamSal.Direction = ParameterDirection.Output
            Sqlcommand.Parameters.Add(oParamSal)

            SQLDataAdapter.SelectCommand = Sqlcommand
            SQLDataAdapter.Fill(lDS_Perfiles, Lstr_NombreTabla)

            lDS_Perfiles_Aux = lDS_Perfiles

            strRetorno = "OK"

            Return lDS_Perfiles_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function Insertar(ByRef IntIDPerfilUsuario As Integer, _
                             ByVal strNombrePerfil As String, _
                             ByVal strNombreCortoPerfil As String, _
                             ByVal strCodigoExternoPerfil As String, _
                             ByVal strEstadoPerfil As String) As String

        Dim strRetorno As String
        Dim Sqlcommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim lstrProcedimiento As String = ""



        'Lstr_Procedimiento = "SGC_SP_MANTENCION_C_PERFIL"
        lstrProcedimiento = "Sis_PerfilUsuario_Mantencion"
        Connect.Abrir()

        Try
            Sqlcommand = New SqlClient.SqlCommand(lstrProcedimiento, Connect.Conexion)

            Sqlcommand.CommandType = CommandType.StoredProcedure
            Sqlcommand.CommandText = lstrProcedimiento
            Sqlcommand.Parameters.Clear()

            Sqlcommand.Parameters.Add("@pCodigoOperacion", SqlDbType.Char, 10).Value = "INSERTAR"

            'IdPerfilUsuario...(Identity)
            Dim ParametroIdPerfilUsuario As New SqlClient.SqlParameter("@pIdPerfilUsuario", SqlDbType.Int)
            ParametroIdPerfilUsuario.Direction = Data.ParameterDirection.InputOutput

            Sqlcommand.Parameters.Add(ParametroIdPerfilUsuario)

            Sqlcommand.Parameters.Add("@pNombrePerfil", SqlDbType.Char, 50).Value = IIf(strNombrePerfil = "", DBNull.Value, strNombrePerfil)
            Sqlcommand.Parameters.Add("@pNombreCortoPerfil", SqlDbType.Char, 20).Value = IIf(strNombreCortoPerfil = "", DBNull.Value, strNombreCortoPerfil)
            Sqlcommand.Parameters.Add("@pCodigoExternoPerfil", SqlDbType.Char, 50).Value = IIf(strCodigoExternoPerfil = "", DBNull.Value, strCodigoExternoPerfil)
            Sqlcommand.Parameters.Add("@pEstadoPerfil", SqlDbType.Char, 3).Value = IIf(strEstadoPerfil = "", DBNull.Value, strEstadoPerfil)

            Dim oParamSal As New SqlClient.SqlParameter("@pMsjRetorno", SqlDbType.Char, 60)
            oParamSal.Direction = ParameterDirection.Output
            Sqlcommand.Parameters.Add(oParamSal)


            Sqlcommand.ExecuteNonQuery()

            strRetorno = Sqlcommand.Parameters("@pMsjRetorno").Value.ToString.Trim

            IntIDPerfilUsuario = Sqlcommand.Parameters("@pIdPerfilUsuario").Value.ToString.Trim

            Return strRetorno

        Catch ex As Exception
            strRetorno = ex.Message
            Return strRetorno
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function Modificar(ByVal IntIDPerfilUsuario As Integer, _
                              ByVal strNombrePerfil As String, _
                              ByVal strNombreCortoPerfil As String, _
                              ByVal strCodigoExternoPerfil As String, _
                              ByVal strEstadoPerfil As String) As String

        Dim Sqlcommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim lstrProcedimiento As String = ""

        Dim strRetorno As String
        lstrProcedimiento = "Sis_PerfilUsuario_Mantencion"
        Connect.Abrir()

        Try
            Sqlcommand = New SqlClient.SqlCommand(lstrProcedimiento, Connect.Conexion)

            Sqlcommand.CommandType = CommandType.StoredProcedure
            Sqlcommand.CommandText = lstrProcedimiento
            Sqlcommand.Parameters.Clear()

            Sqlcommand.Parameters.Add("@pCodigoOperacion", SqlDbType.Char, 10).Value = "MODIFICAR"
            Sqlcommand.Parameters.Add("@pIdPerfilUsuario", SqlDbType.Int).Value = IntIDPerfilUsuario
            Sqlcommand.Parameters.Add("@pNombrePerfil", SqlDbType.Char, 50).Value = IIf(strNombrePerfil = "", DBNull.Value, strNombrePerfil)
            Sqlcommand.Parameters.Add("@pNombreCortoPerfil", SqlDbType.Char, 20).Value = IIf(strNombreCortoPerfil = "", DBNull.Value, strNombreCortoPerfil)
            Sqlcommand.Parameters.Add("@pCodigoExternoPerfil", SqlDbType.Char, 50).Value = IIf(strCodigoExternoPerfil = "", DBNull.Value, strCodigoExternoPerfil)
            Sqlcommand.Parameters.Add("@pEstadoPerfil", SqlDbType.Char, 3).Value = IIf(strEstadoPerfil = "", DBNull.Value, strEstadoPerfil)

            Dim oParamSal As New SqlClient.SqlParameter("@pMsjRetorno", SqlDbType.Char, 60)
            oParamSal.Direction = ParameterDirection.Output
            Sqlcommand.Parameters.Add(oParamSal)

            Sqlcommand.ExecuteNonQuery()

            strRetorno = Sqlcommand.Parameters("@pMsjRetorno").Value.ToString.Trim

            Return strRetorno

        Catch ex As Exception
            strRetorno = ex.Message
            Return strRetorno
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function Eliminar(ByRef IntIDPerfilUsuario As Integer) As String

        Dim Sqlcommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim lstrProcedimiento As String = ""
        Dim lstrRetorno As String

        lstrProcedimiento = "Sis_PerfilUsuario_Mantencion"
        Connect.Abrir()

        Try
            Sqlcommand = New SqlClient.SqlCommand(lstrProcedimiento, Connect.Conexion)

            Sqlcommand.CommandType = CommandType.StoredProcedure
            Sqlcommand.CommandText = lstrProcedimiento
            Sqlcommand.Parameters.Clear()

            Sqlcommand.Parameters.Add("@pCodigoOperacion", SqlDbType.Char, 10).Value = "ELIMINAR"
            Sqlcommand.Parameters.Add("@pIdPerfilUsuario", SqlDbType.Int).Value = IntIDPerfilUsuario

            Dim oParamSal As New SqlClient.SqlParameter("@pMsjRetorno", SqlDbType.Char, 60)
            oParamSal.Direction = ParameterDirection.Output
            Sqlcommand.Parameters.Add(oParamSal)

            Sqlcommand.ExecuteNonQuery()

            lstrRetorno = Sqlcommand.Parameters("@pMsjRetorno").Value.ToString.Trim

            Return lstrRetorno

        Catch ex As Exception
            lstrRetorno = ex.Message
            Return lstrRetorno
        Finally
            Connect.Cerrar()
        End Try

    End Function

End Class
