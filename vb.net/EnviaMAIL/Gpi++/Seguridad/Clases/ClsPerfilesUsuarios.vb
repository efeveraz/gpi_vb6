﻿
Public Class ClsPerfilesUsuarios


    Public Function BuscarPerfilesPorNegocio(ByRef strRetorno As String, _
                                             ByVal intIdNegocio As Integer) As DataSet

        Dim lDS_PerfilesUsuarios As New DataSet
        Dim lDS_PerfilesUsuarios_Aux As New DataSet

        Dim SqlCommand As New SqlClient.SqlCommand
        Dim SqlDataAdapter As New SqlClient.SqlDataAdapter
        Dim SqlConnect As Cls_Conexion = New Cls_Conexion
        Dim lstrProcedimiento As String = ""
        Dim lstrNombreTabla As String = ""

        lstrProcedimiento = "Sis_PerfilesUsuarios_Consultar"
        lstrNombreTabla = "PERFILES_USUARIOS"
        SqlConnect.Abrir()

        Try

            SqlCommand = New SqlClient.SqlCommand(lstrProcedimiento, SqlConnect.Conexion)

            SqlCommand.CommandType = CommandType.StoredProcedure
            SqlCommand.CommandText = lstrProcedimiento
            SqlCommand.Parameters.Clear()

            SqlCommand.Parameters.Add("@pCodigoOperacion", SqlDbType.Char, 15).Value = "PERFILNEGOCIO"
            SqlCommand.Parameters.Add("@pIdNegocio", SqlDbType.Int).Value = intIdNegocio


            Dim oParamSal As New SqlClient.SqlParameter("@pMsjRetorno", SqlDbType.Char, 60)
            oParamSal.Direction = ParameterDirection.Output
            SqlCommand.Parameters.Add(oParamSal)


            SqlDataAdapter.SelectCommand = SqlCommand
            SqlDataAdapter.Fill(lDS_PerfilesUsuarios, lstrNombreTabla)

            lDS_PerfilesUsuarios_Aux = lDS_PerfilesUsuarios

            strRetorno = "OK"

            Return lDS_PerfilesUsuarios_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            SqlConnect.Cerrar()
        End Try

    End Function

    Public Function BuscarDisponibles(ByRef strRetorno As String, _
                                      ByVal intIdUsuario As Integer) As DataSet

        Dim lDS_PerfilesDisponibles As New DataSet
        Dim lDS_PerfilesDisponibles_Aux As New DataSet

        Dim SqlCommand As New SqlClient.SqlCommand
        Dim SqlDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim Lstr_Procedimiento As String = ""
        Dim Lstr_NombreTabla As String = ""

        'Lstr_Procedimiento = "SGC_SP_CONSULTA_PERFIL_USUARIO"
        Lstr_Procedimiento = "Sis_PerfilUsuario_Consultar"
        Lstr_NombreTabla = "PERFILES_USUARIOS"
        Connect.Abrir()

        Try

            SqlCommand = New SqlClient.SqlCommand(Lstr_Procedimiento, Connect.Conexion)

            SqlCommand.CommandType = CommandType.StoredProcedure
            SqlCommand.CommandText = Lstr_Procedimiento
            SqlCommand.Parameters.Clear()

            SqlCommand.Parameters.Add("@pCodigoOperacion", SqlDbType.Char, 15).Value = "DISPONIBLES"
            SqlCommand.Parameters.Add("@pIdUsuario", SqlDbType.Int).Value = intIdUsuario


            Dim oParamSal As New SqlClient.SqlParameter("@pMsjRetorno", SqlDbType.Char, 60)
            oParamSal.Direction = ParameterDirection.Output
            SqlCommand.Parameters.Add(oParamSal)


            SqlDataAdapter.SelectCommand = SqlCommand
            SqlDataAdapter.Fill(lDS_PerfilesDisponibles, Lstr_NombreTabla)

            lDS_PerfilesDisponibles_Aux = lDS_PerfilesDisponibles

            strRetorno = "OK"

            Return lDS_PerfilesDisponibles_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function BuscarAsignados(ByRef strRetorno As String, ByVal intIdUsuario As Integer) As DataSet

        Dim lDS_PerfilesUsuario As New DataSet
        Dim lDS_PerfilesUsuario_Aux As New DataSet

        Dim SqlCommand As New SqlClient.SqlCommand
        Dim SqlDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim Lstr_Procedimiento As String = ""
        Dim Lstr_NombreTabla As String = ""

        'Lstr_Procedimiento = "SGC_SP_CONSULTA_PERFIL_USUARIO"
        Lstr_Procedimiento = "Sis_PerfilUsuario_Consultar"
        Lstr_NombreTabla = "PERFILES_USUARIOS"
        Connect.Abrir()

        Try
            SqlCommand = New SqlClient.SqlCommand(Lstr_Procedimiento, Connect.Conexion)

            SqlCommand.CommandType = CommandType.StoredProcedure
            SqlCommand.CommandText = Lstr_Procedimiento
            SqlCommand.Parameters.Clear()

            SqlCommand.Parameters.Add("@pCodigoOperacion", SqlDbType.Char, 15).Value = "ASIGNADOS"
            SqlCommand.Parameters.Add("@pIdUsuario", SqlDbType.Int).Value = intIdUsuario

            Dim oParamSal As New SqlClient.SqlParameter("@pMsjRetorno", SqlDbType.Char, 60)
            oParamSal.Direction = ParameterDirection.Output
            SqlCommand.Parameters.Add(oParamSal)

            SqlDataAdapter.SelectCommand = SqlCommand
            SqlDataAdapter.Fill(lDS_PerfilesUsuario, Lstr_NombreTabla)

            lDS_PerfilesUsuario_Aux = lDS_PerfilesUsuario

            strRetorno = "OK"

            Return lDS_PerfilesUsuario_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function TraeUsuariosDelPerfil(ByRef strRetorno As String, _
                                          ByVal intIdPerfilUsuario As Integer, _
                                          ByVal intIdNegocio As Integer) As DataSet

        Dim lDS_UsuariosPerfil As New DataSet
        Dim lDS_UsuariosPerfil_Aux As New DataSet

        Dim SqlCommand As New SqlClient.SqlCommand
        Dim SqlDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim lstrProcedimiento As String = ""
        Dim lstrNombreTabla As String = ""

        strRetorno = "OK"
        lstrProcedimiento = "Sis_PerfilUsuario_Consultar"
        lstrNombreTabla = "USUARIOSPERFIL"
        Connect.Abrir()

        Try
            SqlCommand = New SqlClient.SqlCommand(lstrProcedimiento, Connect.Conexion)

            SqlCommand.CommandType = CommandType.StoredProcedure
            SqlCommand.CommandText = lstrProcedimiento
            SqlCommand.Parameters.Clear()

            SqlCommand.Parameters.Add("@pCodigoOperacion", SqlDbType.Char, 15).Value = "USUARIOSPERFIL"
            SqlCommand.Parameters.Add("@pIdPerfilUsuario", SqlDbType.Int).Value = intIdPerfilUsuario
            SqlCommand.Parameters.Add("@pIdNegocio", SqlDbType.Int).Value = IIf(intIdNegocio = 0, DBNull.Value, intIdNegocio)

            Dim oParamSal As New SqlClient.SqlParameter("@pMsjRetorno", SqlDbType.Char, 60)
            oParamSal.Direction = ParameterDirection.Output
            SqlCommand.Parameters.Add(oParamSal)

            SqlDataAdapter.SelectCommand = SqlCommand
            SqlDataAdapter.Fill(lDS_UsuariosPerfil, lstrNombreTabla)

            lDS_UsuariosPerfil_Aux = lDS_UsuariosPerfil
            strRetorno = SqlCommand.Parameters("@pMsjRetorno").Value.ToString.Trim


            Return lDS_UsuariosPerfil_Aux


        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function TraeUsuariosDisponiblesPerfil(ByRef strRetorno As String, _
                                          ByVal intIdPerfilUsuario As Integer, _
                                          ByVal intIdNegocio As Integer) As DataSet

        Dim lDS_UsuariosPerfil As New DataSet
        Dim lDS_UsuariosPerfi_Aux As New DataSet

        Dim Sqlcommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim lstrProcedimiento As String = ""
        Dim lstrNombreTabla As String = ""

        lstrProcedimiento = "Sis_PerfilUsuario_Consultar"
        lstrNombreTabla = "USUARIOSDISP"
        Connect.Abrir()

        Try

            Sqlcommand = New SqlClient.SqlCommand(lstrProcedimiento, Connect.Conexion)

            Sqlcommand.CommandType = CommandType.StoredProcedure
            Sqlcommand.CommandText = lstrProcedimiento
            Sqlcommand.Parameters.Clear()

            Sqlcommand.Parameters.Add("@pCodigoOperacion", SqlDbType.Char, 15).Value = "USUARIOSDISPONI"
            Sqlcommand.Parameters.Add("@pIdNegocio", SqlDbType.Int).Value = IIf(intIdNegocio = 0, DBNull.Value, intIdNegocio)
            Sqlcommand.Parameters.Add("@pIdPerfilUsuario", SqlDbType.Int).Value = IIf(intIdPerfilUsuario = 0, DBNull.Value, intIdPerfilUsuario)


            Dim oParamSal As New SqlClient.SqlParameter("@pMsjRetorno", SqlDbType.Char, 60)
            oParamSal.Direction = ParameterDirection.Output
            Sqlcommand.Parameters.Add(oParamSal)


            SQLDataAdapter.SelectCommand = Sqlcommand
            SQLDataAdapter.Fill(lDS_UsuariosPerfil, lstrNombreTabla)

            lDS_UsuariosPerfi_Aux = lDS_UsuariosPerfil

            strRetorno = Sqlcommand.Parameters("@pMsjRetorno").Value.ToString.Trim

            Return lDS_UsuariosPerfi_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function AgregarRelacionPerfilUsuario(ByVal intIdUsuario As Integer, _
                                                  ByVal DS_PerfilesUsuarios As DataSet) As String

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction
        
        Dim lstrProcedimiento As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim lstrRetorno As String = "OK"
        Dim drFila As DataRow

        lstrProcedimiento = "Sis_PerfilesUsuarios_Mantencion"
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SqlCommand = New SqlClient.SqlCommand(lstrProcedimiento, MiTransaccionSQL.Connection, MiTransaccionSQL)
            SqlCommand.CommandType = CommandType.StoredProcedure
            SqlCommand.CommandText = lstrProcedimiento
            SqlCommand.Parameters.Clear()

            For Each drFila In DS_PerfilesUsuarios.Tables(0).Rows
                SQLCommand.Parameters.Add("@pCodigoOperacion", SqlDbType.Char, 15).Value = "AGREGAR"
                SQLCommand.Parameters.Add("@pIdUsuario", SqlDbType.Int).Value = intIdUsuario
                SQLCommand.Parameters.Add("@pIdPerfilUsuario", SqlDbType.Int).Value = drFila("ID_PERFIL_USUARIO")
                SQLCommand.Parameters.Add("@pIdNegocio", SqlDbType.Int).Value = drFila("ID_NEGOCIO")

                Dim oParamSal As New SqlClient.SqlParameter("@pMsjRetorno", SqlDbType.Char, 60)
                oParamSal.Direction = ParameterDirection.Output
                SQLCommand.Parameters.Add(oParamSal)

                SQLCommand.ExecuteNonQuery()

                lstrRetorno = SQLCommand.Parameters("@pMsjRetorno").Value.ToString.Trim

                If Trim(lstrRetorno) <> "OK" Then
                    GoTo Salir
                End If
                SQLCommand.Parameters.Clear()
            Next
            If Trim(lstrRetorno) = "OK" Then
                MiTransaccionSQL.Commit()
                Return ("OK")
            End If


Salir:
            If Trim(lstrRetorno) = "OK" Then
                MiTransaccionSQL.Commit()
                Return ("OK")
            Else
                MiTransaccionSQL.Rollback()
                Return lstrRetorno
            End If

        Catch ex As Exception
            MiTransaccionSQL.Rollback()
            lstrRetorno = ex.Message
            Return lstrRetorno
        Finally
            SQLConnect.Cerrar()
        End Try

    End Function
    Public Function EliminarRelacionPerfilUsuario(ByVal intIdUsuario As Integer, _
                                                  ByVal DS_PerfilesUsuarios As DataSet) As String

        Dim MiTransaccionSQL As SqlClient.SqlTransaction
        Dim SqlCommand As New SqlClient.SqlCommand
        Dim SqlDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim lstrProcedimiento As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim lstrRetorno As String = ""
        Dim drFila As DataRow
        lstrProcedimiento = "Sis_PerfilesUsuarios_Mantencion"

        Connect.Abrir()
        MiTransaccionSQL = Connect.Transaccion

        Try
            SqlCommand = New SqlClient.SqlCommand(lstrProcedimiento, MiTransaccionSQL.Connection, MiTransaccionSQL)
            SqlCommand.CommandType = CommandType.StoredProcedure
            SqlCommand.CommandText = lstrProcedimiento

            For Each drFila In DS_PerfilesUsuarios.Tables(0).Rows
                SqlCommand.Parameters.Clear()

                SqlCommand.Parameters.Add("@pCodigoOperacion", SqlDbType.Char, 15).Value = "ELIMINAR"
                SqlCommand.Parameters.Add("@pIdUsuario", SqlDbType.Int).Value = intIdUsuario
                SqlCommand.Parameters.Add("@pIdPerfilUsuario", SqlDbType.Int).Value = drFila("ID_PERFIL_USUARIO")
                SqlCommand.Parameters.Add("@pIdNegocio", SqlDbType.Int).Value = drFila("ID_NEGOCIO")

                Dim oParamSal As New SqlClient.SqlParameter("@pMsjRetorno", SqlDbType.Char, 60)
                oParamSal.Direction = ParameterDirection.Output
                SqlCommand.Parameters.Add(oParamSal)

                SqlCommand.ExecuteNonQuery()

                lstrRetorno = SqlCommand.Parameters("@pMsjRetorno").Value.ToString.Trim

                If Trim(lstrRetorno) <> "OK" Then
                    GoTo Salir
                End If
            Next
            If Trim(lstrRetorno) = "OK" Then
                MiTransaccionSQL.Commit()
                Return ("OK")
            End If
Salir:
            If Trim(lstrRetorno) = "OK" Then
                MiTransaccionSQL.Commit()
                Return ("OK")
            Else
                MiTransaccionSQL.Rollback()
                Return lstrRetorno
            End If

        Catch ex As Exception
            MiTransaccionSQL.Rollback()
            lstrRetorno = ex.Message
            Return lstrRetorno
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function AgregarRelacionPerfilesUsuarios_PorPerfil(ByVal intIdPerfilUsuario As Integer, _
                                                              ByVal DS_PerfilesUsuarios As DataSet) As String

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        Dim lstrProcedimiento As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim lstrRetorno As String = "OK"
        Dim DR_Fila As DataRow

        lstrProcedimiento = "Sis_PerfilesUsuarios_Mantencion"
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, MiTransaccionSQL.Connection, MiTransaccionSQL)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = lstrProcedimiento


            For Each DR_Fila In DS_PerfilesUsuarios.Tables(0).Rows
                SQLCommand.Parameters.Clear()

                SQLCommand.Parameters.Add("@pCodigoOperacion", SqlDbType.Char, 15).Value = "AGREGAR"
                SQLCommand.Parameters.Add("@pIdPerfilUsuario", SqlDbType.Int).Value = intIdPerfilUsuario
                SQLCommand.Parameters.Add("@pIdUsuario", SqlDbType.Int).Value = DR_Fila("ID_USUARIO")
                SQLCommand.Parameters.Add("@pIdNegocio", SqlDbType.Int).Value = DR_Fila("ID_NEGOCIO")

                Dim oParamSal As New SqlClient.SqlParameter("@pMsjRetorno", SqlDbType.Char, 60)
                oParamSal.Direction = ParameterDirection.Output
                SQLCommand.Parameters.Add(oParamSal)

                SQLCommand.ExecuteNonQuery()

                lstrRetorno = SQLCommand.Parameters("@pMsjRetorno").Value.ToString.Trim

                If Trim(lstrRetorno) <> "OK" Then
                    GoTo Salir
                End If

            Next
            If Trim(lstrRetorno) = "OK" Then
                MiTransaccionSQL.Commit()
                Return ("OK")
            End If


Salir:
            If Trim(lstrRetorno) = "OK" Then
                MiTransaccionSQL.Commit()
                Return ("OK")
            Else
                MiTransaccionSQL.Rollback()
                Return lstrRetorno
            End If

        Catch ex As Exception
            MiTransaccionSQL.Rollback()
            lstrRetorno = ex.Message
            Return lstrRetorno
        Finally
            SQLConnect.Cerrar()
        End Try

    End Function
    Public Function CambiarRelacionPerfilesUsuarios_PorPerfil(ByVal intIdPerfilUsuario As Integer, _
                                                              ByVal DS_PerfilesUsuarios_Proceso As DataSet) As String

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        Dim lstrProcedimiento As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim lstrRetorno As String = "OK"
        Dim DR_Fila As DataRow

        lstrProcedimiento = "Sis_PerfilesUsuarios_Mantencion"
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, MiTransaccionSQL.Connection, MiTransaccionSQL)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = lstrProcedimiento


            For Each DR_Fila In DS_PerfilesUsuarios_Proceso.Tables(0).Select("ACCION = 'E'")
                SQLCommand.Parameters.Clear()

                SQLCommand.Parameters.Add("@pCodigoOperacion", SqlDbType.Char, 15).Value = "ELIMINAR"
                SQLCommand.Parameters.Add("@pIdPerfilUsuario", SqlDbType.Int).Value = intIdPerfilUsuario
                SQLCommand.Parameters.Add("@pIdUsuario", SqlDbType.Int).Value = DR_Fila("ID_USUARIO")
                SQLCommand.Parameters.Add("@pIdNegocio", SqlDbType.Int).Value = DR_Fila("ID_NEGOCIO")

                Dim oParamSal As New SqlClient.SqlParameter("@pMsjRetorno", SqlDbType.Char, 60)
                oParamSal.Direction = ParameterDirection.Output
                SQLCommand.Parameters.Add(oParamSal)

                SQLCommand.ExecuteNonQuery()

                lstrRetorno = SQLCommand.Parameters("@pMsjRetorno").Value.ToString.Trim

                If Trim(lstrRetorno) <> "OK" Then
                    GoTo Salir
                End If
            Next

            For Each DR_Fila In DS_PerfilesUsuarios_Proceso.Tables(0).Select("ACCION = 'A'")
                SQLCommand.Parameters.Clear()

                SQLCommand.Parameters.Add("@pCodigoOperacion", SqlDbType.Char, 15).Value = "AGREGAR"
                SQLCommand.Parameters.Add("@pIdPerfilUsuario", SqlDbType.Int).Value = intIdPerfilUsuario
                SQLCommand.Parameters.Add("@pIdUsuario", SqlDbType.Int).Value = DR_Fila("ID_USUARIO")
                SQLCommand.Parameters.Add("@pIdNegocio", SqlDbType.Int).Value = DR_Fila("ID_NEGOCIO")

                Dim oParamSal As New SqlClient.SqlParameter("@pMsjRetorno", SqlDbType.Char, 60)
                oParamSal.Direction = ParameterDirection.Output
                SQLCommand.Parameters.Add(oParamSal)

                SQLCommand.ExecuteNonQuery()

                lstrRetorno = SQLCommand.Parameters("@pMsjRetorno").Value.ToString.Trim

                If Trim(lstrRetorno) <> "OK" Then
                    GoTo Salir
                End If

            Next
            If Trim(lstrRetorno) = "OK" Then
                MiTransaccionSQL.Commit()
                Return ("OK")
            End If


Salir:
            If Trim(lstrRetorno) = "OK" Then
                MiTransaccionSQL.Commit()
                Return ("OK")
            Else
                MiTransaccionSQL.Rollback()
                Return lstrRetorno
            End If

        Catch ex As Exception
            MiTransaccionSQL.Rollback()
            lstrRetorno = ex.Message
            Return lstrRetorno
        Finally
            SQLConnect.Cerrar()
        End Try

    End Function

    Public Function EliminarRelacionPerfilesUsuarios_PorPerfil(ByVal intIdPerfilUsuario As Integer) As String

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        Dim lstrProcedimiento As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim lstrRetorno As String = "OK"

        lstrProcedimiento = "Sis_PerfilesUsuarios_Mantencion"
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, MiTransaccionSQL.Connection, MiTransaccionSQL)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = lstrProcedimiento

            SQLCommand.Parameters.Add("@pCodigoOperacion", SqlDbType.Char, 15).Value = "ELIMINARPERFIL"
            SQLCommand.Parameters.Add("@pIdPerfilUsuario", SqlDbType.Int).Value = intIdPerfilUsuario

            Dim oParamSal As New SqlClient.SqlParameter("@pMsjRetorno", SqlDbType.Char, 60)
            oParamSal.Direction = ParameterDirection.Output
            SQLCommand.Parameters.Add(oParamSal)

            SQLCommand.ExecuteNonQuery()

            lstrRetorno = SQLCommand.Parameters("@pMsjRetorno").Value.ToString.Trim

            If Trim(lstrRetorno) = "OK" Then
                MiTransaccionSQL.Commit()
                Return ("OK")
            Else
                MiTransaccionSQL.Rollback()
                Return lstrRetorno
            End If

        Catch ex As Exception
            MiTransaccionSQL.Rollback()
            lstrRetorno = ex.Message
            Return lstrRetorno
        Finally
            SQLConnect.Cerrar()
        End Try

    End Function
    Public Function EliminarRelacionPerfilesUsuarios_PorUsuario(ByVal intIdUsuario As Integer) As String

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        Dim lstrProcedimiento As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim lstrRetorno As String = "OK"

        lstrProcedimiento = "Sis_PerfilesUsuarios_Mantencion"
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, MiTransaccionSQL.Connection, MiTransaccionSQL)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = lstrProcedimiento

            SQLCommand.Parameters.Add("@pCodigoOperacion", SqlDbType.Char, 15).Value = "ELIMINARUSUARIO"
            SQLCommand.Parameters.Add("@pIdUsuario", SqlDbType.Int).Value = intIdUsuario
            SQLCommand.ExecuteNonQuery()

            lstrRetorno = SQLCommand.Parameters("@pMsjRetorno").Value.ToString.Trim

            If Trim(lstrRetorno) = "OK" Then
                MiTransaccionSQL.Commit()
                Return ("OK")
            Else
                MiTransaccionSQL.Rollback()
                Return lstrRetorno
            End If

        Catch ex As Exception
            MiTransaccionSQL.Rollback()
            lstrRetorno = ex.Message
            Return lstrRetorno
        Finally
            SQLConnect.Cerrar()
        End Try

    End Function
End Class
