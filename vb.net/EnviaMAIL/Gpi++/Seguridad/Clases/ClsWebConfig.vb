﻿Imports Microsoft.VisualBasic
Imports System.Configuration

Public Class ClsWebConfig
    Public Function GetKeyValue(ByVal sKey As String, ByRef sValue As String) As Boolean
        Dim config As Configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None)
        If (0 < config.AppSettings.Settings.Count) Then
            Dim customSetting As System.Configuration.KeyValueConfigurationElement
            customSetting = config.AppSettings.Settings(sKey)

            If Not (Nothing = customSetting.Value) Then
                sValue = customSetting.Value
                Return True
            Else
                Return False
            End If
        Else
            Return False
        End If
    End Function
End Class
