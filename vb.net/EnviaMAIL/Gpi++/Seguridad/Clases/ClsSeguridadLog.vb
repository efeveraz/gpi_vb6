﻿Imports System.Data.SqlClient

Public Class ClsSeguridadLog
    Public Function Registra_Log_Basico(ByVal Id_Tipo_Registro As Integer, _
                                        ByVal Id_Concepto_Registro As Integer, _
                                        ByVal Id_Usuario As Integer, _
                                        ByVal Descripcion_registro As String) As String

        Dim SQLcommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction
        Dim Lstr_Procedimiento As String = "Sis_RegistroLogs_Basico"
        Dim lstrRetorna As String = ""

        SQLConnect.Abrir()

        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLcommand = New SqlCommand(Lstr_Procedimiento, MiTransaccionSQL.Connection, MiTransaccionSQL) With {
                .CommandType = CommandType.StoredProcedure,
                .CommandText = Lstr_Procedimiento
            }
            SQLcommand.Parameters.Clear()

            SQLcommand.Parameters.Add("@pCodigoOperacion", SqlDbType.Char, 15).Value = "INSERTAR"
            SQLcommand.Parameters.Add("@pIdTipoRegistro", SqlDbType.Int).Value = Id_Tipo_Registro
            SQLcommand.Parameters.Add("@pIdConceptoRegistro", SqlDbType.Int).Value = Id_Concepto_Registro
            SQLcommand.Parameters.Add("@pIdUsuario", SqlDbType.Int).Value = Id_Usuario
            SQLcommand.Parameters.Add("@pDescripcionRegistro", SqlDbType.Char, 500).Value = Descripcion_registro

            Dim oParamSal As New SqlParameter("@pMsjRetorno", SqlDbType.Char, 60) With {
                .Direction = ParameterDirection.Output
            }
            SQLcommand.Parameters.Add(oParamSal)

            SQLcommand.ExecuteNonQuery()

            lstrRetorna = SQLcommand.Parameters("@pMsjRetorno").Value.ToString.Trim
            If lstrRetorna.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
                Registra_Log_Basico = lstrRetorna
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            lstrRetorna = "No se pudo grabar en el Servicio de Registros de Eventos Log" & vbCr & Ex.Message
            Registra_Log_Basico = lstrRetorna
        Finally
            Registra_Log_Basico = lstrRetorna
            SQLConnect.Cerrar()
        End Try
    End Function

    Public Function Registra_Log_Detallado(ByVal pIdConceptoRegistro As Integer, _
                                           ByVal pIdTipoRegistro As Integer, _
                                           ByVal pId_usuario As Integer, _
                                           ByVal pDescripcion_registro As String, _
                                           ByVal pFechaProceso As String, _
                                           ByVal pClave1 As String, ByVal pClave2 As String, _
                                           ByVal pClave3 As String, ByVal pClave4 As String, _
                                           ByVal pClave5 As String, ByVal pClave6 As String, _
                                           ByVal pClave7 As String, ByVal pClave8 As String, _
                                           ByVal pClave9 As String, ByVal pClave10 As String) As String

        Dim SQLcommand As New SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlTransaction
        Dim Lstr_Procedimiento As String = "Sis_RegistroLogs_Detallado"
        Dim lstrRetorna As String = ""

        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLcommand = New SqlCommand(Lstr_Procedimiento, MiTransaccionSQL.Connection, MiTransaccionSQL) With {
                .CommandType = CommandType.StoredProcedure,
                .CommandText = Lstr_Procedimiento
            }

            SQLcommand.Parameters.Clear()
            SQLcommand.Parameters.Add("@pCodigoOperacion", SqlDbType.VarChar, 15).Value = "INSERTAR"
            SQLcommand.Parameters.Add("@pIdTipoRegistro", SqlDbType.Int).Value = pIdTipoRegistro
            SQLcommand.Parameters.Add("@pIdconceptoregistro", SqlDbType.Int).Value = pIdConceptoRegistro
            SQLcommand.Parameters.Add("@pIdusuario", SqlDbType.Int).Value = pId_usuario
            SQLcommand.Parameters.Add("@pDescripcionRegistro", SqlDbType.Char, 500).Value = pDescripcion_registro
            'SQLcommand.Parameters.Add("@pFechaProceso", SqlDbType.VarChar, 10).Value = pFechaProceso
            SQLcommand.Parameters.Add("@pClave1", SqlDbType.VarChar, 100).Value = pClave1
            SQLcommand.Parameters.Add("@pClave2", SqlDbType.VarChar, 100).Value = pClave2
            SQLcommand.Parameters.Add("@pClave3", SqlDbType.VarChar, 100).Value = pClave3
            SQLcommand.Parameters.Add("@pClave4", SqlDbType.VarChar, 100).Value = pClave4
            SQLcommand.Parameters.Add("@pClave5", SqlDbType.VarChar, 100).Value = pClave5
            SQLcommand.Parameters.Add("@pClave6", SqlDbType.VarChar, 100).Value = pClave6
            SQLcommand.Parameters.Add("@pClave7", SqlDbType.VarChar, 100).Value = pClave7
            SQLcommand.Parameters.Add("@pClave8", SqlDbType.VarChar, 100).Value = pClave8
            SQLcommand.Parameters.Add("@pClave9", SqlDbType.VarChar, 100).Value = pClave9
            SQLcommand.Parameters.Add("@pClave10", SqlDbType.VarChar, 100).Value = pClave10

            Dim oParamSal As New SqlParameter("@pMsjRetorno", SqlDbType.Char, 60) With {
                .Direction = ParameterDirection.Output
            }
            SQLcommand.Parameters.Add(oParamSal)

            SQLcommand.ExecuteNonQuery()

            lstrRetorna = SQLcommand.Parameters("@pMsjRetorno").Value.ToString.Trim

            If lstrRetorna.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
                Registra_Log_Detallado = lstrRetorna
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            lstrRetorna = "No se pudo grabar en el Servicio de Registros de Eventos Log" & vbCr & Ex.Message
            Registra_Log_Detallado = lstrRetorna
        Finally
            Registra_Log_Detallado = lstrRetorna
            SQLConnect.Cerrar()
        End Try

    End Function

    Public Function RegistroEventos_Ver(ByVal LTxt_pId_Usuario As String, _
                                    ByVal LTxt_PId_Tipo_Registro As String, _
                                    ByVal LTxt_PId_ConceptoRegistro As String, _
                                    ByVal LTxt_pFecha_Inicial As String, _
                                    ByVal LTxt_pFecha_Final As String, _
                                    ByVal LTxt_pFecha_Proceso As String, _
                                    ByVal LTxt_pEncabezado As String, _
                                    ByVal LTxt_pFiltro As String, _
                                    ByVal LTxt_pFiltro_Evento As String, _
                                    ByVal LTxt_ColumnasDetalle As String, _
                                    ByRef LTxt_Resultado As String, _
                                    Optional ByRef Ltxt_TipoEstado As String = "") As DataSet

        Dim LArr_NombreColumna_Detalle() As String = Split(LTxt_ColumnasDetalle, ",")
        Dim LInt_Col As Integer
        Dim LTxt_NomCol As String
        Dim Columna As DataColumn
        Dim Remove As Boolean

        Dim LDS_Eventos As New DataSet
        Dim LDS_Eventos_Aux As New DataSet

        Dim Sqlcommand As New SqlClient.SqlCommand
        Dim SqlLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim Lstr_Procedimiento As String = "Sis_RegistroEventos_Ver"
        Dim Lstr_NombreTabla As String = "Registro_Eventos"

        Connect.Abrir()
        Try
            Sqlcommand = New SqlCommand(Lstr_Procedimiento, Connect.Conexion)
            Sqlcommand.CommandType = CommandType.StoredProcedure
            Sqlcommand.CommandText = Lstr_Procedimiento
            Sqlcommand.CommandTimeout = 0
            Sqlcommand.Parameters.Clear()

            Sqlcommand.Parameters.Add("@pIdusuario", SqlDbType.Int).Value = IIf(LTxt_pId_Usuario = "", DBNull.Value, CInt(0 & LTxt_pId_Usuario))
            Sqlcommand.Parameters.Add("@pIdTipoRegistro", SqlDbType.Int).Value = IIf(LTxt_PId_Tipo_Registro.Trim = "", DBNull.Value, CInt(0 & LTxt_PId_Tipo_Registro.Trim))
            Sqlcommand.Parameters.Add("@pIdConceptoRegistro", SqlDbType.Int).Value = IIf(LTxt_PId_ConceptoRegistro.Trim = "", DBNull.Value, CInt(0 & LTxt_PId_ConceptoRegistro.Trim))
            Sqlcommand.Parameters.Add("@pFechaInicial", SqlDbType.Char, 10).Value = IIf(LTxt_pFecha_Inicial.Trim = "", DBNull.Value, LTxt_pFecha_Inicial)
            Sqlcommand.Parameters.Add("@pFechaFinal", SqlDbType.Char, 10).Value = IIf(LTxt_pFecha_Final.Trim = "", DBNull.Value, LTxt_pFecha_Final)
            Sqlcommand.Parameters.Add("@pFechaProceso", SqlDbType.Char, 10).Value = IIf(LTxt_pFecha_Proceso.Trim = "", DBNull.Value, LTxt_pFecha_Proceso)
            Sqlcommand.Parameters.Add("@pEncabezado", SqlDbType.VarChar, 250).Value = IIf(LTxt_pEncabezado = "", DBNull.Value, LTxt_pEncabezado)
            Sqlcommand.Parameters.Add("@pFiltro", SqlDbType.VarChar, 250).Value = IIf(LTxt_pFiltro = "", DBNull.Value, LTxt_pFiltro)
            Sqlcommand.Parameters.Add("@pFiltroEvento", SqlDbType.VarChar, 250).Value = IIf(LTxt_pFiltro_Evento = "", DBNull.Value, LTxt_pFiltro_Evento)
            Sqlcommand.Parameters.Add("@pEstado", SqlDbType.VarChar, 3).Value = IIf(Ltxt_TipoEstado = "", DBNull.Value, Ltxt_TipoEstado)


            SqlLDataAdapter.SelectCommand = Sqlcommand
            SqlLDataAdapter.Fill(LDS_Eventos, Lstr_NombreTabla)

            If LTxt_ColumnasDetalle.Trim = "" Then
                LDS_Eventos_Aux = LDS_Eventos
            Else
                LDS_Eventos_Aux = LDS_Eventos.Copy
                For Each Columna In LDS_Eventos.Tables(0).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna_Detalle.Length - 1
                        LTxt_NomCol = LArr_NombreColumna_Detalle(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LTxt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Eventos_Aux.Tables(0).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            LTxt_Resultado = "OK"
            Return LDS_Eventos_Aux

        Catch ex As Exception
            LTxt_Resultado = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    ''' <summary>
    ''' función que obtiene las ultimas (n) claves del usuario, 
    ''' donde n es el numero de claves a recordar.
    ''' </summary>
    ''' <param name="idUsuario">Identificador del usuario.</param>
    ''' <param name="numClavesRecordar">Numero de claves a recordar.</param>
    ''' <returns>Tabla con las claves encontradas.</returns>
    Public Function RegistroClavesConsultar(idUsuario As Long, numClavesRecordar As Integer) As DataTable
        Dim dtClaves As New DataTable
        Dim Sqlcommand As New SqlCommand
        Dim SqlLDataAdapter As New SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim nombreProcedimiento As String = "Sis_RegistroClaves_Consultar"
        Connect.Abrir()
        Try
            Sqlcommand = New SqlCommand(nombreProcedimiento, Connect.Conexion) With {
                .CommandType = CommandType.StoredProcedure,
                .CommandText = nombreProcedimiento,
                .CommandTimeout = 0
            }
            Sqlcommand.Parameters.Clear()
            Sqlcommand.Parameters.Add("@pIdusuario", SqlDbType.Int).Value = idUsuario
            Sqlcommand.Parameters.Add("@pNumClaveRecordar", SqlDbType.VarChar, 3).Value = numClavesRecordar
            SqlLDataAdapter.SelectCommand = Sqlcommand
            SqlLDataAdapter.Fill(dtClaves)
            Return dtClaves
        Catch ex As Exception
            Throw ex
        Finally
            Connect.Cerrar()
        End Try
    End Function
End Class
