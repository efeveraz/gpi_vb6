Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports Microsoft.VisualBasic

Public Class ClsConceptoRegistro

    Public Function Buscar(ByRef strRetorno As String, _
                           ByVal pId_Concepto_Registro As Integer, _
                           ByVal pId_Tipo_registro As Integer, _
                           ByVal pNombre_Concepto_Registro As String, _
                           ByVal pNombre_Corto_Concepto_Reg As String, _
                           ByVal pEstado_Concepto_Registro As String) As DataSet

        Dim LDS_TipoRegistro As New DataSet
        Dim LDS_TipoRegistro_Aux As New DataSet

        Dim Sqlcommand As New SqlClient.SqlCommand
        Dim SqlLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim Lstr_Procedimiento As String = "SGC_SEGURIDAD_LOG.SP_CONSULTA_CONCEPTO_REGISTRO"
        Dim Lstr_NombreTabla As String = "CONCEPTO_REGISTRO"

        Connect.Abrir()

        Try

            Sqlcommand = New SqlCommand(Lstr_Procedimiento, Connect.Conexion)

            Sqlcommand.CommandType = CommandType.StoredProcedure
            Sqlcommand.CommandText = Lstr_Procedimiento
            Sqlcommand.Parameters.Clear()

            Sqlcommand.Parameters.Add("@pIdConceptoRegistro", SqlDbType.Int).Value = IIf(pId_Concepto_Registro = -1, DBNull.Value, pId_Concepto_Registro)
            Sqlcommand.Parameters.Add("@pIdTiporegistro", SqlDbType.Int).Value = IIf(pId_Tipo_registro = -1, DBNull.Value, pId_Tipo_registro)
            Sqlcommand.Parameters.Add("@pNombreConceptoRegistro", SqlDbType.VarChar, 50).Value = IIf(pNombre_Concepto_Registro = "", DBNull.Value, pNombre_Concepto_Registro)
            Sqlcommand.Parameters.Add("@pNombre_Corto_Concepto_Reg", SqlDbType.Char, 15).Value = IIf(pNombre_Corto_Concepto_Reg = "", DBNull.Value, pNombre_Corto_Concepto_Reg)
            Sqlcommand.Parameters.Add("@pEstadoConceptoRegistro", SqlDbType.Char, 3).Value = IIf(pEstado_Concepto_Registro = "", DBNull.Value, pEstado_Concepto_Registro)

            SqlLDataAdapter.SelectCommand = Sqlcommand
            SqlLDataAdapter.Fill(LDS_TipoRegistro, Lstr_NombreTabla)

            LDS_TipoRegistro_Aux = LDS_TipoRegistro

            strRetorno = "OK"
            Return LDS_TipoRegistro_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

        Connect = Nothing

    End Function

    Public Function ConceptoRegistro_Ver(ByVal LTxt_pId_TipoRegistro As String,
                                         ByVal LTxt_ColumnasDetalle As String,
                                         ByRef LTxt_Resultado As String) As DataSet

        Dim LArr_NombreColumna_Detalle() As String = Split(LTxt_ColumnasDetalle, ",")
        Dim LInt_Col As Integer
        Dim LTxt_NomCol As String
        Dim Columna As DataColumn
        Dim Remove As Boolean

        Dim LDS_Conceptos As New DataSet
        Dim LDS_Conceptos_Aux As New DataSet

        Dim Sqlcommand As New SqlClient.SqlCommand
        Dim SqlLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim Lstr_Procedimiento As String = "Sis_ConceptoRegistro_ver"
        Dim Lstr_NombreTabla As String = "Concepto_Registro"

        Connect.Abrir()
        Try
            Sqlcommand = New SqlCommand(Lstr_Procedimiento, Connect.Conexion)
            Sqlcommand.CommandType = CommandType.StoredProcedure
            Sqlcommand.CommandText = Lstr_Procedimiento
            Sqlcommand.Parameters.Clear()

            Sqlcommand.Parameters.Add("@pIdConceptoRegistro", SqlDbType.Int).Value = IIf(LTxt_pId_TipoRegistro.Trim = "", 0, CDbl(LTxt_pId_TipoRegistro))

            SqlLDataAdapter.SelectCommand = Sqlcommand
            SqlLDataAdapter.Fill(LDS_Conceptos, Lstr_NombreTabla)

            If LTxt_ColumnasDetalle.Trim = "" Then
                LDS_Conceptos_Aux = LDS_Conceptos
            Else
                LDS_Conceptos_Aux = LDS_Conceptos.Copy
                For Each Columna In LDS_Conceptos.Tables(0).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna_Detalle.Length - 1
                        LTxt_NomCol = LArr_NombreColumna_Detalle(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LTxt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Conceptos_Aux.Tables(0).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            LTxt_Resultado = "OK"
            Return LDS_Conceptos_Aux

        Catch ex As Exception
            LTxt_Resultado = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="idTipoRegistro"></param>
    ''' <param name="nombreCorto"></param>
    ''' <returns></returns>
    Public Function GetIdConceptoRegistro(idTipoRegistro As Decimal, ByVal nombreCorto As String) As Decimal
        Dim resultado = "OK"
        Dim dsConceptoRegistro = ConceptoRegistro_Ver("0", "", resultado)
        If resultado = "OK" Then
            If Not IsNothing(dsConceptoRegistro) AndAlso
                    dsConceptoRegistro.Tables.Count > 0 AndAlso
                        dsConceptoRegistro.Tables(0).Rows.Count > 0 Then
                Dim row = dsConceptoRegistro.Tables(0).AsEnumerable().SingleOrDefault(
                        Function(r)
                            Return r.Field(Of String)("NOMBRE_CORTO_CONCEPTO_REGISTRO") = nombreCorto _
                                And r.Field(Of Decimal)("ID_TIPO_REGISTRO") = idTipoRegistro
                        End Function)
                If Not IsNothing(row) Then
                    Return row.Field(Of Decimal)("ID_CONCEPTO_REGISTRO")
                End If
                Throw New Exception("No se encontr� el Concepto de Registro.")
            Else
                Throw New Exception(resultado)
            End If
        End If
    End Function
End Class
