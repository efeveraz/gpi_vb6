<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Frm_MantenedorPerfilesUsuarios
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Frm_MantenedorPerfilesUsuarios))
        Me.Pnl_DatosPerfil = New System.Windows.Forms.Panel
        Me.Cmb_EstadoPerfil = New C1.Win.C1List.C1Combo
        Me.Txt_CodigoExternoPerfil = New C1.Win.C1Input.C1TextBox
        Me.Txt_NombreCortoPerfil = New C1.Win.C1Input.C1TextBox
        Me.Txt_NombrePerfil = New C1.Win.C1Input.C1TextBox
        Me.C1SuperLabel8 = New C1.Win.C1SuperTooltip.C1SuperLabel
        Me.C1SuperLabel5 = New C1.Win.C1SuperTooltip.C1SuperLabel
        Me.C1SuperLabel11 = New C1.Win.C1SuperTooltip.C1SuperLabel
        Me.C1SuperLabel22 = New C1.Win.C1SuperTooltip.C1SuperLabel
        Me.C1SuperLabel16 = New C1.Win.C1SuperTooltip.C1SuperLabel
        Me.Pnl_Datos_TipoOperacion = New System.Windows.Forms.Panel
        Me.Btn_Cancelar = New C1.Win.C1Input.C1Button
        Me.Tdg_PerfilesUsuario = New C1.Win.C1TrueDBGrid.C1TrueDBGrid
        Me.Btn_Accion = New C1.Win.C1Input.C1Button
        Me.Btn_Modificar = New C1.Win.C1Input.C1Button
        Me.Btn_Agregar = New C1.Win.C1Input.C1Button
        Me.Btn_Eliminar = New C1.Win.C1Input.C1Button
        Me.Pnl_UsuariosPerfil = New System.Windows.Forms.Panel
        Me.Cmb_NegocioUsuarios = New C1.Win.C1List.C1Combo
        Me.Tdg_UsuariosDisponibles = New C1.Win.C1TrueDBGrid.C1TrueDBGrid
        Me.Tdg_UsuarioAsignados = New C1.Win.C1TrueDBGrid.C1TrueDBGrid
        Me.Btn_QuitarUsuario = New C1.Win.C1Input.C1Button
        Me.Btn_AsociarUsuario = New C1.Win.C1Input.C1Button
        Me.Btn_Salir = New C1.Win.C1Input.C1Button
        Me.C1SuperTooltip1 = New C1.Win.C1SuperTooltip.C1SuperTooltip(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip
        Me.StatusLabel = New System.Windows.Forms.ToolStripStatusLabel
        Me.Ctrl_Botonera = New System.Windows.Forms.ToolStrip
        Me.Ctrl_Botonera_Agregar = New System.Windows.Forms.ToolStripButton
        Me.Ctrl_Botonera_Buscar = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.Ctrl_Botonera_Guardar = New System.Windows.Forms.ToolStripButton
        Me.Ctrl_Botonera_Editar = New System.Windows.Forms.ToolStripButton
        Me.Ctrl_Botonera_Eliminar = New System.Windows.Forms.ToolStripButton
        Me.Ctrl_Botonera_Verificar = New System.Windows.Forms.ToolStripButton
        Me.Ctrl_Botonera_Procesar = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator
        Me.Ctrl_Botonera_Imprimir = New System.Windows.Forms.ToolStripButton
        Me.Ctrl_Botonera_Excel = New System.Windows.Forms.ToolStripButton
        Me.Ctrl_Botonera_Columnas = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator
        Me.Ctrl_Botonera_Cancelar = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator
        Me.Ctrl_Combo_Plantillas = New System.Windows.Forms.ToolStripComboBox
        Me.Pnl_DatosPerfil.SuspendLayout()
        CType(Me.Cmb_EstadoPerfil, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Txt_CodigoExternoPerfil, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Txt_NombreCortoPerfil, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Txt_NombrePerfil, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Pnl_Datos_TipoOperacion.SuspendLayout()
        CType(Me.Tdg_PerfilesUsuario, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Pnl_UsuariosPerfil.SuspendLayout()
        CType(Me.Cmb_NegocioUsuarios, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Tdg_UsuariosDisponibles, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Tdg_UsuarioAsignados, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.Ctrl_Botonera.SuspendLayout()
        Me.SuspendLayout()
        '
        'Pnl_DatosPerfil
        '
        Me.Pnl_DatosPerfil.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Pnl_DatosPerfil.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Pnl_DatosPerfil.Controls.Add(Me.Cmb_EstadoPerfil)
        Me.Pnl_DatosPerfil.Controls.Add(Me.Txt_CodigoExternoPerfil)
        Me.Pnl_DatosPerfil.Controls.Add(Me.Txt_NombreCortoPerfil)
        Me.Pnl_DatosPerfil.Controls.Add(Me.Txt_NombrePerfil)
        Me.Pnl_DatosPerfil.Controls.Add(Me.C1SuperLabel8)
        Me.Pnl_DatosPerfil.Controls.Add(Me.C1SuperLabel5)
        Me.Pnl_DatosPerfil.Controls.Add(Me.C1SuperLabel11)
        Me.Pnl_DatosPerfil.Controls.Add(Me.C1SuperLabel22)
        Me.Pnl_DatosPerfil.Location = New System.Drawing.Point(1, 23)
        Me.Pnl_DatosPerfil.Name = "Pnl_DatosPerfil"
        Me.Pnl_DatosPerfil.Size = New System.Drawing.Size(352, 77)
        Me.Pnl_DatosPerfil.TabIndex = 1025
        '
        'Cmb_EstadoPerfil
        '
        Me.Cmb_EstadoPerfil.AddItemSeparator = Global.Microsoft.VisualBasic.ChrW(59)
        Me.Cmb_EstadoPerfil.AllowColMove = False
        Me.Cmb_EstadoPerfil.AllowSort = False
        Me.Cmb_EstadoPerfil.AutoDropDown = True
        Me.Cmb_EstadoPerfil.AutoSelect = True
        Me.Cmb_EstadoPerfil.AutoSize = False
        Me.Cmb_EstadoPerfil.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Cmb_EstadoPerfil.Caption = ""
        Me.Cmb_EstadoPerfil.CaptionHeight = 18
        Me.Cmb_EstadoPerfil.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.Cmb_EstadoPerfil.ColumnCaptionHeight = 18
        Me.Cmb_EstadoPerfil.ColumnFooterHeight = 18
        Me.Cmb_EstadoPerfil.ColumnHeaders = False
        Me.Cmb_EstadoPerfil.ComboStyle = C1.Win.C1List.ComboStyleEnum.DropdownList
        Me.Cmb_EstadoPerfil.ContentHeight = 12
        Me.Cmb_EstadoPerfil.DataMode = C1.Win.C1List.DataModeEnum.AddItem
        Me.Cmb_EstadoPerfil.DeadAreaBackColor = System.Drawing.Color.Empty
        Me.Cmb_EstadoPerfil.EditorBackColor = System.Drawing.Color.White
        Me.Cmb_EstadoPerfil.EditorFont = New System.Drawing.Font("Arial", 7.0!, System.Drawing.FontStyle.Bold)
        Me.Cmb_EstadoPerfil.EditorForeColor = System.Drawing.Color.SteelBlue
        Me.Cmb_EstadoPerfil.EditorHeight = 12
        Me.Cmb_EstadoPerfil.ExtendRightColumn = True
        Me.Cmb_EstadoPerfil.FlatStyle = C1.Win.C1List.FlatModeEnum.Flat
        Me.Cmb_EstadoPerfil.Font = New System.Drawing.Font("Arial", 7.0!, System.Drawing.FontStyle.Bold)
        Me.Cmb_EstadoPerfil.Images.Add(CType(resources.GetObject("Cmb_EstadoPerfil.Images"), System.Drawing.Image))
        Me.Cmb_EstadoPerfil.ItemHeight = 18
        Me.Cmb_EstadoPerfil.Location = New System.Drawing.Point(236, 20)
        Me.Cmb_EstadoPerfil.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Cmb_EstadoPerfil.MatchEntry = C1.Win.C1List.MatchEntryEnum.Extended
        Me.Cmb_EstadoPerfil.MatchEntryTimeout = CType(500, Long)
        Me.Cmb_EstadoPerfil.MaxDropDownItems = CType(5, Short)
        Me.Cmb_EstadoPerfil.MaxLength = 0
        Me.Cmb_EstadoPerfil.MinimumSize = New System.Drawing.Size(10, 16)
        Me.Cmb_EstadoPerfil.MouseCursor = System.Windows.Forms.Cursors.Default
        Me.Cmb_EstadoPerfil.Name = "Cmb_EstadoPerfil"
        Me.Cmb_EstadoPerfil.RowSubDividerColor = System.Drawing.Color.DarkGray
        Me.Cmb_EstadoPerfil.Size = New System.Drawing.Size(109, 16)
        Me.Cmb_EstadoPerfil.TabIndex = 1
        Me.Cmb_EstadoPerfil.VisualStyle = C1.Win.C1List.VisualStyle.Office2007Silver
        Me.Cmb_EstadoPerfil.PropBag = resources.GetString("Cmb_EstadoPerfil.PropBag")
        '
        'Txt_CodigoExternoPerfil
        '
        Me.Txt_CodigoExternoPerfil.AcceptsEscape = False
        Me.Txt_CodigoExternoPerfil.AcceptsReturn = True
        Me.Txt_CodigoExternoPerfil.AcceptsTab = True
        Me.Txt_CodigoExternoPerfil.AutoSize = False
        Me.Txt_CodigoExternoPerfil.BackColor = System.Drawing.Color.White
        Me.Txt_CodigoExternoPerfil.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Txt_CodigoExternoPerfil.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.Txt_CodigoExternoPerfil.DisabledForeColor = System.Drawing.Color.Lavender
        Me.Txt_CodigoExternoPerfil.Font = New System.Drawing.Font("Arial", 7.0!, System.Drawing.FontStyle.Bold)
        Me.Txt_CodigoExternoPerfil.ForeColor = System.Drawing.Color.SteelBlue
        Me.Txt_CodigoExternoPerfil.Location = New System.Drawing.Point(186, 52)
        Me.Txt_CodigoExternoPerfil.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.Txt_CodigoExternoPerfil.MaxLength = 20
        Me.Txt_CodigoExternoPerfil.Name = "Txt_CodigoExternoPerfil"
        Me.Txt_CodigoExternoPerfil.NumericInput = False
        Me.Txt_CodigoExternoPerfil.ReadOnly = True
        Me.Txt_CodigoExternoPerfil.Size = New System.Drawing.Size(159, 16)
        Me.Txt_CodigoExternoPerfil.TabIndex = 3
        Me.Txt_CodigoExternoPerfil.TabStop = False
        Me.Txt_CodigoExternoPerfil.Tag = Nothing
        Me.Txt_CodigoExternoPerfil.TextDetached = True
        Me.Txt_CodigoExternoPerfil.Value = " "
        Me.Txt_CodigoExternoPerfil.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Silver
        '
        'Txt_NombreCortoPerfil
        '
        Me.Txt_NombreCortoPerfil.AcceptsEscape = False
        Me.Txt_NombreCortoPerfil.AcceptsReturn = True
        Me.Txt_NombreCortoPerfil.AcceptsTab = True
        Me.Txt_NombreCortoPerfil.AutoSize = False
        Me.Txt_NombreCortoPerfil.BackColor = System.Drawing.Color.White
        Me.Txt_NombreCortoPerfil.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Txt_NombreCortoPerfil.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.Txt_NombreCortoPerfil.DisabledForeColor = System.Drawing.Color.Lavender
        Me.Txt_NombreCortoPerfil.Font = New System.Drawing.Font("Arial", 7.0!, System.Drawing.FontStyle.Bold)
        Me.Txt_NombreCortoPerfil.ForeColor = System.Drawing.Color.SteelBlue
        Me.Txt_NombreCortoPerfil.Location = New System.Drawing.Point(8, 52)
        Me.Txt_NombreCortoPerfil.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.Txt_NombreCortoPerfil.MaxLength = 15
        Me.Txt_NombreCortoPerfil.Name = "Txt_NombreCortoPerfil"
        Me.Txt_NombreCortoPerfil.NumericInput = False
        Me.Txt_NombreCortoPerfil.ReadOnly = True
        Me.Txt_NombreCortoPerfil.Size = New System.Drawing.Size(175, 16)
        Me.Txt_NombreCortoPerfil.TabIndex = 2
        Me.Txt_NombreCortoPerfil.TabStop = False
        Me.Txt_NombreCortoPerfil.Tag = Nothing
        Me.Txt_NombreCortoPerfil.TextDetached = True
        Me.Txt_NombreCortoPerfil.Value = " "
        Me.Txt_NombreCortoPerfil.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Silver
        '
        'Txt_NombrePerfil
        '
        Me.Txt_NombrePerfil.AcceptsEscape = False
        Me.Txt_NombrePerfil.AcceptsReturn = True
        Me.Txt_NombrePerfil.AcceptsTab = True
        Me.Txt_NombrePerfil.AutoSize = False
        Me.Txt_NombrePerfil.BackColor = System.Drawing.Color.White
        Me.Txt_NombrePerfil.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Txt_NombrePerfil.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.Txt_NombrePerfil.DisabledForeColor = System.Drawing.Color.Lavender
        Me.Txt_NombrePerfil.Font = New System.Drawing.Font("Arial", 7.0!, System.Drawing.FontStyle.Bold)
        Me.Txt_NombrePerfil.ForeColor = System.Drawing.Color.SteelBlue
        Me.Txt_NombrePerfil.Location = New System.Drawing.Point(8, 20)
        Me.Txt_NombrePerfil.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.Txt_NombrePerfil.MaxLength = 50
        Me.Txt_NombrePerfil.Name = "Txt_NombrePerfil"
        Me.Txt_NombrePerfil.NumericInput = False
        Me.Txt_NombrePerfil.ReadOnly = True
        Me.Txt_NombrePerfil.Size = New System.Drawing.Size(225, 16)
        Me.Txt_NombrePerfil.TabIndex = 0
        Me.Txt_NombrePerfil.TabStop = False
        Me.Txt_NombrePerfil.Tag = Nothing
        Me.Txt_NombrePerfil.TextDetached = True
        Me.Txt_NombrePerfil.Value = " "
        Me.Txt_NombrePerfil.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Silver
        '
        'C1SuperLabel8
        '
        Me.C1SuperLabel8.AutoSize = True
        Me.C1SuperLabel8.BackColor = System.Drawing.Color.Transparent
        Me.C1SuperLabel8.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.C1SuperLabel8.ForeColor = System.Drawing.Color.DimGray
        Me.C1SuperLabel8.Location = New System.Drawing.Point(184, 36)
        Me.C1SuperLabel8.Margin = New System.Windows.Forms.Padding(2, 4, 2, 4)
        Me.C1SuperLabel8.Name = "C1SuperLabel8"
        Me.C1SuperLabel8.Size = New System.Drawing.Size(79, 19)
        Me.C1SuperLabel8.TabIndex = 1050
        Me.C1SuperLabel8.Text = "Código Externo"
        Me.C1SuperLabel8.UseMnemonic = True
        '
        'C1SuperLabel5
        '
        Me.C1SuperLabel5.AutoSize = True
        Me.C1SuperLabel5.BackColor = System.Drawing.Color.Transparent
        Me.C1SuperLabel5.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.C1SuperLabel5.ForeColor = System.Drawing.Color.DimGray
        Me.C1SuperLabel5.Location = New System.Drawing.Point(234, 4)
        Me.C1SuperLabel5.Margin = New System.Windows.Forms.Padding(2, 4, 2, 4)
        Me.C1SuperLabel5.Name = "C1SuperLabel5"
        Me.C1SuperLabel5.Size = New System.Drawing.Size(38, 19)
        Me.C1SuperLabel5.TabIndex = 1048
        Me.C1SuperLabel5.Text = "Estado"
        Me.C1SuperLabel5.UseMnemonic = True
        '
        'C1SuperLabel11
        '
        Me.C1SuperLabel11.AutoSize = True
        Me.C1SuperLabel11.BackColor = System.Drawing.Color.Transparent
        Me.C1SuperLabel11.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.C1SuperLabel11.ForeColor = System.Drawing.Color.DimGray
        Me.C1SuperLabel11.Location = New System.Drawing.Point(6, 36)
        Me.C1SuperLabel11.Margin = New System.Windows.Forms.Padding(2, 4, 2, 4)
        Me.C1SuperLabel11.Name = "C1SuperLabel11"
        Me.C1SuperLabel11.Size = New System.Drawing.Size(71, 19)
        Me.C1SuperLabel11.TabIndex = 1047
        Me.C1SuperLabel11.Text = "Nombre Corto"
        Me.C1SuperLabel11.UseMnemonic = True
        '
        'C1SuperLabel22
        '
        Me.C1SuperLabel22.AutoSize = True
        Me.C1SuperLabel22.BackColor = System.Drawing.Color.Transparent
        Me.C1SuperLabel22.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.C1SuperLabel22.ForeColor = System.Drawing.Color.DimGray
        Me.C1SuperLabel22.Location = New System.Drawing.Point(6, 4)
        Me.C1SuperLabel22.Margin = New System.Windows.Forms.Padding(2, 4, 2, 4)
        Me.C1SuperLabel22.Name = "C1SuperLabel22"
        Me.C1SuperLabel22.Size = New System.Drawing.Size(43, 19)
        Me.C1SuperLabel22.TabIndex = 1044
        Me.C1SuperLabel22.Text = "Nombre"
        Me.C1SuperLabel22.UseMnemonic = True
        '
        'C1SuperLabel16
        '
        Me.C1SuperLabel16.BackColor = System.Drawing.Color.Transparent
        Me.C1SuperLabel16.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.C1SuperLabel16.ForeColor = System.Drawing.Color.DimGray
        Me.C1SuperLabel16.Location = New System.Drawing.Point(1, 4)
        Me.C1SuperLabel16.Margin = New System.Windows.Forms.Padding(2, 4, 2, 4)
        Me.C1SuperLabel16.Name = "C1SuperLabel16"
        Me.C1SuperLabel16.Size = New System.Drawing.Size(45, 19)
        Me.C1SuperLabel16.TabIndex = 1049
        Me.C1SuperLabel16.Text = "Negocio"
        Me.C1SuperLabel16.UseMnemonic = True
        '
        'Pnl_Datos_TipoOperacion
        '
        Me.Pnl_Datos_TipoOperacion.BackColor = System.Drawing.Color.Gainsboro
        Me.Pnl_Datos_TipoOperacion.Controls.Add(Me.Btn_Cancelar)
        Me.Pnl_Datos_TipoOperacion.Controls.Add(Me.Tdg_PerfilesUsuario)
        Me.Pnl_Datos_TipoOperacion.Controls.Add(Me.Btn_Accion)
        Me.Pnl_Datos_TipoOperacion.Controls.Add(Me.Btn_Modificar)
        Me.Pnl_Datos_TipoOperacion.Controls.Add(Me.Btn_Agregar)
        Me.Pnl_Datos_TipoOperacion.Controls.Add(Me.Btn_Eliminar)
        Me.Pnl_Datos_TipoOperacion.Location = New System.Drawing.Point(1, 99)
        Me.Pnl_Datos_TipoOperacion.Margin = New System.Windows.Forms.Padding(2)
        Me.Pnl_Datos_TipoOperacion.Name = "Pnl_Datos_TipoOperacion"
        Me.Pnl_Datos_TipoOperacion.Size = New System.Drawing.Size(352, 340)
        Me.Pnl_Datos_TipoOperacion.TabIndex = 1026
        Me.Pnl_Datos_TipoOperacion.TabStop = True
        '
        'Btn_Cancelar
        '
        Me.Btn_Cancelar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.Btn_Cancelar.Location = New System.Drawing.Point(280, 4)
        Me.Btn_Cancelar.Margin = New System.Windows.Forms.Padding(2)
        Me.Btn_Cancelar.Name = "Btn_Cancelar"
        Me.Btn_Cancelar.Size = New System.Drawing.Size(65, 21)
        Me.Btn_Cancelar.TabIndex = 5
        Me.Btn_Cancelar.TabStop = False
        Me.Btn_Cancelar.Text = "&Cancelar"
        Me.Btn_Cancelar.UseVisualStyleBackColor = True
        Me.Btn_Cancelar.Visible = False
        Me.Btn_Cancelar.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Silver
        '
        'Tdg_PerfilesUsuario
        '
        Me.Tdg_PerfilesUsuario.AllowColSelect = False
        Me.Tdg_PerfilesUsuario.AllowRowSizing = C1.Win.C1TrueDBGrid.RowSizingEnum.None
        Me.Tdg_PerfilesUsuario.AllowUpdate = False
        Me.Tdg_PerfilesUsuario.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Tdg_PerfilesUsuario.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Tdg_PerfilesUsuario.Caption = "P E R F I L E S"
        Me.Tdg_PerfilesUsuario.CaptionHeight = 21
        Me.Tdg_PerfilesUsuario.DirectionAfterEnter = C1.Win.C1TrueDBGrid.DirectionAfterEnterEnum.MoveDown
        Me.Tdg_PerfilesUsuario.FilterBar = True
        Me.Tdg_PerfilesUsuario.Font = New System.Drawing.Font("Arial", 7.5!)
        Me.Tdg_PerfilesUsuario.ForeColor = System.Drawing.Color.Lavender
        Me.Tdg_PerfilesUsuario.GroupByCaption = "Arrastre hasta aquí las columnas por las que desea agrupar"
        Me.Tdg_PerfilesUsuario.Images.Add(CType(resources.GetObject("Tdg_PerfilesUsuario.Images"), System.Drawing.Image))
        Me.Tdg_PerfilesUsuario.Location = New System.Drawing.Point(0, 28)
        Me.Tdg_PerfilesUsuario.MaintainRowCurrency = True
        Me.Tdg_PerfilesUsuario.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.Tdg_PerfilesUsuario.MarqueeStyle = C1.Win.C1TrueDBGrid.MarqueeEnum.HighlightRow
        Me.Tdg_PerfilesUsuario.MultiSelect = C1.Win.C1TrueDBGrid.MultiSelectEnum.Simple
        Me.Tdg_PerfilesUsuario.Name = "Tdg_PerfilesUsuario"
        Me.Tdg_PerfilesUsuario.PreviewInfo.Location = New System.Drawing.Point(0, 0)
        Me.Tdg_PerfilesUsuario.PreviewInfo.Size = New System.Drawing.Size(0, 0)
        Me.Tdg_PerfilesUsuario.PreviewInfo.ZoomFactor = 75
        Me.Tdg_PerfilesUsuario.PrintInfo.PageSettings = CType(resources.GetObject("Tdg_PerfilesUsuario.PrintInfo.PageSettings"), System.Drawing.Printing.PageSettings)
        Me.Tdg_PerfilesUsuario.RecordSelectors = False
        Me.Tdg_PerfilesUsuario.RecordSelectorWidth = 15
        Me.Tdg_PerfilesUsuario.RowDivider.Color = System.Drawing.Color.DarkGray
        Me.Tdg_PerfilesUsuario.RowDivider.Style = C1.Win.C1TrueDBGrid.LineStyleEnum.Raised
        Me.Tdg_PerfilesUsuario.RowHeight = 15
        Me.Tdg_PerfilesUsuario.ScrollTips = True
        Me.Tdg_PerfilesUsuario.Size = New System.Drawing.Size(352, 309)
        Me.Tdg_PerfilesUsuario.SplitDividerSize = New System.Drawing.Size(0, 0)
        Me.Tdg_PerfilesUsuario.TabIndex = 4
        Me.Tdg_PerfilesUsuario.TabStop = False
        Me.Tdg_PerfilesUsuario.Text = "C1TrueDBGrid1"
        Me.Tdg_PerfilesUsuario.VisualStyle = C1.Win.C1TrueDBGrid.VisualStyle.Office2007Silver
        Me.Tdg_PerfilesUsuario.PropBag = resources.GetString("Tdg_PerfilesUsuario.PropBag")
        '
        'Btn_Accion
        '
        Me.Btn_Accion.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.Btn_Accion.Location = New System.Drawing.Point(212, 4)
        Me.Btn_Accion.Margin = New System.Windows.Forms.Padding(2)
        Me.Btn_Accion.Name = "Btn_Accion"
        Me.Btn_Accion.Size = New System.Drawing.Size(65, 21)
        Me.Btn_Accion.TabIndex = 3
        Me.Btn_Accion.TabStop = False
        Me.Btn_Accion.Text = "&Grabar"
        Me.Btn_Accion.UseVisualStyleBackColor = True
        Me.Btn_Accion.Visible = False
        Me.Btn_Accion.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Silver
        '
        'Btn_Modificar
        '
        Me.Btn_Modificar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.Btn_Modificar.Enabled = False
        Me.Btn_Modificar.Font = New System.Drawing.Font("Arial", 7.5!)
        Me.Btn_Modificar.Location = New System.Drawing.Point(159, 4)
        Me.Btn_Modificar.Margin = New System.Windows.Forms.Padding(2)
        Me.Btn_Modificar.Name = "Btn_Modificar"
        Me.Btn_Modificar.Size = New System.Drawing.Size(24, 21)
        Me.Btn_Modificar.TabIndex = 2
        Me.Btn_Modificar.TabStop = False
        Me.Btn_Modificar.Tag = ""
        Me.Btn_Modificar.Text = "M"
        Me.C1SuperTooltip1.SetToolTip(Me.Btn_Modificar, "Modifica registro seleccionado")
        Me.Btn_Modificar.UseVisualStyleBackColor = True
        Me.Btn_Modificar.Visible = False
        Me.Btn_Modificar.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Silver
        '
        'Btn_Agregar
        '
        Me.Btn_Agregar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.Btn_Agregar.Font = New System.Drawing.Font("Arial", 7.5!)
        Me.Btn_Agregar.Location = New System.Drawing.Point(109, 4)
        Me.Btn_Agregar.Margin = New System.Windows.Forms.Padding(2)
        Me.Btn_Agregar.Name = "Btn_Agregar"
        Me.Btn_Agregar.Size = New System.Drawing.Size(24, 21)
        Me.Btn_Agregar.TabIndex = 0
        Me.Btn_Agregar.TabStop = False
        Me.Btn_Agregar.Tag = ""
        Me.Btn_Agregar.Text = "+"
        Me.C1SuperTooltip1.SetToolTip(Me.Btn_Agregar, "Crea nuevo registro")
        Me.Btn_Agregar.UseVisualStyleBackColor = True
        Me.Btn_Agregar.Visible = False
        Me.Btn_Agregar.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Silver
        '
        'Btn_Eliminar
        '
        Me.Btn_Eliminar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.Btn_Eliminar.Enabled = False
        Me.Btn_Eliminar.Font = New System.Drawing.Font("Arial", 7.5!)
        Me.Btn_Eliminar.Location = New System.Drawing.Point(134, 4)
        Me.Btn_Eliminar.Margin = New System.Windows.Forms.Padding(2)
        Me.Btn_Eliminar.Name = "Btn_Eliminar"
        Me.Btn_Eliminar.Size = New System.Drawing.Size(24, 21)
        Me.Btn_Eliminar.TabIndex = 1
        Me.Btn_Eliminar.TabStop = False
        Me.Btn_Eliminar.Tag = ""
        Me.Btn_Eliminar.Text = "─"
        Me.C1SuperTooltip1.SetToolTip(Me.Btn_Eliminar, "Elimina registro seleccionado")
        Me.Btn_Eliminar.UseVisualStyleBackColor = True
        Me.Btn_Eliminar.Visible = False
        Me.Btn_Eliminar.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Silver
        '
        'Pnl_UsuariosPerfil
        '
        Me.Pnl_UsuariosPerfil.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Pnl_UsuariosPerfil.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.Pnl_UsuariosPerfil.Controls.Add(Me.Cmb_NegocioUsuarios)
        Me.Pnl_UsuariosPerfil.Controls.Add(Me.Tdg_UsuariosDisponibles)
        Me.Pnl_UsuariosPerfil.Controls.Add(Me.Tdg_UsuarioAsignados)
        Me.Pnl_UsuariosPerfil.Controls.Add(Me.Btn_QuitarUsuario)
        Me.Pnl_UsuariosPerfil.Controls.Add(Me.Btn_AsociarUsuario)
        Me.Pnl_UsuariosPerfil.Controls.Add(Me.C1SuperLabel16)
        Me.Pnl_UsuariosPerfil.ForeColor = System.Drawing.Color.DimGray
        Me.Pnl_UsuariosPerfil.Location = New System.Drawing.Point(353, 23)
        Me.Pnl_UsuariosPerfil.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.Pnl_UsuariosPerfil.Name = "Pnl_UsuariosPerfil"
        Me.Pnl_UsuariosPerfil.Size = New System.Drawing.Size(406, 417)
        Me.Pnl_UsuariosPerfil.TabIndex = 1027
        Me.Pnl_UsuariosPerfil.TabStop = True
        '
        'Cmb_NegocioUsuarios
        '
        Me.Cmb_NegocioUsuarios.AddItemSeparator = Global.Microsoft.VisualBasic.ChrW(59)
        Me.Cmb_NegocioUsuarios.AllowColMove = False
        Me.Cmb_NegocioUsuarios.AllowSort = False
        Me.Cmb_NegocioUsuarios.AutoDropDown = True
        Me.Cmb_NegocioUsuarios.AutoSelect = True
        Me.Cmb_NegocioUsuarios.AutoSize = False
        Me.Cmb_NegocioUsuarios.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Cmb_NegocioUsuarios.Caption = ""
        Me.Cmb_NegocioUsuarios.CaptionHeight = 18
        Me.Cmb_NegocioUsuarios.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.Cmb_NegocioUsuarios.ColumnCaptionHeight = 18
        Me.Cmb_NegocioUsuarios.ColumnFooterHeight = 18
        Me.Cmb_NegocioUsuarios.ColumnHeaders = False
        Me.Cmb_NegocioUsuarios.ComboStyle = C1.Win.C1List.ComboStyleEnum.DropdownList
        Me.Cmb_NegocioUsuarios.ContentHeight = 12
        Me.Cmb_NegocioUsuarios.DeadAreaBackColor = System.Drawing.Color.Empty
        Me.Cmb_NegocioUsuarios.EditorBackColor = System.Drawing.Color.White
        Me.Cmb_NegocioUsuarios.EditorFont = New System.Drawing.Font("Arial", 7.0!, System.Drawing.FontStyle.Bold)
        Me.Cmb_NegocioUsuarios.EditorForeColor = System.Drawing.Color.SteelBlue
        Me.Cmb_NegocioUsuarios.EditorHeight = 12
        Me.Cmb_NegocioUsuarios.ExtendRightColumn = True
        Me.Cmb_NegocioUsuarios.FlatStyle = C1.Win.C1List.FlatModeEnum.Flat
        Me.Cmb_NegocioUsuarios.Font = New System.Drawing.Font("Arial", 7.0!, System.Drawing.FontStyle.Bold)
        Me.Cmb_NegocioUsuarios.Images.Add(CType(resources.GetObject("Cmb_NegocioUsuarios.Images"), System.Drawing.Image))
        Me.Cmb_NegocioUsuarios.ItemHeight = 18
        Me.Cmb_NegocioUsuarios.Location = New System.Drawing.Point(6, 20)
        Me.Cmb_NegocioUsuarios.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Cmb_NegocioUsuarios.MatchEntry = C1.Win.C1List.MatchEntryEnum.Extended
        Me.Cmb_NegocioUsuarios.MatchEntryTimeout = CType(500, Long)
        Me.Cmb_NegocioUsuarios.MaxDropDownItems = CType(5, Short)
        Me.Cmb_NegocioUsuarios.MaxLength = 0
        Me.Cmb_NegocioUsuarios.MinimumSize = New System.Drawing.Size(10, 16)
        Me.Cmb_NegocioUsuarios.MouseCursor = System.Windows.Forms.Cursors.Default
        Me.Cmb_NegocioUsuarios.Name = "Cmb_NegocioUsuarios"
        Me.Cmb_NegocioUsuarios.RowSubDividerColor = System.Drawing.Color.DarkGray
        Me.Cmb_NegocioUsuarios.Size = New System.Drawing.Size(395, 16)
        Me.Cmb_NegocioUsuarios.TabIndex = 0
        Me.Cmb_NegocioUsuarios.VisualStyle = C1.Win.C1List.VisualStyle.Office2007Silver
        Me.Cmb_NegocioUsuarios.PropBag = resources.GetString("Cmb_NegocioUsuarios.PropBag")
        '
        'Tdg_UsuariosDisponibles
        '
        Me.Tdg_UsuariosDisponibles.AllowColSelect = False
        Me.Tdg_UsuariosDisponibles.AllowRowSizing = C1.Win.C1TrueDBGrid.RowSizingEnum.None
        Me.Tdg_UsuariosDisponibles.AllowUpdate = False
        Me.Tdg_UsuariosDisponibles.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Tdg_UsuariosDisponibles.Caption = "Usuarios Disponibles"
        Me.Tdg_UsuariosDisponibles.CaptionHeight = 21
        Me.Tdg_UsuariosDisponibles.ColumnHeaders = False
        Me.Tdg_UsuariosDisponibles.DirectionAfterEnter = C1.Win.C1TrueDBGrid.DirectionAfterEnterEnum.MoveDown
        Me.Tdg_UsuariosDisponibles.Font = New System.Drawing.Font("Arial", 7.5!)
        Me.Tdg_UsuariosDisponibles.ForeColor = System.Drawing.Color.Lavender
        Me.Tdg_UsuariosDisponibles.GroupByAreaVisible = False
        Me.Tdg_UsuariosDisponibles.GroupByCaption = "Arrastre hasta aquí las columnas por las que desea agrupar"
        Me.Tdg_UsuariosDisponibles.Images.Add(CType(resources.GetObject("Tdg_UsuariosDisponibles.Images"), System.Drawing.Image))
        Me.Tdg_UsuariosDisponibles.Location = New System.Drawing.Point(217, 43)
        Me.Tdg_UsuariosDisponibles.MaintainRowCurrency = True
        Me.Tdg_UsuariosDisponibles.Margin = New System.Windows.Forms.Padding(0, 3, 0, 0)
        Me.Tdg_UsuariosDisponibles.MarqueeStyle = C1.Win.C1TrueDBGrid.MarqueeEnum.HighlightRow
        Me.Tdg_UsuariosDisponibles.MultiSelect = C1.Win.C1TrueDBGrid.MultiSelectEnum.Simple
        Me.Tdg_UsuariosDisponibles.Name = "Tdg_UsuariosDisponibles"
        Me.Tdg_UsuariosDisponibles.PreviewInfo.Location = New System.Drawing.Point(0, 0)
        Me.Tdg_UsuariosDisponibles.PreviewInfo.Size = New System.Drawing.Size(0, 0)
        Me.Tdg_UsuariosDisponibles.PreviewInfo.ZoomFactor = 75
        Me.Tdg_UsuariosDisponibles.PrintInfo.PageSettings = CType(resources.GetObject("Tdg_UsuariosDisponibles.PrintInfo.PageSettings"), System.Drawing.Printing.PageSettings)
        Me.Tdg_UsuariosDisponibles.RecordSelectors = False
        Me.Tdg_UsuariosDisponibles.RecordSelectorWidth = 15
        Me.Tdg_UsuariosDisponibles.RowDivider.Color = System.Drawing.Color.DarkGray
        Me.Tdg_UsuariosDisponibles.RowDivider.Style = C1.Win.C1TrueDBGrid.LineStyleEnum.Raised
        Me.Tdg_UsuariosDisponibles.RowHeight = 15
        Me.Tdg_UsuariosDisponibles.ScrollTips = True
        Me.Tdg_UsuariosDisponibles.Size = New System.Drawing.Size(189, 374)
        Me.Tdg_UsuariosDisponibles.SplitDividerSize = New System.Drawing.Size(0, 0)
        Me.Tdg_UsuariosDisponibles.TabIndex = 0
        Me.Tdg_UsuariosDisponibles.TabStop = False
        Me.Tdg_UsuariosDisponibles.Text = "C1TrueDBGrid2"
        Me.Tdg_UsuariosDisponibles.PropBag = resources.GetString("Tdg_UsuariosDisponibles.PropBag")
        '
        'Tdg_UsuarioAsignados
        '
        Me.Tdg_UsuarioAsignados.AllowColSelect = False
        Me.Tdg_UsuarioAsignados.AllowRowSizing = C1.Win.C1TrueDBGrid.RowSizingEnum.None
        Me.Tdg_UsuarioAsignados.AllowUpdate = False
        Me.Tdg_UsuarioAsignados.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Tdg_UsuarioAsignados.Caption = "Usuarios Asociados"
        Me.Tdg_UsuarioAsignados.CaptionHeight = 21
        Me.Tdg_UsuarioAsignados.ColumnHeaders = False
        Me.Tdg_UsuarioAsignados.DirectionAfterEnter = C1.Win.C1TrueDBGrid.DirectionAfterEnterEnum.MoveDown
        Me.Tdg_UsuarioAsignados.FlatStyle = C1.Win.C1TrueDBGrid.FlatModeEnum.Standard
        Me.Tdg_UsuarioAsignados.Font = New System.Drawing.Font("Arial", 7.5!)
        Me.Tdg_UsuarioAsignados.ForeColor = System.Drawing.Color.Lavender
        Me.Tdg_UsuarioAsignados.GroupByAreaVisible = False
        Me.Tdg_UsuarioAsignados.GroupByCaption = "Arrastre hasta aquí las columnas por las que desea agrupar"
        Me.Tdg_UsuarioAsignados.Images.Add(CType(resources.GetObject("Tdg_UsuarioAsignados.Images"), System.Drawing.Image))
        Me.Tdg_UsuarioAsignados.Location = New System.Drawing.Point(2, 43)
        Me.Tdg_UsuarioAsignados.MaintainRowCurrency = True
        Me.Tdg_UsuarioAsignados.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.Tdg_UsuarioAsignados.MarqueeStyle = C1.Win.C1TrueDBGrid.MarqueeEnum.HighlightRow
        Me.Tdg_UsuarioAsignados.MultiSelect = C1.Win.C1TrueDBGrid.MultiSelectEnum.Simple
        Me.Tdg_UsuarioAsignados.Name = "Tdg_UsuarioAsignados"
        Me.Tdg_UsuarioAsignados.PreviewInfo.Location = New System.Drawing.Point(0, 0)
        Me.Tdg_UsuarioAsignados.PreviewInfo.Size = New System.Drawing.Size(0, 0)
        Me.Tdg_UsuarioAsignados.PreviewInfo.ZoomFactor = 75
        Me.Tdg_UsuarioAsignados.PrintInfo.PageSettings = CType(resources.GetObject("Tdg_UsuarioAsignados.PrintInfo.PageSettings"), System.Drawing.Printing.PageSettings)
        Me.Tdg_UsuarioAsignados.RecordSelectors = False
        Me.Tdg_UsuarioAsignados.RecordSelectorWidth = 15
        Me.Tdg_UsuarioAsignados.RowDivider.Color = System.Drawing.Color.DarkGray
        Me.Tdg_UsuarioAsignados.RowDivider.Style = C1.Win.C1TrueDBGrid.LineStyleEnum.Raised
        Me.Tdg_UsuarioAsignados.RowHeight = 15
        Me.Tdg_UsuarioAsignados.ScrollTips = True
        Me.Tdg_UsuarioAsignados.Size = New System.Drawing.Size(189, 374)
        Me.Tdg_UsuarioAsignados.SplitDividerSize = New System.Drawing.Size(0, 0)
        Me.Tdg_UsuarioAsignados.TabIndex = 0
        Me.Tdg_UsuarioAsignados.TabStop = False
        Me.Tdg_UsuarioAsignados.Text = "C1TrueDBGrid1"
        Me.Tdg_UsuarioAsignados.PropBag = resources.GetString("Tdg_UsuarioAsignados.PropBag")
        '
        'Btn_QuitarUsuario
        '
        Me.Btn_QuitarUsuario.BackColor = System.Drawing.Color.Gainsboro
        Me.Btn_QuitarUsuario.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.Btn_QuitarUsuario.Font = New System.Drawing.Font("Arial", 7.5!)
        Me.Btn_QuitarUsuario.ForeColor = System.Drawing.Color.MediumBlue
        Me.Btn_QuitarUsuario.Location = New System.Drawing.Point(194, 211)
        Me.Btn_QuitarUsuario.Margin = New System.Windows.Forms.Padding(2)
        Me.Btn_QuitarUsuario.Name = "Btn_QuitarUsuario"
        Me.Btn_QuitarUsuario.Size = New System.Drawing.Size(20, 20)
        Me.Btn_QuitarUsuario.TabIndex = 2
        Me.Btn_QuitarUsuario.TabStop = False
        Me.Btn_QuitarUsuario.Tag = ""
        Me.Btn_QuitarUsuario.Text = "►"
        Me.Btn_QuitarUsuario.UseVisualStyleBackColor = True
        Me.Btn_QuitarUsuario.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Silver
        '
        'Btn_AsociarUsuario
        '
        Me.Btn_AsociarUsuario.BackColor = System.Drawing.Color.Gainsboro
        Me.Btn_AsociarUsuario.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.Btn_AsociarUsuario.Font = New System.Drawing.Font("Arial", 7.5!)
        Me.Btn_AsociarUsuario.ForeColor = System.Drawing.Color.MediumBlue
        Me.Btn_AsociarUsuario.Location = New System.Drawing.Point(194, 187)
        Me.Btn_AsociarUsuario.Margin = New System.Windows.Forms.Padding(2)
        Me.Btn_AsociarUsuario.Name = "Btn_AsociarUsuario"
        Me.Btn_AsociarUsuario.Size = New System.Drawing.Size(20, 20)
        Me.Btn_AsociarUsuario.TabIndex = 1
        Me.Btn_AsociarUsuario.TabStop = False
        Me.Btn_AsociarUsuario.Tag = ""
        Me.Btn_AsociarUsuario.Text = "◄"
        Me.Btn_AsociarUsuario.UseVisualStyleBackColor = True
        Me.Btn_AsociarUsuario.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Silver
        '
        'Btn_Salir
        '
        Me.Btn_Salir.BackColor = System.Drawing.Color.Transparent
        Me.Btn_Salir.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.Btn_Salir.FlatAppearance.BorderSize = 0
        Me.Btn_Salir.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray
        Me.Btn_Salir.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray
        Me.Btn_Salir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Btn_Salir.Font = New System.Drawing.Font("Arial", 8.25!)
        Me.Btn_Salir.ForeColor = System.Drawing.Color.White
        Me.Btn_Salir.Location = New System.Drawing.Point(708, 0)
        Me.Btn_Salir.Margin = New System.Windows.Forms.Padding(2)
        Me.Btn_Salir.Name = "Btn_Salir"
        Me.Btn_Salir.Size = New System.Drawing.Size(53, 22)
        Me.Btn_Salir.TabIndex = 1030
        Me.Btn_Salir.TabStop = False
        Me.Btn_Salir.Text = "&Salir  |x|"
        Me.Btn_Salir.UseVisualStyleBackColor = False
        '
        'C1SuperTooltip1
        '
        Me.C1SuperTooltip1.Font = New System.Drawing.Font("Tahoma", 8.0!)
        Me.C1SuperTooltip1.RoundedCorners = True
        '
        'StatusStrip1
        '
        Me.StatusStrip1.AutoSize = False
        Me.StatusStrip1.BackColor = System.Drawing.Color.DarkGray
        Me.StatusStrip1.Dock = System.Windows.Forms.DockStyle.None
        Me.StatusStrip1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.StatusLabel})
        Me.StatusStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Flow
        Me.StatusStrip1.Location = New System.Drawing.Point(1, 440)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(758, 18)
        Me.StatusStrip1.Stretch = False
        Me.StatusStrip1.TabIndex = 1031
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'StatusLabel
        '
        Me.StatusLabel.BackColor = System.Drawing.Color.Transparent
        Me.StatusLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Bold)
        Me.StatusLabel.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.StatusLabel.Name = "StatusLabel"
        Me.StatusLabel.Size = New System.Drawing.Size(128, 14)
        Me.StatusLabel.Text = "ToolStripStatusLabel1"
        '
        'Ctrl_Botonera
        '
        Me.Ctrl_Botonera.BackColor = System.Drawing.Color.DarkGray
        Me.Ctrl_Botonera.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Ctrl_Botonera_Agregar, Me.Ctrl_Botonera_Buscar, Me.ToolStripSeparator1, Me.Ctrl_Botonera_Guardar, Me.Ctrl_Botonera_Editar, Me.Ctrl_Botonera_Eliminar, Me.Ctrl_Botonera_Verificar, Me.Ctrl_Botonera_Procesar, Me.ToolStripSeparator2, Me.Ctrl_Botonera_Imprimir, Me.Ctrl_Botonera_Excel, Me.Ctrl_Botonera_Columnas, Me.ToolStripSeparator3, Me.Ctrl_Botonera_Cancelar, Me.ToolStripSeparator4, Me.Ctrl_Combo_Plantillas})
        Me.Ctrl_Botonera.Location = New System.Drawing.Point(0, 0)
        Me.Ctrl_Botonera.Name = "Ctrl_Botonera"
        Me.Ctrl_Botonera.Size = New System.Drawing.Size(760, 25)
        Me.Ctrl_Botonera.TabIndex = 1070
        Me.Ctrl_Botonera.Text = "ToolStrip1"
        '
        'Ctrl_Botonera_Agregar
        '
        Me.Ctrl_Botonera_Agregar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Ctrl_Botonera_Agregar.Image = CType(resources.GetObject("Ctrl_Botonera_Agregar.Image"), System.Drawing.Image)
        Me.Ctrl_Botonera_Agregar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.Ctrl_Botonera_Agregar.Name = "Ctrl_Botonera_Agregar"
        Me.Ctrl_Botonera_Agregar.Size = New System.Drawing.Size(23, 22)
        Me.Ctrl_Botonera_Agregar.Text = "ToolStripButton2"
        Me.Ctrl_Botonera_Agregar.ToolTipText = "Agregar"
        '
        'Ctrl_Botonera_Buscar
        '
        Me.Ctrl_Botonera_Buscar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Ctrl_Botonera_Buscar.Enabled = False
        Me.Ctrl_Botonera_Buscar.Image = CType(resources.GetObject("Ctrl_Botonera_Buscar.Image"), System.Drawing.Image)
        Me.Ctrl_Botonera_Buscar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.Ctrl_Botonera_Buscar.Name = "Ctrl_Botonera_Buscar"
        Me.Ctrl_Botonera_Buscar.Size = New System.Drawing.Size(23, 22)
        Me.Ctrl_Botonera_Buscar.Text = "Buscar"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'Ctrl_Botonera_Guardar
        '
        Me.Ctrl_Botonera_Guardar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Ctrl_Botonera_Guardar.Image = CType(resources.GetObject("Ctrl_Botonera_Guardar.Image"), System.Drawing.Image)
        Me.Ctrl_Botonera_Guardar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.Ctrl_Botonera_Guardar.Name = "Ctrl_Botonera_Guardar"
        Me.Ctrl_Botonera_Guardar.Size = New System.Drawing.Size(23, 22)
        Me.Ctrl_Botonera_Guardar.Text = "Guardar"
        '
        'Ctrl_Botonera_Editar
        '
        Me.Ctrl_Botonera_Editar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Ctrl_Botonera_Editar.Image = CType(resources.GetObject("Ctrl_Botonera_Editar.Image"), System.Drawing.Image)
        Me.Ctrl_Botonera_Editar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.Ctrl_Botonera_Editar.Name = "Ctrl_Botonera_Editar"
        Me.Ctrl_Botonera_Editar.Size = New System.Drawing.Size(23, 22)
        Me.Ctrl_Botonera_Editar.Text = "Editar"
        '
        'Ctrl_Botonera_Eliminar
        '
        Me.Ctrl_Botonera_Eliminar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Ctrl_Botonera_Eliminar.Image = CType(resources.GetObject("Ctrl_Botonera_Eliminar.Image"), System.Drawing.Image)
        Me.Ctrl_Botonera_Eliminar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.Ctrl_Botonera_Eliminar.Name = "Ctrl_Botonera_Eliminar"
        Me.Ctrl_Botonera_Eliminar.Size = New System.Drawing.Size(23, 22)
        Me.Ctrl_Botonera_Eliminar.Text = "Eliminar"
        '
        'Ctrl_Botonera_Verificar
        '
        Me.Ctrl_Botonera_Verificar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Ctrl_Botonera_Verificar.Enabled = False
        Me.Ctrl_Botonera_Verificar.Image = CType(resources.GetObject("Ctrl_Botonera_Verificar.Image"), System.Drawing.Image)
        Me.Ctrl_Botonera_Verificar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.Ctrl_Botonera_Verificar.Name = "Ctrl_Botonera_Verificar"
        Me.Ctrl_Botonera_Verificar.Size = New System.Drawing.Size(23, 22)
        Me.Ctrl_Botonera_Verificar.Text = "Verificar"
        '
        'Ctrl_Botonera_Procesar
        '
        Me.Ctrl_Botonera_Procesar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Ctrl_Botonera_Procesar.Enabled = False
        Me.Ctrl_Botonera_Procesar.Image = CType(resources.GetObject("Ctrl_Botonera_Procesar.Image"), System.Drawing.Image)
        Me.Ctrl_Botonera_Procesar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.Ctrl_Botonera_Procesar.Name = "Ctrl_Botonera_Procesar"
        Me.Ctrl_Botonera_Procesar.Size = New System.Drawing.Size(23, 22)
        Me.Ctrl_Botonera_Procesar.Text = "Procesar"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'Ctrl_Botonera_Imprimir
        '
        Me.Ctrl_Botonera_Imprimir.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Ctrl_Botonera_Imprimir.Enabled = False
        Me.Ctrl_Botonera_Imprimir.Image = CType(resources.GetObject("Ctrl_Botonera_Imprimir.Image"), System.Drawing.Image)
        Me.Ctrl_Botonera_Imprimir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.Ctrl_Botonera_Imprimir.Name = "Ctrl_Botonera_Imprimir"
        Me.Ctrl_Botonera_Imprimir.Size = New System.Drawing.Size(23, 22)
        Me.Ctrl_Botonera_Imprimir.Text = "Imprimir"
        Me.Ctrl_Botonera_Imprimir.ToolTipText = "Imprimir"
        '
        'Ctrl_Botonera_Excel
        '
        Me.Ctrl_Botonera_Excel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Ctrl_Botonera_Excel.Enabled = False
        Me.Ctrl_Botonera_Excel.Image = CType(resources.GetObject("Ctrl_Botonera_Excel.Image"), System.Drawing.Image)
        Me.Ctrl_Botonera_Excel.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.Ctrl_Botonera_Excel.Name = "Ctrl_Botonera_Excel"
        Me.Ctrl_Botonera_Excel.Size = New System.Drawing.Size(23, 22)
        Me.Ctrl_Botonera_Excel.Text = "Exportar a Excel"
        '
        'Ctrl_Botonera_Columnas
        '
        Me.Ctrl_Botonera_Columnas.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Ctrl_Botonera_Columnas.Enabled = False
        Me.Ctrl_Botonera_Columnas.Image = CType(resources.GetObject("Ctrl_Botonera_Columnas.Image"), System.Drawing.Image)
        Me.Ctrl_Botonera_Columnas.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.Ctrl_Botonera_Columnas.Name = "Ctrl_Botonera_Columnas"
        Me.Ctrl_Botonera_Columnas.Size = New System.Drawing.Size(23, 22)
        Me.Ctrl_Botonera_Columnas.Text = "Configuración Columnas"
        Me.Ctrl_Botonera_Columnas.ToolTipText = "Configuración Columnas"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(6, 25)
        '
        'Ctrl_Botonera_Cancelar
        '
        Me.Ctrl_Botonera_Cancelar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Ctrl_Botonera_Cancelar.Image = CType(resources.GetObject("Ctrl_Botonera_Cancelar.Image"), System.Drawing.Image)
        Me.Ctrl_Botonera_Cancelar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.Ctrl_Botonera_Cancelar.Name = "Ctrl_Botonera_Cancelar"
        Me.Ctrl_Botonera_Cancelar.Size = New System.Drawing.Size(23, 22)
        Me.Ctrl_Botonera_Cancelar.Text = "Cancelar"
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(6, 25)
        '
        'Ctrl_Combo_Plantillas
        '
        Me.Ctrl_Combo_Plantillas.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.Ctrl_Combo_Plantillas.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Ctrl_Combo_Plantillas.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Ctrl_Combo_Plantillas.Name = "Ctrl_Combo_Plantillas"
        Me.Ctrl_Combo_Plantillas.Size = New System.Drawing.Size(121, 25)
        Me.Ctrl_Combo_Plantillas.Visible = False
        '
        'Frm_MantenedorPerfilesUsuarios
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.DimGray
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.ClientSize = New System.Drawing.Size(760, 459)
        Me.Controls.Add(Me.Ctrl_Botonera)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.Btn_Salir)
        Me.Controls.Add(Me.Pnl_UsuariosPerfil)
        Me.Controls.Add(Me.Pnl_Datos_TipoOperacion)
        Me.Controls.Add(Me.Pnl_DatosPerfil)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Arial", 7.5!)
        Me.ForeColor = System.Drawing.Color.DimGray
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.MaximizeBox = False
        Me.Name = "Frm_MantenedorPerfilesUsuarios"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Mantenedor de Perfiles de Usuario"
        Me.Pnl_DatosPerfil.ResumeLayout(False)
        Me.Pnl_DatosPerfil.PerformLayout()
        CType(Me.Cmb_EstadoPerfil, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Txt_CodigoExternoPerfil, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Txt_NombreCortoPerfil, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Txt_NombrePerfil, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Pnl_Datos_TipoOperacion.ResumeLayout(False)
        CType(Me.Tdg_PerfilesUsuario, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Pnl_UsuariosPerfil.ResumeLayout(False)
        CType(Me.Cmb_NegocioUsuarios, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Tdg_UsuariosDisponibles, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Tdg_UsuarioAsignados, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.Ctrl_Botonera.ResumeLayout(False)
        Me.Ctrl_Botonera.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Pnl_DatosPerfil As System.Windows.Forms.Panel
    Friend WithEvents Txt_CodigoExternoPerfil As C1.Win.C1Input.C1TextBox
    Friend WithEvents Txt_NombreCortoPerfil As C1.Win.C1Input.C1TextBox
    Friend WithEvents Txt_NombrePerfil As C1.Win.C1Input.C1TextBox
    Friend WithEvents C1SuperLabel8 As C1.Win.C1SuperTooltip.C1SuperLabel
    Friend WithEvents C1SuperLabel5 As C1.Win.C1SuperTooltip.C1SuperLabel
    Friend WithEvents C1SuperLabel11 As C1.Win.C1SuperTooltip.C1SuperLabel
    Friend WithEvents C1SuperLabel22 As C1.Win.C1SuperTooltip.C1SuperLabel
    Friend WithEvents Pnl_Datos_TipoOperacion As System.Windows.Forms.Panel
    Friend WithEvents Tdg_PerfilesUsuario As C1.Win.C1TrueDBGrid.C1TrueDBGrid
    Friend WithEvents Btn_Accion As C1.Win.C1Input.C1Button
    Friend WithEvents Btn_Modificar As C1.Win.C1Input.C1Button
    Friend WithEvents Btn_Agregar As C1.Win.C1Input.C1Button
    Friend WithEvents Btn_Eliminar As C1.Win.C1Input.C1Button
    Friend WithEvents Pnl_UsuariosPerfil As System.Windows.Forms.Panel
    Friend WithEvents C1SuperLabel16 As C1.Win.C1SuperTooltip.C1SuperLabel
    Friend WithEvents Btn_Salir As C1.Win.C1Input.C1Button
    Friend WithEvents Btn_Cancelar As C1.Win.C1Input.C1Button
    Friend WithEvents Cmb_EstadoPerfil As C1.Win.C1List.C1Combo
    Friend WithEvents Cmb_NegocioUsuarios As C1.Win.C1List.C1Combo
    Friend WithEvents C1SuperTooltip1 As C1.Win.C1SuperTooltip.C1SuperTooltip
    Friend WithEvents Tdg_UsuariosDisponibles As C1.Win.C1TrueDBGrid.C1TrueDBGrid
    Friend WithEvents Tdg_UsuarioAsignados As C1.Win.C1TrueDBGrid.C1TrueDBGrid
    Friend WithEvents Btn_QuitarUsuario As C1.Win.C1Input.C1Button
    Friend WithEvents Btn_AsociarUsuario As C1.Win.C1Input.C1Button
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents StatusLabel As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents Ctrl_Botonera As System.Windows.Forms.ToolStrip
    Friend WithEvents Ctrl_Botonera_Agregar As System.Windows.Forms.ToolStripButton
    Friend WithEvents Ctrl_Botonera_Buscar As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents Ctrl_Botonera_Guardar As System.Windows.Forms.ToolStripButton
    Friend WithEvents Ctrl_Botonera_Editar As System.Windows.Forms.ToolStripButton
    Friend WithEvents Ctrl_Botonera_Eliminar As System.Windows.Forms.ToolStripButton
    Friend WithEvents Ctrl_Botonera_Verificar As System.Windows.Forms.ToolStripButton
    Friend WithEvents Ctrl_Botonera_Procesar As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents Ctrl_Botonera_Imprimir As System.Windows.Forms.ToolStripButton
    Friend WithEvents Ctrl_Botonera_Excel As System.Windows.Forms.ToolStripButton
    Friend WithEvents Ctrl_Botonera_Columnas As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents Ctrl_Botonera_Cancelar As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents Ctrl_Combo_Plantillas As System.Windows.Forms.ToolStripComboBox
End Class
