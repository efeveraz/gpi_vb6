﻿Imports C1.Win.C1TrueDBGrid
Imports C1.Win.C1List.C1Combo
Public Class Frm_MantenedorTipoUsuario
    Dim ltxtColumnas As String = ""
    Dim ltxtOperacion As String = ""
    Dim lstrResultadoTransaccion As String = ""

    Dim lobjTipoUsuario As ClsTipoUsuario = New ClsTipoUsuario
    Dim DS_TipoUsuario As DataSet

    Dim lblnMoviendoElFormulario As Boolean = False
    Dim lMouseCoordenadaX As Integer = 0
    Dim lMouseCoordenadaY As Integer = 0

#Region " --- Formulario ---"

    Public Sub CargarFormulario()

        'Me.Location = New Point(gobjFormulario._PosX, gobjFormulario._PosY)
        Me.Location = New Point(3, 3)
        If gobjFormulario._Modal Then
            ShowDialog()
        Else
            Show()
            BringToFront()
        End If
    End Sub
    Private Sub AbrirFormulario(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call CargaGrillaTipoUsuario()
        Call InicializaFormulario()
    End Sub
    Private Sub Frm_MantenedorTipoUsuario_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.GotFocus, Me.Click, Me.Click
        Me.BringToFront()
    End Sub
    Private Sub Me_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If (e.KeyCode = Keys.F4) And e.Modifiers = Keys.Alt Then
            e.Handled = True
            Call Btn_Salir_Click(sender, e)
        ElseIf e.KeyCode = Keys.Enter Then
            e.Handled = True
            SendKeys.Send("{TAB}")
        ElseIf e.KeyCode = Keys.Escape Then
            e.Handled = True
            Call Btn_Cancelar_Click(sender, e)
        End If
    End Sub

    Private Sub Me_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.DoubleClick, Me.DoubleClick
        Me.Location = New Point(3, 3)
    End Sub
    Private Sub Me_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Me.Dispose()
    End Sub
    Private Sub Btn_Salir_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Salir.Click
        Me.Close()
    End Sub
    Private Sub Formulario_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles Me.MouseDown, Me.MouseDown
        If e.Button = MouseButtons.Left Then
            lblnMoviendoElFormulario = True
            lMouseCoordenadaX = e.X
            lMouseCoordenadaY = e.Y
        End If
    End Sub
    Private Sub Formulario_MouseUp(ByVal sender As Object, ByVal e As MouseEventArgs) Handles Me.MouseUp, Me.MouseUp
        If e.Button = MouseButtons.Left Then
            lblnMoviendoElFormulario = False
        End If
    End Sub
    Private Sub Formulario_MouseMove(ByVal sender As Object, ByVal e As MouseEventArgs) Handles Me.MouseMove, Me.MouseMove
        If lblnMoviendoElFormulario Then
            '...Arriba
            If Me.Location.Y < 1 Then
                Me.Location = New Point(Me.Location.X, 1)
                lblnMoviendoElFormulario = False
                '...Abajo
            ElseIf Me.Location.Y > (sender.parent.Height - 30) Then
                Me.Location = New Point(Me.Location.X, sender.parent.Height - 30)
                lblnMoviendoElFormulario = False
                '...Izquierda
            ElseIf Me.Location.X < ((Me.Width / 10) - Me.Width) Then
                Me.Location = New Point(((Me.Width / 10) - Me.Width), Me.Location.Y)
                lblnMoviendoElFormulario = False
                '...Derecha
            ElseIf Me.Location.X > (sender.parent.Width - 150) Then
                Me.Location = New Point((sender.parent.Width - 150), Me.Location.Y)
                lblnMoviendoElFormulario = False
            Else
                lblnMoviendoElFormulario = True
                Dim temp As Point = New Point()
                temp.X = Me.Location.X + (e.X - lMouseCoordenadaX)
                temp.Y = Me.Location.Y + (e.Y - lMouseCoordenadaY)
                Me.Location = temp
                temp = Nothing
            End If
        End If
    End Sub

#End Region

#Region " --- Eventos Formulario ---"

    Private Sub CargaEstado()

        Cmb_EstadoTipoUsuario.ClearItems()
        Cmb_EstadoTipoUsuario.AddItem("VIG;VIGENTE")
        Cmb_EstadoTipoUsuario.AddItem("BLO;BLOQUEADO")
        Cmb_EstadoTipoUsuario.AddItem("ELI;ELIMINADO")

        Cmb_EstadoTipoUsuario.Splits(0).DisplayColumns(0).Visible = False
        Cmb_EstadoTipoUsuario.Splits(0).DisplayColumns(1).Visible = True
    End Sub

    Private Sub InicializaFormulario()
        Call CargaEstado()
        ltxtOperacion = ""

        Call HabilitaDeshabilitaDatos()

        Cmb_EstadoTipoUsuario.SelectedIndex = -1

        Limpiar_Campos()

        Tdg_TipoUsuario.SelectedRows.Clear()
        Tdg_TipoUsuario.Row = -1

        'Btn_Agregar.Enabled = True
        Ctrl_Botonera_Agregar.Enabled = True

        'Btn_Accion.Enabled = False
        Ctrl_Botonera_Guardar.Enabled = False

        'Btn_Cancelar.Enabled = False
        Ctrl_Botonera_Cancelar.Enabled = True

        'Btn_Modificar.Enabled = False
        Ctrl_Botonera_Editar.Enabled = False

        'Btn_Eliminar.Enabled = False
        Ctrl_Botonera_Eliminar.Enabled = False

        Tdg_TipoUsuario.Focus()
    End Sub
    Private Sub HabilitaDeshabilitaDatos()
        Select Case ltxtOperacion.Trim
            Case Is = "INSERTAR"
                gSubHabilitaControl(Txt_NombreTipoUsuario, False, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Txt_NombreCortoTipoUsuario, False, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Txt_CodigoExternoTipoUsuario, False, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Cmb_EstadoTipoUsuario, True, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
            Case Is = "MODIFICAR"
                gSubHabilitaControl(Txt_NombreTipoUsuario, False, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Txt_NombreCortoTipoUsuario, False, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Txt_CodigoExternoTipoUsuario, False, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Cmb_EstadoTipoUsuario, False, gColorBackGroundEnabled, gColorBackGroundDesabled, True)

            Case Is = "ELIMINAR", ""
                gSubHabilitaControl(Txt_NombreTipoUsuario, True, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Txt_NombreCortoTipoUsuario, True, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Txt_CodigoExternoTipoUsuario, True, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Cmb_EstadoTipoUsuario, True, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
        End Select
    End Sub
    Public Sub Limpiar_Campos()
        Txt_NombreTipoUsuario.Text = Nothing
        Txt_NombreCortoTipoUsuario.Text = Nothing
        Txt_CodigoExternoTipoUsuario.Text = Nothing
        Cmb_EstadoTipoUsuario.SelectedIndex = -1
    End Sub
    Private Function ValidarCamposOK() As Boolean
        Try
            Dim lbnlSeteaFoco As Boolean = True
            Dim lstrMensajeResumen As String = ""

            gFunValidaValorControl("Falta ingresar Nombre.", Txt_NombreTipoUsuario, lstrMensajeResumen, lbnlSeteaFoco, True, , , False)
            gFunValidaValorControl("Falta ingresar Nombre Corto.", Txt_NombreCortoTipoUsuario, lstrMensajeResumen, lbnlSeteaFoco, True, , , False)
            gFunValidaValorControl("Falta ingresar el Código Externo de TipoUsuario.", Txt_CodigoExternoTipoUsuario, lstrMensajeResumen, lbnlSeteaFoco, True, , , False)
            gFunValidaValorControl("Falta Seleccionar el Estado.", Cmb_EstadoTipoUsuario, lstrMensajeResumen, lbnlSeteaFoco, True, , , False)

            If lstrMensajeResumen.Trim <> "" Then
                MsgBox(lstrMensajeResumen, MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation, gtxtNombreSistema & " Mantenedor de Tipo Usuario.")
                ValidarCamposOK = False
            Else
                ValidarCamposOK = True
            End If
        Catch ex As Exception
            ValidarCamposOK = False
        End Try
    End Function

#End Region

#Region " --- Grilla TipoUsuarioes ---"

    Private Sub CargaGrillaTipoUsuario()

        ltxtColumnas = "ID_TIPO_USUARIO, NOMBRE_TIPO_USUARIO, NOMBRE_CORTO_TIPO_USUARIO, CODIGO_EXTERNO_TIPO_USUARIO, ESTADO_TIPO_USUARIO"
        lstrResultadoTransaccion = ""

        DS_TipoUsuario = lobjTipoUsuario.RetornaTiposUsuarios(lstrResultadoTransaccion, ltxtColumnas, 0, "", "", "", "")

        If lstrResultadoTransaccion = "OK" Then
            If (DS_TipoUsuario.Tables(0).Rows.Count >= 0) Then
                Tdg_TipoUsuario.DataSource = DS_TipoUsuario.Tables(0)
                Call FormateaGrillaTipoUsuario()
                Tdg_TipoUsuario.Row = -1
                Call MuestraDatosDesdeGrilla()
                Tdg_TipoUsuario.Focus()
                StatusLabel.Text = gtxtNombreSistema & " Cantidad de tipos de usuarios: " & Tdg_TipoUsuario.RowCount.ToString
            End If
        Else
            MsgBox("Se produjo un error al tratar de mostrar el listado de Tipos de Usuarios, intente abrir nuevamente el mantenedor.", gtxtNombreSistema & " Mantenedor de Tipo Usuario.")
            StatusLabel.Text = gtxtNombreSistema & " Cantidad de tipos de usuarios: 0"
        End If
    End Sub
    Sub FormateaGrillaTipoUsuario()
        Dim LInt_Col As Integer
        Dim Tdg_Grilla As C1TrueDBGrid
        Tdg_Grilla = Tdg_TipoUsuario
        With Tdg_Grilla

            LInt_Col = 0 'ID_TIPO_USUARIO
            .Columns(LInt_Col).Caption = "Id TipoUsuario"
            .Splits(0).DisplayColumns(LInt_Col).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Near
            .Splits(0).DisplayColumns(LInt_Col).Width = 0
            .Columns(LInt_Col).DataField = "ID_TIPO_USUARIO"
            .Splits(0).DisplayColumns(LInt_Col).Style.HorizontalAlignment = AlignHorzEnum.Near
            .Splits(0).DisplayColumns(LInt_Col).Visible = False
            .Splits(0).DisplayColumns(LInt_Col).AllowSizing = False

            LInt_Col = LInt_Col + 1 'NOMBRE_TIPO_USUARIO
            .Columns(LInt_Col).Caption = "Nombre"
            .Splits(0).DisplayColumns(LInt_Col).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Near
            .Splits(0).DisplayColumns(LInt_Col).Width = 150
            .Columns(LInt_Col).DataField = "NOMBRE_TIPO_USUARIO"
            .Splits(0).DisplayColumns(LInt_Col).Style.HorizontalAlignment = AlignHorzEnum.Near
            .Splits(0).DisplayColumns(LInt_Col).Visible = True
            .Splits(0).DisplayColumns(LInt_Col).AllowSizing = False

            LInt_Col = LInt_Col + 1 'NOMBRE_CORTO_TIPO_USUARIO
            .Columns(LInt_Col).Caption = "Nombre Corto"
            .Splits(0).DisplayColumns(LInt_Col).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Near
            .Splits(0).DisplayColumns(LInt_Col).Width = 110
            .Columns(LInt_Col).DataField = "NOMBRE_CORTO_TIPO_USUARIO"
            .Splits(0).DisplayColumns(LInt_Col).Style.HorizontalAlignment = AlignHorzEnum.Near
            .Splits(0).DisplayColumns(LInt_Col).Visible = True
            .Splits(0).DisplayColumns(LInt_Col).AllowSizing = False

            LInt_Col = LInt_Col + 1 'CODIGO_EXTERNO_TIPO_USUARIO
            .Columns(LInt_Col).Caption = "Codigo Externo"
            .Splits(0).DisplayColumns(LInt_Col).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Center
            .Splits(0).DisplayColumns(LInt_Col).Width = 79
            .Columns(LInt_Col).DataField = "CODIGO_EXTERNO_TIPO_USUARIO"
            .Splits(0).DisplayColumns(LInt_Col).Style.HorizontalAlignment = AlignHorzEnum.Near
            .Splits(0).DisplayColumns(LInt_Col).Visible = True
            .Splits(0).DisplayColumns(LInt_Col).AllowSizing = False

            LInt_Col = LInt_Col + 1 'ESTADO_TIPO_USUARIO
            .Columns(LInt_Col).Caption = "Estado"
            .Splits(0).DisplayColumns(LInt_Col).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Center
            .Splits(0).DisplayColumns(LInt_Col).Width = 45
            .Columns(LInt_Col).DataField = "ESTADO_TIPO_USUARIO"
            .Splits(0).DisplayColumns(LInt_Col).Style.HorizontalAlignment = AlignHorzEnum.Center
            .Splits(0).DisplayColumns(LInt_Col).Visible = True
            .Splits(0).DisplayColumns(LInt_Col).AllowSizing = False

        End With

    End Sub
    Private Sub MuestraDatosDesdeGrilla()

        Dim lIntIdTipoUsuario As Integer
        'Btn_Accion.Enabled = False
        Ctrl_Botonera_Guardar.Enabled = False

        'Btn_Cancelar.Enabled = False
        Ctrl_Botonera_Cancelar.Enabled = False

        'Btn_Agregar.Enabled = True
        Ctrl_Botonera_Agregar.Enabled = True

        If Tdg_TipoUsuario.RowCount = 0 Then
            'Btn_Modificar.Enabled = False
            Ctrl_Botonera_Editar.Enabled = False
            'Btn_Eliminar.Enabled = False
            Ctrl_Botonera_Eliminar.Enabled = False
            Exit Sub
        End If
        lIntIdTipoUsuario = Tdg_TipoUsuario.Columns("ID_TIPO_USUARIO").Text.Trim
        Txt_NombreTipoUsuario.Text = Tdg_TipoUsuario.Columns("NOMBRE_TIPO_USUARIO").Text.Trim
        Txt_NombreCortoTipoUsuario.Text = Tdg_TipoUsuario.Columns("NOMBRE_CORTO_TIPO_USUARIO").Text.Trim
        Txt_CodigoExternoTipoUsuario.Text = Tdg_TipoUsuario.Columns("CODIGO_EXTERNO_TIPO_USUARIO").Text.Trim
        Cmb_EstadoTipoUsuario.SelectedIndex = gFunSeteaItemComboBox(Cmb_EstadoTipoUsuario, Tdg_TipoUsuario.Columns("ESTADO_TIPO_USUARIO").Text.Trim, 0)
        If (DS_TipoUsuario.Tables(0).Rows.Count > 0) Then
            If Tdg_TipoUsuario.Columns("ESTADO_TIPO_USUARIO").Text.Trim <> "ELI" Then
                'Btn_Modificar.Enabled = True
                Ctrl_Botonera_Editar.Enabled = True
                'Btn_Eliminar.Enabled = True
                Ctrl_Botonera_Eliminar.Enabled = True
            Else
                'Btn_Modificar.Enabled = False
                Ctrl_Botonera_Editar.Enabled = False
                'Btn_Eliminar.Enabled = False
                Ctrl_Botonera_Eliminar.Enabled = False
            End If
        End If
    End Sub

    Private Sub Tdg_TipoUsuario_RowColChange(ByVal sender As Object, ByVal e As System.EventArgs) Handles Tdg_TipoUsuario.RowColChange
        'Btn_Accion.Enabled = False
        Ctrl_Botonera_Guardar.Enabled = False

        'Btn_Cancelar.Enabled = False
        Ctrl_Botonera_Cancelar.Enabled = False

        ltxtOperacion = ""
        Call HabilitaDeshabilitaDatos()
        Call MuestraDatosDesdeGrilla()
    End Sub
    Private Sub Tdg_TipoUsuario_AfterFilter(ByVal sender As Object, ByVal e As C1.Win.C1TrueDBGrid.FilterEventArgs) Handles Tdg_TipoUsuario.AfterFilter
        If (DS_TipoUsuario.Tables(0).Rows.Count >= 0) Then
            Call MuestraDatosDesdeGrilla()
        End If
    End Sub
    Private Sub Tdg_TipoUsuario_AfterSort(ByVal sender As Object, ByVal e As C1.Win.C1TrueDBGrid.FilterEventArgs) Handles Tdg_TipoUsuario.AfterSort
        If (DS_TipoUsuario.Tables(0).Rows.Count >= 0) Then
            Call MuestraDatosDesdeGrilla()
        End If
    End Sub

#End Region

#Region " --- Botones ---"

    Private Sub Btn_Accion_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Ctrl_Botonera_Guardar.Click 'Handles Btn_Accion.Click
        lstrResultadoTransaccion = "OK"
        If ltxtOperacion = "ELIMINAR" Then
            If Tdg_TipoUsuario.Columns("ESTADO_TIPO_USUARIO").Text.Trim = "ELI" Then
                MsgBox("El Registro ya se encuentra ELIMINADO.", MsgBoxStyle.Information, gtxtNombreSistema & " Mantenedor de Tipo Usuario.")
            ElseIf Tdg_TipoUsuario.Columns("ESTADO_TIPO_USUARIO").Text.Trim = "SIS" Then
                MsgBox("NO PUEDE eliminar esta Tipo Usuario.", MsgBoxStyle.Information, gtxtNombreSistema & " Mantenedor de Tipo Usuario.")
            Else
                '...Mensaje de confirmacion 
                If (MsgBox(vbCr & " ¿ Confirma la Eliminación de la Tipo Usuario ( " & Txt_NombreTipoUsuario.Text.Trim & " ). ?" & vbCr & vbCr, MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2 + MsgBoxStyle.Exclamation, gtxtNombreSistema & " Mantenedor de Tipo Usuario.")) = MsgBoxResult.No Then
                    Exit Sub
                End If
            End If
        Else
            If Not ValidarCamposOK() Then Exit Sub
        End If

        Select Case ltxtOperacion
            Case Is = "INSERTAR"
                Nuevo_TipoUsuario()
            Case Is = "MODIFICAR"
                Modifica_TipoUsuario()
            Case Is = "ELIMINAR"
                Elimina_TipoUsuario()
        End Select


        If lstrResultadoTransaccion.Trim = "OK" Then
            MsgBox("Operación se realizó con éxito.", MsgBoxStyle.Information, gtxtNombreSistema & " Mantenedor de Tipo Usuario.")
            Call CargaGrillaTipoUsuario()
            Call InicializaFormulario()
        Else
            MsgBox(lstrResultadoTransaccion, MsgBoxStyle.Exclamation, gtxtNombreSistema & " Mantenedor de Tipo Usuario.")
            'Btn_Accion.Focus()
        End If

    End Sub
    Private Sub Btn_Agregar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Ctrl_Botonera_Agregar.Click 'Handles Btn_Agregar.Click
        ltxtOperacion = "INSERTAR"
        Call HabilitaDeshabilitaDatos()
        Call Limpiar_Campos()
        Cmb_EstadoTipoUsuario.SelectedIndex = gFunSeteaItemComboBox(Cmb_EstadoTipoUsuario, "VIG", 0)

        'Btn_Accion.Enabled = True
        Ctrl_Botonera_Guardar.Enabled = True

        'Btn_Cancelar.Enabled = True
        Ctrl_Botonera_Cancelar.Enabled = True

        'Btn_Eliminar.Enabled = False
        Ctrl_Botonera_Eliminar.Enabled = False

        'Btn_Modificar.Enabled = False
        Ctrl_Botonera_Editar.Enabled = False

        Btn_Accion.Text = "&Grabar"

        Txt_NombreTipoUsuario.Focus()
    End Sub
    Private Sub Btn_Modificar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Ctrl_Botonera_Editar.Click 'Handles Btn_Modificar.Click
        ltxtOperacion = "MODIFICAR"
        Call HabilitaDeshabilitaDatos()

        'Btn_Accion.Enabled = True
        Ctrl_Botonera_Guardar.Enabled = True

        'Btn_Cancelar.Enabled = True
        Ctrl_Botonera_Cancelar.Enabled = True

        'Btn_Eliminar.Enabled = False
        Ctrl_Botonera_Eliminar.Enabled = False

        'Btn_Agregar.Enabled = False
        Ctrl_Botonera_Agregar.Enabled = False

        'Btn_Modificar.Enabled = False
        Ctrl_Botonera_Editar.Enabled = False

        Btn_Accion.Text = "&Grabar" '"&Actualizar"

        Txt_NombreTipoUsuario.Focus()
    End Sub
    Private Sub Btn_Eliminar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Ctrl_Botonera_Eliminar.Click   ' Handles Btn_Eliminar.Click
        If Tdg_TipoUsuario.Columns("ESTADO_TIPO_USUARIO").CellValue(Tdg_TipoUsuario.Bookmark).ToString.Trim = "SIS" Then   ' Estado NO BOrrar 
            MsgBox("No se puede eliminar este TIPO DE USUARIO", MsgBoxStyle.Critical, gtxtNombreSistema & " Mantenedor de Tipo Usuario.")
        Else
            ltxtOperacion = "ELIMINAR"
            'Call HabilitaDeshabilitaDatos()
            'Btn_Accion.Enabled = True
            'Btn_Cancelar.Enabled = True
            'Btn_Agregar.Enabled = False
            'Btn_Modificar.Enabled = False
            'Btn_Accion.Text = "&Grabar" '"&Eliminar"
            'Btn_Accion.Focus()

            Call HabilitaDeshabilitaDatos()
            lstrResultadoTransaccion = "OK"
            If Tdg_TipoUsuario.Columns("ESTADO_TIPO_USUARIO").Text.Trim = "ELI" Then
                MsgBox("El Registro ya se encuentra ELIMINADO.", MsgBoxStyle.Information, gtxtNombreSistema & " Mantenedor de Tipo Usuario.")
            ElseIf Tdg_TipoUsuario.Columns("ESTADO_TIPO_USUARIO").Text.Trim = "SIS" Then
                MsgBox("NO PUEDE eliminar esta Tipo Usuario.", MsgBoxStyle.Information, gtxtNombreSistema & " Mantenedor de Tipo Usuario.")
            Else
                If (MsgBox(vbCr & " ¿ Confirma la Eliminación de la Tipo Usuario ( " & Txt_NombreTipoUsuario.Text.Trim & " ). ?" & vbCr & vbCr, MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2 + MsgBoxStyle.Exclamation, gtxtNombreSistema & " Mantenedor de Tipo Usuario.")) = MsgBoxResult.No Then
                    Exit Sub
                End If
            End If
            Elimina_TipoUsuario()
            If lstrResultadoTransaccion.Trim = "OK" Then
                MsgBox("Operación se realizó con éxito.", MsgBoxStyle.Information, gtxtNombreSistema & " Mantenedor de Tipo Usuario.")
                Call CargaGrillaTipoUsuario()
                Call InicializaFormulario()
            Else
                MsgBox(lstrResultadoTransaccion, MsgBoxStyle.Exclamation, gtxtNombreSistema & " Mantenedor de Tipo Usuario.")
                'Btn_Accion.Focus()
            End If
        End If
    End Sub

    Private Sub Btn_Cancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Ctrl_Botonera_Cancelar.Click 'Handles Btn_Cancelar.Click

        'Btn_Accion.Enabled = False
        Ctrl_Botonera_Guardar.Enabled = False
        'Btn_Cancelar.Enabled = False
        Ctrl_Botonera_Cancelar.Enabled = False

        ltxtOperacion = ""
        Call HabilitaDeshabilitaDatos()
        Call MuestraDatosDesdeGrilla()
    End Sub

    Private Sub Nuevo_TipoUsuario()
        Dim lintIdTipoUsuario As Integer = 0
        Dim lstrEstado As String = "VIG"

        Dim intFila As Integer

        intFila = Cmb_EstadoTipoUsuario.SelectedIndex
        If Cmb_EstadoTipoUsuario.ListCount > 0 Then
            lstrEstado = Cmb_EstadoTipoUsuario.Columns(0).CellValue(intFila).ToString()
        End If

        lstrResultadoTransaccion = lobjTipoUsuario.Insertar(lintIdTipoUsuario, Txt_NombreTipoUsuario.Text.Trim, Txt_NombreCortoTipoUsuario.Text.Trim, Txt_CodigoExternoTipoUsuario.Text.Trim, "VIG")

    End Sub
    Private Sub Modifica_TipoUsuario()
        Dim lintIdTipoUsuario As Integer = 0
        Dim lintFila As Integer = 0
        Dim lstrEstado As String = ""


        lintIdTipoUsuario = Tdg_TipoUsuario.Columns("ID_TIPO_USUARIO").Text.Trim

        lintFila = Cmb_EstadoTipoUsuario.SelectedIndex
        lstrEstado = Cmb_EstadoTipoUsuario.Columns(0).CellValue(lintFila).ToString()

        lstrResultadoTransaccion = lobjTipoUsuario.Modificar(lintIdTipoUsuario, _
                                                             Txt_NombreTipoUsuario.Text.Trim, _
                                                             Txt_NombreCortoTipoUsuario.Text.Trim, _
                                                             Txt_CodigoExternoTipoUsuario.Text.Trim, lstrEstado)


    End Sub
    Private Sub Elimina_TipoUsuario()
        Dim lintIdTipoUsuario As Integer = 0

        lintIdTipoUsuario = Tdg_TipoUsuario.Columns("ID_TIPO_USUARIO").Text.Trim
        lstrResultadoTransaccion = lobjTipoUsuario.Eliminar(lintIdTipoUsuario)

    End Sub

#End Region


    Private Sub Ctrl_Botonera_Agregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Ctrl_Botonera_Agregar.Click

    End Sub

    Private Sub Ctrl_Botonera_Eliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Ctrl_Botonera_Eliminar.Click

    End Sub

    Private Sub Ctrl_Botonera_Editar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Ctrl_Botonera_Editar.Click

    End Sub

    Private Sub Ctrl_Botonera_Guardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Ctrl_Botonera_Guardar.Click

    End Sub

    Private Sub Ctrl_Botonera_Cancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Ctrl_Botonera_Cancelar.Click

    End Sub
End Class