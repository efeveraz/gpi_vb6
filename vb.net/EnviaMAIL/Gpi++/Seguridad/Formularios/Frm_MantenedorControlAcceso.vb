Imports C1.Win.C1TrueDBGrid
Imports C1.Win.C1List.C1Combo
Public Class Frm_MantenedorControlAcceso

    Dim ltxtColumnas As String = ""
    Dim ltxtOperacion As String = ""
    Dim lstrResultadoTransaccion As String = ""

    Dim lblnMoviendoElFormulario As Boolean = False
    Dim lMouseCoordenadaX As Integer = 0
    Dim lMouseCoordenadaY As Integer = 0

    Dim lobjPerfilUsuario As ClsPerfilUsuario = New ClsPerfilUsuario
    Dim lobjPerfilesUsuarios As ClsPerfilesUsuarios = New ClsPerfilesUsuarios
    Dim lobjPerfilesAcciones As ClsPerfilesAcciones = New ClsPerfilesAcciones
    Dim lobjAcciones As ClsAcciones = New ClsAcciones
    Dim DS_PerfilUsuario As DataSet
    'Dim DS_Acciones As DataSet
    Dim DS_PerilesUsuario As DataSet

#Region " --- Formulario ---"

    Public Sub CargarFormulario()

        'Me.Location = New Point(gobjFormulario._PosX, gobjFormulario._PosY)
        Me.Location = New Point(3, 3)

        If gobjFormulario._Modal Then
            ShowDialog()
        Else
            Show()
            BringToFront()
        End If
    End Sub
    Private Sub AbrirFormulario(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            '...Carga todos perfiles del usuario conectado
            DS_PerilesUsuario = lobjPerfilesUsuarios.BuscarAsignados(lstrResultadoTransaccion, glngIdUsuario)
            If lstrResultadoTransaccion <> "OK" Then
                MsgBox("Ud. No tiene perfil asociado...", MsgBoxStyle.Critical, gtxtNombreSistema & " Control de Accesos")
                Call Btn_Salir_Click(sender, e)
            End If
            gDS_Acciones = lobjAcciones.Buscar(lstrResultadoTransaccion)
            Call CargaPerfilUsuario()
            Call InicializaFormulario()


        Catch ex As Exception

        End Try

    End Sub
    Private Sub Frm_MantenedorUnidades_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.GotFocus, Me.Click, Me.Click
        Me.BringToFront()
    End Sub
    Private Sub Me_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If (e.KeyCode = Keys.F4) And e.Modifiers = Keys.Alt Then
            e.Handled = True
            Call Btn_Salir_Click(sender, e)
        ElseIf e.KeyCode = Keys.Enter Then
            e.Handled = True
            SendKeys.Send("{TAB}")
            'ElseIf e.KeyCode = Keys.Escape Then
            '    e.Handled = True
            '    Call Btn_Cancelar_Click(sender, e)
        End If
    End Sub
    Private Sub Me_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.DoubleClick, Me.DoubleClick
        Me.Location = New Point(3, 3)
    End Sub
    Private Sub Me_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Me.Dispose()
    End Sub
    Private Sub Btn_Salir_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Salir.Click
        Me.Close()
    End Sub
    Private Sub Formulario_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles Me.MouseDown, Me.MouseDown
        If e.Button = MouseButtons.Left Then
            lblnMoviendoElFormulario = True
            lMouseCoordenadaX = e.X
            lMouseCoordenadaY = e.Y
        End If
    End Sub
    Private Sub Formulario_MouseUp(ByVal sender As Object, ByVal e As MouseEventArgs) Handles Me.MouseUp, Me.MouseUp
        If e.Button = MouseButtons.Left Then
            lblnMoviendoElFormulario = False
        End If
    End Sub
    Private Sub Formulario_MouseMove(ByVal sender As Object, ByVal e As MouseEventArgs) Handles Me.MouseMove, Me.MouseMove
        If lblnMoviendoElFormulario Then
            '...Arriba
            If Me.Location.Y < 1 Then
                Me.Location = New Point(Me.Location.X, 1)
                lblnMoviendoElFormulario = False
                '...Abajo
            ElseIf Me.Location.Y > (sender.parent.Height - 30) Then
                Me.Location = New Point(Me.Location.X, sender.parent.Height - 30)
                lblnMoviendoElFormulario = False
                '...Izquierda
            ElseIf Me.Location.X < ((Me.Width / 10) - Me.Width) Then
                Me.Location = New Point(((Me.Width / 10) - Me.Width), Me.Location.Y)
                lblnMoviendoElFormulario = False
                '...Derecha
            ElseIf Me.Location.X > (sender.parent.Width - 150) Then
                Me.Location = New Point((sender.parent.Width - 150), Me.Location.Y)
                lblnMoviendoElFormulario = False
            Else
                lblnMoviendoElFormulario = True
                Dim temp As Point = New Point()
                temp.X = Me.Location.X + (e.X - lMouseCoordenadaX)
                temp.Y = Me.Location.Y + (e.Y - lMouseCoordenadaY)
                Me.Location = temp
                temp = Nothing
            End If
        End If
    End Sub

#End Region


#Region " --- Eventos Formulario ---"

    Private Sub CargaPerfilUsuario()

        Cmb_Perfiles.ClearItems()

        DS_PerfilUsuario = lobjPerfilesUsuarios.BuscarPerfilesPorNegocio(lstrResultadoTransaccion, glngNegocioOperacion)

        If lstrResultadoTransaccion = "OK" Then
            If DS_PerfilUsuario.Tables.Count > 0 Then
                If DS_PerfilUsuario.Tables(0).Rows.Count > 0 Then
                    Cmb_Perfiles.DataSource = DS_PerfilUsuario.Tables(0)
                    Cmb_Perfiles.Columns(0).DataField = "NOMBRE_PERFIL"
                    Cmb_Perfiles.Columns(1).DataField = "ID_PERFIL_USUARIO"
                    Cmb_Perfiles.Splits(0).DisplayColumns(0).Visible = True
                    Cmb_Perfiles.Splits(0).DisplayColumns(1).Visible = False
                End If
                Cmb_Perfiles.Refresh()

            End If
        End If
    End Sub

    Private Sub InicializaFormulario()
        ltxtOperacion = ""
        Cmb_Perfiles.SelectedIndex = gFunSeteaItemComboBox(Me.Cmb_Perfiles, "1", 1)
        'Cmb_Perfiles.SelectedIndex = -1
        Cmb_Perfiles.Focus()
        StatusLabel.Text = gtxtNombreSistema & "Detalle de control de acceso"


    End Sub


#End Region

    '--> Private Sub GuardarDerechosAccion(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_Grabar_DerechoAccion.Click
    Private Sub GuardarPerfilesAcciones(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Ctrl_Botonera_Guardar.Click ' Handles Btn_Grabar_PerfilesAcciones.Click
        Dim lobjTabPage As New C1.Win.C1Command.C1DockingTabPage ' TabPage   'vcl...
        Dim lobjTreeView As New TreeView
        Dim lstrOpciones As String
        Dim lstrNivel As String
        Dim lintIdPerfilUsuario As String
        Dim lstrRetorno As String
        Dim lintIndexTab As Integer = 0

        lintIdPerfilUsuario = Cmb_Perfiles.Columns("ID_PERFIL_USUARIO").Text.ToString.Trim

        lstrOpciones = ""
        lstrNivel = ""
        For Each lobjTabPage In TabModulos.TabPages
            If TabModulos.TabPages(lintIndexTab).TabVisible Then
                lobjTreeView = CType(lobjTabPage.Controls(0), TreeView)
                lstrNivel = lobjTabPage.Name & "," & lstrNivel
                RecorreArbol(lobjTreeView, lstrOpciones)
                lobjTreeView = Nothing
            End If
            lintIndexTab = lintIndexTab + 1
        Next

        'lstrOpciones = lstrNivel & lstrOpciones
        lstrOpciones = lstrOpciones
        lstrOpciones = lstrOpciones.Substring(0, lstrOpciones.Length - 1)
        lstrRetorno = lobjPerfilesAcciones.ProcesarAccionesPerfil(lstrOpciones, lintIdPerfilUsuario, glngNegocioOperacion, 1)

        If (lstrRetorno = "OK") Then
            MsgBox("Accesos del Perfil fueron actualizados CORRECTAMENTE", MsgBoxStyle.Information, gtxtNombreSistema & " Control de Accesos")
        Else
            MsgBox(lstrRetorno, MsgBoxStyle.Critical, gtxtNombreSistema & " Control de Accesos")
        End If

    End Sub
    Private Sub BuscaDerechosAccion(ByVal LTrn_Arbol As TreeNode, ByRef Ltxt_Cadenas As String)
        Dim LTrn_Nodo As TreeNode

        If (LTrn_Arbol.Checked) Then
            Ltxt_Cadenas &= LTrn_Arbol.Name & ","
        End If
        For Each LTrn_Nodo In LTrn_Arbol.Nodes
            BuscaDerechosAccion(LTrn_Nodo, Ltxt_Cadenas)
        Next
    End Sub
    Private Sub RecorreArbol(ByVal LTrn_Arbol As TreeView, ByRef Ltxt_Cadenas As String)
        Dim LTrn_Nodo As TreeNode

        For Each LTrn_Nodo In LTrn_Arbol.Nodes
            BuscaDerechosAccion(LTrn_Nodo, Ltxt_Cadenas)
        Next
    End Sub

    Private Sub Cmb_Perfiles_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Cmb_Perfiles.TextChanged
        If Cmb_Perfiles.SelectedIndex = -1 Then Exit Sub

        Btn_Grabar_PerfilesAcciones.Enabled = True
        Ctrl_Botonera_Guardar.Enabled = True
        For intIndice As Integer = 0 To DS_PerilesUsuario.Tables(0).Rows.Count - 1
            If DS_PerilesUsuario.Tables(0).Rows(intIndice).Item("ID_PERFIL_USUARIO").ToString.Trim = Cmb_Perfiles.Columns("ID_PERFIL_USUARIO").Text Then
                Btn_Grabar_PerfilesAcciones.Enabled = False
                Ctrl_Botonera_Guardar.Enabled = False
            End If
        Next

        Dim DR_Fila As Data.DataRow
        Dim lintTabIndex As Integer = 0

        '...Oculta TabPage y elimina los controles contenidos en estas paginas
        For lintTabIndex = 0 To TabModulos.TabPages.Count - 1
            TabModulos.TabPages(lintTabIndex).Controls.Clear()
            TabModulos.TabPages(lintTabIndex).TabVisible = False
        Next

        lintTabIndex = 0
        Dim lObjTreenode As New TreeNode ' -->Dim oMnu As New TreeNode

        'DS_Acciones = lobjAcciones.BuscarAcciones(lstrResultadoTransaccion, 1, 0)   'Modulos del sistema 
        If lstrResultadoTransaccion = "OK" Then
            '-->Dim oMenuApp As New clsMakeMenu
            Dim lobjMenu As New ClsGeneraMenu

            'For Each DR_Fila In DS_Acciones.Tables(0).Rows
            For Each DR_Fila In gDS_Acciones.Tables(0).Select("NUMERO_PANTALLA = 1", "")
                Dim oTreeView As New TreeView

                oTreeView.Margin = New Padding(all:=0)
                oTreeView.Name = DR_Fila("ID_ACCION").ToString


                TabModulos.TabPages(lintTabIndex).Text = DR_Fila("DESCRIPCION_ITEM").ToString.Trim
                TabModulos.TabPages(lintTabIndex).CaptionText = DR_Fila("DESCRIPCION_ITEM").ToString.Trim
                TabModulos.TabPages(lintTabIndex).Name = DR_Fila("ID_ACCION").ToString.Trim
                TabModulos.TabPages(lintTabIndex).TabVisible = True

                lobjMenu.MenuParaAsignacionTreeView(DR_Fila("NUMERO_PANTALLA"), Cmb_Perfiles.Columns("ID_PERFIL_USUARIO").Text.ToString.Trim, oTreeView)

                If oTreeView.Nodes.Count > 0 Then
                    'oTreeView.Width = 495
                    oTreeView.Width = 604
                    oTreeView.Height = 452
                    oTreeView.Scrollable = True
                    TabModulos.TabPages(lintTabIndex).Controls.Add(oTreeView)
                End If
                lintTabIndex = lintTabIndex + 1
            Next
            TabModulos.SelectedIndex = 0
            TabModulos.Refresh()
        Else
            MsgBox("No se Obtuvo el detalle de Accesos" & vbCrLf & lstrResultadoTransaccion, MsgBoxStyle.Critical, gtxtNombreSistema & " Control de Accesos")
        End If
    End Sub

    Private Sub TabModulos_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabModulos.SelectedIndexChanged
        If TabModulos.SelectedIndex = -1 Then
            TabModulos.SelectedIndex = 0
        End If
    End Sub

    Private Sub Ctrl_Botonera_Guardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Ctrl_Botonera_Guardar.Click

    End Sub
End Class