﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Frm_MantenedorTipoUsuario
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Frm_MantenedorTipoUsuario))
        Me.Pnl_Datos_TipoOperacion = New System.Windows.Forms.Panel
        Me.Btn_Cancelar = New C1.Win.C1Input.C1Button
        Me.Tdg_TipoUsuario = New C1.Win.C1TrueDBGrid.C1TrueDBGrid
        Me.Btn_Accion = New C1.Win.C1Input.C1Button
        Me.Btn_Modificar = New C1.Win.C1Input.C1Button
        Me.Btn_Agregar = New C1.Win.C1Input.C1Button
        Me.Btn_Eliminar = New C1.Win.C1Input.C1Button
        Me.Pnl_DatosTipoUsuario = New System.Windows.Forms.Panel
        Me.Cmb_EstadoTipoUsuario = New C1.Win.C1List.C1Combo
        Me.Txt_CodigoExternoTipoUsuario = New C1.Win.C1Input.C1TextBox
        Me.Txt_NombreCortoTipoUsuario = New C1.Win.C1Input.C1TextBox
        Me.Txt_NombreTipoUsuario = New C1.Win.C1Input.C1TextBox
        Me.C1SuperLabel8 = New C1.Win.C1SuperTooltip.C1SuperLabel
        Me.C1SuperLabel5 = New C1.Win.C1SuperTooltip.C1SuperLabel
        Me.C1SuperLabel4 = New C1.Win.C1SuperTooltip.C1SuperLabel
        Me.C1SuperLabel22 = New C1.Win.C1SuperTooltip.C1SuperLabel
        Me.Btn_Salir = New C1.Win.C1Input.C1Button
        Me.C1SuperTooltip1 = New C1.Win.C1SuperTooltip.C1SuperTooltip(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip
        Me.StatusLabel = New System.Windows.Forms.ToolStripStatusLabel
        Me.Ctrl_Botonera = New System.Windows.Forms.ToolStrip
        Me.Ctrl_Botonera_Agregar = New System.Windows.Forms.ToolStripButton
        Me.Ctrl_Botonera_Buscar = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.Ctrl_Botonera_Guardar = New System.Windows.Forms.ToolStripButton
        Me.Ctrl_Botonera_Editar = New System.Windows.Forms.ToolStripButton
        Me.Ctrl_Botonera_Eliminar = New System.Windows.Forms.ToolStripButton
        Me.Ctrl_Botonera_Verificar = New System.Windows.Forms.ToolStripButton
        Me.Ctrl_Botonera_Procesar = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator
        Me.Ctrl_Botonera_Imprimir = New System.Windows.Forms.ToolStripButton
        Me.Ctrl_Botonera_Excel = New System.Windows.Forms.ToolStripButton
        Me.Ctrl_Botonera_Columnas = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator
        Me.Ctrl_Botonera_Cancelar = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator
        Me.Ctrl_Combo_Plantillas = New System.Windows.Forms.ToolStripComboBox
        Me.Pnl_Datos_TipoOperacion.SuspendLayout()
        CType(Me.Tdg_TipoUsuario, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Pnl_DatosTipoUsuario.SuspendLayout()
        CType(Me.Cmb_EstadoTipoUsuario, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Txt_CodigoExternoTipoUsuario, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Txt_NombreCortoTipoUsuario, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Txt_NombreTipoUsuario, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.Ctrl_Botonera.SuspendLayout()
        Me.SuspendLayout()
        '
        'Pnl_Datos_TipoOperacion
        '
        Me.Pnl_Datos_TipoOperacion.BackColor = System.Drawing.Color.Gainsboro
        Me.Pnl_Datos_TipoOperacion.Controls.Add(Me.Btn_Cancelar)
        Me.Pnl_Datos_TipoOperacion.Controls.Add(Me.Tdg_TipoUsuario)
        Me.Pnl_Datos_TipoOperacion.Controls.Add(Me.Btn_Accion)
        Me.Pnl_Datos_TipoOperacion.Controls.Add(Me.Btn_Modificar)
        Me.Pnl_Datos_TipoOperacion.Controls.Add(Me.Btn_Agregar)
        Me.Pnl_Datos_TipoOperacion.Controls.Add(Me.Btn_Eliminar)
        Me.Pnl_Datos_TipoOperacion.Location = New System.Drawing.Point(1, 99)
        Me.Pnl_Datos_TipoOperacion.Margin = New System.Windows.Forms.Padding(2)
        Me.Pnl_Datos_TipoOperacion.Name = "Pnl_Datos_TipoOperacion"
        Me.Pnl_Datos_TipoOperacion.Size = New System.Drawing.Size(398, 194)
        Me.Pnl_Datos_TipoOperacion.TabIndex = 1018
        Me.Pnl_Datos_TipoOperacion.TabStop = True
        '
        'Btn_Cancelar
        '
        Me.Btn_Cancelar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.Btn_Cancelar.Location = New System.Drawing.Point(337, 4)
        Me.Btn_Cancelar.Margin = New System.Windows.Forms.Padding(2)
        Me.Btn_Cancelar.Name = "Btn_Cancelar"
        Me.Btn_Cancelar.Size = New System.Drawing.Size(54, 21)
        Me.Btn_Cancelar.TabIndex = 5
        Me.Btn_Cancelar.TabStop = False
        Me.Btn_Cancelar.Text = "&Cancelar"
        Me.Btn_Cancelar.UseVisualStyleBackColor = True
        Me.Btn_Cancelar.Visible = False
        Me.Btn_Cancelar.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Silver
        '
        'Tdg_TipoUsuario
        '
        Me.Tdg_TipoUsuario.AllowColSelect = False
        Me.Tdg_TipoUsuario.AllowRowSizing = C1.Win.C1TrueDBGrid.RowSizingEnum.None
        Me.Tdg_TipoUsuario.AllowUpdate = False
        Me.Tdg_TipoUsuario.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Tdg_TipoUsuario.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Tdg_TipoUsuario.Caption = "T I P O S   D E   U S U A R I O"
        Me.Tdg_TipoUsuario.CaptionHeight = 21
        Me.Tdg_TipoUsuario.DirectionAfterEnter = C1.Win.C1TrueDBGrid.DirectionAfterEnterEnum.MoveDown
        Me.Tdg_TipoUsuario.Font = New System.Drawing.Font("Arial", 7.5!)
        Me.Tdg_TipoUsuario.ForeColor = System.Drawing.Color.Lavender
        Me.Tdg_TipoUsuario.GroupByAreaVisible = False
        Me.Tdg_TipoUsuario.GroupByCaption = "Arrastre hasta aquí las columnas por las que desea agrupar"
        Me.Tdg_TipoUsuario.Images.Add(CType(resources.GetObject("Tdg_TipoUsuario.Images"), System.Drawing.Image))
        Me.Tdg_TipoUsuario.Location = New System.Drawing.Point(0, 28)
        Me.Tdg_TipoUsuario.MaintainRowCurrency = True
        Me.Tdg_TipoUsuario.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.Tdg_TipoUsuario.MarqueeStyle = C1.Win.C1TrueDBGrid.MarqueeEnum.HighlightRow
        Me.Tdg_TipoUsuario.MultiSelect = C1.Win.C1TrueDBGrid.MultiSelectEnum.Simple
        Me.Tdg_TipoUsuario.Name = "Tdg_TipoUsuario"
        Me.Tdg_TipoUsuario.PreviewInfo.Location = New System.Drawing.Point(0, 0)
        Me.Tdg_TipoUsuario.PreviewInfo.Size = New System.Drawing.Size(0, 0)
        Me.Tdg_TipoUsuario.PreviewInfo.ZoomFactor = 75
        Me.Tdg_TipoUsuario.PrintInfo.PageSettings = CType(resources.GetObject("Tdg_TipoUsuario.PrintInfo.PageSettings"), System.Drawing.Printing.PageSettings)
        Me.Tdg_TipoUsuario.RecordSelectors = False
        Me.Tdg_TipoUsuario.RecordSelectorWidth = 15
        Me.Tdg_TipoUsuario.RowDivider.Color = System.Drawing.Color.DarkGray
        Me.Tdg_TipoUsuario.RowDivider.Style = C1.Win.C1TrueDBGrid.LineStyleEnum.Raised
        Me.Tdg_TipoUsuario.RowHeight = 15
        Me.Tdg_TipoUsuario.ScrollTips = True
        Me.Tdg_TipoUsuario.Size = New System.Drawing.Size(397, 164)
        Me.Tdg_TipoUsuario.SplitDividerSize = New System.Drawing.Size(0, 0)
        Me.Tdg_TipoUsuario.TabIndex = 4
        Me.Tdg_TipoUsuario.TabStop = False
        Me.Tdg_TipoUsuario.Text = "C1TrueDBGrid1"
        Me.Tdg_TipoUsuario.VisualStyle = C1.Win.C1TrueDBGrid.VisualStyle.Office2007Silver
        Me.Tdg_TipoUsuario.PropBag = resources.GetString("Tdg_TipoUsuario.PropBag")
        '
        'Btn_Accion
        '
        Me.Btn_Accion.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.Btn_Accion.Location = New System.Drawing.Point(282, 4)
        Me.Btn_Accion.Margin = New System.Windows.Forms.Padding(2)
        Me.Btn_Accion.Name = "Btn_Accion"
        Me.Btn_Accion.Size = New System.Drawing.Size(54, 21)
        Me.Btn_Accion.TabIndex = 3
        Me.Btn_Accion.TabStop = False
        Me.Btn_Accion.Text = "&Grabar"
        Me.Btn_Accion.UseVisualStyleBackColor = True
        Me.Btn_Accion.Visible = False
        Me.Btn_Accion.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Silver
        '
        'Btn_Modificar
        '
        Me.Btn_Modificar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.Btn_Modificar.Enabled = False
        Me.Btn_Modificar.Font = New System.Drawing.Font("Arial", 7.5!)
        Me.Btn_Modificar.Location = New System.Drawing.Point(212, 5)
        Me.Btn_Modificar.Margin = New System.Windows.Forms.Padding(2)
        Me.Btn_Modificar.Name = "Btn_Modificar"
        Me.Btn_Modificar.Size = New System.Drawing.Size(24, 20)
        Me.Btn_Modificar.TabIndex = 2
        Me.Btn_Modificar.TabStop = False
        Me.Btn_Modificar.Tag = ""
        Me.Btn_Modificar.Text = "M"
        Me.C1SuperTooltip1.SetToolTip(Me.Btn_Modificar, "Modifica registro seleccionado")
        Me.Btn_Modificar.UseVisualStyleBackColor = True
        Me.Btn_Modificar.Visible = False
        Me.Btn_Modificar.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Silver
        '
        'Btn_Agregar
        '
        Me.Btn_Agregar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.Btn_Agregar.Font = New System.Drawing.Font("Arial", 7.5!)
        Me.Btn_Agregar.Location = New System.Drawing.Point(162, 5)
        Me.Btn_Agregar.Margin = New System.Windows.Forms.Padding(2)
        Me.Btn_Agregar.Name = "Btn_Agregar"
        Me.Btn_Agregar.Size = New System.Drawing.Size(24, 20)
        Me.Btn_Agregar.TabIndex = 0
        Me.Btn_Agregar.TabStop = False
        Me.Btn_Agregar.Tag = ""
        Me.Btn_Agregar.Text = "+"
        Me.C1SuperTooltip1.SetToolTip(Me.Btn_Agregar, "Crea nuevo registro")
        Me.Btn_Agregar.UseVisualStyleBackColor = True
        Me.Btn_Agregar.Visible = False
        Me.Btn_Agregar.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Silver
        '
        'Btn_Eliminar
        '
        Me.Btn_Eliminar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.Btn_Eliminar.Enabled = False
        Me.Btn_Eliminar.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.Btn_Eliminar.Location = New System.Drawing.Point(187, 5)
        Me.Btn_Eliminar.Margin = New System.Windows.Forms.Padding(2)
        Me.Btn_Eliminar.Name = "Btn_Eliminar"
        Me.Btn_Eliminar.Size = New System.Drawing.Size(24, 20)
        Me.Btn_Eliminar.TabIndex = 1
        Me.Btn_Eliminar.TabStop = False
        Me.Btn_Eliminar.Tag = ""
        Me.Btn_Eliminar.Text = "─"
        Me.C1SuperTooltip1.SetToolTip(Me.Btn_Eliminar, "Elimina registro seleccionado")
        Me.Btn_Eliminar.UseVisualStyleBackColor = True
        Me.Btn_Eliminar.Visible = False
        Me.Btn_Eliminar.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Silver
        '
        'Pnl_DatosTipoUsuario
        '
        Me.Pnl_DatosTipoUsuario.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Pnl_DatosTipoUsuario.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Pnl_DatosTipoUsuario.Controls.Add(Me.Cmb_EstadoTipoUsuario)
        Me.Pnl_DatosTipoUsuario.Controls.Add(Me.Txt_CodigoExternoTipoUsuario)
        Me.Pnl_DatosTipoUsuario.Controls.Add(Me.Txt_NombreCortoTipoUsuario)
        Me.Pnl_DatosTipoUsuario.Controls.Add(Me.Txt_NombreTipoUsuario)
        Me.Pnl_DatosTipoUsuario.Controls.Add(Me.C1SuperLabel8)
        Me.Pnl_DatosTipoUsuario.Controls.Add(Me.C1SuperLabel5)
        Me.Pnl_DatosTipoUsuario.Controls.Add(Me.C1SuperLabel4)
        Me.Pnl_DatosTipoUsuario.Controls.Add(Me.C1SuperLabel22)
        Me.Pnl_DatosTipoUsuario.Location = New System.Drawing.Point(1, 23)
        Me.Pnl_DatosTipoUsuario.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.Pnl_DatosTipoUsuario.Name = "Pnl_DatosTipoUsuario"
        Me.Pnl_DatosTipoUsuario.Size = New System.Drawing.Size(398, 76)
        Me.Pnl_DatosTipoUsuario.TabIndex = 1017
        '
        'Cmb_EstadoTipoUsuario
        '
        Me.Cmb_EstadoTipoUsuario.AddItemSeparator = Global.Microsoft.VisualBasic.ChrW(59)
        Me.Cmb_EstadoTipoUsuario.AllowColMove = False
        Me.Cmb_EstadoTipoUsuario.AllowSort = False
        Me.Cmb_EstadoTipoUsuario.AutoDropDown = True
        Me.Cmb_EstadoTipoUsuario.AutoSelect = True
        Me.Cmb_EstadoTipoUsuario.AutoSize = False
        Me.Cmb_EstadoTipoUsuario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Cmb_EstadoTipoUsuario.Caption = ""
        Me.Cmb_EstadoTipoUsuario.CaptionHeight = 18
        Me.Cmb_EstadoTipoUsuario.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.Cmb_EstadoTipoUsuario.ColumnCaptionHeight = 18
        Me.Cmb_EstadoTipoUsuario.ColumnFooterHeight = 18
        Me.Cmb_EstadoTipoUsuario.ColumnHeaders = False
        Me.Cmb_EstadoTipoUsuario.ComboStyle = C1.Win.C1List.ComboStyleEnum.DropdownList
        Me.Cmb_EstadoTipoUsuario.ContentHeight = 12
        Me.Cmb_EstadoTipoUsuario.DataMode = C1.Win.C1List.DataModeEnum.AddItem
        Me.Cmb_EstadoTipoUsuario.DeadAreaBackColor = System.Drawing.Color.Empty
        Me.Cmb_EstadoTipoUsuario.EditorBackColor = System.Drawing.Color.White
        Me.Cmb_EstadoTipoUsuario.EditorFont = New System.Drawing.Font("Arial", 7.0!, System.Drawing.FontStyle.Bold)
        Me.Cmb_EstadoTipoUsuario.EditorForeColor = System.Drawing.Color.SteelBlue
        Me.Cmb_EstadoTipoUsuario.EditorHeight = 12
        Me.Cmb_EstadoTipoUsuario.ExtendRightColumn = True
        Me.Cmb_EstadoTipoUsuario.FlatStyle = C1.Win.C1List.FlatModeEnum.Flat
        Me.Cmb_EstadoTipoUsuario.Font = New System.Drawing.Font("Arial", 7.0!, System.Drawing.FontStyle.Bold)
        Me.Cmb_EstadoTipoUsuario.Images.Add(CType(resources.GetObject("Cmb_EstadoTipoUsuario.Images"), System.Drawing.Image))
        Me.Cmb_EstadoTipoUsuario.ItemHeight = 18
        Me.Cmb_EstadoTipoUsuario.Location = New System.Drawing.Point(288, 52)
        Me.Cmb_EstadoTipoUsuario.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Cmb_EstadoTipoUsuario.MatchEntry = C1.Win.C1List.MatchEntryEnum.Extended
        Me.Cmb_EstadoTipoUsuario.MatchEntryTimeout = CType(500, Long)
        Me.Cmb_EstadoTipoUsuario.MaxDropDownItems = CType(5, Short)
        Me.Cmb_EstadoTipoUsuario.MaxLength = 0
        Me.Cmb_EstadoTipoUsuario.MinimumSize = New System.Drawing.Size(10, 16)
        Me.Cmb_EstadoTipoUsuario.MouseCursor = System.Windows.Forms.Cursors.Default
        Me.Cmb_EstadoTipoUsuario.Name = "Cmb_EstadoTipoUsuario"
        Me.Cmb_EstadoTipoUsuario.RowSubDividerColor = System.Drawing.Color.DarkGray
        Me.Cmb_EstadoTipoUsuario.Size = New System.Drawing.Size(103, 16)
        Me.Cmb_EstadoTipoUsuario.TabIndex = 3
        Me.Cmb_EstadoTipoUsuario.VisualStyle = C1.Win.C1List.VisualStyle.Office2007Silver
        Me.Cmb_EstadoTipoUsuario.PropBag = resources.GetString("Cmb_EstadoTipoUsuario.PropBag")
        '
        'Txt_CodigoExternoTipoUsuario
        '
        Me.Txt_CodigoExternoTipoUsuario.AcceptsEscape = False
        Me.Txt_CodigoExternoTipoUsuario.AcceptsReturn = True
        Me.Txt_CodigoExternoTipoUsuario.AcceptsTab = True
        Me.Txt_CodigoExternoTipoUsuario.AutoSize = False
        Me.Txt_CodigoExternoTipoUsuario.BackColor = System.Drawing.Color.White
        Me.Txt_CodigoExternoTipoUsuario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Txt_CodigoExternoTipoUsuario.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.Txt_CodigoExternoTipoUsuario.DisabledForeColor = System.Drawing.Color.Lavender
        Me.Txt_CodigoExternoTipoUsuario.Font = New System.Drawing.Font("Arial", 7.0!, System.Drawing.FontStyle.Bold)
        Me.Txt_CodigoExternoTipoUsuario.ForeColor = System.Drawing.Color.SteelBlue
        Me.Txt_CodigoExternoTipoUsuario.Location = New System.Drawing.Point(149, 52)
        Me.Txt_CodigoExternoTipoUsuario.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.Txt_CodigoExternoTipoUsuario.MaxLength = 20
        Me.Txt_CodigoExternoTipoUsuario.Name = "Txt_CodigoExternoTipoUsuario"
        Me.Txt_CodigoExternoTipoUsuario.NumericInput = False
        Me.Txt_CodigoExternoTipoUsuario.ReadOnly = True
        Me.Txt_CodigoExternoTipoUsuario.Size = New System.Drawing.Size(136, 16)
        Me.Txt_CodigoExternoTipoUsuario.TabIndex = 2
        Me.Txt_CodigoExternoTipoUsuario.TabStop = False
        Me.Txt_CodigoExternoTipoUsuario.Tag = Nothing
        Me.Txt_CodigoExternoTipoUsuario.TextDetached = True
        Me.Txt_CodigoExternoTipoUsuario.Value = " "
        Me.Txt_CodigoExternoTipoUsuario.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Silver
        '
        'Txt_NombreCortoTipoUsuario
        '
        Me.Txt_NombreCortoTipoUsuario.AcceptsEscape = False
        Me.Txt_NombreCortoTipoUsuario.AcceptsReturn = True
        Me.Txt_NombreCortoTipoUsuario.AcceptsTab = True
        Me.Txt_NombreCortoTipoUsuario.AutoSize = False
        Me.Txt_NombreCortoTipoUsuario.BackColor = System.Drawing.Color.White
        Me.Txt_NombreCortoTipoUsuario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Txt_NombreCortoTipoUsuario.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.Txt_NombreCortoTipoUsuario.DisabledForeColor = System.Drawing.Color.Lavender
        Me.Txt_NombreCortoTipoUsuario.Font = New System.Drawing.Font("Arial", 7.0!, System.Drawing.FontStyle.Bold)
        Me.Txt_NombreCortoTipoUsuario.ForeColor = System.Drawing.Color.SteelBlue
        Me.Txt_NombreCortoTipoUsuario.Location = New System.Drawing.Point(8, 52)
        Me.Txt_NombreCortoTipoUsuario.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.Txt_NombreCortoTipoUsuario.MaxLength = 15
        Me.Txt_NombreCortoTipoUsuario.Name = "Txt_NombreCortoTipoUsuario"
        Me.Txt_NombreCortoTipoUsuario.NumericInput = False
        Me.Txt_NombreCortoTipoUsuario.ReadOnly = True
        Me.Txt_NombreCortoTipoUsuario.Size = New System.Drawing.Size(138, 16)
        Me.Txt_NombreCortoTipoUsuario.TabIndex = 1
        Me.Txt_NombreCortoTipoUsuario.TabStop = False
        Me.Txt_NombreCortoTipoUsuario.Tag = Nothing
        Me.Txt_NombreCortoTipoUsuario.TextDetached = True
        Me.Txt_NombreCortoTipoUsuario.Value = " "
        Me.Txt_NombreCortoTipoUsuario.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Silver
        '
        'Txt_NombreTipoUsuario
        '
        Me.Txt_NombreTipoUsuario.AcceptsEscape = False
        Me.Txt_NombreTipoUsuario.AcceptsReturn = True
        Me.Txt_NombreTipoUsuario.AcceptsTab = True
        Me.Txt_NombreTipoUsuario.AutoSize = False
        Me.Txt_NombreTipoUsuario.BackColor = System.Drawing.Color.White
        Me.Txt_NombreTipoUsuario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Txt_NombreTipoUsuario.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.Txt_NombreTipoUsuario.DisabledForeColor = System.Drawing.Color.Lavender
        Me.Txt_NombreTipoUsuario.Font = New System.Drawing.Font("Arial", 7.0!, System.Drawing.FontStyle.Bold)
        Me.Txt_NombreTipoUsuario.ForeColor = System.Drawing.Color.SteelBlue
        Me.Txt_NombreTipoUsuario.Location = New System.Drawing.Point(8, 20)
        Me.Txt_NombreTipoUsuario.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.Txt_NombreTipoUsuario.MaxLength = 50
        Me.Txt_NombreTipoUsuario.Name = "Txt_NombreTipoUsuario"
        Me.Txt_NombreTipoUsuario.NumericInput = False
        Me.Txt_NombreTipoUsuario.ReadOnly = True
        Me.Txt_NombreTipoUsuario.Size = New System.Drawing.Size(383, 16)
        Me.Txt_NombreTipoUsuario.TabIndex = 0
        Me.Txt_NombreTipoUsuario.TabStop = False
        Me.Txt_NombreTipoUsuario.Tag = Nothing
        Me.Txt_NombreTipoUsuario.TextDetached = True
        Me.Txt_NombreTipoUsuario.Value = " "
        Me.Txt_NombreTipoUsuario.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Silver
        '
        'C1SuperLabel8
        '
        Me.C1SuperLabel8.AutoSize = True
        Me.C1SuperLabel8.BackColor = System.Drawing.Color.Transparent
        Me.C1SuperLabel8.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.C1SuperLabel8.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.C1SuperLabel8.Location = New System.Drawing.Point(147, 36)
        Me.C1SuperLabel8.Margin = New System.Windows.Forms.Padding(2, 4, 2, 4)
        Me.C1SuperLabel8.Name = "C1SuperLabel8"
        Me.C1SuperLabel8.Size = New System.Drawing.Size(79, 19)
        Me.C1SuperLabel8.TabIndex = 1050
        Me.C1SuperLabel8.Text = "Código Externo"
        Me.C1SuperLabel8.UseMnemonic = True
        '
        'C1SuperLabel5
        '
        Me.C1SuperLabel5.AutoSize = True
        Me.C1SuperLabel5.BackColor = System.Drawing.Color.Transparent
        Me.C1SuperLabel5.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.C1SuperLabel5.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.C1SuperLabel5.Location = New System.Drawing.Point(286, 36)
        Me.C1SuperLabel5.Margin = New System.Windows.Forms.Padding(2, 4, 2, 4)
        Me.C1SuperLabel5.Name = "C1SuperLabel5"
        Me.C1SuperLabel5.Size = New System.Drawing.Size(38, 19)
        Me.C1SuperLabel5.TabIndex = 1048
        Me.C1SuperLabel5.Text = "Estado"
        Me.C1SuperLabel5.UseMnemonic = True
        '
        'C1SuperLabel4
        '
        Me.C1SuperLabel4.AutoSize = True
        Me.C1SuperLabel4.BackColor = System.Drawing.Color.Transparent
        Me.C1SuperLabel4.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.C1SuperLabel4.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.C1SuperLabel4.Location = New System.Drawing.Point(6, 36)
        Me.C1SuperLabel4.Margin = New System.Windows.Forms.Padding(2, 4, 2, 4)
        Me.C1SuperLabel4.Name = "C1SuperLabel4"
        Me.C1SuperLabel4.Size = New System.Drawing.Size(71, 19)
        Me.C1SuperLabel4.TabIndex = 1047
        Me.C1SuperLabel4.Text = "Nombre Corto"
        Me.C1SuperLabel4.UseMnemonic = True
        '
        'C1SuperLabel22
        '
        Me.C1SuperLabel22.AutoSize = True
        Me.C1SuperLabel22.BackColor = System.Drawing.Color.Transparent
        Me.C1SuperLabel22.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.C1SuperLabel22.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.C1SuperLabel22.Location = New System.Drawing.Point(6, 4)
        Me.C1SuperLabel22.Margin = New System.Windows.Forms.Padding(2, 4, 2, 4)
        Me.C1SuperLabel22.Name = "C1SuperLabel22"
        Me.C1SuperLabel22.Size = New System.Drawing.Size(43, 19)
        Me.C1SuperLabel22.TabIndex = 1044
        Me.C1SuperLabel22.Text = "Nombre"
        Me.C1SuperLabel22.UseMnemonic = True
        '
        'Btn_Salir
        '
        Me.Btn_Salir.BackColor = System.Drawing.Color.Transparent
        Me.Btn_Salir.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.Btn_Salir.FlatAppearance.BorderSize = 0
        Me.Btn_Salir.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray
        Me.Btn_Salir.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray
        Me.Btn_Salir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Btn_Salir.Font = New System.Drawing.Font("Arial", 8.25!)
        Me.Btn_Salir.ForeColor = System.Drawing.Color.White
        Me.Btn_Salir.Location = New System.Drawing.Point(346, 0)
        Me.Btn_Salir.Margin = New System.Windows.Forms.Padding(2)
        Me.Btn_Salir.Name = "Btn_Salir"
        Me.Btn_Salir.Size = New System.Drawing.Size(53, 22)
        Me.Btn_Salir.TabIndex = 1031
        Me.Btn_Salir.TabStop = False
        Me.Btn_Salir.Text = "&Salir  |x|"
        Me.Btn_Salir.UseVisualStyleBackColor = True
        '
        'C1SuperTooltip1
        '
        Me.C1SuperTooltip1.Font = New System.Drawing.Font("Tahoma", 8.0!)
        Me.C1SuperTooltip1.RoundedCorners = True
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.StatusStrip1.AutoSize = False
        Me.StatusStrip1.BackColor = System.Drawing.Color.DarkGray
        Me.StatusStrip1.Dock = System.Windows.Forms.DockStyle.None
        Me.StatusStrip1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.StatusLabel})
        Me.StatusStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Flow
        Me.StatusStrip1.Location = New System.Drawing.Point(1, 294)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(398, 18)
        Me.StatusStrip1.Stretch = False
        Me.StatusStrip1.TabIndex = 1032
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'StatusLabel
        '
        Me.StatusLabel.BackColor = System.Drawing.Color.Transparent
        Me.StatusLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Bold)
        Me.StatusLabel.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.StatusLabel.Name = "StatusLabel"
        Me.StatusLabel.Size = New System.Drawing.Size(128, 14)
        Me.StatusLabel.Text = "ToolStripStatusLabel1"
        '
        'Ctrl_Botonera
        '
        Me.Ctrl_Botonera.BackColor = System.Drawing.Color.DarkGray
        Me.Ctrl_Botonera.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Ctrl_Botonera_Agregar, Me.Ctrl_Botonera_Buscar, Me.ToolStripSeparator1, Me.Ctrl_Botonera_Guardar, Me.Ctrl_Botonera_Editar, Me.Ctrl_Botonera_Eliminar, Me.Ctrl_Botonera_Verificar, Me.Ctrl_Botonera_Procesar, Me.ToolStripSeparator2, Me.Ctrl_Botonera_Imprimir, Me.Ctrl_Botonera_Excel, Me.Ctrl_Botonera_Columnas, Me.ToolStripSeparator3, Me.Ctrl_Botonera_Cancelar, Me.ToolStripSeparator4, Me.Ctrl_Combo_Plantillas})
        Me.Ctrl_Botonera.Location = New System.Drawing.Point(0, 0)
        Me.Ctrl_Botonera.Name = "Ctrl_Botonera"
        Me.Ctrl_Botonera.Size = New System.Drawing.Size(400, 25)
        Me.Ctrl_Botonera.TabIndex = 1068
        Me.Ctrl_Botonera.Text = "ToolStrip1"
        '
        'Ctrl_Botonera_Agregar
        '
        Me.Ctrl_Botonera_Agregar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Ctrl_Botonera_Agregar.Image = CType(resources.GetObject("Ctrl_Botonera_Agregar.Image"), System.Drawing.Image)
        Me.Ctrl_Botonera_Agregar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.Ctrl_Botonera_Agregar.Name = "Ctrl_Botonera_Agregar"
        Me.Ctrl_Botonera_Agregar.Size = New System.Drawing.Size(23, 22)
        Me.Ctrl_Botonera_Agregar.Text = "ToolStripButton2"
        Me.Ctrl_Botonera_Agregar.ToolTipText = "Agregar"
        '
        'Ctrl_Botonera_Buscar
        '
        Me.Ctrl_Botonera_Buscar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Ctrl_Botonera_Buscar.Enabled = False
        Me.Ctrl_Botonera_Buscar.Image = CType(resources.GetObject("Ctrl_Botonera_Buscar.Image"), System.Drawing.Image)
        Me.Ctrl_Botonera_Buscar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.Ctrl_Botonera_Buscar.Name = "Ctrl_Botonera_Buscar"
        Me.Ctrl_Botonera_Buscar.Size = New System.Drawing.Size(23, 22)
        Me.Ctrl_Botonera_Buscar.Text = "Buscar"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'Ctrl_Botonera_Guardar
        '
        Me.Ctrl_Botonera_Guardar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Ctrl_Botonera_Guardar.Image = CType(resources.GetObject("Ctrl_Botonera_Guardar.Image"), System.Drawing.Image)
        Me.Ctrl_Botonera_Guardar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.Ctrl_Botonera_Guardar.Name = "Ctrl_Botonera_Guardar"
        Me.Ctrl_Botonera_Guardar.Size = New System.Drawing.Size(23, 22)
        Me.Ctrl_Botonera_Guardar.Text = "Guardar"
        '
        'Ctrl_Botonera_Editar
        '
        Me.Ctrl_Botonera_Editar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Ctrl_Botonera_Editar.Image = CType(resources.GetObject("Ctrl_Botonera_Editar.Image"), System.Drawing.Image)
        Me.Ctrl_Botonera_Editar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.Ctrl_Botonera_Editar.Name = "Ctrl_Botonera_Editar"
        Me.Ctrl_Botonera_Editar.Size = New System.Drawing.Size(23, 22)
        Me.Ctrl_Botonera_Editar.Text = "Editar"
        '
        'Ctrl_Botonera_Eliminar
        '
        Me.Ctrl_Botonera_Eliminar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Ctrl_Botonera_Eliminar.Image = CType(resources.GetObject("Ctrl_Botonera_Eliminar.Image"), System.Drawing.Image)
        Me.Ctrl_Botonera_Eliminar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.Ctrl_Botonera_Eliminar.Name = "Ctrl_Botonera_Eliminar"
        Me.Ctrl_Botonera_Eliminar.Size = New System.Drawing.Size(23, 22)
        Me.Ctrl_Botonera_Eliminar.Text = "Eliminar"
        '
        'Ctrl_Botonera_Verificar
        '
        Me.Ctrl_Botonera_Verificar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Ctrl_Botonera_Verificar.Enabled = False
        Me.Ctrl_Botonera_Verificar.Image = CType(resources.GetObject("Ctrl_Botonera_Verificar.Image"), System.Drawing.Image)
        Me.Ctrl_Botonera_Verificar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.Ctrl_Botonera_Verificar.Name = "Ctrl_Botonera_Verificar"
        Me.Ctrl_Botonera_Verificar.Size = New System.Drawing.Size(23, 22)
        Me.Ctrl_Botonera_Verificar.Text = "Verificar"
        '
        'Ctrl_Botonera_Procesar
        '
        Me.Ctrl_Botonera_Procesar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Ctrl_Botonera_Procesar.Enabled = False
        Me.Ctrl_Botonera_Procesar.Image = CType(resources.GetObject("Ctrl_Botonera_Procesar.Image"), System.Drawing.Image)
        Me.Ctrl_Botonera_Procesar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.Ctrl_Botonera_Procesar.Name = "Ctrl_Botonera_Procesar"
        Me.Ctrl_Botonera_Procesar.Size = New System.Drawing.Size(23, 22)
        Me.Ctrl_Botonera_Procesar.Text = "Procesar"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'Ctrl_Botonera_Imprimir
        '
        Me.Ctrl_Botonera_Imprimir.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Ctrl_Botonera_Imprimir.Enabled = False
        Me.Ctrl_Botonera_Imprimir.Image = CType(resources.GetObject("Ctrl_Botonera_Imprimir.Image"), System.Drawing.Image)
        Me.Ctrl_Botonera_Imprimir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.Ctrl_Botonera_Imprimir.Name = "Ctrl_Botonera_Imprimir"
        Me.Ctrl_Botonera_Imprimir.Size = New System.Drawing.Size(23, 22)
        Me.Ctrl_Botonera_Imprimir.Text = "Imprimir"
        Me.Ctrl_Botonera_Imprimir.ToolTipText = "Imprimir"
        '
        'Ctrl_Botonera_Excel
        '
        Me.Ctrl_Botonera_Excel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Ctrl_Botonera_Excel.Enabled = False
        Me.Ctrl_Botonera_Excel.Image = CType(resources.GetObject("Ctrl_Botonera_Excel.Image"), System.Drawing.Image)
        Me.Ctrl_Botonera_Excel.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.Ctrl_Botonera_Excel.Name = "Ctrl_Botonera_Excel"
        Me.Ctrl_Botonera_Excel.Size = New System.Drawing.Size(23, 22)
        Me.Ctrl_Botonera_Excel.Text = "Exportar a Excel"
        '
        'Ctrl_Botonera_Columnas
        '
        Me.Ctrl_Botonera_Columnas.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Ctrl_Botonera_Columnas.Enabled = False
        Me.Ctrl_Botonera_Columnas.Image = CType(resources.GetObject("Ctrl_Botonera_Columnas.Image"), System.Drawing.Image)
        Me.Ctrl_Botonera_Columnas.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.Ctrl_Botonera_Columnas.Name = "Ctrl_Botonera_Columnas"
        Me.Ctrl_Botonera_Columnas.Size = New System.Drawing.Size(23, 22)
        Me.Ctrl_Botonera_Columnas.Text = "Configuración Columnas"
        Me.Ctrl_Botonera_Columnas.ToolTipText = "Configuración Columnas"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(6, 25)
        '
        'Ctrl_Botonera_Cancelar
        '
        Me.Ctrl_Botonera_Cancelar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Ctrl_Botonera_Cancelar.Image = CType(resources.GetObject("Ctrl_Botonera_Cancelar.Image"), System.Drawing.Image)
        Me.Ctrl_Botonera_Cancelar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.Ctrl_Botonera_Cancelar.Name = "Ctrl_Botonera_Cancelar"
        Me.Ctrl_Botonera_Cancelar.Size = New System.Drawing.Size(23, 22)
        Me.Ctrl_Botonera_Cancelar.Text = "Cancelar"
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(6, 25)
        '
        'Ctrl_Combo_Plantillas
        '
        Me.Ctrl_Combo_Plantillas.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.Ctrl_Combo_Plantillas.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Ctrl_Combo_Plantillas.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Ctrl_Combo_Plantillas.Name = "Ctrl_Combo_Plantillas"
        Me.Ctrl_Combo_Plantillas.Size = New System.Drawing.Size(121, 21)
        Me.Ctrl_Combo_Plantillas.Visible = False
        '
        'Frm_MantenedorTipoUsuario
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(5.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.DimGray
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.ClientSize = New System.Drawing.Size(400, 313)
        Me.Controls.Add(Me.Ctrl_Botonera)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.Btn_Salir)
        Me.Controls.Add(Me.Pnl_Datos_TipoOperacion)
        Me.Controls.Add(Me.Pnl_DatosTipoUsuario)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.ForeColor = System.Drawing.Color.DimGray
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.MaximizeBox = False
        Me.Name = "Frm_MantenedorTipoUsuario"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Mantenedor Tipos de Usuario"
        Me.Pnl_Datos_TipoOperacion.ResumeLayout(False)
        CType(Me.Tdg_TipoUsuario, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Pnl_DatosTipoUsuario.ResumeLayout(False)
        Me.Pnl_DatosTipoUsuario.PerformLayout()
        CType(Me.Cmb_EstadoTipoUsuario, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Txt_CodigoExternoTipoUsuario, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Txt_NombreCortoTipoUsuario, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Txt_NombreTipoUsuario, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.Ctrl_Botonera.ResumeLayout(False)
        Me.Ctrl_Botonera.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Pnl_Datos_TipoOperacion As System.Windows.Forms.Panel
    Friend WithEvents Tdg_TipoUsuario As C1.Win.C1TrueDBGrid.C1TrueDBGrid
    Friend WithEvents Btn_Accion As C1.Win.C1Input.C1Button
    Friend WithEvents Btn_Modificar As C1.Win.C1Input.C1Button
    Friend WithEvents Btn_Agregar As C1.Win.C1Input.C1Button
    Friend WithEvents Btn_Eliminar As C1.Win.C1Input.C1Button
    Friend WithEvents Pnl_DatosTipoUsuario As System.Windows.Forms.Panel
    Friend WithEvents Txt_CodigoExternoTipoUsuario As C1.Win.C1Input.C1TextBox
    Friend WithEvents Txt_NombreCortoTipoUsuario As C1.Win.C1Input.C1TextBox
    Friend WithEvents Txt_NombreTipoUsuario As C1.Win.C1Input.C1TextBox
    Friend WithEvents C1SuperLabel8 As C1.Win.C1SuperTooltip.C1SuperLabel
    Friend WithEvents C1SuperLabel5 As C1.Win.C1SuperTooltip.C1SuperLabel
    Friend WithEvents C1SuperLabel4 As C1.Win.C1SuperTooltip.C1SuperLabel
    Friend WithEvents C1SuperLabel22 As C1.Win.C1SuperTooltip.C1SuperLabel
    Friend WithEvents Btn_Salir As C1.Win.C1Input.C1Button
    Friend WithEvents Btn_Cancelar As C1.Win.C1Input.C1Button
    Friend WithEvents Cmb_EstadoTipoUsuario As C1.Win.C1List.C1Combo
    Friend WithEvents C1SuperTooltip1 As C1.Win.C1SuperTooltip.C1SuperTooltip
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents StatusLabel As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents Ctrl_Botonera As System.Windows.Forms.ToolStrip
    Friend WithEvents Ctrl_Botonera_Agregar As System.Windows.Forms.ToolStripButton
    Friend WithEvents Ctrl_Botonera_Buscar As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents Ctrl_Botonera_Guardar As System.Windows.Forms.ToolStripButton
    Friend WithEvents Ctrl_Botonera_Editar As System.Windows.Forms.ToolStripButton
    Friend WithEvents Ctrl_Botonera_Eliminar As System.Windows.Forms.ToolStripButton
    Friend WithEvents Ctrl_Botonera_Verificar As System.Windows.Forms.ToolStripButton
    Friend WithEvents Ctrl_Botonera_Procesar As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents Ctrl_Botonera_Imprimir As System.Windows.Forms.ToolStripButton
    Friend WithEvents Ctrl_Botonera_Excel As System.Windows.Forms.ToolStripButton
    Friend WithEvents Ctrl_Botonera_Columnas As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents Ctrl_Botonera_Cancelar As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents Ctrl_Combo_Plantillas As System.Windows.Forms.ToolStripComboBox
End Class
