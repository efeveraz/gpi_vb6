Imports Gpi__.My.Resources

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Frm_MantenedorUnidades


    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Frm_MantenedorUnidades))
        Me.Pnl_DatosUnidad = New System.Windows.Forms.Panel
        Me.Cmb_EstadoUnidad = New C1.Win.C1List.C1Combo
        Me.Txt_CodigoExternoUnidad = New C1.Win.C1Input.C1TextBox
        Me.Txt_NombreCortoUnidad = New C1.Win.C1Input.C1TextBox
        Me.Txt_NombreUnidad = New C1.Win.C1Input.C1TextBox
        Me.C1SuperLabel8 = New C1.Win.C1SuperTooltip.C1SuperLabel
        Me.C1SuperLabel5 = New C1.Win.C1SuperTooltip.C1SuperLabel
        Me.C1SuperLabel4 = New C1.Win.C1SuperTooltip.C1SuperLabel
        Me.C1SuperLabel22 = New C1.Win.C1SuperTooltip.C1SuperLabel
        Me.Pnl_Datos_TipoOperacion = New System.Windows.Forms.Panel
        Me.Btn_Cancelar = New C1.Win.C1Input.C1Button
        Me.Tdg_Unidades = New C1.Win.C1TrueDBGrid.C1TrueDBGrid
        Me.Btn_Accion = New C1.Win.C1Input.C1Button
        Me.Btn_Modificar = New C1.Win.C1Input.C1Button
        Me.Btn_Agregar = New C1.Win.C1Input.C1Button
        Me.Btn_Eliminar = New C1.Win.C1Input.C1Button
        Me.Btn_Salir = New C1.Win.C1Input.C1Button
        Me.C1SuperTooltip1 = New C1.Win.C1SuperTooltip.C1SuperTooltip(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip
        Me.StatusLabel = New System.Windows.Forms.ToolStripStatusLabel
        Me.Ctrl_Botonera = New System.Windows.Forms.ToolStrip
        Me.Ctrl_Botonera_Agregar = New System.Windows.Forms.ToolStripButton
        Me.Ctrl_Botonera_Buscar = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.Ctrl_Botonera_Guardar = New System.Windows.Forms.ToolStripButton
        Me.Ctrl_Botonera_Editar = New System.Windows.Forms.ToolStripButton
        Me.Ctrl_Botonera_Eliminar = New System.Windows.Forms.ToolStripButton
        Me.Ctrl_Botonera_Verificar = New System.Windows.Forms.ToolStripButton
        Me.Ctrl_Botonera_Procesar = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator
        Me.Ctrl_Botonera_Imprimir = New System.Windows.Forms.ToolStripButton
        Me.Ctrl_Botonera_Excel = New System.Windows.Forms.ToolStripButton
        Me.Ctrl_Botonera_Columnas = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator
        Me.Ctrl_Botonera_Cancelar = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator
        Me.Ctrl_Combo_Plantillas = New System.Windows.Forms.ToolStripComboBox
        Me.Pnl_DatosUnidad.SuspendLayout()
        CType(Me.Cmb_EstadoUnidad, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Txt_CodigoExternoUnidad, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Txt_NombreCortoUnidad, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Txt_NombreUnidad, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Pnl_Datos_TipoOperacion.SuspendLayout()
        CType(Me.Tdg_Unidades, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.Ctrl_Botonera.SuspendLayout()
        Me.SuspendLayout()
        '
        'Pnl_DatosUnidad
        '
        Me.Pnl_DatosUnidad.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Pnl_DatosUnidad.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Pnl_DatosUnidad.Controls.Add(Me.Cmb_EstadoUnidad)
        Me.Pnl_DatosUnidad.Controls.Add(Me.Txt_CodigoExternoUnidad)
        Me.Pnl_DatosUnidad.Controls.Add(Me.Txt_NombreCortoUnidad)
        Me.Pnl_DatosUnidad.Controls.Add(Me.Txt_NombreUnidad)
        Me.Pnl_DatosUnidad.Controls.Add(Me.C1SuperLabel8)
        Me.Pnl_DatosUnidad.Controls.Add(Me.C1SuperLabel5)
        Me.Pnl_DatosUnidad.Controls.Add(Me.C1SuperLabel4)
        Me.Pnl_DatosUnidad.Controls.Add(Me.C1SuperLabel22)
        Me.Pnl_DatosUnidad.Location = New System.Drawing.Point(1, 23)
        Me.Pnl_DatosUnidad.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.Pnl_DatosUnidad.Name = "Pnl_DatosUnidad"
        Me.Pnl_DatosUnidad.Size = New System.Drawing.Size(398, 76)
        Me.Pnl_DatosUnidad.TabIndex = 1015
        '
        'Cmb_EstadoUnidad
        '
        Me.Cmb_EstadoUnidad.AddItemSeparator = Global.Microsoft.VisualBasic.ChrW(59)
        Me.Cmb_EstadoUnidad.AllowColMove = False
        Me.Cmb_EstadoUnidad.AllowSort = False
        Me.Cmb_EstadoUnidad.AutoDropDown = True
        Me.Cmb_EstadoUnidad.AutoSelect = True
        Me.Cmb_EstadoUnidad.AutoSize = False
        Me.Cmb_EstadoUnidad.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Cmb_EstadoUnidad.Caption = ""
        Me.Cmb_EstadoUnidad.CaptionHeight = 18
        Me.Cmb_EstadoUnidad.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.Cmb_EstadoUnidad.ColumnCaptionHeight = 18
        Me.Cmb_EstadoUnidad.ColumnFooterHeight = 18
        Me.Cmb_EstadoUnidad.ColumnHeaders = False
        Me.Cmb_EstadoUnidad.ComboStyle = C1.Win.C1List.ComboStyleEnum.DropdownList
        Me.Cmb_EstadoUnidad.ContentHeight = 12
        Me.Cmb_EstadoUnidad.DataMode = C1.Win.C1List.DataModeEnum.AddItem
        Me.Cmb_EstadoUnidad.DeadAreaBackColor = System.Drawing.Color.Empty
        Me.Cmb_EstadoUnidad.EditorBackColor = System.Drawing.Color.White
        Me.Cmb_EstadoUnidad.EditorFont = New System.Drawing.Font("Arial", 7.0!, System.Drawing.FontStyle.Bold)
        Me.Cmb_EstadoUnidad.EditorForeColor = System.Drawing.Color.SteelBlue
        Me.Cmb_EstadoUnidad.EditorHeight = 12
        Me.Cmb_EstadoUnidad.ExtendRightColumn = True
        Me.Cmb_EstadoUnidad.FlatStyle = C1.Win.C1List.FlatModeEnum.Flat
        Me.Cmb_EstadoUnidad.Font = New System.Drawing.Font("Arial", 7.0!, System.Drawing.FontStyle.Bold)
        Me.Cmb_EstadoUnidad.Images.Add(CType(resources.GetObject("Cmb_EstadoUnidad.Images"), System.Drawing.Image))
        Me.Cmb_EstadoUnidad.ItemHeight = 18
        Me.Cmb_EstadoUnidad.Location = New System.Drawing.Point(288, 52)
        Me.Cmb_EstadoUnidad.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Cmb_EstadoUnidad.MatchEntry = C1.Win.C1List.MatchEntryEnum.Extended
        Me.Cmb_EstadoUnidad.MatchEntryTimeout = CType(500, Long)
        Me.Cmb_EstadoUnidad.MaxDropDownItems = CType(5, Short)
        Me.Cmb_EstadoUnidad.MaxLength = 0
        Me.Cmb_EstadoUnidad.MinimumSize = New System.Drawing.Size(10, 16)
        Me.Cmb_EstadoUnidad.MouseCursor = System.Windows.Forms.Cursors.Default
        Me.Cmb_EstadoUnidad.Name = "Cmb_EstadoUnidad"
        Me.Cmb_EstadoUnidad.RowSubDividerColor = System.Drawing.Color.DarkGray
        Me.Cmb_EstadoUnidad.Size = New System.Drawing.Size(103, 16)
        Me.Cmb_EstadoUnidad.TabIndex = 3
        Me.Cmb_EstadoUnidad.VisualStyle = C1.Win.C1List.VisualStyle.Office2007Silver
        Me.Cmb_EstadoUnidad.PropBag = resources.GetString("Cmb_EstadoUnidad.PropBag")
        '
        'Txt_CodigoExternoUnidad
        '
        Me.Txt_CodigoExternoUnidad.AcceptsEscape = False
        Me.Txt_CodigoExternoUnidad.AcceptsReturn = True
        Me.Txt_CodigoExternoUnidad.AcceptsTab = True
        Me.Txt_CodigoExternoUnidad.AutoSize = False
        Me.Txt_CodigoExternoUnidad.BackColor = System.Drawing.Color.White
        Me.Txt_CodigoExternoUnidad.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Txt_CodigoExternoUnidad.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.Txt_CodigoExternoUnidad.DisabledForeColor = System.Drawing.Color.Lavender
        Me.Txt_CodigoExternoUnidad.Font = New System.Drawing.Font("Arial", 7.0!, System.Drawing.FontStyle.Bold)
        Me.Txt_CodigoExternoUnidad.ForeColor = System.Drawing.Color.SteelBlue
        Me.Txt_CodigoExternoUnidad.Location = New System.Drawing.Point(149, 52)
        Me.Txt_CodigoExternoUnidad.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.Txt_CodigoExternoUnidad.MaxLength = 20
        Me.Txt_CodigoExternoUnidad.Name = "Txt_CodigoExternoUnidad"
        Me.Txt_CodigoExternoUnidad.NumericInput = False
        Me.Txt_CodigoExternoUnidad.ReadOnly = True
        Me.Txt_CodigoExternoUnidad.Size = New System.Drawing.Size(136, 16)
        Me.Txt_CodigoExternoUnidad.TabIndex = 2
        Me.Txt_CodigoExternoUnidad.TabStop = False
        Me.Txt_CodigoExternoUnidad.Tag = Nothing
        Me.Txt_CodigoExternoUnidad.TextDetached = True
        Me.Txt_CodigoExternoUnidad.Value = " "
        Me.Txt_CodigoExternoUnidad.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Silver
        '
        'Txt_NombreCortoUnidad
        '
        Me.Txt_NombreCortoUnidad.AcceptsEscape = False
        Me.Txt_NombreCortoUnidad.AcceptsReturn = True
        Me.Txt_NombreCortoUnidad.AcceptsTab = True
        Me.Txt_NombreCortoUnidad.AutoSize = False
        Me.Txt_NombreCortoUnidad.BackColor = System.Drawing.Color.White
        Me.Txt_NombreCortoUnidad.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Txt_NombreCortoUnidad.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.Txt_NombreCortoUnidad.DisabledForeColor = System.Drawing.Color.Lavender
        Me.Txt_NombreCortoUnidad.Font = New System.Drawing.Font("Arial", 7.0!, System.Drawing.FontStyle.Bold)
        Me.Txt_NombreCortoUnidad.ForeColor = System.Drawing.Color.SteelBlue
        Me.Txt_NombreCortoUnidad.Location = New System.Drawing.Point(8, 52)
        Me.Txt_NombreCortoUnidad.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.Txt_NombreCortoUnidad.MaxLength = 15
        Me.Txt_NombreCortoUnidad.Name = "Txt_NombreCortoUnidad"
        Me.Txt_NombreCortoUnidad.NumericInput = False
        Me.Txt_NombreCortoUnidad.ReadOnly = True
        Me.Txt_NombreCortoUnidad.Size = New System.Drawing.Size(138, 16)
        Me.Txt_NombreCortoUnidad.TabIndex = 1
        Me.Txt_NombreCortoUnidad.TabStop = False
        Me.Txt_NombreCortoUnidad.Tag = Nothing
        Me.Txt_NombreCortoUnidad.TextDetached = True
        Me.Txt_NombreCortoUnidad.Value = " "
        Me.Txt_NombreCortoUnidad.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Silver
        '
        'Txt_NombreUnidad
        '
        Me.Txt_NombreUnidad.AcceptsEscape = False
        Me.Txt_NombreUnidad.AcceptsReturn = True
        Me.Txt_NombreUnidad.AcceptsTab = True
        Me.Txt_NombreUnidad.AutoSize = False
        Me.Txt_NombreUnidad.BackColor = System.Drawing.Color.White
        Me.Txt_NombreUnidad.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Txt_NombreUnidad.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.Txt_NombreUnidad.DisabledForeColor = System.Drawing.Color.Lavender
        Me.Txt_NombreUnidad.Font = New System.Drawing.Font("Arial", 7.0!, System.Drawing.FontStyle.Bold)
        Me.Txt_NombreUnidad.ForeColor = System.Drawing.Color.SteelBlue
        Me.Txt_NombreUnidad.Location = New System.Drawing.Point(8, 20)
        Me.Txt_NombreUnidad.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.Txt_NombreUnidad.MaxLength = 50
        Me.Txt_NombreUnidad.Name = "Txt_NombreUnidad"
        Me.Txt_NombreUnidad.NumericInput = False
        Me.Txt_NombreUnidad.ReadOnly = True
        Me.Txt_NombreUnidad.Size = New System.Drawing.Size(383, 16)
        Me.Txt_NombreUnidad.TabIndex = 0
        Me.Txt_NombreUnidad.TabStop = False
        Me.Txt_NombreUnidad.Tag = Nothing
        Me.Txt_NombreUnidad.TextDetached = True
        Me.Txt_NombreUnidad.Value = " "
        Me.Txt_NombreUnidad.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Silver
        '
        'C1SuperLabel8
        '
        Me.C1SuperLabel8.AutoSize = True
        Me.C1SuperLabel8.BackColor = System.Drawing.Color.Transparent
        Me.C1SuperLabel8.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.C1SuperLabel8.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.C1SuperLabel8.Location = New System.Drawing.Point(147, 36)
        Me.C1SuperLabel8.Margin = New System.Windows.Forms.Padding(2, 4, 2, 4)
        Me.C1SuperLabel8.Name = "C1SuperLabel8"
        Me.C1SuperLabel8.Size = New System.Drawing.Size(79, 19)
        Me.C1SuperLabel8.TabIndex = 1050
        Me.C1SuperLabel8.Text = "Código Externo"
        Me.C1SuperLabel8.UseMnemonic = True
        '
        'C1SuperLabel5
        '
        Me.C1SuperLabel5.AutoSize = True
        Me.C1SuperLabel5.BackColor = System.Drawing.Color.Transparent
        Me.C1SuperLabel5.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.C1SuperLabel5.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.C1SuperLabel5.Location = New System.Drawing.Point(286, 36)
        Me.C1SuperLabel5.Margin = New System.Windows.Forms.Padding(2, 4, 2, 4)
        Me.C1SuperLabel5.Name = "C1SuperLabel5"
        Me.C1SuperLabel5.Size = New System.Drawing.Size(38, 19)
        Me.C1SuperLabel5.TabIndex = 1048
        Me.C1SuperLabel5.Text = "Estado"
        Me.C1SuperLabel5.UseMnemonic = True
        '
        'C1SuperLabel4
        '
        Me.C1SuperLabel4.AutoSize = True
        Me.C1SuperLabel4.BackColor = System.Drawing.Color.Transparent
        Me.C1SuperLabel4.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.C1SuperLabel4.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.C1SuperLabel4.Location = New System.Drawing.Point(6, 36)
        Me.C1SuperLabel4.Margin = New System.Windows.Forms.Padding(2, 4, 2, 4)
        Me.C1SuperLabel4.Name = "C1SuperLabel4"
        Me.C1SuperLabel4.Size = New System.Drawing.Size(71, 19)
        Me.C1SuperLabel4.TabIndex = 1047
        Me.C1SuperLabel4.Text = "Nombre Corto"
        Me.C1SuperLabel4.UseMnemonic = True
        '
        'C1SuperLabel22
        '
        Me.C1SuperLabel22.AutoSize = True
        Me.C1SuperLabel22.BackColor = System.Drawing.Color.Transparent
        Me.C1SuperLabel22.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.C1SuperLabel22.Location = New System.Drawing.Point(6, 4)
        Me.C1SuperLabel22.Margin = New System.Windows.Forms.Padding(2, 4, 2, 4)
        Me.C1SuperLabel22.Name = "C1SuperLabel22"
        Me.C1SuperLabel22.Size = New System.Drawing.Size(43, 19)
        Me.C1SuperLabel22.TabIndex = 1044
        Me.C1SuperLabel22.Text = "Nombre"
        Me.C1SuperLabel22.UseMnemonic = True
        '
        'Pnl_Datos_TipoOperacion
        '
        Me.Pnl_Datos_TipoOperacion.BackColor = System.Drawing.Color.Gainsboro
        Me.Pnl_Datos_TipoOperacion.Controls.Add(Me.Btn_Cancelar)
        Me.Pnl_Datos_TipoOperacion.Controls.Add(Me.Tdg_Unidades)
        Me.Pnl_Datos_TipoOperacion.Controls.Add(Me.Btn_Accion)
        Me.Pnl_Datos_TipoOperacion.Controls.Add(Me.Btn_Modificar)
        Me.Pnl_Datos_TipoOperacion.Controls.Add(Me.Btn_Agregar)
        Me.Pnl_Datos_TipoOperacion.Controls.Add(Me.Btn_Eliminar)
        Me.Pnl_Datos_TipoOperacion.Location = New System.Drawing.Point(1, 99)
        Me.Pnl_Datos_TipoOperacion.Margin = New System.Windows.Forms.Padding(2)
        Me.Pnl_Datos_TipoOperacion.Name = "Pnl_Datos_TipoOperacion"
        Me.Pnl_Datos_TipoOperacion.Size = New System.Drawing.Size(398, 193)
        Me.Pnl_Datos_TipoOperacion.TabIndex = 0
        Me.Pnl_Datos_TipoOperacion.TabStop = True
        '
        'Btn_Cancelar
        '
        Me.Btn_Cancelar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.Btn_Cancelar.Location = New System.Drawing.Point(337, 4)
        Me.Btn_Cancelar.Margin = New System.Windows.Forms.Padding(2)
        Me.Btn_Cancelar.Name = "Btn_Cancelar"
        Me.Btn_Cancelar.Size = New System.Drawing.Size(54, 21)
        Me.Btn_Cancelar.TabIndex = 6
        Me.Btn_Cancelar.TabStop = False
        Me.Btn_Cancelar.Text = "&Cancelar"
        Me.Btn_Cancelar.UseVisualStyleBackColor = True
        Me.Btn_Cancelar.Visible = False
        Me.Btn_Cancelar.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Silver
        '
        'Tdg_Unidades
        '
        Me.Tdg_Unidades.AllowColSelect = False
        Me.Tdg_Unidades.AllowRowSizing = C1.Win.C1TrueDBGrid.RowSizingEnum.None
        Me.Tdg_Unidades.AllowUpdate = False
        Me.Tdg_Unidades.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Tdg_Unidades.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Tdg_Unidades.Caption = "U N I D A D E S"
        Me.Tdg_Unidades.CaptionHeight = 21
        Me.Tdg_Unidades.DirectionAfterEnter = C1.Win.C1TrueDBGrid.DirectionAfterEnterEnum.MoveDown
        Me.Tdg_Unidades.Font = New System.Drawing.Font("Arial", 7.5!)
        Me.Tdg_Unidades.ForeColor = System.Drawing.Color.Lavender
        Me.Tdg_Unidades.GroupByAreaVisible = False
        Me.Tdg_Unidades.GroupByCaption = "Arrastre hasta aquí las columnas por las que desea agrupar"
        Me.Tdg_Unidades.Images.Add(CType(resources.GetObject("Tdg_Unidades.Images"), System.Drawing.Image))
        Me.Tdg_Unidades.Location = New System.Drawing.Point(0, 28)
        Me.Tdg_Unidades.MaintainRowCurrency = True
        Me.Tdg_Unidades.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.Tdg_Unidades.MarqueeStyle = C1.Win.C1TrueDBGrid.MarqueeEnum.HighlightRow
        Me.Tdg_Unidades.MultiSelect = C1.Win.C1TrueDBGrid.MultiSelectEnum.Simple
        Me.Tdg_Unidades.Name = "Tdg_Unidades"
        Me.Tdg_Unidades.PreviewInfo.Location = New System.Drawing.Point(0, 0)
        Me.Tdg_Unidades.PreviewInfo.Size = New System.Drawing.Size(0, 0)
        Me.Tdg_Unidades.PreviewInfo.ZoomFactor = 75
        Me.Tdg_Unidades.PrintInfo.PageSettings = CType(resources.GetObject("Tdg_Unidades.PrintInfo.PageSettings"), System.Drawing.Printing.PageSettings)
        Me.Tdg_Unidades.RecordSelectors = False
        Me.Tdg_Unidades.RecordSelectorWidth = 15
        Me.Tdg_Unidades.RowDivider.Color = System.Drawing.Color.DarkGray
        Me.Tdg_Unidades.RowDivider.Style = C1.Win.C1TrueDBGrid.LineStyleEnum.Raised
        Me.Tdg_Unidades.RowHeight = 15
        Me.Tdg_Unidades.ScrollTips = True
        Me.Tdg_Unidades.Size = New System.Drawing.Size(397, 164)
        Me.Tdg_Unidades.SplitDividerSize = New System.Drawing.Size(0, 0)
        Me.Tdg_Unidades.TabIndex = 5
        Me.Tdg_Unidades.TabStop = False
        Me.Tdg_Unidades.Text = "C1TrueDBGrid1"
        Me.Tdg_Unidades.VisualStyle = C1.Win.C1TrueDBGrid.VisualStyle.Office2007Silver
        Me.Tdg_Unidades.PropBag = resources.GetString("Tdg_Unidades.PropBag")
        '
        'Btn_Accion
        '
        Me.Btn_Accion.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.Btn_Accion.Location = New System.Drawing.Point(282, 4)
        Me.Btn_Accion.Margin = New System.Windows.Forms.Padding(2)
        Me.Btn_Accion.Name = "Btn_Accion"
        Me.Btn_Accion.Size = New System.Drawing.Size(54, 21)
        Me.Btn_Accion.TabIndex = 4
        Me.Btn_Accion.TabStop = False
        Me.Btn_Accion.Text = "&Grabar"
        Me.Btn_Accion.UseVisualStyleBackColor = True
        Me.Btn_Accion.Visible = False
        Me.Btn_Accion.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Silver
        '
        'Btn_Modificar
        '
        Me.Btn_Modificar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.Btn_Modificar.Enabled = False
        Me.Btn_Modificar.Font = New System.Drawing.Font("Arial", 7.5!)
        Me.Btn_Modificar.Location = New System.Drawing.Point(212, 5)
        Me.Btn_Modificar.Margin = New System.Windows.Forms.Padding(2)
        Me.Btn_Modificar.Name = "Btn_Modificar"
        Me.Btn_Modificar.Size = New System.Drawing.Size(24, 20)
        Me.Btn_Modificar.TabIndex = 3
        Me.Btn_Modificar.TabStop = False
        Me.Btn_Modificar.Tag = ""
        Me.Btn_Modificar.Text = "M"
        Me.C1SuperTooltip1.SetToolTip(Me.Btn_Modificar, "Modifica registro seleccionado")
        Me.Btn_Modificar.UseVisualStyleBackColor = True
        Me.Btn_Modificar.Visible = False
        Me.Btn_Modificar.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Silver
        '
        'Btn_Agregar
        '
        Me.Btn_Agregar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.Btn_Agregar.Font = New System.Drawing.Font("Arial", 7.5!)
        Me.Btn_Agregar.Location = New System.Drawing.Point(162, 5)
        Me.Btn_Agregar.Margin = New System.Windows.Forms.Padding(2)
        Me.Btn_Agregar.Name = "Btn_Agregar"
        Me.Btn_Agregar.Size = New System.Drawing.Size(24, 20)
        Me.Btn_Agregar.TabIndex = 1
        Me.Btn_Agregar.TabStop = False
        Me.Btn_Agregar.Tag = ""
        Me.Btn_Agregar.Text = "+"
        Me.C1SuperTooltip1.SetToolTip(Me.Btn_Agregar, "Crea nuevo registro")
        Me.Btn_Agregar.UseVisualStyleBackColor = True
        Me.Btn_Agregar.Visible = False
        Me.Btn_Agregar.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Silver
        '
        'Btn_Eliminar
        '
        Me.Btn_Eliminar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.Btn_Eliminar.Enabled = False
        Me.Btn_Eliminar.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.Btn_Eliminar.Location = New System.Drawing.Point(187, 5)
        Me.Btn_Eliminar.Margin = New System.Windows.Forms.Padding(2)
        Me.Btn_Eliminar.Name = "Btn_Eliminar"
        Me.Btn_Eliminar.Size = New System.Drawing.Size(24, 20)
        Me.Btn_Eliminar.TabIndex = 2
        Me.Btn_Eliminar.TabStop = False
        Me.Btn_Eliminar.Tag = ""
        Me.Btn_Eliminar.Text = "─"
        Me.C1SuperTooltip1.SetToolTip(Me.Btn_Eliminar, "Elimina registro seleccionado")
        Me.Btn_Eliminar.UseVisualStyleBackColor = True
        Me.Btn_Eliminar.Visible = False
        Me.Btn_Eliminar.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Silver
        '
        'Btn_Salir
        '
        Me.Btn_Salir.BackColor = System.Drawing.Color.Transparent
        Me.Btn_Salir.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.Btn_Salir.FlatAppearance.BorderSize = 0
        Me.Btn_Salir.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray
        Me.Btn_Salir.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray
        Me.Btn_Salir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Btn_Salir.Font = New System.Drawing.Font("Arial", 8.25!)
        Me.Btn_Salir.ForeColor = System.Drawing.Color.White
        Me.Btn_Salir.Location = New System.Drawing.Point(346, 0)
        Me.Btn_Salir.Margin = New System.Windows.Forms.Padding(2)
        Me.Btn_Salir.Name = "Btn_Salir"
        Me.Btn_Salir.Size = New System.Drawing.Size(53, 22)
        Me.Btn_Salir.TabIndex = 1032
        Me.Btn_Salir.TabStop = False
        Me.Btn_Salir.Text = "&Salir  |x|"
        Me.Btn_Salir.UseVisualStyleBackColor = False
        '
        'C1SuperTooltip1
        '
        Me.C1SuperTooltip1.Font = New System.Drawing.Font("Tahoma", 8.0!)
        Me.C1SuperTooltip1.RoundedCorners = True
        '
        'StatusStrip1
        '
        Me.StatusStrip1.AutoSize = False
        Me.StatusStrip1.BackColor = System.Drawing.Color.DarkGray
        Me.StatusStrip1.Dock = System.Windows.Forms.DockStyle.None
        Me.StatusStrip1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.StatusLabel})
        Me.StatusStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Flow
        Me.StatusStrip1.Location = New System.Drawing.Point(1, 293)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(398, 18)
        Me.StatusStrip1.Stretch = False
        Me.StatusStrip1.TabIndex = 1033
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'StatusLabel
        '
        Me.StatusLabel.BackColor = System.Drawing.Color.Transparent
        Me.StatusLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Bold)
        Me.StatusLabel.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.StatusLabel.Name = "StatusLabel"
        Me.StatusLabel.Size = New System.Drawing.Size(128, 14)
        Me.StatusLabel.Text = "ToolStripStatusLabel1"
        '
        'Ctrl_Botonera
        '
        Me.Ctrl_Botonera.BackColor = System.Drawing.Color.DarkGray
        Me.Ctrl_Botonera.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Ctrl_Botonera_Agregar, Me.Ctrl_Botonera_Buscar, Me.ToolStripSeparator1, Me.Ctrl_Botonera_Guardar, Me.Ctrl_Botonera_Editar, Me.Ctrl_Botonera_Eliminar, Me.Ctrl_Botonera_Verificar, Me.Ctrl_Botonera_Procesar, Me.ToolStripSeparator2, Me.Ctrl_Botonera_Imprimir, Me.Ctrl_Botonera_Excel, Me.Ctrl_Botonera_Columnas, Me.ToolStripSeparator3, Me.Ctrl_Botonera_Cancelar, Me.ToolStripSeparator4, Me.Ctrl_Combo_Plantillas})
        Me.Ctrl_Botonera.Location = New System.Drawing.Point(0, 0)
        Me.Ctrl_Botonera.Name = "Ctrl_Botonera"
        Me.Ctrl_Botonera.Size = New System.Drawing.Size(400, 25)
        Me.Ctrl_Botonera.TabIndex = 1069
        Me.Ctrl_Botonera.Text = "ToolStrip1"
        '
        'Ctrl_Botonera_Agregar
        '
        Me.Ctrl_Botonera_Agregar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Ctrl_Botonera_Agregar.Image = CType(resources.GetObject("Ctrl_Botonera_Agregar.Image"), System.Drawing.Image)
        Me.Ctrl_Botonera_Agregar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.Ctrl_Botonera_Agregar.Name = "Ctrl_Botonera_Agregar"
        Me.Ctrl_Botonera_Agregar.Size = New System.Drawing.Size(23, 22)
        Me.Ctrl_Botonera_Agregar.Text = "ToolStripButton2"
        Me.Ctrl_Botonera_Agregar.ToolTipText = "Agregar"
        '
        'Ctrl_Botonera_Buscar
        '
        Me.Ctrl_Botonera_Buscar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Ctrl_Botonera_Buscar.Enabled = False
        Me.Ctrl_Botonera_Buscar.Image = CType(resources.GetObject("Ctrl_Botonera_Buscar.Image"), System.Drawing.Image)
        Me.Ctrl_Botonera_Buscar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.Ctrl_Botonera_Buscar.Name = "Ctrl_Botonera_Buscar"
        Me.Ctrl_Botonera_Buscar.Size = New System.Drawing.Size(23, 22)
        Me.Ctrl_Botonera_Buscar.Text = "Buscar"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'Ctrl_Botonera_Guardar
        '
        Me.Ctrl_Botonera_Guardar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Ctrl_Botonera_Guardar.Image = CType(resources.GetObject("Ctrl_Botonera_Guardar.Image"), System.Drawing.Image)
        Me.Ctrl_Botonera_Guardar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.Ctrl_Botonera_Guardar.Name = "Ctrl_Botonera_Guardar"
        Me.Ctrl_Botonera_Guardar.Size = New System.Drawing.Size(23, 22)
        Me.Ctrl_Botonera_Guardar.Text = "Guardar"
        '
        'Ctrl_Botonera_Editar
        '
        Me.Ctrl_Botonera_Editar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Ctrl_Botonera_Editar.Image = CType(resources.GetObject("Ctrl_Botonera_Editar.Image"), System.Drawing.Image)
        Me.Ctrl_Botonera_Editar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.Ctrl_Botonera_Editar.Name = "Ctrl_Botonera_Editar"
        Me.Ctrl_Botonera_Editar.Size = New System.Drawing.Size(23, 22)
        Me.Ctrl_Botonera_Editar.Text = "Editar"
        '
        'Ctrl_Botonera_Eliminar
        '
        Me.Ctrl_Botonera_Eliminar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Ctrl_Botonera_Eliminar.Image = CType(resources.GetObject("Ctrl_Botonera_Eliminar.Image"), System.Drawing.Image)
        Me.Ctrl_Botonera_Eliminar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.Ctrl_Botonera_Eliminar.Name = "Ctrl_Botonera_Eliminar"
        Me.Ctrl_Botonera_Eliminar.Size = New System.Drawing.Size(23, 22)
        Me.Ctrl_Botonera_Eliminar.Text = "Eliminar"
        '
        'Ctrl_Botonera_Verificar
        '
        Me.Ctrl_Botonera_Verificar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Ctrl_Botonera_Verificar.Enabled = False
        Me.Ctrl_Botonera_Verificar.Image = CType(resources.GetObject("Ctrl_Botonera_Verificar.Image"), System.Drawing.Image)
        Me.Ctrl_Botonera_Verificar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.Ctrl_Botonera_Verificar.Name = "Ctrl_Botonera_Verificar"
        Me.Ctrl_Botonera_Verificar.Size = New System.Drawing.Size(23, 22)
        Me.Ctrl_Botonera_Verificar.Text = "Verificar"
        '
        'Ctrl_Botonera_Procesar
        '
        Me.Ctrl_Botonera_Procesar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Ctrl_Botonera_Procesar.Enabled = False
        Me.Ctrl_Botonera_Procesar.Image = CType(resources.GetObject("Ctrl_Botonera_Procesar.Image"), System.Drawing.Image)
        Me.Ctrl_Botonera_Procesar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.Ctrl_Botonera_Procesar.Name = "Ctrl_Botonera_Procesar"
        Me.Ctrl_Botonera_Procesar.Size = New System.Drawing.Size(23, 22)
        Me.Ctrl_Botonera_Procesar.Text = "Procesar"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'Ctrl_Botonera_Imprimir
        '
        Me.Ctrl_Botonera_Imprimir.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Ctrl_Botonera_Imprimir.Enabled = False
        Me.Ctrl_Botonera_Imprimir.Image = CType(resources.GetObject("Ctrl_Botonera_Imprimir.Image"), System.Drawing.Image)
        Me.Ctrl_Botonera_Imprimir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.Ctrl_Botonera_Imprimir.Name = "Ctrl_Botonera_Imprimir"
        Me.Ctrl_Botonera_Imprimir.Size = New System.Drawing.Size(23, 22)
        Me.Ctrl_Botonera_Imprimir.Text = "Imprimir"
        Me.Ctrl_Botonera_Imprimir.ToolTipText = "Imprimir"
        '
        'Ctrl_Botonera_Excel
        '
        Me.Ctrl_Botonera_Excel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Ctrl_Botonera_Excel.Enabled = False
        Me.Ctrl_Botonera_Excel.Image = CType(resources.GetObject("Ctrl_Botonera_Excel.Image"), System.Drawing.Image)
        Me.Ctrl_Botonera_Excel.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.Ctrl_Botonera_Excel.Name = "Ctrl_Botonera_Excel"
        Me.Ctrl_Botonera_Excel.Size = New System.Drawing.Size(23, 22)
        Me.Ctrl_Botonera_Excel.Text = "Exportar a Excel"
        '
        'Ctrl_Botonera_Columnas
        '
        Me.Ctrl_Botonera_Columnas.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Ctrl_Botonera_Columnas.Enabled = False
        Me.Ctrl_Botonera_Columnas.Image = CType(resources.GetObject("Ctrl_Botonera_Columnas.Image"), System.Drawing.Image)
        Me.Ctrl_Botonera_Columnas.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.Ctrl_Botonera_Columnas.Name = "Ctrl_Botonera_Columnas"
        Me.Ctrl_Botonera_Columnas.Size = New System.Drawing.Size(23, 22)
        Me.Ctrl_Botonera_Columnas.Text = "Configuración Columnas"
        Me.Ctrl_Botonera_Columnas.ToolTipText = "Configuración Columnas"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(6, 25)
        '
        'Ctrl_Botonera_Cancelar
        '
        Me.Ctrl_Botonera_Cancelar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Ctrl_Botonera_Cancelar.Image = CType(resources.GetObject("Ctrl_Botonera_Cancelar.Image"), System.Drawing.Image)
        Me.Ctrl_Botonera_Cancelar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.Ctrl_Botonera_Cancelar.Name = "Ctrl_Botonera_Cancelar"
        Me.Ctrl_Botonera_Cancelar.Size = New System.Drawing.Size(23, 22)
        Me.Ctrl_Botonera_Cancelar.Text = "Cancelar"
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(6, 25)
        '
        'Ctrl_Combo_Plantillas
        '
        Me.Ctrl_Combo_Plantillas.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.Ctrl_Combo_Plantillas.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Ctrl_Combo_Plantillas.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Ctrl_Combo_Plantillas.Name = "Ctrl_Combo_Plantillas"
        Me.Ctrl_Combo_Plantillas.Size = New System.Drawing.Size(121, 21)
        Me.Ctrl_Combo_Plantillas.Visible = False
        '
        'Frm_MantenedorUnidades
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(5.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.DimGray
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.ClientSize = New System.Drawing.Size(400, 312)
        Me.Controls.Add(Me.Ctrl_Botonera)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.Btn_Salir)
        Me.Controls.Add(Me.Pnl_Datos_TipoOperacion)
        Me.Controls.Add(Me.Pnl_DatosUnidad)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.ForeColor = System.Drawing.Color.DimGray
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.MaximizeBox = False
        Me.Name = "Frm_MantenedorUnidades"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Mantenedor Unidades"
        Me.Pnl_DatosUnidad.ResumeLayout(False)
        Me.Pnl_DatosUnidad.PerformLayout()
        CType(Me.Cmb_EstadoUnidad, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Txt_CodigoExternoUnidad, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Txt_NombreCortoUnidad, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Txt_NombreUnidad, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Pnl_Datos_TipoOperacion.ResumeLayout(False)
        CType(Me.Tdg_Unidades, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.Ctrl_Botonera.ResumeLayout(False)
        Me.Ctrl_Botonera.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Public Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub
    Friend WithEvents Pnl_DatosUnidad As System.Windows.Forms.Panel
    Friend WithEvents Txt_CodigoExternoUnidad As C1.Win.C1Input.C1TextBox
    Friend WithEvents Txt_NombreCortoUnidad As C1.Win.C1Input.C1TextBox
    Friend WithEvents Txt_NombreUnidad As C1.Win.C1Input.C1TextBox
    Friend WithEvents C1SuperLabel8 As C1.Win.C1SuperTooltip.C1SuperLabel
    Friend WithEvents C1SuperLabel5 As C1.Win.C1SuperTooltip.C1SuperLabel
    Friend WithEvents C1SuperLabel4 As C1.Win.C1SuperTooltip.C1SuperLabel
    Friend WithEvents C1SuperLabel22 As C1.Win.C1SuperTooltip.C1SuperLabel
    Friend WithEvents Pnl_Datos_TipoOperacion As System.Windows.Forms.Panel
    Friend WithEvents Tdg_Unidades As C1.Win.C1TrueDBGrid.C1TrueDBGrid
    Friend WithEvents Btn_Accion As C1.Win.C1Input.C1Button
    Friend WithEvents Btn_Modificar As C1.Win.C1Input.C1Button
    Friend WithEvents Btn_Agregar As C1.Win.C1Input.C1Button
    Friend WithEvents Btn_Eliminar As C1.Win.C1Input.C1Button
    Friend WithEvents Btn_Salir As C1.Win.C1Input.C1Button
    Friend WithEvents Btn_Cancelar As C1.Win.C1Input.C1Button
    Friend WithEvents Cmb_EstadoUnidad As C1.Win.C1List.C1Combo
    Friend WithEvents C1SuperTooltip1 As C1.Win.C1SuperTooltip.C1SuperTooltip
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents StatusLabel As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents Ctrl_Botonera As System.Windows.Forms.ToolStrip
    Friend WithEvents Ctrl_Botonera_Agregar As System.Windows.Forms.ToolStripButton
    Friend WithEvents Ctrl_Botonera_Buscar As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents Ctrl_Botonera_Guardar As System.Windows.Forms.ToolStripButton
    Friend WithEvents Ctrl_Botonera_Editar As System.Windows.Forms.ToolStripButton
    Friend WithEvents Ctrl_Botonera_Eliminar As System.Windows.Forms.ToolStripButton
    Friend WithEvents Ctrl_Botonera_Verificar As System.Windows.Forms.ToolStripButton
    Friend WithEvents Ctrl_Botonera_Procesar As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents Ctrl_Botonera_Imprimir As System.Windows.Forms.ToolStripButton
    Friend WithEvents Ctrl_Botonera_Excel As System.Windows.Forms.ToolStripButton
    Friend WithEvents Ctrl_Botonera_Columnas As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents Ctrl_Botonera_Cancelar As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents Ctrl_Combo_Plantillas As System.Windows.Forms.ToolStripComboBox
End Class
