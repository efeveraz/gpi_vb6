Imports C1.Win.C1TrueDBGrid
Imports C1.Win.C1List.C1Combo
Public Class Frm_MantenedorUnidades

    Dim ltxtColumnas As String = ""
    Dim ltxtOperacion As String = ""
    Dim lstrResultadoTransaccion As String = ""

    Dim lobjUnidad As ClsUnidad = New ClsUnidad
    Dim DS_Unidades As DataSet

    Dim lblnMoviendoElFormulario As Boolean = False
    Dim lMouseCoordenadaX As Integer = 0
    Dim lMouseCoordenadaY As Integer = 0

#Region " --- Formulario ---"

    Public Sub CargarFormulario()

        'Me.Location = New Point(gobjFormulario._PosX, gobjFormulario._PosY)
        Me.Location = New Point(3, 3)
        If gobjFormulario._Modal Then
            ShowDialog()
        Else
            Show()
            BringToFront()
        End If
    End Sub
    Private Sub AbrirFormulario(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call CargaGrillaUnidades()
        Call InicializaFormulario()
    End Sub
    Private Sub Frm_MantenedorUnidades_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.GotFocus, Me.Click, Me.Click
        Me.BringToFront()
    End Sub
    Private Sub Me_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If (e.KeyCode = Keys.F4) And e.Modifiers = Keys.Alt Then
            e.Handled = True
            Call Btn_Salir_Click(sender, e)
        ElseIf e.KeyCode = Keys.Enter Then
            e.Handled = True
            SendKeys.Send("{TAB}")
        ElseIf e.KeyCode = Keys.Escape Then
            e.Handled = True
            Call Btn_Cancelar_Click(sender, e)
        End If
    End Sub

    Private Sub Me_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.DoubleClick, Me.DoubleClick
        Me.Location = New Point(3, 3)
    End Sub
    Private Sub Me_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Me.Dispose()
    End Sub
    Private Sub Btn_Salir_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Salir.Click
        Me.Close()
    End Sub
    Private Sub Formulario_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles Me.MouseDown, Me.MouseDown
        If e.Button = MouseButtons.Left Then
            lblnMoviendoElFormulario = True
            lMouseCoordenadaX = e.X
            lMouseCoordenadaY = e.Y
        End If
    End Sub
    Private Sub Formulario_MouseUp(ByVal sender As Object, ByVal e As MouseEventArgs) Handles Me.MouseUp, Me.MouseUp
        If e.Button = MouseButtons.Left Then
            lblnMoviendoElFormulario = False
        End If
    End Sub
    Private Sub Formulario_MouseMove(ByVal sender As Object, ByVal e As MouseEventArgs) Handles Me.MouseMove, Me.MouseMove
        If lblnMoviendoElFormulario Then
            '...Arriba
            If Me.Location.Y < 1 Then
                Me.Location = New Point(Me.Location.X, 1)
                lblnMoviendoElFormulario = False
                '...Abajo
            ElseIf Me.Location.Y > (sender.parent.Height - 30) Then
                Me.Location = New Point(Me.Location.X, sender.parent.Height - 30)
                lblnMoviendoElFormulario = False
                '...Izquierda
            ElseIf Me.Location.X < ((Me.Width / 10) - Me.Width) Then
                Me.Location = New Point(((Me.Width / 10) - Me.Width), Me.Location.Y)
                lblnMoviendoElFormulario = False
                '...Derecha
            ElseIf Me.Location.X > (sender.parent.Width - 150) Then
                Me.Location = New Point((sender.parent.Width - 150), Me.Location.Y)
                lblnMoviendoElFormulario = False
            Else
                lblnMoviendoElFormulario = True
                Dim temp As Point = New Point()
                temp.X = Me.Location.X + (e.X - lMouseCoordenadaX)
                temp.Y = Me.Location.Y + (e.Y - lMouseCoordenadaY)
                Me.Location = temp
                temp = Nothing
            End If
        End If
    End Sub

#End Region

#Region " --- Eventos Formulario ---"

    Private Sub CargaEstado()

        Cmb_EstadoUnidad.ClearItems()
        Cmb_EstadoUnidad.AddItem("VIG;VIGENTE")
        Cmb_EstadoUnidad.AddItem("BLO;BLOQUEADO")
        Cmb_EstadoUnidad.AddItem("ELI;ELIMINADO")

        Cmb_EstadoUnidad.Splits(0).DisplayColumns(0).Visible = False
        Cmb_EstadoUnidad.Splits(0).DisplayColumns(1).Visible = True
    End Sub

    Private Sub InicializaFormulario()
        Call CargaEstado()
        ltxtOperacion = ""

        Call HabilitaDeshabilitaDatos()

        Cmb_EstadoUnidad.SelectedIndex = -1

        Limpiar_Campos()

        Tdg_Unidades.SelectedRows.Clear()
        Tdg_Unidades.Row = -1
        Btn_Agregar.Enabled = True
        Ctrl_Botonera_Agregar.Enabled = True

        Btn_Accion.Enabled = False
        Ctrl_Botonera_Guardar.Enabled = False

        Btn_Cancelar.Enabled = Btn_Accion.Enabled
        Ctrl_Botonera_Cancelar.Enabled = Ctrl_Botonera_Guardar.Enabled

        Btn_Modificar.Enabled = False
        Ctrl_Botonera_Editar.Enabled = False

        Btn_Eliminar.Enabled = False
        Ctrl_Botonera_Eliminar.Enabled = False

        Tdg_Unidades.Focus()
    End Sub
    Private Sub HabilitaDeshabilitaDatos()
        Select Case ltxtOperacion.Trim
            Case Is = "INSERTAR"
                gSubHabilitaControl(Txt_NombreUnidad, False, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Txt_NombreCortoUnidad, False, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Txt_CodigoExternoUnidad, False, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Cmb_EstadoUnidad, True, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
            Case Is = "MODIFICAR"
                gSubHabilitaControl(Txt_NombreUnidad, True, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Txt_NombreCortoUnidad, False, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Txt_CodigoExternoUnidad, False, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Cmb_EstadoUnidad, False, gColorBackGroundEnabled, gColorBackGroundDesabled, True)

            Case Is = "ELIMINAR", ""
                gSubHabilitaControl(Txt_NombreUnidad, True, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Txt_NombreCortoUnidad, True, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Txt_CodigoExternoUnidad, True, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Cmb_EstadoUnidad, True, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
        End Select
    End Sub
    Public Sub Limpiar_Campos()
        Txt_NombreUnidad.Text = ""
        Txt_NombreCortoUnidad.Text = ""
        Txt_CodigoExternoUnidad.Text = ""
        Cmb_EstadoUnidad.SelectedIndex = -1
    End Sub
    Private Function ValidarCamposOK() As Boolean
        Try
            Dim lbnlSeteaFoco As Boolean = True
            Dim lstrMensajeResumen As String = ""

            gFunValidaValorControl("Falta ingresar Nombre de la Unidad.", Txt_NombreUnidad, lstrMensajeResumen, lbnlSeteaFoco, True, , , False)
            gFunValidaValorControl("Falta ingresar Nombre Corto de la Unidad.", Txt_NombreCortoUnidad, lstrMensajeResumen, lbnlSeteaFoco, True, , , False)
            gFunValidaValorControl("Falta Seleccionar el Estado de la Unidad.", Cmb_EstadoUnidad, lstrMensajeResumen, lbnlSeteaFoco, True, , , False)
            gFunValidaValorControl("Falta ingresar el C�digo Externo de la Unidad.", Txt_CodigoExternoUnidad, lstrMensajeResumen, lbnlSeteaFoco, True, , , False)

            If lstrMensajeResumen.Trim <> "" Then
                MsgBox(lstrMensajeResumen, MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation, gtxtNombreSistema & " Mantenedor de Unidades.")
                ValidarCamposOK = False
            Else
                ValidarCamposOK = True
            End If
        Catch ex As Exception
            ValidarCamposOK = False
        End Try
    End Function

#End Region

#Region " --- Grilla Unidades ---"

    Private Sub CargaGrillaUnidades()

        ltxtColumnas = "ID_UNIDAD, NOMBRE_UNIDAD, NOMBRE_CORTO_UNIDAD, CODIGO_EXTERNO_UNIDAD, ESTADO_UNIDAD"
        lstrResultadoTransaccion = ""

        DS_Unidades = lobjUnidad.RetornaUnidades(lstrResultadoTransaccion, ltxtColumnas, 0, "", "", "", "")

        If lstrResultadoTransaccion = "OK" Then
            If (DS_Unidades.Tables(0).Rows.Count >= 0) Then
                Tdg_Unidades.DataSource = DS_Unidades.Tables(0)
                Call FormateaGrillaUnidades()
                Tdg_Unidades.Row = -1
                Call MuestraDatosDesdeGrilla()
                Tdg_Unidades.Focus()
                StatusLabel.Text = gtxtNombreSistema & " Cantidad de unidades: " & Tdg_Unidades.RowCount.ToString
            End If
        Else
            MsgBox("Se produjo un error al tratar de mostrar el listado de Unidades, intente abrir nuevamente el mantenedor.", gtxtNombreSistema & " Mantenedor de Unidades.")
            StatusLabel.Text = gtxtNombreSistema & " Cantidad de unidades: 0"
        End If
    End Sub
    Sub FormateaGrillaUnidades()
        Dim LInt_Col As Integer
        Dim Tdg_Grilla As C1TrueDBGrid
        Tdg_Grilla = Tdg_Unidades
        With Tdg_Grilla

            LInt_Col = 0 'ID_UNIDAD_USUARIO
            .Columns(LInt_Col).Caption = "Id Unidad"
            .Splits(0).DisplayColumns(LInt_Col).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Near
            .Splits(0).DisplayColumns(LInt_Col).Width = 0
            .Columns(LInt_Col).DataField = "ID_UNIDAD"
            .Splits(0).DisplayColumns(LInt_Col).Style.HorizontalAlignment = AlignHorzEnum.Near
            .Splits(0).DisplayColumns(LInt_Col).Visible = False
            .Splits(0).DisplayColumns(LInt_Col).AllowSizing = False

            LInt_Col = LInt_Col + 1 'NOMBRE_UNIDAD
            .Columns(LInt_Col).Caption = "Nombre Unidad"
            .Splits(0).DisplayColumns(LInt_Col).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Near
            .Splits(0).DisplayColumns(LInt_Col).Width = 150
            .Columns(LInt_Col).DataField = "NOMBRE_UNIDAD"
            .Splits(0).DisplayColumns(LInt_Col).Style.HorizontalAlignment = AlignHorzEnum.Near
            .Splits(0).DisplayColumns(LInt_Col).Visible = True
            .Splits(0).DisplayColumns(LInt_Col).AllowSizing = False

            LInt_Col = LInt_Col + 1 'NOMBRE_CORTO_UNIDAD
            .Columns(LInt_Col).Caption = "Nombre Corto Unidad"
            .Splits(0).DisplayColumns(LInt_Col).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Near
            .Splits(0).DisplayColumns(LInt_Col).Width = 110
            .Columns(LInt_Col).DataField = "NOMBRE_CORTO_UNIDAD"
            .Splits(0).DisplayColumns(LInt_Col).Style.HorizontalAlignment = AlignHorzEnum.Near
            .Splits(0).DisplayColumns(LInt_Col).Visible = True
            .Splits(0).DisplayColumns(LInt_Col).AllowSizing = False

            LInt_Col = LInt_Col + 1 'CODIGO_EXTERNO_UNIDAD
            .Columns(LInt_Col).Caption = "Codigo Externo Unidad"
            .Splits(0).DisplayColumns(LInt_Col).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Center
            .Splits(0).DisplayColumns(LInt_Col).Width = 79
            .Columns(LInt_Col).DataField = "CODIGO_EXTERNO_UNIDAD"
            .Splits(0).DisplayColumns(LInt_Col).Style.HorizontalAlignment = AlignHorzEnum.Near
            .Splits(0).DisplayColumns(LInt_Col).Visible = True
            .Splits(0).DisplayColumns(LInt_Col).AllowSizing = False

            LInt_Col = LInt_Col + 1 'ESTADO_UNIDAD
            .Columns(LInt_Col).Caption = "Estado Unidad"
            .Splits(0).DisplayColumns(LInt_Col).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Center
            .Splits(0).DisplayColumns(LInt_Col).Width = 45
            .Columns(LInt_Col).DataField = "ESTADO_UNIDAD"
            .Splits(0).DisplayColumns(LInt_Col).Style.HorizontalAlignment = AlignHorzEnum.Center
            .Splits(0).DisplayColumns(LInt_Col).Visible = True
            .Splits(0).DisplayColumns(LInt_Col).AllowSizing = False

        End With

    End Sub
    Private Sub MuestraDatosDesdeGrilla()

        Dim lIntIdUnidad As Integer
        Btn_Accion.Enabled = False
        Ctrl_Botonera_Guardar.Enabled = False

        Btn_Cancelar.Enabled = Btn_Accion.Enabled
        Ctrl_Botonera_Cancelar.Enabled = Ctrl_Botonera_Guardar.Enabled

        Btn_Agregar.Enabled = True
        Ctrl_Botonera_Agregar.Enabled = True

        If Tdg_Unidades.RowCount = 0 Then
            Btn_Modificar.Enabled = False
            Ctrl_Botonera_Editar.Enabled = False

            Btn_Eliminar.Enabled = False
            Ctrl_Botonera_Eliminar.Enabled = False

            Exit Sub
        End If
        lIntIdUnidad = Tdg_Unidades.Columns("ID_UNIDAD").Text.Trim
        Txt_NombreUnidad.Text = Tdg_Unidades.Columns("NOMBRE_UNIDAD").Text.Trim
        Txt_NombreCortoUnidad.Text = Tdg_Unidades.Columns("NOMBRE_CORTO_UNIDAD").Text.Trim
        Txt_CodigoExternoUnidad.Text = Tdg_Unidades.Columns("CODIGO_EXTERNO_UNIDAD").Text.Trim
        Cmb_EstadoUnidad.SelectedIndex = gFunSeteaItemComboBox(Cmb_EstadoUnidad, Tdg_Unidades.Columns("ESTADO_UNIDAD").Text.Trim, 0)
        If (DS_Unidades.Tables(0).Rows.Count > 0) Then
            If Tdg_Unidades.Columns("ESTADO_UNIDAD").Text.Trim <> "ELI" Then
                Btn_Modificar.Enabled = True
                Ctrl_Botonera_Editar.Enabled = True

                Btn_Eliminar.Enabled = True
                Ctrl_Botonera_Eliminar.Enabled = True
            Else
                Btn_Modificar.Enabled = False
                Ctrl_Botonera_Editar.Enabled = False

                Btn_Eliminar.Enabled = False
                Ctrl_Botonera_Eliminar.Enabled = False

            End If
        End If
    End Sub

    Private Sub Tdg_Unidades_RowColChange(ByVal sender As Object, ByVal e As System.EventArgs) Handles Tdg_Unidades.RowColChange
        Btn_Accion.Enabled = False
        Ctrl_Botonera_Guardar.Enabled = False

        Btn_Cancelar.Enabled = Btn_Accion.Enabled
        Ctrl_Botonera_Cancelar.Enabled = Ctrl_Botonera_Guardar.Enabled

        ltxtOperacion = ""
        Call HabilitaDeshabilitaDatos()
        Call MuestraDatosDesdeGrilla()
    End Sub
    Private Sub Tdg_Unidades_AfterFilter(ByVal sender As Object, ByVal e As C1.Win.C1TrueDBGrid.FilterEventArgs) Handles Tdg_Unidades.AfterFilter
        If (DS_Unidades.Tables(0).Rows.Count >= 0) Then
            Call MuestraDatosDesdeGrilla()
        End If
    End Sub
    Private Sub Tdg_Unidades_AfterSort(ByVal sender As Object, ByVal e As C1.Win.C1TrueDBGrid.FilterEventArgs) Handles Tdg_Unidades.AfterSort
        If (DS_Unidades.Tables(0).Rows.Count >= 0) Then
            Call MuestraDatosDesdeGrilla()
        End If
    End Sub

#End Region

#Region " --- Botones ---"

    Private Sub Btn_Accion_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Ctrl_Botonera_Guardar.Click 'Handles Btn_Accion.Click
        lstrResultadoTransaccion = "OK"
        If ltxtOperacion = "ELIMINAR" Then
            If Tdg_Unidades.Columns("ESTADO_UNIDAD").Text.Trim = "ELI" Then
                MsgBox("El Registro ya se encuentra ELIMINADO.", MsgBoxStyle.Information, gtxtNombreSistema & " Mantenedor de Unidades.")
            ElseIf Tdg_Unidades.Columns("ESTADO_UNIDAD").Text.Trim = "SIS" Then
                MsgBox("NO PUEDE eliminar este Unidad.", MsgBoxStyle.Information, gtxtNombreSistema & " Mantenedor de Unidades.")
            Else
                '...Mensaje de confirmacion 
                If (MsgBox(vbCr & " � Confirma la Eliminaci�n de la Unidad ( " & Txt_NombreUnidad.Text.Trim & " ). ?" & vbCr & vbCr, MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2 + MsgBoxStyle.Exclamation, gtxtNombreSistema & " Mantenedor de Unidades.")) = MsgBoxResult.No Then
                    Exit Sub
                End If
            End If
        Else
            If Not ValidarCamposOK() Then Exit Sub
        End If

        Select Case ltxtOperacion
            Case Is = "INSERTAR"
                Nuevo_Unidad()
            Case Is = "MODIFICAR"
                Modifica_Unidad()
            Case Is = "ELIMINAR"
                Elimina_Unidad()
        End Select

        If lstrResultadoTransaccion.Trim = "OK" Then
            MsgBox("Operaci�n se realiz� con �xito.", MsgBoxStyle.Information, gtxtNombreSistema & " Mantenedor de Unidades.")
            Call CargaGrillaUnidades()
            Call InicializaFormulario()
        Else
            MsgBox(lstrResultadoTransaccion, MsgBoxStyle.Exclamation, gtxtNombreSistema & " Mantenedor de Unidades.")
            '            Btn_Accion.Focus()
        End If

    End Sub
    Private Sub Btn_Agregar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Ctrl_Botonera_Agregar.Click 'Handles Btn_Agregar.Click
        ltxtOperacion = "INSERTAR"
        Call HabilitaDeshabilitaDatos()
        Call Limpiar_Campos()
        Cmb_EstadoUnidad.SelectedIndex = gFunSeteaItemComboBox(Cmb_EstadoUnidad, "VIG", 0)
        Btn_Accion.Enabled = True
        Ctrl_Botonera_Guardar.Enabled = True

        Btn_Cancelar.Enabled = Btn_Accion.Enabled
        Ctrl_Botonera_Cancelar.Enabled = Ctrl_Botonera_Guardar.Enabled

        Btn_Eliminar.Enabled = False
        Ctrl_Botonera_Eliminar.Enabled = False

        Btn_Modificar.Enabled = False
        Ctrl_Botonera_Editar.Enabled = False

        Btn_Accion.Text = "&Grabar"
        Txt_NombreUnidad.Focus()
    End Sub
    Private Sub Btn_Modificar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Ctrl_Botonera_Editar.Click  'Handles Btn_Modificar.Click
        ltxtOperacion = "MODIFICAR"
        Call HabilitaDeshabilitaDatos()
        Btn_Accion.Enabled = True
        Ctrl_Botonera_Guardar.Enabled = True

        Btn_Cancelar.Enabled = Btn_Accion.Enabled
        Ctrl_Botonera_Cancelar.Enabled = Ctrl_Botonera_Guardar.Enabled

        Btn_Eliminar.Enabled = False
        Ctrl_Botonera_Eliminar.Enabled = False

        Btn_Agregar.Enabled = False
        Ctrl_Botonera_Agregar.Enabled = False

        Btn_Modificar.Enabled = False
        Ctrl_Botonera_Editar.Enabled = False


        Btn_Accion.Text = "&Grabar"

        'Txt_NombreUnidad.Focus()
    End Sub
    Private Sub Btn_Eliminar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Ctrl_Botonera_Eliminar.Click 'Handles Btn_Eliminar.Click
        If Tdg_Unidades.Columns("ESTADO_UNIDAD").CellValue(Tdg_Unidades.Bookmark).ToString.Trim = "SIS" Then   ' Estado NO BOrrar 
            MsgBox("No se puede eliminar este UNIDAD", MsgBoxStyle.Critical, gtxtNombreSistema & " Mantenedor de Unidades.")
        Else
            ltxtOperacion = "ELIMINAR"
            'Call HabilitaDeshabilitaDatos()
            'Btn_Accion.Enabled = True
            'Btn_Cancelar.Enabled = Btn_Accion.Enabled
            'Btn_Agregar.Enabled = False
            'Btn_Modificar.Enabled = False
            'Btn_Accion.Text = "&Eliminar"
            'Btn_Accion.Focus()

            Call HabilitaDeshabilitaDatos()
            lstrResultadoTransaccion = "OK"
            If Tdg_Unidades.Columns("ESTADO_UNIDAD").Text.Trim = "ELI" Then
                MsgBox("El Registro ya se encuentra ELIMINADO.", MsgBoxStyle.Information, gtxtNombreSistema & " Mantenedor de Unidades.")
            ElseIf Tdg_Unidades.Columns("ESTADO_UNIDAD").Text.Trim = "SIS" Then
                MsgBox("NO PUEDE eliminar este Unidad.", MsgBoxStyle.Information, gtxtNombreSistema & " Mantenedor de Unidades.")
            Else
                If (MsgBox(vbCr & " � Confirma la Eliminaci�n de la Unidad ( " & Txt_NombreUnidad.Text.Trim & " ). ?" & vbCr & vbCr, MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2 + MsgBoxStyle.Exclamation, gtxtNombreSistema & " Mantenedor de Unidades.")) = MsgBoxResult.No Then
                    Exit Sub
                End If
            End If
            Elimina_Unidad()
            If lstrResultadoTransaccion.Trim = "OK" Then
                MsgBox("Operaci�n se realiz� con �xito.", MsgBoxStyle.Information, gtxtNombreSistema & " Mantenedor de Unidades.")
                Call CargaGrillaUnidades()
                Call InicializaFormulario()
            Else
                MsgBox(lstrResultadoTransaccion, MsgBoxStyle.Exclamation, gtxtNombreSistema & " Mantenedor de Unidades.")
                'Btn_Accion.Focus()
            End If
        End If
    End Sub
    Private Sub Btn_Cancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Ctrl_Botonera_Cancelar.Click 'Handles Btn_Cancelar.Click
        Btn_Accion.Enabled = False
        Ctrl_Botonera_Guardar.Enabled = False

        Btn_Cancelar.Enabled = Btn_Accion.Enabled
        Ctrl_Botonera_Cancelar.Enabled = Ctrl_Botonera_Guardar.Enabled


        ltxtOperacion = ""
        Call HabilitaDeshabilitaDatos()
        Call MuestraDatosDesdeGrilla()
    End Sub

    Private Sub Nuevo_Unidad()
        Dim lintIdUnidad As Integer = 0
        Dim lstrEstado As String = "VIG"

        Dim intFila As Integer

        intFila = Cmb_EstadoUnidad.SelectedIndex
        If Cmb_EstadoUnidad.ListCount > 0 Then
            lstrEstado = Cmb_EstadoUnidad.Columns(0).CellValue(intFila).ToString()
        End If

        lstrResultadoTransaccion = lobjUnidad.Insertar(lintIdUnidad, Txt_NombreUnidad.Text.Trim, Txt_NombreCortoUnidad.Text.Trim, Txt_CodigoExternoUnidad.Text.Trim, "VIG")

    End Sub
    Private Sub Modifica_Unidad()
        Dim lintIdUnidad As Integer = 0
        Dim lintFila As Integer = 0
        Dim lstrEstado As String = ""


        lintIdUnidad = Tdg_Unidades.Columns("ID_UNIDAD").Text.Trim

        lintFila = Cmb_EstadoUnidad.SelectedIndex
        lstrEstado = Cmb_EstadoUnidad.Columns(0).CellValue(lintFila).ToString()

        lstrResultadoTransaccion = lobjUnidad.Modificar(lintIdUnidad, _
                                                        Txt_NombreUnidad.Text.Trim, _
                                                        Txt_NombreCortoUnidad.Text.Trim, _
                                                        Txt_CodigoExternoUnidad.Text.Trim, lstrEstado)


    End Sub
    Private Sub Elimina_Unidad()
        Dim lintIdUnidad As Integer = 0

        lintIdUnidad = Tdg_Unidades.Columns("ID_UNIDAD").Text.Trim
        lstrResultadoTransaccion = lobjUnidad.Eliminar(lintIdUnidad)

    End Sub

#End Region

    Private Sub Ctrl_Botonera_Guardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Ctrl_Botonera_Guardar.Click

    End Sub

    Private Sub Ctrl_Botonera_Cancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Ctrl_Botonera_Cancelar.Click

    End Sub

    Private Sub Ctrl_Botonera_Agregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Ctrl_Botonera_Agregar.Click

    End Sub

    Private Sub Ctrl_Botonera_Eliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Ctrl_Botonera_Eliminar.Click

    End Sub

    Private Sub Ctrl_Botonera_Editar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Ctrl_Botonera_Editar.Click

    End Sub
End Class