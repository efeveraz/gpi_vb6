Imports C1.Win.C1TrueDBGrid
Imports C1.Win.C1List.C1Combo

Public Class Frm_MantenedorPerfilesUsuarios

#Region " --- Mantenedor de Perfiles de Usuarios - Declaraciones ---"
    Dim ltxtColumnas As String = ""
    Dim ltxtOperacion As String = ""
    Dim lstrResultadoTransaccion As String = ""

    Dim lblnMoviendoElFormulario As Boolean = False
    Dim lMouseCoordenadaX As Integer = 0
    Dim lMouseCoordenadaY As Integer = 0

    Dim lobjPerfilUsuario As ClsPerfilUsuario = New ClsPerfilUsuario
    Dim lobjPerfilesUsuarios As ClsPerfilesUsuarios = New ClsPerfilesUsuarios
    Dim lobjUsuario As ClsUsuario = New ClsUsuario
    Dim lobjNegocio As ClsNegociosUsuarios = New ClsNegociosUsuarios

    Dim DS_PerfilUsuario As New DataSet
    Dim DS_UsuariosAsociados As New DataSet
    Dim DS_UsuariosDisponibles As New DataSet
    Dim DS_Negocios As New DataSet
    Dim DS_UsuariosAsociados_Proceso As New DataSet

#End Region

#Region " --- Formulario ---"

    Public Sub CargarFormulario()

        'Me.Location = New Point(gobjFormulario._PosX, gobjFormulario._PosY)
        Me.Location = New Point(3, 3)

        If gobjFormulario._Modal Then
            ShowDialog()
        Else
            Show()
            BringToFront()
        End If
    End Sub
    Private Sub AbrirFormulario(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call CargaGrillaPerfiles()
        Call InicializaFormulario()
    End Sub
    Private Sub Frm_MantenedorPerilesUsuarios_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.GotFocus, Me.Click, Me.Click
        Me.BringToFront()
    End Sub
    Private Sub Me_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If (e.KeyCode = Keys.F4) And e.Modifiers = Keys.Alt Then
            e.Handled = True
            Call Btn_Salir_Click(sender, e)
        ElseIf e.KeyCode = Keys.Enter Then
            e.Handled = True
            SendKeys.Send("{TAB}")
        ElseIf e.KeyCode = Keys.Escape Then
            e.Handled = True
            Call Btn_Cancelar_Click(sender, e)
        End If
    End Sub
    Private Sub Me_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.DoubleClick, Me.DoubleClick
        Me.Location = New Point(3, 3)
    End Sub
    Private Sub Me_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Me.Dispose()
    End Sub
    Private Sub Btn_Salir_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Salir.Click
        Me.Close()
    End Sub
    Private Sub Formulario_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles Me.MouseDown, Me.MouseDown
        If e.Button = MouseButtons.Left Then
            lblnMoviendoElFormulario = True
            lMouseCoordenadaX = e.X
            lMouseCoordenadaY = e.Y
        End If
    End Sub
    Private Sub Formulario_MouseUp(ByVal sender As Object, ByVal e As MouseEventArgs) Handles Me.MouseUp, Me.MouseUp
        If e.Button = MouseButtons.Left Then
            lblnMoviendoElFormulario = False
        End If
    End Sub
    Private Sub Formulario_MouseMove(ByVal sender As Object, ByVal e As MouseEventArgs) Handles Me.MouseMove, Me.MouseMove
        If lblnMoviendoElFormulario Then
            '...Arriba
            If Me.Location.Y < 1 Then
                Me.Location = New Point(Me.Location.X, 1)
                lblnMoviendoElFormulario = False
                '...Abajo
            ElseIf Me.Location.Y > (sender.parent.Height - 30) Then
                Me.Location = New Point(Me.Location.X, sender.parent.Height - 30)
                lblnMoviendoElFormulario = False
                '...Izquierda
            ElseIf Me.Location.X < ((Me.Width / 10) - Me.Width) Then
                Me.Location = New Point(((Me.Width / 10) - Me.Width), Me.Location.Y)
                lblnMoviendoElFormulario = False
                '...Derecha
            ElseIf Me.Location.X > (sender.parent.Width - 150) Then
                Me.Location = New Point((sender.parent.Width - 150), Me.Location.Y)
                lblnMoviendoElFormulario = False
            Else
                lblnMoviendoElFormulario = True
                Dim temp As Point = New Point()
                temp.X = Me.Location.X + (e.X - lMouseCoordenadaX)
                temp.Y = Me.Location.Y + (e.Y - lMouseCoordenadaY)
                Me.Location = temp
                temp = Nothing
            End If
        End If
    End Sub

#End Region

#Region " --- Eventos Formulario ---"

    Private Sub CargaEstado()
        Cmb_EstadoPerfil.ClearItems()
        Cmb_EstadoPerfil.AddItem("VIGENTE;VIG")
        Cmb_EstadoPerfil.AddItem("BLOQUEADO;BLO")
        Cmb_EstadoPerfil.AddItem("ELIMINADO;ELI")
        Cmb_EstadoPerfil.Splits(0).DisplayColumns(0).Visible = True
        Cmb_EstadoPerfil.Splits(0).DisplayColumns(1).Visible = False
    End Sub
    Private Sub CargaNegocios()
        Dim lstrColumnas As String = "ABR_NEGOCIO,ID_NEGOCIO"
        DS_Negocios = lobjNegocio.RetornaNegociosUsuarioConectado(lstrResultadoTransaccion, glngIdUsuario, lstrColumnas)
        If lstrResultadoTransaccion = "OK" Then
            If DS_Negocios.Tables.Count > 0 Then
                If DS_Negocios.Tables(0).Rows.Count > 0 Then
                    Cmb_NegocioUsuarios.DataSource = DS_Negocios.Tables(0)

                    Cmb_NegocioUsuarios.Columns(0).DataField = "ABR_NEGOCIO"
                    Cmb_NegocioUsuarios.Columns(1).DataField = "ID_NEGOCIO"

                    Cmb_NegocioUsuarios.Splits(0).DisplayColumns("ABR_NEGOCIO").Visible = True
                    Cmb_NegocioUsuarios.Splits(0).DisplayColumns("ID_NEGOCIO").Visible = False
                    Cmb_NegocioUsuarios.SelectedIndex = -1
                    Cmb_NegocioUsuarios.Text = ""
                End If
            End If
        End If
    End Sub
    Private Sub InicializaFormulario()
        Call CargaEstado()
        Call CargaNegocios()
        ltxtOperacion = ""

        Call HabilitaDeshabilitaDatos()

        Cmb_EstadoPerfil.SelectedIndex = -1

        Limpiar_Campos()

        Tdg_UsuarioAsignados.DataSource = Nothing
        Tdg_UsuariosDisponibles.DataSource = Nothing
        Pnl_UsuariosPerfil.Enabled = False

        Tdg_PerfilesUsuario.SelectedRows.Clear()
        Tdg_PerfilesUsuario.Row = -1
        Btn_Agregar.Enabled = True
        Ctrl_Botonera_Agregar.Enabled = True

        Btn_Accion.Enabled = False
        Ctrl_Botonera_Guardar.Enabled = False

        Btn_Cancelar.Enabled = Btn_Accion.Enabled
        Ctrl_Botonera_Cancelar.Enabled = Ctrl_Botonera_Guardar.Enabled

        Btn_Modificar.Enabled = False
        Ctrl_Botonera_Editar.Enabled = False

        Btn_Eliminar.Enabled = False
        Ctrl_Botonera_Eliminar.Enabled = False

        Tdg_PerfilesUsuario.Focus()
    End Sub
    Private Sub HabilitaDeshabilitaDatos()
        Select Case ltxtOperacion.Trim
            Case Is = "INSERTAR"
                gSubHabilitaControl(Txt_NombrePerfil, False, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Txt_NombreCortoPerfil, False, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Txt_CodigoExternoPerfil, False, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Cmb_EstadoPerfil, True, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
            Case Is = "MODIFICAR"
                gSubHabilitaControl(Txt_NombrePerfil, True, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Txt_NombreCortoPerfil, False, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Txt_CodigoExternoPerfil, False, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Cmb_EstadoPerfil, False, gColorBackGroundEnabled, gColorBackGroundDesabled, True)

            Case Is = "ELIMINAR", ""
                gSubHabilitaControl(Txt_NombrePerfil, True, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Txt_NombreCortoPerfil, True, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Txt_CodigoExternoPerfil, True, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Cmb_EstadoPerfil, True, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
        End Select
    End Sub
    Public Sub Limpiar_Campos()
        Txt_NombrePerfil.Text = ""
        Txt_NombreCortoPerfil.Text = ""
        Txt_CodigoExternoPerfil.Text = ""
        Cmb_EstadoPerfil.SelectedIndex = -1
    End Sub
    Private Function ValidarCamposOK() As Boolean
        Try
            Dim lbnlSeteaFoco As Boolean = True
            Dim lstrMensajeResumen As String = ""

            gFunValidaValorControl("Falta ingresar Nombre del Perfil.", Txt_NombrePerfil, lstrMensajeResumen, lbnlSeteaFoco, True, , , False)
            gFunValidaValorControl("Falta ingresar Nombre Corto del Perfil.", Txt_NombreCortoPerfil, lstrMensajeResumen, lbnlSeteaFoco, True, , , False)
            gFunValidaValorControl("Falta Seleccionar el Estado del Perfil.", Cmb_EstadoPerfil, lstrMensajeResumen, lbnlSeteaFoco, True, , , False)

            If lstrMensajeResumen.Trim <> "" Then
                MsgBox(lstrMensajeResumen, MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation, gtxtNombreSistema & " Mantenedor de Perfiles")
                ValidarCamposOK = False
            Else
                ValidarCamposOK = True
            End If
        Catch ex As Exception
            ValidarCamposOK = False
        End Try
    End Function

#End Region

#Region " --- Grilla Perfiles ---"

    Private Sub CargaGrillaPerfiles()

        ltxtColumnas = "ID_PERFIL_USUARIO, NOMBRE_PERFIL, NOMBRE_CORTO_PERFIL, CODIGO_EXTERNO_PERFIL, EST_PERFIL"
        lstrResultadoTransaccion = ""

        DS_PerfilUsuario = lobjPerfilUsuario.RetornaPerfiles(lstrResultadoTransaccion, 0, "", "", "", "")


        If lstrResultadoTransaccion = "OK" Then
            If (DS_PerfilUsuario.Tables(0).Rows.Count >= 0) Then
                Tdg_PerfilesUsuario.DataSource = DS_PerfilUsuario.Tables(0)
                Call FormateaGrillaPerfiles()
                Tdg_PerfilesUsuario.Row = -1
                Call MuestraDatosDesdeGrilla()
                Tdg_PerfilesUsuario.Focus()
                StatusLabel.Text = gtxtNombreSistema & " Cantidad de tipos de perfiles de usuario: " & Tdg_PerfilesUsuario.RowCount.ToString
            End If
        Else
            MsgBox("Se produjo un error al tratar de mostrar el listado de Usuarios, intente abrir nuevamente el mantenedor.", gtxtNombreSistema & " Mantenedor de Perfiles")
            StatusLabel.Text = gtxtNombreSistema & " Cantidad de tipos de perfiles de usuario: 0"
        End If
    End Sub
    Sub FormateaGrillaPerfiles()
        Dim LInt_Col As Integer
        Dim Tdg_Grilla As C1TrueDBGrid
        Tdg_Grilla = Tdg_PerfilesUsuario
        With Tdg_Grilla

            LInt_Col = 0 'ID_PERFIL_USUARIO
            .Columns(LInt_Col).Caption = "Id Perfil Usuario"
            .Splits(0).DisplayColumns(LInt_Col).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Near
            .Splits(0).DisplayColumns(LInt_Col).Width = 10
            .Columns(LInt_Col).DataField = "ID_PERFIL_USUARIO"
            .Splits(0).DisplayColumns(LInt_Col).Style.HorizontalAlignment = AlignHorzEnum.Near
            .Splits(0).DisplayColumns(LInt_Col).Visible = False
            .Splits(0).DisplayColumns(LInt_Col).AllowSizing = False

            LInt_Col = LInt_Col + 1 'NOMBRE_PERFIL
            .Columns(LInt_Col).Caption = "Nombre"
            .Splits(0).DisplayColumns(LInt_Col).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Near
            .Splits(0).DisplayColumns(LInt_Col).Width = 140
            .Columns(LInt_Col).DataField = "NOMBRE_PERFIL"
            .Splits(0).DisplayColumns(LInt_Col).Style.HorizontalAlignment = AlignHorzEnum.Near
            .Splits(0).DisplayColumns(LInt_Col).Visible = True
            .Splits(0).DisplayColumns(LInt_Col).AllowSizing = False

            LInt_Col = LInt_Col + 1 'NOMBRE_CORTO_PERFIL
            .Columns(LInt_Col).Caption = "Nombre Corto"
            .Splits(0).DisplayColumns(LInt_Col).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Near
            .Splits(0).DisplayColumns(LInt_Col).Width = 100
            .Columns(LInt_Col).DataField = "NOMBRE_CORTO_PERFIL"
            .Splits(0).DisplayColumns(LInt_Col).Style.HorizontalAlignment = AlignHorzEnum.Near
            .Splits(0).DisplayColumns(LInt_Col).Visible = True
            .Splits(0).DisplayColumns(LInt_Col).AllowSizing = False

            LInt_Col = LInt_Col + 1 'CODIGO_EXTERNO_PERFIL
            .Columns(LInt_Col).Caption = "Codigo Externo"
            .Splits(0).DisplayColumns(LInt_Col).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Center
            .Splits(0).DisplayColumns(LInt_Col).Width = 70
            .Columns(LInt_Col).DataField = "CODIGO_EXTERNO_PERFIL"
            .Splits(0).DisplayColumns(LInt_Col).Style.HorizontalAlignment = AlignHorzEnum.Near
            .Splits(0).DisplayColumns(LInt_Col).Visible = True
            .Splits(0).DisplayColumns(LInt_Col).AllowSizing = False

            LInt_Col = LInt_Col + 1 'EST_PERFIL
            .Columns(LInt_Col).Caption = "Estado"
            .Splits(0).DisplayColumns(LInt_Col).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Center
            .Splits(0).DisplayColumns(LInt_Col).Width = 38
            .Columns(LInt_Col).DataField = "EST_PERFIL"
            .Splits(0).DisplayColumns(LInt_Col).Style.HorizontalAlignment = AlignHorzEnum.Center
            .Splits(0).DisplayColumns(LInt_Col).Visible = True
            .Splits(0).DisplayColumns(LInt_Col).AllowSizing = False

        End With

    End Sub
    Private Sub MuestraDatosDesdeGrilla()
        Dim lIntIdPerfil As Integer
        Btn_Accion.Enabled = False
        Ctrl_Botonera_Guardar.Enabled = False

        Btn_Cancelar.Enabled = Btn_Accion.Enabled
        Ctrl_Botonera_Cancelar.Enabled = Ctrl_Botonera_Guardar.Enabled

        Btn_Agregar.Enabled = True
        Ctrl_Botonera_Agregar.Enabled = True

        Cmb_NegocioUsuarios.Text = ""

        lIntIdPerfil = "0" & Tdg_PerfilesUsuario.Columns("ID_PERFIL_USUARIO").Text.Trim
        Txt_NombrePerfil.Text = Tdg_PerfilesUsuario.Columns("NOMBRE_PERFIL").Text.Trim
        Txt_NombreCortoPerfil.Text = Tdg_PerfilesUsuario.Columns("NOMBRE_CORTO_PERFIL").Text.Trim
        Txt_CodigoExternoPerfil.Text = Tdg_PerfilesUsuario.Columns("CODIGO_EXTERNO_PERFIL").Text.Trim
        Cmb_EstadoPerfil.SelectedIndex = gFunSeteaItemComboBox(Cmb_EstadoPerfil, Tdg_PerfilesUsuario.Columns("EST_PERFIL").Text.Trim, 1)
        Pnl_UsuariosPerfil.Enabled = False
        If (DS_PerfilUsuario.Tables(0).Rows.Count > 0) Then
            If Tdg_PerfilesUsuario.Columns("EST_PERFIL").Text.Trim <> "ELI" Then
                Btn_Modificar.Enabled = True
                Ctrl_Botonera_Editar.Enabled = True
                Btn_Eliminar.Enabled = True
                Ctrl_Botonera_Eliminar.Enabled = True
            Else
                Btn_Modificar.Enabled = False
                Ctrl_Botonera_Editar.Enabled = False

                Btn_Eliminar.Enabled = False
                Ctrl_Botonera_Eliminar.Enabled = False
            End If
        End If
        CargarUsuariosAsociados(lIntIdPerfil, 0)

        If Not IsNothing(Tdg_UsuariosDisponibles.DataSource) Then
            Tdg_UsuariosDisponibles.Columns("ID_NEGOCIO").FilterText = -1
        End If

    End Sub

    Private Sub Tdg_PerfilesUsuario_RowColChange(ByVal sender As Object, ByVal e As System.EventArgs) Handles Tdg_PerfilesUsuario.RowColChange
        Btn_Accion.Enabled = False
        Ctrl_Botonera_Guardar.Enabled = False

        Btn_Cancelar.Enabled = Btn_Accion.Enabled
        Ctrl_Botonera_Cancelar.Enabled = Ctrl_Botonera_Guardar.Enabled

        ltxtOperacion = ""
        Call HabilitaDeshabilitaDatos()
        Call MuestraDatosDesdeGrilla()
    End Sub
    Private Sub Tdg_PerfilesUsuario_AfterFilter(ByVal sender As Object, ByVal e As C1.Win.C1TrueDBGrid.FilterEventArgs) Handles Tdg_PerfilesUsuario.AfterFilter
        If (DS_PerfilUsuario.Tables(0).Rows.Count >= 0) Then
            Call MuestraDatosDesdeGrilla()
        End If
    End Sub
    Private Sub Tdg_PerfilesUsuario_AfterSort(ByVal sender As Object, ByVal e As C1.Win.C1TrueDBGrid.FilterEventArgs) Handles Tdg_PerfilesUsuario.AfterSort
        If (DS_PerfilUsuario.Tables(0).Rows.Count >= 0) Then
            Call MuestraDatosDesdeGrilla()
        End If
    End Sub

#End Region

#Region " --- Botones ---"

    Private Sub Btn_Accion_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Ctrl_Botonera_Guardar.Click 'Handles Btn_Accion.Click
        lstrResultadoTransaccion = "OK"
        If ltxtOperacion = "ELIMINAR" Then
            If Tdg_PerfilesUsuario.Columns("EST_PERFIL").Text.Trim = "ELI" Then
                MsgBox("El Registro ya se encuentra ELIMINADO.", MsgBoxStyle.Information, gtxtNombreSistema & " Mantenedor de Perfiles")
            ElseIf Tdg_PerfilesUsuario.Columns("EST_PERFIL").Text.Trim = "SIS" Then
                MsgBox("NO PUEDE eliminar este usuario.", MsgBoxStyle.Information, gtxtNombreSistema & " Mantenedor de Perfiles")
            Else
                '...Mensaje de confirmacion 
                If (MsgBox(vbCr & " � Confirma la Eliminaci�n del Perfil ( " & Txt_NombrePerfil.Text.Trim & " ). ?" & vbCr & vbCr, MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2 + MsgBoxStyle.Exclamation, gtxtNombreSistema & " Mantenedor de Perfiles")) = MsgBoxResult.No Then
                    Exit Sub
                End If
            End If
        Else
            If Not ValidarCamposOK() Then Exit Sub
        End If

        Select Case ltxtOperacion
            Case Is = "INSERTAR"
                Nuevo_PerfilUsuario()
            Case Is = "MODIFICAR"
                Modifica_PerfilUsuario()
            Case Is = "ELIMINAR"
                Elimina_PerfilUsuario()
        End Select


        If lstrResultadoTransaccion.Trim = "OK" Then
            MsgBox("Operaci�n se realiz� con �xito.", MsgBoxStyle.Information, gtxtNombreSistema & " Mantenedor de Perfiles")
            Call CargaGrillaPerfiles()
            Call InicializaFormulario()
        Else
            MsgBox(lstrResultadoTransaccion, MsgBoxStyle.Exclamation, gtxtNombreSistema & " Mantenedor de Perfiles")
            'Btn_Accion.Focus()
        End If

    End Sub
    Private Sub Btn_Agregar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Ctrl_Botonera_Agregar.Click 'Handles Btn_Agregar.Click
        ltxtOperacion = "INSERTAR"
        Call HabilitaDeshabilitaDatos()
        Call Limpiar_Campos()
        Cmb_EstadoPerfil.SelectedIndex = gFunSeteaItemComboBox(Cmb_EstadoPerfil, "VIG", 1)
        Btn_Accion.Enabled = True
        Ctrl_Botonera_Guardar.Enabled = True

        Btn_Cancelar.Enabled = Btn_Accion.Enabled
        Ctrl_Botonera_Cancelar.Enabled = Ctrl_Botonera_Guardar.Enabled

        Btn_Eliminar.Enabled = False
        Ctrl_Botonera_Eliminar.Enabled = False

        Btn_Modificar.Enabled = False
        Ctrl_Botonera_Editar.Enabled = False

        Pnl_UsuariosPerfil.Enabled = True
        'Btn_Accion.Text = "&Grabar"
        Txt_NombrePerfil.Focus()
    End Sub
    Private Sub Btn_Modificar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Ctrl_Botonera_Editar.Click 'Handles Btn_Modificar.Click
        ltxtOperacion = "MODIFICAR"
        Call HabilitaDeshabilitaDatos()
        Btn_Accion.Enabled = True
        Ctrl_Botonera_Guardar.Enabled = True

        Btn_Cancelar.Enabled = Btn_Accion.Enabled
        Ctrl_Botonera_Cancelar.Enabled = Ctrl_Botonera_Guardar.Enabled

        Btn_Eliminar.Enabled = False
        Ctrl_Botonera_Eliminar.Enabled = False

        Btn_Agregar.Enabled = False
        Ctrl_Botonera_Agregar.Enabled = False

        Btn_Modificar.Enabled = False
        Ctrl_Botonera_Editar.Enabled = False

        Pnl_UsuariosPerfil.Enabled = True
        'Btn_Accion.Text = "&Actualizar"
        Txt_NombrePerfil.Focus()
    End Sub
    Private Sub Btn_Eliminar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Ctrl_Botonera_Eliminar.Click ' Handles Btn_Eliminar.Click
        If Tdg_PerfilesUsuario.Columns("EST_PERFIL").CellValue(Tdg_PerfilesUsuario.Bookmark).ToString.Trim = "SIS" Then   ' Estado NO BOrrar 
            MsgBox("No se puede eliminar este PERFIL", MsgBoxStyle.Critical, gtxtNombreSistema & " Mantenedor de Perfiles")
        Else
            ltxtOperacion = "ELIMINAR"
            'Call HabilitaDeshabilitaDatos()
            'Btn_Accion.Enabled = True
            'Btn_Cancelar.Enabled = Btn_Accion.Enabled
            'Btn_Agregar.Enabled = False
            'Btn_Modificar.Enabled = False
            'Btn_Accion.Text = "&Eliminar"
            'Btn_Accion.Focus()

            Call HabilitaDeshabilitaDatos()

            lstrResultadoTransaccion = "OK"
            If Tdg_PerfilesUsuario.Columns("EST_PERFIL").Text.Trim = "ELI" Then
                MsgBox("El Registro ya se encuentra ELIMINADO.", MsgBoxStyle.Information, gtxtNombreSistema & " Mantenedor de Perfiles")
            ElseIf Tdg_PerfilesUsuario.Columns("EST_PERFIL").Text.Trim = "SIS" Then
                MsgBox("NO PUEDE eliminar este usuario.", MsgBoxStyle.Information, gtxtNombreSistema & " Mantenedor de Perfiles")
            Else
                If (MsgBox(vbCr & " � Confirma la Eliminaci�n del Perfil ( " & Txt_NombrePerfil.Text.Trim & " ). ?" & vbCr & vbCr, MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2 + MsgBoxStyle.Exclamation, gtxtNombreSistema & " Mantenedor de Perfiles")) = MsgBoxResult.No Then
                    Exit Sub
                End If
            End If
            Elimina_PerfilUsuario()
            If lstrResultadoTransaccion.Trim = "OK" Then
                MsgBox("Operaci�n se realiz� con �xito.", MsgBoxStyle.Information, gtxtNombreSistema & " Mantenedor de Perfiles")
                Call CargaGrillaPerfiles()
                Call InicializaFormulario()
            Else
                MsgBox(lstrResultadoTransaccion, MsgBoxStyle.Exclamation, gtxtNombreSistema & " Mantenedor de Perfiles")
                'Btn_Accion.Focus()
            End If
        End If
    End Sub

    Private Sub Btn_Cancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Ctrl_Botonera_Cancelar.Click 'Handles Btn_Cancelar.Click
        Btn_Accion.Enabled = False
        Ctrl_Botonera_Guardar.Enabled = False

        Btn_Cancelar.Enabled = Btn_Accion.Enabled
        Ctrl_Botonera_Cancelar.Enabled = Ctrl_Botonera_Guardar.Enabled

        ltxtOperacion = ""
        Call HabilitaDeshabilitaDatos()
        Call MuestraDatosDesdeGrilla()
    End Sub

    Private Sub Nuevo_PerfilUsuario()
        Dim lintIdPerfilUsuario As Integer = 0
        Dim lintIdTipoUsuario As Integer = 0
        Dim lstrEstado As String = "VIG"

        Dim intFila As Integer

        intFila = Cmb_EstadoPerfil.SelectedIndex
        If Cmb_EstadoPerfil.ListCount > 0 Then
            lstrEstado = Cmb_EstadoPerfil.Columns(0).CellValue(intFila).ToString()
        End If

        lstrResultadoTransaccion = lobjPerfilUsuario.Insertar(lintIdPerfilUsuario, Txt_NombrePerfil.Text.Trim, Txt_NombreCortoPerfil.Text.Trim, Txt_CodigoExternoPerfil.Text.Trim, "VIG")


        If lstrResultadoTransaccion = "OK" Then
            If Tdg_UsuarioAsignados.RowCount > 0 Then
                Call AgregarUsuarios_al_Perfil(lintIdPerfilUsuario)
            End If
        End If

    End Sub
    Private Sub Modifica_PerfilUsuario()
        Dim lintIdPerfilUsuario As Integer = 0
        Dim lintFila As Integer = 0
        Dim lintIdTipoUsuario As Integer = 0
        Dim lintIdUnidad As Integer = 0
        Dim lstrEstado As String = ""


        lintFila = Tdg_PerfilesUsuario.Bookmark
        lintIdPerfilUsuario = Tdg_PerfilesUsuario.Columns("ID_PERFIL_USUARIO").Text.Trim

        lintFila = Cmb_EstadoPerfil.SelectedIndex
        lstrEstado = Cmb_EstadoPerfil.Columns(0).CellValue(lintFila).ToString()

        lstrResultadoTransaccion = lobjPerfilUsuario.Modificar(lintIdPerfilUsuario, _
                                      Txt_NombrePerfil.Text.Trim, _
                                      Txt_NombreCortoPerfil.Text.Trim, _
                                      Txt_CodigoExternoPerfil.Text.Trim, lstrEstado)


        If lstrResultadoTransaccion = "OK" Then
            Call ModificarUsuarios_del_Perfil(lintIdPerfilUsuario)
        End If


    End Sub
    Private Sub Elimina_PerfilUsuario()
        Dim lintIdPerfilUsuario As Integer = 0

        lintIdPerfilUsuario = Tdg_PerfilesUsuario.Columns("ID_PERFIL_USUARIO").Text.Trim
        lstrResultadoTransaccion = lobjPerfilUsuario.Eliminar(lintIdPerfilUsuario)

        If lstrResultadoTransaccion = "OK" Then
            lstrResultadoTransaccion = lobjPerfilesUsuarios.EliminarRelacionPerfilesUsuarios_PorPerfil(lintIdPerfilUsuario)
        End If

    End Sub
#End Region

#Region " --- Usuarios del Perfil ---"
    Private Sub Cmb_NegocioUsuarios_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Cmb_NegocioUsuarios.TextChanged
        Dim lintIdPerfilUsuario As Integer
        Dim lintIdNegocio As Integer
        lstrResultadoTransaccion = ""
        If Cmb_NegocioUsuarios.Text = "" Then Exit Sub
        If ltxtOperacion = "INSERTAR" Then
            lintIdPerfilUsuario = 0
            CargarUsuariosAsociados(lintIdPerfilUsuario, 0)
        Else
            lintIdPerfilUsuario = Tdg_PerfilesUsuario.Columns("ID_PERFIL_USUARIO").Value.ToString
        End If
        If Cmb_NegocioUsuarios.SelectedIndex <> -1 Then
            lintIdNegocio = Cmb_NegocioUsuarios.Columns(1).CellValue(Cmb_NegocioUsuarios.SelectedIndex).ToString()
            Tdg_UsuarioAsignados.Columns("ID_NEGOCIO").FilterText = lintIdNegocio
            CargarUsuariosDisponibles(lintIdPerfilUsuario, 0)
        End If
    End Sub
    Private Sub CargarUsuariosAsociados(ByVal intIdPerfilUsuario As Integer, ByVal intIdNegocio As Integer)
        Dim DR_Nueva As DataRow
        DS_UsuariosAsociados = Nothing
        Tdg_UsuarioAsignados.DataSource = Nothing
        DS_UsuariosAsociados = lobjPerfilesUsuarios.TraeUsuariosDelPerfil(lstrResultadoTransaccion, intIdPerfilUsuario, 0)
        DS_UsuariosAsociados_Proceso = DS_UsuariosAsociados.Clone
        DS_UsuariosAsociados_Proceso.Tables(0).Columns("NOMBRE_USUARIO").ColumnName = "ACCION"

        For Each DR_Fila As DataRow In DS_UsuariosAsociados.Tables(0).Rows
            DR_Nueva = DS_UsuariosAsociados_Proceso.Tables(0).NewRow
            DR_Nueva("ID_USUARIO") = DR_Fila("ID_USUARIO")
            DR_Nueva("ID_NEGOCIO") = DR_Fila("ID_NEGOCIO")
            DR_Nueva("ACCION") = "X"
            DS_UsuariosAsociados_Proceso.Tables(0).Rows.Add(DR_Nueva)
        Next
        DS_UsuariosAsociados_Proceso.Tables(0).AcceptChanges()
        Tdg_UsuarioAsignados.DataSource = DS_UsuariosAsociados.Tables(0)
        Call FormateaGrillaUsuariosAsociados()
        Tdg_UsuarioAsignados.Columns("ID_NEGOCIO").FilterText = intIdNegocio
    End Sub
    Private Sub CargarUsuariosDisponibles(ByVal intIdPerfilUsuario As Integer, ByVal intIdNegocio As Integer)
        Dim DR_Asociada As DataRow
        Dim lintFila As Integer
        Dim lintIdNegocio As Integer

        DS_UsuariosDisponibles = Nothing
        Tdg_UsuariosDisponibles.DataSource = Nothing
        DS_UsuariosDisponibles = lobjPerfilesUsuarios.TraeUsuariosDisponiblesPerfil(lstrResultadoTransaccion, intIdPerfilUsuario, 0)
        lintIdNegocio = Cmb_NegocioUsuarios.Columns(1).CellValue(Cmb_NegocioUsuarios.SelectedIndex).ToString()

        For lintFila = 0 To DS_UsuariosDisponibles.Tables(0).Rows.Count - 1
            For Each DR_Asociada In DS_UsuariosAsociados.Tables(0).Rows
                If DS_UsuariosDisponibles.Tables(0).Rows(lintFila)("ID_USUARIO") = DR_Asociada("ID_USUARIO") And DR_Asociada("ID_NEGOCIO") = lintIdNegocio Then
                    DS_UsuariosDisponibles.Tables(0).Rows(lintFila)("ID_NEGOCIO") = lintIdNegocio
                    Exit For
                End If
            Next

        Next
        DS_UsuariosDisponibles.Tables(0).AcceptChanges()
        Tdg_UsuariosDisponibles.DataSource = DS_UsuariosDisponibles.Tables(0)
        Tdg_UsuariosDisponibles.Columns("ID_NEGOCIO").FilterText = 0
        Call FormateaGrillaUsuariosDisponibles()
    End Sub

    Private Sub FormateaGrillaUsuariosAsociados()
        Dim LInt_Col As Integer
        Dim Tdg_Grilla As C1TrueDBGrid
        Tdg_Grilla = Tdg_UsuarioAsignados
        With Tdg_Grilla

            LInt_Col = 0 'ID_USUARIO
            .Columns(LInt_Col).Caption = "C�digo"
            .Splits(0).DisplayColumns(LInt_Col).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Near
            .Splits(0).DisplayColumns(LInt_Col).Width = 40
            .Columns(LInt_Col).DataField = "ID_USUARIO"
            .Splits(0).DisplayColumns(LInt_Col).Style.HorizontalAlignment = AlignHorzEnum.Near
            .Splits(0).DisplayColumns(LInt_Col).Visible = True
            .Splits(0).DisplayColumns(LInt_Col).AllowSizing = False

            LInt_Col = LInt_Col + 1 'NOMBRE_USUARIO
            .Columns(LInt_Col).Caption = "Nombre Usuario"
            .Splits(0).DisplayColumns(LInt_Col).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Near
            .Splits(0).DisplayColumns(LInt_Col).Width = 145
            .Columns(LInt_Col).DataField = "NOMBRE_USUARIO"
            .Splits(0).DisplayColumns(LInt_Col).Style.HorizontalAlignment = AlignHorzEnum.Near
            .Splits(0).DisplayColumns(LInt_Col).Visible = True
            .Splits(0).DisplayColumns(LInt_Col).AllowSizing = False

            LInt_Col = LInt_Col + 1 'ID_NEGOCIO
            .Columns(LInt_Col).Caption = "ID_NEGOCIO"
            .Splits(0).DisplayColumns(LInt_Col).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Near
            .Splits(0).DisplayColumns(LInt_Col).Width = 10
            .Columns(LInt_Col).DataField = "ID_NEGOCIO"
            .Splits(0).DisplayColumns(LInt_Col).Style.HorizontalAlignment = AlignHorzEnum.Near
            .Splits(0).DisplayColumns(LInt_Col).Visible = False
            .Splits(0).DisplayColumns(LInt_Col).AllowSizing = False
        End With
    End Sub
    Private Sub FormateaGrillaUsuariosDisponibles()
        Dim LInt_Col As Integer
        Dim Tdg_Grilla As C1TrueDBGrid
        Tdg_Grilla = Tdg_UsuariosDisponibles
        With Tdg_Grilla

            LInt_Col = 0 'ID_USUARIO
            .Columns(LInt_Col).Caption = "C�digo"
            .Splits(0).DisplayColumns(LInt_Col).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Near
            .Splits(0).DisplayColumns(LInt_Col).Width = 40
            .Columns(LInt_Col).DataField = "ID_USUARIO"
            .Splits(0).DisplayColumns(LInt_Col).Style.HorizontalAlignment = AlignHorzEnum.Near
            .Splits(0).DisplayColumns(LInt_Col).Visible = True
            .Splits(0).DisplayColumns(LInt_Col).AllowSizing = False

            LInt_Col = LInt_Col + 1 'NOMBRE_USUARIO
            .Columns(LInt_Col).Caption = "Nombre Usuario"
            .Splits(0).DisplayColumns(LInt_Col).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Near
            .Splits(0).DisplayColumns(LInt_Col).Width = 144
            .Columns(LInt_Col).DataField = "NOMBRE_USUARIO"
            .Splits(0).DisplayColumns(LInt_Col).Style.HorizontalAlignment = AlignHorzEnum.Near
            .Splits(0).DisplayColumns(LInt_Col).Visible = True
            .Splits(0).DisplayColumns(LInt_Col).AllowSizing = False

            LInt_Col = LInt_Col + 1 'ID_NEGOCIO
            .Columns(LInt_Col).Caption = "ID_NEGOCIO"
            .Splits(0).DisplayColumns(LInt_Col).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Near
            .Splits(0).DisplayColumns(LInt_Col).Width = 10
            .Columns(LInt_Col).DataField = "ID_NEGOCIO"
            .Splits(0).DisplayColumns(LInt_Col).Style.HorizontalAlignment = AlignHorzEnum.Near
            .Splits(0).DisplayColumns(LInt_Col).Visible = False
            .Splits(0).DisplayColumns(LInt_Col).AllowSizing = False

        End With
    End Sub

    Private Sub Btn_AsociarUsuario_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_AsociarUsuario.Click
        Dim lintIdNegocio As Integer
        Dim lintIdUsuario As Integer
        Dim lstrNombreUsuario As String
        Dim lintFila As Integer
        Dim DR_NuevaFila As DataRow

        If Tdg_UsuariosDisponibles.RowCount = 0 Then Exit Sub
        lintIdNegocio = Cmb_NegocioUsuarios.Columns(1).CellValue(Cmb_NegocioUsuarios.SelectedIndex).ToString().Trim
        lintIdUsuario = Tdg_UsuariosDisponibles.Columns("ID_USUARIO").Text.Trim
        lstrNombreUsuario = Tdg_UsuariosDisponibles.Columns("NOMBRE_USUARIO").Text.Trim
        For lintFila = 0 To DS_UsuariosDisponibles.Tables(0).Rows.Count - 1
            If DS_UsuariosDisponibles.Tables(0).Rows(lintFila)("ID_USUARIO") = lintIdUsuario Then
                DS_UsuariosDisponibles.Tables(0).Rows(lintFila)("ID_NEGOCIO") = lintIdNegocio
                DS_UsuariosDisponibles.Tables(0).AcceptChanges()
                Exit For
            End If
        Next

        Tdg_UsuariosDisponibles.DataSource = DS_UsuariosDisponibles.Tables(0)
        Tdg_UsuariosDisponibles.Columns("ID_NEGOCIO").FilterText = 0
        Call FormateaGrillaUsuariosDisponibles()

        DR_NuevaFila = DS_UsuariosAsociados.Tables(0).NewRow
        DR_NuevaFila("ID_USUARIO") = lintIdUsuario
        DR_NuevaFila("NOMBRE_USUARIO") = lstrNombreUsuario
        DR_NuevaFila("ID_NEGOCIO") = lintIdNegocio

        DS_UsuariosAsociados.Tables(0).Rows.Add(DR_NuevaFila)
        DS_UsuariosAsociados.Tables(0).AcceptChanges()
        Tdg_UsuarioAsignados.DataSource = DS_UsuariosAsociados.Tables(0)
        DS_UsuariosAsociados.Tables(0).DefaultView.Sort = "ID_USUARIO"

        Tdg_UsuarioAsignados.DataSource = DS_UsuariosAsociados.Tables(0)
        Call FormateaGrillaUsuariosAsociados()

    End Sub
    Private Sub Btn_QuitarUsuario_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_QuitarUsuario.Click
        Dim lintIdNegocio As Integer
        Dim lintIdUsuario As Integer
        Dim lstrNombreUsuario As String
        Dim lintFila As Integer
        Dim lintFilaBorrar As Integer
        Dim DR_NuevaFila As DataRow
        Dim lbnNuevo As Boolean = True

        If Tdg_UsuarioAsignados.RowCount = 0 Then Exit Sub
        lintIdNegocio = Cmb_NegocioUsuarios.Columns(1).CellValue(Cmb_NegocioUsuarios.SelectedIndex).ToString().Trim
        lintIdUsuario = Tdg_UsuarioAsignados.Columns("ID_USUARIO").Text.Trim
        lstrNombreUsuario = Tdg_UsuarioAsignados.Columns("NOMBRE_USUARIO").Text.Trim


        For lintFila = 0 To DS_UsuariosAsociados.Tables(0).Rows.Count - 1
            If DS_UsuariosAsociados.Tables(0).Rows(lintFila)("ID_USUARIO") = lintIdUsuario And DS_UsuariosAsociados.Tables(0).Rows(lintFila)("ID_NEGOCIO") = lintIdNegocio Then
                lintFilaBorrar = lintFila
                Exit For
            End If
        Next

        DS_UsuariosAsociados.Tables(0).Rows(lintFilaBorrar).Delete()
        DS_UsuariosAsociados.Tables(0).AcceptChanges()
        Tdg_UsuarioAsignados.DataSource = DS_UsuariosAsociados.Tables(0)
        Call FormateaGrillaUsuariosAsociados()


        For lintFila = 0 To DS_UsuariosDisponibles.Tables(0).Rows.Count - 1
            If DS_UsuariosDisponibles.Tables(0).Rows(lintFila)("ID_USUARIO") = lintIdUsuario And DS_UsuariosDisponibles.Tables(0).Rows(lintFila)("ID_NEGOCIO") = lintIdNegocio Then
                DS_UsuariosDisponibles.Tables(0).Rows(lintFila)("ID_NEGOCIO") = 0
                lbnNuevo = False
                Exit For
            End If
        Next

        If lbnNuevo Then
            DR_NuevaFila = DS_UsuariosDisponibles.Tables(0).NewRow
            DR_NuevaFila("ID_USUARIO") = lintIdUsuario
            DR_NuevaFila("NOMBRE_USUARIO") = lstrNombreUsuario
            DR_NuevaFila("ID_NEGOCIO") = lintIdNegocio

            DS_UsuariosDisponibles.Tables(0).Rows.Add(DR_NuevaFila)
            DS_UsuariosDisponibles.Tables(0).AcceptChanges()
            Tdg_UsuariosDisponibles.DataSource = DS_UsuariosDisponibles.Tables(0)
            DS_UsuariosDisponibles.Tables(0).DefaultView.Sort = "ID_USUARIO"
        End If

        Tdg_UsuarioAsignados.DataSource = DS_UsuariosAsociados.Tables(0)
        Call FormateaGrillaUsuariosAsociados()
        Tdg_UsuariosDisponibles.Columns("ID_NEGOCIO").FilterText = 0
    End Sub

    Private Sub AgregarUsuarios_al_Perfil(ByVal intIdPerfilUsuario As Integer)
        lstrResultadoTransaccion = lobjPerfilesUsuarios.AgregarRelacionPerfilesUsuarios_PorPerfil(intIdPerfilUsuario, DS_UsuariosAsociados)

        If lstrResultadoTransaccion <> "OK" Then
            lstrResultadoTransaccion = "No se agregaron los Usuarios al Perfil." & vbCrLf & lstrResultadoTransaccion
        End If
    End Sub
    Private Sub ModificarUsuarios_del_Perfil(ByVal intIdPerfilUsuario As Integer)
        Dim DR_Nueva As DataRow
        Dim lBlnNuevo As Boolean
        Dim lBlnEliminada As Boolean

        For Each DR_Asociada As DataRow In DS_UsuariosAsociados.Tables(0).Rows
            lBlnNuevo = True
            For Each DR_Original As DataRow In DS_UsuariosAsociados_Proceso.Tables(0).Rows
                If DR_Original("ID_USUARIO") = DR_Asociada("ID_USUARIO") And DR_Original("ID_NEGOCIO") = DR_Asociada("ID_NEGOCIO") Then
                    lBlnNuevo = False
                    Exit For
                End If
            Next

            If lBlnNuevo Then
                DR_Nueva = DS_UsuariosAsociados_Proceso.Tables(0).NewRow
                DR_Nueva("ID_USUARIO") = DR_Asociada("ID_USUARIO")
                DR_Nueva("ID_NEGOCIO") = DR_Asociada("ID_NEGOCIO")
                DR_Nueva("ACCION") = "A"
                DS_UsuariosAsociados_Proceso.Tables(0).Rows.Add(DR_Nueva)
                DS_UsuariosAsociados_Proceso.Tables(0).AcceptChanges()
            End If
        Next
        For Each DR_Original As DataRow In DS_UsuariosAsociados_Proceso.Tables(0).Rows
            lBlnEliminada = True
            For Each DR_Asociada As DataRow In DS_UsuariosAsociados.Tables(0).Rows
                If DR_Asociada("ID_USUARIO") = DR_Original("ID_USUARIO") And DR_Asociada("ID_NEGOCIO") = DR_Original("ID_NEGOCIO") Then
                    lBlnEliminada = False
                    Exit For
                End If
            Next
            If lBlnEliminada Then
                DR_Original("ACCION") = "E"
                DS_UsuariosAsociados_Proceso.Tables(0).AcceptChanges()
            End If
        Next
        lstrResultadoTransaccion = lobjPerfilesUsuarios.CambiarRelacionPerfilesUsuarios_PorPerfil(intIdPerfilUsuario, DS_UsuariosAsociados_Proceso)

        If lstrResultadoTransaccion <> "OK" Then
            lstrResultadoTransaccion = "No se Modificaron los Usuarios al Perfil." & vbCrLf & lstrResultadoTransaccion
        End If
    End Sub

#End Region

    Private Sub Ctrl_Botonera_Agregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Ctrl_Botonera_Agregar.Click

    End Sub

    Private Sub Ctrl_Botonera_Eliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Ctrl_Botonera_Eliminar.Click

    End Sub

    Private Sub Ctrl_Botonera_Editar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Ctrl_Botonera_Editar.Click

    End Sub

    Private Sub Ctrl_Botonera_Guardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Ctrl_Botonera_Guardar.Click

    End Sub

    Private Sub Ctrl_Botonera_Cancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Ctrl_Botonera_Cancelar.Click

    End Sub
End Class