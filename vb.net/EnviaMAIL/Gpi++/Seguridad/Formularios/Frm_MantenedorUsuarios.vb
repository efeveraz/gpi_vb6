Imports C1.Win.C1TrueDBGrid
Imports C1.Win.C1List.C1Combo
Imports System.Windows.Forms.ComboBox

Public Class Frm_MantenedorUsuarios

#Region " --- Mantenedor de Usuarios - Declaraciones ---"

    Public PrevSizeX As Integer = Me.Width
    Public PrevSizeY As Integer = Me.Height

    Dim ltxtPUsuario As String = ""
    Dim ltxtCodUsuario As String = ""

    Dim ltxtResultadoTransaccion As String = ""
    Dim ltxtColumnas As String = ""
    Dim ltxtOperacion As String = ""
    Dim lstrResultadoTransaccion As String = ""

    Dim lblnMoviendoElFormulario As Boolean = False
    Dim lMouseCoordenadaX As Integer = 0
    Dim lMouseCoordenadaY As Integer = 0

    Dim lintIdNegocio As Integer

    Private ldrvClienteSeleccionado As DataRowView
    Private ldrvCuentaSeleccionada As DataRowView

    Dim lobjUsuario As ClsUsuario = New ClsUsuario
    Dim lobjPerfilUsuario As ClsPerfilUsuario = New ClsPerfilUsuario
    Dim lobjPerfilesUsuarios As ClsPerfilesUsuarios = New ClsPerfilesUsuarios
    Dim lobjUnidad As ClsUnidad = New ClsUnidad
    Dim lobjTipoUsuario As ClsTipoUsuario = New ClsTipoUsuario
    Dim lobjAsesor As ClsAsesor = New ClsAsesor
    Dim lobjNegocio As ClsNegociosUsuarios = New ClsNegociosUsuarios
    Dim lobjPerfilRiesgo As ClsPerfilRiesgo = New ClsPerfilRiesgo
    Dim lobjTipoAdministracion As ClsTipoAdministracion = New ClsTipoAdministracion
    Dim lobjSucursal As ClsSucursal = New ClsSucursal
    Dim lobjCuentas As ClsCuentas = New ClsCuentas
    Dim lobjCuentasSeguridad As ClsCuentasUsuarios = New ClsCuentasUsuarios

    Dim DS_Usuarios As New DataSet
    Dim DS_PerfilesUsuariosDisponibles As New DataSet
    Dim DS_PerfilesUsuariosAsignados As New DataSet
    Dim DS_Unidad As New DataSet
    Dim DS_TipoUsuario As New DataSet
    Dim DS_TipoControl As New DataSet
    Dim DS_Asesores_Disponibles As New DataSet
    Dim DS_Negocios As New DataSet
    Dim DS_NegociosCuentas As New DataSet
    Dim DS_PerfilRiesgo As New DataSet
    Dim DS_TipoAdministracion As New DataSet
    Dim DS_Sucursal As New DataSet
    Dim DS_Clientes As DataSet = New DataSet
    Dim DS_Cuentas As DataSet = New DataSet
    Dim DS_CuentasDisponibles As DataSet = New DataSet
    Dim DS_CuentasAsociadas As DataSet = New DataSet

    Dim DT_PerfilesDisponibles_Original As New DataTable("PerfilesUsuariosDisponibles")
    Dim DT_PerfilesAsignados_Original As New DataTable("PerfilesUsuariosAsignados")

    Dim lstrColumnas As String = ""
    Dim DS_General As New DataSet

    Dim bln_PressControl As Boolean = False
    Dim bln_PressShift As Boolean = False
    Dim int_PrimeraFila As Integer = 0
    Dim int_UltimaFila As Integer = 0
    Dim lint_Fila As Integer = 0
    Dim lstyEncabezadoIzq, lstyEncabezadoCen, lstyEncabezadoDer As C1.Win.C1FlexGrid.CellStyle
    Dim lstyCeldaHoja, lstyCeldaPadre As C1.Win.C1FlexGrid.CellStyle

#End Region

#Region " --- Formulario ---"

    Public Sub CargarFormulario()
        ltxtPUsuario = "" 'oApp.NombreUsuario
        ltxtCodUsuario = "" 'oApp.UsuarioLogeado

        Me.Location = New Point(3, 3)
        'Me.Location = New Point(gobjFormulario._PosX, gobjFormulario._PosY)
        If gobjFormulario._Modal Then
            ShowDialog()
        Else
            Show()
            BringToFront()
        End If
    End Sub

    Private Sub AbrirFormulario(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.MinimumSize = Me.Size
        CargaGrilla()
        InicializaFormulario()
        'CargaCuentasAsociadas(0)
        CuentasDisponibles()
    End Sub
    Private Sub Frm_MantenedorUsuarios_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.GotFocus, Me.Click
        Me.BringToFront()
    End Sub
    'Private Sub Me_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
    '    If (e.KeyCode = Keys.F4) And e.Modifiers = Keys.Alt Then
    '        e.Handled = True
    '        Btn_Salir_Click(sender, e)
    '    ElseIf e.KeyCode = Keys.Enter Then
    '        e.Handled = True
    '        SendKeys.Send("{TAB}")
    '    ElseIf e.KeyCode = Keys.Escape Then
    '        e.Handled = True
    '        Btn_Cancelar_Click(sender, e)
    '    End If
    'End Sub
    Private Sub Me_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.DoubleClick
        Me.Location = New Point(3, 3)
    End Sub
    Private Sub Me_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Me.Dispose()
    End Sub
    'Private Sub Btn_Salir_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Salir.Click
    '    Me.Close()
    'End Sub
    Private Sub Formulario_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles Me.MouseDown
        If e.Button = MouseButtons.Left Then
            lblnMoviendoElFormulario = True
            lMouseCoordenadaX = e.X
            lMouseCoordenadaY = e.Y
        End If
    End Sub
    Private Sub Formulario_MouseUp(ByVal sender As Object, ByVal e As MouseEventArgs) Handles Me.MouseUp
        If e.Button = MouseButtons.Left Then
            lblnMoviendoElFormulario = False
        End If
    End Sub
    Private Sub Formulario_MouseMove(ByVal sender As Object, ByVal e As MouseEventArgs) Handles Me.MouseMove
        If lblnMoviendoElFormulario Then
            '...Arriba
            If Me.Location.Y < 1 Then
                Me.Location = New Point(Me.Location.X, 1)
                lblnMoviendoElFormulario = False
                '...Abajo
            ElseIf Me.Location.Y > (sender.parent.Height - 30) Then
                Me.Location = New Point(Me.Location.X, sender.parent.Height - 30)
                lblnMoviendoElFormulario = False
                '...Izquierda
            ElseIf Me.Location.X < ((Me.Width / 10) - Me.Width) Then
                Me.Location = New Point(((Me.Width / 10) - Me.Width), Me.Location.Y)
                lblnMoviendoElFormulario = False
                '...Derecha
            ElseIf Me.Location.X > (sender.parent.Width - 150) Then
                Me.Location = New Point((sender.parent.Width - 150), Me.Location.Y)
                lblnMoviendoElFormulario = False
            Else
                lblnMoviendoElFormulario = True
                Dim temp As Point = New Point()
                temp.X = Me.Location.X + (e.X - lMouseCoordenadaX)
                temp.Y = Me.Location.Y + (e.Y - lMouseCoordenadaY)
                Me.Location = temp
                temp = Nothing
            End If
        End If
    End Sub

    Private Sub InicializaFormulario()

        CargaUnidad()
        CargaTipoUsuario()
        CargaComboTipoControl(Me.Cmb_TipoControl, Me.Text, True)
        CargaEstado()
        CargaPerfilRiesgo()
        CargaTipoAdministracion()
        CargaSucursal()
        CargaAsesor("", Me.Cmb_Asesores)
        ltxtOperacion = ""

        HabilitaDeshabilitaDatos()

        gSubHabilitaControl(Txt_NombreCliente, True, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
        Cmb_Unidad.SelectedIndex = -1
        Cmb_TipoUsuario.SelectedIndex = -1
        Cmb_TipoControl.SelectedIndex = -1
        Cmb_Estado.SelectedIndex = -1
        Cmb_PerfilRiesgo.SelectedIndex = -1
        Cmb_Asesores.SelectedIndex = -1

        Limpiar_Campos()

        Tdg_PerfilesAsignados.DataSource = Nothing
        Tdg_PerfilesDisponibles.DataSource = Nothing
        Pnl_PerfilesUsuarios.Enabled = False
        Pnl_CuentasUsuario.Enabled = False

        Tdg_Usuarios.SelectedRows.Clear()
        Tdg_Usuarios.Row = -1
        Ctrl_Botonera_Agregar.Enabled = True
        Ctrl_Botonera_Guardar.Enabled = False
        Ctrl_Botonera_Cancelar.Enabled = Ctrl_Botonera_Guardar.Enabled
        Ctrl_Botonera_Editar.Enabled = False
        Ctrl_Botonera_Eliminar.Enabled = False
        Tab_Contenedor.SelectedIndex = 0

    End Sub
    Private Sub HabilitaDeshabilitaDatos()
        Select Case ltxtOperacion.Trim
            Case Is = "INSERTAR"
                gSubHabilitaControl(Txt_NombreUsuario, False, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Txt_NombreCorto, False, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Txt_Login, False, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Cmb_Unidad, False, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Txt_CodigoExterno, False, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Txt_Cargo, False, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Txt_Telefono, False, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Cmb_TipoUsuario, False, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Cmb_TipoControl, False, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Cmb_Estado, True, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Ded_FechaLiquidacion, True, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Chk_TodasCuentas, False, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                'gSubHabilitaControl(Chk_EsAsesor, False, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                'gSubHabilitaControl(Cmb_AsesoresDisponibles, True, gColorBackGroundEnabled, gColorBackGroundDesabled, True)

                gSubHabilitaControl(Cmb_NegocioCuentas, True, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Txt_NumeroCuenta, False, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Txt_NombreCuenta, False, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Txt_RutCliente, False, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Txt_NombreCliente, False, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Cmb_TipoAdministracion, False, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Cmb_PerfilRiesgo, False, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Cmb_Segmento, False, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Cmb_Sucursal, False, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Cmb_Asesores, False, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(txtEmailUsuario, False, gColorBackGroundEnabled, gColorBackGroundDesabled, True)

            Case Is = "MODIFICAR"
                gSubHabilitaControl(Txt_NombreUsuario, False, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Txt_NombreCorto, False, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Txt_Login, False, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Cmb_Unidad, False, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Txt_CodigoExterno, False, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Txt_Cargo, False, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Txt_Telefono, False, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Cmb_TipoUsuario, False, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Cmb_TipoControl, False, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Cmb_Estado, False, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Ded_FechaLiquidacion, False, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Chk_TodasCuentas, False, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                'gSubHabilitaControl(Chk_EsAsesor, False, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                'gSubHabilitaControl(Cmb_AsesoresDisponibles, Not Chk_EsAsesor.Checked, gColorBackGroundEnabled, gColorBackGroundDesabled, True)

                gSubHabilitaControl(Cmb_NegocioCuentas, True, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Txt_NumeroCuenta, False, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Txt_NombreCuenta, False, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Txt_RutCliente, False, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Txt_NombreCliente, False, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Cmb_TipoAdministracion, False, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Cmb_PerfilRiesgo, False, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Cmb_Segmento, False, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Cmb_Sucursal, False, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Cmb_Asesores, False, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(txtEmailUsuario, False, gColorBackGroundEnabled, gColorBackGroundDesabled, True)

            Case Is = "ELIMINAR", ""
                gSubHabilitaControl(Txt_NombreUsuario, True, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Txt_NombreCorto, True, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Txt_Login, True, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Cmb_Unidad, True, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Txt_CodigoExterno, True, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Txt_Cargo, True, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Txt_Telefono, True, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Cmb_TipoUsuario, True, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Cmb_TipoControl, True, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Cmb_Estado, True, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Ded_FechaLiquidacion, True, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Chk_TodasCuentas, True, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                'gSubHabilitaControl(Chk_EsAsesor, True, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                'gSubHabilitaControl(Cmb_AsesoresDisponibles, True, gColorBackGroundEnabled, gColorBackGroundDesabled, True)

                gSubHabilitaControl(Cmb_NegocioCuentas, True, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Txt_NumeroCuenta, True, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Txt_NombreCuenta, True, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Txt_RutCliente, True, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Txt_NombreCliente, True, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Cmb_TipoAdministracion, True, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Cmb_PerfilRiesgo, True, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Cmb_Segmento, True, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Cmb_Sucursal, True, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(Cmb_Asesores, True, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
                gSubHabilitaControl(txtEmailUsuario, True, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
        End Select
    End Sub
    Public Sub Limpiar_Campos()

        ' tab usuario
        Txt_NombreUsuario.Text = ""
        Txt_NombreCorto.Text = ""
        Cmb_Unidad.SelectedIndex = -1
        Txt_Login.Text = ""
        Cmb_TipoUsuario.SelectedIndex = -1
        Cmb_TipoControl.SelectedIndex = -1
        Txt_Telefono.Text = ""
        Txt_Cargo.Text = ""
        Txt_CodigoExterno.Text = ""
        Cmb_Estado.SelectedIndex = -1
        Ded_FechaLiquidacion.Value = gdteFechaSistema.AddDays(gobjParametro.Dias_Exp_Clave.ValorParametro)
        Ded_FechaLiquidacion.Value = DateAdd(DateInterval.Year, 1, Ded_FechaLiquidacion.Value)
        Chk_TodasCuentas.Checked = False
        txtEmailUsuario.Text = String.Empty
        ' tab Perfiles
        'DS_PerfilesUsuariosDisponibles = Nothing
        'DS_PerfilesUsuariosAsignados = Nothing
        'Tdg_PerfilesAsignados.DataSource = Nothing
        'Tdg_PerfilesDisponibles.DataSource = Nothing

        ' tab cuentas
        'DS_Cuentas = Nothing
        'DS_CuentasDisponibles = Nothing
        'DS_CuentasAsociadas = Nothing
        Tdg_CuentasDisponibles.DataSource = Nothing
        Tdg_CuentasAsociadas.DataSource = Nothing

        CuentasDisponibles()
        CargaCuentasAsociadas(0)

        Txt_NumeroCuenta.Text = ""
        Txt_NombreCuenta.Text = ""
        Txt_RutCliente.Text = ""
        Txt_NombreCliente.Text = ""
        Cmb_TipoAdministracion.SelectedIndex = -1
        Cmb_PerfilRiesgo.SelectedIndex = -1
        Cmb_Segmento.SelectedIndex = -1
        Cmb_Sucursal.SelectedIndex = -1
    End Sub

#End Region

#Region " --- Eventos Formulario ---"

    Private Sub Tab_Contenedor_SelectedIndexChanging(ByVal sender As Object, ByVal e As C1.Win.C1Command.SelectedIndexChangingEventArgs) Handles Tab_Contenedor.SelectedIndexChanging
        If e.NewIndex = 2 Then

            If TienePerfilAsociado() Then
                MostrarTabCuentas()
            Else
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub MostrarTabCuentas()
        Dim lintUsuario As Integer
        If ltxtOperacion = "INSERTAR" Or Tdg_Usuarios.RowCount <= 0 Then
            lintUsuario = 0
        Else
            lintUsuario = Tdg_Usuarios.Columns("ID_USUARIO").Text.Trim
        End If
        Txt_NumeroCuenta.Text = ""
        Txt_NombreCuenta.Text = ""
        Txt_NombreCliente.Text = ""
        Cmb_TipoAdministracion.SelectedIndex = -1
        Cmb_PerfilRiesgo.SelectedIndex = -1
        Cmb_Segmento.SelectedIndex = -1
        Cmb_Sucursal.SelectedIndex = -1
        Tdg_CuentasDisponibles.DataSource = Nothing

        CuentasDisponibles()
        CargaCuentasAsociadas(0)

        CargaNegociosCuenta()
        CargaCuentasAsociadas(lintUsuario)
    End Sub
#End Region

#Region " --- Funciones / Procedimientos ---"

    Private Sub CargaAsesor(ByVal strCodigoAsesor As String, ByRef meComboBox As C1.Win.C1List.C1Combo)
        lstrColumnas = "NOMBRE,ID_ASESOR"
        '...Limpia Combo
        meComboBox.ClearItems()
        meComboBox.Text = ""
        '...Inicializa Data Set Combo
        DS_General = Nothing
        DS_General = lobjAsesor.Asesor_Ver("", "", "VIG", "N", lstrColumnas, lstrResultadoTransaccion)
        Try
            If lstrResultadoTransaccion.ToUpper <> "OK" Then
                MsgBox("Error al cargar Combo Asesores : " & strCodigoAsesor, MsgBoxStyle.Information, gtxtNombreSistema & " Mantenedor de Clientes / Cuentas.")
            Else
                '...Asigna Data Source
                meComboBox.DataSource = DS_General.Tables("Asesor")
                meComboBox.Sort(0, C1.Win.C1List.SortDirEnum.ASC)
                '...Ordena y visibilidad de columnas del combo
                meComboBox.Columns(0).DataField = "NOMBRE"
                meComboBox.Splits(0).DisplayColumns(0).Visible = True
                meComboBox.Columns(1).DataField = "ID_ASESOR"
                meComboBox.Splits(0).DisplayColumns(1).Visible = False
            End If
        Catch ex As Exception
            MsgBox("Error al cargar Combo Asesores : " & strCodigoAsesor, MsgBoxStyle.Information, gtxtNombreSistema & " Mantenedor de Clientes / Cuentas.")
        End Try
    End Sub
#End Region

#Region " --- TAB Usuarios ---"
    Private Sub CargaUnidad()

        Cmb_Unidad.AddItem(";")


        DS_Unidad = lobjUnidad.RetornaUnidades(ltxtResultadoTransaccion, "NOMBRE_UNIDAD, ID_UNIDAD", 0, "", "", "", "")

        If ltxtResultadoTransaccion = "OK" Then
            If DS_Unidad.Tables("UNIDAD").Rows.Count > 0 Then
                Cmb_Unidad.DataSource = DS_Unidad.Tables("UNIDAD")

                Cmb_Unidad.Columns(0).DataField = "NOMBRE_UNIDAD"
                Cmb_Unidad.Columns(1).DataField = "ID_UNIDAD"

                Cmb_Unidad.Splits(0).DisplayColumns(1).Visible = False
                Cmb_Unidad.Splits(0).DisplayColumns(0).Visible = True
            End If
        Else
            MsgBox(ltxtResultadoTransaccion, , gtxtNombreSistema & Me.Text)
        End If

    End Sub
    Private Sub CargaTipoUsuario()
        DS_TipoUsuario = lobjTipoUsuario.RetornaTiposUsuarios(ltxtResultadoTransaccion, "NOMBRE_TIPO_USUARIO, ID_TIPO_USUARIO", 0, "", "", "", "")
        If ltxtResultadoTransaccion = "OK" Then
            If DS_TipoUsuario.Tables("TIPOUSUARIO").Rows.Count > 0 Then
                Cmb_TipoUsuario.DataSource = DS_TipoUsuario.Tables("TIPOUSUARIO")

                Cmb_TipoUsuario.Columns(0).DataField = "NOMBRE_TIPO_USUARIO"
                Cmb_TipoUsuario.Columns(1).DataField = "ID_TIPO_USUARIO"

                Cmb_TipoUsuario.Splits(0).DisplayColumns(0).Visible = True
                Cmb_TipoUsuario.Splits(0).DisplayColumns(1).Visible = False
                Cmb_TipoUsuario.AddItem("0;TODOS")

            End If
        Else
            MsgBox(ltxtResultadoTransaccion, , gtxtNombreSistema & Me.Text)
        End If
    End Sub
    Private Sub CargaEstado()

        Cmb_Estado.ClearItems()
        Cmb_Estado.AddItem("VIG;VIGENTE")
        Cmb_Estado.AddItem("BLO;BLOQUEADO")
        Cmb_Estado.AddItem("ELI;ELIMINADO")
        Cmb_Estado.AddItem("PWD;CAMBIOCLAVE")

        Cmb_Estado.Splits(0).DisplayColumns(0).Visible = False
        Cmb_Estado.Splits(0).DisplayColumns(1).Visible = True
    End Sub
    'Private Sub CargaAsesores(ByVal intIdUsuario As Integer)

    '    DS_Asesores_Disponibles = lobjAsesor.RetornaAsesoresDisponibles(ltxtResultadoTransaccion, intIdUsuario)
    '    If DS_Asesores_Disponibles.Tables.Count > 0 Then
    '        Cmb_AsesoresDisponibles.DataSource = DS_Asesores_Disponibles.Tables("ASESOR")
    '        'Cmb_AsesoresDisponibles.DataMember(0) = "NOMBRE_ASESOR"
    '        Cmb_AsesoresDisponibles.ColumnHeaders = True
    '        Cmb_AsesoresDisponibles.Columns(0).DataField = "NOMBRE_ASESOR"
    '        Cmb_AsesoresDisponibles.Columns(1).DataField = "RUT_ASESOR"
    '        Cmb_AsesoresDisponibles.Columns(2).DataField = "ID_ASESOR"
    '        Cmb_AsesoresDisponibles.Columns(3).DataField = "EMAIL_ASESOR"
    '        Cmb_AsesoresDisponibles.Columns(4).DataField = "FONO_ASESOR"
    '        Cmb_AsesoresDisponibles.Columns(5).DataField = "ABRNOMBRE_ASESOR"
    '        Cmb_AsesoresDisponibles.Columns(6).DataField = "BLOQUEO_ASESOR"
    '        Cmb_AsesoresDisponibles.Columns(7).DataField = "ID_USUARIO"

    '        Cmb_AsesoresDisponibles.Splits(0).DisplayColumns("NOMBRE_ASESOR").Visible = True
    '        Cmb_AsesoresDisponibles.Splits(0).DisplayColumns("RUT_ASESOR").Visible = True
    '        Cmb_AsesoresDisponibles.Splits(0).DisplayColumns("ID_ASESOR").Visible = False
    '        Cmb_AsesoresDisponibles.Splits(0).DisplayColumns("EMAIL_ASESOR").Visible = False
    '        Cmb_AsesoresDisponibles.Splits(0).DisplayColumns("FONO_ASESOR").Visible = False
    '        Cmb_AsesoresDisponibles.Splits(0).DisplayColumns("ABRNOMBRE_ASESOR").Visible = False
    '        Cmb_AsesoresDisponibles.Splits(0).DisplayColumns("BLOQUEO_ASESOR").Visible = False
    '        Cmb_AsesoresDisponibles.Splits(0).DisplayColumns("ID_USUARIO").Visible = False
    '    End If
    'End Sub
    Private Sub CargaPerfilRiesgo()
        Dim dr_nuevafila As DataRow
        Cmb_PerfilRiesgo.DataMode = C1.Win.C1List.DataModeEnum.Normal

        ltxtResultadoTransaccion = "OK"
        DS_PerfilRiesgo = lobjPerfilRiesgo.TraeDatosPerfilRiesgo(ltxtResultadoTransaccion)
        If ltxtResultadoTransaccion = "OK" Then
            If DS_PerfilRiesgo.Tables(0).Rows.Count > 0 Then
                dr_nuevafila = DS_PerfilRiesgo.Tables(0).NewRow
                dr_nuevafila("ID_PERFIL_RIESGO") = 0
                dr_nuevafila("DSC_PERFIL_RIESGO") = ""
                DS_PerfilRiesgo.Tables(0).Rows.InsertAt(dr_nuevafila, 0)
                DS_PerfilRiesgo.Tables(0).AcceptChanges()
                Cmb_PerfilRiesgo.DataSource = DS_PerfilRiesgo.Tables(0)

                Cmb_PerfilRiesgo.Columns(0).DataField = "ID_PERFIL_RIESGO"
                Cmb_PerfilRiesgo.Columns(1).DataField = "DSC_PERFIL_RIESGO"
                Cmb_PerfilRiesgo.Splits(0).DisplayColumns(0).Visible = False
                Cmb_PerfilRiesgo.Splits(0).DisplayColumns(1).Visible = True
                Cmb_PerfilRiesgo.DisplayMember = "DSC_PERFIL_RIESGO"

                For lintcol As Integer = 2 To Cmb_PerfilRiesgo.Columns.Count - 1
                    Cmb_PerfilRiesgo.Splits(0).DisplayColumns(lintcol).Visible = False
                Next
            End If
        End If

    End Sub
    Private Sub CargaTipoAdministracion()
        Dim dr_nuevafila As DataRow
        ltxtResultadoTransaccion = "OK"
        DS_TipoAdministracion = lobjTipoAdministracion.TipoAdministracion_Ver("", "", "DSC_TIPO_ADMINISTRACION,COD_TIPO_ADMINISTRACION ", ltxtResultadoTransaccion)
        If ltxtResultadoTransaccion = "OK" Then
            If DS_TipoAdministracion.Tables(0).Rows.Count > 0 Then
                dr_nuevafila = DS_TipoAdministracion.Tables(0).NewRow
                dr_nuevafila("COD_TIPO_ADMINISTRACION") = ""
                dr_nuevafila("DSC_TIPO_ADMINISTRACION") = ""
                DS_TipoAdministracion.Tables(0).Rows.InsertAt(dr_nuevafila, 0)
                DS_TipoAdministracion.Tables(0).AcceptChanges()
                Cmb_TipoAdministracion.DataSource = DS_TipoAdministracion.Tables(0)

                Cmb_TipoAdministracion.Columns(0).DataField = "DSC_TIPO_ADMINISTRACION"
                Cmb_TipoAdministracion.Columns(1).DataField = "COD_TIPO_ADMINISTRACION"

                Cmb_TipoAdministracion.Splits(0).DisplayColumns(0).Visible = True
                Cmb_TipoAdministracion.Splits(0).DisplayColumns(1).Visible = False
            End If
        End If

    End Sub
    Private Sub CargaSucursal()
        Dim dr_nuevafila As DataRow
        ltxtResultadoTransaccion = "OK"
        DS_Sucursal = lobjSucursal.RetornaSucursal(ltxtResultadoTransaccion, "DSC_SUCURSAL, COD_SUCURSAL,ID_SUCURSAL")
        If ltxtResultadoTransaccion = "OK" Then
            If DS_Sucursal.Tables(0).Rows.Count > 0 Then
                dr_nuevafila = DS_Sucursal.Tables(0).NewRow
                dr_nuevafila("COD_SUCURSAL") = ""
                dr_nuevafila("DSC_SUCURSAL") = ""
                DS_Sucursal.Tables(0).Rows.InsertAt(dr_nuevafila, 0)
                DS_Sucursal.Tables(0).AcceptChanges()
                Cmb_Sucursal.DataSource = DS_Sucursal.Tables(0)

                Cmb_Sucursal.Columns(0).DataField = "DSC_SUCURSAL"
                Cmb_Sucursal.Columns(1).DataField = "COD_SUCURSAL"


                Cmb_Sucursal.Splits(0).DisplayColumns(0).Visible = True
                Cmb_Sucursal.Splits(0).DisplayColumns(1).Visible = False
            End If
        End If

    End Sub

    Private Sub CargaGrilla()
        ltxtResultadoTransaccion = ""
        DS_Usuarios = lobjUsuario.Buscar(lstrResultadoTransaccion, 0, "", "", "")
        If lstrResultadoTransaccion = "OK" Then
            If (DS_Usuarios.Tables("USUARIO").Rows.Count >= 0) Then
                Tdg_Usuarios.DataSource = DS_Usuarios.Tables("USUARIO")
                FormateaGrillaUsuarios()
                Tdg_Usuarios.Row = -1
                MuestraDatosDesdeGrilla()
                Tdg_Usuarios.Focus()
                StatusLabel.Text = gtxtNombreSistema & " Cantidad de usuarios: " & Tdg_Usuarios.RowCount.ToString
                Tab_Usuario.Select()
            End If
        Else
            MsgBox("Se produjo un error al tratar de mostrar el listado de Usuarios, intente abrir nuevamente el mantenedor.", , gtxtNombreSistema & Me.Text)
            StatusLabel.Text = gtxtNombreSistema & " Cantidad de usuarios: 0"
        End If
    End Sub
    Private Sub FormateaGrillaUsuarios()

        Dim LInt_Col As Integer
        Dim Tdg_Grilla As C1.Win.C1TrueDBGrid.C1TrueDBGrid
        Tdg_Grilla = Tdg_Usuarios
        Tdg_Grilla.Splits(0).HScrollBar.Style = ScrollBarStyleEnum.None
        Tdg_Grilla.AllowFilter = True

        If (Tdg_Grilla.RowCount >= 0) Then
            Tdg_Grilla.Splits(0).HScrollBar.Style = C1.Win.C1TrueDBGrid.ScrollBarStyleEnum.Automatic
            Tdg_Grilla.Splits(0).VScrollBar.Style = C1.Win.C1TrueDBGrid.ScrollBarStyleEnum.Always

            With Tdg_Grilla
                .AllowFilter = False
                .FilterBar = True
                .FetchRowStyles = True
                LInt_Col = 0
                .Columns(LInt_Col).Caption = "Id Usuario"
                .Splits(0).DisplayColumns(LInt_Col).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Width = 50
                .Columns(LInt_Col).DataField = "ID_USUARIO"
                .Splits(0).DisplayColumns(LInt_Col).Style.HorizontalAlignment = AlignHorzEnum.Center
                .Splits(0).DisplayColumns(LInt_Col).Visible = False
                .Splits(0).DisplayColumns(LInt_Col).AllowSizing = False

                LInt_Col = LInt_Col + 1
                .Columns(LInt_Col).Caption = "Id Tipo Usuario"
                .Splits(0).DisplayColumns(LInt_Col).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Width = 50
                .Columns(LInt_Col).DataField = "ID_TIPO_USUARIO"
                .Splits(0).DisplayColumns(LInt_Col).Style.HorizontalAlignment = AlignHorzEnum.Center
                .Splits(0).DisplayColumns(LInt_Col).Visible = False
                .Splits(0).DisplayColumns(LInt_Col).AllowSizing = False

                LInt_Col = LInt_Col + 1
                .Columns(LInt_Col).Caption = "Id Unidad"
                .Splits(0).DisplayColumns(LInt_Col).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Width = 50
                .Columns(LInt_Col).DataField = "ID_UNIDAD"
                .Splits(0).DisplayColumns(LInt_Col).Style.HorizontalAlignment = AlignHorzEnum.Center
                .Splits(0).DisplayColumns(LInt_Col).Visible = False
                .Splits(0).DisplayColumns(LInt_Col).AllowSizing = False

                LInt_Col = LInt_Col + 1
                .Columns(LInt_Col).Caption = "Nombre"
                .Splits(0).DisplayColumns(LInt_Col).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Width = 220
                .Columns(LInt_Col).DataField = "NOMBRE_USUARIO"
                .Splits(0).DisplayColumns(LInt_Col).Style.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Visible = True
                .Splits(0).DisplayColumns(LInt_Col).AllowSizing = False

                LInt_Col = LInt_Col + 1
                .Columns(LInt_Col).Caption = "Nombre Corto"
                .Splits(0).DisplayColumns(LInt_Col).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Width = 75
                .Columns(LInt_Col).DataField = "NOMBRE_CORTO_USUARIO"
                .Splits(0).DisplayColumns(LInt_Col).Style.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Visible = True
                .Splits(0).DisplayColumns(LInt_Col).AllowSizing = False

                LInt_Col = LInt_Col + 1
                .Columns(LInt_Col).Caption = "Cod. Ext."
                .Splits(0).DisplayColumns(LInt_Col).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Width = 80
                .Columns(LInt_Col).DataField = "CODIGO_EXTERNO_USUARIO"
                .Splits(0).DisplayColumns(LInt_Col).Style.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Visible = False
                .Splits(0).DisplayColumns(LInt_Col).AllowSizing = False

                LInt_Col = LInt_Col + 1
                .Columns(LInt_Col).Caption = "Cargo"
                .Splits(0).DisplayColumns(LInt_Col).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Width = 150
                .Columns(LInt_Col).DataField = "CARGO_USUARIO"
                .Splits(0).DisplayColumns(LInt_Col).Style.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Visible = True
                .Splits(0).DisplayColumns(LInt_Col).AllowSizing = False

                LInt_Col = LInt_Col + 1
                .Columns(LInt_Col).Caption = "Tel�fono"
                .Splits(0).DisplayColumns(LInt_Col).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Width = 50
                .Columns(LInt_Col).DataField = "TELEFONO_USUARIO"
                .Splits(0).DisplayColumns(LInt_Col).Style.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Visible = True
                .Splits(0).DisplayColumns(LInt_Col).AllowSizing = False

                LInt_Col = LInt_Col + 1
                .Columns(LInt_Col).Caption = "Login"
                .Splits(0).DisplayColumns(LInt_Col).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Width = 70
                .Columns(LInt_Col).DataField = "LOGIN_USUARIO"
                .Splits(0).DisplayColumns(LInt_Col).Style.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Visible = False
                .Splits(0).DisplayColumns(LInt_Col).AllowSizing = False

                LInt_Col = LInt_Col + 1
                .Columns(LInt_Col).Caption = "CLAVE_USUARIO"
                .Splits(0).DisplayColumns(LInt_Col).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Width = 200
                .Columns(LInt_Col).DataField = "CLAVE_USUARIO"
                .Splits(0).DisplayColumns(LInt_Col).Style.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Visible = False
                .Splits(0).DisplayColumns(LInt_Col).AllowSizing = False

                LInt_Col = LInt_Col + 1
                .Columns(LInt_Col).Caption = "Estado"
                .Splits(0).DisplayColumns(LInt_Col).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Width = 40
                .Columns(LInt_Col).DataField = "ESTADO_USUARIO"
                .Splits(0).DisplayColumns(LInt_Col).Style.HorizontalAlignment = AlignHorzEnum.Center
                .Splits(0).DisplayColumns(LInt_Col).Visible = True
                .Splits(0).DisplayColumns(LInt_Col).AllowSizing = False

                LInt_Col = LInt_Col + 1
                .Columns(LInt_Col).Caption = "Fecha Vigencia"
                .Splits(0).DisplayColumns(LInt_Col).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Width = 60
                .Columns(LInt_Col).DataField = "FECHA_VIGENCIA"
                .Splits(0).DisplayColumns(LInt_Col).Style.HorizontalAlignment = AlignHorzEnum.Center
                .Splits(0).DisplayColumns(LInt_Col).Visible = True
                .Splits(0).DisplayColumns(LInt_Col).AllowSizing = False

                LInt_Col = LInt_Col + 1
                .Columns(LInt_Col).Caption = "Todas Cuentas"
                .Splits(0).DisplayColumns(LInt_Col).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Width = 50
                .Columns(LInt_Col).DataField = "TODAS_CUENTAS"
                .Splits(0).DisplayColumns(LInt_Col).Style.HorizontalAlignment = AlignHorzEnum.Center
                .Splits(0).DisplayColumns(LInt_Col).Visible = True
                .Splits(0).DisplayColumns(LInt_Col).AllowSizing = False

                'Dim ValItem As C1.Win.C1TrueDBGrid.ValueItemCollection
                'ValItem = .Columns("TODAS_CUENTAS").ValueItems.Values
                'ValItem.Add(New C1.Win.C1TrueDBGrid.ValueItem("1", "S"))
                'ValItem.Add(New C1.Win.C1TrueDBGrid.ValueItem("0", "N"))
                '.Columns("TODAS_CUENTAS").ValueItems.Translate = True

                LInt_Col = LInt_Col + 1
                .Columns(LInt_Col).Caption = "Tipo"
                .Splits(0).DisplayColumns(LInt_Col).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Width = 80
                .Columns(LInt_Col).DataField = "NOMBRE_TIPO_USUARIO"
                .Splits(0).DisplayColumns(LInt_Col).Style.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Visible = True
                .Splits(0).DisplayColumns(LInt_Col).AllowSizing = False

                LInt_Col = LInt_Col + 1
                .Columns(LInt_Col).Caption = "NOMBRE_CORTO_TIPO_USUARIO"
                .Splits(0).DisplayColumns(LInt_Col).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Width = 10
                .Columns(LInt_Col).DataField = "NOMBRE_CORTO_TIPO_USUARIO"
                .Splits(0).DisplayColumns(LInt_Col).Style.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Visible = False
                .Splits(0).DisplayColumns(LInt_Col).AllowSizing = False

                LInt_Col = LInt_Col + 1
                .Columns(LInt_Col).Caption = "CODIGO_EXTERNO_TIPO_USUARIO"
                .Splits(0).DisplayColumns(LInt_Col).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Width = 10
                .Columns(LInt_Col).DataField = "CODIGO_EXTERNO_TIPO_USUARIO"
                .Splits(0).DisplayColumns(LInt_Col).Style.HorizontalAlignment = AlignHorzEnum.Center
                .Splits(0).DisplayColumns(LInt_Col).Visible = False
                .Splits(0).DisplayColumns(LInt_Col).AllowSizing = False

                LInt_Col = LInt_Col + 1
                .Columns(LInt_Col).Caption = "ESTADO_TIPO_USUARIO"
                .Splits(0).DisplayColumns(LInt_Col).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Width = 10
                .Columns(LInt_Col).DataField = "ESTADO_TIPO_USUARIO"
                .Splits(0).DisplayColumns(LInt_Col).Style.HorizontalAlignment = AlignHorzEnum.Center
                .Splits(0).DisplayColumns(LInt_Col).Visible = False
                .Splits(0).DisplayColumns(LInt_Col).AllowSizing = False

                LInt_Col = LInt_Col + 1
                .Columns(LInt_Col).Caption = "Unidad"
                .Splits(0).DisplayColumns(LInt_Col).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Width = 80
                .Columns(LInt_Col).DataField = "NOMBRE_UNIDAD"
                .Splits(0).DisplayColumns(LInt_Col).Style.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Visible = True
                .Splits(0).DisplayColumns(LInt_Col).AllowSizing = False

                LInt_Col = LInt_Col + 1
                .Columns(LInt_Col).Caption = "NOMBRE_CORTO_UNIDAD"
                .Splits(0).DisplayColumns(LInt_Col).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Width = 10
                .Columns(LInt_Col).DataField = "NOMBRE_CORTO_UNIDAD"
                .Splits(0).DisplayColumns(LInt_Col).Style.HorizontalAlignment = AlignHorzEnum.Center
                .Splits(0).DisplayColumns(LInt_Col).Visible = False
                .Splits(0).DisplayColumns(LInt_Col).AllowSizing = False

                LInt_Col = LInt_Col + 1
                .Columns(LInt_Col).Caption = "CODIGO_EXTERNO_UNIDAD"
                .Splits(0).DisplayColumns(LInt_Col).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Width = 10
                .Columns(LInt_Col).DataField = "CODIGO_EXTERNO_UNIDAD"
                .Splits(0).DisplayColumns(LInt_Col).Style.HorizontalAlignment = AlignHorzEnum.Center
                .Splits(0).DisplayColumns(LInt_Col).Visible = False
                .Splits(0).DisplayColumns(LInt_Col).AllowSizing = False

                LInt_Col = LInt_Col + 1
                .Columns(LInt_Col).Caption = "ESTADO_UNIDAD"
                .Splits(0).DisplayColumns(LInt_Col).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Width = 10
                .Columns(LInt_Col).DataField = "ESTADO_UNIDAD"
                .Splits(0).DisplayColumns(LInt_Col).Style.HorizontalAlignment = AlignHorzEnum.Center
                .Splits(0).DisplayColumns(LInt_Col).Visible = False
                .Splits(0).DisplayColumns(LInt_Col).AllowSizing = False


                'LInt_Col = LInt_Col + 1
                '.Columns(LInt_Col).Caption = "ID_ASESOR"
                '.Splits(0).DisplayColumns(LInt_Col).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Near
                '.Splits(0).DisplayColumns(LInt_Col).Width = 10
                '.Columns(LInt_Col).DataField = "ID_ASESOR"
                '.Splits(0).DisplayColumns(LInt_Col).Style.HorizontalAlignment = AlignHorzEnum.Center
                '.Splits(0).DisplayColumns(LInt_Col).Visible = False
                '.Splits(0).DisplayColumns(LInt_Col).AllowSizing = False


                'LInt_Col = LInt_Col + 1
                '.Columns(LInt_Col).Caption = "RUT_ASESOR"
                '.Splits(0).DisplayColumns(LInt_Col).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Near
                '.Splits(0).DisplayColumns(LInt_Col).Width = 10
                '.Columns(LInt_Col).DataField = "RUT_ASESOR"
                '.Splits(0).DisplayColumns(LInt_Col).Style.HorizontalAlignment = AlignHorzEnum.Center
                '.Splits(0).DisplayColumns(LInt_Col).Visible = False
                '.Splits(0).DisplayColumns(LInt_Col).AllowSizing = False

                'LInt_Col = LInt_Col + 1
                '.Columns(LInt_Col).Caption = "NOMBRE_ASESOR"
                '.Splits(0).DisplayColumns(LInt_Col).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Near
                '.Splits(0).DisplayColumns(LInt_Col).Width = 120
                '.Columns(LInt_Col).DataField = "NOMBRE_ASESOR"
                '.Splits(0).DisplayColumns(LInt_Col).Style.HorizontalAlignment = AlignHorzEnum.Center
                '.Splits(0).DisplayColumns(LInt_Col).Visible = True
                '.Splits(0).DisplayColumns(LInt_Col).AllowSizing = False

                LInt_Col = LInt_Col + 1
                .Columns(LInt_Col).Caption = "Id Tipo Control"
                .Splits(0).DisplayColumns(LInt_Col).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Width = 50
                .Columns(LInt_Col).DataField = "ID_TIPO_CONTROL"
                .Splits(0).DisplayColumns(LInt_Col).Style.HorizontalAlignment = AlignHorzEnum.Center
                .Splits(0).DisplayColumns(LInt_Col).Visible = False
                .Splits(0).DisplayColumns(LInt_Col).AllowSizing = False

                LInt_Col = LInt_Col + 1
                .Columns(LInt_Col).Caption = "Tipo Control"
                .Splits(0).DisplayColumns(LInt_Col).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Width = 80
                .Columns(LInt_Col).DataField = "NOMBRE_TIPO_CONTROL"
                .Splits(0).DisplayColumns(LInt_Col).Style.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Visible = True
                .Splits(0).DisplayColumns(LInt_Col).AllowSizing = False

            End With
        Else
            'TDG_Grilla.Enabled = False
            'HabilitaControles(False)
        End If

    End Sub
    Private Sub MuestraDatosDesdeGrilla()
        Dim lintUsuario As Integer
        Ctrl_Botonera_Guardar.Enabled = False
        Ctrl_Botonera_Cancelar.Enabled = Ctrl_Botonera_Guardar.Enabled
        Ctrl_Botonera_Agregar.Enabled = True
        Cmb_NegocioPerfiles.Text = ""
        lintUsuario = CInt("0" & Tdg_Usuarios.Columns("ID_USUARIO").Text.Trim)
        Txt_NombreUsuario.Text = Tdg_Usuarios.Columns("NOMBRE_USUARIO").Text.Trim
        Txt_NombreCorto.Text = Tdg_Usuarios.Columns("NOMBRE_CORTO_USUARIO").Text.Trim
        Txt_Login.Text = Tdg_Usuarios.Columns("LOGIN_USUARIO").Text.Trim
        Txt_Cargo.Text = Tdg_Usuarios.Columns("CARGO_USUARIO").Text.Trim
        Txt_CodigoExterno.Text = Tdg_Usuarios.Columns("CODIGO_EXTERNO_USUARIO").Text.Trim
        Txt_Telefono.Text = Tdg_Usuarios.Columns("TELEFONO_USUARIO").Text.Trim
        txtEmailUsuario.Text = Tdg_Usuarios.Columns("EMAIL_USUARIO").Text.Trim

        Cmb_Estado.SelectedIndex = gFunSeteaItemComboBox(Cmb_Estado, Tdg_Usuarios.Columns("ESTADO_USUARIO").Text.Trim, 0)
        Cmb_TipoUsuario.SelectedIndex = gFunSeteaItemComboBox(Cmb_TipoUsuario, Tdg_Usuarios.Columns("NOMBRE_TIPO_USUARIO").Text.Trim, 0)
        Cmb_TipoControl.SelectedIndex = gFunSeteaItemComboBox(Cmb_TipoControl, Tdg_Usuarios.Columns("NOMBRE_TIPO_CONTROL").Text.Trim, 0)
        Cmb_Unidad.SelectedIndex = gFunSeteaItemComboBox(Cmb_Unidad, Tdg_Usuarios.Columns("ID_UNIDAD").Text.Trim, 1)
        Ded_FechaLiquidacion.Value = Tdg_Usuarios.Columns("FECHA_VIGENCIA").Text.Trim
        Chk_TodasCuentas.Checked = IIf(Tdg_Usuarios.Columns("TODAS_CUENTAS").Text.Trim = "S", True, False)
        'CargaAsesores(lintUsuario)
        'If Tdg_Usuarios.Columns("ID_ASESOR").Text <> "" Then
        '    Chk_EsAsesor.Checked = True
        '    Cmb_AsesoresDisponibles.SelectedIndex = gFunSeteaItemComboBox(Cmb_AsesoresDisponibles, Tdg_Usuarios.Columns("ID_ASESOR").Text, 2)
        'Else
        '    Cmb_AsesoresDisponibles.Text = ""
        '    Chk_EsAsesor.Checked = False
        'End If
        'gSubHabilitaControl(Cmb_AsesoresDisponibles, True, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
        Pnl_PerfilesUsuarios.Enabled = False
        Pnl_CuentasUsuario.Enabled = False
        If (DS_Usuarios.Tables(0).Rows.Count > 0) Then
            If Tdg_Usuarios.Columns("ESTADO_USUARIO").Text.Trim <> "ELI" Then
                Ctrl_Botonera_Editar.Enabled = True
                Ctrl_Botonera_Eliminar.Enabled = True
            Else
                Ctrl_Botonera_Editar.Enabled = False
                Ctrl_Botonera_Eliminar.Enabled = False
            End If
        End If

        CargaNegocios(lintUsuario)
        lintIdNegocio = Cmb_NegocioPerfiles.Columns(1).CellValue(Cmb_NegocioPerfiles.SelectedIndex).ToString()
        CargaPerfiles(lintUsuario)
        If Tab_Contenedor.SelectedIndex = 2 Then
            If TienePerfilAsociado() Then
                MostrarTabCuentas()
            Else
                Tab_Contenedor.SelectedIndex = 1
            End If

        End If
    End Sub



    Private Sub Tdg_Usuarios_FilterChange(ByVal sender As Object, ByVal e As System.EventArgs) Handles Tdg_Usuarios.FilterChange
        Dim sb As New System.Text.StringBuilder
        Dim dc As C1.Win.C1TrueDBGrid.C1DataColumn
        Try
            sender.SelectedRows.Clear()
            For Each dc In sender.Columns
                If dc.FilterText.Trim.Length > 0 Then
                    If sb.Length > 0 Then
                        sb.Append(" AND ")
                    End If
                    sb.Append(gstrFiltro_X_TipoColumna(dc))
                End If
            Next dc
            '... Realiza el filtro sobre el dataset
            sender.DataSource.DefaultView.RowFilter = sb.ToString
            sender.Refresh()

            If sender.RowCount > 0 Then
                sender.SelectedRows.Add(0)
            End If
            Tdg_Usuarios.Bookmark = 0
            Tdg_Usuarios_Click(Tdg_Usuarios, Nothing)

        Catch ex As Exception
            Debug.Print(ex.Message)
        End Try
    End Sub
    Private Sub Tdg_Usuarios_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Tdg_Usuarios.RowColChange
        Ctrl_Botonera_Guardar.Enabled = False
        Ctrl_Botonera_Cancelar.Enabled = Ctrl_Botonera_Guardar.Enabled
        ltxtOperacion = ""
        HabilitaDeshabilitaDatos()
        MuestraDatosDesdeGrilla()

    End Sub
    Private Sub Tdg_Usuarios_AfterFilter(ByVal sender As Object, ByVal e As C1.Win.C1TrueDBGrid.FilterEventArgs) Handles Tdg_Usuarios.AfterFilter
        If (DS_Usuarios.Tables("USUARIO").Rows.Count >= 0) Then
            MuestraDatosDesdeGrilla()
        End If
    End Sub
    Private Sub Tdg_Usuarios_AfterSort(ByVal sender As Object, ByVal e As C1.Win.C1TrueDBGrid.FilterEventArgs) Handles Tdg_Usuarios.AfterSort
        If (DS_Usuarios.Tables("USUARIO").Rows.Count >= 0) Then
            MuestraDatosDesdeGrilla()
        End If
    End Sub

    'Private Sub Chk_EsAsesor_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    If Chk_EsAsesor.Checked Then
    '        gSubHabilitaControl(Cmb_AsesoresDisponibles, False, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
    '    Else
    '        'Cmb_AsesoresDisponibles.Text = ""
    '        Cmb_AsesoresDisponibles.SelectedIndex = -1
    '        gSubHabilitaControl(Cmb_AsesoresDisponibles, True, gColorBackGroundEnabled, gColorBackGroundDesabled, True)
    '    End If
    'End Sub
    Private Sub Chk_TodasCuentas_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Chk_TodasCuentas.CheckedChanged
        If Chk_TodasCuentas.Checked Then
            Tab_CuentasUsuario.TabVisible = False

        Else
            Tab_CuentasUsuario.TabVisible = True
        End If

    End Sub

    Private Function ValidarCamposOK() As Boolean
        Try
            Dim lbnlSeteaFoco As Boolean = True
            Dim lstrMensajeResumen As String = ""

            gFunValidaValorControl("Falta ingresar Nombre del Usuario.", Txt_NombreUsuario, lstrMensajeResumen, lbnlSeteaFoco, True, , , False)
            gFunValidaValorControl("Falta ingresar Nombre Corto del Usuario.", Txt_NombreCorto, lstrMensajeResumen, lbnlSeteaFoco, True, , , False)
            'gFunValidaValorControl("Falta Seleccionar la Unidad del Usuario.", Cmb_Unidad, lstrMensajeResumen, lbnlSeteaFoco, True, , , False)
            gFunValidaValorControl("Falta ingresar el Login del Usuario.", Txt_Login, lstrMensajeResumen, lbnlSeteaFoco, True, , , False)
            'gFunValidaValorControl("Falta Seleccionar el Tipo de Usuario.", Cmb_TipoUsuario, lstrMensajeResumen, lbnlSeteaFoco, True, , , False)
            gFunValidaValorControl("Falta ingresar el Tel�fono del Usuario.", Txt_Telefono, lstrMensajeResumen, lbnlSeteaFoco, True, , , False)
            gFunValidaValorControl("Falta ingresar el Cargo del Usuario.", Txt_Cargo, lstrMensajeResumen, lbnlSeteaFoco, True, , , False)
            'gFunValidaValorControl("Falta ingresar el C�digo Externo del Usuario.", Txt_CodigoExterno, lstrMensajeResumen, lbnlSeteaFoco, True, , , False)
            gFunValidaValorControl("Falta Seleccionar la Fecha de Vigencia del Usuario.", Ded_FechaLiquidacion, lstrMensajeResumen, lbnlSeteaFoco, True, , , False)
            gFunValidaValorControl("Falta ingresar Email del Usuario.", txtEmailUsuario, lstrMensajeResumen, lbnlSeteaFoco, True, , , False)
            If lstrMensajeResumen.Trim <> "" Then
                MsgBox(lstrMensajeResumen, MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation, gtxtNombreSistema & Me.Text)
                ValidarCamposOK = False
            Else
                ValidarCamposOK = True
            End If
        Catch ex As Exception
            ValidarCamposOK = False
        End Try
    End Function
    'Private Sub ModificarAsesor()
    '    Dim lintIdUsuario As String
    '    Dim lstrIdAsesorUsuario As String
    '    Dim lstrIdAsesorSeleccionado As String
    '    Dim lintFila As Integer
    '    ltxtResultadoTransaccion = "OK"
    '    lintIdUsuario = Tdg_Usuarios.Columns("ID_USUARIO").Text.Trim

    '    lstrIdAsesorUsuario = Tdg_Usuarios.Columns("ID_ASESOR").Text
    '    lintFila = Cmb_AsesoresDisponibles.SelectedIndex
    '    If (Not Chk_EsAsesor.Checked) Then
    '        lstrIdAsesorSeleccionado = ""
    '    Else
    '        lstrIdAsesorSeleccionado = Cmb_AsesoresDisponibles.Columns(2).CellValue(lintFila)
    '    End If


    '    If lstrIdAsesorSeleccionado = lstrIdAsesorUsuario Then
    '        'No se cambi� el asesor
    '        Exit Sub
    '    Else
    '        Dim lintAsesorUsuario As Integer
    '        Dim lintAsesorNuevo As Integer
    '        If lstrIdAsesorUsuario = "" Then lintAsesorUsuario = 0 Else lintAsesorUsuario = CInt(lstrIdAsesorUsuario)
    '        If lstrIdAsesorSeleccionado = "" Then lintAsesorNuevo = 0 Else lintAsesorNuevo = CInt(lstrIdAsesorSeleccionado)

    '        ltxtResultadoTransaccion = lobjAsesor.RelacionarUsuarioAsesor(lintAsesorUsuario, lintIdUsuario, lintAsesorNuevo)
    '    End If

    'End Sub

#Region " --- Botones ---"
    Private Sub Ctrl_Botonera_Guardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Ctrl_Botonera_Guardar.Click
        ltxtResultadoTransaccion = "OK"
        If ltxtOperacion = "ELIMINAR" Then
            If Tdg_Usuarios.Columns("ESTADO_USUARIO").Text.Trim = "ELI" Then
                MsgBox("El Registro ya se encuentra ELIMINADO.", MsgBoxStyle.Information, gtxtNombreSistema & Me.Text)
            ElseIf Tdg_Usuarios.Columns("ESTADO_USUARIO").Text.Trim = "SIS" Then
                MsgBox("NO puede eliminar este usuario.", MsgBoxStyle.Information, gtxtNombreSistema & Me.Text)
            Else
                '...Mensaje de confirmacion 
                If (MsgBox(vbCr & " � Confirma la Eliminaci�n del Usuario ( " & Txt_NombreUsuario.Text.Trim & " ). ?" & vbCr & vbCr, MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2 + MsgBoxStyle.Exclamation, gtxtNombreSistema & Me.Text)) = MsgBoxResult.No Then
                    Exit Sub
                End If
            End If
        Else
            If Not ValidarCamposOK() Then Exit Sub
        End If

        Dim resultadoEmail = "OK"
        Select Case ltxtOperacion
            Case Is = "INSERTAR"
                resultadoEmail = Nuevo_Usuario()
            Case Is = "MODIFICAR"
                Modifica_Usuario()
            Case Is = "ELIMINAR"
                Elimina_Usuario()
        End Select

        If ltxtResultadoTransaccion.Trim = "OK" Then
            MsgBox("Operaci�n se realiz� con �xito" &
                   IIf(Not resultadoEmail.Equals("OK"), ", pero el email no pudo ser enviado, debido al siguiente problema: " & vbCrLf & resultadoEmail, "."),
                   MsgBoxStyle.Information,
                   gtxtNombreSistema & Me.Text)
            InicializaFormulario()
            CargaGrilla()
        Else
            MsgBox(ltxtResultadoTransaccion, MsgBoxStyle.Exclamation, gtxtNombreSistema & Me.Text)
            'Ctrl_Botonera_Guardar.Focus(
        End If

    End Sub
    Private Sub Ctrl_Botonera_Agregar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Ctrl_Botonera_Agregar.Click
        ltxtOperacion = "INSERTAR"
        Tab_Contenedor.SelectedIndex = 0
        HabilitaDeshabilitaDatos()
        CargaPerfiles(0)
        CargaNegocios(0)
        'CargaAsesores(0)
        Limpiar_Campos()
        Cmb_Estado.SelectedIndex = gFunSeteaItemComboBox(Cmb_Estado, "PWD", 0)
        Chk_TodasCuentas.Enabled = True
        Ctrl_Botonera_Guardar.Enabled = True
        Ctrl_Botonera_Cancelar.Enabled = Ctrl_Botonera_Guardar.Enabled
        Ctrl_Botonera_Eliminar.Enabled = False
        Ctrl_Botonera_Editar.Enabled = False
        Pnl_PerfilesUsuarios.Enabled = True
        Pnl_CuentasUsuario.Enabled = True
        Ctrl_Botonera_Guardar.Text = "&Grabar"
        Txt_NombreUsuario.Focus()
    End Sub
    Private Sub Ctrl_Botonera_Editar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Ctrl_Botonera_Editar.Click
        ltxtOperacion = "MODIFICAR"
        HabilitaDeshabilitaDatos()
        Ctrl_Botonera_Guardar.Enabled = True
        Ctrl_Botonera_Cancelar.Enabled = Ctrl_Botonera_Guardar.Enabled
        Ctrl_Botonera_Eliminar.Enabled = False
        Ctrl_Botonera_Agregar.Enabled = False
        Ctrl_Botonera_Editar.Enabled = False
        Pnl_PerfilesUsuarios.Enabled = True
        Pnl_CuentasUsuario.Enabled = True
        Ctrl_Botonera_Guardar.Text = "&Grabar"
        Txt_NombreUsuario.Focus()
    End Sub
    Private Sub Ctrl_Botonera_Eliminar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Ctrl_Botonera_Eliminar.Click
        If Tdg_Usuarios.Columns("ESTADO_USUARIO").CellValue(Tdg_Usuarios.Bookmark).ToString.Trim = "SIS" Then   ' Estado NO BOrrar 
            MsgBox("No se puede eliminar este usuario", MsgBoxStyle.Critical, gtxtNombreSistema & Me.Text)
        Else
            ltxtOperacion = "ELIMINAR"
            'HabilitaDeshabilitaDatos()
            'Ctrl_Botonera_Guardar.Enabled = True
            'Ctrl_Botonera_Cancelar.Enabled = Ctrl_Botonera_Guardar.Enabled
            'Ctrl_Botonera_Agregar.Enabled = False
            'Ctrl_Botonera_Editar.Enabled = False
            'Ctrl_Botonera_Guardar.Text = "&Eliminar"
            'Ctrl_Botonera_Guardar.Focus()

            HabilitaDeshabilitaDatos()
            ltxtResultadoTransaccion = "OK"

            If Tdg_Usuarios.Columns("ESTADO_USUARIO").Text.Trim = "ELI" Then
                MsgBox("El Registro ya se encuentra ELIMINADO.", MsgBoxStyle.Information, gtxtNombreSistema & Me.Text)
            ElseIf Tdg_Usuarios.Columns("ESTADO_USUARIO").Text.Trim = "SIS" Then
                MsgBox("NO PUEDE eliminar este usuario.", MsgBoxStyle.Information, gtxtNombreSistema & Me.Text)
            Else
                If (MsgBox(vbCr & " � Confirma la Eliminaci�n del Usuario ( " & Txt_NombreUsuario.Text.Trim & " ). ?" & vbCr & vbCr, MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2 + MsgBoxStyle.Exclamation, gtxtNombreSistema & Me.Text)) = MsgBoxResult.No Then
                    Exit Sub
                End If
            End If
            Elimina_Usuario()
            If ltxtResultadoTransaccion.Trim = "OK" Then
                MsgBox("Operaci�n se realiz� con �xito.", MsgBoxStyle.Information, gtxtNombreSistema & Me.Text)
                InicializaFormulario()
                CargaGrilla()
            Else
                MsgBox(ltxtResultadoTransaccion, MsgBoxStyle.Exclamation, gtxtNombreSistema & Me.Text)
                'Btn_Accion.Focus()
            End If

        End If
    End Sub
    Private Sub Ctrl_Botonera_Cancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Ctrl_Botonera_Cancelar.Click
        Ctrl_Botonera_Guardar.Enabled = False
        Ctrl_Botonera_Cancelar.Enabled = Ctrl_Botonera_Guardar.Enabled
        ltxtOperacion = ""
        HabilitaDeshabilitaDatos()
        MuestraDatosDesdeGrilla()
    End Sub

    Private Sub Btn_ReiniciaPassword_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_ReiniciaPassword.Click
        If MsgBox("� Est� seguro de resetear la contrase�a de este Usuario ?", MsgBoxStyle.YesNo, gtxtNombreSistema & Me.Text) = MsgBoxResult.Yes Then
            Cursor = Cursors.WaitCursor
            ResetPassWord()
            Cursor = Cursors.Default
        End If
    End Sub

    Private Function Nuevo_Usuario() As String
        Dim lintIdUsuario As Integer = 0
        Dim lintIdTipoUsuario As Integer = 0
        Dim lintIdTipoControl As Integer = 0
        Dim lintIdUnidad As Long = 0

        Dim intFila As Integer

        intFila = Cmb_TipoUsuario.SelectedIndex
        If Cmb_TipoUsuario.SelectedIndex <> -1 Then
            lintIdTipoUsuario = Cmb_TipoUsuario.Columns("ID_TIPO_USUARIO").CellValue(intFila).ToString()
        End If

        intFila = Cmb_TipoControl.SelectedIndex
        If Cmb_TipoControl.SelectedIndex <> -1 Then
            lintIdTipoControl = Cmb_TipoControl.Columns("ID_TIPO_CONTROL").CellValue(intFila).ToString()
        End If

        intFila = Cmb_Unidad.SelectedIndex
        If Cmb_Unidad.SelectedIndex <> -1 Then
            lintIdUnidad = Cmb_Unidad.Columns("ID_UNIDAD").CellValue(intFila).ToString()
        End If

        Dim password = ClsSeguridad.GenerarPasswordRandom()
        Dim passwordEncriptada = ClsEncriptacion.Encriptar(password)
        lobjUsuario.Insertar(ltxtResultadoTransaccion,
                             lintIdUsuario,
                             lintIdTipoUsuario,
                             lintIdUnidad,
                             Txt_NombreUsuario.Text,
                             Txt_NombreCorto.Text,
                             Txt_CodigoExterno.Text,
                             Txt_Cargo.Text,
                             Txt_Telefono.Text,
                             Txt_Login.Text,
                             passwordEncriptada,
                             "PWD",
                             Ded_FechaLiquidacion.Text,
                             Chk_TodasCuentas.Checked,
                             lintIdTipoControl,
                             txtEmailUsuario.Text)

        Dim resultadoEmail = String.Empty
        If ltxtResultadoTransaccion = "OK" Then
            'If Chk_EsAsesor.Checked Then
            '    ModificarAsesor()
            'End If
            AgregarPerfilesUsuarios(lintIdUsuario)
            ModificarCuentasUsuario(lintIdUsuario)
            resultadoEmail = ClsSeguridad.EnviarMailPassword(
                Txt_NombreUsuario.Text,
                Txt_Login.Text,
                txtEmailUsuario.Text,
                password)
        End If
        Return resultadoEmail
    End Function

    Private Sub Modifica_Usuario()
        Dim lintIdUsuario As Integer = 0
        Dim lintFila As Integer = 0
        Dim lintIdTipoUsuario As Integer = 0
        Dim lintIdTipoControl As Integer = 0
        Dim lintIdUnidad As Integer = 0
        Dim lstrEstado As String = ""


        'Se define el valor de los campos
        lintFila = Tdg_Usuarios.Bookmark
        lintIdUsuario = Tdg_Usuarios.Columns("ID_USUARIO").Text.Trim

        lintFila = Cmb_TipoUsuario.SelectedIndex
        If Cmb_TipoUsuario.SelectedIndex <> -1 Then
            lintIdTipoUsuario = Cmb_TipoUsuario.Columns("ID_TIPO_USUARIO").CellValue(lintFila).ToString()
        End If

        lintFila = Cmb_TipoControl.SelectedIndex
        If Cmb_TipoControl.SelectedIndex <> -1 Then
            lintIdTipoControl = Cmb_TipoControl.Columns("ID_TIPO_CONTROL").CellValue(lintFila).ToString()
        End If

        lintFila = Cmb_Unidad.SelectedIndex
        If Cmb_Unidad.SelectedIndex <> -1 Then
            lintIdUnidad = Cmb_Unidad.Columns("ID_UNIDAD").CellValue(lintFila).ToString()
        End If

        lintFila = Cmb_Estado.SelectedIndex
        lstrEstado = Cmb_Estado.Columns(0).CellValue(lintFila).ToString()

        'Invoco el metodo Nuevo
        lobjUsuario.Modificar(ltxtResultadoTransaccion,
                              lintIdUsuario,
                              lintIdTipoUsuario,
                              lintIdUnidad,
                              Txt_NombreUsuario.Text,
                              Txt_NombreCorto.Text,
                              Txt_CodigoExterno.Text,
                              Txt_Cargo.Text,
                              Txt_Telefono.Text,
                              Txt_Login.Text,
                              String.Empty,
                              lstrEstado,
                              Ded_FechaLiquidacion.Text,
                              Chk_TodasCuentas.Checked,
                              lintIdTipoControl,
                              txtEmailUsuario.Text)

        If ltxtResultadoTransaccion = "OK" Then
            ModificarPerfilesUsuario(lintIdUsuario)
            If Chk_TodasCuentas.Checked = True Then
                ''''''''
                Elimina_Cuentas_Asociadas()
            Else
                ModificarCuentasUsuario(lintIdUsuario)
            End If
        End If


    End Sub

    Private Sub Elimina_Cuentas_Asociadas()
        Try
            Dim lintIdUsuario As Integer = 0
            Dim lstrEliminaCtas As String = ""

            lintIdUsuario = Tdg_Usuarios.Columns("ID_USUARIO").Text.Trim
            If DS_CuentasAsociadas.Tables.Count > 0 Then
                DS_CuentasAsociadas.Tables(0).Rows.Clear()
                lstrEliminaCtas = lobjCuentasSeguridad.ProcesarCuentasUsuario(lintIdUsuario, DS_CuentasAsociadas)
                If lstrEliminaCtas <> "OK" Then
                    MsgBox("Problemas al eliminar las cuentas asociadas, debido a: " & lstrEliminaCtas, MsgBoxStyle.Information, gtxtNombreSistema & Me.Text)
                End If
            End If
        Catch ex As Exception
            MsgBox("Problemas al eliminar las cuentas asociadas, debido a: " & ex.Message, MsgBoxStyle.Information, gtxtNombreSistema & Me.Text)
        End Try
    End Sub

    Private Sub Elimina_Perfiles_Asociados()
        Try
            Dim lintIdUsuario As Integer = 0
            Dim lstrEliminaPerf As String = ""

            lintIdUsuario = Tdg_Usuarios.Columns("ID_USUARIO").Text.Trim
            If DS_PerfilesUsuariosAsignados.Tables(0).Rows.Count > 0 Then
                lstrEliminaPerf = lobjPerfilesUsuarios.EliminarRelacionPerfilUsuario(lintIdUsuario, DS_PerfilesUsuariosAsignados)
                If lstrEliminaPerf <> "OK" Then
                    MsgBox("Problemas al eliminar las cuentas asociadas, debido a: " & lstrEliminaPerf, MsgBoxStyle.Information, gtxtNombreSistema & Me.Text)
                End If
            End If
        Catch ex As Exception
            MsgBox("Problemas al eliminar las cuentas asociadas, debido a: " & ex.Message, MsgBoxStyle.Information, gtxtNombreSistema & Me.Text)
        End Try
    End Sub

    Private Sub Elimina_Usuario()
        Dim lintIdUsuario As Integer = 0

        Elimina_Cuentas_Asociadas()
        Elimina_Perfiles_Asociados()
        lintIdUsuario = Tdg_Usuarios.Columns("ID_USUARIO").Text.Trim
        lobjUsuario.Eliminar(ltxtResultadoTransaccion, lintIdUsuario)
    End Sub

    Public Sub ResetPassWord()
        Dim lintIdUsuario As Integer
        Dim lstrPassword, lstrPassEncriptada As String
        If Tdg_Usuarios.RowCount <= 0 Then
            lintIdUsuario = 0
        Else
            lintIdUsuario = Tdg_Usuarios.Columns("ID_USUARIO").Text
        End If
        Dim mensajeValidacion = String.Empty
        gFunValidaValorControl("Falta ingresar Email del Usuario, favor edite el usuario luego resetee la password.", txtEmailUsuario, mensajeValidacion, False, True, , , False)
        If mensajeValidacion.Trim <> "" Then
            MsgBox(mensajeValidacion, MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation, gtxtNombreSistema & Me.Text)
            Exit Sub
        End If

        lstrPassword = ClsSeguridad.GenerarPasswordRandom()
        lstrPassEncriptada = ClsEncriptacion.Encriptar(lstrPassword)

        lobjUsuario.ResetearPassword(ltxtResultadoTransaccion, lintIdUsuario, lstrPassEncriptada, "PWD")
        Dim resultadoEmail = "OK"
        With Tdg_Usuarios
            resultadoEmail = ClsSeguridad.EnviarMailPassword(
                .Columns("NOMBRE_USUARIO").Text,
                .Columns("LOGIN_USUARIO").Text,
                .Columns("EMAIL_USUARIO").Text,
                lstrPassword)
        End With

        If ltxtResultadoTransaccion = "OK" Then
            MsgBox("La Contrase�a ha sido cambiada correctamente" & 
                   IIf(Not resultadoEmail.Equals("OK"), ", pero el email no pudo ser enviado, debido al siguiente problema: " & vbCrLf & resultadoEmail, "."),
                   MsgBoxStyle.Information,
                   gtxtNombreSistema & Me.Text)
            InicializaFormulario()
            CargaGrilla()
        End If

    End Sub
#End Region

#End Region

#Region " --- TAB Perfiles del Usuario ---"

    Private Sub CargaPerfiles(ByVal intIdUsuario As Integer)
        DS_PerfilesUsuariosAsignados = Nothing
        Tdg_PerfilesAsignados.DataSource = Nothing
        DS_PerfilesUsuariosDisponibles = Nothing
        Tdg_PerfilesDisponibles.DataSource = Nothing

        ltxtResultadoTransaccion = ""
        DS_PerfilesUsuariosAsignados = lobjPerfilesUsuarios.BuscarAsignados(ltxtResultadoTransaccion, intIdUsuario)
        DT_PerfilesAsignados_Original = DS_PerfilesUsuariosAsignados.Tables("PERFILES_USUARIOS").Clone

        For Each DR_FilaPerfil As DataRow In DS_PerfilesUsuariosAsignados.Tables("PERFILES_USUARIOS").Rows
            DT_PerfilesAsignados_Original.ImportRow(DR_FilaPerfil)
        Next

        Tdg_PerfilesAsignados.DataSource = DS_PerfilesUsuariosAsignados.Tables("PERFILES_USUARIOS")
        FormateaGrillaPerfilesAsignados()

        ltxtResultadoTransaccion = ""
        DS_PerfilesUsuariosDisponibles = lobjPerfilesUsuarios.BuscarDisponibles(ltxtResultadoTransaccion, intIdUsuario)
        DT_PerfilesDisponibles_Original = DS_PerfilesUsuariosDisponibles.Tables("PERFILES_USUARIOS").Clone

        For Each DR_FilaPerfil As DataRow In DS_PerfilesUsuariosDisponibles.Tables("PERFILES_USUARIOS").Rows
            DT_PerfilesDisponibles_Original.ImportRow(DR_FilaPerfil)
        Next

        Tdg_PerfilesDisponibles.DataSource = DS_PerfilesUsuariosDisponibles.Tables("PERFILES_USUARIOS")

        FormateaGrillaPerfilesDisponibles()
        Tdg_PerfilesDisponibles.Columns("ABR_NEGOCIO").FilterText = Cmb_NegocioPerfiles.Text
        Tdg_PerfilesDisponibles.Refresh()
    End Sub

    Private Sub FormateaGrillaPerfilesDisponibles()
        Dim LInt_Col As Integer
        Dim Tdg_Grilla As C1.Win.C1TrueDBGrid.C1TrueDBGrid
        Tdg_Grilla = Tdg_PerfilesDisponibles
        Tdg_Grilla.Splits(0).HScrollBar.Style = ScrollBarStyleEnum.None

        If (Tdg_Grilla.RowCount >= 0) Then
            Tdg_Grilla.Splits(0).HScrollBar.Style = C1.Win.C1TrueDBGrid.ScrollBarStyleEnum.Automatic
            Tdg_Grilla.Splits(0).VScrollBar.Style = C1.Win.C1TrueDBGrid.ScrollBarStyleEnum.Always

            With Tdg_Grilla
                .FilterBar = True
                .FetchRowStyles = True
                LInt_Col = 0
                .Columns(LInt_Col).Caption = "ID_PERFIL_USUARIO"
                .Splits(0).DisplayColumns(LInt_Col).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Width = 50
                .Columns(LInt_Col).DataField = "ID_PERFIL_USUARIO"
                .Splits(0).DisplayColumns(LInt_Col).Style.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Visible = False
                .Splits(0).DisplayColumns(LInt_Col).AllowSizing = False

                LInt_Col = LInt_Col + 1
                .Columns(LInt_Col).Caption = "ID_NEGOCIO"
                .Splits(0).DisplayColumns(LInt_Col).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Width = 50
                .Columns(LInt_Col).DataField = "ID_NEGOCIO"
                .Splits(0).DisplayColumns(LInt_Col).Style.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Visible = False
                .Splits(0).DisplayColumns(LInt_Col).AllowSizing = False

                LInt_Col = LInt_Col + 1
                .Columns(LInt_Col).Caption = "Negocio"
                .Splits(0).DisplayColumns(LInt_Col).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Width = 120
                .Columns(LInt_Col).DataField = "ABR_NEGOCIO"
                .Splits(0).DisplayColumns(LInt_Col).Style.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Visible = False
                .Splits(0).DisplayColumns(LInt_Col).AllowSizing = False

                LInt_Col = LInt_Col + 1
                .Columns(LInt_Col).Caption = "Perfil"
                .Splits(0).DisplayColumns(LInt_Col).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Width = 335
                .Columns(LInt_Col).DataField = "NOMBRE_PERFIL"
                .Splits(0).DisplayColumns(LInt_Col).Style.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Visible = True
                .Splits(0).DisplayColumns(LInt_Col).AllowSizing = False

            End With
        End If


    End Sub

    Private Sub FormateaGrillaPerfilesAsignados()
        Dim LInt_Col As Integer
        Dim Tdg_Grilla As C1.Win.C1TrueDBGrid.C1TrueDBGrid
        Tdg_Grilla = Tdg_PerfilesAsignados
        Tdg_Grilla.Splits(0).HScrollBar.Style = ScrollBarStyleEnum.None

        If (Tdg_Grilla.RowCount >= 0) Then
            Tdg_Grilla.Splits(0).HScrollBar.Style = C1.Win.C1TrueDBGrid.ScrollBarStyleEnum.Automatic
            Tdg_Grilla.Splits(0).VScrollBar.Style = C1.Win.C1TrueDBGrid.ScrollBarStyleEnum.Always

            With Tdg_Grilla
                .FilterBar = True
                .FetchRowStyles = True
                .AllowUpdate = True
                LInt_Col = 0
                .Columns(LInt_Col).Caption = "ID_PERFIL_USUARIO"
                .Splits(0).DisplayColumns(LInt_Col).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Width = 50
                .Columns(LInt_Col).DataField = "ID_PERFIL_USUARIO"
                .Splits(0).DisplayColumns(LInt_Col).Style.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Visible = False
                .Splits(0).DisplayColumns(LInt_Col).AllowSizing = False
                .Splits(0).DisplayColumns(LInt_Col).Locked = True

                LInt_Col = LInt_Col + 1
                .Columns(LInt_Col).Caption = "ID_NEGOCIO"
                .Splits(0).DisplayColumns(LInt_Col).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Width = 50
                .Columns(LInt_Col).DataField = "ID_NEGOCIO"
                .Splits(0).DisplayColumns(LInt_Col).Style.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Visible = False
                .Splits(0).DisplayColumns(LInt_Col).AllowSizing = False
                .Splits(0).DisplayColumns(LInt_Col).Locked = True

                LInt_Col = LInt_Col + 1
                .Columns(LInt_Col).Caption = "Negocio"
                .Splits(0).DisplayColumns(LInt_Col).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Width = 103
                .Columns(LInt_Col).DataField = "ABR_NEGOCIO"
                .Splits(0).DisplayColumns(LInt_Col).Style.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Visible = True
                .Splits(0).DisplayColumns(LInt_Col).AllowSizing = False
                .Splits(0).DisplayColumns(LInt_Col).Locked = True

                LInt_Col = LInt_Col + 1
                .Columns(LInt_Col).Caption = "Perfil"
                .Splits(0).DisplayColumns(LInt_Col).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Width = 230
                .Columns(LInt_Col).DataField = "NOMBRE_PERFIL"
                .Splits(0).DisplayColumns(LInt_Col).Style.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Visible = True
                .Splits(0).DisplayColumns(LInt_Col).AllowSizing = False
                .Splits(0).DisplayColumns(LInt_Col).Locked = True

                .ColumnHeaders = True

            End With
        End If


    End Sub

    Private Sub CargaNegocios(ByVal intIdUsuario As Integer)
        Dim lstrColumnas As String = "ABR_NEGOCIO,ID_NEGOCIO"
        DS_Negocios = lobjNegocio.RetornaNegociosUsuarioConectado(ltxtResultadoTransaccion, glngIdUsuario, lstrColumnas)
        If ltxtResultadoTransaccion = "OK" Then
            If DS_Negocios.Tables.Count > 0 Then
                If DS_Negocios.Tables(0).Rows.Count > 0 Then
                    Cmb_NegocioPerfiles.DataSource = DS_Negocios.Tables(0)

                    Cmb_NegocioPerfiles.Columns(0).DataField = "ABR_NEGOCIO"
                    Cmb_NegocioPerfiles.Columns(1).DataField = "ID_NEGOCIO"

                    Cmb_NegocioPerfiles.Splits(0).DisplayColumns("ABR_NEGOCIO").Visible = True
                    Cmb_NegocioPerfiles.Splits(0).DisplayColumns("ID_NEGOCIO").Visible = False
                    Cmb_NegocioPerfiles.SelectedIndex = 0
                    'Cmb_NegocioPerfiles.Text = ""
                End If
            End If
        End If
    End Sub

    Private Sub Cmb_NegocioPerfiles_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Cmb_NegocioPerfiles.TextChanged
        Dim ldtrDatos As DataRow()
        Dim DS_Datos As DataSet

        If Cmb_NegocioPerfiles.Text = "" Then Exit Sub
        If Tdg_PerfilesDisponibles.DataSource Is Nothing Then Exit Sub

        If Cmb_NegocioPerfiles.ListCount > 0 Then

            lintIdNegocio = Cmb_NegocioPerfiles.Columns(1).CellValue(Cmb_NegocioPerfiles.SelectedIndex).ToString()

            DS_Datos = DS_PerfilesUsuariosDisponibles.Clone
            DS_Datos.Clear()
            ldtrDatos = DS_PerfilesUsuariosDisponibles.Tables("PERFILES_USUARIOS").Select("Id_Negocio = " & lintIdNegocio)
            For Each ldtrRegistro In ldtrDatos
                DS_Datos.Tables(0).ImportRow(ldtrRegistro)
            Next

            Tdg_PerfilesDisponibles.DataSource = DS_Datos.Tables(0)
        Else
            Tdg_PerfilesDisponibles.DataSource = DS_PerfilesUsuariosDisponibles.Tables("PERFILES_USUARIOS")
        End If

        FormateaGrillaPerfilesDisponibles()

    End Sub

    Private Sub Btn_AsociarPerfilUsuario_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_AsociarPerfilUsuario.Click
        If Tdg_PerfilesDisponibles.RowCount = 0 Then
            Exit Sub
        End If

        Dim lintIdNegocioPorAsociar As Integer
        Dim lintPerfilPorAsociar As Integer
        Dim lintIndiceFilaPerfil As Integer
        Dim lstrDscPerfil As String = ""
        Dim DR_PerfilPorAsociar As DataRow

        Dim lblnExisteAdministrador As Boolean = False
        Dim lblnExisteSeguridad As Boolean = False


        lintPerfilPorAsociar = Tdg_PerfilesDisponibles.Columns("ID_PERFIL_USUARIO").Text
        lintIdNegocioPorAsociar = lintIdNegocio 'Tdg_PerfilesDisponibles.Columns("ID_NEGOCIO").Text

        '...Valida Perfiles Seguridad y Administrador, deben ser unicos por negocio
        '...Id de Administrador debe ser = 1
        '...Id de Seguridad     debe ser = 2
        For lintIndiceFilaPerfil = 0 To DS_PerfilesUsuariosAsignados.Tables(0).Rows.Count - 1
            If DS_PerfilesUsuariosAsignados.Tables(0).Rows(lintIndiceFilaPerfil).Item("ID_PERFIL_USUARIO").ToString.Trim = "1" And _
               DS_PerfilesUsuariosAsignados.Tables(0).Rows(lintIndiceFilaPerfil).Item("ID_NEGOCIO").ToString.Trim = lintIdNegocioPorAsociar.ToString.Trim Then
                lblnExisteAdministrador = True
            End If
            If DS_PerfilesUsuariosAsignados.Tables(0).Rows(lintIndiceFilaPerfil).Item("ID_PERFIL_USUARIO").ToString.Trim = "2" And _
               DS_PerfilesUsuariosAsignados.Tables(0).Rows(lintIndiceFilaPerfil).Item("ID_NEGOCIO").ToString.Trim = lintIdNegocioPorAsociar.ToString.Trim Then
                lblnExisteSeguridad = True
            End If
        Next

        If (lblnExisteAdministrador Or lblnExisteSeguridad) And (lintPerfilPorAsociar = 1 Or lintPerfilPorAsociar = 2) Then 'InStr("12", lintPerfilPorAsociar) <> 0 And (DS_PerfilesUsuariosAsignados.Tables(0).Rows.Count >= 1) Then
            MsgBox(("Los perfiles ADMINISTRADOR y SEGURIDAD no se deben mezclar con otros Perfiles."), MsgBoxStyle.Critical, gtxtNombreSistema & " Mantenedor de Usuarios")
            Exit Sub
        End If

        For lintIndiceFilaPerfil = 0 To DS_PerfilesUsuariosAsignados.Tables(0).Rows.Count - 1
            If lblnExisteAdministrador And (DS_PerfilesUsuariosAsignados.Tables(0).Rows.Count >= 1) And _
               DS_PerfilesUsuariosAsignados.Tables(0).Rows(lintIndiceFilaPerfil).Item("ID_NEGOCIO").ToString.Trim = lintIdNegocioPorAsociar.ToString.Trim Then
                MsgBox(("El perfil ADMINISTRADOR no se deben mezclar con otros Perfiles."), MsgBoxStyle.Critical, gtxtNombreSistema & " Mantenedor de Usuarios")
                Exit Sub
            End If
            If lblnExisteSeguridad And (DS_PerfilesUsuariosAsignados.Tables(0).Rows.Count >= 1) And _
               DS_PerfilesUsuariosAsignados.Tables(0).Rows(lintIndiceFilaPerfil).Item("ID_NEGOCIO").ToString.Trim = lintIdNegocioPorAsociar.ToString.Trim Then
                MsgBox(("El perfil SEGURIDAD no se deben mezclar con otros Perfiles."), MsgBoxStyle.Critical, gtxtNombreSistema & " Mantenedor de Usuarios")
                Exit Sub
            End If
        Next
        '...FIN Valida Perfiles Seguridad y Administrador, deben ser unicos por negocio


        For lintIndiceFilaPerfil = 0 To DS_PerfilesUsuariosDisponibles.Tables("PERFILES_USUARIOS").Rows.Count - 1
            If DS_PerfilesUsuariosDisponibles.Tables("PERFILES_USUARIOS").Rows(lintIndiceFilaPerfil)("ID_PERFIL_USUARIO") = lintPerfilPorAsociar And _
               DS_PerfilesUsuariosDisponibles.Tables("PERFILES_USUARIOS").Rows(lintIndiceFilaPerfil)("ID_NEGOCIO") = lintIdNegocioPorAsociar Then

                DR_PerfilPorAsociar = DS_PerfilesUsuariosDisponibles.Tables("PERFILES_USUARIOS").Rows(lintIndiceFilaPerfil)

                DS_PerfilesUsuariosAsignados.Tables("PERFILES_USUARIOS").ImportRow(DR_PerfilPorAsociar)
                DS_PerfilesUsuariosAsignados.Tables("PERFILES_USUARIOS").AcceptChanges()
                DS_PerfilesUsuariosAsignados.Tables("PERFILES_USUARIOS").DefaultView.Sort = "NOMBRE_PERFIL"

                Tdg_PerfilesAsignados.DataSource = DS_PerfilesUsuariosAsignados.Tables("PERFILES_USUARIOS").DefaultView.ToTable
                FormateaGrillaPerfilesAsignados()
                Exit For
            End If
        Next

        DS_PerfilesUsuariosDisponibles.Tables("PERFILES_USUARIOS").Rows(lintIndiceFilaPerfil).Delete()
        DS_PerfilesUsuariosDisponibles.Tables("PERFILES_USUARIOS").AcceptChanges()
        Tdg_PerfilesDisponibles.DataSource = DS_PerfilesUsuariosDisponibles.Tables("PERFILES_USUARIOS")
        FormateaGrillaPerfilesDisponibles()
    End Sub

    Private Sub Btn_QuitarPerfilUsuario_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_QuitarPerfilUsuario.Click
        If Tdg_PerfilesAsignados.RowCount = 0 Then
            Exit Sub
        End If

        Dim lintIdNegocioAQuitar As Integer
        Dim lintPerfilAQuitar As Integer
        Dim lintIndiceFilaPerfil As Integer
        Dim DR_PerfilAQuitar As DataRow

        lintPerfilAQuitar = Tdg_PerfilesAsignados.Columns("ID_PERFIL_USUARIO").Text
        lintIdNegocioAQuitar = Tdg_PerfilesAsignados.Columns("ID_NEGOCIO").Text

        For lintIndiceFilaPerfil = 0 To DS_PerfilesUsuariosAsignados.Tables("PERFILES_USUARIOS").Rows.Count - 1
            If DS_PerfilesUsuariosAsignados.Tables("PERFILES_USUARIOS").Rows(lintIndiceFilaPerfil)("ID_PERFIL_USUARIO") = lintPerfilAQuitar And _
               DS_PerfilesUsuariosAsignados.Tables("PERFILES_USUARIOS").Rows(lintIndiceFilaPerfil)("ID_NEGOCIO") = lintIdNegocioAQuitar Then

                DR_PerfilAQuitar = DS_PerfilesUsuariosAsignados.Tables("PERFILES_USUARIOS").Rows(lintIndiceFilaPerfil)

                DS_PerfilesUsuariosDisponibles.Tables("PERFILES_USUARIOS").ImportRow(DR_PerfilAQuitar)
                DS_PerfilesUsuariosDisponibles.Tables("PERFILES_USUARIOS").AcceptChanges()
                DS_PerfilesUsuariosDisponibles.Tables("PERFILES_USUARIOS").DefaultView.Sort = "NOMBRE_PERFIL"

                Tdg_PerfilesDisponibles.DataSource = DS_PerfilesUsuariosDisponibles.Tables("PERFILES_USUARIOS").DefaultView.ToTable
                FormateaGrillaPerfilesDisponibles()
                Exit For
            End If
        Next

        DS_PerfilesUsuariosAsignados.Tables("PERFILES_USUARIOS").Rows(lintIndiceFilaPerfil).Delete()
        DS_PerfilesUsuariosAsignados.Tables("PERFILES_USUARIOS").AcceptChanges()
        Tdg_PerfilesAsignados.DataSource = DS_PerfilesUsuariosAsignados.Tables("PERFILES_USUARIOS")
        FormateaGrillaPerfilesAsignados()
    End Sub

    Private Sub AgregarPerfilesUsuarios(ByVal intIdUsuario As Integer)
        Dim lintIdPerfilUsuario As Integer = 0
        Dim lintFila As Integer = 0

        If Tdg_PerfilesAsignados.RowCount > 0 Then
            ltxtResultadoTransaccion = lobjPerfilesUsuarios.AgregarRelacionPerfilUsuario(intIdUsuario, DS_PerfilesUsuariosAsignados)
        End If
    End Sub

    Private Sub ModificarPerfilesUsuario(ByVal intIdUsuario As Integer)
        Dim lintFila_Original As Integer
        Dim lintFila_Asignados As Integer
        'Dim lstrCadenaPerfilesEliminados As String = ""
        'Dim lstrCadenaPerfilesAgregados As String = ""
        Dim lintIdPerfilUsuario As Integer
        Dim lintIdNegocio As Integer
        Dim lintTodasCuentas As Integer = 0
        Dim lblnEliminado As Boolean
        Dim lblnAgregado As Boolean

        Dim lintCantidadEliminados As Integer = 0
        Dim lintCantidadAgregados As Integer = 0

        Dim lDS_PerfilesUsuariosAgregados As New DataSet
        Dim lDS_PerfilesUsuariosEliminados As New DataSet
        Dim lDT_PerfilesUsuarios As New DataTable

        'Crea DS de Id de Perfiles que fueron Eliminados
        lDT_PerfilesUsuarios = DT_PerfilesAsignados_Original.Clone
        For lintFila_Original = 0 To DT_PerfilesAsignados_Original.Rows.Count - 1

            lintIdPerfilUsuario = DT_PerfilesAsignados_Original.Rows(lintFila_Original).Item("ID_PERFIL_USUARIO")
            lintIdNegocio = DT_PerfilesAsignados_Original.Rows(lintFila_Original).Item("ID_NEGOCIO")

            lblnEliminado = True
            For lintFila_Asignados = 0 To DS_PerfilesUsuariosAsignados.Tables(0).Rows.Count - 1
                If DT_PerfilesAsignados_Original.Rows(lintFila_Original).Item("ID_PERFIL_USUARIO") = DS_PerfilesUsuariosAsignados.Tables(0).Rows(lintFila_Asignados).Item("ID_PERFIL_USUARIO") And _
                   DT_PerfilesAsignados_Original.Rows(lintFila_Original).Item("ID_NEGOCIO") = DS_PerfilesUsuariosAsignados.Tables(0).Rows(lintFila_Asignados).Item("ID_NEGOCIO") Then
                    lblnEliminado = False
                    Exit For
                End If
            Next
            If lblnEliminado Then
                lintCantidadEliminados = lintCantidadEliminados + 1
                lDT_PerfilesUsuarios.ImportRow(DT_PerfilesAsignados_Original.Rows(lintFila_Original))
                lDT_PerfilesUsuarios.AcceptChanges()
            End If
        Next

        lDS_PerfilesUsuariosEliminados.Tables.Add(lDT_PerfilesUsuarios)
        lDS_PerfilesUsuariosEliminados.AcceptChanges()
        lDT_PerfilesUsuarios = Nothing

        'Crea Cadena de Id de Perfiles que fueron Agregados
        lDT_PerfilesUsuarios = DT_PerfilesAsignados_Original.Clone
        For lintFila_Asignados = 0 To DS_PerfilesUsuariosAsignados.Tables(0).Rows.Count - 1

            lintIdPerfilUsuario = DS_PerfilesUsuariosAsignados.Tables(0).Rows(lintFila_Asignados).Item("ID_PERFIL_USUARIO")
            lintIdNegocio = DS_PerfilesUsuariosAsignados.Tables(0).Rows(lintFila_Asignados).Item("ID_NEGOCIO")

            lblnAgregado = True
            Dim lintFilaAgrega As Integer = 0
            For lintFila_Original = 0 To DT_PerfilesAsignados_Original.Rows.Count - 1
                If DS_PerfilesUsuariosAsignados.Tables(0).Rows(lintFila_Asignados).Item("ID_PERFIL_USUARIO") = DT_PerfilesAsignados_Original.Rows(lintFila_Original).Item("ID_PERFIL_USUARIO") And _
                   DS_PerfilesUsuariosAsignados.Tables(0).Rows(lintFila_Asignados).Item("ID_NEGOCIO") = DT_PerfilesAsignados_Original.Rows(lintFila_Original).Item("ID_NEGOCIO") Then
                    lintFilaAgrega = lintFila_Original
                    lblnAgregado = False
                    Exit For
                End If
            Next
            If lblnAgregado Then
                lintCantidadAgregados = lintCantidadAgregados + 1
                lDT_PerfilesUsuarios.ImportRow(DS_PerfilesUsuariosAsignados.Tables(0).Rows(lintFila_Asignados))
                lDT_PerfilesUsuarios.AcceptChanges()
            End If
        Next

        lDS_PerfilesUsuariosAgregados.Tables.Add(lDT_PerfilesUsuarios)
        lDS_PerfilesUsuariosAgregados.AcceptChanges()

        If lintCantidadEliminados > 0 Then
            ltxtResultadoTransaccion = lobjPerfilesUsuarios.EliminarRelacionPerfilUsuario(intIdUsuario, lDS_PerfilesUsuariosEliminados)
        End If

        If lintCantidadAgregados > 0 Then
            ltxtResultadoTransaccion = lobjPerfilesUsuarios.AgregarRelacionPerfilUsuario(intIdUsuario, lDS_PerfilesUsuariosAgregados)
        End If

    End Sub

#End Region

#Region " --- TAB Cuentas del Usuario ---"
    Private Sub CargaNegociosCuenta()
        Dim DT_NegociosCuentas As New DataTable
        'Dim DR_NegociosCuentas As DataRow
        ' Dim lblnNoExiste As Boolean
        'DT_NegociosCuentas = DS_Negocios.Tables(0).Clone
        'DR_NegociosCuentas = DS_Negocios.Tables(0).NewRow()
        ''

        'For Each DR_Fila As DataRow In DS_PerfilesUsuariosAsignados.Tables(0).Rows
        '    DR_NegociosCuentas = DT_NegociosCuentas.NewRow()
        '    lblnNoExiste = True
        '    For lint As Integer = 0 To DT_NegociosCuentas.Rows.Count - 1
        '        If DT_NegociosCuentas.Rows(lint)("ID_NEGOCIO") = DR_Fila("ID_NEGOCIO") Then
        '            lblnNoExiste = False
        '            Exit For
        '        End If
        '    Next
        '    If lblnNoExiste Then
        '        DR_NegociosCuentas("ID_NEGOCIO") = DR_Fila("ID_NEGOCIO")
        '        DR_NegociosCuentas("ABR_NEGOCIO") = DR_Fila("ABR_NEGOCIO")
        '        DT_NegociosCuentas.Rows.Add(DR_NegociosCuentas)
        '        DT_NegociosCuentas.AcceptChanges()
        '    End If


        'Next
        'DT_NegociosCuentas.DefaultView.Sort = "ABR_NEGOCIO"
        'Cmb_NegocioCuentas.DataSource = Nothing
        'Cmb_NegocioCuentas.DataSource = DS_Negocios

        'Cmb_NegocioCuentas.Columns(0).DataField = "ABR_NEGOCIO"
        'Cmb_NegocioCuentas.Columns(1).DataField = "ID_NEGOCIO"

        'Cmb_NegocioCuentas.Splits(0).DisplayColumns("ABR_NEGOCIO").Visible = True
        'Cmb_NegocioCuentas.Splits(0).DisplayColumns("ID_NEGOCIO").Visible = False
        If ltxtResultadoTransaccion = "OK" Then
            If DS_Negocios.Tables.Count > 0 Then
                If DS_Negocios.Tables(0).Rows.Count > 0 Then
                    Cmb_NegocioCuentas.DataSource = DS_Negocios.Tables(0)

                    Cmb_NegocioCuentas.Columns(0).DataField = "ABR_NEGOCIO"
                    Cmb_NegocioCuentas.Columns(1).DataField = "ID_NEGOCIO"

                    Cmb_NegocioCuentas.Splits(0).DisplayColumns("ABR_NEGOCIO").Visible = True
                    Cmb_NegocioCuentas.Splits(0).DisplayColumns("ID_NEGOCIO").Visible = False
                    Cmb_NegocioCuentas.SelectedIndex = gFunSeteaItemComboBox(Cmb_NegocioCuentas, glngNegocioOperacion)
                End If
            End If
        End If
    End Sub

    Private Sub CargaCuentasAsociadas(ByVal intIdUsuario As Integer)
        Dim lstrColumnas As String = "NUM_CTA,ABR_CTA,ID_CUENTA,DCS_CUENTA,NEGOCIO"
        ltxtResultadoTransaccion = ""
        DS_CuentasAsociadas = Nothing
        Tdg_CuentasAsociadas.DataSource = Nothing
        DS_CuentasAsociadas = lobjCuentasSeguridad.RetornaCuentasAsociadas(ltxtResultadoTransaccion, lstrColumnas, intIdUsuario)

        If ltxtResultadoTransaccion = "OK" Then
            Tdg_CuentasAsociadas.DataSource = DS_CuentasAsociadas.Tables(0)
            FormateaGrillaCuentasAsociadas()
        Else
            MsgBox("No se pudo Obtener Cuentas Asociadas" & Chr(13) & ltxtResultadoTransaccion, MsgBoxStyle.Critical, "Mantenedor de Usuarios")
        End If

        If intIdUsuario = 0 Then
            'DS_CuentasDisponibles.Tables(0) = lobjCuentasSeguridad.RetornaCuentasAsociadas(ltxtResultadoTransaccion, lstrColumnas, intIdUsuario)
            Tdg_CuentasAsociadas.DataSource = DS_CuentasAsociadas.Tables(0)

            FormateaGrillaCuentaDisponibles()
        End If

    End Sub

    Private Sub FormateaGrillaCuentasAsociadas()
        Dim Tdg_Grilla As C1TrueDBGrid
        Dim LInt_Col As Integer
        Tdg_Grilla = Tdg_CuentasAsociadas
        Tdg_Grilla.Splits(0).HScrollBar.Style = ScrollBarStyleEnum.None
        Tdg_Grilla.ColumnHeaders = True

        If (Tdg_Grilla.RowCount >= 0) Then
            Tdg_Grilla.Splits(0).HScrollBar.Style = C1.Win.C1TrueDBGrid.ScrollBarStyleEnum.Automatic
            Tdg_Grilla.Splits(0).VScrollBar.Style = C1.Win.C1TrueDBGrid.ScrollBarStyleEnum.Always

            With Tdg_Grilla
                .FilterBar = True
                .FetchRowStyles = True
                LInt_Col = 0
                .Columns(LInt_Col).Caption = "N� Cta"
                .Splits(0).DisplayColumns(LInt_Col).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Width = 50
                .Columns(LInt_Col).DataField = "NUM_CTA"
                .Splits(0).DisplayColumns(LInt_Col).Style.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Visible = True
                .Splits(0).DisplayColumns(LInt_Col).AllowSizing = False

                LInt_Col = LInt_Col + 1
                .Columns(LInt_Col).Caption = "ABR_CTA"
                .Splits(0).DisplayColumns(LInt_Col).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Width = 103
                .Columns(LInt_Col).DataField = "ABR_CTA"
                .Splits(0).DisplayColumns(LInt_Col).Style.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Visible = False
                .Splits(0).DisplayColumns(LInt_Col).AllowSizing = False

                LInt_Col = LInt_Col + 1
                .Columns(LInt_Col).Caption = "ID_CUENTA"
                .Splits(0).DisplayColumns(LInt_Col).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Width = 50
                .Columns(LInt_Col).DataField = "ID_CUENTA"
                .Splits(0).DisplayColumns(LInt_Col).Style.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Visible = False
                .Splits(0).DisplayColumns(LInt_Col).AllowSizing = False

                LInt_Col = LInt_Col + 1
                .Columns(LInt_Col).Caption = "Nombre Cta."
                .Splits(0).DisplayColumns(LInt_Col).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Width = 150
                .Columns(LInt_Col).DataField = "DCS_CUENTA"
                .Splits(0).DisplayColumns(LInt_Col).Style.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Visible = True
                .Splits(0).DisplayColumns(LInt_Col).AllowSizing = False

                LInt_Col = LInt_Col + 1
                .Columns(LInt_Col).Caption = "Negocio"
                .Splits(0).DisplayColumns(LInt_Col).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Width = 80
                .Columns(LInt_Col).DataField = "NEGOCIO"
                .Splits(0).DisplayColumns(LInt_Col).Style.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Visible = True
                .Splits(0).DisplayColumns(LInt_Col).AllowSizing = False
            End With
        End If

    End Sub

    Private Function TienePerfilAsociado() As Boolean
        Dim lblnTienePerfiles As Boolean = True
        If DS_PerfilesUsuariosAsignados.Tables(0).Rows.Count = 0 Then
            MsgBox("Para asignar cuentas debe asignar al menos un perfil al usuario", MsgBoxStyle.Information, gtxtNombreSistema & Me.Text)
            lblnTienePerfiles = False
        End If

        Return lblnTienePerfiles
    End Function

    Private Sub Btn_BuscarCuenta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_BuscarCuenta.Click
        'Dim objFrmBuscarCuentas As New Frm_BuscarCuentas

        'objFrmBuscarCuentas.gStrNCuenta = Txt_NumeroCuenta.Text.Trim
        'objFrmBuscarCuentas.gStrNombreCuenta = Txt_NombreCuenta.Text.Trim
        'objFrmBuscarCuentas.gIntIdNegocio = Cmb_NegocioCuentas.Columns(1).CellValue(Cmb_NegocioCuentas.SelectedIndex)

        'Dim lDlgRetorno As DialogResult
        'If Not objFrmBuscarCuentas.Visible Then
        '    lDlgRetorno = objFrmBuscarCuentas.ShowDialog()
        'Else
        '    lDlgRetorno = Windows.Forms.DialogResult.Cancel
        'End If

        'objFrmBuscarCuentas.Dispose()

        'If lDlgRetorno = Windows.Forms.DialogResult.Cancel Then
        '    Txt_NombreCliente.Text = objFrmBuscarCuentas.gStrNombresCliente
        'ElseIf objFrmBuscarCuentas.gRowCuentaSeleccionada("ESTADO_CUENTA").ToString.Trim.ToUpper = "HABILITADA" Then
        '    ldrvCuentaSeleccionada = objFrmBuscarCuentas.gRowCuentaSeleccionada
        '    'Txt_RutCliente.Text = ldrvCuentaSeleccionada("RUT_CLIENTE").ToString.Trim
        '    'Txt_NombreCliente.Text = ldrvCuentaSeleccionada("NOMBRE_CLIENTE").ToString.Trim
        '    Txt_NumeroCuenta.Text = ldrvCuentaSeleccionada("NUM_CTA").ToString.Trim
        '    Txt_NombreCuenta.Text = ldrvCuentaSeleccionada("DCS_CUENTA").ToString.Trim
        '    Cmb_PerfilRiesgo.SelectedIndex = gFunSeteaItemComboBox(Cmb_PerfilRiesgo, ldrvCuentaSeleccionada("DSC_PERFIL_RIESGO").ToString.Trim)
        '    'Txt_PerfilRiesgo.Text = ldrvCuentaSeleccionada("DSC_PERFIL_RIESGO").ToString.Trim
        '    ldrvClienteSeleccionado = ldrvCuentaSeleccionada

        '    Btn_BuscarCuentasDisponibles_Click(Btn_BuscarCuentasDisponibles, Nothing)
        'End If
    End Sub

    Private Sub BuscarCuentas(ByVal strNumeroCuenta As String, ByVal drvCuenta As DataRowView, ByVal drvCliente As DataRowView)
        Dim strColumnasCuentas As String = ""
        Dim strResultado As String = ""
        Dim ldtvCuentas As DataView
        Dim lintIdNegocioCuenta As Integer

        lintIdNegocioCuenta = Cmb_NegocioCuentas.Columns(1).CellValue(Cmb_NegocioCuentas.SelectedIndex)
        'Buscando las cuentas 
        DS_Cuentas = lobjCuentas.TraerCuentas(lintIdNegocioCuenta, "", "", strNumeroCuenta, "", "", "", "", "", strResultado)

        If strResultado = "OK" Then

            If DS_Cuentas.Tables("CUENTAS").Rows.Count = 1 Then
                'Asigna al dataview del formulario los datos de la cuenta encontrada
                ldtvCuentas = DS_Cuentas.Tables("CUENTAS").DefaultView
                drvCuenta = ldtvCuentas.Item(0)

                If drvCuenta("ESTADO_CUENTA").ToString.Trim.ToUpper = "HABILITADA" Then


                    Txt_NombreCliente.Text = drvCuenta("NOMBRE_CLIENTE").ToString.Trim
                    Txt_NumeroCuenta.Text = drvCuenta("NUM_CTA").ToString.Trim
                    Txt_NombreCuenta.Text = drvCuenta("DCS_CUENTA").ToString.Trim
                    Cmb_PerfilRiesgo.SelectedIndex = gFunSeteaItemComboBox(Cmb_PerfilRiesgo, drvCuenta("DSC_PERFIL_RIESGO").ToString.Trim)

                    'Asigna al DataRowview Origen encontrado en el dataset
                    ldrvCuentaSeleccionada = drvCuenta
                    'Al ser clientes un subconjunto de cuentas se le puede asignar el mismo resultado de cuentas
                    ldrvClienteSeleccionado = drvCuenta

                Else
                    If drvCuenta("ESTADO_CUENTA") = "DESHABILITADA" Then
                        MsgBox("Cuenta se encuentra deshabilitada.", MsgBoxStyle.Exclamation, gtxtNombreSistema & Me.Text)

                    ElseIf drvCuenta("FLG_BLOQUEADO") = "S" Then
                        MsgBox("Cuenta se encuentra bloqueada." & vbCrLf & "Observaciones:" & vbCrLf & ldrvClienteSeleccionado("OBS_BLOQUEO"), MsgBoxStyle.Exclamation, gtxtNombreSistema & Me.Text)
                        Txt_NumeroCuenta.Text = ""
                        Txt_NombreCuenta.Text = ""

                    End If
                End If
            Else

                Btn_BuscarCuenta_Click(Btn_BuscarCuenta, Nothing)

            End If
        End If
    End Sub

    Private Sub BuscarCuentasxCliente(ByVal strIdCliente As String)
        Dim lstrRetorno As String = ""
        Dim lstrColumnas As String = "ID_CUENTA, ABR_CTA, NUM_CTA,DCS_CUENTA"
        Dim lintIdNegocioCuenta As Integer

        lintIdNegocioCuenta = Cmb_NegocioCuentas.Columns(1).CellValue(Cmb_NegocioCuentas.SelectedIndex)

        'Por el momento se pasa 1 en el valor negocio se sacara de una clase por el negocio de entorno
        DS_Cuentas = lobjCuentas.TraerCuentasxCliente(lintIdNegocioCuenta, strIdCliente, lstrColumnas, lstrRetorno)

        If lstrRetorno = "OK" Then
            Tdg_CuentasDisponibles.DataSource = DS_Cuentas.Tables("CUENTAS")
            FormateaGrillaCuentaDisponibles()
        End If

    End Sub

    Private Sub FormateaGrillaCuentaDisponibles()
        Dim Tdg_Grilla As C1TrueDBGrid
        Dim LInt_Col As Integer
        Tdg_Grilla = Tdg_CuentasDisponibles
        Tdg_Grilla.Splits(0).HScrollBar.Style = ScrollBarStyleEnum.None
        Tdg_Grilla.ColumnHeaders = True

        If (Tdg_Grilla.RowCount >= 0) Then
            Tdg_Grilla.Splits(0).HScrollBar.Style = C1.Win.C1TrueDBGrid.ScrollBarStyleEnum.Automatic
            Tdg_Grilla.Splits(0).VScrollBar.Style = C1.Win.C1TrueDBGrid.ScrollBarStyleEnum.Always

            With Tdg_Grilla
                .FilterBar = True
                .FetchRowStyles = True
                LInt_Col = 0
                .Columns(LInt_Col).Caption = "ID_CUENTA"
                .Splits(0).DisplayColumns(LInt_Col).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Width = 50
                .Columns(LInt_Col).DataField = "ID_CUENTA"
                .Splits(0).DisplayColumns(LInt_Col).Style.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Visible = False
                .Splits(0).DisplayColumns(LInt_Col).AllowSizing = False

                LInt_Col = LInt_Col + 1
                .Columns(LInt_Col).Caption = "N� Cta."
                .Splits(0).DisplayColumns(LInt_Col).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Width = 45
                .Columns(LInt_Col).DataField = "NUM_CTA"
                .Splits(0).DisplayColumns(LInt_Col).Style.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Visible = True
                .Splits(0).DisplayColumns(LInt_Col).AllowSizing = False

                LInt_Col = LInt_Col + 1
                .Columns(LInt_Col).Caption = "ABR_CTA"
                .Splits(0).DisplayColumns(LInt_Col).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Width = 120
                .Columns(LInt_Col).DataField = "ABR_CTA"
                .Splits(0).DisplayColumns(LInt_Col).Style.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Visible = False
                .Splits(0).DisplayColumns(LInt_Col).AllowSizing = False

                LInt_Col = LInt_Col + 1
                .Columns(LInt_Col).Caption = "Nombre Cta."
                .Splits(0).DisplayColumns(LInt_Col).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Width = 145
                .Columns(LInt_Col).DataField = "DCS_CUENTA"
                .Splits(0).DisplayColumns(LInt_Col).Style.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Visible = True
                .Splits(0).DisplayColumns(LInt_Col).AllowSizing = False

                LInt_Col = LInt_Col + 1
                .Columns(LInt_Col).Caption = "ASOCIADA"
                .Splits(0).DisplayColumns(LInt_Col).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Width = 10
                .Columns(LInt_Col).DataField = "ASOCIADA"
                .Splits(0).DisplayColumns(LInt_Col).Style.HorizontalAlignment = AlignHorzEnum.Near
                .Splits(0).DisplayColumns(LInt_Col).Visible = False
                .Splits(0).DisplayColumns(LInt_Col).AllowSizing = False
            End With
        End If
    End Sub

    Private Sub Txt_NumeroCuenta_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Txt_NumeroCuenta.KeyDown
        If e.KeyCode = Keys.Enter Or e.KeyCode = Keys.Tab Then
            If Txt_NumeroCuenta.Text.Trim <> "" Then
                BuscarCuentas(Txt_NumeroCuenta.Text.Trim, ldrvCuentaSeleccionada, ldrvClienteSeleccionado)
            End If
        End If
    End Sub

    Private Sub Btn_BuscarCliente_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_BuscarCliente.Click
        'Dim objFrmBuscarClientes As New Frm_BuscarClientes

        'objFrmBuscarClientes.gStrRutCliente = Txt_RutCliente.Text
        'objFrmBuscarClientes.gIntIdNegocio = Cmb_NegocioCuentas.Columns(1).CellValue(Cmb_NegocioCuentas.SelectedIndex)
        ''objFrmBuscarClientes.gStrNombresCliente = Txt_NombreCliente.Text

        'Dim lDlgRetorno As DialogResult
        'If Not objFrmBuscarClientes.Visible Then
        '    lDlgRetorno = objFrmBuscarClientes.ShowDialog()
        'Else
        '    lDlgRetorno = Windows.Forms.DialogResult.Cancel
        'End If

        'objFrmBuscarClientes.Dispose()

        'If lDlgRetorno = Windows.Forms.DialogResult.Cancel Then
        '    Txt_RutCliente.Text = objFrmBuscarClientes.gStrRutCliente
        '    Txt_NombreCliente.Text = objFrmBuscarClientes.gStrNombresCliente
        'Else
        '    If objFrmBuscarClientes.gRowClienteSeleccionado("ESTADO_CLIENTE").ToString.Trim.ToUpper = "VIGENTE" Then
        '        Txt_RutCliente.Text = objFrmBuscarClientes.gRowClienteSeleccionado("RUT_CLIENTE").ToString.Trim
        '        Txt_NombreCliente.Text = objFrmBuscarClientes.gRowClienteSeleccionado("NOMBRE_CLIENTE").ToString.Trim
        '        ldrvClienteSeleccionado = objFrmBuscarClientes.gRowClienteSeleccionado
        '        'BuscarCuentasxCliente(ldrvClienteSeleccionado("ID_CLIENTE").ToString.Trim.ToUpper)
        '    End If
        'End If

    End Sub

    Private Sub Btn_LimpiarFiltroCuentas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_LimpiarFiltroCuentas.Click
        Tdg_CuentasDisponibles.DataSource = Nothing
        Tdg_CuentasAsociadas.DataSource = Nothing

        CuentasDisponibles()
        CargaCuentasAsociadas(0)

        Txt_NumeroCuenta.Text = ""
        Txt_NombreCuenta.Text = ""
        Txt_RutCliente.Text = ""
        Txt_NombreCliente.Text = ""
        Cmb_TipoAdministracion.SelectedIndex = -1
        Cmb_PerfilRiesgo.SelectedIndex = -1
        Cmb_Segmento.SelectedIndex = -1
        Cmb_Sucursal.SelectedIndex = -1
        Cmb_Asesores.SelectedIndex = -1
    End Sub

    Private Sub Btn_BuscarCuentasDisponibles_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_BuscarCuentasDisponibles.Click
        If FunExisteFiltro() Then
            Dim lintIdUsuario As Integer
            Dim lintIdNegocio As Integer
            Dim lintIdPerfilRiesgo As Integer
            Dim lstrCodSucursal As String
            Dim lintIdSegmento As Integer
            Dim lstrTipoAdministracion As String = ""
            Dim DblAsesor As Double = 0
            Dim lstrColumnas As String = "ID_CUENTA,NUM_CTA,ABR_CTA,DCS_CUENTA, ASOCIADA"
            lintIdNegocio = Cmb_NegocioCuentas.Columns(1).CellValue(Cmb_NegocioCuentas.SelectedIndex).ToString()

            If Cmb_PerfilRiesgo.SelectedIndex <> -1 Then
                lintIdPerfilRiesgo = Cmb_PerfilRiesgo.Columns(0).CellValue(Cmb_PerfilRiesgo.SelectedIndex).ToString()
            End If

            If Cmb_Asesores.SelectedIndex = -1 Then
                DblAsesor = 0
            Else
                DblAsesor = Cmb_Asesores.GetItemText(Cmb_Asesores.SelectedIndex, 1)
            End If
            If Cmb_Sucursal.SelectedIndex <> -1 Then
                lstrCodSucursal = Cmb_Sucursal.Columns(1).CellValue(Cmb_Sucursal.SelectedIndex).ToString()
            Else
                lstrCodSucursal = "0"
            End If

            If Cmb_Segmento.SelectedIndex <> -1 Then
                lintIdSegmento = Cmb_Segmento.Columns(0).CellValue(Cmb_Segmento.SelectedIndex).ToString()
            End If

            If Cmb_TipoAdministracion.SelectedIndex <> -1 Then
                lstrTipoAdministracion = Cmb_TipoAdministracion.Columns(1).CellValue(Cmb_TipoAdministracion.SelectedIndex).ToString()
            End If

            ltxtResultadoTransaccion = ""
            If ltxtOperacion = "INSERTAR" Then lintIdUsuario = 0 Else lintIdUsuario = Tdg_Usuarios.Columns("ID_USUARIO").Text.Trim
            DS_CuentasDisponibles = lobjCuentasSeguridad.RetornaCuentasDisponibles(ltxtResultadoTransaccion, _
                                                                                    lintIdNegocio, _
                                                                                    lintIdUsuario, _
                                                                                    Txt_NombreCuenta.Text.Trim, _
                                                                                    "", _
                                                                                    Txt_NumeroCuenta.Text.Trim, _
                                                                                    Txt_RutCliente.Text.Trim, _
                                                                                    "", _
                                                                                    "", "", "", _
                                                                                    lintIdPerfilRiesgo, _
                                                                                    lstrTipoAdministracion, _
                                                                                    lstrCodSucursal, _
                                                                                    lintIdSegmento, _
                                                                                    "", DblAsesor, lstrColumnas)


            If ltxtResultadoTransaccion = "OK" Then
                For lIntFilaDisponible As Integer = 0 To DS_CuentasDisponibles.Tables(0).Rows.Count - 1
                    If Not IsNothing(DS_CuentasAsociadas) Then
                        If DS_CuentasAsociadas.Tables.Count > 0 Then
                            For Each DR_Asociada As DataRow In DS_CuentasAsociadas.Tables(0).Rows
                                If DS_CuentasDisponibles.Tables(0).Rows(lIntFilaDisponible)("ID_CUENTA") = DR_Asociada("ID_CUENTA") Then
                                    DS_CuentasDisponibles.Tables(0).Rows(lIntFilaDisponible)("ASOCIADA") = "S"
                                    Exit For
                                End If
                            Next
                        End If
                    End If
                Next
                Tdg_CuentasDisponibles.DataSource = DS_CuentasDisponibles.Tables(0)
                FormateaGrillaCuentaDisponibles()
                Tdg_CuentasDisponibles.Columns("ASOCIADA").FilterText = "N"
            End If
        Else
            MsgBox("Debe seleccionar al menos un criterio de b�squeda", MsgBoxStyle.Information, gtxtNombreSistema & Me.Text)
        End If
    End Sub

    '---

    Private Sub CuentasDisponibles()

        Dim lintIdUsuario As Integer
        Dim lintIdNegocio As Integer
        'Dim lintIdPerfilRiesgo As Integer
        'Dim lstrCodSucursal As String
        'Dim lintIdSegmento As Integer
        Dim lstrTipoAdministracion As String = ""
        Dim DblAsesor As Double = 0
        Dim lstrColumnas As String = "ID_CUENTA,NUM_CTA,ABR_CTA,DCS_CUENTA, ASOCIADA"
        lintIdNegocio = 1 'Cmb_NegocioCuentas.Columns(1).CellValue(Cmb_NegocioCuentas.SelectedIndex).ToString()

        ltxtResultadoTransaccion = ""
        If ltxtOperacion = "INSERTAR" Then lintIdUsuario = 0 Else lintIdUsuario = Tdg_Usuarios.Columns("ID_USUARIO").Text.Trim
        DS_CuentasDisponibles = lobjCuentasSeguridad.RetornaCuentasDisponibles(ltxtResultadoTransaccion, _
                                                                                lintIdNegocio, _
                                                                                lintIdUsuario, _
                                                                                "", _
                                                                                "", _
                                                                                "", _
                                                                                "", _
                                                                                "", _
                                                                                "", "", "", _
                                                                                0, _
                                                                                "", _
                                                                                "", _
                                                                                0, _
                                                                                "", 0, lstrColumnas)


        If ltxtResultadoTransaccion = "OK" Then
            For lIntFilaDisponible As Integer = 0 To DS_CuentasDisponibles.Tables(0).Rows.Count - 1
                If Not IsNothing(DS_CuentasAsociadas) Then
                    If DS_CuentasAsociadas.Tables.Count > 0 Then
                        For Each DR_Asociada As DataRow In DS_CuentasAsociadas.Tables(0).Rows
                            If DS_CuentasDisponibles.Tables(0).Rows(lIntFilaDisponible)("ID_CUENTA") = DR_Asociada("ID_CUENTA") Then
                                DS_CuentasDisponibles.Tables(0).Rows(lIntFilaDisponible)("ASOCIADA") = "S"
                                Exit For
                            End If
                        Next
                    End If
                End If
            Next
            Tdg_CuentasDisponibles.DataSource = DS_CuentasDisponibles.Tables(0)
            FormateaGrillaCuentaDisponibles()
            Tdg_CuentasDisponibles.Columns("ASOCIADA").FilterText = "N"
        End If

    End Sub






    Private Sub Btn_QuitarCuenta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_QuitarCuenta.Click
        'AgregarIsntrumento()
        'FormateaGrillaCuentasAsociadas()

        FUNC_Btn_QuitarRestriccion()
    End Sub

    Private Sub QuitarInstrumento()
        If Tdg_CuentasDisponibles.RowCount = 0 Then
            Exit Sub
        End If
        Dim lintIdCuenta As Integer = Tdg_CuentasDisponibles.Columns("ID_CUENTA").Value
        Dim lstrAbrCuenta As String = ""
        Dim lstrNumCuenta As String = Tdg_CuentasDisponibles.Columns("NUM_CTA").Value
        Dim lstrDcsCuenta As String = Tdg_CuentasDisponibles.Columns("DCS_CUENTA").Value

        lstrAbrCuenta = Tdg_CuentasDisponibles.Columns("ABR_CTA").Value.ToString

        Dim ldrFila As DataRow = Nothing
        Dim lintFila As Integer = 0
        Dim lintFilaEliminar As Integer = 0
        Dim DRNueva As DataRow = Nothing
        Dim lintFilaSel As Integer = 0


        For lintFilaSel = 0 To Tdg_CuentasDisponibles.SelectedRows.Count - 1

            lintIdCuenta = Tdg_CuentasDisponibles.Columns("ID_CUENTA").CellValue(lintFilaSel)
            'lstrAbrCuenta = Tdg_CuentasDisponibles.Columns("ABR_CTA").CellValue(Tdg_CuentasDisponibles.SelectedRows(lintFilaSel))
            lstrNumCuenta = Tdg_CuentasDisponibles.Columns("NUM_CTA").CellValue(lintFilaSel)
            lstrDcsCuenta = Tdg_CuentasDisponibles.Columns("DCS_CUENTA").CellValue(lintFilaSel)

            For lintFila = 0 To DS_CuentasDisponibles.Tables(0).Rows.Count - 1
                ldrFila = DS_CuentasDisponibles.Tables(0).Rows(lintFila)
                If ldrFila("ID_CUENTA") = lintIdCuenta Then
                    lintFilaEliminar = lintFila
                    DRNueva = DS_CuentasAsociadas.Tables(0).NewRow
                    DRNueva("ABR_CTA") = lstrAbrCuenta 'ldrFila("ABR_CTA")
                    DRNueva("ID_CUENTA") = ldrFila("ID_CUENTA")
                    DRNueva("NUM_CTA") = ldrFila("NUM_CTA")
                    DRNueva("DCS_CUENTA") = ldrFila("DCS_CUENTA")
                    DS_CuentasAsociadas.Tables(0).Rows.Add(DRNueva)
                    DS_CuentasAsociadas.AcceptChanges()
                    Exit For
                End If
            Next

            DS_CuentasDisponibles.Tables(0).Rows.RemoveAt(lintFilaEliminar)
            DS_CuentasDisponibles.Tables(0).AcceptChanges()
        Next
        If (Not IsNothing(DS_CuentasAsociadas)) AndAlso DS_CuentasAsociadas.Tables.Count > 0 Then
            DS_CuentasAsociadas.Tables(0).DefaultView.Sort = "ABR_CTA ASC"
        End If
        Tdg_CuentasAsociadas.Enabled = True
        Tdg_CuentasDisponibles.SelectedRows.Clear()

        StatusLabel.Text = "Cuentas asociados" & Tdg_CuentasAsociadas.RowCount - 1
    End Sub

    Private Sub Btn_AsociarCuenta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_AsociarCuenta.Click
        'QuitarInstrumento()
        'FormateaGrillaCuentasAsociadas()

        Func_Btn_AsociarRestriccion()
    End Sub

    Private Sub AgregarIsntrumento()
        If Tdg_CuentasAsociadas.RowCount = 0 Then
            Exit Sub
        End If
        Dim lintIdCuenta As Integer = Tdg_CuentasAsociadas.Columns("ID_CUENTA").Value
        Dim lstrAbrCuenta As String = ""
        Dim lstrNumCuenta As String = Tdg_CuentasAsociadas.Columns("NUM_CTA").Value
        Dim lstrDcsCuenta As String = Tdg_CuentasAsociadas.Columns("DCS_CUENTA").Value

        lstrAbrCuenta = Tdg_CuentasAsociadas.Columns("ABR_CTA").Value.ToString

        Dim ldrFila As DataRow = Nothing
        Dim lintFila As Integer = 0
        Dim lintFilaEliminar As Integer = 0
        Dim DRNueva As DataRow = Nothing
        Dim lintFilaSel As Integer = 0


        For lintFilaSel = 0 To Tdg_CuentasAsociadas.SelectedRows.Count - 1

            lintIdCuenta = Tdg_CuentasAsociadas.Columns("ID_CUENTA").CellValue(lintFilaSel)
            'lstrAbrCuenta = Tdg_CuentasAsociadas.Columns("ABR_CTA").CellValue(Tdg_CuentasDisponibles.SelectedRows(lintFilaSel))
            lstrNumCuenta = Tdg_CuentasAsociadas.Columns("NUM_CTA").CellValue(lintFilaSel)
            lstrDcsCuenta = Tdg_CuentasAsociadas.Columns("DCS_CUENTA").CellValue(lintFilaSel)

            For lintFila = 0 To DS_CuentasAsociadas.Tables(0).Rows.Count - 1
                ldrFila = DS_CuentasAsociadas.Tables(0).Rows(lintFila)
                If ldrFila("ID_CUENTA") = lintIdCuenta Then
                    lintFilaEliminar = lintFila
                    DRNueva = DS_CuentasDisponibles.Tables(0).NewRow
                    DRNueva("ABR_CTA") = lstrAbrCuenta 'ldrFila("ABR_CTA")
                    DRNueva("ID_CUENTA") = ldrFila("ID_CUENTA")
                    DRNueva("NUM_CTA") = ldrFila("NUM_CTA")
                    DRNueva("DCS_CUENTA") = ldrFila("DCS_CUENTA")
                    DS_CuentasDisponibles.Tables(0).Rows.Add(DRNueva)
                    DS_CuentasDisponibles.AcceptChanges()
                    Exit For
                End If
            Next

            DS_CuentasAsociadas.Tables(0).Rows.RemoveAt(lintFilaEliminar)
            'DS_CuentasDisponibles.Tables(0).AcceptChanges()
            DS_CuentasAsociadas.Tables(0).AcceptChanges()
        Next
        If (Not IsNothing(DS_CuentasDisponibles)) AndAlso DS_CuentasDisponibles.Tables.Count > 0 Then
            DS_CuentasDisponibles.Tables(0).DefaultView.Sort = "ABR_CTA ASC"
        End If
        Tdg_CuentasDisponibles.Enabled = True
        'Tdg_CuentasDisponibles.SelectedRows.Clear()
        Tdg_CuentasAsociadas.SelectedRows.Clear()
        StatusLabel.Text = "Instrumentos asociados" & Tdg_CuentasDisponibles.RowCount - 1
    End Sub

    Private Sub Func_Btn_AsociarRestriccion() 'Handles Btn_AsociarCliente.Click
        If Tdg_CuentasDisponibles.RowCount = 0 Then
            Exit Sub
        End If

        'Dim lintIdCliente As Integer = Tdg_ClientesDisponibles.Columns("ID_CLIENTE").Value
        'Dim lstrCodMonedaEvalua As String = "" & Tdg_ClientesDisponibles.Columns("COD_MONEDA_EVALUA").Value

        Dim lintIdCuenta As Integer = Tdg_CuentasDisponibles.Columns("ID_CUENTA").Value
        Dim lstrAbrCuenta As String = ""
        Dim lstrNumCuenta As String = Tdg_CuentasDisponibles.Columns("NUM_CTA").Value
        Dim lstrDcsCuenta As String = Tdg_CuentasDisponibles.Columns("DCS_CUENTA").Value



        Dim ldrFila As DataRow = Nothing
        Dim lintFila As Integer = 0
        Dim lintFilaEliminar As Integer = 0
        Dim DRNueva As DataRow = Nothing
        Dim lintFilaSel As Integer = 0
        Dim lintClientes() As Integer
        Dim lstrMonedas() As String
        Dim lblnEnLinea() As Boolean

        Dim lstrFiltro As String = ""
        'lstrFiltro = Tdg_CuentasDisponibles.Columns("NOMBRE_CLIENTE").FilterText

        ReDim lintClientes(Tdg_CuentasDisponibles.SelectedRows.Count - 1)
        ReDim lstrMonedas(Tdg_CuentasDisponibles.SelectedRows.Count - 1)
        ReDim lblnEnLinea(Tdg_CuentasDisponibles.SelectedRows.Count - 1)

        For lintFilaSel = 0 To Tdg_CuentasDisponibles.SelectedRows.Count - 1
            lintClientes(lintFilaSel) = Tdg_CuentasDisponibles.Columns("ID_CUENTA").CellValue(Tdg_CuentasDisponibles.SelectedRows(lintFilaSel))


        Next

        For lintFilaSel = 0 To Tdg_CuentasDisponibles.SelectedRows.Count - 1
            lintIdCuenta = lintClientes(lintFilaSel)
            For lintFila = 0 To DS_CuentasDisponibles.Tables(0).Rows.Count - 1
                ldrFila = DS_CuentasDisponibles.Tables(0).Rows(lintFila)
                If ldrFila("ID_CUENTA") = lintIdCuenta Then
                    lintFilaEliminar = lintFila
                    DRNueva = DS_CuentasAsociadas.Tables(0).NewRow
                    DRNueva("ABR_CTA") = lstrAbrCuenta 'ldrFila("ABR_CTA")
                    DRNueva("ID_CUENTA") = ldrFila("ID_CUENTA")
                    DRNueva("NUM_CTA") = ldrFila("NUM_CTA")
                    DRNueva("DCS_CUENTA") = ldrFila("DCS_CUENTA")
                    DS_CuentasAsociadas.Tables(0).Rows.Add(DRNueva)
                    DS_CuentasAsociadas.AcceptChanges()
                    Exit For
                End If
            Next

            DS_CuentasDisponibles.Tables(0).Rows.RemoveAt(lintFilaEliminar)
            DS_CuentasDisponibles.Tables(0).AcceptChanges()

            'Dim lintFilaAsociar = -1
            'For lintFila = 0 To dtbCambiosClientes.Rows.Count - 1
            '    ldrFila = dtbCambiosClientes.Rows(lintFila)
            '    If ldrFila("ID_CLIENTE") = lintIdCliente And ldrFila("ACCION") = "E" Then
            '        lintFilaAsociar = lintFila
            '        Exit For
            '    End If
            'Next
            'If lintFilaAsociar <> -1 Then
            '    dtbCambiosClientes.Rows(lintFilaAsociar)("ACCION") = "O"
            'Else
            '    DRNueva = dtbCambiosClientes.NewRow
            '    DRNueva("ID_CLIENTE") = lintIdCliente
            '    DRNueva("COD_MONEDA_EVALUA") = lstrCodMonedaEvalua
            '    DRNueva("ACCION") = "I"
            '    dtbCambiosClientes.Rows.Add(DRNueva)
            '    dtbCambiosClientes.AcceptChanges()
            'End If

        Next
        Tdg_CuentasDisponibles.DataSource = DS_CuentasDisponibles.Tables(0)
        Tdg_CuentasAsociadas.DataSource = DS_CuentasAsociadas.Tables(0)
        DS_CuentasAsociadas.Tables(0).DefaultView.Sort = "ABR_CTA ASC"
        Tdg_CuentasAsociadas.SelectedRows.Clear()
        Tdg_CuentasDisponibles.SelectedRows.Clear()
        Tdg_CuentasDisponibles.Enabled = True
        StatusLabel.Text = "Cuentas asociados: " & Tdg_CuentasAsociadas.RowCount
    End Sub


    Private Sub FUNC_Btn_QuitarRestriccion() 'Handles Btn_QuitarCliente.Click
        If Tdg_CuentasAsociadas.RowCount = 0 Then
            Exit Sub
        End If
        Dim lintIdCliente As Integer = Tdg_CuentasAsociadas.Columns("ID_CUENTA").Value
        Dim ldrFila As DataRow = Nothing
        Dim lintFila As Integer = 0
        Dim lintFilaEliminar As Integer = 0
        Dim DRNueva As DataRow = Nothing
        Dim lintFilaSel As Integer = 0
        Dim lintClientes() As Integer

        Dim lintIdCuenta As Integer = Tdg_CuentasAsociadas.Columns("ID_CUENTA").Value
        Dim lstrAbrCuenta As String = ""
        Dim lstrNumCuenta As String = Tdg_CuentasAsociadas.Columns("NUM_CTA").Value
        Dim lstrDcsCuenta As String = Tdg_CuentasAsociadas.Columns("DCS_CUENTA").Value


        ReDim lintClientes(Tdg_CuentasAsociadas.SelectedRows.Count - 1)


        For lintFilaSel = 0 To Tdg_CuentasAsociadas.SelectedRows.Count - 1
            lintClientes(lintFilaSel) = Tdg_CuentasAsociadas.Columns("ID_CUENTA").CellValue(Tdg_CuentasAsociadas.SelectedRows(lintFilaSel))


        Next

        For lintFilaSel = 0 To Tdg_CuentasAsociadas.SelectedRows.Count - 1
            lintIdCuenta = lintClientes(lintFilaSel)
            For lintFila = 0 To DS_CuentasAsociadas.Tables(0).Rows.Count - 1
                ldrFila = DS_CuentasAsociadas.Tables(0).Rows(lintFila)
                If ldrFila("ID_CUENTA") = lintIdCuenta Then
                    lintFilaEliminar = lintFila
                    DRNueva = DS_CuentasDisponibles.Tables(0).NewRow
                    DRNueva("ABR_CTA") = lstrAbrCuenta 'ldrFila("ABR_CTA")
                    DRNueva("ID_CUENTA") = ldrFila("ID_CUENTA")
                    DRNueva("NUM_CTA") = ldrFila("NUM_CTA")
                    DRNueva("DCS_CUENTA") = ldrFila("DCS_CUENTA")
                    DRNueva("ASOCIADA") = "N"
                    DS_CuentasDisponibles.Tables(0).Rows.Add(DRNueva)
                    DS_CuentasDisponibles.AcceptChanges()
                    Exit For
                End If
            Next

            'Dim dvDisponibles As DataView = Nothing
            DS_CuentasAsociadas.Tables(0).Rows.RemoveAt(lintFilaEliminar)
            DS_CuentasAsociadas.Tables(0).AcceptChanges()

            'Dim lintFilaModifica As Integer = -1
            'lintFilaEliminar = -1
            'For lintFila = 0 To dtbCambiosClientes.Rows.Count - 1
            '    ldrFila = dtbCambiosClientes.Rows(lintFila)
            '    If ldrFila("ID_CLIENTE") = lintIdCliente And ldrFila("ACCION") = "I" Then
            '        lintFilaEliminar = lintFila
            '        Exit For
            '    End If
            '    If ldrFila("ID_CLIENTE") = lintIdCliente And ldrFila("ACCION") = "O" Then
            '        lintFilaModifica = lintFila
            '        Exit For
            '    End If
            'Next
            'If lintFilaEliminar <> -1 Then
            '    dtbCambiosClientes.Rows.RemoveAt(lintFilaEliminar)
            'ElseIf lintFilaModifica <> -1 Then
            '    dtbCambiosClientes.Rows(lintFilaModifica)("ACCION") = "E"
            '    dtbCambiosClientes.AcceptChanges()
            'End If
        Next
        Tdg_CuentasDisponibles.DataSource = DS_CuentasDisponibles.Tables(0)
        Tdg_CuentasAsociadas.DataSource = DS_CuentasAsociadas.Tables(0)
        DS_CuentasDisponibles.Tables(0).DefaultView.Sort = "ABR_CTA ASC"
        Tdg_CuentasDisponibles.SelectedRows.Clear()
        Tdg_CuentasAsociadas.SelectedRows.Clear()
        Tdg_CuentasAsociadas.Enabled = True
        StatusLabel.Text = "Cuentas asociados" & Tdg_CuentasAsociadas.RowCount - 1
    End Sub



    Private Sub ModificarCuentasUsuario(ByVal intIdUsuario As Integer)
        If DS_CuentasAsociadas.Tables.Count > 0 Then
            ltxtResultadoTransaccion = lobjCuentasSeguridad.ProcesarCuentasUsuario(intIdUsuario, DS_CuentasAsociadas)
            If ltxtResultadoTransaccion <> "OK" Then
                MsgBox("No se pudo actualizar Cuentas del Usuario", MsgBoxStyle.Information, gtxtNombreSistema & Me.Text)
            End If
        End If
    End Sub

    Private Function FunExisteFiltro() As Boolean
        Dim lBlnFiltro As Boolean = False

        If Txt_NumeroCuenta.Text <> "" Then
            lBlnFiltro = True
        End If

        If Txt_NombreCuenta.Text <> "" Then
            lBlnFiltro = True
        End If

        If Txt_RutCliente.Text <> "" Then
            lBlnFiltro = True
        End If

        If Cmb_TipoAdministracion.SelectedIndex <> -1 Then
            lBlnFiltro = True
        End If

        If Cmb_PerfilRiesgo.SelectedIndex <> -1 Then
            lBlnFiltro = True
        End If

        If Cmb_Segmento.SelectedIndex <> -1 Then
            lBlnFiltro = True
        End If

        If Cmb_Sucursal.SelectedIndex <> -1 Then
            lBlnFiltro = True
        End If

        Return lBlnFiltro

        MsgBox("Debe seleccionar al menos un criterio de b�squeda", MsgBoxStyle.Exclamation, gtxtNombreSistema & Me.Text)

    End Function
#End Region

    Private Sub Tdg_CuentasDisponibles_FilterChange(ByVal sender As Object, ByVal e As System.EventArgs) Handles Tdg_CuentasDisponibles.FilterChange
        Dim sb As New System.Text.StringBuilder
        Dim dc As C1.Win.C1TrueDBGrid.C1DataColumn
        Try
            sender.SelectedRows.Clear()
            For Each dc In sender.Columns
                If dc.FilterText.Trim.Length > 0 Then
                    If sb.Length > 0 Then
                        sb.Append(" AND ")
                    End If
                    sb.Append(gstrFiltro_X_TipoColumna(dc))
                End If
            Next dc
            '... Realiza el filtro sobre el dataset
            sender.DataSource.DefaultView.RowFilter = sb.ToString
            sender.Refresh()

            If sender.RowCount > 0 Then
                sender.SelectedRows.Add(0)
            End If
        Catch ex As Exception
            Debug.Print(ex.Message)
        End Try
    End Sub

    Private Sub Tdg_CuentasDisponibles_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Tdg_CuentasDisponibles.KeyDown
        If Not e.Shift And Not e.Control Then
            sender.SelectedRows.Clear()
            sender.MarqueeStyle = MarqueeEnum.NoMarquee
            sender.MarqueeStyle = MarqueeEnum.HighlightRow
        Else
            sender.MarqueeStyle = MarqueeEnum.NoMarquee
        End If

        bln_PressControl = e.Control
        bln_PressShift = e.Shift
    End Sub

    Private Sub Tdg_CuentasDisponibles_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Tdg_CuentasDisponibles.KeyUp
        bln_PressControl = False
        bln_PressShift = False
    End Sub

    Private Sub Tdg_CuentasDisponibles_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Tdg_CuentasDisponibles.MouseDown
        lint_Fila = sender.RowContaining(e.Y)
        Dim lint_Fila_Aux As Integer = 0
        Dim lint_Indice As Integer = 0
        Dim lint_Step As Integer = 0

        If lint_Fila <> -1 Then
            If bln_PressControl Then
                int_PrimeraFila = -1
                lint_Indice = sender.SelectedRows.IndexOF(lint_Fila)
                If lint_Indice <> -1 Then
                    sender.SelectedRows.RemoveAt(lint_Indice)
                Else
                    sender.SelectedRows.Add(lint_Fila)
                End If
            Else
                If bln_PressShift Then
                    int_PrimeraFila = -1
                    If int_UltimaFila > lint_Fila Then
                        lint_Step = -1
                    Else
                        lint_Step = 1
                    End If

                    For lint_Fila_Aux = int_UltimaFila To lint_Fila Step lint_Step
                        lint_Indice = sender.SelectedRows.IndexOf(lint_Fila_Aux)
                        If lint_Indice = -1 Then
                            sender.SelectedRows.Add(lint_Fila_Aux)
                        End If
                    Next
                Else
                    sender.SelectedRows.Clear()
                    sender.SelectedRows.Add(lint_Fila)
                    int_PrimeraFila = lint_Fila
                End If
            End If
        End If
    End Sub

    Private Sub Tdg_CuentasDisponibles_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Tdg_CuentasDisponibles.MouseUp
        int_UltimaFila = sender.RowContaining(e.Y)
        Dim lint_Fila_Aux As Integer = 0
        Dim lint_Indice As Integer = 0
        Dim lint_Step As Integer = 0

        If int_UltimaFila <> -1 Then
            If int_PrimeraFila <> -1 And int_PrimeraFila <> int_UltimaFila Then
                If int_UltimaFila > int_PrimeraFila Then
                    lint_Step = -1
                Else
                    lint_Step = 1
                End If
                For lint_Fila_Aux = int_UltimaFila To int_PrimeraFila Step lint_Step
                    lint_Indice = sender.SelectedRows.IndexOf(lint_Fila_Aux)
                    If lint_Indice = -1 Then
                        sender.SelectedRows.Add(lint_Fila_Aux)
                    End If
                Next
            End If
        End If
    End Sub

    Private Sub Tdg_CuentasDisponibles_SelChange(ByVal sender As Object, ByVal e As C1.Win.C1TrueDBGrid.CancelEventArgs) Handles Tdg_CuentasDisponibles.SelChange
        e.Cancel = True
    End Sub


    Private Sub Tdg_CuentasAsociadas_FilterChange(ByVal sender As Object, ByVal e As System.EventArgs) Handles Tdg_CuentasAsociadas.FilterChange
        Dim sb As New System.Text.StringBuilder
        Dim dc As C1.Win.C1TrueDBGrid.C1DataColumn
        Try
            sender.SelectedRows.Clear()
            For Each dc In sender.Columns
                If dc.FilterText.Trim.Length > 0 Then
                    If sb.Length > 0 Then
                        sb.Append(" AND ")
                    End If
                    sb.Append(gstrFiltro_X_TipoColumna(dc))
                End If
            Next dc
            '... Realiza el filtro sobre el dataset
            sender.DataSource.DefaultView.RowFilter = sb.ToString
            sender.Refresh()

            If sender.RowCount > 0 Then
                sender.SelectedRows.Add(0)
            End If
        Catch ex As Exception
            Debug.Print(ex.Message)
        End Try
    End Sub

    Private Sub Tdg_CuentasAsociadas_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Tdg_CuentasAsociadas.KeyDown
        If Not e.Shift And Not e.Control Then
            sender.SelectedRows.Clear()
            sender.MarqueeStyle = MarqueeEnum.NoMarquee
            sender.MarqueeStyle = MarqueeEnum.HighlightRow
        Else
            sender.MarqueeStyle = MarqueeEnum.NoMarquee
        End If

        bln_PressControl = e.Control
        bln_PressShift = e.Shift
    End Sub

    Private Sub Tdg_CuentasAsociadas_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Tdg_CuentasAsociadas.KeyUp
        bln_PressControl = False
        bln_PressShift = False
    End Sub

    Private Sub Tdg_CuentasAsociadas_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Tdg_CuentasAsociadas.MouseDown
        lint_Fila = sender.RowContaining(e.Y)
        Dim lint_Fila_Aux As Integer = 0
        Dim lint_Indice As Integer = 0
        Dim lint_Step As Integer = 0

        If lint_Fila <> -1 Then
            If bln_PressControl Then
                int_PrimeraFila = -1
                lint_Indice = sender.SelectedRows.IndexOF(lint_Fila)
                If lint_Indice <> -1 Then
                    sender.SelectedRows.RemoveAt(lint_Indice)
                Else
                    sender.SelectedRows.Add(lint_Fila)
                End If
            Else
                If bln_PressShift Then
                    int_PrimeraFila = -1
                    If int_UltimaFila > lint_Fila Then
                        lint_Step = -1
                    Else
                        lint_Step = 1
                    End If

                    For lint_Fila_Aux = int_UltimaFila To lint_Fila Step lint_Step
                        lint_Indice = sender.SelectedRows.IndexOf(lint_Fila_Aux)
                        If lint_Indice = -1 Then
                            sender.SelectedRows.Add(lint_Fila_Aux)
                        End If
                    Next
                Else
                    sender.SelectedRows.Clear()
                    sender.SelectedRows.Add(lint_Fila)
                    int_PrimeraFila = lint_Fila
                End If
            End If
        End If
    End Sub

    Private Sub Tdg_CuentasAsociadas_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Tdg_CuentasAsociadas.MouseUp
        int_UltimaFila = sender.RowContaining(e.Y)
        Dim lint_Fila_Aux As Integer = 0
        Dim lint_Indice As Integer = 0
        Dim lint_Step As Integer = 0

        If int_UltimaFila <> -1 Then
            If int_PrimeraFila <> -1 And int_PrimeraFila <> int_UltimaFila Then
                If int_UltimaFila > int_PrimeraFila Then
                    lint_Step = -1
                Else
                    lint_Step = 1
                End If
                For lint_Fila_Aux = int_UltimaFila To int_PrimeraFila Step lint_Step
                    lint_Indice = sender.SelectedRows.IndexOf(lint_Fila_Aux)
                    If lint_Indice = -1 Then
                        sender.SelectedRows.Add(lint_Fila_Aux)
                    End If
                Next
            End If
        End If
    End Sub

    Private Sub Tdg_CuentasAsociadas_SelChange(ByVal sender As Object, ByVal e As C1.Win.C1TrueDBGrid.CancelEventArgs) Handles Tdg_CuentasAsociadas.SelChange
        e.Cancel = True
    End Sub


    Private Sub Frm_MantenedorUsuarios_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
        Fnt_Resize_Controls(Me, Me)
    End Sub

End Class