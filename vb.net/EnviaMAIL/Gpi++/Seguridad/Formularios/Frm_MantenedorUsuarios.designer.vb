<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Frm_MantenedorUsuarios
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Frm_MantenedorUsuarios))
        Me.txt_NCuentaBancoOperaciones_b = New C1.Win.C1Input.C1TextBox()
        Me.Pnl_DatosUsuario = New System.Windows.Forms.Panel()
        Me.txtEmailUsuario = New C1.Win.C1Input.C1TextBox()
        Me.lblEmailUsuario = New C1.Win.C1SuperTooltip.C1SuperLabel()
        Me.Cmb_TipoControl = New C1.Win.C1List.C1Combo()
        Me.C1SuperLabel7 = New C1.Win.C1SuperTooltip.C1SuperLabel()
        Me.Cmb_Estado = New C1.Win.C1List.C1Combo()
        Me.Cmb_TipoUsuario = New C1.Win.C1List.C1Combo()
        Me.Cmb_Unidad = New C1.Win.C1List.C1Combo()
        Me.Ded_FechaLiquidacion = New C1.Win.C1Input.C1DateEdit()
        Me.Chk_TodasCuentas = New System.Windows.Forms.CheckBox()
        Me.Txt_Telefono = New C1.Win.C1Input.C1TextBox()
        Me.Txt_CodigoExterno = New C1.Win.C1Input.C1TextBox()
        Me.Txt_Login = New C1.Win.C1Input.C1TextBox()
        Me.Txt_NombreCorto = New C1.Win.C1Input.C1TextBox()
        Me.Txt_Cargo = New C1.Win.C1Input.C1TextBox()
        Me.Txt_NombreUsuario = New C1.Win.C1Input.C1TextBox()
        Me.C1SuperLabel10 = New C1.Win.C1SuperTooltip.C1SuperLabel()
        Me.C1SuperLabel9 = New C1.Win.C1SuperTooltip.C1SuperLabel()
        Me.C1SuperLabel8 = New C1.Win.C1SuperTooltip.C1SuperLabel()
        Me.C1SuperLabel6 = New C1.Win.C1SuperTooltip.C1SuperLabel()
        Me.C1SuperLabel5 = New C1.Win.C1SuperTooltip.C1SuperLabel()
        Me.C1SuperLabel4 = New C1.Win.C1SuperTooltip.C1SuperLabel()
        Me.C1SuperLabel3 = New C1.Win.C1SuperTooltip.C1SuperLabel()
        Me.C1SuperLabel2 = New C1.Win.C1SuperTooltip.C1SuperLabel()
        Me.C1SuperLabel22 = New C1.Win.C1SuperTooltip.C1SuperLabel()
        Me.C1SuperLabel12 = New C1.Win.C1SuperTooltip.C1SuperLabel()
        Me.Btn_VerPerfiles = New C1.Win.C1Input.C1Button()
        Me.Btn_ReiniciaPassword = New C1.Win.C1Input.C1Button()
        Me.Btn_Modificar = New C1.Win.C1Input.C1Button()
        Me.C1SuperLabel14 = New C1.Win.C1SuperTooltip.C1SuperLabel()
        Me.C1SuperLabel1 = New C1.Win.C1SuperTooltip.C1SuperLabel()
        Me.Pnl_Datos_TipoOperacion = New System.Windows.Forms.Panel()
        Me.Tdg_Usuarios = New C1.Win.C1TrueDBGrid.C1TrueDBGrid()
        Me.Pnl_PerfilesUsuarios = New System.Windows.Forms.Panel()
        Me.Cmb_NegocioPerfiles = New C1.Win.C1List.C1Combo()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Tdg_PerfilesAsignados = New C1.Win.C1TrueDBGrid.C1TrueDBGrid()
        Me.Btn_QuitarPerfilUsuario = New C1.Win.C1Input.C1Button()
        Me.Btn_AsociarPerfilUsuario = New C1.Win.C1Input.C1Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Tdg_PerfilesDisponibles = New C1.Win.C1TrueDBGrid.C1TrueDBGrid()
        Me.C1SuperLabel16 = New C1.Win.C1SuperTooltip.C1SuperLabel()
        Me.Tab_Contenedor = New C1.Win.C1Command.C1DockingTab()
        Me.Tab_Usuario = New C1.Win.C1Command.C1DockingTabPage()
        Me.Tab_PerfilesUsuario = New C1.Win.C1Command.C1DockingTabPage()
        Me.Tab_CuentasUsuario = New C1.Win.C1Command.C1DockingTabPage()
        Me.Pnl_CuentasUsuario = New System.Windows.Forms.Panel()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.Cmb_Asesores = New C1.Win.C1List.C1Combo()
        Me.C1SuperLabel21 = New C1.Win.C1SuperTooltip.C1SuperLabel()
        Me.Cmb_Sucursal = New C1.Win.C1List.C1Combo()
        Me.Cmb_Segmento = New C1.Win.C1List.C1Combo()
        Me.Cmb_PerfilRiesgo = New C1.Win.C1List.C1Combo()
        Me.Cmb_TipoAdministracion = New C1.Win.C1List.C1Combo()
        Me.Cmb_NegocioCuentas = New C1.Win.C1List.C1Combo()
        Me.Txt_RutCliente = New C1.Win.C1Input.C1TextBox()
        Me.C1SuperLabel20 = New C1.Win.C1SuperTooltip.C1SuperLabel()
        Me.Txt_NombreCuenta = New C1.Win.C1Input.C1TextBox()
        Me.Btn_BuscarCuentasDisponibles = New C1.Win.C1Input.C1Button()
        Me.Btn_LimpiarFiltroCuentas = New C1.Win.C1Input.C1Button()
        Me.C1SuperLabel19 = New C1.Win.C1SuperTooltip.C1SuperLabel()
        Me.Btn_BuscarCuenta = New C1.Win.C1Input.C1Button()
        Me.Txt_NumeroCuenta = New C1.Win.C1Input.C1TextBox()
        Me.Btn_BuscarCliente = New C1.Win.C1Input.C1Button()
        Me.Txt_NombreCliente = New C1.Win.C1Input.C1TextBox()
        Me.C1SuperLabel11 = New C1.Win.C1SuperTooltip.C1SuperLabel()
        Me.C1SuperLabel37 = New C1.Win.C1SuperTooltip.C1SuperLabel()
        Me.C1SuperLabel13 = New C1.Win.C1SuperTooltip.C1SuperLabel()
        Me.C1SuperLabel40 = New C1.Win.C1SuperTooltip.C1SuperLabel()
        Me.C1SuperLabel15 = New C1.Win.C1SuperTooltip.C1SuperLabel()
        Me.C1SuperLabel18 = New C1.Win.C1SuperTooltip.C1SuperLabel()
        Me.C1SuperLabel17 = New C1.Win.C1SuperTooltip.C1SuperLabel()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Tdg_CuentasAsociadas = New C1.Win.C1TrueDBGrid.C1TrueDBGrid()
        Me.Btn_AsociarCuenta = New C1.Win.C1Input.C1Button()
        Me.Btn_QuitarCuenta = New C1.Win.C1Input.C1Button()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.Tdg_CuentasDisponibles = New C1.Win.C1TrueDBGrid.C1TrueDBGrid()
        Me.C1SuperTooltip1 = New C1.Win.C1SuperTooltip.C1SuperTooltip(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.StatusLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me.Ctrl_Botonera = New System.Windows.Forms.ToolStrip()
        Me.Ctrl_Botonera_Agregar = New System.Windows.Forms.ToolStripButton()
        Me.Ctrl_Botonera_Buscar = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.Ctrl_Botonera_Guardar = New System.Windows.Forms.ToolStripButton()
        Me.Ctrl_Botonera_Editar = New System.Windows.Forms.ToolStripButton()
        Me.Ctrl_Botonera_Eliminar = New System.Windows.Forms.ToolStripButton()
        Me.Ctrl_Botonera_Verificar = New System.Windows.Forms.ToolStripButton()
        Me.Ctrl_Botonera_Procesar = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.Ctrl_Botonera_Imprimir = New System.Windows.Forms.ToolStripButton()
        Me.Ctrl_Botonera_Excel = New System.Windows.Forms.ToolStripButton()
        Me.Ctrl_Botonera_PDF = New System.Windows.Forms.ToolStripButton()
        Me.Ctrl_Botonera_Columnas = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.Ctrl_Botonera_Cancelar = New System.Windows.Forms.ToolStripButton()
        Me.Ctrl_Combo_Plantillas = New System.Windows.Forms.ToolStripComboBox()
        CType(Me.txt_NCuentaBancoOperaciones_b, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Pnl_DatosUsuario.SuspendLayout()
        CType(Me.txtEmailUsuario, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Cmb_TipoControl, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Cmb_Estado, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Cmb_TipoUsuario, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Cmb_Unidad, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Ded_FechaLiquidacion, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Txt_Telefono, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Txt_CodigoExterno, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Txt_Login, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Txt_NombreCorto, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Txt_Cargo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Txt_NombreUsuario, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Pnl_Datos_TipoOperacion.SuspendLayout()
        CType(Me.Tdg_Usuarios, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Pnl_PerfilesUsuarios.SuspendLayout()
        CType(Me.Cmb_NegocioPerfiles, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.Tdg_PerfilesAsignados, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.Tdg_PerfilesDisponibles, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Tab_Contenedor, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Tab_Contenedor.SuspendLayout()
        Me.Tab_Usuario.SuspendLayout()
        Me.Tab_PerfilesUsuario.SuspendLayout()
        Me.Tab_CuentasUsuario.SuspendLayout()
        Me.Pnl_CuentasUsuario.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        CType(Me.Cmb_Asesores, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Cmb_Sucursal, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Cmb_Segmento, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Cmb_PerfilRiesgo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Cmb_TipoAdministracion, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Cmb_NegocioCuentas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Txt_RutCliente, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Txt_NombreCuenta, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Txt_NumeroCuenta, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Txt_NombreCliente, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        CType(Me.Tdg_CuentasAsociadas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox4.SuspendLayout()
        CType(Me.Tdg_CuentasDisponibles, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.Ctrl_Botonera.SuspendLayout()
        Me.SuspendLayout()
        '
        'txt_NCuentaBancoOperaciones_b
        '
        Me.txt_NCuentaBancoOperaciones_b.Font = New System.Drawing.Font("Verdana", 6.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_NCuentaBancoOperaciones_b.Location = New System.Drawing.Point(5, 64)
        Me.txt_NCuentaBancoOperaciones_b.Name = "txt_NCuentaBancoOperaciones_b"
        Me.txt_NCuentaBancoOperaciones_b.Size = New System.Drawing.Size(150, 18)
        Me.txt_NCuentaBancoOperaciones_b.TabIndex = 94
        Me.txt_NCuentaBancoOperaciones_b.Tag = Nothing
        '
        'Pnl_DatosUsuario
        '
        Me.Pnl_DatosUsuario.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Pnl_DatosUsuario.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Pnl_DatosUsuario.Controls.Add(Me.txtEmailUsuario)
        Me.Pnl_DatosUsuario.Controls.Add(Me.lblEmailUsuario)
        Me.Pnl_DatosUsuario.Controls.Add(Me.Cmb_TipoControl)
        Me.Pnl_DatosUsuario.Controls.Add(Me.C1SuperLabel7)
        Me.Pnl_DatosUsuario.Controls.Add(Me.Cmb_Estado)
        Me.Pnl_DatosUsuario.Controls.Add(Me.Cmb_TipoUsuario)
        Me.Pnl_DatosUsuario.Controls.Add(Me.Cmb_Unidad)
        Me.Pnl_DatosUsuario.Controls.Add(Me.Ded_FechaLiquidacion)
        Me.Pnl_DatosUsuario.Controls.Add(Me.Chk_TodasCuentas)
        Me.Pnl_DatosUsuario.Controls.Add(Me.Txt_Telefono)
        Me.Pnl_DatosUsuario.Controls.Add(Me.Txt_CodigoExterno)
        Me.Pnl_DatosUsuario.Controls.Add(Me.Txt_Login)
        Me.Pnl_DatosUsuario.Controls.Add(Me.Txt_NombreCorto)
        Me.Pnl_DatosUsuario.Controls.Add(Me.Txt_Cargo)
        Me.Pnl_DatosUsuario.Controls.Add(Me.Txt_NombreUsuario)
        Me.Pnl_DatosUsuario.Controls.Add(Me.C1SuperLabel10)
        Me.Pnl_DatosUsuario.Controls.Add(Me.C1SuperLabel9)
        Me.Pnl_DatosUsuario.Controls.Add(Me.C1SuperLabel8)
        Me.Pnl_DatosUsuario.Controls.Add(Me.C1SuperLabel6)
        Me.Pnl_DatosUsuario.Controls.Add(Me.C1SuperLabel5)
        Me.Pnl_DatosUsuario.Controls.Add(Me.C1SuperLabel4)
        Me.Pnl_DatosUsuario.Controls.Add(Me.C1SuperLabel3)
        Me.Pnl_DatosUsuario.Controls.Add(Me.C1SuperLabel2)
        Me.Pnl_DatosUsuario.Controls.Add(Me.C1SuperLabel22)
        Me.Pnl_DatosUsuario.Controls.Add(Me.C1SuperLabel12)
        Me.Pnl_DatosUsuario.Location = New System.Drawing.Point(0, 2)
        Me.Pnl_DatosUsuario.Name = "Pnl_DatosUsuario"
        Me.Pnl_DatosUsuario.Size = New System.Drawing.Size(747, 290)
        Me.Pnl_DatosUsuario.TabIndex = 1
        '
        'txtEmailUsuario
        '
        Me.txtEmailUsuario.AcceptsEscape = False
        Me.txtEmailUsuario.AcceptsReturn = True
        Me.txtEmailUsuario.AcceptsTab = True
        Me.txtEmailUsuario.AutoSize = False
        Me.txtEmailUsuario.BackColor = System.Drawing.Color.White
        Me.txtEmailUsuario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtEmailUsuario.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtEmailUsuario.DisabledForeColor = System.Drawing.Color.Lavender
        Me.txtEmailUsuario.Font = New System.Drawing.Font("Arial", 7.0!, System.Drawing.FontStyle.Bold)
        Me.txtEmailUsuario.ForeColor = System.Drawing.Color.SteelBlue
        Me.txtEmailUsuario.Location = New System.Drawing.Point(334, 148)
        Me.txtEmailUsuario.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.txtEmailUsuario.MaxLength = 50
        Me.txtEmailUsuario.Name = "txtEmailUsuario"
        Me.txtEmailUsuario.NumericInput = False
        Me.txtEmailUsuario.ReadOnly = True
        Me.txtEmailUsuario.Size = New System.Drawing.Size(340, 16)
        Me.txtEmailUsuario.TabIndex = 1057
        Me.txtEmailUsuario.TabStop = False
        Me.txtEmailUsuario.Tag = Nothing
        Me.txtEmailUsuario.TextDetached = True
        Me.txtEmailUsuario.Value = " "
        Me.txtEmailUsuario.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Silver
        '
        'lblEmailUsuario
        '
        Me.lblEmailUsuario.AutoSize = True
        Me.lblEmailUsuario.BackColor = System.Drawing.Color.Transparent
        Me.lblEmailUsuario.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.lblEmailUsuario.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.lblEmailUsuario.Location = New System.Drawing.Point(332, 132)
        Me.lblEmailUsuario.Margin = New System.Windows.Forms.Padding(2, 4, 2, 4)
        Me.lblEmailUsuario.Name = "lblEmailUsuario"
        Me.lblEmailUsuario.Size = New System.Drawing.Size(34, 19)
        Me.lblEmailUsuario.TabIndex = 1058
        Me.lblEmailUsuario.Text = "Email"
        Me.lblEmailUsuario.UseMnemonic = True
        '
        'Cmb_TipoControl
        '
        Me.Cmb_TipoControl.AddItemSeparator = Global.Microsoft.VisualBasic.ChrW(59)
        Me.Cmb_TipoControl.AllowColMove = False
        Me.Cmb_TipoControl.AllowSort = False
        Me.Cmb_TipoControl.AutoDropDown = True
        Me.Cmb_TipoControl.AutoSelect = True
        Me.Cmb_TipoControl.AutoSize = False
        Me.Cmb_TipoControl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Cmb_TipoControl.Caption = ""
        Me.Cmb_TipoControl.CaptionHeight = 18
        Me.Cmb_TipoControl.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.Cmb_TipoControl.ColumnCaptionHeight = 18
        Me.Cmb_TipoControl.ColumnFooterHeight = 18
        Me.Cmb_TipoControl.ColumnHeaders = False
        Me.Cmb_TipoControl.ComboStyle = C1.Win.C1List.ComboStyleEnum.DropdownList
        Me.Cmb_TipoControl.ContentHeight = 12
        Me.Cmb_TipoControl.DeadAreaBackColor = System.Drawing.Color.Empty
        Me.Cmb_TipoControl.EditorBackColor = System.Drawing.Color.White
        Me.Cmb_TipoControl.EditorFont = New System.Drawing.Font("Arial", 7.0!, System.Drawing.FontStyle.Bold)
        Me.Cmb_TipoControl.EditorForeColor = System.Drawing.Color.SteelBlue
        Me.Cmb_TipoControl.EditorHeight = 12
        Me.Cmb_TipoControl.ExtendRightColumn = True
        Me.Cmb_TipoControl.FlatStyle = C1.Win.C1List.FlatModeEnum.Flat
        Me.Cmb_TipoControl.Font = New System.Drawing.Font("Arial", 7.0!, System.Drawing.FontStyle.Bold)
        Me.Cmb_TipoControl.Images.Add(CType(resources.GetObject("Cmb_TipoControl.Images"), System.Drawing.Image))
        Me.Cmb_TipoControl.ItemHeight = 18
        Me.Cmb_TipoControl.Location = New System.Drawing.Point(9, 148)
        Me.Cmb_TipoControl.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Cmb_TipoControl.MatchEntry = C1.Win.C1List.MatchEntryEnum.Extended
        Me.Cmb_TipoControl.MatchEntryTimeout = CType(500, Long)
        Me.Cmb_TipoControl.MaxDropDownItems = CType(5, Short)
        Me.Cmb_TipoControl.MaxLength = 0
        Me.Cmb_TipoControl.MinimumSize = New System.Drawing.Size(10, 16)
        Me.Cmb_TipoControl.MouseCursor = System.Windows.Forms.Cursors.Default
        Me.Cmb_TipoControl.Name = "Cmb_TipoControl"
        Me.Cmb_TipoControl.RowSubDividerColor = System.Drawing.Color.DarkGray
        Me.Cmb_TipoControl.Size = New System.Drawing.Size(320, 16)
        Me.Cmb_TipoControl.TabIndex = 8
        Me.Cmb_TipoControl.VisualStyle = C1.Win.C1List.VisualStyle.Office2007Silver
        Me.Cmb_TipoControl.PropBag = resources.GetString("Cmb_TipoControl.PropBag")
        '
        'C1SuperLabel7
        '
        Me.C1SuperLabel7.AutoSize = True
        Me.C1SuperLabel7.BackColor = System.Drawing.Color.Transparent
        Me.C1SuperLabel7.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.C1SuperLabel7.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.C1SuperLabel7.Location = New System.Drawing.Point(6, 132)
        Me.C1SuperLabel7.Margin = New System.Windows.Forms.Padding(2, 4, 2, 4)
        Me.C1SuperLabel7.Name = "C1SuperLabel7"
        Me.C1SuperLabel7.Size = New System.Drawing.Size(80, 19)
        Me.C1SuperLabel7.TabIndex = 1056
        Me.C1SuperLabel7.Text = "Tipo de Control"
        Me.C1SuperLabel7.UseMnemonic = True
        '
        'Cmb_Estado
        '
        Me.Cmb_Estado.AddItemSeparator = Global.Microsoft.VisualBasic.ChrW(59)
        Me.Cmb_Estado.AllowColMove = False
        Me.Cmb_Estado.AllowSort = False
        Me.Cmb_Estado.AutoDropDown = True
        Me.Cmb_Estado.AutoSelect = True
        Me.Cmb_Estado.AutoSize = False
        Me.Cmb_Estado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Cmb_Estado.Caption = ""
        Me.Cmb_Estado.CaptionHeight = 18
        Me.Cmb_Estado.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.Cmb_Estado.ColumnCaptionHeight = 18
        Me.Cmb_Estado.ColumnFooterHeight = 18
        Me.Cmb_Estado.ColumnHeaders = False
        Me.Cmb_Estado.ComboStyle = C1.Win.C1List.ComboStyleEnum.DropdownList
        Me.Cmb_Estado.ContentHeight = 12
        Me.Cmb_Estado.DataMode = C1.Win.C1List.DataModeEnum.AddItem
        Me.Cmb_Estado.DeadAreaBackColor = System.Drawing.Color.Empty
        Me.Cmb_Estado.EditorBackColor = System.Drawing.Color.White
        Me.Cmb_Estado.EditorFont = New System.Drawing.Font("Arial", 7.0!, System.Drawing.FontStyle.Bold)
        Me.Cmb_Estado.EditorForeColor = System.Drawing.Color.SteelBlue
        Me.Cmb_Estado.EditorHeight = 12
        Me.Cmb_Estado.ExtendRightColumn = True
        Me.Cmb_Estado.FlatStyle = C1.Win.C1List.FlatModeEnum.Flat
        Me.Cmb_Estado.Font = New System.Drawing.Font("Arial", 7.0!, System.Drawing.FontStyle.Bold)
        Me.Cmb_Estado.Images.Add(CType(resources.GetObject("Cmb_Estado.Images"), System.Drawing.Image))
        Me.Cmb_Estado.ItemHeight = 18
        Me.Cmb_Estado.Location = New System.Drawing.Point(8, 181)
        Me.Cmb_Estado.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Cmb_Estado.MatchEntry = C1.Win.C1List.MatchEntryEnum.Extended
        Me.Cmb_Estado.MatchEntryTimeout = CType(500, Long)
        Me.Cmb_Estado.MaxDropDownItems = CType(5, Short)
        Me.Cmb_Estado.MaxLength = 0
        Me.Cmb_Estado.MinimumSize = New System.Drawing.Size(10, 16)
        Me.Cmb_Estado.MouseCursor = System.Windows.Forms.Cursors.Default
        Me.Cmb_Estado.Name = "Cmb_Estado"
        Me.Cmb_Estado.RowSubDividerColor = System.Drawing.Color.DarkGray
        Me.Cmb_Estado.Size = New System.Drawing.Size(200, 16)
        Me.Cmb_Estado.TabIndex = 14
        Me.Cmb_Estado.VisualStyle = C1.Win.C1List.VisualStyle.Office2007Silver
        Me.Cmb_Estado.PropBag = resources.GetString("Cmb_Estado.PropBag")
        '
        'Cmb_TipoUsuario
        '
        Me.Cmb_TipoUsuario.AddItemSeparator = Global.Microsoft.VisualBasic.ChrW(59)
        Me.Cmb_TipoUsuario.AllowColMove = False
        Me.Cmb_TipoUsuario.AllowSort = False
        Me.Cmb_TipoUsuario.AutoDropDown = True
        Me.Cmb_TipoUsuario.AutoSelect = True
        Me.Cmb_TipoUsuario.AutoSize = False
        Me.Cmb_TipoUsuario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Cmb_TipoUsuario.Caption = ""
        Me.Cmb_TipoUsuario.CaptionHeight = 18
        Me.Cmb_TipoUsuario.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.Cmb_TipoUsuario.ColumnCaptionHeight = 18
        Me.Cmb_TipoUsuario.ColumnFooterHeight = 18
        Me.Cmb_TipoUsuario.ColumnHeaders = False
        Me.Cmb_TipoUsuario.ComboStyle = C1.Win.C1List.ComboStyleEnum.DropdownList
        Me.Cmb_TipoUsuario.ContentHeight = 12
        Me.Cmb_TipoUsuario.DeadAreaBackColor = System.Drawing.Color.Empty
        Me.Cmb_TipoUsuario.EditorBackColor = System.Drawing.Color.White
        Me.Cmb_TipoUsuario.EditorFont = New System.Drawing.Font("Arial", 7.0!, System.Drawing.FontStyle.Bold)
        Me.Cmb_TipoUsuario.EditorForeColor = System.Drawing.Color.SteelBlue
        Me.Cmb_TipoUsuario.EditorHeight = 12
        Me.Cmb_TipoUsuario.ExtendRightColumn = True
        Me.Cmb_TipoUsuario.FlatStyle = C1.Win.C1List.FlatModeEnum.Flat
        Me.Cmb_TipoUsuario.Font = New System.Drawing.Font("Arial", 7.0!, System.Drawing.FontStyle.Bold)
        Me.Cmb_TipoUsuario.Images.Add(CType(resources.GetObject("Cmb_TipoUsuario.Images"), System.Drawing.Image))
        Me.Cmb_TipoUsuario.ItemHeight = 18
        Me.Cmb_TipoUsuario.Location = New System.Drawing.Point(8, 84)
        Me.Cmb_TipoUsuario.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Cmb_TipoUsuario.MatchEntry = C1.Win.C1List.MatchEntryEnum.Extended
        Me.Cmb_TipoUsuario.MatchEntryTimeout = CType(500, Long)
        Me.Cmb_TipoUsuario.MaxDropDownItems = CType(5, Short)
        Me.Cmb_TipoUsuario.MaxLength = 0
        Me.Cmb_TipoUsuario.MinimumSize = New System.Drawing.Size(10, 16)
        Me.Cmb_TipoUsuario.MouseCursor = System.Windows.Forms.Cursors.Default
        Me.Cmb_TipoUsuario.Name = "Cmb_TipoUsuario"
        Me.Cmb_TipoUsuario.RowSubDividerColor = System.Drawing.Color.DarkGray
        Me.Cmb_TipoUsuario.Size = New System.Drawing.Size(320, 16)
        Me.Cmb_TipoUsuario.TabIndex = 4
        Me.Cmb_TipoUsuario.VisualStyle = C1.Win.C1List.VisualStyle.Office2007Silver
        Me.Cmb_TipoUsuario.PropBag = resources.GetString("Cmb_TipoUsuario.PropBag")
        '
        'Cmb_Unidad
        '
        Me.Cmb_Unidad.AddItemSeparator = Global.Microsoft.VisualBasic.ChrW(59)
        Me.Cmb_Unidad.AllowColMove = False
        Me.Cmb_Unidad.AllowSort = False
        Me.Cmb_Unidad.AutoDropDown = True
        Me.Cmb_Unidad.AutoSelect = True
        Me.Cmb_Unidad.AutoSize = False
        Me.Cmb_Unidad.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Cmb_Unidad.Caption = ""
        Me.Cmb_Unidad.CaptionHeight = 18
        Me.Cmb_Unidad.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.Cmb_Unidad.ColumnCaptionHeight = 18
        Me.Cmb_Unidad.ColumnFooterHeight = 18
        Me.Cmb_Unidad.ColumnHeaders = False
        Me.Cmb_Unidad.ComboStyle = C1.Win.C1List.ComboStyleEnum.DropdownList
        Me.Cmb_Unidad.ContentHeight = 12
        Me.Cmb_Unidad.DeadAreaBackColor = System.Drawing.Color.Empty
        Me.Cmb_Unidad.EditorBackColor = System.Drawing.Color.White
        Me.Cmb_Unidad.EditorFont = New System.Drawing.Font("Arial", 7.0!, System.Drawing.FontStyle.Bold)
        Me.Cmb_Unidad.EditorForeColor = System.Drawing.Color.SteelBlue
        Me.Cmb_Unidad.EditorHeight = 12
        Me.Cmb_Unidad.ExtendRightColumn = True
        Me.Cmb_Unidad.FlatStyle = C1.Win.C1List.FlatModeEnum.Flat
        Me.Cmb_Unidad.Font = New System.Drawing.Font("Arial", 7.0!, System.Drawing.FontStyle.Bold)
        Me.Cmb_Unidad.Images.Add(CType(resources.GetObject("Cmb_Unidad.Images"), System.Drawing.Image))
        Me.Cmb_Unidad.ItemHeight = 18
        Me.Cmb_Unidad.Location = New System.Drawing.Point(8, 52)
        Me.Cmb_Unidad.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Cmb_Unidad.MatchEntry = C1.Win.C1List.MatchEntryEnum.Extended
        Me.Cmb_Unidad.MatchEntryTimeout = CType(500, Long)
        Me.Cmb_Unidad.MaxDropDownItems = CType(5, Short)
        Me.Cmb_Unidad.MaxLength = 0
        Me.Cmb_Unidad.MinimumSize = New System.Drawing.Size(10, 16)
        Me.Cmb_Unidad.MouseCursor = System.Windows.Forms.Cursors.Default
        Me.Cmb_Unidad.Name = "Cmb_Unidad"
        Me.Cmb_Unidad.RowSubDividerColor = System.Drawing.Color.DarkGray
        Me.Cmb_Unidad.Size = New System.Drawing.Size(320, 16)
        Me.Cmb_Unidad.TabIndex = 2
        Me.Cmb_Unidad.VisualStyle = C1.Win.C1List.VisualStyle.Office2007Silver
        Me.Cmb_Unidad.PropBag = resources.GetString("Cmb_Unidad.PropBag")
        '
        'Ded_FechaLiquidacion
        '
        Me.Ded_FechaLiquidacion.AcceptsEscape = False
        Me.Ded_FechaLiquidacion.AutoSize = False
        Me.Ded_FechaLiquidacion.BackColor = System.Drawing.Color.White
        Me.Ded_FechaLiquidacion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Ded_FechaLiquidacion.ButtonWidth = 14
        '
        '
        '
        Me.Ded_FechaLiquidacion.Calendar.AccessibleDescription = ""
        Me.Ded_FechaLiquidacion.Calendar.ArrowColor = System.Drawing.Color.SteelBlue
        Me.Ded_FechaLiquidacion.Calendar.BackColor = System.Drawing.Color.White
        Me.Ded_FechaLiquidacion.Calendar.CaptionFormat = ""
        Me.Ded_FechaLiquidacion.Calendar.ClearText = "&Limpiar"
        Me.Ded_FechaLiquidacion.Calendar.DayNameLength = 3
        Me.Ded_FechaLiquidacion.Calendar.DayNamesColor = System.Drawing.Color.DarkSlateGray
        Me.Ded_FechaLiquidacion.Calendar.DayNamesFont = New System.Drawing.Font("Arial", 7.0!, System.Drawing.FontStyle.Bold)
        Me.Ded_FechaLiquidacion.Calendar.Font = New System.Drawing.Font("Arial", 7.0!, System.Drawing.FontStyle.Bold)
        Me.Ded_FechaLiquidacion.Calendar.ForeColor = System.Drawing.Color.SteelBlue
        Me.Ded_FechaLiquidacion.Calendar.MinDate = New Date(1990, 1, 1, 0, 0, 0, 0)
        Me.Ded_FechaLiquidacion.Calendar.SelectionForeColor = System.Drawing.Color.SteelBlue
        Me.Ded_FechaLiquidacion.Calendar.ShowClearButton = False
        Me.Ded_FechaLiquidacion.Calendar.ShowToday = True
        Me.Ded_FechaLiquidacion.Calendar.ShowTodayButton = False
        Me.Ded_FechaLiquidacion.Calendar.TitleBackColor = System.Drawing.Color.WhiteSmoke
        Me.Ded_FechaLiquidacion.Calendar.TitleFont = New System.Drawing.Font("Arial", 7.0!)
        Me.Ded_FechaLiquidacion.Calendar.TitleForeColor = System.Drawing.Color.DarkSlateGray
        Me.Ded_FechaLiquidacion.Calendar.TodayText = "&Hoy"
        Me.Ded_FechaLiquidacion.Calendar.TrailingForeColor = System.Drawing.Color.DarkSlateGray
        Me.Ded_FechaLiquidacion.Calendar.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Silver
        Me.Ded_FechaLiquidacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.Ded_FechaLiquidacion.CustomFormat = "dd-MM-yyyy"
        Me.Ded_FechaLiquidacion.DisabledForeColor = System.Drawing.Color.White
        Me.Ded_FechaLiquidacion.DisplayFormat.CustomFormat = "dd-MM-yyyy"
        Me.Ded_FechaLiquidacion.DisplayFormat.FormatType = C1.Win.C1Input.FormatTypeEnum.CustomFormat
        Me.Ded_FechaLiquidacion.DisplayFormat.Inherit = CType((((C1.Win.C1Input.FormatInfoInheritFlags.NullText Or C1.Win.C1Input.FormatInfoInheritFlags.EmptyAsNull) _
            Or C1.Win.C1Input.FormatInfoInheritFlags.TrimStart) _
            Or C1.Win.C1Input.FormatInfoInheritFlags.TrimEnd), C1.Win.C1Input.FormatInfoInheritFlags)
        Me.Ded_FechaLiquidacion.EditFormat.CustomFormat = "dd-MM-yyyy"
        Me.Ded_FechaLiquidacion.EditFormat.FormatType = C1.Win.C1Input.FormatTypeEnum.CustomFormat
        Me.Ded_FechaLiquidacion.EditFormat.Inherit = CType((((C1.Win.C1Input.FormatInfoInheritFlags.NullText Or C1.Win.C1Input.FormatInfoInheritFlags.EmptyAsNull) _
            Or C1.Win.C1Input.FormatInfoInheritFlags.TrimStart) _
            Or C1.Win.C1Input.FormatInfoInheritFlags.TrimEnd), C1.Win.C1Input.FormatInfoInheritFlags)
        Me.Ded_FechaLiquidacion.Font = New System.Drawing.Font("Arial", 7.0!, System.Drawing.FontStyle.Bold)
        Me.Ded_FechaLiquidacion.ForeColor = System.Drawing.Color.SteelBlue
        Me.Ded_FechaLiquidacion.FormatType = C1.Win.C1Input.FormatTypeEnum.CustomFormat
        Me.Ded_FechaLiquidacion.InitialSelection = C1.Win.C1Input.InitialSelectionEnum.CaretAtStart
        Me.Ded_FechaLiquidacion.Location = New System.Drawing.Point(211, 181)
        Me.Ded_FechaLiquidacion.Margin = New System.Windows.Forms.Padding(0)
        Me.Ded_FechaLiquidacion.MaxLength = 10
        Me.Ded_FechaLiquidacion.Name = "Ded_FechaLiquidacion"
        Me.Ded_FechaLiquidacion.Size = New System.Drawing.Size(117, 16)
        Me.Ded_FechaLiquidacion.TabIndex = 15
        Me.Ded_FechaLiquidacion.Tag = Nothing
        Me.Ded_FechaLiquidacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.Ded_FechaLiquidacion.VerticalAlign = C1.Win.C1Input.VerticalAlignEnum.Middle
        Me.Ded_FechaLiquidacion.VisibleButtons = C1.Win.C1Input.DropDownControlButtonFlags.DropDown
        Me.Ded_FechaLiquidacion.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Silver
        '
        'Chk_TodasCuentas
        '
        Me.Chk_TodasCuentas.Font = New System.Drawing.Font("Arial", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Chk_TodasCuentas.ForeColor = System.Drawing.Color.SteelBlue
        Me.Chk_TodasCuentas.Location = New System.Drawing.Point(331, 182)
        Me.Chk_TodasCuentas.Name = "Chk_TodasCuentas"
        Me.Chk_TodasCuentas.Size = New System.Drawing.Size(179, 16)
        Me.Chk_TodasCuentas.TabIndex = 13
        Me.Chk_TodasCuentas.Text = "Habilitar para Todas las Cuentas"
        Me.Chk_TodasCuentas.UseVisualStyleBackColor = True
        '
        'Txt_Telefono
        '
        Me.Txt_Telefono.AcceptsEscape = False
        Me.Txt_Telefono.AcceptsReturn = True
        Me.Txt_Telefono.AcceptsTab = True
        Me.Txt_Telefono.AutoSize = False
        Me.Txt_Telefono.BackColor = System.Drawing.Color.White
        Me.Txt_Telefono.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Txt_Telefono.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.Txt_Telefono.DisabledForeColor = System.Drawing.Color.Lavender
        Me.Txt_Telefono.Font = New System.Drawing.Font("Arial", 7.0!, System.Drawing.FontStyle.Bold)
        Me.Txt_Telefono.ForeColor = System.Drawing.Color.SteelBlue
        Me.Txt_Telefono.Location = New System.Drawing.Point(333, 84)
        Me.Txt_Telefono.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.Txt_Telefono.MaxLength = 30
        Me.Txt_Telefono.Name = "Txt_Telefono"
        Me.Txt_Telefono.NumericInput = False
        Me.Txt_Telefono.ReadOnly = True
        Me.Txt_Telefono.Size = New System.Drawing.Size(340, 16)
        Me.Txt_Telefono.TabIndex = 5
        Me.Txt_Telefono.TabStop = False
        Me.Txt_Telefono.Tag = Nothing
        Me.Txt_Telefono.TextDetached = True
        Me.Txt_Telefono.Value = " "
        Me.Txt_Telefono.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Silver
        '
        'Txt_CodigoExterno
        '
        Me.Txt_CodigoExterno.AcceptsEscape = False
        Me.Txt_CodigoExterno.AcceptsReturn = True
        Me.Txt_CodigoExterno.AcceptsTab = True
        Me.Txt_CodigoExterno.AutoSize = False
        Me.Txt_CodigoExterno.BackColor = System.Drawing.Color.White
        Me.Txt_CodigoExterno.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Txt_CodigoExterno.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.Txt_CodigoExterno.DisabledForeColor = System.Drawing.Color.Lavender
        Me.Txt_CodigoExterno.Font = New System.Drawing.Font("Arial", 7.0!, System.Drawing.FontStyle.Bold)
        Me.Txt_CodigoExterno.ForeColor = System.Drawing.Color.SteelBlue
        Me.Txt_CodigoExterno.Location = New System.Drawing.Point(333, 116)
        Me.Txt_CodigoExterno.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.Txt_CodigoExterno.MaxLength = 20
        Me.Txt_CodigoExterno.Name = "Txt_CodigoExterno"
        Me.Txt_CodigoExterno.NumericInput = False
        Me.Txt_CodigoExterno.ReadOnly = True
        Me.Txt_CodigoExterno.Size = New System.Drawing.Size(340, 16)
        Me.Txt_CodigoExterno.TabIndex = 7
        Me.Txt_CodigoExterno.TabStop = False
        Me.Txt_CodigoExterno.Tag = Nothing
        Me.Txt_CodigoExterno.TextDetached = True
        Me.Txt_CodigoExterno.Value = " "
        Me.Txt_CodigoExterno.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Silver
        '
        'Txt_Login
        '
        Me.Txt_Login.AcceptsEscape = False
        Me.Txt_Login.AcceptsReturn = True
        Me.Txt_Login.AcceptsTab = True
        Me.Txt_Login.AutoSize = False
        Me.Txt_Login.BackColor = System.Drawing.Color.White
        Me.Txt_Login.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Txt_Login.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.Txt_Login.DisabledForeColor = System.Drawing.Color.Lavender
        Me.Txt_Login.Font = New System.Drawing.Font("Arial", 7.0!, System.Drawing.FontStyle.Bold)
        Me.Txt_Login.ForeColor = System.Drawing.Color.SteelBlue
        Me.Txt_Login.Location = New System.Drawing.Point(333, 52)
        Me.Txt_Login.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.Txt_Login.MaxLength = 15
        Me.Txt_Login.Name = "Txt_Login"
        Me.Txt_Login.NumericInput = False
        Me.Txt_Login.ReadOnly = True
        Me.Txt_Login.Size = New System.Drawing.Size(340, 16)
        Me.Txt_Login.TabIndex = 3
        Me.Txt_Login.TabStop = False
        Me.Txt_Login.Tag = Nothing
        Me.Txt_Login.TextDetached = True
        Me.Txt_Login.Value = " "
        Me.Txt_Login.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Silver
        '
        'Txt_NombreCorto
        '
        Me.Txt_NombreCorto.AcceptsEscape = False
        Me.Txt_NombreCorto.AcceptsReturn = True
        Me.Txt_NombreCorto.AcceptsTab = True
        Me.Txt_NombreCorto.AutoSize = False
        Me.Txt_NombreCorto.BackColor = System.Drawing.Color.White
        Me.Txt_NombreCorto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Txt_NombreCorto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.Txt_NombreCorto.DisabledForeColor = System.Drawing.Color.Lavender
        Me.Txt_NombreCorto.Font = New System.Drawing.Font("Arial", 7.0!, System.Drawing.FontStyle.Bold)
        Me.Txt_NombreCorto.ForeColor = System.Drawing.Color.SteelBlue
        Me.Txt_NombreCorto.Location = New System.Drawing.Point(333, 20)
        Me.Txt_NombreCorto.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.Txt_NombreCorto.MaxLength = 15
        Me.Txt_NombreCorto.Name = "Txt_NombreCorto"
        Me.Txt_NombreCorto.NumericInput = False
        Me.Txt_NombreCorto.ReadOnly = True
        Me.Txt_NombreCorto.Size = New System.Drawing.Size(340, 16)
        Me.Txt_NombreCorto.TabIndex = 1
        Me.Txt_NombreCorto.TabStop = False
        Me.Txt_NombreCorto.Tag = Nothing
        Me.Txt_NombreCorto.TextDetached = True
        Me.Txt_NombreCorto.Value = " "
        Me.Txt_NombreCorto.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Silver
        '
        'Txt_Cargo
        '
        Me.Txt_Cargo.AcceptsEscape = False
        Me.Txt_Cargo.AcceptsReturn = True
        Me.Txt_Cargo.AcceptsTab = True
        Me.Txt_Cargo.AutoSize = False
        Me.Txt_Cargo.BackColor = System.Drawing.Color.White
        Me.Txt_Cargo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Txt_Cargo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.Txt_Cargo.DisabledForeColor = System.Drawing.Color.Lavender
        Me.Txt_Cargo.Font = New System.Drawing.Font("Arial", 7.0!, System.Drawing.FontStyle.Bold)
        Me.Txt_Cargo.ForeColor = System.Drawing.Color.SteelBlue
        Me.Txt_Cargo.Location = New System.Drawing.Point(8, 116)
        Me.Txt_Cargo.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.Txt_Cargo.MaxLength = 30
        Me.Txt_Cargo.Name = "Txt_Cargo"
        Me.Txt_Cargo.NumericInput = False
        Me.Txt_Cargo.ReadOnly = True
        Me.Txt_Cargo.Size = New System.Drawing.Size(320, 16)
        Me.Txt_Cargo.TabIndex = 6
        Me.Txt_Cargo.TabStop = False
        Me.Txt_Cargo.Tag = Nothing
        Me.Txt_Cargo.TextDetached = True
        Me.Txt_Cargo.Value = " "
        Me.Txt_Cargo.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Silver
        '
        'Txt_NombreUsuario
        '
        Me.Txt_NombreUsuario.AcceptsEscape = False
        Me.Txt_NombreUsuario.AcceptsReturn = True
        Me.Txt_NombreUsuario.AcceptsTab = True
        Me.Txt_NombreUsuario.AutoSize = False
        Me.Txt_NombreUsuario.BackColor = System.Drawing.Color.White
        Me.Txt_NombreUsuario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Txt_NombreUsuario.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.Txt_NombreUsuario.DisabledForeColor = System.Drawing.Color.Lavender
        Me.Txt_NombreUsuario.Font = New System.Drawing.Font("Arial", 7.0!, System.Drawing.FontStyle.Bold)
        Me.Txt_NombreUsuario.ForeColor = System.Drawing.Color.SteelBlue
        Me.Txt_NombreUsuario.Location = New System.Drawing.Point(8, 20)
        Me.Txt_NombreUsuario.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.Txt_NombreUsuario.MaxLength = 50
        Me.Txt_NombreUsuario.Name = "Txt_NombreUsuario"
        Me.Txt_NombreUsuario.NumericInput = False
        Me.Txt_NombreUsuario.ReadOnly = True
        Me.Txt_NombreUsuario.Size = New System.Drawing.Size(320, 16)
        Me.Txt_NombreUsuario.TabIndex = 0
        Me.Txt_NombreUsuario.TabStop = False
        Me.Txt_NombreUsuario.Tag = Nothing
        Me.Txt_NombreUsuario.TextDetached = True
        Me.Txt_NombreUsuario.Value = " "
        Me.Txt_NombreUsuario.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Silver
        '
        'C1SuperLabel10
        '
        Me.C1SuperLabel10.AutoSize = True
        Me.C1SuperLabel10.BackColor = System.Drawing.Color.Transparent
        Me.C1SuperLabel10.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.C1SuperLabel10.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.C1SuperLabel10.Location = New System.Drawing.Point(331, 36)
        Me.C1SuperLabel10.Margin = New System.Windows.Forms.Padding(2, 4, 2, 4)
        Me.C1SuperLabel10.Name = "C1SuperLabel10"
        Me.C1SuperLabel10.Size = New System.Drawing.Size(33, 19)
        Me.C1SuperLabel10.TabIndex = 1052
        Me.C1SuperLabel10.Text = "Login"
        Me.C1SuperLabel10.UseMnemonic = True
        '
        'C1SuperLabel9
        '
        Me.C1SuperLabel9.AutoSize = True
        Me.C1SuperLabel9.BackColor = System.Drawing.Color.Transparent
        Me.C1SuperLabel9.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.C1SuperLabel9.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.C1SuperLabel9.Location = New System.Drawing.Point(331, 68)
        Me.C1SuperLabel9.Margin = New System.Windows.Forms.Padding(2, 4, 2, 4)
        Me.C1SuperLabel9.Name = "C1SuperLabel9"
        Me.C1SuperLabel9.Size = New System.Drawing.Size(51, 19)
        Me.C1SuperLabel9.TabIndex = 1051
        Me.C1SuperLabel9.Text = "Teléfono "
        Me.C1SuperLabel9.UseMnemonic = True
        '
        'C1SuperLabel8
        '
        Me.C1SuperLabel8.AutoSize = True
        Me.C1SuperLabel8.BackColor = System.Drawing.Color.Transparent
        Me.C1SuperLabel8.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.C1SuperLabel8.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.C1SuperLabel8.Location = New System.Drawing.Point(331, 100)
        Me.C1SuperLabel8.Margin = New System.Windows.Forms.Padding(2, 4, 2, 4)
        Me.C1SuperLabel8.Name = "C1SuperLabel8"
        Me.C1SuperLabel8.Size = New System.Drawing.Size(79, 19)
        Me.C1SuperLabel8.TabIndex = 1050
        Me.C1SuperLabel8.Text = "Código Externo"
        Me.C1SuperLabel8.UseMnemonic = True
        '
        'C1SuperLabel6
        '
        Me.C1SuperLabel6.AutoSize = True
        Me.C1SuperLabel6.BackColor = System.Drawing.Color.Transparent
        Me.C1SuperLabel6.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.C1SuperLabel6.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.C1SuperLabel6.Location = New System.Drawing.Point(6, 100)
        Me.C1SuperLabel6.Margin = New System.Windows.Forms.Padding(2, 4, 2, 4)
        Me.C1SuperLabel6.Name = "C1SuperLabel6"
        Me.C1SuperLabel6.Size = New System.Drawing.Size(72, 19)
        Me.C1SuperLabel6.TabIndex = 1049
        Me.C1SuperLabel6.Text = "Cargo Usuario"
        Me.C1SuperLabel6.UseMnemonic = True
        '
        'C1SuperLabel5
        '
        Me.C1SuperLabel5.AutoSize = True
        Me.C1SuperLabel5.BackColor = System.Drawing.Color.Transparent
        Me.C1SuperLabel5.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.C1SuperLabel5.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.C1SuperLabel5.Location = New System.Drawing.Point(6, 165)
        Me.C1SuperLabel5.Margin = New System.Windows.Forms.Padding(2, 4, 2, 4)
        Me.C1SuperLabel5.Name = "C1SuperLabel5"
        Me.C1SuperLabel5.Size = New System.Drawing.Size(38, 19)
        Me.C1SuperLabel5.TabIndex = 1048
        Me.C1SuperLabel5.Text = "Estado"
        Me.C1SuperLabel5.UseMnemonic = True
        '
        'C1SuperLabel4
        '
        Me.C1SuperLabel4.AutoSize = True
        Me.C1SuperLabel4.BackColor = System.Drawing.Color.Transparent
        Me.C1SuperLabel4.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.C1SuperLabel4.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.C1SuperLabel4.Location = New System.Drawing.Point(331, 4)
        Me.C1SuperLabel4.Margin = New System.Windows.Forms.Padding(2, 4, 2, 4)
        Me.C1SuperLabel4.Name = "C1SuperLabel4"
        Me.C1SuperLabel4.Size = New System.Drawing.Size(71, 19)
        Me.C1SuperLabel4.TabIndex = 1047
        Me.C1SuperLabel4.Text = "Nombre Corto"
        Me.C1SuperLabel4.UseMnemonic = True
        '
        'C1SuperLabel3
        '
        Me.C1SuperLabel3.AutoSize = True
        Me.C1SuperLabel3.BackColor = System.Drawing.Color.Transparent
        Me.C1SuperLabel3.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.C1SuperLabel3.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.C1SuperLabel3.Location = New System.Drawing.Point(6, 36)
        Me.C1SuperLabel3.Margin = New System.Windows.Forms.Padding(2, 4, 2, 4)
        Me.C1SuperLabel3.Name = "C1SuperLabel3"
        Me.C1SuperLabel3.Size = New System.Drawing.Size(40, 19)
        Me.C1SuperLabel3.TabIndex = 1046
        Me.C1SuperLabel3.Text = "Unidad"
        Me.C1SuperLabel3.UseMnemonic = True
        '
        'C1SuperLabel2
        '
        Me.C1SuperLabel2.AutoSize = True
        Me.C1SuperLabel2.BackColor = System.Drawing.Color.Transparent
        Me.C1SuperLabel2.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.C1SuperLabel2.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.C1SuperLabel2.Location = New System.Drawing.Point(6, 68)
        Me.C1SuperLabel2.Margin = New System.Windows.Forms.Padding(2, 4, 2, 4)
        Me.C1SuperLabel2.Name = "C1SuperLabel2"
        Me.C1SuperLabel2.Size = New System.Drawing.Size(81, 19)
        Me.C1SuperLabel2.TabIndex = 1045
        Me.C1SuperLabel2.Text = "Tipo de Usuario"
        Me.C1SuperLabel2.UseMnemonic = True
        '
        'C1SuperLabel22
        '
        Me.C1SuperLabel22.AutoSize = True
        Me.C1SuperLabel22.BackColor = System.Drawing.Color.Transparent
        Me.C1SuperLabel22.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.C1SuperLabel22.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.C1SuperLabel22.Location = New System.Drawing.Point(6, 4)
        Me.C1SuperLabel22.Margin = New System.Windows.Forms.Padding(2, 4, 2, 4)
        Me.C1SuperLabel22.Name = "C1SuperLabel22"
        Me.C1SuperLabel22.Size = New System.Drawing.Size(43, 19)
        Me.C1SuperLabel22.TabIndex = 1044
        Me.C1SuperLabel22.Text = "Nombre"
        Me.C1SuperLabel22.UseMnemonic = True
        '
        'C1SuperLabel12
        '
        Me.C1SuperLabel12.AutoSize = True
        Me.C1SuperLabel12.BackColor = System.Drawing.Color.Transparent
        Me.C1SuperLabel12.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.C1SuperLabel12.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.C1SuperLabel12.Location = New System.Drawing.Point(209, 165)
        Me.C1SuperLabel12.Margin = New System.Windows.Forms.Padding(2, 4, 2, 4)
        Me.C1SuperLabel12.Name = "C1SuperLabel12"
        Me.C1SuperLabel12.Size = New System.Drawing.Size(80, 19)
        Me.C1SuperLabel12.TabIndex = 1054
        Me.C1SuperLabel12.Text = "Fecha Vigencia"
        Me.C1SuperLabel12.UseMnemonic = True
        '
        'Btn_VerPerfiles
        '
        Me.Btn_VerPerfiles.BackColor = System.Drawing.Color.Gainsboro
        Me.Btn_VerPerfiles.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.Btn_VerPerfiles.Font = New System.Drawing.Font("Arial", 7.5!)
        Me.Btn_VerPerfiles.ForeColor = System.Drawing.Color.MediumBlue
        Me.Btn_VerPerfiles.Location = New System.Drawing.Point(543, 2)
        Me.Btn_VerPerfiles.Margin = New System.Windows.Forms.Padding(2)
        Me.Btn_VerPerfiles.Name = "Btn_VerPerfiles"
        Me.Btn_VerPerfiles.Size = New System.Drawing.Size(65, 21)
        Me.Btn_VerPerfiles.TabIndex = 1
        Me.Btn_VerPerfiles.TabStop = False
        Me.Btn_VerPerfiles.Tag = ""
        Me.Btn_VerPerfiles.Text = "&Perfiles"
        Me.Btn_VerPerfiles.UseVisualStyleBackColor = True
        Me.Btn_VerPerfiles.Visible = False
        Me.Btn_VerPerfiles.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Silver
        '
        'Btn_ReiniciaPassword
        '
        Me.Btn_ReiniciaPassword.BackColor = System.Drawing.Color.Gainsboro
        Me.Btn_ReiniciaPassword.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.Btn_ReiniciaPassword.Font = New System.Drawing.Font("Arial", 7.5!)
        Me.Btn_ReiniciaPassword.ForeColor = System.Drawing.Color.MediumBlue
        Me.Btn_ReiniciaPassword.Location = New System.Drawing.Point(669, 2)
        Me.Btn_ReiniciaPassword.Margin = New System.Windows.Forms.Padding(2)
        Me.Btn_ReiniciaPassword.Name = "Btn_ReiniciaPassword"
        Me.Btn_ReiniciaPassword.Size = New System.Drawing.Size(65, 21)
        Me.Btn_ReiniciaPassword.TabIndex = 0
        Me.Btn_ReiniciaPassword.TabStop = False
        Me.Btn_ReiniciaPassword.Tag = ""
        Me.Btn_ReiniciaPassword.Text = "&Reset Pwd"
        Me.Btn_ReiniciaPassword.UseVisualStyleBackColor = True
        Me.Btn_ReiniciaPassword.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Silver
        '
        'Btn_Modificar
        '
        Me.Btn_Modificar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.Btn_Modificar.Enabled = False
        Me.Btn_Modificar.Font = New System.Drawing.Font("Arial", 7.5!)
        Me.Btn_Modificar.Location = New System.Drawing.Point(361, 3)
        Me.Btn_Modificar.Margin = New System.Windows.Forms.Padding(2)
        Me.Btn_Modificar.Name = "Btn_Modificar"
        Me.Btn_Modificar.Size = New System.Drawing.Size(24, 20)
        Me.Btn_Modificar.TabIndex = 3
        Me.Btn_Modificar.TabStop = False
        Me.Btn_Modificar.Tag = ""
        Me.Btn_Modificar.Text = "M"
        Me.C1SuperTooltip1.SetToolTip(Me.Btn_Modificar, "Modifica registro seleccionado")
        Me.Btn_Modificar.UseVisualStyleBackColor = True
        Me.Btn_Modificar.Visible = False
        Me.Btn_Modificar.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Silver
        '
        'C1SuperLabel14
        '
        Me.C1SuperLabel14.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.C1SuperLabel14.BackgroundImage = CType(resources.GetObject("C1SuperLabel14.BackgroundImage"), System.Drawing.Image)
        Me.C1SuperLabel14.BackgroundImageLayout = C1.Win.C1SuperTooltip.BackgroundImageLayout.Stretch
        Me.C1SuperLabel14.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Bold)
        Me.C1SuperLabel14.ForeColor = System.Drawing.Color.Navy
        Me.C1SuperLabel14.Location = New System.Drawing.Point(474, 875)
        Me.C1SuperLabel14.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.C1SuperLabel14.Name = "C1SuperLabel14"
        Me.C1SuperLabel14.Size = New System.Drawing.Size(302, 22)
        Me.C1SuperLabel14.TabIndex = 165
        Me.C1SuperLabel14.Text = "Perfiles de Usuario"
        Me.C1SuperLabel14.UseMnemonic = True
        '
        'C1SuperLabel1
        '
        Me.C1SuperLabel1.BackgroundImage = CType(resources.GetObject("C1SuperLabel1.BackgroundImage"), System.Drawing.Image)
        Me.C1SuperLabel1.BackgroundImageLayout = C1.Win.C1SuperTooltip.BackgroundImageLayout.Stretch
        Me.C1SuperLabel1.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Bold)
        Me.C1SuperLabel1.ForeColor = System.Drawing.Color.Navy
        Me.C1SuperLabel1.Location = New System.Drawing.Point(4, 874)
        Me.C1SuperLabel1.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.C1SuperLabel1.Name = "C1SuperLabel1"
        Me.C1SuperLabel1.Size = New System.Drawing.Size(465, 22)
        Me.C1SuperLabel1.TabIndex = 90
        Me.C1SuperLabel1.Text = "Usuarios del Sistema"
        Me.C1SuperLabel1.UseMnemonic = True
        '
        'Pnl_Datos_TipoOperacion
        '
        Me.Pnl_Datos_TipoOperacion.BackColor = System.Drawing.Color.Gainsboro
        Me.Pnl_Datos_TipoOperacion.Controls.Add(Me.Btn_VerPerfiles)
        Me.Pnl_Datos_TipoOperacion.Controls.Add(Me.Tdg_Usuarios)
        Me.Pnl_Datos_TipoOperacion.Controls.Add(Me.Btn_ReiniciaPassword)
        Me.Pnl_Datos_TipoOperacion.Controls.Add(Me.Btn_Modificar)
        Me.Pnl_Datos_TipoOperacion.Location = New System.Drawing.Point(1, 340)
        Me.Pnl_Datos_TipoOperacion.Margin = New System.Windows.Forms.Padding(2)
        Me.Pnl_Datos_TipoOperacion.Name = "Pnl_Datos_TipoOperacion"
        Me.Pnl_Datos_TipoOperacion.Size = New System.Drawing.Size(748, 257)
        Me.Pnl_Datos_TipoOperacion.TabIndex = 0
        Me.Pnl_Datos_TipoOperacion.TabStop = True
        '
        'Tdg_Usuarios
        '
        Me.Tdg_Usuarios.AllowColSelect = False
        Me.Tdg_Usuarios.AllowRowSizing = C1.Win.C1TrueDBGrid.RowSizingEnum.None
        Me.Tdg_Usuarios.AllowUpdate = False
        Me.Tdg_Usuarios.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Tdg_Usuarios.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Tdg_Usuarios.Caption = "U S U A R I O S"
        Me.Tdg_Usuarios.CaptionHeight = 24
        Me.Tdg_Usuarios.CollapseColor = System.Drawing.Color.WhiteSmoke
        Me.Tdg_Usuarios.DirectionAfterEnter = C1.Win.C1TrueDBGrid.DirectionAfterEnterEnum.MoveDown
        Me.Tdg_Usuarios.ExpandColor = System.Drawing.Color.WhiteSmoke
        Me.Tdg_Usuarios.Font = New System.Drawing.Font("Arial", 7.5!)
        Me.Tdg_Usuarios.ForeColor = System.Drawing.Color.Lavender
        Me.Tdg_Usuarios.GroupByAreaVisible = False
        Me.Tdg_Usuarios.GroupByCaption = "Arrastre hasta aquí las columnas por las que desea agrupar"
        Me.Tdg_Usuarios.Images.Add(CType(resources.GetObject("Tdg_Usuarios.Images"), System.Drawing.Image))
        Me.Tdg_Usuarios.Location = New System.Drawing.Point(1, 25)
        Me.Tdg_Usuarios.MaintainRowCurrency = True
        Me.Tdg_Usuarios.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.Tdg_Usuarios.MarqueeStyle = C1.Win.C1TrueDBGrid.MarqueeEnum.HighlightRow
        Me.Tdg_Usuarios.MultiSelect = C1.Win.C1TrueDBGrid.MultiSelectEnum.Simple
        Me.Tdg_Usuarios.Name = "Tdg_Usuarios"
        Me.Tdg_Usuarios.PreviewInfo.Location = New System.Drawing.Point(0, 0)
        Me.Tdg_Usuarios.PreviewInfo.Size = New System.Drawing.Size(0, 0)
        Me.Tdg_Usuarios.PreviewInfo.ZoomFactor = 75.0R
        Me.Tdg_Usuarios.PrintInfo.PageSettings = CType(resources.GetObject("Tdg_Usuarios.PrintInfo.PageSettings"), System.Drawing.Printing.PageSettings)
        Me.Tdg_Usuarios.RecordSelectors = False
        Me.Tdg_Usuarios.RecordSelectorWidth = 15
        Me.Tdg_Usuarios.RowDivider.Color = System.Drawing.Color.DarkGray
        Me.Tdg_Usuarios.RowDivider.Style = C1.Win.C1TrueDBGrid.LineStyleEnum.Raised
        Me.Tdg_Usuarios.RowHeight = 15
        Me.Tdg_Usuarios.ScrollTips = True
        Me.Tdg_Usuarios.Size = New System.Drawing.Size(746, 229)
        Me.Tdg_Usuarios.SplitDividerSize = New System.Drawing.Size(0, 0)
        Me.Tdg_Usuarios.TabIndex = 0
        Me.Tdg_Usuarios.TabStop = False
        Me.Tdg_Usuarios.Text = "C1TrueDBGrid1"
        Me.Tdg_Usuarios.VisualStyle = C1.Win.C1TrueDBGrid.VisualStyle.Office2007Silver
        Me.Tdg_Usuarios.PropBag = resources.GetString("Tdg_Usuarios.PropBag")
        '
        'Pnl_PerfilesUsuarios
        '
        Me.Pnl_PerfilesUsuarios.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Pnl_PerfilesUsuarios.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.Pnl_PerfilesUsuarios.Controls.Add(Me.Cmb_NegocioPerfiles)
        Me.Pnl_PerfilesUsuarios.Controls.Add(Me.GroupBox2)
        Me.Pnl_PerfilesUsuarios.Controls.Add(Me.Btn_QuitarPerfilUsuario)
        Me.Pnl_PerfilesUsuarios.Controls.Add(Me.Btn_AsociarPerfilUsuario)
        Me.Pnl_PerfilesUsuarios.Controls.Add(Me.GroupBox1)
        Me.Pnl_PerfilesUsuarios.Controls.Add(Me.C1SuperLabel16)
        Me.Pnl_PerfilesUsuarios.ForeColor = System.Drawing.Color.DimGray
        Me.Pnl_PerfilesUsuarios.Location = New System.Drawing.Point(-1, 2)
        Me.Pnl_PerfilesUsuarios.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.Pnl_PerfilesUsuarios.Name = "Pnl_PerfilesUsuarios"
        Me.Pnl_PerfilesUsuarios.Size = New System.Drawing.Size(747, 258)
        Me.Pnl_PerfilesUsuarios.TabIndex = 2
        Me.Pnl_PerfilesUsuarios.TabStop = True
        '
        'Cmb_NegocioPerfiles
        '
        Me.Cmb_NegocioPerfiles.AddItemSeparator = Global.Microsoft.VisualBasic.ChrW(59)
        Me.Cmb_NegocioPerfiles.AllowColMove = False
        Me.Cmb_NegocioPerfiles.AllowSort = False
        Me.Cmb_NegocioPerfiles.AutoDropDown = True
        Me.Cmb_NegocioPerfiles.AutoSelect = True
        Me.Cmb_NegocioPerfiles.AutoSize = False
        Me.Cmb_NegocioPerfiles.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Cmb_NegocioPerfiles.Caption = ""
        Me.Cmb_NegocioPerfiles.CaptionHeight = 18
        Me.Cmb_NegocioPerfiles.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.Cmb_NegocioPerfiles.ColumnCaptionHeight = 18
        Me.Cmb_NegocioPerfiles.ColumnFooterHeight = 18
        Me.Cmb_NegocioPerfiles.ColumnHeaders = False
        Me.Cmb_NegocioPerfiles.ComboStyle = C1.Win.C1List.ComboStyleEnum.DropdownList
        Me.Cmb_NegocioPerfiles.ContentHeight = 12
        Me.Cmb_NegocioPerfiles.DeadAreaBackColor = System.Drawing.Color.Empty
        Me.Cmb_NegocioPerfiles.EditorBackColor = System.Drawing.Color.White
        Me.Cmb_NegocioPerfiles.EditorFont = New System.Drawing.Font("Arial", 7.0!, System.Drawing.FontStyle.Bold)
        Me.Cmb_NegocioPerfiles.EditorForeColor = System.Drawing.Color.SteelBlue
        Me.Cmb_NegocioPerfiles.EditorHeight = 12
        Me.Cmb_NegocioPerfiles.ExtendRightColumn = True
        Me.Cmb_NegocioPerfiles.FlatStyle = C1.Win.C1List.FlatModeEnum.Flat
        Me.Cmb_NegocioPerfiles.Font = New System.Drawing.Font("Arial", 7.0!, System.Drawing.FontStyle.Bold)
        Me.Cmb_NegocioPerfiles.Images.Add(CType(resources.GetObject("Cmb_NegocioPerfiles.Images"), System.Drawing.Image))
        Me.Cmb_NegocioPerfiles.ItemHeight = 18
        Me.Cmb_NegocioPerfiles.Location = New System.Drawing.Point(445, 3)
        Me.Cmb_NegocioPerfiles.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Cmb_NegocioPerfiles.MatchEntry = C1.Win.C1List.MatchEntryEnum.Extended
        Me.Cmb_NegocioPerfiles.MatchEntryTimeout = CType(500, Long)
        Me.Cmb_NegocioPerfiles.MaxDropDownItems = CType(5, Short)
        Me.Cmb_NegocioPerfiles.MaxLength = 0
        Me.Cmb_NegocioPerfiles.MinimumSize = New System.Drawing.Size(10, 16)
        Me.Cmb_NegocioPerfiles.MouseCursor = System.Windows.Forms.Cursors.Default
        Me.Cmb_NegocioPerfiles.Name = "Cmb_NegocioPerfiles"
        Me.Cmb_NegocioPerfiles.RowSubDividerColor = System.Drawing.Color.DarkGray
        Me.Cmb_NegocioPerfiles.Size = New System.Drawing.Size(300, 16)
        Me.Cmb_NegocioPerfiles.TabIndex = 0
        Me.Cmb_NegocioPerfiles.VisualStyle = C1.Win.C1List.VisualStyle.Office2007Silver
        Me.Cmb_NegocioPerfiles.PropBag = resources.GetString("Cmb_NegocioPerfiles.PropBag")
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Tdg_PerfilesAsignados)
        Me.GroupBox2.Location = New System.Drawing.Point(0, 16)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(354, 240)
        Me.GroupBox2.TabIndex = 5
        Me.GroupBox2.TabStop = False
        '
        'Tdg_PerfilesAsignados
        '
        Me.Tdg_PerfilesAsignados.AllowColSelect = False
        Me.Tdg_PerfilesAsignados.AllowRowSizing = C1.Win.C1TrueDBGrid.RowSizingEnum.None
        Me.Tdg_PerfilesAsignados.AllowUpdate = False
        Me.Tdg_PerfilesAsignados.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Tdg_PerfilesAsignados.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Tdg_PerfilesAsignados.Caption = "A S O C I A D O S"
        Me.Tdg_PerfilesAsignados.CaptionHeight = 21
        Me.Tdg_PerfilesAsignados.CollapseColor = System.Drawing.Color.WhiteSmoke
        Me.Tdg_PerfilesAsignados.ColumnHeaders = False
        Me.Tdg_PerfilesAsignados.DirectionAfterEnter = C1.Win.C1TrueDBGrid.DirectionAfterEnterEnum.MoveDown
        Me.Tdg_PerfilesAsignados.ExpandColor = System.Drawing.Color.WhiteSmoke
        Me.Tdg_PerfilesAsignados.Font = New System.Drawing.Font("Arial", 7.5!)
        Me.Tdg_PerfilesAsignados.ForeColor = System.Drawing.Color.Lavender
        Me.Tdg_PerfilesAsignados.GroupByAreaVisible = False
        Me.Tdg_PerfilesAsignados.GroupByCaption = "Arrastre hasta aquí las columnas por las que desea agrupar"
        Me.Tdg_PerfilesAsignados.Images.Add(CType(resources.GetObject("Tdg_PerfilesAsignados.Images"), System.Drawing.Image))
        Me.Tdg_PerfilesAsignados.Location = New System.Drawing.Point(1, 8)
        Me.Tdg_PerfilesAsignados.MaintainRowCurrency = True
        Me.Tdg_PerfilesAsignados.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.Tdg_PerfilesAsignados.MarqueeStyle = C1.Win.C1TrueDBGrid.MarqueeEnum.HighlightRow
        Me.Tdg_PerfilesAsignados.MultiSelect = C1.Win.C1TrueDBGrid.MultiSelectEnum.Simple
        Me.Tdg_PerfilesAsignados.Name = "Tdg_PerfilesAsignados"
        Me.Tdg_PerfilesAsignados.PreviewInfo.Location = New System.Drawing.Point(0, 0)
        Me.Tdg_PerfilesAsignados.PreviewInfo.Size = New System.Drawing.Size(0, 0)
        Me.Tdg_PerfilesAsignados.PreviewInfo.ZoomFactor = 75.0R
        Me.Tdg_PerfilesAsignados.PrintInfo.PageSettings = CType(resources.GetObject("Tdg_PerfilesAsignados.PrintInfo.PageSettings"), System.Drawing.Printing.PageSettings)
        Me.Tdg_PerfilesAsignados.RecordSelectors = False
        Me.Tdg_PerfilesAsignados.RecordSelectorWidth = 15
        Me.Tdg_PerfilesAsignados.RowDivider.Color = System.Drawing.Color.DarkGray
        Me.Tdg_PerfilesAsignados.RowDivider.Style = C1.Win.C1TrueDBGrid.LineStyleEnum.Raised
        Me.Tdg_PerfilesAsignados.RowHeight = 15
        Me.Tdg_PerfilesAsignados.ScrollTips = True
        Me.Tdg_PerfilesAsignados.Size = New System.Drawing.Size(350, 229)
        Me.Tdg_PerfilesAsignados.SplitDividerSize = New System.Drawing.Size(0, 0)
        Me.Tdg_PerfilesAsignados.TabIndex = 0
        Me.Tdg_PerfilesAsignados.TabStop = False
        Me.Tdg_PerfilesAsignados.Text = "C1TrueDBGrid1"
        Me.Tdg_PerfilesAsignados.VisualStyle = C1.Win.C1TrueDBGrid.VisualStyle.Office2007Silver
        Me.Tdg_PerfilesAsignados.PropBag = resources.GetString("Tdg_PerfilesAsignados.PropBag")
        '
        'Btn_QuitarPerfilUsuario
        '
        Me.Btn_QuitarPerfilUsuario.BackColor = System.Drawing.Color.Gainsboro
        Me.Btn_QuitarPerfilUsuario.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.Btn_QuitarPerfilUsuario.Font = New System.Drawing.Font("Arial", 7.5!)
        Me.Btn_QuitarPerfilUsuario.ForeColor = System.Drawing.Color.MediumBlue
        Me.Btn_QuitarPerfilUsuario.Location = New System.Drawing.Point(362, 130)
        Me.Btn_QuitarPerfilUsuario.Margin = New System.Windows.Forms.Padding(2)
        Me.Btn_QuitarPerfilUsuario.Name = "Btn_QuitarPerfilUsuario"
        Me.Btn_QuitarPerfilUsuario.Size = New System.Drawing.Size(24, 20)
        Me.Btn_QuitarPerfilUsuario.TabIndex = 4
        Me.Btn_QuitarPerfilUsuario.TabStop = False
        Me.Btn_QuitarPerfilUsuario.Tag = ""
        Me.Btn_QuitarPerfilUsuario.Text = "►"
        Me.Btn_QuitarPerfilUsuario.UseVisualStyleBackColor = True
        Me.Btn_QuitarPerfilUsuario.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Silver
        '
        'Btn_AsociarPerfilUsuario
        '
        Me.Btn_AsociarPerfilUsuario.BackColor = System.Drawing.Color.Gainsboro
        Me.Btn_AsociarPerfilUsuario.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.Btn_AsociarPerfilUsuario.Font = New System.Drawing.Font("Arial", 7.5!)
        Me.Btn_AsociarPerfilUsuario.ForeColor = System.Drawing.Color.MediumBlue
        Me.Btn_AsociarPerfilUsuario.Location = New System.Drawing.Point(362, 107)
        Me.Btn_AsociarPerfilUsuario.Margin = New System.Windows.Forms.Padding(2)
        Me.Btn_AsociarPerfilUsuario.Name = "Btn_AsociarPerfilUsuario"
        Me.Btn_AsociarPerfilUsuario.Size = New System.Drawing.Size(24, 20)
        Me.Btn_AsociarPerfilUsuario.TabIndex = 3
        Me.Btn_AsociarPerfilUsuario.TabStop = False
        Me.Btn_AsociarPerfilUsuario.Tag = ""
        Me.Btn_AsociarPerfilUsuario.Text = "◄"
        Me.Btn_AsociarPerfilUsuario.UseVisualStyleBackColor = True
        Me.Btn_AsociarPerfilUsuario.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Silver
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Tdg_PerfilesDisponibles)
        Me.GroupBox1.Location = New System.Drawing.Point(396, 16)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(0)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(0)
        Me.GroupBox1.Size = New System.Drawing.Size(353, 240)
        Me.GroupBox1.TabIndex = 3
        Me.GroupBox1.TabStop = False
        '
        'Tdg_PerfilesDisponibles
        '
        Me.Tdg_PerfilesDisponibles.AllowColSelect = False
        Me.Tdg_PerfilesDisponibles.AllowRowSizing = C1.Win.C1TrueDBGrid.RowSizingEnum.None
        Me.Tdg_PerfilesDisponibles.AllowUpdate = False
        Me.Tdg_PerfilesDisponibles.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Tdg_PerfilesDisponibles.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Tdg_PerfilesDisponibles.Caption = "D I S P O N I B L E S"
        Me.Tdg_PerfilesDisponibles.CaptionHeight = 21
        Me.Tdg_PerfilesDisponibles.CollapseColor = System.Drawing.Color.WhiteSmoke
        Me.Tdg_PerfilesDisponibles.ColumnHeaders = False
        Me.Tdg_PerfilesDisponibles.DirectionAfterEnter = C1.Win.C1TrueDBGrid.DirectionAfterEnterEnum.MoveDown
        Me.Tdg_PerfilesDisponibles.ExpandColor = System.Drawing.Color.WhiteSmoke
        Me.Tdg_PerfilesDisponibles.Font = New System.Drawing.Font("Arial", 7.5!)
        Me.Tdg_PerfilesDisponibles.ForeColor = System.Drawing.Color.Lavender
        Me.Tdg_PerfilesDisponibles.GroupByAreaVisible = False
        Me.Tdg_PerfilesDisponibles.GroupByCaption = "Arrastre hasta aquí las columnas por las que desea agrupar"
        Me.Tdg_PerfilesDisponibles.Images.Add(CType(resources.GetObject("Tdg_PerfilesDisponibles.Images"), System.Drawing.Image))
        Me.Tdg_PerfilesDisponibles.Location = New System.Drawing.Point(1, 8)
        Me.Tdg_PerfilesDisponibles.MaintainRowCurrency = True
        Me.Tdg_PerfilesDisponibles.Margin = New System.Windows.Forms.Padding(0, 3, 0, 0)
        Me.Tdg_PerfilesDisponibles.MarqueeStyle = C1.Win.C1TrueDBGrid.MarqueeEnum.HighlightRow
        Me.Tdg_PerfilesDisponibles.MultiSelect = C1.Win.C1TrueDBGrid.MultiSelectEnum.Simple
        Me.Tdg_PerfilesDisponibles.Name = "Tdg_PerfilesDisponibles"
        Me.Tdg_PerfilesDisponibles.PreviewInfo.Location = New System.Drawing.Point(0, 0)
        Me.Tdg_PerfilesDisponibles.PreviewInfo.Size = New System.Drawing.Size(0, 0)
        Me.Tdg_PerfilesDisponibles.PreviewInfo.ZoomFactor = 75.0R
        Me.Tdg_PerfilesDisponibles.PrintInfo.PageSettings = CType(resources.GetObject("Tdg_PerfilesDisponibles.PrintInfo.PageSettings"), System.Drawing.Printing.PageSettings)
        Me.Tdg_PerfilesDisponibles.RecordSelectors = False
        Me.Tdg_PerfilesDisponibles.RecordSelectorWidth = 15
        Me.Tdg_PerfilesDisponibles.RowDivider.Color = System.Drawing.Color.DarkGray
        Me.Tdg_PerfilesDisponibles.RowDivider.Style = C1.Win.C1TrueDBGrid.LineStyleEnum.Raised
        Me.Tdg_PerfilesDisponibles.RowHeight = 15
        Me.Tdg_PerfilesDisponibles.ScrollTips = True
        Me.Tdg_PerfilesDisponibles.Size = New System.Drawing.Size(350, 229)
        Me.Tdg_PerfilesDisponibles.SplitDividerSize = New System.Drawing.Size(0, 0)
        Me.Tdg_PerfilesDisponibles.TabIndex = 0
        Me.Tdg_PerfilesDisponibles.TabStop = False
        Me.Tdg_PerfilesDisponibles.Text = "C1TrueDBGrid2"
        Me.Tdg_PerfilesDisponibles.VisualStyle = C1.Win.C1TrueDBGrid.VisualStyle.Office2007Silver
        Me.Tdg_PerfilesDisponibles.PropBag = resources.GetString("Tdg_PerfilesDisponibles.PropBag")
        '
        'C1SuperLabel16
        '
        Me.C1SuperLabel16.BackColor = System.Drawing.Color.Transparent
        Me.C1SuperLabel16.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.C1SuperLabel16.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.C1SuperLabel16.Location = New System.Drawing.Point(400, 1)
        Me.C1SuperLabel16.Margin = New System.Windows.Forms.Padding(2, 4, 2, 4)
        Me.C1SuperLabel16.Name = "C1SuperLabel16"
        Me.C1SuperLabel16.Size = New System.Drawing.Size(45, 19)
        Me.C1SuperLabel16.TabIndex = 1049
        Me.C1SuperLabel16.Text = "Negocio"
        Me.C1SuperLabel16.UseMnemonic = True
        '
        'Tab_Contenedor
        '
        Me.Tab_Contenedor.Animate = False
        Me.Tab_Contenedor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.Tab_Contenedor.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Tab_Contenedor.Controls.Add(Me.Tab_Usuario)
        Me.Tab_Contenedor.Controls.Add(Me.Tab_PerfilesUsuario)
        Me.Tab_Contenedor.Controls.Add(Me.Tab_CuentasUsuario)
        Me.Tab_Contenedor.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Tab_Contenedor.HotTrack = True
        Me.Tab_Contenedor.Indent = 10
        Me.Tab_Contenedor.ItemSize = New System.Drawing.Size(0, 21)
        Me.Tab_Contenedor.Location = New System.Drawing.Point(2, 22)
        Me.Tab_Contenedor.Margin = New System.Windows.Forms.Padding(0)
        Me.Tab_Contenedor.Name = "Tab_Contenedor"
        Me.Tab_Contenedor.Padding = New System.Drawing.Point(27, 10)
        Me.Tab_Contenedor.Size = New System.Drawing.Size(747, 318)
        Me.Tab_Contenedor.TabAreaSpacing = 1
        Me.Tab_Contenedor.TabIndex = 1036
        Me.Tab_Contenedor.TabSizeMode = C1.Win.C1Command.TabSizeModeEnum.Fit
        Me.Tab_Contenedor.TabsSpacing = 10
        Me.Tab_Contenedor.TabStyle = C1.Win.C1Command.TabStyleEnum.Office2007
        Me.Tab_Contenedor.VisualStyle = C1.Win.C1Command.VisualStyle.Custom
        Me.Tab_Contenedor.VisualStyleBase = C1.Win.C1Command.VisualStyle.Office2007Silver
        '
        'Tab_Usuario
        '
        Me.Tab_Usuario.BackColor = System.Drawing.Color.Gainsboro
        Me.Tab_Usuario.Controls.Add(Me.Pnl_DatosUsuario)
        Me.Tab_Usuario.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.Tab_Usuario.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.Tab_Usuario.Location = New System.Drawing.Point(0, 23)
        Me.Tab_Usuario.Margin = New System.Windows.Forms.Padding(0)
        Me.Tab_Usuario.Name = "Tab_Usuario"
        Me.Tab_Usuario.Size = New System.Drawing.Size(747, 295)
        Me.Tab_Usuario.TabBackColor = System.Drawing.Color.Transparent
        Me.Tab_Usuario.TabForeColor = System.Drawing.Color.DarkSlateGray
        Me.Tab_Usuario.TabForeColorSelected = System.Drawing.Color.DarkSlateGray
        Me.Tab_Usuario.TabIndex = 0
        Me.Tab_Usuario.Text = "Usuario"
        '
        'Tab_PerfilesUsuario
        '
        Me.Tab_PerfilesUsuario.BackColor = System.Drawing.Color.Gainsboro
        Me.Tab_PerfilesUsuario.Controls.Add(Me.Pnl_PerfilesUsuarios)
        Me.Tab_PerfilesUsuario.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.Tab_PerfilesUsuario.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.Tab_PerfilesUsuario.Location = New System.Drawing.Point(0, 23)
        Me.Tab_PerfilesUsuario.Margin = New System.Windows.Forms.Padding(0)
        Me.Tab_PerfilesUsuario.Name = "Tab_PerfilesUsuario"
        Me.Tab_PerfilesUsuario.Size = New System.Drawing.Size(747, 295)
        Me.Tab_PerfilesUsuario.TabBackColor = System.Drawing.Color.Transparent
        Me.Tab_PerfilesUsuario.TabForeColor = System.Drawing.Color.DarkSlateGray
        Me.Tab_PerfilesUsuario.TabForeColorSelected = System.Drawing.Color.DarkSlateGray
        Me.Tab_PerfilesUsuario.TabIndex = 1
        Me.Tab_PerfilesUsuario.Text = "Perfiles del Usuario"
        '
        'Tab_CuentasUsuario
        '
        Me.Tab_CuentasUsuario.BackColor = System.Drawing.Color.Gainsboro
        Me.Tab_CuentasUsuario.Controls.Add(Me.Pnl_CuentasUsuario)
        Me.Tab_CuentasUsuario.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.Tab_CuentasUsuario.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.Tab_CuentasUsuario.Location = New System.Drawing.Point(0, 23)
        Me.Tab_CuentasUsuario.Margin = New System.Windows.Forms.Padding(0)
        Me.Tab_CuentasUsuario.Name = "Tab_CuentasUsuario"
        Me.Tab_CuentasUsuario.Size = New System.Drawing.Size(747, 295)
        Me.Tab_CuentasUsuario.TabBackColor = System.Drawing.Color.Transparent
        Me.Tab_CuentasUsuario.TabForeColor = System.Drawing.Color.DarkSlateGray
        Me.Tab_CuentasUsuario.TabForeColorSelected = System.Drawing.Color.DarkSlateGray
        Me.Tab_CuentasUsuario.TabIndex = 2
        Me.Tab_CuentasUsuario.Text = "Cuentas del Usuario"
        '
        'Pnl_CuentasUsuario
        '
        Me.Pnl_CuentasUsuario.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Pnl_CuentasUsuario.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.Pnl_CuentasUsuario.Controls.Add(Me.GroupBox5)
        Me.Pnl_CuentasUsuario.Controls.Add(Me.GroupBox3)
        Me.Pnl_CuentasUsuario.Controls.Add(Me.Btn_AsociarCuenta)
        Me.Pnl_CuentasUsuario.Controls.Add(Me.Btn_QuitarCuenta)
        Me.Pnl_CuentasUsuario.Controls.Add(Me.GroupBox4)
        Me.Pnl_CuentasUsuario.ForeColor = System.Drawing.Color.DimGray
        Me.Pnl_CuentasUsuario.Location = New System.Drawing.Point(-1, 4)
        Me.Pnl_CuentasUsuario.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.Pnl_CuentasUsuario.Name = "Pnl_CuentasUsuario"
        Me.Pnl_CuentasUsuario.Size = New System.Drawing.Size(748, 288)
        Me.Pnl_CuentasUsuario.TabIndex = 3
        Me.Pnl_CuentasUsuario.TabStop = True
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.Cmb_Asesores)
        Me.GroupBox5.Controls.Add(Me.C1SuperLabel21)
        Me.GroupBox5.Controls.Add(Me.Cmb_Sucursal)
        Me.GroupBox5.Controls.Add(Me.Cmb_Segmento)
        Me.GroupBox5.Controls.Add(Me.Cmb_PerfilRiesgo)
        Me.GroupBox5.Controls.Add(Me.Cmb_TipoAdministracion)
        Me.GroupBox5.Controls.Add(Me.Cmb_NegocioCuentas)
        Me.GroupBox5.Controls.Add(Me.Txt_RutCliente)
        Me.GroupBox5.Controls.Add(Me.C1SuperLabel20)
        Me.GroupBox5.Controls.Add(Me.Txt_NombreCuenta)
        Me.GroupBox5.Controls.Add(Me.Btn_BuscarCuentasDisponibles)
        Me.GroupBox5.Controls.Add(Me.Btn_LimpiarFiltroCuentas)
        Me.GroupBox5.Controls.Add(Me.C1SuperLabel19)
        Me.GroupBox5.Controls.Add(Me.Btn_BuscarCuenta)
        Me.GroupBox5.Controls.Add(Me.Txt_NumeroCuenta)
        Me.GroupBox5.Controls.Add(Me.Btn_BuscarCliente)
        Me.GroupBox5.Controls.Add(Me.Txt_NombreCliente)
        Me.GroupBox5.Controls.Add(Me.C1SuperLabel11)
        Me.GroupBox5.Controls.Add(Me.C1SuperLabel37)
        Me.GroupBox5.Controls.Add(Me.C1SuperLabel13)
        Me.GroupBox5.Controls.Add(Me.C1SuperLabel40)
        Me.GroupBox5.Controls.Add(Me.C1SuperLabel15)
        Me.GroupBox5.Controls.Add(Me.C1SuperLabel18)
        Me.GroupBox5.Controls.Add(Me.C1SuperLabel17)
        Me.GroupBox5.Location = New System.Drawing.Point(2, 0)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(288, 281)
        Me.GroupBox5.TabIndex = 6
        Me.GroupBox5.TabStop = False
        '
        'Cmb_Asesores
        '
        Me.Cmb_Asesores.AddItemSeparator = Global.Microsoft.VisualBasic.ChrW(59)
        Me.Cmb_Asesores.AllowColMove = False
        Me.Cmb_Asesores.AllowSort = False
        Me.Cmb_Asesores.AutoDropDown = True
        Me.Cmb_Asesores.AutoSelect = True
        Me.Cmb_Asesores.AutoSize = False
        Me.Cmb_Asesores.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Cmb_Asesores.Caption = ""
        Me.Cmb_Asesores.CaptionHeight = 18
        Me.Cmb_Asesores.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.Cmb_Asesores.ColumnCaptionHeight = 18
        Me.Cmb_Asesores.ColumnFooterHeight = 18
        Me.Cmb_Asesores.ColumnHeaders = False
        Me.Cmb_Asesores.ComboStyle = C1.Win.C1List.ComboStyleEnum.DropdownList
        Me.Cmb_Asesores.ContentHeight = 12
        Me.Cmb_Asesores.DeadAreaBackColor = System.Drawing.Color.Empty
        Me.Cmb_Asesores.EditorBackColor = System.Drawing.Color.White
        Me.Cmb_Asesores.EditorFont = New System.Drawing.Font("Arial", 7.0!, System.Drawing.FontStyle.Bold)
        Me.Cmb_Asesores.EditorForeColor = System.Drawing.Color.SteelBlue
        Me.Cmb_Asesores.EditorHeight = 12
        Me.Cmb_Asesores.ExtendRightColumn = True
        Me.Cmb_Asesores.FlatStyle = C1.Win.C1List.FlatModeEnum.Flat
        Me.Cmb_Asesores.Font = New System.Drawing.Font("Arial", 7.0!, System.Drawing.FontStyle.Bold)
        Me.Cmb_Asesores.Images.Add(CType(resources.GetObject("Cmb_Asesores.Images"), System.Drawing.Image))
        Me.Cmb_Asesores.ItemHeight = 18
        Me.Cmb_Asesores.Location = New System.Drawing.Point(8, 244)
        Me.Cmb_Asesores.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Cmb_Asesores.MatchEntry = C1.Win.C1List.MatchEntryEnum.Extended
        Me.Cmb_Asesores.MatchEntryTimeout = CType(500, Long)
        Me.Cmb_Asesores.MaxDropDownItems = CType(5, Short)
        Me.Cmb_Asesores.MaxLength = 0
        Me.Cmb_Asesores.MinimumSize = New System.Drawing.Size(10, 16)
        Me.Cmb_Asesores.MouseCursor = System.Windows.Forms.Cursors.Default
        Me.Cmb_Asesores.Name = "Cmb_Asesores"
        Me.Cmb_Asesores.RowSubDividerColor = System.Drawing.Color.DarkGray
        Me.Cmb_Asesores.Size = New System.Drawing.Size(256, 16)
        Me.Cmb_Asesores.TabIndex = 1034
        Me.Cmb_Asesores.VisualStyle = C1.Win.C1List.VisualStyle.Office2007Silver
        Me.Cmb_Asesores.PropBag = resources.GetString("Cmb_Asesores.PropBag")
        '
        'C1SuperLabel21
        '
        Me.C1SuperLabel21.AutoSize = True
        Me.C1SuperLabel21.BackColor = System.Drawing.Color.Transparent
        Me.C1SuperLabel21.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.C1SuperLabel21.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.C1SuperLabel21.Location = New System.Drawing.Point(6, 229)
        Me.C1SuperLabel21.Margin = New System.Windows.Forms.Padding(2, 4, 2, 4)
        Me.C1SuperLabel21.Name = "C1SuperLabel21"
        Me.C1SuperLabel21.Size = New System.Drawing.Size(36, 19)
        Me.C1SuperLabel21.TabIndex = 1035
        Me.C1SuperLabel21.Text = "Asesor"
        Me.C1SuperLabel21.UseMnemonic = True
        '
        'Cmb_Sucursal
        '
        Me.Cmb_Sucursal.AddItemSeparator = Global.Microsoft.VisualBasic.ChrW(59)
        Me.Cmb_Sucursal.AllowColMove = False
        Me.Cmb_Sucursal.AllowSort = False
        Me.Cmb_Sucursal.AutoDropDown = True
        Me.Cmb_Sucursal.AutoSelect = True
        Me.Cmb_Sucursal.AutoSize = False
        Me.Cmb_Sucursal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Cmb_Sucursal.Caption = ""
        Me.Cmb_Sucursal.CaptionHeight = 18
        Me.Cmb_Sucursal.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.Cmb_Sucursal.ColumnCaptionHeight = 18
        Me.Cmb_Sucursal.ColumnFooterHeight = 18
        Me.Cmb_Sucursal.ColumnHeaders = False
        Me.Cmb_Sucursal.ComboStyle = C1.Win.C1List.ComboStyleEnum.DropdownList
        Me.Cmb_Sucursal.ContentHeight = 12
        Me.Cmb_Sucursal.DeadAreaBackColor = System.Drawing.Color.Empty
        Me.Cmb_Sucursal.EditorBackColor = System.Drawing.Color.White
        Me.Cmb_Sucursal.EditorFont = New System.Drawing.Font("Arial", 7.0!, System.Drawing.FontStyle.Bold)
        Me.Cmb_Sucursal.EditorForeColor = System.Drawing.Color.SteelBlue
        Me.Cmb_Sucursal.EditorHeight = 12
        Me.Cmb_Sucursal.ExtendRightColumn = True
        Me.Cmb_Sucursal.FlatStyle = C1.Win.C1List.FlatModeEnum.Flat
        Me.Cmb_Sucursal.Font = New System.Drawing.Font("Arial", 7.0!, System.Drawing.FontStyle.Bold)
        Me.Cmb_Sucursal.Images.Add(CType(resources.GetObject("Cmb_Sucursal.Images"), System.Drawing.Image))
        Me.Cmb_Sucursal.ItemHeight = 18
        Me.Cmb_Sucursal.Location = New System.Drawing.Point(8, 212)
        Me.Cmb_Sucursal.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Cmb_Sucursal.MatchEntry = C1.Win.C1List.MatchEntryEnum.Extended
        Me.Cmb_Sucursal.MatchEntryTimeout = CType(500, Long)
        Me.Cmb_Sucursal.MaxDropDownItems = CType(5, Short)
        Me.Cmb_Sucursal.MaxLength = 0
        Me.Cmb_Sucursal.MinimumSize = New System.Drawing.Size(10, 16)
        Me.Cmb_Sucursal.MouseCursor = System.Windows.Forms.Cursors.Default
        Me.Cmb_Sucursal.Name = "Cmb_Sucursal"
        Me.Cmb_Sucursal.RowSubDividerColor = System.Drawing.Color.DarkGray
        Me.Cmb_Sucursal.Size = New System.Drawing.Size(256, 16)
        Me.Cmb_Sucursal.TabIndex = 8
        Me.Cmb_Sucursal.VisualStyle = C1.Win.C1List.VisualStyle.Office2007Silver
        Me.Cmb_Sucursal.PropBag = resources.GetString("Cmb_Sucursal.PropBag")
        '
        'Cmb_Segmento
        '
        Me.Cmb_Segmento.AddItemSeparator = Global.Microsoft.VisualBasic.ChrW(59)
        Me.Cmb_Segmento.AllowColMove = False
        Me.Cmb_Segmento.AllowSort = False
        Me.Cmb_Segmento.AutoDropDown = True
        Me.Cmb_Segmento.AutoSelect = True
        Me.Cmb_Segmento.AutoSize = False
        Me.Cmb_Segmento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Cmb_Segmento.Caption = ""
        Me.Cmb_Segmento.CaptionHeight = 18
        Me.Cmb_Segmento.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.Cmb_Segmento.ColumnCaptionHeight = 18
        Me.Cmb_Segmento.ColumnFooterHeight = 18
        Me.Cmb_Segmento.ColumnHeaders = False
        Me.Cmb_Segmento.ComboStyle = C1.Win.C1List.ComboStyleEnum.DropdownList
        Me.Cmb_Segmento.ContentHeight = 12
        Me.Cmb_Segmento.DeadAreaBackColor = System.Drawing.Color.Empty
        Me.Cmb_Segmento.EditorBackColor = System.Drawing.Color.White
        Me.Cmb_Segmento.EditorFont = New System.Drawing.Font("Arial", 7.0!, System.Drawing.FontStyle.Bold)
        Me.Cmb_Segmento.EditorForeColor = System.Drawing.Color.SteelBlue
        Me.Cmb_Segmento.EditorHeight = 12
        Me.Cmb_Segmento.ExtendRightColumn = True
        Me.Cmb_Segmento.FlatStyle = C1.Win.C1List.FlatModeEnum.Flat
        Me.Cmb_Segmento.Font = New System.Drawing.Font("Arial", 7.0!, System.Drawing.FontStyle.Bold)
        Me.Cmb_Segmento.Images.Add(CType(resources.GetObject("Cmb_Segmento.Images"), System.Drawing.Image))
        Me.Cmb_Segmento.ItemHeight = 18
        Me.Cmb_Segmento.Location = New System.Drawing.Point(8, 180)
        Me.Cmb_Segmento.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Cmb_Segmento.MatchEntry = C1.Win.C1List.MatchEntryEnum.Extended
        Me.Cmb_Segmento.MatchEntryTimeout = CType(500, Long)
        Me.Cmb_Segmento.MaxDropDownItems = CType(5, Short)
        Me.Cmb_Segmento.MaxLength = 0
        Me.Cmb_Segmento.MinimumSize = New System.Drawing.Size(10, 16)
        Me.Cmb_Segmento.MouseCursor = System.Windows.Forms.Cursors.Default
        Me.Cmb_Segmento.Name = "Cmb_Segmento"
        Me.Cmb_Segmento.RowSubDividerColor = System.Drawing.Color.DarkGray
        Me.Cmb_Segmento.Size = New System.Drawing.Size(256, 16)
        Me.Cmb_Segmento.TabIndex = 7
        Me.Cmb_Segmento.VisualStyle = C1.Win.C1List.VisualStyle.Office2007Silver
        Me.Cmb_Segmento.PropBag = resources.GetString("Cmb_Segmento.PropBag")
        '
        'Cmb_PerfilRiesgo
        '
        Me.Cmb_PerfilRiesgo.AddItemSeparator = Global.Microsoft.VisualBasic.ChrW(59)
        Me.Cmb_PerfilRiesgo.AllowColMove = False
        Me.Cmb_PerfilRiesgo.AllowSort = False
        Me.Cmb_PerfilRiesgo.AutoDropDown = True
        Me.Cmb_PerfilRiesgo.AutoSelect = True
        Me.Cmb_PerfilRiesgo.AutoSize = False
        Me.Cmb_PerfilRiesgo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Cmb_PerfilRiesgo.Caption = ""
        Me.Cmb_PerfilRiesgo.CaptionHeight = 18
        Me.Cmb_PerfilRiesgo.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.Cmb_PerfilRiesgo.ColumnCaptionHeight = 18
        Me.Cmb_PerfilRiesgo.ColumnFooterHeight = 18
        Me.Cmb_PerfilRiesgo.ColumnHeaders = False
        Me.Cmb_PerfilRiesgo.ComboStyle = C1.Win.C1List.ComboStyleEnum.DropdownList
        Me.Cmb_PerfilRiesgo.ContentHeight = 12
        Me.Cmb_PerfilRiesgo.DeadAreaBackColor = System.Drawing.Color.Empty
        Me.Cmb_PerfilRiesgo.EditorBackColor = System.Drawing.Color.White
        Me.Cmb_PerfilRiesgo.EditorFont = New System.Drawing.Font("Arial", 7.0!, System.Drawing.FontStyle.Bold)
        Me.Cmb_PerfilRiesgo.EditorForeColor = System.Drawing.Color.SteelBlue
        Me.Cmb_PerfilRiesgo.EditorHeight = 12
        Me.Cmb_PerfilRiesgo.ExtendRightColumn = True
        Me.Cmb_PerfilRiesgo.FlatStyle = C1.Win.C1List.FlatModeEnum.Flat
        Me.Cmb_PerfilRiesgo.Font = New System.Drawing.Font("Arial", 7.0!, System.Drawing.FontStyle.Bold)
        Me.Cmb_PerfilRiesgo.Images.Add(CType(resources.GetObject("Cmb_PerfilRiesgo.Images"), System.Drawing.Image))
        Me.Cmb_PerfilRiesgo.ItemHeight = 18
        Me.Cmb_PerfilRiesgo.Location = New System.Drawing.Point(8, 148)
        Me.Cmb_PerfilRiesgo.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Cmb_PerfilRiesgo.MatchEntry = C1.Win.C1List.MatchEntryEnum.Extended
        Me.Cmb_PerfilRiesgo.MatchEntryTimeout = CType(500, Long)
        Me.Cmb_PerfilRiesgo.MaxDropDownItems = CType(5, Short)
        Me.Cmb_PerfilRiesgo.MaxLength = 0
        Me.Cmb_PerfilRiesgo.MinimumSize = New System.Drawing.Size(10, 16)
        Me.Cmb_PerfilRiesgo.MouseCursor = System.Windows.Forms.Cursors.Default
        Me.Cmb_PerfilRiesgo.Name = "Cmb_PerfilRiesgo"
        Me.Cmb_PerfilRiesgo.RowSubDividerColor = System.Drawing.Color.DarkGray
        Me.Cmb_PerfilRiesgo.Size = New System.Drawing.Size(256, 16)
        Me.Cmb_PerfilRiesgo.TabIndex = 6
        Me.Cmb_PerfilRiesgo.VisualStyle = C1.Win.C1List.VisualStyle.Office2007Silver
        Me.Cmb_PerfilRiesgo.PropBag = resources.GetString("Cmb_PerfilRiesgo.PropBag")
        '
        'Cmb_TipoAdministracion
        '
        Me.Cmb_TipoAdministracion.AddItemSeparator = Global.Microsoft.VisualBasic.ChrW(59)
        Me.Cmb_TipoAdministracion.AllowColMove = False
        Me.Cmb_TipoAdministracion.AllowSort = False
        Me.Cmb_TipoAdministracion.AutoDropDown = True
        Me.Cmb_TipoAdministracion.AutoSelect = True
        Me.Cmb_TipoAdministracion.AutoSize = False
        Me.Cmb_TipoAdministracion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Cmb_TipoAdministracion.Caption = ""
        Me.Cmb_TipoAdministracion.CaptionHeight = 18
        Me.Cmb_TipoAdministracion.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.Cmb_TipoAdministracion.ColumnCaptionHeight = 18
        Me.Cmb_TipoAdministracion.ColumnFooterHeight = 18
        Me.Cmb_TipoAdministracion.ColumnHeaders = False
        Me.Cmb_TipoAdministracion.ComboStyle = C1.Win.C1List.ComboStyleEnum.DropdownList
        Me.Cmb_TipoAdministracion.ContentHeight = 12
        Me.Cmb_TipoAdministracion.DeadAreaBackColor = System.Drawing.Color.Empty
        Me.Cmb_TipoAdministracion.EditorBackColor = System.Drawing.Color.White
        Me.Cmb_TipoAdministracion.EditorFont = New System.Drawing.Font("Arial", 7.0!, System.Drawing.FontStyle.Bold)
        Me.Cmb_TipoAdministracion.EditorForeColor = System.Drawing.Color.SteelBlue
        Me.Cmb_TipoAdministracion.EditorHeight = 12
        Me.Cmb_TipoAdministracion.ExtendRightColumn = True
        Me.Cmb_TipoAdministracion.FlatStyle = C1.Win.C1List.FlatModeEnum.Flat
        Me.Cmb_TipoAdministracion.Font = New System.Drawing.Font("Arial", 7.0!, System.Drawing.FontStyle.Bold)
        Me.Cmb_TipoAdministracion.Images.Add(CType(resources.GetObject("Cmb_TipoAdministracion.Images"), System.Drawing.Image))
        Me.Cmb_TipoAdministracion.ItemHeight = 18
        Me.Cmb_TipoAdministracion.Location = New System.Drawing.Point(8, 116)
        Me.Cmb_TipoAdministracion.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Cmb_TipoAdministracion.MatchEntry = C1.Win.C1List.MatchEntryEnum.Extended
        Me.Cmb_TipoAdministracion.MatchEntryTimeout = CType(500, Long)
        Me.Cmb_TipoAdministracion.MaxDropDownItems = CType(5, Short)
        Me.Cmb_TipoAdministracion.MaxLength = 0
        Me.Cmb_TipoAdministracion.MinimumSize = New System.Drawing.Size(10, 16)
        Me.Cmb_TipoAdministracion.MouseCursor = System.Windows.Forms.Cursors.Default
        Me.Cmb_TipoAdministracion.Name = "Cmb_TipoAdministracion"
        Me.Cmb_TipoAdministracion.RowSubDividerColor = System.Drawing.Color.DarkGray
        Me.Cmb_TipoAdministracion.Size = New System.Drawing.Size(256, 16)
        Me.Cmb_TipoAdministracion.TabIndex = 5
        Me.Cmb_TipoAdministracion.VisualStyle = C1.Win.C1List.VisualStyle.Office2007Silver
        Me.Cmb_TipoAdministracion.PropBag = resources.GetString("Cmb_TipoAdministracion.PropBag")
        '
        'Cmb_NegocioCuentas
        '
        Me.Cmb_NegocioCuentas.AddItemSeparator = Global.Microsoft.VisualBasic.ChrW(59)
        Me.Cmb_NegocioCuentas.AllowColMove = False
        Me.Cmb_NegocioCuentas.AllowSort = False
        Me.Cmb_NegocioCuentas.AutoDropDown = True
        Me.Cmb_NegocioCuentas.AutoSelect = True
        Me.Cmb_NegocioCuentas.AutoSize = False
        Me.Cmb_NegocioCuentas.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Cmb_NegocioCuentas.Caption = ""
        Me.Cmb_NegocioCuentas.CaptionHeight = 18
        Me.Cmb_NegocioCuentas.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.Cmb_NegocioCuentas.ColumnCaptionHeight = 18
        Me.Cmb_NegocioCuentas.ColumnFooterHeight = 18
        Me.Cmb_NegocioCuentas.ColumnHeaders = False
        Me.Cmb_NegocioCuentas.ComboStyle = C1.Win.C1List.ComboStyleEnum.DropdownList
        Me.Cmb_NegocioCuentas.ContentHeight = 12
        Me.Cmb_NegocioCuentas.DeadAreaBackColor = System.Drawing.Color.Empty
        Me.Cmb_NegocioCuentas.EditorBackColor = System.Drawing.Color.White
        Me.Cmb_NegocioCuentas.EditorFont = New System.Drawing.Font("Arial", 7.0!, System.Drawing.FontStyle.Bold)
        Me.Cmb_NegocioCuentas.EditorForeColor = System.Drawing.Color.SteelBlue
        Me.Cmb_NegocioCuentas.EditorHeight = 12
        Me.Cmb_NegocioCuentas.ExtendRightColumn = True
        Me.Cmb_NegocioCuentas.FlatStyle = C1.Win.C1List.FlatModeEnum.Flat
        Me.Cmb_NegocioCuentas.Font = New System.Drawing.Font("Arial", 7.0!, System.Drawing.FontStyle.Bold)
        Me.Cmb_NegocioCuentas.Images.Add(CType(resources.GetObject("Cmb_NegocioCuentas.Images"), System.Drawing.Image))
        Me.Cmb_NegocioCuentas.ItemHeight = 18
        Me.Cmb_NegocioCuentas.Location = New System.Drawing.Point(8, 20)
        Me.Cmb_NegocioCuentas.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Cmb_NegocioCuentas.MatchEntry = C1.Win.C1List.MatchEntryEnum.Extended
        Me.Cmb_NegocioCuentas.MatchEntryTimeout = CType(500, Long)
        Me.Cmb_NegocioCuentas.MaxDropDownItems = CType(5, Short)
        Me.Cmb_NegocioCuentas.MaxLength = 0
        Me.Cmb_NegocioCuentas.MinimumSize = New System.Drawing.Size(10, 16)
        Me.Cmb_NegocioCuentas.MouseCursor = System.Windows.Forms.Cursors.Default
        Me.Cmb_NegocioCuentas.Name = "Cmb_NegocioCuentas"
        Me.Cmb_NegocioCuentas.RowSubDividerColor = System.Drawing.Color.DarkGray
        Me.Cmb_NegocioCuentas.Size = New System.Drawing.Size(256, 16)
        Me.Cmb_NegocioCuentas.TabIndex = 0
        Me.Cmb_NegocioCuentas.VisualStyle = C1.Win.C1List.VisualStyle.Office2007Silver
        Me.Cmb_NegocioCuentas.PropBag = resources.GetString("Cmb_NegocioCuentas.PropBag")
        '
        'Txt_RutCliente
        '
        Me.Txt_RutCliente.AcceptsEscape = False
        Me.Txt_RutCliente.AcceptsReturn = True
        Me.Txt_RutCliente.AcceptsTab = True
        Me.Txt_RutCliente.AutoSize = False
        Me.Txt_RutCliente.BackColor = System.Drawing.Color.White
        Me.Txt_RutCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Txt_RutCliente.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.Txt_RutCliente.DisabledForeColor = System.Drawing.Color.Lavender
        Me.Txt_RutCliente.Font = New System.Drawing.Font("Arial", 7.0!, System.Drawing.FontStyle.Bold)
        Me.Txt_RutCliente.ForeColor = System.Drawing.Color.SteelBlue
        Me.Txt_RutCliente.Location = New System.Drawing.Point(8, 84)
        Me.Txt_RutCliente.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.Txt_RutCliente.MaxLength = 12
        Me.Txt_RutCliente.Name = "Txt_RutCliente"
        Me.Txt_RutCliente.Size = New System.Drawing.Size(83, 16)
        Me.Txt_RutCliente.TabIndex = 1032
        Me.Txt_RutCliente.Tag = Nothing
        Me.Txt_RutCliente.TextDetached = True
        Me.Txt_RutCliente.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Silver
        '
        'C1SuperLabel20
        '
        Me.C1SuperLabel20.AutoSize = True
        Me.C1SuperLabel20.BackColor = System.Drawing.Color.Transparent
        Me.C1SuperLabel20.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.C1SuperLabel20.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.C1SuperLabel20.Location = New System.Drawing.Point(6, 68)
        Me.C1SuperLabel20.Margin = New System.Windows.Forms.Padding(2, 4, 2, 4)
        Me.C1SuperLabel20.Name = "C1SuperLabel20"
        Me.C1SuperLabel20.Size = New System.Drawing.Size(59, 19)
        Me.C1SuperLabel20.TabIndex = 1033
        Me.C1SuperLabel20.Text = "Rut Cliente"
        Me.C1SuperLabel20.UseMnemonic = True
        '
        'Txt_NombreCuenta
        '
        Me.Txt_NombreCuenta.AcceptsEscape = False
        Me.Txt_NombreCuenta.AcceptsReturn = True
        Me.Txt_NombreCuenta.AcceptsTab = True
        Me.Txt_NombreCuenta.AutoSize = False
        Me.Txt_NombreCuenta.BackColor = System.Drawing.Color.White
        Me.Txt_NombreCuenta.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Txt_NombreCuenta.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.Txt_NombreCuenta.DisabledForeColor = System.Drawing.Color.Lavender
        Me.Txt_NombreCuenta.Font = New System.Drawing.Font("Arial", 7.0!, System.Drawing.FontStyle.Bold)
        Me.Txt_NombreCuenta.ForeColor = System.Drawing.Color.SteelBlue
        Me.Txt_NombreCuenta.Location = New System.Drawing.Point(94, 52)
        Me.Txt_NombreCuenta.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.Txt_NombreCuenta.MaxLength = 100
        Me.Txt_NombreCuenta.Name = "Txt_NombreCuenta"
        Me.Txt_NombreCuenta.Size = New System.Drawing.Size(170, 16)
        Me.Txt_NombreCuenta.TabIndex = 2
        Me.Txt_NombreCuenta.Tag = Nothing
        Me.Txt_NombreCuenta.TextDetached = True
        Me.Txt_NombreCuenta.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Silver
        '
        'Btn_BuscarCuentasDisponibles
        '
        Me.Btn_BuscarCuentasDisponibles.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.Btn_BuscarCuentasDisponibles.Font = New System.Drawing.Font("Arial", 7.5!)
        Me.Btn_BuscarCuentasDisponibles.Location = New System.Drawing.Point(177, 262)
        Me.Btn_BuscarCuentasDisponibles.Margin = New System.Windows.Forms.Padding(2)
        Me.Btn_BuscarCuentasDisponibles.Name = "Btn_BuscarCuentasDisponibles"
        Me.Btn_BuscarCuentasDisponibles.Size = New System.Drawing.Size(51, 19)
        Me.Btn_BuscarCuentasDisponibles.TabIndex = 9
        Me.Btn_BuscarCuentasDisponibles.TabStop = False
        Me.Btn_BuscarCuentasDisponibles.Tag = ""
        Me.Btn_BuscarCuentasDisponibles.Text = "&Buscar"
        Me.Btn_BuscarCuentasDisponibles.UseVisualStyleBackColor = True
        Me.Btn_BuscarCuentasDisponibles.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Silver
        '
        'Btn_LimpiarFiltroCuentas
        '
        Me.Btn_LimpiarFiltroCuentas.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.Btn_LimpiarFiltroCuentas.Font = New System.Drawing.Font("Arial", 7.5!)
        Me.Btn_LimpiarFiltroCuentas.Location = New System.Drawing.Point(231, 262)
        Me.Btn_LimpiarFiltroCuentas.Margin = New System.Windows.Forms.Padding(2)
        Me.Btn_LimpiarFiltroCuentas.Name = "Btn_LimpiarFiltroCuentas"
        Me.Btn_LimpiarFiltroCuentas.Size = New System.Drawing.Size(51, 19)
        Me.Btn_LimpiarFiltroCuentas.TabIndex = 10
        Me.Btn_LimpiarFiltroCuentas.TabStop = False
        Me.Btn_LimpiarFiltroCuentas.Tag = ""
        Me.Btn_LimpiarFiltroCuentas.Text = "&Limpiar"
        Me.Btn_LimpiarFiltroCuentas.UseVisualStyleBackColor = True
        Me.Btn_LimpiarFiltroCuentas.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Silver
        '
        'C1SuperLabel19
        '
        Me.C1SuperLabel19.AutoSize = True
        Me.C1SuperLabel19.BackColor = System.Drawing.Color.Transparent
        Me.C1SuperLabel19.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.C1SuperLabel19.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.C1SuperLabel19.Location = New System.Drawing.Point(6, 4)
        Me.C1SuperLabel19.Margin = New System.Windows.Forms.Padding(2, 4, 2, 4)
        Me.C1SuperLabel19.Name = "C1SuperLabel19"
        Me.C1SuperLabel19.Size = New System.Drawing.Size(45, 19)
        Me.C1SuperLabel19.TabIndex = 1031
        Me.C1SuperLabel19.Text = "Negocio"
        Me.C1SuperLabel19.UseMnemonic = True
        '
        'Btn_BuscarCuenta
        '
        Me.Btn_BuscarCuenta.ForeColor = System.Drawing.Color.MidnightBlue
        Me.Btn_BuscarCuenta.Location = New System.Drawing.Point(265, 52)
        Me.Btn_BuscarCuenta.Margin = New System.Windows.Forms.Padding(2)
        Me.Btn_BuscarCuenta.Name = "Btn_BuscarCuenta"
        Me.Btn_BuscarCuenta.Size = New System.Drawing.Size(17, 16)
        Me.Btn_BuscarCuenta.TabIndex = 2
        Me.Btn_BuscarCuenta.TabStop = False
        Me.Btn_BuscarCuenta.Text = "..."
        Me.Btn_BuscarCuenta.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Btn_BuscarCuenta.UseVisualStyleBackColor = True
        Me.Btn_BuscarCuenta.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Silver
        '
        'Txt_NumeroCuenta
        '
        Me.Txt_NumeroCuenta.AcceptsEscape = False
        Me.Txt_NumeroCuenta.AcceptsReturn = True
        Me.Txt_NumeroCuenta.AcceptsTab = True
        Me.Txt_NumeroCuenta.AutoSize = False
        Me.Txt_NumeroCuenta.BackColor = System.Drawing.Color.White
        Me.Txt_NumeroCuenta.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Txt_NumeroCuenta.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.Txt_NumeroCuenta.DisabledForeColor = System.Drawing.Color.Lavender
        Me.Txt_NumeroCuenta.Font = New System.Drawing.Font("Arial", 7.0!, System.Drawing.FontStyle.Bold)
        Me.Txt_NumeroCuenta.ForeColor = System.Drawing.Color.SteelBlue
        Me.Txt_NumeroCuenta.Location = New System.Drawing.Point(8, 52)
        Me.Txt_NumeroCuenta.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.Txt_NumeroCuenta.MaxLength = 15
        Me.Txt_NumeroCuenta.Name = "Txt_NumeroCuenta"
        Me.Txt_NumeroCuenta.Size = New System.Drawing.Size(83, 16)
        Me.Txt_NumeroCuenta.TabIndex = 1
        Me.Txt_NumeroCuenta.Tag = Nothing
        Me.Txt_NumeroCuenta.TextDetached = True
        Me.Txt_NumeroCuenta.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Silver
        '
        'Btn_BuscarCliente
        '
        Me.Btn_BuscarCliente.ForeColor = System.Drawing.Color.MidnightBlue
        Me.Btn_BuscarCliente.Location = New System.Drawing.Point(265, 84)
        Me.Btn_BuscarCliente.Margin = New System.Windows.Forms.Padding(2)
        Me.Btn_BuscarCliente.Name = "Btn_BuscarCliente"
        Me.Btn_BuscarCliente.Size = New System.Drawing.Size(17, 16)
        Me.Btn_BuscarCliente.TabIndex = 4
        Me.Btn_BuscarCliente.TabStop = False
        Me.Btn_BuscarCliente.Text = "..."
        Me.Btn_BuscarCliente.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Btn_BuscarCliente.UseVisualStyleBackColor = True
        Me.Btn_BuscarCliente.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Silver
        '
        'Txt_NombreCliente
        '
        Me.Txt_NombreCliente.AcceptsEscape = False
        Me.Txt_NombreCliente.AcceptsReturn = True
        Me.Txt_NombreCliente.AcceptsTab = True
        Me.Txt_NombreCliente.AutoSize = False
        Me.Txt_NombreCliente.BackColor = System.Drawing.Color.White
        Me.Txt_NombreCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Txt_NombreCliente.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.Txt_NombreCliente.DisabledForeColor = System.Drawing.Color.Lavender
        Me.Txt_NombreCliente.Enabled = False
        Me.Txt_NombreCliente.Font = New System.Drawing.Font("Arial", 7.0!, System.Drawing.FontStyle.Bold)
        Me.Txt_NombreCliente.ForeColor = System.Drawing.Color.SteelBlue
        Me.Txt_NombreCliente.Location = New System.Drawing.Point(94, 84)
        Me.Txt_NombreCliente.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.Txt_NombreCliente.MaxLength = 100
        Me.Txt_NombreCliente.Name = "Txt_NombreCliente"
        Me.Txt_NombreCliente.Size = New System.Drawing.Size(170, 16)
        Me.Txt_NombreCliente.TabIndex = 3
        Me.Txt_NombreCliente.Tag = Nothing
        Me.Txt_NombreCliente.TextDetached = True
        Me.Txt_NombreCliente.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Silver
        '
        'C1SuperLabel11
        '
        Me.C1SuperLabel11.AutoSize = True
        Me.C1SuperLabel11.BackColor = System.Drawing.Color.Transparent
        Me.C1SuperLabel11.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.C1SuperLabel11.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.C1SuperLabel11.Location = New System.Drawing.Point(6, 36)
        Me.C1SuperLabel11.Margin = New System.Windows.Forms.Padding(2, 4, 2, 4)
        Me.C1SuperLabel11.Name = "C1SuperLabel11"
        Me.C1SuperLabel11.Size = New System.Drawing.Size(54, 19)
        Me.C1SuperLabel11.TabIndex = 1016
        Me.C1SuperLabel11.Text = "Nº Cuenta"
        Me.C1SuperLabel11.UseMnemonic = True
        '
        'C1SuperLabel37
        '
        Me.C1SuperLabel37.AutoSize = True
        Me.C1SuperLabel37.BackColor = System.Drawing.Color.Transparent
        Me.C1SuperLabel37.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.C1SuperLabel37.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.C1SuperLabel37.Location = New System.Drawing.Point(6, 132)
        Me.C1SuperLabel37.Margin = New System.Windows.Forms.Padding(2, 4, 2, 4)
        Me.C1SuperLabel37.Name = "C1SuperLabel37"
        Me.C1SuperLabel37.Size = New System.Drawing.Size(66, 19)
        Me.C1SuperLabel37.TabIndex = 1017
        Me.C1SuperLabel37.Text = "Perfil Riesgo"
        Me.C1SuperLabel37.UseMnemonic = True
        '
        'C1SuperLabel13
        '
        Me.C1SuperLabel13.AutoSize = True
        Me.C1SuperLabel13.BackColor = System.Drawing.Color.Transparent
        Me.C1SuperLabel13.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.C1SuperLabel13.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.C1SuperLabel13.Location = New System.Drawing.Point(94, 36)
        Me.C1SuperLabel13.Margin = New System.Windows.Forms.Padding(2, 4, 2, 4)
        Me.C1SuperLabel13.Name = "C1SuperLabel13"
        Me.C1SuperLabel13.Size = New System.Drawing.Size(80, 19)
        Me.C1SuperLabel13.TabIndex = 1015
        Me.C1SuperLabel13.Text = "Nombre Cuenta"
        Me.C1SuperLabel13.UseMnemonic = True
        '
        'C1SuperLabel40
        '
        Me.C1SuperLabel40.AutoSize = True
        Me.C1SuperLabel40.BackColor = System.Drawing.Color.Transparent
        Me.C1SuperLabel40.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.C1SuperLabel40.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.C1SuperLabel40.Location = New System.Drawing.Point(94, 68)
        Me.C1SuperLabel40.Margin = New System.Windows.Forms.Padding(2, 4, 2, 4)
        Me.C1SuperLabel40.Name = "C1SuperLabel40"
        Me.C1SuperLabel40.Size = New System.Drawing.Size(80, 19)
        Me.C1SuperLabel40.TabIndex = 1014
        Me.C1SuperLabel40.Text = "Nombre Cliente"
        Me.C1SuperLabel40.UseMnemonic = True
        '
        'C1SuperLabel15
        '
        Me.C1SuperLabel15.AutoSize = True
        Me.C1SuperLabel15.BackColor = System.Drawing.Color.Transparent
        Me.C1SuperLabel15.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.C1SuperLabel15.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.C1SuperLabel15.Location = New System.Drawing.Point(6, 100)
        Me.C1SuperLabel15.Margin = New System.Windows.Forms.Padding(2, 4, 2, 4)
        Me.C1SuperLabel15.Name = "C1SuperLabel15"
        Me.C1SuperLabel15.Size = New System.Drawing.Size(101, 19)
        Me.C1SuperLabel15.TabIndex = 1025
        Me.C1SuperLabel15.Text = "Tipo Administración"
        Me.C1SuperLabel15.UseMnemonic = True
        '
        'C1SuperLabel18
        '
        Me.C1SuperLabel18.AutoSize = True
        Me.C1SuperLabel18.BackColor = System.Drawing.Color.Transparent
        Me.C1SuperLabel18.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.C1SuperLabel18.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.C1SuperLabel18.Location = New System.Drawing.Point(6, 164)
        Me.C1SuperLabel18.Margin = New System.Windows.Forms.Padding(2, 4, 2, 4)
        Me.C1SuperLabel18.Name = "C1SuperLabel18"
        Me.C1SuperLabel18.Size = New System.Drawing.Size(55, 19)
        Me.C1SuperLabel18.TabIndex = 1028
        Me.C1SuperLabel18.Text = "Segmento"
        Me.C1SuperLabel18.UseMnemonic = True
        '
        'C1SuperLabel17
        '
        Me.C1SuperLabel17.AutoSize = True
        Me.C1SuperLabel17.BackColor = System.Drawing.Color.Transparent
        Me.C1SuperLabel17.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.C1SuperLabel17.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.C1SuperLabel17.Location = New System.Drawing.Point(6, 196)
        Me.C1SuperLabel17.Margin = New System.Windows.Forms.Padding(2, 4, 2, 4)
        Me.C1SuperLabel17.Name = "C1SuperLabel17"
        Me.C1SuperLabel17.Size = New System.Drawing.Size(46, 19)
        Me.C1SuperLabel17.TabIndex = 1026
        Me.C1SuperLabel17.Text = "Sucursal"
        Me.C1SuperLabel17.UseMnemonic = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.Tdg_CuentasAsociadas)
        Me.GroupBox3.Location = New System.Drawing.Point(537, 0)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(208, 281)
        Me.GroupBox3.TabIndex = 5
        Me.GroupBox3.TabStop = False
        '
        'Tdg_CuentasAsociadas
        '
        Me.Tdg_CuentasAsociadas.AllowColSelect = False
        Me.Tdg_CuentasAsociadas.AllowRowSizing = C1.Win.C1TrueDBGrid.RowSizingEnum.None
        Me.Tdg_CuentasAsociadas.AllowUpdate = False
        Me.Tdg_CuentasAsociadas.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Tdg_CuentasAsociadas.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Tdg_CuentasAsociadas.Caption = "A S O C I A D A S"
        Me.Tdg_CuentasAsociadas.CaptionHeight = 21
        Me.Tdg_CuentasAsociadas.CollapseColor = System.Drawing.Color.WhiteSmoke
        Me.Tdg_CuentasAsociadas.ColumnHeaders = False
        Me.Tdg_CuentasAsociadas.DirectionAfterEnter = C1.Win.C1TrueDBGrid.DirectionAfterEnterEnum.MoveDown
        Me.Tdg_CuentasAsociadas.ExpandColor = System.Drawing.Color.WhiteSmoke
        Me.Tdg_CuentasAsociadas.Font = New System.Drawing.Font("Arial", 7.5!)
        Me.Tdg_CuentasAsociadas.ForeColor = System.Drawing.Color.Lavender
        Me.Tdg_CuentasAsociadas.GroupByAreaVisible = False
        Me.Tdg_CuentasAsociadas.GroupByCaption = "Arrastre hasta aquí las columnas por las que desea agrupar"
        Me.Tdg_CuentasAsociadas.Images.Add(CType(resources.GetObject("Tdg_CuentasAsociadas.Images"), System.Drawing.Image))
        Me.Tdg_CuentasAsociadas.Location = New System.Drawing.Point(1, 8)
        Me.Tdg_CuentasAsociadas.MaintainRowCurrency = True
        Me.Tdg_CuentasAsociadas.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.Tdg_CuentasAsociadas.MarqueeStyle = C1.Win.C1TrueDBGrid.MarqueeEnum.HighlightRow
        Me.Tdg_CuentasAsociadas.MultiSelect = C1.Win.C1TrueDBGrid.MultiSelectEnum.Simple
        Me.Tdg_CuentasAsociadas.Name = "Tdg_CuentasAsociadas"
        Me.Tdg_CuentasAsociadas.PreviewInfo.Location = New System.Drawing.Point(0, 0)
        Me.Tdg_CuentasAsociadas.PreviewInfo.Size = New System.Drawing.Size(0, 0)
        Me.Tdg_CuentasAsociadas.PreviewInfo.ZoomFactor = 75.0R
        Me.Tdg_CuentasAsociadas.PrintInfo.PageSettings = CType(resources.GetObject("Tdg_CuentasAsociadas.PrintInfo.PageSettings"), System.Drawing.Printing.PageSettings)
        Me.Tdg_CuentasAsociadas.RecordSelectors = False
        Me.Tdg_CuentasAsociadas.RecordSelectorWidth = 15
        Me.Tdg_CuentasAsociadas.RowDivider.Color = System.Drawing.Color.DarkGray
        Me.Tdg_CuentasAsociadas.RowDivider.Style = C1.Win.C1TrueDBGrid.LineStyleEnum.Raised
        Me.Tdg_CuentasAsociadas.RowHeight = 15
        Me.Tdg_CuentasAsociadas.ScrollTips = True
        Me.Tdg_CuentasAsociadas.Size = New System.Drawing.Size(204, 247)
        Me.Tdg_CuentasAsociadas.SplitDividerSize = New System.Drawing.Size(0, 0)
        Me.Tdg_CuentasAsociadas.TabIndex = 0
        Me.Tdg_CuentasAsociadas.TabStop = False
        Me.Tdg_CuentasAsociadas.Text = "C1TrueDBGrid1"
        Me.Tdg_CuentasAsociadas.VisualStyle = C1.Win.C1TrueDBGrid.VisualStyle.Office2007Silver
        Me.Tdg_CuentasAsociadas.PropBag = resources.GetString("Tdg_CuentasAsociadas.PropBag")
        '
        'Btn_AsociarCuenta
        '
        Me.Btn_AsociarCuenta.BackColor = System.Drawing.Color.Gainsboro
        Me.Btn_AsociarCuenta.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.Btn_AsociarCuenta.Font = New System.Drawing.Font("Arial", 7.5!)
        Me.Btn_AsociarCuenta.ForeColor = System.Drawing.Color.MediumBlue
        Me.Btn_AsociarCuenta.Location = New System.Drawing.Point(510, 130)
        Me.Btn_AsociarCuenta.Margin = New System.Windows.Forms.Padding(2)
        Me.Btn_AsociarCuenta.Name = "Btn_AsociarCuenta"
        Me.Btn_AsociarCuenta.Size = New System.Drawing.Size(24, 20)
        Me.Btn_AsociarCuenta.TabIndex = 4
        Me.Btn_AsociarCuenta.TabStop = False
        Me.Btn_AsociarCuenta.Tag = ""
        Me.Btn_AsociarCuenta.Text = "►"
        Me.Btn_AsociarCuenta.UseVisualStyleBackColor = True
        Me.Btn_AsociarCuenta.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Silver
        '
        'Btn_QuitarCuenta
        '
        Me.Btn_QuitarCuenta.BackColor = System.Drawing.Color.Gainsboro
        Me.Btn_QuitarCuenta.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.Btn_QuitarCuenta.Font = New System.Drawing.Font("Arial", 7.5!)
        Me.Btn_QuitarCuenta.ForeColor = System.Drawing.Color.MediumBlue
        Me.Btn_QuitarCuenta.Location = New System.Drawing.Point(510, 109)
        Me.Btn_QuitarCuenta.Margin = New System.Windows.Forms.Padding(2)
        Me.Btn_QuitarCuenta.Name = "Btn_QuitarCuenta"
        Me.Btn_QuitarCuenta.Size = New System.Drawing.Size(24, 20)
        Me.Btn_QuitarCuenta.TabIndex = 3
        Me.Btn_QuitarCuenta.TabStop = False
        Me.Btn_QuitarCuenta.Tag = ""
        Me.Btn_QuitarCuenta.Text = "◄"
        Me.Btn_QuitarCuenta.UseVisualStyleBackColor = True
        Me.Btn_QuitarCuenta.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Silver
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.Tdg_CuentasDisponibles)
        Me.GroupBox4.Location = New System.Drawing.Point(293, 0)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(213, 281)
        Me.GroupBox4.TabIndex = 7
        Me.GroupBox4.TabStop = False
        '
        'Tdg_CuentasDisponibles
        '
        Me.Tdg_CuentasDisponibles.AllowColSelect = False
        Me.Tdg_CuentasDisponibles.AllowRowSizing = C1.Win.C1TrueDBGrid.RowSizingEnum.None
        Me.Tdg_CuentasDisponibles.AllowUpdate = False
        Me.Tdg_CuentasDisponibles.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Tdg_CuentasDisponibles.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Tdg_CuentasDisponibles.Caption = "D I S P O N I B L E S"
        Me.Tdg_CuentasDisponibles.CaptionHeight = 21
        Me.Tdg_CuentasDisponibles.CollapseColor = System.Drawing.Color.WhiteSmoke
        Me.Tdg_CuentasDisponibles.ColumnHeaders = False
        Me.Tdg_CuentasDisponibles.DirectionAfterEnter = C1.Win.C1TrueDBGrid.DirectionAfterEnterEnum.MoveDown
        Me.Tdg_CuentasDisponibles.ExpandColor = System.Drawing.Color.WhiteSmoke
        Me.Tdg_CuentasDisponibles.Font = New System.Drawing.Font("Arial", 7.5!)
        Me.Tdg_CuentasDisponibles.ForeColor = System.Drawing.Color.Lavender
        Me.Tdg_CuentasDisponibles.GroupByAreaVisible = False
        Me.Tdg_CuentasDisponibles.GroupByCaption = "Arrastre hasta aquí las columnas por las que desea agrupar"
        Me.Tdg_CuentasDisponibles.Images.Add(CType(resources.GetObject("Tdg_CuentasDisponibles.Images"), System.Drawing.Image))
        Me.Tdg_CuentasDisponibles.Location = New System.Drawing.Point(1, 8)
        Me.Tdg_CuentasDisponibles.MaintainRowCurrency = True
        Me.Tdg_CuentasDisponibles.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.Tdg_CuentasDisponibles.MarqueeStyle = C1.Win.C1TrueDBGrid.MarqueeEnum.HighlightRow
        Me.Tdg_CuentasDisponibles.MultiSelect = C1.Win.C1TrueDBGrid.MultiSelectEnum.Simple
        Me.Tdg_CuentasDisponibles.Name = "Tdg_CuentasDisponibles"
        Me.Tdg_CuentasDisponibles.PreviewInfo.Location = New System.Drawing.Point(0, 0)
        Me.Tdg_CuentasDisponibles.PreviewInfo.Size = New System.Drawing.Size(0, 0)
        Me.Tdg_CuentasDisponibles.PreviewInfo.ZoomFactor = 75.0R
        Me.Tdg_CuentasDisponibles.PrintInfo.PageSettings = CType(resources.GetObject("Tdg_CuentasDisponibles.PrintInfo.PageSettings"), System.Drawing.Printing.PageSettings)
        Me.Tdg_CuentasDisponibles.RecordSelectors = False
        Me.Tdg_CuentasDisponibles.RecordSelectorWidth = 15
        Me.Tdg_CuentasDisponibles.RowDivider.Color = System.Drawing.Color.DarkGray
        Me.Tdg_CuentasDisponibles.RowDivider.Style = C1.Win.C1TrueDBGrid.LineStyleEnum.Raised
        Me.Tdg_CuentasDisponibles.RowHeight = 15
        Me.Tdg_CuentasDisponibles.ScrollTips = True
        Me.Tdg_CuentasDisponibles.Size = New System.Drawing.Size(209, 246)
        Me.Tdg_CuentasDisponibles.SplitDividerSize = New System.Drawing.Size(0, 0)
        Me.Tdg_CuentasDisponibles.TabIndex = 0
        Me.Tdg_CuentasDisponibles.TabStop = False
        Me.Tdg_CuentasDisponibles.Text = "C1TrueDBGrid2"
        Me.Tdg_CuentasDisponibles.VisualStyle = C1.Win.C1TrueDBGrid.VisualStyle.Office2007Silver
        Me.Tdg_CuentasDisponibles.PropBag = resources.GetString("Tdg_CuentasDisponibles.PropBag")
        '
        'C1SuperTooltip1
        '
        Me.C1SuperTooltip1.Font = New System.Drawing.Font("Tahoma", 8.0!)
        Me.C1SuperTooltip1.RoundedCorners = True
        '
        'StatusStrip1
        '
        Me.StatusStrip1.AutoSize = False
        Me.StatusStrip1.BackColor = System.Drawing.Color.DarkGray
        Me.StatusStrip1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.StatusLabel})
        Me.StatusStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Flow
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 597)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(746, 18)
        Me.StatusStrip1.Stretch = False
        Me.StatusStrip1.TabIndex = 1038
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'StatusLabel
        '
        Me.StatusLabel.BackColor = System.Drawing.Color.Transparent
        Me.StatusLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Bold)
        Me.StatusLabel.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.StatusLabel.Name = "StatusLabel"
        Me.StatusLabel.Size = New System.Drawing.Size(127, 14)
        Me.StatusLabel.Text = "ToolStripStatusLabel1"
        '
        'Ctrl_Botonera
        '
        Me.Ctrl_Botonera.BackColor = System.Drawing.Color.DarkGray
        Me.Ctrl_Botonera.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Ctrl_Botonera_Agregar, Me.Ctrl_Botonera_Buscar, Me.ToolStripSeparator1, Me.Ctrl_Botonera_Guardar, Me.Ctrl_Botonera_Editar, Me.Ctrl_Botonera_Eliminar, Me.Ctrl_Botonera_Verificar, Me.Ctrl_Botonera_Procesar, Me.ToolStripSeparator2, Me.Ctrl_Botonera_Imprimir, Me.Ctrl_Botonera_Excel, Me.Ctrl_Botonera_PDF, Me.Ctrl_Botonera_Columnas, Me.ToolStripSeparator3, Me.Ctrl_Botonera_Cancelar, Me.Ctrl_Combo_Plantillas})
        Me.Ctrl_Botonera.Location = New System.Drawing.Point(0, 0)
        Me.Ctrl_Botonera.Name = "Ctrl_Botonera"
        Me.Ctrl_Botonera.Size = New System.Drawing.Size(746, 25)
        Me.Ctrl_Botonera.TabIndex = 1137
        Me.Ctrl_Botonera.Text = "ToolStrip1"
        '
        'Ctrl_Botonera_Agregar
        '
        Me.Ctrl_Botonera_Agregar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Ctrl_Botonera_Agregar.Enabled = False
        Me.Ctrl_Botonera_Agregar.Image = CType(resources.GetObject("Ctrl_Botonera_Agregar.Image"), System.Drawing.Image)
        Me.Ctrl_Botonera_Agregar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.Ctrl_Botonera_Agregar.Name = "Ctrl_Botonera_Agregar"
        Me.Ctrl_Botonera_Agregar.Size = New System.Drawing.Size(23, 22)
        Me.Ctrl_Botonera_Agregar.Text = "ToolStripButton2"
        Me.Ctrl_Botonera_Agregar.ToolTipText = "Agregar"
        '
        'Ctrl_Botonera_Buscar
        '
        Me.Ctrl_Botonera_Buscar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Ctrl_Botonera_Buscar.Enabled = False
        Me.Ctrl_Botonera_Buscar.Image = CType(resources.GetObject("Ctrl_Botonera_Buscar.Image"), System.Drawing.Image)
        Me.Ctrl_Botonera_Buscar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.Ctrl_Botonera_Buscar.Name = "Ctrl_Botonera_Buscar"
        Me.Ctrl_Botonera_Buscar.Size = New System.Drawing.Size(23, 22)
        Me.Ctrl_Botonera_Buscar.Text = "Buscar"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'Ctrl_Botonera_Guardar
        '
        Me.Ctrl_Botonera_Guardar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Ctrl_Botonera_Guardar.Enabled = False
        Me.Ctrl_Botonera_Guardar.Image = CType(resources.GetObject("Ctrl_Botonera_Guardar.Image"), System.Drawing.Image)
        Me.Ctrl_Botonera_Guardar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.Ctrl_Botonera_Guardar.Name = "Ctrl_Botonera_Guardar"
        Me.Ctrl_Botonera_Guardar.Size = New System.Drawing.Size(23, 22)
        Me.Ctrl_Botonera_Guardar.Text = "Guardar"
        '
        'Ctrl_Botonera_Editar
        '
        Me.Ctrl_Botonera_Editar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Ctrl_Botonera_Editar.Enabled = False
        Me.Ctrl_Botonera_Editar.Image = CType(resources.GetObject("Ctrl_Botonera_Editar.Image"), System.Drawing.Image)
        Me.Ctrl_Botonera_Editar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.Ctrl_Botonera_Editar.Name = "Ctrl_Botonera_Editar"
        Me.Ctrl_Botonera_Editar.Size = New System.Drawing.Size(23, 22)
        Me.Ctrl_Botonera_Editar.Text = "Editar"
        '
        'Ctrl_Botonera_Eliminar
        '
        Me.Ctrl_Botonera_Eliminar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Ctrl_Botonera_Eliminar.Enabled = False
        Me.Ctrl_Botonera_Eliminar.Image = CType(resources.GetObject("Ctrl_Botonera_Eliminar.Image"), System.Drawing.Image)
        Me.Ctrl_Botonera_Eliminar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.Ctrl_Botonera_Eliminar.Name = "Ctrl_Botonera_Eliminar"
        Me.Ctrl_Botonera_Eliminar.Size = New System.Drawing.Size(23, 22)
        Me.Ctrl_Botonera_Eliminar.Text = "Eliminar"
        '
        'Ctrl_Botonera_Verificar
        '
        Me.Ctrl_Botonera_Verificar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Ctrl_Botonera_Verificar.Enabled = False
        Me.Ctrl_Botonera_Verificar.Image = CType(resources.GetObject("Ctrl_Botonera_Verificar.Image"), System.Drawing.Image)
        Me.Ctrl_Botonera_Verificar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.Ctrl_Botonera_Verificar.Name = "Ctrl_Botonera_Verificar"
        Me.Ctrl_Botonera_Verificar.Size = New System.Drawing.Size(23, 22)
        Me.Ctrl_Botonera_Verificar.Text = "Verificar"
        '
        'Ctrl_Botonera_Procesar
        '
        Me.Ctrl_Botonera_Procesar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Ctrl_Botonera_Procesar.Enabled = False
        Me.Ctrl_Botonera_Procesar.Image = CType(resources.GetObject("Ctrl_Botonera_Procesar.Image"), System.Drawing.Image)
        Me.Ctrl_Botonera_Procesar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.Ctrl_Botonera_Procesar.Name = "Ctrl_Botonera_Procesar"
        Me.Ctrl_Botonera_Procesar.Size = New System.Drawing.Size(23, 22)
        Me.Ctrl_Botonera_Procesar.Text = "Procesar"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'Ctrl_Botonera_Imprimir
        '
        Me.Ctrl_Botonera_Imprimir.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Ctrl_Botonera_Imprimir.Enabled = False
        Me.Ctrl_Botonera_Imprimir.Image = CType(resources.GetObject("Ctrl_Botonera_Imprimir.Image"), System.Drawing.Image)
        Me.Ctrl_Botonera_Imprimir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.Ctrl_Botonera_Imprimir.Name = "Ctrl_Botonera_Imprimir"
        Me.Ctrl_Botonera_Imprimir.Size = New System.Drawing.Size(23, 22)
        Me.Ctrl_Botonera_Imprimir.Text = "Imprimir"
        Me.Ctrl_Botonera_Imprimir.ToolTipText = "Imprimir"
        '
        'Ctrl_Botonera_Excel
        '
        Me.Ctrl_Botonera_Excel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Ctrl_Botonera_Excel.Enabled = False
        Me.Ctrl_Botonera_Excel.Image = CType(resources.GetObject("Ctrl_Botonera_Excel.Image"), System.Drawing.Image)
        Me.Ctrl_Botonera_Excel.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.Ctrl_Botonera_Excel.Name = "Ctrl_Botonera_Excel"
        Me.Ctrl_Botonera_Excel.Size = New System.Drawing.Size(23, 22)
        Me.Ctrl_Botonera_Excel.Text = "Exportar a Excel"
        '
        'Ctrl_Botonera_PDF
        '
        Me.Ctrl_Botonera_PDF.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Ctrl_Botonera_PDF.Enabled = False
        Me.Ctrl_Botonera_PDF.Image = CType(resources.GetObject("Ctrl_Botonera_PDF.Image"), System.Drawing.Image)
        Me.Ctrl_Botonera_PDF.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.Ctrl_Botonera_PDF.Name = "Ctrl_Botonera_PDF"
        Me.Ctrl_Botonera_PDF.Size = New System.Drawing.Size(23, 22)
        Me.Ctrl_Botonera_PDF.Text = "Exportar a PDF"
        '
        'Ctrl_Botonera_Columnas
        '
        Me.Ctrl_Botonera_Columnas.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Ctrl_Botonera_Columnas.Enabled = False
        Me.Ctrl_Botonera_Columnas.Image = CType(resources.GetObject("Ctrl_Botonera_Columnas.Image"), System.Drawing.Image)
        Me.Ctrl_Botonera_Columnas.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.Ctrl_Botonera_Columnas.Name = "Ctrl_Botonera_Columnas"
        Me.Ctrl_Botonera_Columnas.Size = New System.Drawing.Size(23, 22)
        Me.Ctrl_Botonera_Columnas.Text = "Configuración Columnas"
        Me.Ctrl_Botonera_Columnas.ToolTipText = "Configuración Columnas"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(6, 25)
        '
        'Ctrl_Botonera_Cancelar
        '
        Me.Ctrl_Botonera_Cancelar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Ctrl_Botonera_Cancelar.Enabled = False
        Me.Ctrl_Botonera_Cancelar.Image = CType(resources.GetObject("Ctrl_Botonera_Cancelar.Image"), System.Drawing.Image)
        Me.Ctrl_Botonera_Cancelar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.Ctrl_Botonera_Cancelar.Name = "Ctrl_Botonera_Cancelar"
        Me.Ctrl_Botonera_Cancelar.Size = New System.Drawing.Size(23, 22)
        Me.Ctrl_Botonera_Cancelar.Text = "Cancelar"
        '
        'Ctrl_Combo_Plantillas
        '
        Me.Ctrl_Combo_Plantillas.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.Ctrl_Combo_Plantillas.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Ctrl_Combo_Plantillas.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Ctrl_Combo_Plantillas.Name = "Ctrl_Combo_Plantillas"
        Me.Ctrl_Combo_Plantillas.Size = New System.Drawing.Size(121, 25)
        Me.Ctrl_Combo_Plantillas.Visible = False
        '
        'Frm_MantenedorUsuarios
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.DimGray
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.ClientSize = New System.Drawing.Size(746, 615)
        Me.Controls.Add(Me.Ctrl_Botonera)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.Pnl_Datos_TipoOperacion)
        Me.Controls.Add(Me.C1SuperLabel14)
        Me.Controls.Add(Me.C1SuperLabel1)
        Me.Controls.Add(Me.Tab_Contenedor)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Arial", 7.5!)
        Me.ForeColor = System.Drawing.Color.DimGray
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Name = "Frm_MantenedorUsuarios"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Mantenedor de Usuarios del Sistema"
        CType(Me.txt_NCuentaBancoOperaciones_b, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Pnl_DatosUsuario.ResumeLayout(False)
        Me.Pnl_DatosUsuario.PerformLayout()
        CType(Me.txtEmailUsuario, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Cmb_TipoControl, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Cmb_Estado, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Cmb_TipoUsuario, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Cmb_Unidad, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Ded_FechaLiquidacion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Txt_Telefono, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Txt_CodigoExterno, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Txt_Login, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Txt_NombreCorto, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Txt_Cargo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Txt_NombreUsuario, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Pnl_Datos_TipoOperacion.ResumeLayout(False)
        CType(Me.Tdg_Usuarios, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Pnl_PerfilesUsuarios.ResumeLayout(False)
        CType(Me.Cmb_NegocioPerfiles, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.Tdg_PerfilesAsignados, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.Tdg_PerfilesDisponibles, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Tab_Contenedor, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Tab_Contenedor.ResumeLayout(False)
        Me.Tab_Usuario.ResumeLayout(False)
        Me.Tab_PerfilesUsuario.ResumeLayout(False)
        Me.Tab_CuentasUsuario.ResumeLayout(False)
        Me.Pnl_CuentasUsuario.ResumeLayout(False)
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        CType(Me.Cmb_Asesores, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Cmb_Sucursal, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Cmb_Segmento, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Cmb_PerfilRiesgo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Cmb_TipoAdministracion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Cmb_NegocioCuentas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Txt_RutCliente, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Txt_NombreCuenta, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Txt_NumeroCuenta, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Txt_NombreCliente, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        CType(Me.Tdg_CuentasAsociadas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox4.ResumeLayout(False)
        CType(Me.Tdg_CuentasDisponibles, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.Ctrl_Botonera.ResumeLayout(False)
        Me.Ctrl_Botonera.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txt_NCuentaBancoOperaciones_b As C1.Win.C1Input.C1TextBox
    Friend WithEvents Pnl_DatosUsuario As System.Windows.Forms.Panel
    Friend WithEvents C1SuperLabel14 As C1.Win.C1SuperTooltip.C1SuperLabel

    Friend WithEvents C1SuperLabel1 As C1.Win.C1SuperTooltip.C1SuperLabel

    Friend WithEvents Btn_Modificar As C1.Win.C1Input.C1Button

    Friend WithEvents Btn_VerPerfiles As C1.Win.C1Input.C1Button
    Friend WithEvents Btn_ReiniciaPassword As C1.Win.C1Input.C1Button
    Friend WithEvents C1SuperLabel22 As C1.Win.C1SuperTooltip.C1SuperLabel
    Friend WithEvents C1SuperLabel10 As C1.Win.C1SuperTooltip.C1SuperLabel
    Friend WithEvents C1SuperLabel9 As C1.Win.C1SuperTooltip.C1SuperLabel
    Friend WithEvents C1SuperLabel8 As C1.Win.C1SuperTooltip.C1SuperLabel
    Friend WithEvents C1SuperLabel6 As C1.Win.C1SuperTooltip.C1SuperLabel
    Friend WithEvents C1SuperLabel5 As C1.Win.C1SuperTooltip.C1SuperLabel
    Friend WithEvents C1SuperLabel4 As C1.Win.C1SuperTooltip.C1SuperLabel
    Friend WithEvents C1SuperLabel3 As C1.Win.C1SuperTooltip.C1SuperLabel
    Friend WithEvents C1SuperLabel2 As C1.Win.C1SuperTooltip.C1SuperLabel
    Friend WithEvents C1SuperLabel12 As C1.Win.C1SuperTooltip.C1SuperLabel
    Friend WithEvents Txt_NombreUsuario As C1.Win.C1Input.C1TextBox
    Friend WithEvents Pnl_Datos_TipoOperacion As System.Windows.Forms.Panel
    Friend WithEvents Tdg_Usuarios As C1.Win.C1TrueDBGrid.C1TrueDBGrid

    Friend WithEvents Pnl_PerfilesUsuarios As System.Windows.Forms.Panel
    Friend WithEvents Btn_QuitarPerfilUsuario As C1.Win.C1Input.C1Button
    Friend WithEvents Tdg_PerfilesDisponibles As C1.Win.C1TrueDBGrid.C1TrueDBGrid
    Friend WithEvents Tdg_PerfilesAsignados As C1.Win.C1TrueDBGrid.C1TrueDBGrid
    Friend WithEvents Btn_AsociarPerfilUsuario As C1.Win.C1Input.C1Button
    Friend WithEvents Txt_NombreCorto As C1.Win.C1Input.C1TextBox
    Friend WithEvents Txt_Cargo As C1.Win.C1Input.C1TextBox
    Friend WithEvents Txt_Telefono As C1.Win.C1Input.C1TextBox
    Friend WithEvents Txt_CodigoExterno As C1.Win.C1Input.C1TextBox
    Friend WithEvents Txt_Login As C1.Win.C1Input.C1TextBox
    Friend WithEvents Chk_TodasCuentas As System.Windows.Forms.CheckBox
    Friend WithEvents Tab_Contenedor As C1.Win.C1Command.C1DockingTab
    Friend WithEvents Tab_Usuario As C1.Win.C1Command.C1DockingTabPage
    Friend WithEvents Tab_PerfilesUsuario As C1.Win.C1Command.C1DockingTabPage
    Friend WithEvents Tab_CuentasUsuario As C1.Win.C1Command.C1DockingTabPage
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Pnl_CuentasUsuario As System.Windows.Forms.Panel
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Tdg_CuentasAsociadas As C1.Win.C1TrueDBGrid.C1TrueDBGrid
    Friend WithEvents Btn_AsociarCuenta As C1.Win.C1Input.C1Button
    Friend WithEvents Btn_QuitarCuenta As C1.Win.C1Input.C1Button
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents Btn_BuscarCuenta As C1.Win.C1Input.C1Button
    Friend WithEvents Txt_NumeroCuenta As C1.Win.C1Input.C1TextBox
    Friend WithEvents Btn_BuscarCliente As C1.Win.C1Input.C1Button
    Friend WithEvents Txt_NombreCliente As C1.Win.C1Input.C1TextBox
    Friend WithEvents C1SuperLabel11 As C1.Win.C1SuperTooltip.C1SuperLabel
    Friend WithEvents C1SuperLabel13 As C1.Win.C1SuperTooltip.C1SuperLabel
    Friend WithEvents C1SuperLabel37 As C1.Win.C1SuperTooltip.C1SuperLabel
    Friend WithEvents C1SuperLabel40 As C1.Win.C1SuperTooltip.C1SuperLabel
    Friend WithEvents C1SuperLabel16 As C1.Win.C1SuperTooltip.C1SuperLabel
    Friend WithEvents C1SuperLabel15 As C1.Win.C1SuperTooltip.C1SuperLabel
    Friend WithEvents C1SuperLabel17 As C1.Win.C1SuperTooltip.C1SuperLabel
    Friend WithEvents C1SuperLabel18 As C1.Win.C1SuperTooltip.C1SuperLabel
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents Tdg_CuentasDisponibles As C1.Win.C1TrueDBGrid.C1TrueDBGrid
    Friend WithEvents C1SuperLabel19 As C1.Win.C1SuperTooltip.C1SuperLabel
    Friend WithEvents Txt_NombreCuenta As C1.Win.C1Input.C1TextBox
    Friend WithEvents Btn_BuscarCuentasDisponibles As C1.Win.C1Input.C1Button
    Friend WithEvents Btn_LimpiarFiltroCuentas As C1.Win.C1Input.C1Button
    Public WithEvents Ded_FechaLiquidacion As C1.Win.C1Input.C1DateEdit
    Friend WithEvents Txt_RutCliente As C1.Win.C1Input.C1TextBox
    Friend WithEvents C1SuperLabel20 As C1.Win.C1SuperTooltip.C1SuperLabel


    Friend WithEvents Cmb_Estado As C1.Win.C1List.C1Combo
    Friend WithEvents Cmb_TipoUsuario As C1.Win.C1List.C1Combo
    Friend WithEvents Cmb_Unidad As C1.Win.C1List.C1Combo
    Friend WithEvents Cmb_NegocioPerfiles As C1.Win.C1List.C1Combo
    Friend WithEvents Cmb_Sucursal As C1.Win.C1List.C1Combo
    Friend WithEvents Cmb_Segmento As C1.Win.C1List.C1Combo
    Friend WithEvents Cmb_PerfilRiesgo As C1.Win.C1List.C1Combo
    Friend WithEvents Cmb_TipoAdministracion As C1.Win.C1List.C1Combo
    Friend WithEvents Cmb_NegocioCuentas As C1.Win.C1List.C1Combo
    Friend WithEvents C1SuperTooltip1 As C1.Win.C1SuperTooltip.C1SuperTooltip
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents StatusLabel As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents Cmb_TipoControl As C1.Win.C1List.C1Combo
    Friend WithEvents C1SuperLabel7 As C1.Win.C1SuperTooltip.C1SuperLabel
    Friend WithEvents Ctrl_Botonera As System.Windows.Forms.ToolStrip
    Friend WithEvents Ctrl_Botonera_Agregar As System.Windows.Forms.ToolStripButton
    Friend WithEvents Ctrl_Botonera_Buscar As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents Ctrl_Botonera_Guardar As System.Windows.Forms.ToolStripButton
    Friend WithEvents Ctrl_Botonera_Editar As System.Windows.Forms.ToolStripButton
    Friend WithEvents Ctrl_Botonera_Eliminar As System.Windows.Forms.ToolStripButton
    Friend WithEvents Ctrl_Botonera_Verificar As System.Windows.Forms.ToolStripButton
    Friend WithEvents Ctrl_Botonera_Procesar As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents Ctrl_Botonera_Imprimir As System.Windows.Forms.ToolStripButton
    Friend WithEvents Ctrl_Botonera_Excel As System.Windows.Forms.ToolStripButton
    Friend WithEvents Ctrl_Botonera_PDF As System.Windows.Forms.ToolStripButton
    Friend WithEvents Ctrl_Botonera_Columnas As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents Ctrl_Botonera_Cancelar As System.Windows.Forms.ToolStripButton
    Friend WithEvents Ctrl_Combo_Plantillas As System.Windows.Forms.ToolStripComboBox
    Friend WithEvents Cmb_Asesores As C1.Win.C1List.C1Combo
    Friend WithEvents C1SuperLabel21 As C1.Win.C1SuperTooltip.C1SuperLabel
    Friend WithEvents txtEmailUsuario As C1.Win.C1Input.C1TextBox
    Friend WithEvents lblEmailUsuario As C1.Win.C1SuperTooltip.C1SuperLabel
End Class

