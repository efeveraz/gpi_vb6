Imports Gpi__.My.Resources

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Frm_MantenedorControlAcceso


    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Frm_MantenedorControlAcceso))
        Me.Pnl_UsuariosPerfil = New System.Windows.Forms.Panel
        Me.Cmb_Perfiles = New C1.Win.C1List.C1Combo
        Me.TabModulos = New C1.Win.C1Command.C1DockingTab
        Me.C1DockingTabPage1 = New C1.Win.C1Command.C1DockingTabPage
        Me.C1DockingTabPage2 = New C1.Win.C1Command.C1DockingTabPage
        Me.C1DockingTabPage3 = New C1.Win.C1Command.C1DockingTabPage
        Me.C1DockingTabPage4 = New C1.Win.C1Command.C1DockingTabPage
        Me.C1DockingTabPage5 = New C1.Win.C1Command.C1DockingTabPage
        Me.Btn_Grabar_PerfilesAcciones = New C1.Win.C1Input.C1Button
        Me.C1SuperLabel16 = New C1.Win.C1SuperTooltip.C1SuperLabel
        Me.Btn_Salir = New C1.Win.C1Input.C1Button
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip
        Me.StatusLabel = New System.Windows.Forms.ToolStripStatusLabel
        Me.Ctrl_Botonera = New System.Windows.Forms.ToolStrip
        Me.Ctrl_Botonera_Agregar = New System.Windows.Forms.ToolStripButton
        Me.Ctrl_Botonera_Buscar = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.Ctrl_Botonera_Guardar = New System.Windows.Forms.ToolStripButton
        Me.Ctrl_Botonera_Editar = New System.Windows.Forms.ToolStripButton
        Me.Ctrl_Botonera_Eliminar = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator
        Me.Ctrl_Botonera_Imprimir = New System.Windows.Forms.ToolStripButton
        Me.Ctrl_Botonera_Excel = New System.Windows.Forms.ToolStripButton
        Me.Ctrl_Botonera_Columnas = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator
        Me.Ctrl_Botonera_Cancelar = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator
        Me.Pnl_UsuariosPerfil.SuspendLayout()
        CType(Me.Cmb_Perfiles, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TabModulos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabModulos.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        Me.Ctrl_Botonera.SuspendLayout()
        Me.SuspendLayout()
        '
        'Pnl_UsuariosPerfil
        '
        Me.Pnl_UsuariosPerfil.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Pnl_UsuariosPerfil.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.Pnl_UsuariosPerfil.Controls.Add(Me.Cmb_Perfiles)
        Me.Pnl_UsuariosPerfil.Controls.Add(Me.TabModulos)
        Me.Pnl_UsuariosPerfil.Controls.Add(Me.Btn_Grabar_PerfilesAcciones)
        Me.Pnl_UsuariosPerfil.Controls.Add(Me.C1SuperLabel16)
        Me.Pnl_UsuariosPerfil.ForeColor = System.Drawing.Color.DimGray
        Me.Pnl_UsuariosPerfil.Location = New System.Drawing.Point(1, 27)
        Me.Pnl_UsuariosPerfil.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.Pnl_UsuariosPerfil.Name = "Pnl_UsuariosPerfil"
        Me.Pnl_UsuariosPerfil.Size = New System.Drawing.Size(604, 517)
        Me.Pnl_UsuariosPerfil.TabIndex = 1028
        Me.Pnl_UsuariosPerfil.TabStop = True
        '
        'Cmb_Perfiles
        '
        Me.Cmb_Perfiles.AddItemSeparator = Global.Microsoft.VisualBasic.ChrW(59)
        Me.Cmb_Perfiles.AllowColMove = False
        Me.Cmb_Perfiles.AllowSort = False
        Me.Cmb_Perfiles.AutoDropDown = True
        Me.Cmb_Perfiles.AutoSelect = True
        Me.Cmb_Perfiles.AutoSize = False
        Me.Cmb_Perfiles.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Cmb_Perfiles.Caption = ""
        Me.Cmb_Perfiles.CaptionHeight = 18
        Me.Cmb_Perfiles.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.Cmb_Perfiles.ColumnCaptionHeight = 18
        Me.Cmb_Perfiles.ColumnFooterHeight = 18
        Me.Cmb_Perfiles.ColumnHeaders = False
        Me.Cmb_Perfiles.ComboStyle = C1.Win.C1List.ComboStyleEnum.DropdownList
        Me.Cmb_Perfiles.ContentHeight = 12
        Me.Cmb_Perfiles.DeadAreaBackColor = System.Drawing.Color.Empty
        Me.Cmb_Perfiles.EditorBackColor = System.Drawing.Color.White
        Me.Cmb_Perfiles.EditorFont = New System.Drawing.Font("Arial", 7.0!, System.Drawing.FontStyle.Bold)
        Me.Cmb_Perfiles.EditorForeColor = System.Drawing.Color.SteelBlue
        Me.Cmb_Perfiles.EditorHeight = 12
        Me.Cmb_Perfiles.ExtendRightColumn = True
        Me.Cmb_Perfiles.FlatStyle = C1.Win.C1List.FlatModeEnum.Flat
        Me.Cmb_Perfiles.Font = New System.Drawing.Font("Arial", 7.0!, System.Drawing.FontStyle.Bold)
        Me.Cmb_Perfiles.Images.Add(CType(resources.GetObject("Cmb_Perfiles.Images"), System.Drawing.Image))
        Me.Cmb_Perfiles.ItemHeight = 18
        Me.Cmb_Perfiles.Location = New System.Drawing.Point(8, 20)
        Me.Cmb_Perfiles.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Cmb_Perfiles.MatchEntry = C1.Win.C1List.MatchEntryEnum.Extended
        Me.Cmb_Perfiles.MatchEntryTimeout = CType(500, Long)
        Me.Cmb_Perfiles.MaxDropDownItems = CType(5, Short)
        Me.Cmb_Perfiles.MaxLength = 0
        Me.Cmb_Perfiles.MinimumSize = New System.Drawing.Size(10, 16)
        Me.Cmb_Perfiles.MouseCursor = System.Windows.Forms.Cursors.Default
        Me.Cmb_Perfiles.Name = "Cmb_Perfiles"
        Me.Cmb_Perfiles.RowSubDividerColor = System.Drawing.Color.DarkGray
        Me.Cmb_Perfiles.Size = New System.Drawing.Size(417, 16)
        Me.Cmb_Perfiles.TabIndex = 0
        Me.Cmb_Perfiles.VisualStyle = C1.Win.C1List.VisualStyle.Office2007Silver
        Me.Cmb_Perfiles.PropBag = resources.GetString("Cmb_Perfiles.PropBag")
        '
        'TabModulos
        '
        Me.TabModulos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.TabModulos.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TabModulos.Controls.Add(Me.C1DockingTabPage1)
        Me.TabModulos.Controls.Add(Me.C1DockingTabPage2)
        Me.TabModulos.Controls.Add(Me.C1DockingTabPage3)
        Me.TabModulos.Controls.Add(Me.C1DockingTabPage4)
        Me.TabModulos.Controls.Add(Me.C1DockingTabPage5)
        Me.TabModulos.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold)
        Me.TabModulos.Indent = 8
        Me.TabModulos.ItemSize = New System.Drawing.Size(0, 21)
        Me.TabModulos.Location = New System.Drawing.Point(1, 43)
        Me.TabModulos.Margin = New System.Windows.Forms.Padding(0)
        Me.TabModulos.Name = "TabModulos"
        Me.TabModulos.Padding = New System.Drawing.Point(27, 10)
        Me.TabModulos.SelectedIndex = 1
        Me.TabModulos.Size = New System.Drawing.Size(604, 478)
        Me.TabModulos.TabAreaSpacing = 3
        Me.TabModulos.TabIndex = 1
        Me.TabModulos.TabLook = C1.Win.C1Command.ButtonLookFlags.Text
        Me.TabModulos.TabSizeMode = C1.Win.C1Command.TabSizeModeEnum.FillToEnd
        Me.TabModulos.TabsSpacing = 10
        Me.TabModulos.TabStyle = C1.Win.C1Command.TabStyleEnum.Office2007
        Me.TabModulos.VisualStyle = C1.Win.C1Command.VisualStyle.Custom
        Me.TabModulos.VisualStyleBase = C1.Win.C1Command.VisualStyle.Office2007Silver
        '
        'C1DockingTabPage1
        '
        Me.C1DockingTabPage1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.C1DockingTabPage1.BackColor = System.Drawing.Color.Gainsboro
        Me.C1DockingTabPage1.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.C1DockingTabPage1.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.C1DockingTabPage1.Location = New System.Drawing.Point(0, 25)
        Me.C1DockingTabPage1.Margin = New System.Windows.Forms.Padding(0)
        Me.C1DockingTabPage1.Name = "C1DockingTabPage1"
        Me.C1DockingTabPage1.Size = New System.Drawing.Size(604, 453)
        Me.C1DockingTabPage1.TabBackColor = System.Drawing.Color.Transparent
        Me.C1DockingTabPage1.TabIndex = 0
        Me.C1DockingTabPage1.TabVisible = False
        Me.C1DockingTabPage1.Text = "Page1"
        '
        'C1DockingTabPage2
        '
        Me.C1DockingTabPage2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.C1DockingTabPage2.BackColor = System.Drawing.Color.Gainsboro
        Me.C1DockingTabPage2.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.C1DockingTabPage2.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.C1DockingTabPage2.Location = New System.Drawing.Point(0, 25)
        Me.C1DockingTabPage2.Margin = New System.Windows.Forms.Padding(0)
        Me.C1DockingTabPage2.Name = "C1DockingTabPage2"
        Me.C1DockingTabPage2.Size = New System.Drawing.Size(604, 453)
        Me.C1DockingTabPage2.TabBackColor = System.Drawing.Color.Transparent
        Me.C1DockingTabPage2.TabIndex = 1
        Me.C1DockingTabPage2.TabVisible = False
        Me.C1DockingTabPage2.Text = "Page2"
        '
        'C1DockingTabPage3
        '
        Me.C1DockingTabPage3.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.C1DockingTabPage3.BackColor = System.Drawing.Color.Gainsboro
        Me.C1DockingTabPage3.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.C1DockingTabPage3.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.C1DockingTabPage3.Location = New System.Drawing.Point(0, 25)
        Me.C1DockingTabPage3.Margin = New System.Windows.Forms.Padding(0)
        Me.C1DockingTabPage3.Name = "C1DockingTabPage3"
        Me.C1DockingTabPage3.Size = New System.Drawing.Size(604, 453)
        Me.C1DockingTabPage3.TabBackColor = System.Drawing.Color.Transparent
        Me.C1DockingTabPage3.TabIndex = 2
        Me.C1DockingTabPage3.TabVisible = False
        Me.C1DockingTabPage3.Text = "Page3"
        '
        'C1DockingTabPage4
        '
        Me.C1DockingTabPage4.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.C1DockingTabPage4.BackColor = System.Drawing.Color.Gainsboro
        Me.C1DockingTabPage4.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.C1DockingTabPage4.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.C1DockingTabPage4.Location = New System.Drawing.Point(0, 25)
        Me.C1DockingTabPage4.Margin = New System.Windows.Forms.Padding(0)
        Me.C1DockingTabPage4.Name = "C1DockingTabPage4"
        Me.C1DockingTabPage4.Size = New System.Drawing.Size(604, 453)
        Me.C1DockingTabPage4.TabBackColor = System.Drawing.Color.Transparent
        Me.C1DockingTabPage4.TabIndex = 3
        Me.C1DockingTabPage4.TabVisible = False
        Me.C1DockingTabPage4.Text = "Page4"
        '
        'C1DockingTabPage5
        '
        Me.C1DockingTabPage5.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.C1DockingTabPage5.BackColor = System.Drawing.Color.Gainsboro
        Me.C1DockingTabPage5.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.C1DockingTabPage5.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.C1DockingTabPage5.Location = New System.Drawing.Point(0, 25)
        Me.C1DockingTabPage5.Margin = New System.Windows.Forms.Padding(0)
        Me.C1DockingTabPage5.Name = "C1DockingTabPage5"
        Me.C1DockingTabPage5.Size = New System.Drawing.Size(604, 453)
        Me.C1DockingTabPage5.TabBackColor = System.Drawing.Color.Transparent
        Me.C1DockingTabPage5.TabIndex = 4
        Me.C1DockingTabPage5.TabVisible = False
        Me.C1DockingTabPage5.Text = "Page5"
        '
        'Btn_Grabar_PerfilesAcciones
        '
        Me.Btn_Grabar_PerfilesAcciones.BackColor = System.Drawing.Color.Gainsboro
        Me.Btn_Grabar_PerfilesAcciones.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.Btn_Grabar_PerfilesAcciones.Font = New System.Drawing.Font("Arial", 7.5!)
        Me.Btn_Grabar_PerfilesAcciones.Location = New System.Drawing.Point(428, 16)
        Me.Btn_Grabar_PerfilesAcciones.Margin = New System.Windows.Forms.Padding(2)
        Me.Btn_Grabar_PerfilesAcciones.Name = "Btn_Grabar_PerfilesAcciones"
        Me.Btn_Grabar_PerfilesAcciones.Size = New System.Drawing.Size(65, 20)
        Me.Btn_Grabar_PerfilesAcciones.TabIndex = 1051
        Me.Btn_Grabar_PerfilesAcciones.TabStop = False
        Me.Btn_Grabar_PerfilesAcciones.Text = "&Grabar"
        Me.Btn_Grabar_PerfilesAcciones.UseVisualStyleBackColor = False
        Me.Btn_Grabar_PerfilesAcciones.Visible = False
        Me.Btn_Grabar_PerfilesAcciones.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Silver
        '
        'C1SuperLabel16
        '
        Me.C1SuperLabel16.AutoSize = True
        Me.C1SuperLabel16.BackColor = System.Drawing.Color.Transparent
        Me.C1SuperLabel16.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.C1SuperLabel16.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.C1SuperLabel16.Location = New System.Drawing.Point(6, 4)
        Me.C1SuperLabel16.Margin = New System.Windows.Forms.Padding(2, 4, 2, 4)
        Me.C1SuperLabel16.Name = "C1SuperLabel16"
        Me.C1SuperLabel16.Size = New System.Drawing.Size(31, 19)
        Me.C1SuperLabel16.TabIndex = 1049
        Me.C1SuperLabel16.Text = "Perfil"
        Me.C1SuperLabel16.UseMnemonic = True
        '
        'Btn_Salir
        '
        Me.Btn_Salir.BackColor = System.Drawing.Color.Transparent
        Me.Btn_Salir.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.Btn_Salir.FlatAppearance.BorderSize = 0
        Me.Btn_Salir.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray
        Me.Btn_Salir.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray
        Me.Btn_Salir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Btn_Salir.Font = New System.Drawing.Font("Arial", 8.25!)
        Me.Btn_Salir.ForeColor = System.Drawing.Color.White
        Me.Btn_Salir.Location = New System.Drawing.Point(446, 0)
        Me.Btn_Salir.Margin = New System.Windows.Forms.Padding(2)
        Me.Btn_Salir.Name = "Btn_Salir"
        Me.Btn_Salir.Size = New System.Drawing.Size(53, 22)
        Me.Btn_Salir.TabIndex = 1029
        Me.Btn_Salir.TabStop = False
        Me.Btn_Salir.Text = "&Salir  |x|"
        Me.Btn_Salir.UseVisualStyleBackColor = False
        '
        'StatusStrip1
        '
        Me.StatusStrip1.AutoSize = False
        Me.StatusStrip1.BackColor = System.Drawing.Color.DarkGray
        Me.StatusStrip1.Dock = System.Windows.Forms.DockStyle.None
        Me.StatusStrip1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.StatusLabel})
        Me.StatusStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Flow
        Me.StatusStrip1.Location = New System.Drawing.Point(1, 547)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(1000, 18)
        Me.StatusStrip1.Stretch = False
        Me.StatusStrip1.TabIndex = 1033
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'StatusLabel
        '
        Me.StatusLabel.BackColor = System.Drawing.Color.Transparent
        Me.StatusLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Bold)
        Me.StatusLabel.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.StatusLabel.Name = "StatusLabel"
        Me.StatusLabel.Size = New System.Drawing.Size(128, 14)
        Me.StatusLabel.Text = "ToolStripStatusLabel1"
        '
        'Ctrl_Botonera
        '
        Me.Ctrl_Botonera.BackColor = System.Drawing.Color.DarkGray
        Me.Ctrl_Botonera.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Ctrl_Botonera_Agregar, Me.Ctrl_Botonera_Buscar, Me.ToolStripSeparator1, Me.Ctrl_Botonera_Guardar, Me.Ctrl_Botonera_Editar, Me.Ctrl_Botonera_Eliminar, Me.ToolStripSeparator2, Me.Ctrl_Botonera_Imprimir, Me.Ctrl_Botonera_Excel, Me.Ctrl_Botonera_Columnas, Me.ToolStripSeparator3, Me.Ctrl_Botonera_Cancelar, Me.ToolStripSeparator4})
        Me.Ctrl_Botonera.Location = New System.Drawing.Point(0, 0)
        Me.Ctrl_Botonera.Name = "Ctrl_Botonera"
        Me.Ctrl_Botonera.Size = New System.Drawing.Size(604, 25)
        Me.Ctrl_Botonera.TabIndex = 1066
        Me.Ctrl_Botonera.Text = "ToolStrip1"
        '
        'Ctrl_Botonera_Agregar
        '
        Me.Ctrl_Botonera_Agregar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Ctrl_Botonera_Agregar.Enabled = False
        Me.Ctrl_Botonera_Agregar.Image = CType(resources.GetObject("Ctrl_Botonera_Agregar.Image"), System.Drawing.Image)
        Me.Ctrl_Botonera_Agregar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.Ctrl_Botonera_Agregar.Name = "Ctrl_Botonera_Agregar"
        Me.Ctrl_Botonera_Agregar.Size = New System.Drawing.Size(23, 22)
        Me.Ctrl_Botonera_Agregar.Text = "ToolStripButton2"
        Me.Ctrl_Botonera_Agregar.ToolTipText = "Agregar"
        '
        'Ctrl_Botonera_Buscar
        '
        Me.Ctrl_Botonera_Buscar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Ctrl_Botonera_Buscar.Enabled = False
        Me.Ctrl_Botonera_Buscar.Image = CType(resources.GetObject("Ctrl_Botonera_Buscar.Image"), System.Drawing.Image)
        Me.Ctrl_Botonera_Buscar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.Ctrl_Botonera_Buscar.Name = "Ctrl_Botonera_Buscar"
        Me.Ctrl_Botonera_Buscar.Size = New System.Drawing.Size(23, 22)
        Me.Ctrl_Botonera_Buscar.Text = "Buscar"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'Ctrl_Botonera_Guardar
        '
        Me.Ctrl_Botonera_Guardar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Ctrl_Botonera_Guardar.Image = CType(resources.GetObject("Ctrl_Botonera_Guardar.Image"), System.Drawing.Image)
        Me.Ctrl_Botonera_Guardar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.Ctrl_Botonera_Guardar.Name = "Ctrl_Botonera_Guardar"
        Me.Ctrl_Botonera_Guardar.Size = New System.Drawing.Size(23, 22)
        Me.Ctrl_Botonera_Guardar.Text = "Guardar"
        '
        'Ctrl_Botonera_Editar
        '
        Me.Ctrl_Botonera_Editar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Ctrl_Botonera_Editar.Enabled = False
        Me.Ctrl_Botonera_Editar.Image = CType(resources.GetObject("Ctrl_Botonera_Editar.Image"), System.Drawing.Image)
        Me.Ctrl_Botonera_Editar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.Ctrl_Botonera_Editar.Name = "Ctrl_Botonera_Editar"
        Me.Ctrl_Botonera_Editar.Size = New System.Drawing.Size(23, 22)
        Me.Ctrl_Botonera_Editar.Text = "Editar"
        '
        'Ctrl_Botonera_Eliminar
        '
        Me.Ctrl_Botonera_Eliminar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Ctrl_Botonera_Eliminar.Enabled = False
        Me.Ctrl_Botonera_Eliminar.Image = CType(resources.GetObject("Ctrl_Botonera_Eliminar.Image"), System.Drawing.Image)
        Me.Ctrl_Botonera_Eliminar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.Ctrl_Botonera_Eliminar.Name = "Ctrl_Botonera_Eliminar"
        Me.Ctrl_Botonera_Eliminar.Size = New System.Drawing.Size(23, 22)
        Me.Ctrl_Botonera_Eliminar.Text = "Eliminar"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'Ctrl_Botonera_Imprimir
        '
        Me.Ctrl_Botonera_Imprimir.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Ctrl_Botonera_Imprimir.Enabled = False
        Me.Ctrl_Botonera_Imprimir.Image = CType(resources.GetObject("Ctrl_Botonera_Imprimir.Image"), System.Drawing.Image)
        Me.Ctrl_Botonera_Imprimir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.Ctrl_Botonera_Imprimir.Name = "Ctrl_Botonera_Imprimir"
        Me.Ctrl_Botonera_Imprimir.Size = New System.Drawing.Size(23, 22)
        Me.Ctrl_Botonera_Imprimir.Text = "Imprimir"
        Me.Ctrl_Botonera_Imprimir.ToolTipText = "Imprimir"
        '
        'Ctrl_Botonera_Excel
        '
        Me.Ctrl_Botonera_Excel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Ctrl_Botonera_Excel.Enabled = False
        Me.Ctrl_Botonera_Excel.Image = CType(resources.GetObject("Ctrl_Botonera_Excel.Image"), System.Drawing.Image)
        Me.Ctrl_Botonera_Excel.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.Ctrl_Botonera_Excel.Name = "Ctrl_Botonera_Excel"
        Me.Ctrl_Botonera_Excel.Size = New System.Drawing.Size(23, 22)
        Me.Ctrl_Botonera_Excel.Text = "Exportar a Excel"
        '
        'Ctrl_Botonera_Columnas
        '
        Me.Ctrl_Botonera_Columnas.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Ctrl_Botonera_Columnas.Enabled = False
        Me.Ctrl_Botonera_Columnas.Image = CType(resources.GetObject("Ctrl_Botonera_Columnas.Image"), System.Drawing.Image)
        Me.Ctrl_Botonera_Columnas.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.Ctrl_Botonera_Columnas.Name = "Ctrl_Botonera_Columnas"
        Me.Ctrl_Botonera_Columnas.Size = New System.Drawing.Size(23, 22)
        Me.Ctrl_Botonera_Columnas.Text = "ToolStripButton1"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(6, 25)
        '
        'Ctrl_Botonera_Cancelar
        '
        Me.Ctrl_Botonera_Cancelar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Ctrl_Botonera_Cancelar.Enabled = False
        Me.Ctrl_Botonera_Cancelar.Image = CType(resources.GetObject("Ctrl_Botonera_Cancelar.Image"), System.Drawing.Image)
        Me.Ctrl_Botonera_Cancelar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.Ctrl_Botonera_Cancelar.Name = "Ctrl_Botonera_Cancelar"
        Me.Ctrl_Botonera_Cancelar.Size = New System.Drawing.Size(23, 22)
        Me.Ctrl_Botonera_Cancelar.Text = "Cancelar"
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(6, 25)
        '
        'Frm_MantenedorControlAcceso
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(5.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.DimGray
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.ClientSize = New System.Drawing.Size(604, 573)
        Me.Controls.Add(Me.Ctrl_Botonera)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.Btn_Salir)
        Me.Controls.Add(Me.Pnl_UsuariosPerfil)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.ForeColor = System.Drawing.Color.DimGray
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Frm_MantenedorControlAcceso"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Control de Acceso"
        Me.Pnl_UsuariosPerfil.ResumeLayout(False)
        Me.Pnl_UsuariosPerfil.PerformLayout()
        CType(Me.Cmb_Perfiles, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TabModulos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabModulos.ResumeLayout(False)
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.Ctrl_Botonera.ResumeLayout(False)
        Me.Ctrl_Botonera.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Public Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub
    Friend WithEvents Pnl_UsuariosPerfil As System.Windows.Forms.Panel
    Friend WithEvents C1SuperLabel16 As C1.Win.C1SuperTooltip.C1SuperLabel
    Friend WithEvents Btn_Grabar_PerfilesAcciones As C1.Win.C1Input.C1Button
    Friend WithEvents TabModulos As C1.Win.C1Command.C1DockingTab
    Friend WithEvents C1DockingTabPage3 As C1.Win.C1Command.C1DockingTabPage
    Friend WithEvents C1DockingTabPage4 As C1.Win.C1Command.C1DockingTabPage
    Friend WithEvents C1DockingTabPage5 As C1.Win.C1Command.C1DockingTabPage
    Friend WithEvents C1DockingTabPage1 As C1.Win.C1Command.C1DockingTabPage
    Friend WithEvents C1DockingTabPage2 As C1.Win.C1Command.C1DockingTabPage
    Friend WithEvents Btn_Salir As C1.Win.C1Input.C1Button
    Friend WithEvents Cmb_Perfiles As C1.Win.C1List.C1Combo
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents StatusLabel As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents Ctrl_Botonera As System.Windows.Forms.ToolStrip
    Friend WithEvents Ctrl_Botonera_Agregar As System.Windows.Forms.ToolStripButton
    Friend WithEvents Ctrl_Botonera_Buscar As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents Ctrl_Botonera_Guardar As System.Windows.Forms.ToolStripButton
    Friend WithEvents Ctrl_Botonera_Editar As System.Windows.Forms.ToolStripButton
    Friend WithEvents Ctrl_Botonera_Eliminar As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents Ctrl_Botonera_Imprimir As System.Windows.Forms.ToolStripButton
    Friend WithEvents Ctrl_Botonera_Excel As System.Windows.Forms.ToolStripButton
    Friend WithEvents Ctrl_Botonera_Columnas As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents Ctrl_Botonera_Cancelar As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
End Class
