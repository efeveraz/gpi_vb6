﻿'------------------------------------------------------------------------------
' <auto-generated>
'     Este código fue generado por una herramienta.
'     Versión de runtime:4.0.30319.42000
'
'     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
'     se vuelve a generar el código.
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict Off
Option Explicit On

Imports System
Imports System.ComponentModel
Imports System.Diagnostics
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Xml.Serialization

'
'Microsoft.VSDesigner generó automáticamente este código fuente, versión=4.0.30319.42000.
'
Namespace WsBiceCMA_ObtenerSubMargen_Desa
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.4084.0"),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code"),  _
     System.Web.Services.WebServiceBindingAttribute(Name:="ObtenerSubmargenPortBinding", [Namespace]:="http://ws.bice.com/"),  _
     System.Xml.Serialization.XmlIncludeAttribute(GetType(baseEntidad))>  _
    Partial Public Class ObtenerSubmargenService
        Inherits System.Web.Services.Protocols.SoapHttpClientProtocol
        
        Private obtenerSMCMAOperationCompleted As System.Threading.SendOrPostCallback
        
        Private useDefaultCredentialsSetExplicitly As Boolean
        
        '''<remarks/>
        Public Sub New()
            MyBase.New
            Me.Url = Global.ServicioMail.My.MySettings.Default.Gpi_20_WsBiceCMA_ObtenerSubMargen_Desa_ObtenerSubmargenService
            If (Me.IsLocalFileSystemWebService(Me.Url) = true) Then
                Me.UseDefaultCredentials = true
                Me.useDefaultCredentialsSetExplicitly = false
            Else
                Me.useDefaultCredentialsSetExplicitly = true
            End If
        End Sub
        
        Public Shadows Property Url() As String
            Get
                Return MyBase.Url
            End Get
            Set
                If (((Me.IsLocalFileSystemWebService(MyBase.Url) = true)  _
                            AndAlso (Me.useDefaultCredentialsSetExplicitly = false))  _
                            AndAlso (Me.IsLocalFileSystemWebService(value) = false)) Then
                    MyBase.UseDefaultCredentials = false
                End If
                MyBase.Url = value
            End Set
        End Property
        
        Public Shadows Property UseDefaultCredentials() As Boolean
            Get
                Return MyBase.UseDefaultCredentials
            End Get
            Set
                MyBase.UseDefaultCredentials = value
                Me.useDefaultCredentialsSetExplicitly = true
            End Set
        End Property
        
        '''<remarks/>
        Public Event obtenerSMCMACompleted As obtenerSMCMACompletedEventHandler
        
        '''<remarks/>
        <System.Web.Services.Protocols.SoapDocumentMethodAttribute("", RequestNamespace:="http://ws.bice.com/", ResponseNamespace:="http://ws.bice.com/", Use:=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle:=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)>  _
        Public Function obtenerSMCMA(<System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)> ByVal request As requestObtenerSubmargen) As <System.Xml.Serialization.XmlElementAttribute("return", Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)> responseObtenerSubmargen
            Dim results() As Object = Me.Invoke("obtenerSMCMA", New Object() {request})
            Return CType(results(0),responseObtenerSubmargen)
        End Function
        
        '''<remarks/>
        Public Overloads Sub obtenerSMCMAAsync(ByVal request As requestObtenerSubmargen)
            Me.obtenerSMCMAAsync(request, Nothing)
        End Sub
        
        '''<remarks/>
        Public Overloads Sub obtenerSMCMAAsync(ByVal request As requestObtenerSubmargen, ByVal userState As Object)
            If (Me.obtenerSMCMAOperationCompleted Is Nothing) Then
                Me.obtenerSMCMAOperationCompleted = AddressOf Me.OnobtenerSMCMAOperationCompleted
            End If
            Me.InvokeAsync("obtenerSMCMA", New Object() {request}, Me.obtenerSMCMAOperationCompleted, userState)
        End Sub
        
        Private Sub OnobtenerSMCMAOperationCompleted(ByVal arg As Object)
            If (Not (Me.obtenerSMCMACompletedEvent) Is Nothing) Then
                Dim invokeArgs As System.Web.Services.Protocols.InvokeCompletedEventArgs = CType(arg,System.Web.Services.Protocols.InvokeCompletedEventArgs)
                RaiseEvent obtenerSMCMACompleted(Me, New obtenerSMCMACompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState))
            End If
        End Sub
        
        '''<remarks/>
        Public Shadows Sub CancelAsync(ByVal userState As Object)
            MyBase.CancelAsync(userState)
        End Sub
        
        Private Function IsLocalFileSystemWebService(ByVal url As String) As Boolean
            If ((url Is Nothing)  _
                        OrElse (url Is String.Empty)) Then
                Return false
            End If
            Dim wsUri As System.Uri = New System.Uri(url)
            If ((wsUri.Port >= 1024)  _
                        AndAlso (String.Compare(wsUri.Host, "localHost", System.StringComparison.OrdinalIgnoreCase) = 0)) Then
                Return true
            End If
            Return false
        End Function
    End Class
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.4084.0"),  _
     System.SerializableAttribute(),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code"),  _
     System.Xml.Serialization.XmlTypeAttribute([Namespace]:="http://ws.bice.com/")>  _
    Partial Public Class requestObtenerSubmargen
        Inherits baseEntidad
        
        Private numIdenField() As String
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute("numIden", Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true)>  _
        Public Property numIden() As String()
            Get
                Return Me.numIdenField
            End Get
            Set
                Me.numIdenField = value
            End Set
        End Property
    End Class
    
    '''<remarks/>
    <System.Xml.Serialization.XmlIncludeAttribute(GetType(requestObtenerSubmargen)),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.4084.0"),  _
     System.SerializableAttribute(),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code"),  _
     System.Xml.Serialization.XmlTypeAttribute([Namespace]:="http://ws.bice.com/")>  _
    Partial Public Class baseEntidad
    End Class
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.4084.0"),  _
     System.SerializableAttribute(),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code"),  _
     System.Xml.Serialization.XmlTypeAttribute([Namespace]:="http://ws.bice.com/")>  _
    Partial Public Class serviceStatus
        
        Private estadoField As estado
        
        Private estadoFieldSpecified As Boolean
        
        Private mensajeField As String
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property estado() As estado
            Get
                Return Me.estadoField
            End Get
            Set
                Me.estadoField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlIgnoreAttribute()>  _
        Public Property estadoSpecified() As Boolean
            Get
                Return Me.estadoFieldSpecified
            End Get
            Set
                Me.estadoFieldSpecified = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property mensaje() As String
            Get
                Return Me.mensajeField
            End Get
            Set
                Me.mensajeField = value
            End Set
        End Property
    End Class
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.4084.0"),  _
     System.SerializableAttribute(),  _
     System.Xml.Serialization.XmlTypeAttribute([Namespace]:="http://ws.bice.com/")>  _
    Public Enum estado
        
        '''<remarks/>
        SUCCESS
        
        '''<remarks/>
        EMPTY
        
        '''<remarks/>
        [ERROR]
        
        '''<remarks/>
        ERROR_DESCONOCIDO
    End Enum
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.4084.0"),  _
     System.SerializableAttribute(),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code"),  _
     System.Xml.Serialization.XmlTypeAttribute([Namespace]:="http://ws.bice.com/")>  _
    Partial Public Class submargen
        
        Private numMemoField As Integer
        
        Private numMemoFieldSpecified As Boolean
        
        Private numCorrSmargenField As Integer
        
        Private numCorrSmargenFieldSpecified As Boolean
        
        Private codSmargenField As String
        
        Private glsSmargenField As String
        
        Private codSmargenUnicoField As String
        
        Private mtoAprobField As Decimal
        
        Private mtoAprobFieldSpecified As Boolean
        
        Private codMonField As String
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property numMemo() As Integer
            Get
                Return Me.numMemoField
            End Get
            Set
                Me.numMemoField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlIgnoreAttribute()>  _
        Public Property numMemoSpecified() As Boolean
            Get
                Return Me.numMemoFieldSpecified
            End Get
            Set
                Me.numMemoFieldSpecified = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property numCorrSmargen() As Integer
            Get
                Return Me.numCorrSmargenField
            End Get
            Set
                Me.numCorrSmargenField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlIgnoreAttribute()>  _
        Public Property numCorrSmargenSpecified() As Boolean
            Get
                Return Me.numCorrSmargenFieldSpecified
            End Get
            Set
                Me.numCorrSmargenFieldSpecified = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property codSmargen() As String
            Get
                Return Me.codSmargenField
            End Get
            Set
                Me.codSmargenField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property glsSmargen() As String
            Get
                Return Me.glsSmargenField
            End Get
            Set
                Me.glsSmargenField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property codSmargenUnico() As String
            Get
                Return Me.codSmargenUnicoField
            End Get
            Set
                Me.codSmargenUnicoField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property mtoAprob() As Decimal
            Get
                Return Me.mtoAprobField
            End Get
            Set
                Me.mtoAprobField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlIgnoreAttribute()>  _
        Public Property mtoAprobSpecified() As Boolean
            Get
                Return Me.mtoAprobFieldSpecified
            End Get
            Set
                Me.mtoAprobFieldSpecified = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property codMon() As String
            Get
                Return Me.codMonField
            End Get
            Set
                Me.codMonField = value
            End Set
        End Property
    End Class
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.4084.0"),  _
     System.SerializableAttribute(),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code"),  _
     System.Xml.Serialization.XmlTypeAttribute([Namespace]:="http://ws.bice.com/")>  _
    Partial Public Class cliente
        
        Private numIdenField As String
        
        Private numMemoField As Integer
        
        Private numMemoFieldSpecified As Boolean
        
        Private cantidadSubmargenesField As Integer
        
        Private fecVencimientoField As String
        
        Private submargenField() As submargen
        
        Private resultadoField As estado
        
        Private resultadoFieldSpecified As Boolean
        
        Private mensajeField As String
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property numIden() As String
            Get
                Return Me.numIdenField
            End Get
            Set
                Me.numIdenField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property numMemo() As Integer
            Get
                Return Me.numMemoField
            End Get
            Set
                Me.numMemoField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlIgnoreAttribute()>  _
        Public Property numMemoSpecified() As Boolean
            Get
                Return Me.numMemoFieldSpecified
            End Get
            Set
                Me.numMemoFieldSpecified = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property cantidadSubmargenes() As Integer
            Get
                Return Me.cantidadSubmargenesField
            End Get
            Set
                Me.cantidadSubmargenesField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property fecVencimiento() As String
            Get
                Return Me.fecVencimientoField
            End Get
            Set
                Me.fecVencimientoField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute("submargen", Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true)>  _
        Public Property submargen() As submargen()
            Get
                Return Me.submargenField
            End Get
            Set
                Me.submargenField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property resultado() As estado
            Get
                Return Me.resultadoField
            End Get
            Set
                Me.resultadoField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlIgnoreAttribute()>  _
        Public Property resultadoSpecified() As Boolean
            Get
                Return Me.resultadoFieldSpecified
            End Get
            Set
                Me.resultadoFieldSpecified = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property mensaje() As String
            Get
                Return Me.mensajeField
            End Get
            Set
                Me.mensajeField = value
            End Set
        End Property
    End Class
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.4084.0"),  _
     System.SerializableAttribute(),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code"),  _
     System.Xml.Serialization.XmlTypeAttribute([Namespace]:="http://ws.bice.com/")>  _
    Partial Public Class responseObtenerSubmargen
        
        Private clienteField() As cliente
        
        Private statusField As serviceStatus
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute("cliente", Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true)>  _
        Public Property cliente() As cliente()
            Get
                Return Me.clienteField
            End Get
            Set
                Me.clienteField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property status() As serviceStatus
            Get
                Return Me.statusField
            End Get
            Set
                Me.statusField = value
            End Set
        End Property
    End Class
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.4084.0")>  _
    Public Delegate Sub obtenerSMCMACompletedEventHandler(ByVal sender As Object, ByVal e As obtenerSMCMACompletedEventArgs)
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.4084.0"),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code")>  _
    Partial Public Class obtenerSMCMACompletedEventArgs
        Inherits System.ComponentModel.AsyncCompletedEventArgs
        
        Private results() As Object
        
        Friend Sub New(ByVal results() As Object, ByVal exception As System.Exception, ByVal cancelled As Boolean, ByVal userState As Object)
            MyBase.New(exception, cancelled, userState)
            Me.results = results
        End Sub
        
        '''<remarks/>
        Public ReadOnly Property Result() As responseObtenerSubmargen
            Get
                Me.RaiseExceptionIfNecessary
                Return CType(Me.results(0),responseObtenerSubmargen)
            End Get
        End Property
    End Class
End Namespace
