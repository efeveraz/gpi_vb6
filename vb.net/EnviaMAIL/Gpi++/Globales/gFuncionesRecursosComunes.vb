Imports System.IO
Imports C1.Win.C1List
Imports Microsoft.Office.Interop
Imports System.ComponentModel
Imports System.Threading
Imports System.Net.Mail
Imports System.Net
Imports System.Net.Mime

Public Module gFuncionesRecursosComunes

    Dim lobjConcepto As ClsBloqueo = New ClsBloqueo

#Region " Funciones Victor Caro "

    '...Declaraci�n de la API
    Private Declare Auto Function SetProcessWorkingSetSize Lib "kernel32.dll" (ByVal procHandle As IntPtr, ByVal min As Int32, ByVal max As Int32) As Boolean
    '..Funci�n de liberaci�n de memoria
    Public Sub gFunLiberaMemoria()
        Try
            If Environment.OSVersion.Platform = PlatformID.Win32NT Then
                SetProcessWorkingSetSize(System.Diagnostics.Process.GetCurrentProcess().Handle, -1, -1)
            End If
        Catch ex As Exception
            '...Control de errores
        End Try
    End Sub

    Public Function gFunDiadelaSemana(ByVal txtFecha As String) As String
        Try
            Dim strFecha As Date = CDate(txtFecha.Trim)
            gFunDiadelaSemana = strFecha.ToString("dddd", New Globalization.CultureInfo("es-ES"))
        Catch ex As Exception
            gFunDiadelaSemana = txtFecha
        End Try
    End Function

    Public Function gFunExisteAmbienteConexion() As String
        gFunExisteAmbienteConexion = "OK"
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Try
            Connect.Abrir()
        Catch ex As Exception
            gFunExisteAmbienteConexion = vbCrLf & "__ NO SE PUDO ESTABLECER CONEXI�N CON LA BASE DE DATOS __" & vbCrLf & vbCrLf
            gFunExisteAmbienteConexion = gFunExisteAmbienteConexion & "AMBIENTE: [ " & gtxtAmbienteSistema & " ]"
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function gFunControlTienePermiso(ByVal strNombreControl As String,
                                            ByVal ArregloDePermisos(,) As String) As Boolean
        Dim lblnTienePermiso As Boolean = False
        ' --+ validacion valida que traiga al menos un permiso
        If ArregloDePermisos.Length <= 1 Then
            gFunControlTienePermiso = lblnTienePermiso
            Exit Function
        End If

        For lintFor As Integer = 0 To ArregloDePermisos.GetLength(0) - 1
            If strNombreControl.ToUpper.Trim = ArregloDePermisos(lintFor, 1).ToUpper.Trim Then
                lblnTienePermiso = True
                Exit For
            End If
        Next
        gFunControlTienePermiso = lblnTienePermiso
    End Function

    Public Function gFunFechaFormatoConfiguracionRegional(ByVal str_Fecha_DD_MM_AAAA As String) As String
        Try
            '  "26-06-2008"
            '   0123456789
            Dim MiFecha As String = str_Fecha_DD_MM_AAAA.Trim
            If MiFecha.Trim <> "" Then
                Dim FormatoFechaActual = System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern
                FormatoFechaActual = FormatoFechaActual.ToString.ToUpper.Replace("Y", "A")
                FormatoFechaActual = FormatoFechaActual.ToString.Replace(".", "/")
                FormatoFechaActual = FormatoFechaActual.ToString.Replace("-", "/")
                FormatoFechaActual = FormatoFechaActual.ToString.ToUpper
                Select Case FormatoFechaActual
                    Case "DD/MM/AAAA"
                        MiFecha = MiFecha.Trim
                    Case "AAAA/MM/DD"
                        MiFecha = MiFecha.Substring(6, 4) & "/" & MiFecha.Substring(3, 2) & "/" & MiFecha.Substring(0, 2)
                    Case "DD/MM/AA"
                        MiFecha = MiFecha.Substring(0, 2) & "/" & MiFecha.Substring(3, 2) & "/" & MiFecha.Substring(8, 2)
                    Case "AA/MM/DD"
                        MiFecha = MiFecha.Substring(8, 2) & "/" & MiFecha.Substring(3, 2) & "/" & MiFecha.Substring(0, 2)
                    Case "M/D/AAAA"
                        MiFecha = MiFecha.Substring(3, 2).PadRight(2, "0") & "/" & MiFecha.Substring(0, 2).PadRight(2, "0") & "/" & MiFecha.Substring(6, 4)
                    Case "AAAA/D/M"
                        MiFecha = MiFecha.Substring(6, 4) & "/" & MiFecha.Substring(0, 2).PadRight(2, "0") & "/" & MiFecha.Substring(3, 2)
                    Case "AAAA/M/D"
                        MiFecha = MiFecha.Substring(6, 4) & "/" & MiFecha.Substring(3, 2).PadRight(2, "0") & "/" & MiFecha.Substring(0, 2)
                    Case "D/M/AAAA"
                        MiFecha = MiFecha.Substring(0, 2).PadRight(2, "0") & "/" & MiFecha.Substring(3, 2).PadRight(2, "0") & "/" & MiFecha.Substring(6, 4)
                    Case "D/M/AA"
                        MiFecha = MiFecha.Substring(0, 2).PadRight(2, "0") & "/" & MiFecha.Substring(3, 2).PadRight(2, "0") & "/" & MiFecha.Substring(6, 4)
                    Case "M/D/AA"
                        MiFecha = MiFecha.Substring(3, 2).PadRight(2, "0") & "/" & MiFecha.Substring(0, 2).PadRight(2, "0") & "/" & MiFecha.Substring(8, 2)
                    Case "AA/D/M"
                        MiFecha = MiFecha.Substring(8, 2) & "/" & MiFecha.Substring(0, 2).PadRight(2, "0") & "/" & MiFecha.Substring(3, 2).PadRight(2, "0")
                    Case Else
                        MiFecha = MiFecha
                End Select
            End If
            gFunFechaFormatoConfiguracionRegional = MiFecha.Trim
        Catch ex As Exception
            gFunFechaFormatoConfiguracionRegional = ""
        End Try
    End Function

    Public Sub gSubGuardaAgrupacionGrilla(ByVal MyGrilla As C1.Win.C1TrueDBGrid.C1TrueDBGrid)
        '...Salva las agrupaciones de una grilla
        Try
            If MyGrilla.RowCount > 0 Then
                Dim int_Index As Integer
                Dim arr_GroupBy(MyGrilla.GroupedColumns.Count - 1) As String

                For int_Index = 0 To MyGrilla.GroupedColumns.Count - 1
                    arr_GroupBy(int_Index) = MyGrilla.GroupedColumns.Item(int_Index).DataField.ToString() & "|"

                    If MyGrilla.GroupedColumns.Item(int_Index).SortDirection.ToString.Trim() = "Ascending" Then
                        arr_GroupBy(int_Index) = arr_GroupBy(int_Index) & "1"
                    Else
                        arr_GroupBy(int_Index) = arr_GroupBy(int_Index) & "2"
                    End If
                Next
                garrColumnaAgrupacion = arr_GroupBy
            End If
        Catch ex As Exception
            MsgBox("Error al Salvar Agrupaci�n.", vbCritical, My.Application.Info.Title & " : GSub_GuardaAgrupacionGrilla")
        End Try
    End Sub

    Public Sub gSubRecuperaAgrupacionGrilla(ByRef MyGrilla As C1.Win.C1TrueDBGrid.C1TrueDBGrid, ByVal MyKeyGrilla As String)
        '...Recupera las agrupaciones de una grilla
        Try
            Dim int_Index As Integer
            For int_Index = 0 To garrColumnaAgrupacion.Length - 1
                Dim ColOrder() = garrColumnaAgrupacion(int_Index).Split("|")
                MyGrilla.GroupedColumns.Add(MyGrilla.Columns(ColOrder(0).ToString.Trim))
                MyGrilla.GroupedColumns.Item(ColOrder(0).ToString.Trim).SortDirection = CInt(ColOrder(1))
            Next
            '...Genera otra agrupacion y la elimina ( para que la grilla agrupe en forma correcta )
            MyGrilla.GroupedColumns.Add(MyGrilla.Columns(MyKeyGrilla))
            MyGrilla.GroupedColumns.Item(MyKeyGrilla).SortDirection = C1.Win.C1TrueDBGrid.SortDirEnum.None
            MyGrilla.GroupedColumns.RemoveAt(MyGrilla.GroupedColumns.Count - 1)
        Catch ex As Exception
            Exit Sub
        End Try
    End Sub

    Public Sub gSubGuardaFiltrosGrilla(ByVal MyGrilla As C1.Win.C1TrueDBGrid.C1TrueDBGrid, ByRef G_arr_ColFiltros() As String)
        Try
            Dim int_Index As Integer
            Dim arr_Filtros(MyGrilla.Columns.Count - 1) As String
            For int_Index = 0 To MyGrilla.Columns.Count - 1
                arr_Filtros(int_Index) = MyGrilla.Columns.Item(int_Index).FilterText.Trim
            Next
            G_arr_ColFiltros = arr_Filtros
        Catch ex As Exception
            MsgBox("Error al guardar filtros de la lista.", vbCritical, My.Application.Info.Title & " : gSubGuardaFiltrosGrilla")
        End Try
    End Sub

    Public Sub gSubRecuperaFiltrosGrilla(ByRef MyGrilla As C1.Win.C1TrueDBGrid.C1TrueDBGrid, ByVal G_arr_ColFiltros() As String)
        Try
            Dim int_Index As Integer
            For int_Index = 0 To G_arr_ColFiltros.Length - 1
                If G_arr_ColFiltros(int_Index).Trim <> "" Then
                    MyGrilla.Columns.Item(int_Index).FilterText = G_arr_ColFiltros(int_Index).Trim
                End If
            Next
        Catch ex As Exception
            Exit Sub
        End Try
    End Sub

    Public Function gFunValidaCaracterAlfaNumerico(
                    ByVal KeyChar As Char,
                    Optional ByVal blnSoloMayusculas As Boolean = True,
                    Optional ByVal Beper As Boolean = True) As Char
        '...Valida en keypress que solo permita los caracteres en los string "strMayuscula" y "strMinuscula"
        Try
            Dim strMayuscula As String
            Dim strMinuscula As String
            Dim strCompara As String
            strMayuscula = " ABCDEFGHIJKLMN�OPQRSTUVWXYZ_-+*=/1234567890,.:@#$()?�"
            strMinuscula = " abcdefghijklmn�opqrstuvwxyz_-+*=/1234567890,.:@#$()?�"
            If blnSoloMayusculas Then
                gFunValidaCaracterAlfaNumerico = CStr(KeyChar).ToUpper : strCompara = strMayuscula
            Else
                gFunValidaCaracterAlfaNumerico = KeyChar : strCompara = strMayuscula & strMinuscula
            End If
            If Asc(KeyChar) <> Asc(vbBack) Then
                If (InStr(strCompara, gFunValidaCaracterAlfaNumerico)) <= 0 Then
                    gFunValidaCaracterAlfaNumerico = ""
                    If (Beper) Then Beep()
                End If
            End If
        Catch ex As Exception
            MsgBox("Error de validaci�n Alfanum�rico.", vbCritical, My.Application.Info.Title & " : gFunValidaCaracterAlfaNumerico")
        End Try
    End Function

    Public Function gFunValidaValorControl(
                    ByVal strMemsajeError As String,
                    ByVal objetoControl As Object,
                    ByRef StrMensajeResumen As String,
                    ByRef SeteaFoco As Boolean,
                    Optional ByVal QueTengaValor As Boolean = True,
                    Optional ByRef DockingTabControl As C1.Win.C1Command.C1DockingTab = Nothing,
                    Optional ByVal DockingTabSelectedIndex As Integer = -1,
                    Optional ByVal MuestraError As Boolean = True) As Boolean
        '...Valida que un control Tenga o NO Tenga un valor, si hay error posiciona el "Focus" en el control.
        '   Si el control est� en un control "DockingTab", cambia el SelectedIndex del tabulador, pasado
        '   a trav�s del par�metro "DockingTabSelectedIndex"
        Try
            gFunValidaValorControl = False
            '...TextBox, C1.Win.C1Input.C1TextBox y C1.Win.C1Input.C1DateEdit
            If (TypeOf objetoControl Is TextBox) Or
               (TypeOf objetoControl Is C1.Win.C1Input.C1TextBox) Or
               (TypeOf objetoControl Is C1.Win.C1Input.C1DateEdit) Then
                If (objetoControl.Text.Trim = "" And QueTengaValor) Or
                   (objetoControl.text.ToString.Replace("0", "").Trim = objetoControl.nulltext.ToString.Replace("0", "").Trim And QueTengaValor And objetoControl.numericinput) Or
                   (objetoControl.value.ToString.Replace("0", "").Trim = objetoControl.nulltext.ToString.Replace("0", "").Trim And QueTengaValor And objetoControl.numericinput) Or
                   (objetoControl.Text.Trim <> "" And Not QueTengaValor) Then
                    If MuestraError Then
                        MsgBox(strMemsajeError, vbCritical, My.Application.Info.Title)
                    Else
                        StrMensajeResumen = StrMensajeResumen & strMemsajeError & vbCr
                    End If
                    If SeteaFoco Then
                        If (Not DockingTabSelectedIndex = -1) Then DockingTabControl.SelectedIndex = DockingTabSelectedIndex
                        objetoControl.Focus()
                        SeteaFoco = False
                    End If
                    Exit Function
                End If
            Else
                '...C1.Win.C1List.C1Combo
                If (TypeOf objetoControl Is C1.Win.C1List.C1Combo) Then
                    If (objetoControl.Text.Trim = "" And QueTengaValor) Or
                       (objetoControl.Text.Trim <> "" And Not QueTengaValor) Then
                        If MuestraError Then
                            MsgBox(strMemsajeError, vbCritical, My.Application.Info.Title)
                        Else
                            StrMensajeResumen = StrMensajeResumen & strMemsajeError & vbCr
                        End If
                        If SeteaFoco Then
                            If (Not DockingTabSelectedIndex = -1) Then DockingTabControl.SelectedIndex = DockingTabSelectedIndex
                            objetoControl.Focus()
                            SeteaFoco = False
                        End If
                        Exit Function
                    End If
                End If
            End If
            gFunValidaValorControl = True
        Catch ex As Exception
            MsgBox("Error de validaci�n del Control:" & vbCr & objetoControl.name, vbCritical, My.Application.Info.Title & " : GFun_ValidaValorControl")
            gFunValidaValorControl = False
        End Try
    End Function

    Public Sub gSubHabilitaControl(
                    ByRef objetoControl As Object,
                    ByVal blnReadOnly As Boolean,
                    ByVal LColorHabilitado As Color,
                    ByVal LColorDeshabilitado As Color,
                    Optional ByVal blnSetColorControl As Boolean = True)
        Try
            If (TypeOf objetoControl Is Label) Then Exit Sub
            '...Color
            If (blnReadOnly) And (blnSetColorControl) Then
                If (TypeOf objetoControl Is C1.Win.C1List.C1Combo) Then
                    objetoControl.EditorBackColor = gColorBackGroundDesabled
                Else
                    If (TypeOf objetoControl Is System.Windows.Forms.RadioButton) Or
                       (TypeOf objetoControl Is System.Windows.Forms.CheckBox) Then
                        objetoControl.BackColor = Color.Transparent
                    Else
                        If Not (TypeOf objetoControl Is System.Windows.Forms.Panel) Then
                            objetoControl.BackColor = LColorDeshabilitado
                        End If
                    End If
                End If
            Else
                If (TypeOf objetoControl Is C1.Win.C1List.C1Combo) Then
                    objetoControl.EditorBackColor = gColorBackGroundEnabled
                Else
                    If (TypeOf objetoControl Is System.Windows.Forms.RadioButton) Or
                       (TypeOf objetoControl Is System.Windows.Forms.CheckBox) Then
                        objetoControl.BackColor = Color.Transparent
                    Else
                        If Not (TypeOf objetoControl Is System.Windows.Forms.Panel) Then
                            objetoControl.BackColor = LColorHabilitado
                        End If
                    End If
                End If
            End If
            '...Enable / TabStop
            If (TypeOf objetoControl Is C1.Win.C1Input.C1TextBox) Or
               (TypeOf objetoControl Is C1.Win.C1Input.C1DateEdit) Or
               (TypeOf objetoControl Is C1.Win.C1List.C1Combo) Then
                objetoControl.ReadOnly = blnReadOnly
                objetoControl.TabStop = (Not blnReadOnly)
            Else
                If (TypeOf objetoControl Is System.Windows.Forms.RadioButton) Or
                   (TypeOf objetoControl Is System.Windows.Forms.CheckBox) Or
                   (TypeOf objetoControl Is System.Windows.Forms.Button) Or
                   (TypeOf objetoControl Is System.Windows.Forms.Panel) Then
                    objetoControl.Enabled = (Not blnReadOnly)
                    objetoControl.TabStop = (Not blnReadOnly)
                Else
                    objetoControl.Locked = blnReadOnly
                    objetoControl.TabStop = (Not blnReadOnly)
                End If
            End If
        Catch ex As Exception
            MsgBox("Error al Habilitar Control :" & vbCr & objetoControl.name, vbCritical, My.Application.Info.Title & " : gSubHabilitaControl")
        End Try
    End Sub

    Public Function gFunSeteaItemComboBox(
                    ByVal ComboDondeBuscar As C1.Win.C1List.C1Combo,
                    ByVal QueBusca As String,
                    Optional ByVal NumeroColumnaDondeBusca As Integer = 1) As Integer

        Dim intIndex As Integer
        gFunSeteaItemComboBox = -1
        Try
            For intIndex = 0 To ComboDondeBuscar.ListCount - 1
                If ComboDondeBuscar.Columns(NumeroColumnaDondeBusca).CellValue(intIndex).ToString.Trim.ToUpper = QueBusca.ToString.Trim.ToUpper Then
                    gFunSeteaItemComboBox = intIndex
                    Exit Function
                End If
            Next
        Catch ex As Exception
            MsgBox("Error al setear indice Combo Box :" & vbCr & ComboDondeBuscar.Name, vbCritical, My.Application.Info.Title & " : gFunSeteaItemComboBox")
        End Try
    End Function

    Public Function gFunBloqueaRegistros(ByVal LTxt_Operacion As String, ByVal LTxt_Negocio As String, ByVal LTxt_Concepto As String,
                                          ByVal LTxt_Clave1 As String, ByVal LTxt_Clave2 As String, ByVal LTxt_Clave3 As String,
                                          ByVal LTxt_Clave4 As String, ByVal LTxt_Clave5 As String, ByVal LTxt_Clave6 As String,
                                          ByRef PUsuario As String, ByRef PCod_Usuario As String) As String

        Dim ClsGeneral As ClsGeneral = New ClsGeneral

        Dim EstadoRegistro As String = ""
        'EstadoRegistro = ClsGeneral.BloqueaRegistros(LTxt_Operacion, LTxt_Negocio, LTxt_Concepto, _
        '                                                LTxt_Clave1, LTxt_Clave2, LTxt_Clave3, _
        '                                                LTxt_Clave4, LTxt_Clave5, LTxt_Clave6, _
        '                                                "", "", _
        '                                                PUsuario, PCod_Usuario)
        'Date.Today.ToShortDateString, Date.Today.ToShortTimeString, _
        Return EstadoRegistro
    End Function

    Public Function gFunFiltroValido(ByRef FilterText As String) As Boolean
        If IsNumeric(FilterText.Trim) Then
            FilterText = " = " + FilterText
            Return True
        Else
            If FilterText.Length = 1 Then
                If FilterText.Trim.Substring(0, 1) = ">" Or
                   FilterText.Trim.Substring(0, 1) = "<" Or
                   FilterText.Trim.Substring(0, 1) = "=" Or
                   FilterText.Trim.Substring(0, 1) = "-" Then
                    FilterText = "*"
                    Return True
                Else
                    If IsNumeric(FilterText.Trim.Substring(0)) Then
                        FilterText = FilterText
                        Return True
                    ElseIf FilterText.Trim.Substring(0) = "" Then
                        FilterText = "*"
                        Return True
                    Else
                        Return False
                    End If
                End If
            End If
            If FilterText.Length = 2 Then
                If FilterText.Substring(0, 2) = "<>" Or
                   FilterText.Substring(0, 2) = "<=" Or
                   FilterText.Substring(0, 2) = ">=" Or
                   FilterText.Substring(0, 2) = "=<" Or
                   FilterText.Substring(0, 2) = "=>" Or
                   FilterText.Substring(0, 2) = ">-" Or
                   FilterText.Substring(0, 2) = "<-" Or
                   FilterText.Substring(0, 2) = "=-" Then

                    FilterText = "*"
                    Return True
                Else
                    If IsNumeric(FilterText.Trim.Substring(1)) Then
                        FilterText = FilterText
                        Return True
                    ElseIf FilterText.Trim.Substring(1) = "" Then
                        FilterText = "*"
                        Return True
                    Else
                        Return False
                    End If
                End If
            End If
            If FilterText.Length > 2 Then
                If FilterText.Substring(0, 3) = "<>-" Or
                   FilterText.Substring(0, 3) = "<=-" Or
                   FilterText.Substring(0, 3) = ">=-" Or
                   FilterText.Substring(0, 3) = "=<-" Or
                   FilterText.Substring(0, 3) = "=>-" Then
                    FilterText = "*"
                    Return True
                Else
                    If IsNumeric(FilterText.Trim.Substring(2)) Then
                        FilterText = FilterText
                        Return True
                    ElseIf FilterText.Trim.Substring(2) = "" Then
                        FilterText = "*"
                        Return True
                    Else
                        Return False
                    End If
                End If
            End If
        End If
    End Function

    Public Sub gSubGuardarOrdenGrilla(ByRef Orden As SortOrder, ByRef Columna As Integer, ByVal Grilla As C1.Win.C1TrueDBGrid.C1TrueDBGrid)
        Try
            Dim Col As C1.Win.C1TrueDBGrid.C1DataColumn
            Dim Index As Integer = 0

            If Grilla.RowCount > 0 Then
                For Index = 0 To Grilla.Columns.Count - 1
                    Col = Grilla.Columns(Index)
                    If Col.SortDirection <> SortOrder.None Then
                        Columna = Index
                        Orden = Col.SortDirection
                        Exit For
                    End If
                Next
            End If
        Catch ex As Exception
            Exit Sub
        End Try
    End Sub

    Public Sub gSubRecuperarOrdenGrilla(ByVal Orden As SortOrder, ByVal Columna As Integer, ByVal Grilla As C1.Win.C1TrueDBGrid.C1TrueDBGrid, ByRef DatasetGrilla As DataSet)
        Try
            gSubRecuperarOrdenGrilla(Orden, Columna, Grilla, DatasetGrilla.Tables(0))
        Catch ex As Exception
            Exit Sub
        End Try
    End Sub

    Public Sub gSubRecuperarOrdenGrilla(ByVal Orden As SortOrder, ByVal Columna As Integer, ByVal Grilla As C1.Win.C1TrueDBGrid.C1TrueDBGrid, ByRef DataTableGrilla As DataTable)
        Try
            Dim sortCondition As String = Grilla.Splits(0).DisplayColumns(Columna).DataColumn.DataField + " "
            sortCondition += IIf(Orden = SortOrder.Descending, "DESC", "")

            DataTableGrilla.DefaultView.Sort = sortCondition
            Grilla.Columns(Columna).SortDirection = Orden
        Catch ex As Exception
            Exit Sub
        End Try
    End Sub

    Public Function gFun_Busca_RelacionContraparte_GPI(
                    ByVal strEntidadGPI As String,
                    ByVal strEntidadContraparte As String,
                    ByVal strCodigoContrapate As String) As String
        Try
            Dim objRelacionContraparte As ClsRelacionContraparteGPI = New ClsRelacionContraparteGPI

            Return objRelacionContraparte.Entidad_Relacion_Contraparte_GPI_Buscar(strEntidadGPI, strEntidadContraparte, strCodigoContrapate)
        Catch ex As Exception
            Return ""
        End Try
    End Function

#End Region '....

#Region " Funciones Hans Muller "

    Public Sub gSub_BuscarContraparte(ByVal lngIdContraparte As Long,
                                      ByVal strIngresaOp As String,
                                      ByVal strClaseInstrumento As String,
                                      ByVal strSubClaseInstrumento As String,
                                      ByVal strColumnas As String,
                                      ByRef strResultado As String,
                                      ByRef DS_Contraparte As DataSet,
                                      Optional ByVal strEstado As String = "VIG",
                                      Optional ByVal strDscTipoContraparte As String = "")
        Dim objContrapartes As ClsContraParte = New ClsContraParte
        DS_Contraparte = objContrapartes.ContraParte_Ver(lngIdContraparte, strIngresaOp, strClaseInstrumento, strSubClaseInstrumento, strColumnas, strEstado, strResultado, strDscTipoContraparte)
    End Sub

#End Region '....

#Region " Funciones Alexis Gonzalez "

    Public Function GfunValidar_Email(ByVal Email As String, ByRef MensajeError As String) As Boolean
        'Dim MensajeError As String
        Dim strTmp As String
        Dim n As Long
        Dim sEXT As String
        GfunValidar_Email = True
        sEXT = Email
        Do While InStr(1, sEXT, ".") <> 0
            sEXT = Microsoft.VisualBasic.Right(sEXT, Len(sEXT) - InStr(1, sEXT, "."))
        Loop
        If Email = "" Then
            GfunValidar_Email = False
            MensajeError = MensajeError & "No se indic� ninguna direcci�n de " & "mail para verificar!" & vbNewLine
        ElseIf InStr(1, Email, "@") = 0 Then
            GfunValidar_Email = False
            MensajeError = MensajeError & "La direcci�n de email debe contener el signo @" & vbNewLine
        ElseIf InStr(1, Email, "@") = 1 Then
            GfunValidar_Email = False
            MensajeError = MensajeError & "El @ No puede estar al principio" & vbNewLine
        ElseIf InStr(1, Email, "@") = Len(Email) Then
            GfunValidar_Email = False
            MensajeError = MensajeError & "El @ no puede estar al final de la direcci�n" & vbNewLine
        ElseIf EXTisOK(sEXT) = False Or sEXT = "" Then
            GfunValidar_Email = False
            MensajeError = MensajeError & "LA direcci�n no tiene un dominio v�lido, "
            MensajeError = MensajeError & "por ejemplo : "
            MensajeError = MensajeError & ".com, .net, .gov, .org, .edu, .biz, .tv etc.. " & vbNewLine
        ElseIf Len(Email) < 6 Then
            GfunValidar_Email = False
            MensajeError = MensajeError & "La direcci�n no puede ser menor a 6 caracteres." & vbNewLine
        End If
        strTmp = Email
        Do While InStr(1, strTmp, "@") <> 0
            n = 1
            strTmp = Microsoft.VisualBasic.Right(strTmp, Len(strTmp) - InStr(1, strTmp, "@"))
        Loop
        If n > 1 Then
            GfunValidar_Email = False
            MensajeError = MensajeError & "Solo puede haber un @ en la direcci�n de email" & vbNewLine
        End If
        Dim pos As Integer
        pos = InStr(1, Email, "@")
        If Mid(Email, pos + 1, 1) = "." Then
            GfunValidar_Email = False
            MensajeError = MensajeError & "El punto no puede estar seguido del @" & vbNewLine
        End If
        'If MensajeError <> "" Then
        'MsgBox(MensajeError, vbCritical)
        'End If
    End Function

    Public Function EXTisOK(ByVal sEXT As String) As Boolean
        Dim EXT As String = "", X As Long = 0
        EXTisOK = False
        If Microsoft.VisualBasic.Left(sEXT, 1) <> "." Then sEXT = "." & sEXT
        sEXT = UCase(sEXT) 'just to avoid errors
        EXT = EXT & ".COM.EDU.GOV.NET.BIZ.ORG.TV"
        EXT = EXT & ".AF.AL.DZ.As.AD.AO.AI.AQ.AG.AP.AR.AM.AW.AU.AT.AZ.BS.BH.BD.BB.BY"
        EXT = EXT & ".BE.BZ.BJ.BM.BT.BO.BA.BW.BV.BR.IO.BN.BG.BF.MM.BI.KH.CM.CA.CV.KY"
        EXT = EXT & ".CF.TD.CL.CN.CX.CC.CO.KM.CG.CD.CK.CR.CI.HR.CU.CY.CZ.DK.DJ.DM.DO"
        EXT = EXT & ".TP.EC.EG.SV.GQ.ER.EE.ET.FK.FO.FJ.FI.CS.SU.FR.FX.GF.PF.TF.GA.GM.GE.DE"
        EXT = EXT & ".GH.GI.GB.GR.GL.GD.GP.GU.GT.GN.GW.GY.HT.HM.HN.HK.HU.IS.IN.ID.IR.IQ"
        EXT = EXT & ".IE.IL.IT.JM.JP.JO.KZ.KE.KI.KW.KG.LA.LV.LB.LS.LR.LY.LI.LT.LU.MO.MK.MG"
        EXT = EXT & ".MW.MY.MV.ML.MT.MH.MQ.MR.MU.YT.MX.FM.MD.MC.MN.MS.MA.MZ.NA"
        EXT = EXT & ".NR.NP.NL.AN.NT.NC.NZ.NI.NE.NG.NU.NF.KP.MP.NO.OM.PK.PW.PA.PG.PY"
        EXT = EXT & ".PE.PH.PN.PL.PT.PR.QA.RE.RO.RU.RW.GS.SH.KN.LC.PM.ST.VC.SM.SA.SN.SC"
        EXT = EXT & ".SL.SG.SK.SI.SB.SO.ZA.KR.ES.LK.SD.SR.SJ.SZ.SE.CH.SY.TJ.TW.TZ.TH.TG.TK"
        EXT = EXT & ".TO.TT.TN.TR.TM.TC.TV.UG.UA.AE.UK.US.UY.UM.UZ.VU.VA.VE.VN.VG.VI"
        EXT = EXT & ".WF.WS.EH.YE.YU.ZR.ZM.ZW"
        EXT = UCase(EXT) 'just to avoid errors
        If InStr(1, EXT, sEXT, 0) <> 0 Then
            EXTisOK = True
        End If
    End Function

    Public Function EntrgaRutDigito(ByVal Rut As Long) As String
        Dim Digito As Integer
        Dim Contador As Integer
        Dim Multiplo As Integer
        Dim Acumulador As Integer

        Contador = 2
        Acumulador = 0
        While Rut <> 0
            Multiplo = (Rut Mod 10) * Contador
            Acumulador = Acumulador + Multiplo
            Rut = Rut \ 10
            Contador = Contador + 1
            If Contador = 8 Then
                Contador = 2
            End If
        End While
        Digito = 11 - (Acumulador Mod 11)
        EntrgaRutDigito = CStr(Digito)
        If Digito = 10 Then EntrgaRutDigito = "K"
        If Digito = 11 Then EntrgaRutDigito = "0"
    End Function

    Public Function ValidaRut(ByVal Rut As String, ByRef StrMensajeResumen As String, ByVal strMemsajeError As String) As Boolean
        Dim i As Integer, L As Integer, Valor As Integer, Suma As Integer, Resultado As Integer
        Dim Resto As String

        ValidaRut = False

        If Not Trim(Rut) = "" Then
            If InStr(Rut, "-") > 0 Then
                Rut = Mid(Rut, 1, InStr(Rut, "-") - 1) & Mid(Rut, InStr(Rut, "-") + 1, 1)
            End If

            If Not IsNumeric(Mid(Rut, Len(Rut), 1)) Then
                Rut = Mid(Rut, 1, Len(Rut) - 1) & UCase(Mid(Rut, Len(Rut), 1))
            End If

            Valor = 2
            L = Len(Rut) - 1
            For i = L To 1 Step -1
                Suma = Suma + Val(Mid(Rut, i, 1)) * Valor
                Valor = Valor + 1
                If Valor > 7 Then
                    Valor = 2
                End If
            Next i

            Resultado = Suma Mod 11
            Resto = 11 - Resultado
            If Resto = 11 Then
                Resto = "0"
            End If
            If Resto = 10 Then
                Resto = "K"
            End If

            If Not UCase(Resto) = UCase(Mid(Rut, Len(Rut), 1)) Then
                StrMensajeResumen = StrMensajeResumen & strMemsajeError & vbCr
                ValidaRut = False
            Else
                ValidaRut = True
            End If
        Else
            ValidaRut = True
        End If
    End Function

    Public Sub gSub_BuscarMonedas(ByVal strCodigo As String,
                                  ByVal strColumnas As String,
                                  ByRef strRetorno As String,
                                  ByRef DS_Moneda As DataSet,
                                  Optional ByVal strEstado As String = "TOD",
                                  Optional ByVal strEsMonedaPago As String = "",
                                  Optional ByVal strEsDual As String = "D",
                                  Optional ByVal strMonedaIndice As String = "M",
                                  Optional ByVal strEsMonedaPagoConUf As String = "NO")
        Dim objMoneda As ClsMoneda = New ClsMoneda
        DS_Moneda = objMoneda.Moneda_Ver(strCodigo:=strCodigo,
                                         strColumnas:=strColumnas,
                                         strEstado:=strEstado,
                                         strRetorno:=strRetorno,
                                         strEsMonedaPago:=strEsMonedaPago,
                                         strEsDual:=strEsDual,
                                         strMonedaIndice:=strMonedaIndice,
                                         strEsMonedaPagoConUf:=strEsMonedaPagoConUf)
    End Sub
    Public Function gSub_BuscarSimboloMoneda(ByVal strCodigo As String) As String
        Dim objMoneda As ClsMoneda = New ClsMoneda
        Dim strRetorno As String = "OK"
        Dim DS_Moneda As DataSet
        DS_Moneda = objMoneda.Moneda_Ver(strCodigo:=strCodigo,
                                         strColumnas:="MONEDA_SIMBOLO",
                                         strEstado:="TOD",
                                         strRetorno:=strRetorno,
                                         strEsMonedaPago:="",
                                         strEsDual:="D",
                                         strMonedaIndice:="M",
                                         strEsMonedaPagoConUf:="NO")
        If strRetorno = "OK" Then
            If DS_Moneda.Tables.Count > 0 Then
                gSub_BuscarSimboloMoneda = DS_Moneda.Tables(0).Rows(0)("MONEDA_SIMBOLO").ToString
            Else
                gSub_BuscarSimboloMoneda = "$"
            End If
        Else
            gSub_BuscarSimboloMoneda = "$"
        End If
    End Function

    Public Sub gSub_BuscarEmisor(ByVal strCodigo As String,
                                 ByVal strColumnas As String,
                                 ByRef strRetorno As String,
                                 ByRef DS_Emisor As DataSet,
                                 Optional ByVal strEstado As String = "VIG")
        Dim objEmisor As ClsEmisor = New ClsEmisor
        DS_Emisor = objEmisor.Emisor_Ver(strCodigo, strColumnas, strEstado, strRetorno)
    End Sub

    Public Sub gSub_BuscarBancos(ByVal strCodigo As String,
                                 ByVal strCodPais As String,
                                 ByVal strCodMercado As String,
                                 ByVal strColumnas As String,
                                 ByRef strRetorno As String,
                                 ByRef DS_Banco As DataSet,
                                 Optional ByVal strEstado As String = "VIG")
        Dim objBanco As ClsBanco = New ClsBanco
        DS_Banco = objBanco.Banco_Ver(strCodigo, strEstado, strCodPais, strCodMercado, strColumnas, strRetorno)
    End Sub

    Public Sub gSub_BuscarTiposDeLiquidacion(ByVal intIdTipoDeLiquidacion As Integer,
                                             ByVal intIdNegocio As Integer,
                                             ByVal strCodSubClaseIns As String,
                                             ByVal strPagoCobro As String,
                                             ByVal intDiasRetencion As Integer,
                                             ByVal strColumnas As String,
                                             ByRef strRetorno As String,
                                             ByRef DS_TiposDeLiquidaciones As DataSet,
                                             Optional ByVal intIdInstrumento As Integer = 0)

        Dim objTipoDeLiquidacion As ClsTipoDeLiquidacion = New ClsTipoDeLiquidacion
        DS_TiposDeLiquidaciones = objTipoDeLiquidacion.TipoDeLiquidacion_Ver(intIdTipoDeLiquidacion, intIdNegocio, strCodSubClaseIns, strPagoCobro, strColumnas, strRetorno, intIdInstrumento)
    End Sub

    Public Sub gSub_BuscarTiposPasivo(ByVal strDescTipoPasivo As String,
                                        ByRef strRetorno As String,
                                        ByRef DS_TiposPasivo As DataSet)

        Dim objTipoPasivo As ClsTipoPasivo = New ClsTipoPasivo
        DS_TiposPasivo = objTipoPasivo.Consultar_Tipo_Pasivo(strDescTipoPasivo, strRetorno)
    End Sub
    Public Sub gSub_BuscarTiposContacto(ByVal intIdTipoContacto As Integer,
                                    ByVal strDescTipoContacto As String,
                                    ByRef strRetorno As String,
                                    ByRef DS_TiposContacto As DataSet)

        Dim objTipoContacto As ClsTipoContacto = New ClsTipoContacto
        DS_TiposContacto = objTipoContacto.Consultar_Tipo_Contacto(intIdTipoContacto, strDescTipoContacto, strRetorno)
    End Sub
#End Region '....

#Region " Funciones Rogers Abreu "



    Public Function gFun_BuscarFechaHoraSistema(ByRef strFecha As String, ByRef strHora As String) As String
        Dim lstrResultado As String = ""
        Dim objGeneral As ClsGeneral = New ClsGeneral

        ' LOS PARAMETROS DE ENTRADA POR EL MOMENTO NO TIENEN EFECTO SOBRE EL RESULTADO PERO A FUTURO SI PODRIA SER ASI
        objGeneral.TraerFechaHoraSistema("DD/MM/RRRR", "HH24:MI:SS", strFecha, strHora, lstrResultado)
        Return lstrResultado
    End Function

    Public Function gFun_Busca_RelacionPublicador_GPI(
                ByVal strEntidadGPI As String,
                ByVal strEntidadPublicador As String,
                ByVal strCodigoPublicador As String) As String
        Try
            Dim objRelacionPublicador As ClsRelacionPublicadorGPI = New ClsRelacionPublicadorGPI
            Return objRelacionPublicador.Entidad_Relacion_Publicador_GPI_Buscar(strEntidadGPI, strEntidadPublicador, strCodigoPublicador)
        Catch ex As Exception
            Return ""
        End Try
    End Function

#End Region '...

#Region " Funciones Fabi�n Bonilla "

    Public Function gFun_BuscarDiaHabil(Optional ByVal strCodPais As String = "CHI", Optional ByVal strFecha As String = "", Optional ByVal lngNumDias As Long = 1) As String
        Dim lstrResultado As String = ""
        Dim objGeneral As ClsGeneral = New ClsGeneral

        Return objGeneral.TraerDiaHabil(strCodPais, strFecha, lngNumDias, lstrResultado)
    End Function

    Public Function gFun_BuscarDiaHabil_Anterior(Optional ByVal strCodPais As String = "CHI", Optional ByVal strFecha As String = "", Optional ByVal lngNumDias As Long = 1) As String
        Dim lstrResultado As String = ""
        Dim objGeneral As ClsGeneral = New ClsGeneral

        Return objGeneral.TraerDiaHabil_Anterior(strCodPais, strFecha, lngNumDias, lstrResultado)
    End Function

    Public Sub CargaComboMonedas(ByRef meComboBox As C1.Win.C1List.C1Combo,
                                Optional ByVal strTransaccion As String = "",
                                Optional ByVal strCodigoMoneda As String = "",
                                Optional ByVal strEstadoMoneda As String = "",
                                Optional ByVal bIfBlank As Boolean = True,
                                Optional ByVal strEsMonedaPago As String = "",
                                Optional ByVal strEsDual As String = "D",
                                Optional ByVal strMonedaIndice As String = "M",
                                Optional ByVal strSoloMonedaMX As String = "NO",
                                Optional ByVal strFlgConUF As String = "NO")
        Dim lobjMoneda As ClsMoneda = New ClsMoneda
        Dim DS_General As New DataSet
        Dim MonedaTemp As DataRow
        Dim ltxtColumnas As String
        Dim ltxtResultadoTransaccion As String = Nothing

        Try
            ltxtColumnas = "MONEDA_CODIGO,MONEDA_DESCRIPCION,MONEDA_DECIMALES"
            '...Limpia Combo
            meComboBox.ClearItems()
            meComboBox.Text = ""
            '...Inicializa Data Set Combo
            DS_General = Nothing
            DS_General = lobjMoneda.Moneda_Ver(strCodigoMoneda, ltxtColumnas, strEstadoMoneda, ltxtResultadoTransaccion, strEsMonedaPago, strEsDual, strMonedaIndice, strSoloMonedaMX, strFlgConUF)

            If ltxtResultadoTransaccion.ToUpper = "OK" Then
                '+ Si se indico que acepta blanco se incluye un registro en blanco en el combo
                If bIfBlank Then
                    MonedaTemp = DS_General.Tables("Moneda").NewRow()
                    MonedaTemp("MONEDA_DESCRIPCION") = ""
                    MonedaTemp("MONEDA_CODIGO") = ""
                    MonedaTemp("MONEDA_DECIMALES") = "0"
                    DS_General.Tables("Moneda").Rows.Add(MonedaTemp)
                    DS_General.AcceptChanges()
                End If

                '...Asigna Data Source
                meComboBox.AddItem(";")
                meComboBox.DataSource = DS_General.Tables("Moneda")
                meComboBox.Sort(0, C1.Win.C1List.SortDirEnum.ASC)
                '...Ordena y visibilidad de columnas del combo
                meComboBox.Columns(0).DataField = "MONEDA_DESCRIPCION"
                meComboBox.Splits(0).DisplayColumns(0).Visible = True

                meComboBox.Columns(1).DataField = "MONEDA_CODIGO"
                meComboBox.Splits(0).DisplayColumns(1).Visible = False

                meComboBox.Columns(2).DataField = "MONEDA_DECIMALES"
                meComboBox.Splits(0).DisplayColumns(2).Visible = False
            End If
        Catch ex As Exception
            ltxtResultadoTransaccion = ex.Message
        Finally
            If ltxtResultadoTransaccion <> "OK" Then
                MsgBox("Error al cargar combo monedas : " & ltxtResultadoTransaccion, MsgBoxStyle.Information, gtxtNombreSistema & strTransaccion)
            End If
        End Try
    End Sub

    Public Sub CargaComboSubClase(ByRef meComboBox As C1.Win.C1List.C1Combo, ByVal strCodClaseInstrumento As String, Optional ByVal lstrTransaccion As String = "", Optional ByVal bIfBlank As Boolean = True)
        Dim lobjSubClases As ClsSubClaseInstrumento = New ClsSubClaseInstrumento
        Dim DS_General As New DataSet
        Dim lstrColumnas As String
        Dim lstrResultadoTransaccion As String = ""
        Dim DatoTemp As DataRow

        lstrColumnas = "DSC_SUB_CLASE_INSTRUMENTO, COD_SUB_CLASE_INSTRUMENTO, COD_MERCADO, FLG_APORTE_CAPITAL"
        '...Limpia Combo
        meComboBox.ClearItems()
        meComboBox.Text = ""
        '...Inicializa Data Set Combo
        DS_General = Nothing
        DS_General = lobjSubClases.TraeDatosSubClaseInstrumento(strCodClaseInstrumento, "", lstrColumnas, lstrResultadoTransaccion)
        Try
            If lstrResultadoTransaccion.ToUpper <> "OK" Then
                MsgBox("Error al cargar combo sub-clase instrumentos : " & strCodClaseInstrumento, MsgBoxStyle.Information, gtxtNombreSistema)
            Else
                If bIfBlank Then
                    DatoTemp = DS_General.Tables("SubClaseInstrumento").NewRow()
                    DatoTemp("DSC_SUB_CLASE_INSTRUMENTO") = ""
                    DS_General.Tables("SubClaseInstrumento").Rows.Add(DatoTemp)
                    DS_General.AcceptChanges()
                End If
                '...Asigna Data Source
                meComboBox.DataSource = DS_General.Tables("SubClaseInstrumento")
                meComboBox.Sort(0, C1.Win.C1List.SortDirEnum.ASC)
                '...Ordena y visibilidad de columnas del combo
                meComboBox.Columns(0).DataField = "DSC_SUB_CLASE_INSTRUMENTO"
                meComboBox.Splits(0).DisplayColumns(0).Visible = True
                meComboBox.Columns(1).DataField = "COD_SUB_CLASE_INSTRUMENTO"
                meComboBox.Splits(0).DisplayColumns(1).Visible = False
                meComboBox.Columns(2).DataField = "COD_MERCADO"
                meComboBox.Splits(0).DisplayColumns(2).Visible = False
                meComboBox.Columns(3).DataField = "FLG_APORTE_CAPITAL"
                meComboBox.Splits(0).DisplayColumns(3).Visible = False
            End If
        Catch ex As Exception
            MsgBox("Error al cargar combo sub-clase de instrumentos : " & strCodClaseInstrumento, MsgBoxStyle.Information, gtxtNombreSistema & lstrTransaccion)
        End Try
    End Sub

    Public Sub CargaComboSubClaseF(ByRef meComboBox As C1.Win.C1List.C1Combo,
                                   ByVal strCodClaseInstrumento As String,
                                   ByVal strCodFamilia As String,
                                   Optional ByVal lstrTransaccion As String = "",
                                   Optional ByVal bIfBlank As Boolean = True,
                                   Optional ByVal strCodMoneda As String = "",
                                   Optional ByVal strCodTipoEmisor As String = "")
        Dim lobjSubClases As ClsSubClaseInstrumento = New ClsSubClaseInstrumento
        Dim DS_General As New DataSet
        Dim lstrColumnas As String
        Dim lstrResultadoTransaccion As String = ""
        Dim DatoTemp As DataRow

        lstrColumnas = "DSC_SUB_CLASE_INSTRUMENTO, COD_SUB_CLASE_INSTRUMENTO, COD_MERCADO, FLG_APORTE_CAPITAL, COD_MONEDA, COD_TIPO_EMISOR "

        '...Limpia Combo
        meComboBox.ClearItems()
        meComboBox.Text = ""
        '...Inicializa Data Set Combo
        DS_General = Nothing
        DS_General = lobjSubClases.TraeDatosSubClaseInstrumentoF(strCodClaseInstrumento, strCodFamilia, strCodMoneda, strCodTipoEmisor, lstrColumnas, lstrResultadoTransaccion)
        Try
            If lstrResultadoTransaccion.ToUpper <> "OK" Then
                MsgBox("Error al cargar combo sub-clase instrumentos : " & strCodClaseInstrumento, MsgBoxStyle.Information, gtxtNombreSistema)
            Else
                If bIfBlank Then
                    DatoTemp = DS_General.Tables("SubClaseInstrumento").NewRow()
                    DatoTemp("DSC_SUB_CLASE_INSTRUMENTO") = ""
                    DS_General.Tables("SubClaseInstrumento").Rows.Add(DatoTemp)
                    DS_General.AcceptChanges()
                End If
                '...Asigna Data Source
                meComboBox.DataSource = DS_General.Tables("SubClaseInstrumento")
                meComboBox.Sort(0, C1.Win.C1List.SortDirEnum.ASC)
                '...Ordena y visibilidad de columnas del combo
                meComboBox.Columns(0).DataField = "DSC_SUB_CLASE_INSTRUMENTO"
                meComboBox.Splits(0).DisplayColumns(0).Visible = True
                meComboBox.Columns(1).DataField = "COD_SUB_CLASE_INSTRUMENTO"
                meComboBox.Splits(0).DisplayColumns(1).Visible = False
                meComboBox.Columns(2).DataField = "COD_MERCADO"
                meComboBox.Splits(0).DisplayColumns(2).Visible = False
                meComboBox.Columns(3).DataField = "FLG_APORTE_CAPITAL"
                meComboBox.Splits(0).DisplayColumns(3).Visible = False
                meComboBox.Columns(4).DataField = "COD_MONEDA"
                meComboBox.Splits(0).DisplayColumns(4).Visible = False
                meComboBox.Columns(5).DataField = "COD_TIPO_EMISOR"
                meComboBox.Splits(0).DisplayColumns(5).Visible = False
            End If
        Catch ex As Exception
            MsgBox("Error al cargar combo sub-clase de instrumentos : " & strCodClaseInstrumento, MsgBoxStyle.Information, gtxtNombreSistema & lstrTransaccion)
        End Try
    End Sub

    Public Sub CargaComboMonedaPago(ByRef meComboBox As C1.Win.C1List.C1Combo,
                                    Optional ByVal lngIdCuenta As Long = 0,
                                    Optional ByVal strCodMonedaPago As String = "",
                                    Optional ByVal strCodEstado As String = "TOD",
                                    Optional ByVal lstrTransaccion As String = "CargaComboMonedaPago",
                                    Optional ByVal strCodMonedaExc As String = "",
                                    Optional ByVal strEsMonedaPagoConUf As String = "NO",
                                    Optional ByVal bIfBlank As Boolean = False)
        Dim lobjCajaCuenta As ClsCaja = New ClsCaja

        Dim DS_General As New DataSet
        Dim lstrColumnas As String
        Dim lstrResultadoTransaccion As String = ""

        Dim DatoTemp As DataRow

        lstrColumnas = "MONEDA_DESCRIPCION, MONEDA_CODIGO, MONEDA_FLAG_MONEDA_PAGO, MONEDA_DECIMALES, MONEDA_SIMBOLO, MONEDA_ESTADO, MONEDA_ESTADO_DESCRIPCION"
        '...Limpia Combo
        meComboBox.ClearItems()
        meComboBox.Text = ""
        '...Inicializa Data Set Combo
        DS_General = Nothing
        DS_General = lobjCajaCuenta.TraeDatosMonedaPago(lngIdCuenta, strCodMonedaPago, strCodEstado, lstrColumnas, lstrResultadoTransaccion, strCodMonedaExc, strEsMonedaPagoConUf)
        Try
            If lstrResultadoTransaccion.ToUpper <> "OK" Then
                MsgBox("Error al cargar combo modedas de pago: " & strCodMonedaPago & "[" & lstrResultadoTransaccion & "]", MsgBoxStyle.Information, gtxtNombreSistema)
            Else
                If bIfBlank Then
                    DatoTemp = DS_General.Tables("Caja_Cuenta").NewRow()
                    DatoTemp(0) = ""
                    DatoTemp(1) = ""
                    DatoTemp(2) = ""
                    DatoTemp(3) = 0
                    DatoTemp(4) = 0
                    DatoTemp(5) = ""
                    DatoTemp(6) = ""
                    DS_General.Tables("Caja_Cuenta").Rows.Add(DatoTemp)
                    DS_General.AcceptChanges()
                End If
                '...Asigna Data Source
                meComboBox.DataSource = DS_General.Tables("Caja_Cuenta")
                meComboBox.Sort(1, C1.Win.C1List.SortDirEnum.ASC)
                '...Ordena y visibilidad de columnas del combo
                If DS_General.Tables("Caja_Cuenta").Rows.Count >= 1 Then
                    meComboBox.SelectedIndex = 0
                    meComboBox.Enabled = Not (DS_General.Tables("Caja_Cuenta").Rows.Count = 1)
                End If
                meComboBox.ColumnWidth = meComboBox.Width - 30
                meComboBox.Splits(0).DisplayColumns(0).Visible = True
                meComboBox.Splits(0).DisplayColumns(1).Visible = False
                meComboBox.Splits(0).DisplayColumns(2).Visible = False
                meComboBox.Splits(0).DisplayColumns(3).Visible = False
                meComboBox.Splits(0).DisplayColumns(4).Visible = False
                meComboBox.Splits(0).DisplayColumns(5).Visible = False
                meComboBox.Splits(0).DisplayColumns(6).Visible = False
            End If
        Catch ex As Exception
            MsgBox("Error al cargar combo monedas de pago: " & strCodMonedaPago, MsgBoxStyle.Information, gtxtNombreSistema & lstrTransaccion)
        End Try
    End Sub

    Public Sub CargaComboContrapartes(ByRef meComboBox As C1.Win.C1List.C1Combo,
                                      ByVal strClaseInstrumento As String,
                                      Optional ByVal bIfBlank As Boolean = True,
                                      Optional ByVal strIngresaOper As String = "S",
                                      Optional ByVal strDscTipoContraparte As String = "")

        Dim objContrapartes As ClsContraParte = New ClsContraParte
        Dim lstrColumnas As String = "DSC_CONTRAPARTE, ID_CONTRAPARTE"
        Dim lstrResultado As String = ""
        Dim DS_Contrapartes As New DataSet
        Dim DatoTemp As DataRow

        gSub_BuscarContraparte(0, strIngresaOper, strClaseInstrumento, "", lstrColumnas, lstrResultado, DS_Contrapartes, , strDscTipoContraparte)

        If lstrResultado = "OK" Then
            If bIfBlank Then
                DatoTemp = DS_Contrapartes.Tables("CONTRAPARTE").NewRow()
                DatoTemp("DSC_CONTRAPARTE") = ""
                DS_Contrapartes.Tables("CONTRAPARTE").Rows.Add(DatoTemp)
                DS_Contrapartes.AcceptChanges()
            End If
            meComboBox.DataSource = DS_Contrapartes.Tables("CONTRAPARTE")
            meComboBox.Splits(0).DisplayColumns("DSC_CONTRAPARTE").Visible = True
            meComboBox.Splits(0).DisplayColumns("ID_CONTRAPARTE").Visible = False
            meComboBox.Sort(0, C1.Win.C1List.SortDirEnum.ASC)
            meComboBox.Text = ""
        End If
    End Sub

    Public Sub RecuperaAccesoDeControles(ByVal lint_NumeroDePantalla As Integer, ByRef larr_AccesoDeControles(,) As String)
        Dim objAccesoDeControles As ClsGeneral = New ClsGeneral
        Try
            If Not (objAccesoDeControles.TraerControles(lint_NumeroDePantalla, larr_AccesoDeControles)) Then
                Exit Sub
            End If
        Catch ex As Exception
            larr_AccesoDeControles = Nothing
            objAccesoDeControles = Nothing
        End Try
    End Sub

    Public Sub Accion_NoPermitida(ByVal lstrMensajeAccionNoPermitida As String, Optional ByVal lstrAccion As String = "ejecutar", Optional ByVal lstrTransaccion As String = "")
        If lstrMensajeAccionNoPermitida.Trim = "" Then
            lstrMensajeAccionNoPermitida = "Ud. no est� autorizado para realizar esta acci�n."
        Else
            lstrMensajeAccionNoPermitida = "Ud. no est� autorizado para " & lstrAccion & " " & lstrMensajeAccionNoPermitida.Replace("&", "") & "."
        End If
        MsgBox(lstrMensajeAccionNoPermitida, MsgBoxStyle.Information + MsgBoxStyle.OkOnly, gtxtNombreSistema & lstrTransaccion)
    End Sub

    Public Function gFun_TipoEstadoDesc(ByVal strCodigoTipo As String, ByVal strCodigoEstado As String, ByVal strDefault As String, ByVal strTransaccion As String) As String
        Dim DS_Estado As DataSet
        Dim lobjEstados As ClsEstados = New ClsEstados
        Dim lstrResultadoTransaccion As String = ""
        Dim lstrMsg As String = ""

        DS_Estado = lobjEstados.Estados_Ver(strCodigoEstado, strCodigoTipo, strDefault, "", lstrResultadoTransaccion)
        Try
            If lstrResultadoTransaccion.ToUpper <> "OK" Then
                lstrMsg = lstrResultadoTransaccion
                MsgBox("Error al obtener la descripci�n del Estados : " & strCodigoEstado, MsgBoxStyle.Information, gtxtNombreSistema)
            Else

            End If
        Catch ex As Exception
            lstrMsg = ex.Message
        Finally
            gFun_TipoEstadoDesc = DS_Estado.Tables(0).Rows(0)("DSC_ESTADO")
            If lstrMsg <> "" Then
                MsgBox("Error al obtenr la descripci�n del estado: " & strCodigoEstado & vbCr & "[" & lstrMsg & "]", MsgBoxStyle.Information, gtxtNombreSistema & strTransaccion)
            End If
        End Try
    End Function

    Public Function gFun_ResultadoMsg(ByVal strMsg As String, ByVal strTransaccion As String, Optional ByVal blnSoloValidar As Boolean = False, Optional ByVal strSepColumna As String = "|") As Boolean
        Dim arr_Columnas() As String
        Dim lblnSimple As Boolean
        Dim lstrMensaje As String
        Dim lstrTipoError As String
        Dim msrResultadoMensaje As MsgBoxResult

        If strMsg = "" Then
            gFun_ResultadoMsg = True
            Exit Function
        End If

        '+ Se verifica si el mensaje viene con m�s de una columna; es decir, es compuesto no simple
        lblnSimple = IIf(InStr(strMsg, strSepColumna) > 0, False, True)

        If lblnSimple Then
            lstrMensaje = "OK"
            If strMsg = "OK" Then
                lstrTipoError = "0"
            Else
                lstrTipoError = "1"
                lstrMensaje = strMsg
            End If
        Else
            If Mid(strMsg, 1, 1) <> strSepColumna Then
                strMsg = strSepColumna & strMsg
            End If
            arr_Columnas = strMsg.Split(strSepColumna)
            lstrTipoError = arr_Columnas(1)
            lstrMensaje = arr_Columnas(2)
        End If

        '+ Si el tipo de error es: 1)Error
        If lstrTipoError = "1" Then
            gFun_ResultadoMsg = False
        Else
            gFun_ResultadoMsg = True
        End If

        '+ Si solamente se utiliza para validar y no mostrar el mensaje
        If Not blnSoloValidar Then
            '+ Si hay que desplegar un mensaje
            If lstrTipoError <> "0" Then
                Select Case lstrTipoError
                    Case "1"
                        '+ Mensaje de Error
                        MsgBox(lstrMensaje, MsgBoxStyle.Critical, gtxtNombreSistema & strTransaccion)
                    Case "2"
                        '+ Mensaje de Advertencia
                        MsgBox(lstrMensaje, MsgBoxStyle.Exclamation, gtxtNombreSistema & strTransaccion)
                    Case "3"
                        '+ Mensaje de Informaci�n
                        MsgBox(lstrMensaje, MsgBoxStyle.Information, gtxtNombreSistema & strTransaccion)
                    Case "4"
                        '+ Mensaje de Pregunta
                        MsgBox(lstrMensaje, MsgBoxStyle.Question, gtxtNombreSistema & strTransaccion)
                    Case "5"
                        '+ Mensaje de Advertencia con pregunta con el segundo bot�n habilitado (bot�n del no).
                        msrResultadoMensaje = MsgBox(lstrMensaje, MsgBoxStyle.DefaultButton2 + MsgBoxStyle.YesNo + MsgBoxStyle.Exclamation, gtxtNombreSistema & strTransaccion)
                        If msrResultadoMensaje = MsgBoxResult.No Then
                            gFun_ResultadoMsg = False
                        End If
                    Case "6"
                        '+ Mensaje de Advertencia con pregunta con el primer bot�n habilitado (bot�n del si).
                        msrResultadoMensaje = MsgBox(lstrMensaje, MsgBoxStyle.DefaultButton1 + MsgBoxStyle.YesNo + MsgBoxStyle.Exclamation, gtxtNombreSistema & strTransaccion)
                        If msrResultadoMensaje = MsgBoxResult.No Then
                            gFun_ResultadoMsg = False
                        End If
                    Case Else
                        MsgBox(lstrMensaje, , gtxtNombreSistema & strTransaccion)
                End Select
            End If
        End If
    End Function

    Public Sub gSubCargaComboMonedaPermitidas(
            ByRef objetoControl As C1.Win.C1List.C1Combo)
        Try
            Dim ldtbEstados As New DataTable
            Dim ldrNewRowEstado As DataRow

            With ldtbEstados
                .Columns.Clear()
                .Rows.Clear()
                .Columns.Add("MONEDA_DESCRIPCION", GetType(String))
                .Columns.Add("MONEDA_CODIGO", GetType(String))

                ldrNewRowEstado = .NewRow
                ldrNewRowEstado("MONEDA_DESCRIPCION") = "PESO"
                ldrNewRowEstado("MONEDA_CODIGO") = "CLP"
                Call .Rows.Add(ldrNewRowEstado)

                ldrNewRowEstado = .NewRow
                ldrNewRowEstado("MONEDA_DESCRIPCION") = "DOLAR"
                ldrNewRowEstado("MONEDA_CODIGO") = "USD"
                Call .Rows.Add(ldrNewRowEstado)
            End With

            With objetoControl
                .DataSource = ldtbEstados
                .Columns(0).DataField = "MONEDA_DESCRIPCION"
                .Columns(1).DataField = "MONEDA_CODIGO"
                .Splits(0).DisplayColumns(1).Visible = False
                .Text = ""
                .Sort(0, C1.Win.C1List.SortDirEnum.ASC)
                .SelectedIndex = -1
            End With
        Catch ex As Exception

        End Try
    End Sub

    Public Sub ObtenerComisionGeneral(ByRef DS_EsquemaComision As DataSet,
                                        ByVal lstrClase As String,
                                        ByVal lstrSubClase As String,
                                        ByVal lstrTipoOperacionMov As String,
                                        ByVal lstrIdCuenta As String,
                                        ByVal dtbDetalle As DataTable,
                                        ByVal lstrCodMoneda As String,
                                        ByVal dtbComision As DataTable,
                                        ByRef ldblSubTotal As Double,
                                        ByRef ldblMontoComisionConImpuesto As Double,
                                        ByRef ldblMontoComisionSinImpuesto As Double,
                                        ByRef ldblTotal As Double,
                                        ByVal lidcontraparte As Integer,
                                        Optional ByVal bReload As Boolean = False,
                                        Optional ByVal lstrTransaccion As String = "ObtenerComisionGeneral",
                                        Optional ByVal txtTipOperacionCompraVentaEtc As String = "",
                                        Optional ByVal lstrCodFamilia As String = "")
        Dim lstrConceptoComision As Integer = 0
        Dim lstrTipoOperacion As String = IIf(lstrTipoOperacionMov = "I", "COMPRA", "VENTA")

        Dim ldblMontoOperacion As Double = 0
        Dim lstrCodigoEstado As String = "V"
        Dim lstrColumnas As String = ""
        Dim lstrResultado As String = ""
        Dim lstrSubClaseInstrumento As String = lstrSubClase

        Dim lblnAplicaPorcentaje As Boolean = True
        Dim lblnAfectoImpuesto As Boolean = True

        Dim lobjComision As ClsComision = New ClsComision

        '...El calculo de comision SOLO DEBE CALCULAR PARA "COMPRAS" o "VENTAS"
        If txtTipOperacionCompraVentaEtc = "APORTE" Or txtTipOperacionCompraVentaEtc = "RESCATE" Then
            txtTipOperacionCompraVentaEtc = IIf(txtTipOperacionCompraVentaEtc = "APORTE", "COMPRA", "VENTA")
        End If
        If txtTipOperacionCompraVentaEtc.Trim.ToUpper = "COMPRA" Or txtTipOperacionCompraVentaEtc.Trim.ToUpper = "VENTA" Then
            If bReload Then
                lstrColumnas = "ID_COMISION," &
                   "ID_GRUPO_COMISIONES," &
                   "ID_CONCEPTO_COMISION," &
                   "DSC_CONCEPTO_COMISION," &
                   "COD_CLASE_INSTRUMENTO," &
                   "COD_SUB_CLASE_INSTRUMENTO," &
                   "ID_CONTRAPARTE," &
                   "DSC_CONTRAPARTE," &
                   "TIPO_OPERACION," &
                   "PORCENTAJE," &
                   "MONTO," &
                   "MONTO_IMPUESTO," &
                   "AFECTO_IMPUESTO," &
                   "COBRO_MINIMO," &
                   "RANGO_DESDE," &
                   "RANGO_HASTA," &
                   "FECHA_VIGENCIA," &
                   "APLICA_PORCENTAJE," &
                   "DECIMALES_COMISION"
                DS_EsquemaComision = lobjComision.Comision_BuscarEsquema(lstrIdCuenta,
                                                                        0,
                                                                        "TRANS",
                                                                        lstrConceptoComision,
                                                                        lstrTipoOperacion,
                                                                        lstrClase,
                                                                        lstrSubClaseInstrumento,
                                                                        lidcontraparte,
                                                                        lstrCodMoneda,
                                                                        ldblMontoOperacion,
                                                                        lstrCodigoEstado,
                                                                        lstrColumnas,
                                                                        lstrResultado,
                                                                        lstrCodFamilia)
            Else
                lstrResultado = "OK"
            End If
            If lstrResultado = "OK" Then
                If DS_EsquemaComision.Tables.Count > 0 Then
                    If DS_EsquemaComision.Tables(0).Rows.Count > 0 Then
                        CalcularComisionEncabezado(DS_EsquemaComision, dtbDetalle,
                                                   dtbComision, ldblSubTotal, ldblMontoComisionConImpuesto, ldblMontoComisionSinImpuesto, 1)
                    Else
                        If dtbDetalle.Rows.Count > 0 Then
                            ldblSubTotal = dtbDetalle.Compute("SUM(MONTO2)", "")
                        Else
                            ldblSubTotal = 0
                        End If
                    End If
                    '+ Si es un ingreso entonces al monto a invertir se le suman los cobros
                    If lstrTipoOperacionMov = "I" Then
                        ldblTotal = ldblSubTotal + (ldblMontoComisionConImpuesto + ldblMontoComisionSinImpuesto)
                    Else
                        '+ Si es un egreso entonces al monto a invertir se le restan los cobros
                        ldblTotal = ldblSubTotal - (ldblMontoComisionConImpuesto + ldblMontoComisionSinImpuesto)
                    End If
                End If
            Else
                gFun_ResultadoMsg("1|No pudo obtener las informaci�n de comisiones" & vbCrLf & lstrResultado, gtxtNombreSistema & "-" & lstrTransaccion)
            End If
        Else
            If dtbDetalle.Rows.Count > 0 Then
                ldblSubTotal = dtbDetalle.Compute("SUM(MONTO2)", "")
            Else
                ldblSubTotal = 0
            End If
            '+ Si es un ingreso entonces al monto a invertir se le suman los cobros
            If lstrTipoOperacionMov = "I" Then
                ldblTotal = ldblSubTotal + (ldblMontoComisionConImpuesto + ldblMontoComisionSinImpuesto)
            Else
                '+ Si es un egreso entonces al monto a invertir se le restan los cobros
                ldblTotal = ldblSubTotal - (ldblMontoComisionConImpuesto + ldblMontoComisionSinImpuesto)
            End If
        End If
    End Sub

    Public Sub ObtenerComisionGeneral2(ByRef DS_EsquemaComision As DataSet,
                                        ByVal lstrClase As String,
                                        ByVal lstrSubClase As String,
                                        ByVal lstrTipoOperacionMov As String,
                                        ByVal lstrIdCuenta As String,
                                        ByVal dtbDetalle As DataTable,
                                        ByVal lstrCodMoneda As String,
                                        ByVal dtbComision As DataTable,
                                        ByRef ldblSubTotal As Double,
                                        ByRef ldblMontoComisionConImpuesto As Double,
                                        ByRef ldblMontoComisionSinImpuesto As Double,
                                        ByRef ldblTotal As Double,
                                        ByVal lidcontraparte As Integer,
                                        Optional ByVal bReload As Boolean = False,
                                        Optional ByVal lstrTransaccion As String = "ObtenerComisionGeneral",
                                        Optional ByVal txtTipOperacionCompraVentaEtc As String = "",
                                        Optional ByVal lstrCodFamilia As String = "")
        Dim lstrConceptoComision As String = ""
        Dim lstrTipoOperacion As String = IIf(lstrTipoOperacionMov = "I", "COMPRA", "VENTA")

        'Dim ldblMontoOperacion As Double = 0
        Dim lstrCodigoEstado As String = "V"
        Dim lstrColumnas As String = ""
        Dim lstrResultado As String = ""
        Dim lstrSubClaseInstrumento As String = lstrSubClase

        Dim lblnAplicaPorcentaje As Boolean = True
        Dim lblnAfectoImpuesto As Boolean = True
        Dim dtrComision As DataRow
        Dim id_grupo_comisiones As Integer
        Dim ldblValor_Buscar_Rango As Double
        Dim lobjComision As ClsComision = New ClsComision

        Dim ldblTipoCambio As Double
        Dim dblParidad As Double
        Dim strOperacion As String = ""
        Dim strDescError As String = ""
        Dim strFechaConsulta As String = ""
        Dim clsGeneral As New ClsGeneral
        Dim lintIdCuenta As Integer
        '...El calculo de comision SOLO DEBE CALCULAR PARA "COMPRAS" o "VENTAS"
        lintIdCuenta = CInt(lstrIdCuenta)

        If txtTipOperacionCompraVentaEtc = "APORTE" Or txtTipOperacionCompraVentaEtc = "RESCATE" Then
            txtTipOperacionCompraVentaEtc = IIf(txtTipOperacionCompraVentaEtc = "APORTE", "COMPRA", "VENTA")
        End If

        If txtTipOperacionCompraVentaEtc.Trim.ToUpper = "COMPRA" Or txtTipOperacionCompraVentaEtc.Trim.ToUpper = "VENTA" Then
            If bReload Then
                If dtbDetalle.Rows.Count > 0 Then
                    ldblValor_Buscar_Rango = dtbDetalle.Compute("SUM(MONTO2)", "")
                Else
                    ldblValor_Buscar_Rango = 0
                End If

                lstrColumnas = "ID_COMISION," &
                   "ID_GRUPO_COMISIONES," &
                   "ID_CONCEPTO_COMISION," &
                   "DSC_CONCEPTO_COMISION," &
                   "COD_CLASE_INSTRUMENTO," &
                    "ID_CONTRAPARTE," &
                   "TIPO_OPERACION," &
                   "PORCENTAJE," &
                   "MONTO," &
                   "AFECTO_IMPUESTO," &
                   "COBRO_MINIMO," &
                   "FECHA_VIGENCIA," &
                   "APLICA_PORCENTAJE," &
                   "COD_MONEDA"

                DS_EsquemaComision = lobjComision.Plantilla_Comision_Consultar(lstrColumnas,
                                  lstrIdCuenta,
                                  "",
                                  "TRANS",
                                  lstrTipoOperacion,
                                  lstrClase,
                                  Trim(lstrCodFamilia),
                                  lidcontraparte,
                                   lstrConceptoComision,
                                   lstrResultado,
                                   ldblValor_Buscar_Rango,
                                   "CLP", "")
            Else
                lstrResultado = "OK"
            End If

            If dtbDetalle.Rows.Count > 0 And (Not DS_EsquemaComision Is Nothing AndAlso DS_EsquemaComision.Tables.Count > 0) Then
                If dtbDetalle.Rows(0).Item("COD_MONEDA") = DS_EsquemaComision.Tables(0).Rows(0).Item("COD_MONEDA") Then
                    ldblTipoCambio = 1
                Else
                    clsGeneral.TraerTipoCambio(glngNegocioOperacion,
                            lintIdCuenta,
                            dtbDetalle.Rows(0).Item("COD_MONEDA_PAGO"),
                            DS_EsquemaComision.Tables(0).Rows(0).Item("COD_MONEDA"),
                            (Mid(dtbDetalle.Rows(0).Item("FECHA_LIQUIDACION"), 7, 4) & Mid(dtbDetalle.Rows(0).Item("FECHA_LIQUIDACION"), 4, 2) & Mid(dtbDetalle.Rows(0).Item("FECHA_LIQUIDACION"), 1, 2)),
                            0,
                            dblParidad,
                            strOperacion,
                            strDescError)
                    If strOperacion = "M" Then
                        ldblTipoCambio = dblParidad
                    Else
                        ldblTipoCambio = 1 / dblParidad
                    End If

                End If
            End If
            If lstrResultado = "OK" And (Not DS_EsquemaComision Is Nothing) Then
                If DS_EsquemaComision.Tables.Count > 0 Then
                    If DS_EsquemaComision.Tables(0).Rows.Count > 0 Then
                        dtrComision = DS_EsquemaComision.Tables(0).Rows(0)
                        id_grupo_comisiones = dtrComision("ID_GRUPO_COMISIONES")
                        CalcularComisionEncabezado(DS_EsquemaComision, dtbDetalle,
                                                   dtbComision, ldblSubTotal, ldblMontoComisionConImpuesto, ldblMontoComisionSinImpuesto, ldblTipoCambio)
                    Else
                        If dtbDetalle.Rows.Count > 0 Then
                            ldblSubTotal = dtbDetalle.Compute("SUM(MONTO2)", "")
                        Else
                            ldblSubTotal = 0
                        End If
                    End If
                    '+ Si es un ingreso entonces al monto a invertir se le suman los cobros
                    If lstrTipoOperacionMov = "I" Then
                        ldblTotal = ldblSubTotal + (ldblMontoComisionConImpuesto + ldblMontoComisionSinImpuesto)
                    Else
                        '+ Si es un egreso entonces al monto a invertir se le restan los cobros
                        ldblTotal = ldblSubTotal - (ldblMontoComisionConImpuesto + ldblMontoComisionSinImpuesto)
                    End If
                End If
            Else
                'gFun_ResultadoMsg("1|No pudo obtener las informaci�n de comisiones" & vbCrLf & lstrResultado, gtxtNombreSistema & "-" & lstrTransaccion)
                If Not IsDBNull(dtbDetalle.Compute("SUM(MONTO2)", "")) Then
                    ldblSubTotal = dtbDetalle.Compute("SUM(MONTO2)", "")
                    ldblTotal = ldblSubTotal
                End If
            End If
        Else
            If dtbDetalle.Rows.Count > 0 Then
                ldblSubTotal = dtbDetalle.Compute("SUM(MONTO2)", "")
                ldblTotal = ldblSubTotal
            Else
                ldblSubTotal = 0
                ldblTotal = 0
            End If
            '+ Si es un ingreso entonces al monto a invertir se le suman los cobros
            If lstrTipoOperacionMov = "I" Then
                ldblTotal = ldblSubTotal + (ldblMontoComisionConImpuesto + ldblMontoComisionSinImpuesto)
            Else
                '+ Si es un egreso entonces al monto a invertir se le restan los cobros
                ldblTotal = ldblSubTotal - (ldblMontoComisionConImpuesto + ldblMontoComisionSinImpuesto)
            End If
        End If
    End Sub

    Public Sub CalcularComisionImpuestos_SC(ByVal pDataComision As DataSet, ByVal pstrTipoOperacionMov As String,
                                            ByRef pdblSubTotal As Double, ByRef pdblMontoComisionConImpuesto As Double, ByRef pdblMontoComisionSinImpuesto As Double, ByRef pdblTotal As Double)
        Dim lComisionConImpuesto As Double = 0
        Dim lComisionSinImpuesto As Double = 0

        For lintRowCom As Integer = 0 To pDataComision.Tables(0).Rows.Count - 1

            If CDbl("0" & pDataComision.Tables(0).Rows(lintRowCom).Item("MONTO_IMPUESTO").ToString.Trim) <> 0 Then
                lComisionConImpuesto = lComisionConImpuesto + (CDbl(pDataComision.Tables(0).Rows(lintRowCom).Item("MONTO_IMPUESTO").ToString.Trim) + CDbl(pDataComision.Tables(0).Rows(lintRowCom).Item("MONTO").ToString.Trim))
            Else
                lComisionSinImpuesto = lComisionSinImpuesto + (CDbl(pDataComision.Tables(0).Rows(lintRowCom).Item("MONTO").ToString.Trim))
            End If
        Next
        pdblMontoComisionConImpuesto = lComisionConImpuesto
        pdblMontoComisionSinImpuesto = lComisionSinImpuesto

        If pstrTipoOperacionMov = "I" Then
            pdblTotal = pdblSubTotal + (pdblMontoComisionConImpuesto + pdblMontoComisionSinImpuesto)
        Else
            pdblTotal = pdblSubTotal - (pdblMontoComisionConImpuesto + pdblMontoComisionSinImpuesto)
        End If
    End Sub

    Public Sub CalcularComisionEncabezado(ByVal pDS_Comision As DataSet,
                                          ByVal dtbDetalle As DataTable,
                                          ByVal dtbComision As DataTable,
                                          ByRef pdblSubTotal As Double,
                                          ByRef pdblMontoComisionConImpuesto As Double,
                                          ByRef pdblMontoComisionSinImpuesto As Double,
                                          ByRef pdblTipoCambio As Double)

        Dim dtrComision As DataRow
        Dim ldblMonto As Double
        Dim lblnAplicaPorcentaje As Boolean
        Dim lblnAfectoImpuesto As Boolean
        Dim ldblPorcComision As Double
        Dim ldblMontoImpuesto As Double
        Dim ldblSubTotalAux As Double
        Dim ldlbMonto_Excluido As Double = 0
        Dim lredondeo As Integer = 0

        Dim objMoneda As ClsMoneda = New ClsMoneda
        Dim DS_Moneda As DataSet
        Dim strRetorno As String

        dtbComision.Clear()
        pdblMontoComisionConImpuesto = 0
        pdblMontoComisionSinImpuesto = 0

        If dtbDetalle.Rows.Count <= 0 Then
            pdblSubTotal = 0
            pdblMontoComisionConImpuesto = 0
            pdblMontoComisionSinImpuesto = 0
        Else
            pdblSubTotal = dtbDetalle.Compute("SUM(MONTO)", "")
            ldblSubTotalAux = pdblSubTotal
            pdblSubTotal = pdblSubTotal - ldlbMonto_Excluido
            If pDS_Comision.Tables.Count > 0 Then
                lredondeo = 1
                For lintIndex As Integer = 0 To pDS_Comision.Tables(0).Rows.Count - 1
                    dtrComision = pDS_Comision.Tables(0).Rows(lintIndex)
                    If Not IsNothing(dtrComision) Then
                        strRetorno = ""
                        DS_Moneda = objMoneda.Moneda_Ver(dtrComision("COD_MONEDA"), "MONEDA_DECIMALES", "VIG", strRetorno)
                        If strRetorno = "OK" AndAlso Not (DS_Moneda Is Nothing) Then
                            lredondeo = DS_Moneda.Tables(0).Rows(0).Item("MONEDA_DECIMALES")
                        End If

                        lblnAplicaPorcentaje = dtrComision("APLICA_PORCENTAJE")
                        lblnAfectoImpuesto = dtrComision("AFECTO_IMPUESTO")
                        If Not IsDBNull(dtrComision("PORCENTAJE")) And lblnAplicaPorcentaje Then
                            ldblPorcComision = dtrComision("PORCENTAJE")
                            ldblMonto = CDbl(pdblSubTotal) * (ldblPorcComision / 100)
                        Else
                            ldblPorcComision = 0
                            If IsDBNull(dtrComision("MONTO")) Then
                                dtrComision("MONTO") = 0
                            End If
                            ldblMonto = dtrComision("MONTO") / pdblTipoCambio
                        End If
                        ldblMonto = Math.Round(ldblMonto, lredondeo)
                        If Not (IsDBNull(dtrComision("AFECTO_IMPUESTO"))) AndAlso dtrComision("AFECTO_IMPUESTO") = True Then
                            ldblMontoImpuesto = ldblMonto * (gobjParametro.PorcentajeIva.ValorParametro / 100)
                            ldblMontoImpuesto = Math.Round(ldblMontoImpuesto, lredondeo)
                            pdblMontoComisionConImpuesto = pdblMontoComisionConImpuesto + (ldblMonto + ldblMontoImpuesto)
                        Else
                            pdblMontoComisionSinImpuesto = pdblMontoComisionSinImpuesto + ldblMonto
                            pdblMontoComisionSinImpuesto = Math.Round(pdblMontoComisionSinImpuesto, lredondeo)
                            ldblMontoImpuesto = 0
                        End If
                    End If

                    Dim dtrFila As DataRow
                    Dim lintNroDetalle As Integer = 0
                    lintNroDetalle = 1


                    dtrFila = dtbComision.NewRow
                    dtrFila("ID_COMISION") = dtrComision("ID_COMISION")
                    dtrFila("ID_NUMEROLINEA_DETALLE") = lintNroDetalle
                    dtrFila("PORCENTAJE") = ldblPorcComision
                    dtrFila("MONTO") = ldblMonto
                    dtrFila("IMPUESTO") = ldblMontoImpuesto
                    dtrFila("AFECTO_IMPUESTO") = dtrComision("AFECTO_IMPUESTO")
                    dtrFila("APLICA_PORCENTAJE") = dtrComision("APLICA_PORCENTAJE")
                    dtbComision.Rows.Add(dtrFila)
                Next
            End If
            pdblSubTotal = ldblSubTotalAux
        End If
    End Sub

    Public Sub ReCalcularComisionGeneral2(ByVal pDS_Comision As DataSet, ByVal dtbDetalle As DataTable, ByRef dtbComision As DataTable,
                                         ByVal lstrTipoOperacionMov As String, ByRef pdblSubTotal As Double,
                                         ByRef pdblMontoComisionConImpuesto As Double, ByRef pdblMontoComisionSinImpuesto As Double,
                                         ByRef pdblTotal As Double, ByRef pintIdCuenta As Integer)
        Dim dtrComision As DataRow
        Dim dtrDetalle2 As DataRow
        Dim ldblMonto As Double
        Dim ldblMontoOperacion As Double
        Dim ldblMontoOperacionDetalle As Double
        Dim ldblFactor As Double
        Dim lintNroDetalle As Integer
        Dim lblnAplicaPorcentaje As Boolean
        Dim lblnAfectoImpuesto As Boolean
        Dim ldblPorcComision As Double
        Dim ldblMontoComisionConImpuesto As Double = 0
        Dim ldblMontoComisionSinImpuesto As Double = 0
        Dim ldblMontoImpuesto As Double
        Dim id_grupo_comisiones As Integer
        Dim ldblTipoCambio As Double

        Dim dblParidad As Double
        Dim strOperacion As String = ""
        Dim strDescError As String = ""
        Dim strFechaConsulta As String = ""
        Dim clsGeneral As New ClsGeneral

        Dim objMoneda As ClsMoneda = New ClsMoneda
        Dim DS_Moneda As DataSet
        Dim strRetorno As String
        Dim lredondeo As Integer

        dtbComision.Clear()
        'german
        '(Not DS_EsquemaComision Is Nothing AndAlso DS_EsquemaComision.Tables.Count > 0) 
        If dtbDetalle.Rows.Count > 0 And (Not pDS_Comision Is Nothing AndAlso pDS_Comision.Tables.Count > 0) Then
            If (dtbDetalle.Rows(0).Item("COD_MONEDA_PAGO") = pDS_Comision.Tables(0).Rows(0).Item("COD_MONEDA")) Then
                ldblTipoCambio = 1
            Else
                clsGeneral.TraerTipoCambio(glngNegocioOperacion,
                        pintIdCuenta,
                        dtbDetalle.Rows(0).Item("COD_MONEDA"),
                        pDS_Comision.Tables(0).Rows(0).Item("COD_MONEDA"),
                        (Mid(dtbDetalle.Rows(0).Item("FECHA_LIQUIDACION"), 7, 4) & Mid(dtbDetalle.Rows(0).Item("FECHA_LIQUIDACION"), 4, 2) & Mid(dtbDetalle.Rows(0).Item("FECHA_LIQUIDACION"), 1, 2)),
                        0,
                        dblParidad,
                        strOperacion,
                        strDescError)
                If strOperacion = "M" Then
                    ldblTipoCambio = dblParidad
                Else
                    ldblTipoCambio = 1 / dblParidad
                End If
            End If
        End If

        Dim ldblLineasTot As Double
        Dim ldblLineasDet As Double
        Dim ldblMontoTot As Double
        Dim ldblImpuestoTot As Double
        Dim ldblMontoSum As Double
        Dim ldblImpuestoSum As Double
        Dim ldblMontoDif As Double
        Dim ldblImpuestoDif As Double
        Dim ldblPorcentajeIva As Double
        ldblPorcentajeIva = gobjParametro.PorcentajeIva.ValorParametro

        If dtbDetalle.Rows.Count > 0 Then
            ldblMontoOperacion = dtbDetalle.Compute("SUM(MONTO2)", "") * ldblTipoCambio
            If Not pDS_Comision Is Nothing Then
                If pDS_Comision.Tables.Count > 0 Then
                    dtrComision = pDS_Comision.Tables(0).Rows(0)
                    id_grupo_comisiones = dtrComision("ID_GRUPO_COMISIONES")

                    ldblLineasTot = dtbDetalle.Rows.Count
                    ldblLineasDet = 0
                    ldblMontoTot = 0
                    ldblImpuestoTot = 0
                    ldblMontoSum = 0
                    ldblImpuestoSum = 0
                    ldblMontoDif = 0
                    ldblImpuestoDif = 0
                    strRetorno = ""
                    DS_Moneda = objMoneda.Moneda_Ver(dtrComision("COD_MONEDA"), "MONEDA_DECIMALES", "VIG", strRetorno)
                    If strRetorno = "OK" AndAlso Not (DS_Moneda Is Nothing) Then
                        lredondeo = DS_Moneda.Tables(0).Rows(0).Item("MONEDA_DECIMALES")
                    End If

                    For Each dtrDetalle2 In dtbDetalle.Rows
                        lintNroDetalle = dtrDetalle2.Item("ID_NUMEROLINEA_DETALLE")
                        ldblMontoOperacionDetalle = dtrDetalle2.Item("MONTO2") * ldblTipoCambio
                        ldblLineasDet = ldblLineasDet + 1

                        ldblFactor = (ldblMontoOperacionDetalle / ldblMontoOperacion)
                        For lintIndex As Integer = 0 To pDS_Comision.Tables(0).Rows.Count - 1
                            dtrComision = pDS_Comision.Tables(0).Rows(lintIndex)
                            ldblPorcComision = 0
                            ldblMonto = 0
                            ldblMontoImpuesto = 0
                            If Not IsNothing(dtrComision) Then
                                lblnAplicaPorcentaje = dtrComision("APLICA_PORCENTAJE")
                                lblnAfectoImpuesto = dtrComision("AFECTO_IMPUESTO")
                                If Not IsDBNull(dtrComision("PORCENTAJE")) Then
                                    ldblPorcComision = dtrComision("PORCENTAJE")
                                    ldblMonto = CDbl(ldblMontoOperacion) * (ldblPorcComision / 100)
                                    ldblMonto = Math.Round(ldblMonto, lredondeo)
                                    'ldblMonto = Format(ldblMonto * ldblFactor, "0")
                                Else
                                    ldblPorcComision = 0
                                    ldblMonto = dtrComision("MONTO")
                                End If
                                ldblMontoTot = ldblMonto
                                ldblMonto = ldblMonto * ldblFactor
                                ldblMonto = Math.Round(ldblMonto, lredondeo)

                                If dtrComision("AFECTO_IMPUESTO") Then
                                    ldblImpuestoTot = ldblMontoTot * (ldblPorcentajeIva / 100)
                                    ldblImpuestoTot = Math.Round(ldblImpuestoTot, lredondeo)
                                    ldblMontoImpuesto = ldblMonto * (ldblPorcentajeIva / 100)
                                    ldblMontoImpuesto = Math.Round(ldblMontoImpuesto, lredondeo)
                                    ldblMontoComisionConImpuesto = ldblMontoComisionConImpuesto + (ldblMonto + ldblMontoImpuesto)
                                Else
                                    ldblMontoImpuesto = 0
                                    ldblMontoComisionSinImpuesto = ldblMontoComisionSinImpuesto + ldblMonto
                                End If
                                ldblImpuestoSum = ldblImpuestoSum + ldblMontoImpuesto
                                ldblMontoSum = ldblMontoSum + ldblMonto

                            End If

                            '+ Si se trata de agregar informaci�n
                            Dim dtrFila As DataRow


                            If ldblLineasTot = ldblLineasDet Then
                                'Calcula ajuste 
                                ldblMontoDif = ldblMontoTot - ldblMontoSum
                                ldblImpuestoDif = ldblImpuestoTot - ldblImpuestoSum
                                ldblMonto = ldblMonto + ldblMontoDif
                                ldblMontoImpuesto = ldblMontoImpuesto + ldblImpuestoDif
                                ldblMontoComisionConImpuesto = ldblMontoComisionConImpuesto + ldblMontoDif + ldblImpuestoDif
                            End If

                            dtrFila = dtbComision.NewRow
                            dtrFila("ID_COMISION") = dtrComision("ID_COMISION")
                            dtrFila("ID_NUMEROLINEA_DETALLE") = lintNroDetalle
                            dtrFila("PORCENTAJE") = ldblPorcComision
                            dtrFila("MONTO") = ldblMonto
                            dtrFila("IMPUESTO") = ldblMontoImpuesto
                            dtrFila("AFECTO_IMPUESTO") = dtrComision("AFECTO_IMPUESTO")
                            dtrFila("APLICA_PORCENTAJE") = dtrComision("APLICA_PORCENTAJE")
                            dtbComision.Rows.Add(dtrFila)
                        Next
                    Next
                End If

                pdblMontoComisionConImpuesto = ldblMontoComisionConImpuesto
                pdblMontoComisionSinImpuesto = ldblMontoComisionSinImpuesto
            Else
                pdblMontoComisionConImpuesto = 0
                pdblMontoComisionSinImpuesto = 0
            End If
            pdblSubTotal = ldblMontoOperacion
            If lstrTipoOperacionMov = "I" Then
                pdblTotal = pdblSubTotal + (pdblMontoComisionConImpuesto + pdblMontoComisionSinImpuesto)
            Else
                pdblTotal = pdblSubTotal - (pdblMontoComisionConImpuesto + pdblMontoComisionSinImpuesto)
            End If
        End If
    End Sub


    Public Sub CargaComboAdministradorCuenta(ByRef meComboBox As C1.Win.C1List.C1Combo, Optional ByVal strTransaccion As String = "", Optional ByVal idAdministradorCuenta As Long = 0, Optional ByVal strEstadoAdministradorCuenta As String = "", Optional ByVal bIfBlank As Boolean = True)
        Dim lobjAdministradorCuenta As ClsAdministradorCuenta = New ClsAdministradorCuenta
        Dim DS_General As New DataSet
        Dim TablaTemp As DataRow
        Dim ltxtColumnas As String
        Dim ltxtResultadoTransaccion As String = Nothing

        Try
            ltxtColumnas = "ABR_ADMINISTRADOR_CUENTA, ID_ADMINISTRADOR_CUENTA, DSC_ADMINISTRADOR_CUENTA, EST_ADMINISTRADOR_CUENTA"
            '...Limpia Combo
            meComboBox.ClearItems()
            meComboBox.Text = ""
            '...Inicializa Data Set Combo
            DS_General = Nothing
            DS_General = lobjAdministradorCuenta.AdministradorCuenta_Ver(0, "VIG", ltxtColumnas, ltxtResultadoTransaccion)

            If ltxtResultadoTransaccion.ToUpper = "OK" Then
                '+ Si se indico que acepta blanco se incluye un registro en blanco en el combo
                If bIfBlank Then
                    TablaTemp = DS_General.Tables("AdministradorCuenta").NewRow()
                    TablaTemp("ABR_ADMINISTRADOR_CUENTA") = ""
                    TablaTemp("ID_ADMINISTRADOR_CUENTA") = DBNull.Value
                    TablaTemp("DSC_ADMINISTRADOR_CUENTA") = ""
                    TablaTemp("EST_ADMINISTRADOR_CUENTA") = ""
                    DS_General.Tables("AdministradorCuenta").Rows.Add(TablaTemp)
                    DS_General.AcceptChanges()
                End If

                '...Asigna Data Source
                meComboBox.AddItem(";")
                meComboBox.DataSource = DS_General.Tables("AdministradorCuenta")
                meComboBox.Sort(0, C1.Win.C1List.SortDirEnum.ASC)
                '...Ordena y visibilidad de columnas del combo
                meComboBox.Columns(0).DataField = "ABR_ADMINISTRADOR_CUENTA"
                meComboBox.Splits(0).DisplayColumns(0).Visible = True

                meComboBox.Columns(1).DataField = "ID_ADMINISTRADOR_CUENTA"
                meComboBox.Splits(0).DisplayColumns(1).Visible = False

                meComboBox.Columns(2).DataField = "DSC_ADMINISTRADOR_CUENTA"
                meComboBox.Splits(0).DisplayColumns(2).Visible = False

                meComboBox.Columns(3).DataField = "EST_ADMINISTRADOR_CUENTA"
                meComboBox.Splits(0).DisplayColumns(3).Visible = False
            End If
        Catch ex As Exception
            ltxtResultadoTransaccion = ex.Message
        Finally
            If ltxtResultadoTransaccion <> "OK" Then
                MsgBox("Error al cargar combo administrador cuenta : " & ltxtResultadoTransaccion, MsgBoxStyle.Information, gtxtNombreSistema & strTransaccion)
            End If
        End Try
    End Sub

    Public Function gFunValidaObligatoriedad(
                    ByVal strMemsajeError As String,
                    ByVal strValDirectaNoControl As String,
                    ByVal objetoControl As Object,
                    ByRef StrMensajeResumen As String,
                    ByRef SeteaFoco As Boolean,
                    ByVal larr_DatosObligatorios(,) As String,
                    Optional ByVal EsObligatorioCodigo As Boolean = False,
                    Optional ByRef DockingTabControl As C1.Win.C1Command.C1DockingTab = Nothing,
                    Optional ByVal DockingTabSelectedIndex As Integer = -1,
                    Optional ByVal MuestraError As Boolean = True) As Boolean
        Dim lblnEsObligatorio As Boolean = False
        '...Valida que un control Tenga o NO Tenga un valor, si es obligatorio y no tiene valor se posiciona el "Focus" en el control.
        '   Si el control est� en un control "DockingTab", cambia el SelectedIndex del tabulador, pasado
        '   a trav�s del par�metro "DockingTabSelectedIndex"
        Try
            '+ Si el campo no es obligatorio por c�digo se busca en la DB
            If Not EsObligatorioCodigo Then
                '+ Si se indic� una validaci�n obligatoria fija; es decir, no por nombre de control
                If strValDirectaNoControl <> "" Then
                    If larr_DatosObligatorios Is Nothing Or larr_DatosObligatorios(0, 0) Is Nothing Then
                        lblnEsObligatorio = False
                    Else
                        lblnEsObligatorio = gFunExistArrOrd(strValDirectaNoControl, larr_DatosObligatorios)
                    End If

                    If lblnEsObligatorio Then
                        If MuestraError Then
                            MsgBox(strMemsajeError, vbCritical, My.Application.Info.Title)
                        Else
                            StrMensajeResumen = StrMensajeResumen & strMemsajeError & vbCr
                        End If
                        gFunValidaObligatoriedad = lblnEsObligatorio
                        Exit Function
                    End If
                ElseIf Not objetoControl Is Nothing Then
                    If larr_DatosObligatorios Is Nothing Or larr_DatosObligatorios(0, 0) Is Nothing Then
                        lblnEsObligatorio = False
                    Else
                        lblnEsObligatorio = gFunExistArrOrd(objetoControl.name, larr_DatosObligatorios)
                    End If
                End If
            Else
                lblnEsObligatorio = True
                If strValDirectaNoControl <> "" Then
                    If MuestraError Then
                        MsgBox(strMemsajeError, vbCritical, My.Application.Info.Title)
                    Else
                        StrMensajeResumen = StrMensajeResumen & strMemsajeError & vbCr
                    End If
                    gFunValidaObligatoriedad = lblnEsObligatorio
                    Exit Function
                End If
            End If

            If lblnEsObligatorio Then
                '...TextBox, C1.Win.C1Input.C1TextBox y C1.Win.C1Input.C1DateEdit
                If (TypeOf objetoControl Is TextBox) Or
                   (TypeOf objetoControl Is C1.Win.C1Input.C1TextBox) Or
                   (TypeOf objetoControl Is C1.Win.C1Input.C1DateEdit) Then
                    If (objetoControl.Text.Trim = "" And lblnEsObligatorio) Or
                       (objetoControl.text.ToString.Replace("0", "").Trim = objetoControl.nulltext.ToString.Replace("0", "").Trim And lblnEsObligatorio And objetoControl.numericinput) Or
                       (objetoControl.value.ToString.Replace("0", "").Trim = objetoControl.nulltext.ToString.Replace("0", "").Trim And lblnEsObligatorio And objetoControl.numericinput) Or
                       (objetoControl.Text.Trim <> "" And Not lblnEsObligatorio) Then
                        If MuestraError Then
                            MsgBox(strMemsajeError, vbCritical, My.Application.Info.Title)
                        Else
                            StrMensajeResumen = StrMensajeResumen & strMemsajeError & vbCr
                        End If
                        If SeteaFoco Then
                            If (Not DockingTabSelectedIndex = -1) Then DockingTabControl.SelectedIndex = DockingTabSelectedIndex
                            objetoControl.Focus()
                            SeteaFoco = False
                        End If
                        gFunValidaObligatoriedad = lblnEsObligatorio
                        Exit Function
                    End If
                Else
                    '...C1.Win.C1List.C1Combo
                    If (TypeOf objetoControl Is C1.Win.C1List.C1Combo) Then
                        If (objetoControl.Text.Trim = "" And lblnEsObligatorio) Or
                           (objetoControl.Text.Trim <> "" And Not lblnEsObligatorio) Then
                            If MuestraError Then
                                MsgBox(strMemsajeError, vbCritical, My.Application.Info.Title)
                            Else
                                StrMensajeResumen = StrMensajeResumen & strMemsajeError & vbCr
                            End If
                            If SeteaFoco Then
                                If (Not DockingTabSelectedIndex = -1) Then DockingTabControl.SelectedIndex = DockingTabSelectedIndex
                                objetoControl.Focus()
                                SeteaFoco = False
                            End If
                            gFunValidaObligatoriedad = lblnEsObligatorio
                            Exit Function
                        End If
                    End If
                End If
            End If
            gFunValidaObligatoriedad = lblnEsObligatorio
        Catch ex As Exception
            MsgBox("Error de validaci�n obligatoria:" & vbCr & objetoControl.name, vbCritical, My.Application.Info.Title & " : GFun_ValidaValorControl")
            gFunValidaObligatoriedad = False
        End Try
    End Function

    Public Sub RecuperaDatosObligatorios(ByVal lint_NumeroDePantalla As Integer, ByRef larr_DatosObligatorios(,) As String)
        Dim objAccesoDeControles As ClsGeneral = New ClsGeneral
        Try
            If Not (objAccesoDeControles.TraerDatosObligatorios(lint_NumeroDePantalla, larr_DatosObligatorios)) Then
                Exit Sub
            End If
        Catch ex As Exception
            larr_DatosObligatorios = Nothing
            objAccesoDeControles = Nothing
        End Try
    End Sub

    Public Function gFunExistArrOrd(ByVal strValor As String,
                                    ByVal Arreglo(,) As String) As Boolean
        Dim lblnExiste As Boolean = False
        ' --+ validacion valida que traiga al menos un permiso
        If Arreglo.Length < 1 Then
            gFunExistArrOrd = lblnExiste
            Exit Function
        End If

        For lintFor As Integer = 0 To Arreglo.GetLength(0) - 1
            If strValor.ToUpper.Trim = Arreglo(lintFor, 0).ToUpper.Trim Then
                lblnExiste = True
                Exit For
            End If
            '+ Si el valor del arreglo es mayor que el que se esta buscando (pasado como par�metro) se sale del ciclo ya que el arreglo est� ordenado alfab�ticamente
            If Arreglo(lintFor, 0).ToUpper.Trim > strValor.ToUpper.Trim Then
                Exit For
            End If
        Next
        gFunExistArrOrd = lblnExiste
    End Function

    Public Sub CargaComboPaises(ByRef meComboBox As C1.Win.C1List.C1Combo, Optional ByVal strTransaccion As String = "", Optional ByVal strCodigoPais As String = "", Optional ByVal bIfBlank As Boolean = True)
        Dim lobjPais As ClsPais = New ClsPais
        Dim DS_General As New DataSet
        Dim DataTemp As DataRow
        Dim ltxtColumnas As String
        Dim ltxtResultadoTransaccion As String = Nothing

        Try
            ltxtColumnas = "CODIGO_PAIS,DESCRIPCION_PAIS"
            '...Limpia Combo
            meComboBox.ClearItems()
            meComboBox.Text = ""
            '...Inicializa Data Set Combo
            DS_General = Nothing
            DS_General = lobjPais.Pais_Ver(strCodigoPais, ltxtColumnas, ltxtResultadoTransaccion)

            If ltxtResultadoTransaccion.ToUpper = "OK" Then
                '+ Si se indico que acepta blanco se incluye un registro en blanco en el combo
                If bIfBlank Then
                    DataTemp = DS_General.Tables("PAIS").NewRow()
                    DataTemp("DESCRIPCION_PAIS") = ""
                    DataTemp("CODIGO_PAIS") = ""
                    DS_General.Tables("PAIS").Rows.Add(DataTemp)
                    DS_General.AcceptChanges()
                End If

                '...Asigna Data Source
                meComboBox.AddItem(";")
                meComboBox.DataSource = DS_General.Tables("PAIS")
                meComboBox.Sort(0, C1.Win.C1List.SortDirEnum.ASC)
                '...Ordena y visibilidad de columnas del combo
                meComboBox.Columns(0).DataField = "DESCRIPCION_PAIS"
                meComboBox.Splits(0).DisplayColumns(0).Visible = True

                meComboBox.Columns(1).DataField = "CODIGO_PAIS"
                meComboBox.Splits(0).DisplayColumns(1).Visible = False
            End If
        Catch ex As Exception
            ltxtResultadoTransaccion = ex.Message
        Finally
            If ltxtResultadoTransaccion <> "OK" Then
                MsgBox("Error al cargar combo pa�ses : " & ltxtResultadoTransaccion, MsgBoxStyle.Information, gtxtNombreSistema & strTransaccion)
            End If
        End Try
    End Sub

    Public Function gSubstrSoloNumerosLetras(ByVal pstrCadena As String) As String
        Dim strValor As String = ""
        Dim strCaracteres = " ABCDEFGHIJKLMN�OPQRSTUVWXYZ1234567890"
        Dim strCaracter As String

        Try
            For lIndex = 1 To pstrCadena.Length
                strCaracter = Mid(pstrCadena, lIndex, 1)
                If InStr(strCaracteres, strCaracter.ToUpper) > 0 Then
                    strValor = strValor & strCaracter
                End If
            Next
        Catch ex As Exception
            MsgBox("Error al obtener la cadena de s�lo numeros y letras.", vbCritical, My.Application.Info.Title & " : gSubstrSoloNumerosLetras")
        End Try
        gSubstrSoloNumerosLetras = strValor
    End Function

    Public Function gGetSKeyUser(Optional ByVal plngUsuario As Long = 0, Optional ByVal pintModo As Integer = 0) As String
        Dim strValor As String = ""
        Dim strFecha As String = Mid(Now.ToString, 7, 4) & Mid(Now, 4, 2) & Mid(Now, 1, 2)
        Dim strHora As String = Mid(String.Format("{0:T}", TimeString), 1, 2) & Mid(String.Format("{0:T}", TimeString), 4, 2) & Mid(String.Format("{0:T}", TimeString), 7, 2)

        'Mid(ldrRegistro("Fecha Proceso"), 4, 2) & Mid(ldrRegistro("Fecha Proceso"), 1, 2) & Mid(String.Format("{0:T}", TimeString), 1, 2) & Mid(String.Format("{0:T}", TimeString), 4, 2)
        Try
            Select Case pintModo
                '+ Skey con formato: <id_usuario>_<yyyymmdd_hhmiss>. Primer valor c�digo de usuario y segundo valor fecha y hora.
                Case 0
                    strValor = plngUsuario.ToString & "_" & strFecha & "_" & strHora
                    '+ Skey con formato: <id_usuario>_<hhmiss>. Primer valor c�digo de usuario y segundo valor s�lo la hora.
                Case 1
                    strValor = plngUsuario.ToString & "_" & strHora
                Case Else

            End Select

            '+ Se rellenan con ceros a la izquierda hasta completar los 25 caracteres.
            strValor = strValor.PadLeft(25, "0")
        Catch ex As Exception
            gFun_ResultadoMsg("1|Error al obtener el sKey.", My.Application.Info.Title)
        End Try
        gGetSKeyUser = strValor
    End Function

    Public Function getValorFormatoXLS(ByVal pstrFormato As String,
                                       ByVal pTipoFormato As Est_Formato_XLS) As String
        Dim arrValores() As String
        Dim lstrValor As String

        If Mid(pstrFormato, 1, 1) = "|" Then
            pstrFormato = Mid(pstrFormato, 2, Len(pstrFormato))
        End If

        If Mid(pstrFormato, Len(pstrFormato), 1) = "|" Then
            pstrFormato = Mid(pstrFormato, 1, Len(pstrFormato) - 1)
        End If

        arrValores = pstrFormato.Split("|")

        lstrValor = arrValores(pTipoFormato)
        If Not IsNumeric(lstrValor) Then
            Select Case lstrValor
                Case "T"
                    lstrValor = "True"
                Case "F"
                    lstrValor = "False"
                Case Else
                    lstrValor = ""
            End Select
        End If
        getValorFormatoXLS = lstrValor
    End Function

    Public Function gEntregaLetraXLS(ByVal pNumero As Long) As String
        Dim lstrColLetrasCE As String = "A B C D E F G H I J K L M N O P Q R S T U V W X Y Z"
        Dim lstrColLetrasSE As String = lstrColLetrasCE.Replace(" ", "")
        Dim lstrColLetras() As String = lstrColLetrasCE.Split(" ")
        Dim llngRes As Long = pNumero \ Len(lstrColLetrasSE)
        Dim llngMod As Long = (pNumero Mod Len(lstrColLetrasSE))

        gEntregaLetraXLS = ""
        If llngRes = 0 Then
            gEntregaLetraXLS = lstrColLetras(llngMod - 1)
        Else
            If llngMod = 0 Then
                If llngRes > 1 Then
                    gEntregaLetraXLS = lstrColLetras(llngRes - 2)
                End If
                gEntregaLetraXLS = gEntregaLetraXLS & lstrColLetras(Len(lstrColLetrasSE) - 1)
            Else
                gEntregaLetraXLS = lstrColLetras(llngRes - 1)
                gEntregaLetraXLS = gEntregaLetraXLS & lstrColLetras(llngMod - 1)
            End If
        End If
    End Function

    Public Function getValorFormatoArr(ByVal pstrFormato As String,
                                       ByVal pTipoFormato As Est_Formato_XLS,
                                       Optional ByVal pSep As String = "|") As String
        Dim arrValores() As String
        Dim lstrValor As String

        If Mid(pstrFormato, 1, 1) = pSep Then
            pstrFormato = Mid(pstrFormato, 2, Len(pstrFormato))
        End If

        If Mid(pstrFormato, Len(pstrFormato), 1) = pSep Then
            pstrFormato = Mid(pstrFormato, 1, Len(pstrFormato) - 1)
        End If

        arrValores = pstrFormato.Split(pSep)

        lstrValor = arrValores(pTipoFormato)
        If Not IsNumeric(lstrValor) Then
            Select Case lstrValor
                Case "T"
                    lstrValor = "True"
                Case "F"
                    lstrValor = "False"
                Case Else
                    lstrValor = ""
            End Select
        End If
        getValorFormatoArr = lstrValor
    End Function

    Public Function getNumberFormat(ByVal pLargo As Integer,
                                    ByVal pDecimales As Integer,
                                    Optional ByVal pFormatSimbol As Boolean = True) As String
        Dim lstrFormat As String = ""
        Dim lintParteEntera As Integer = 0
        '"###,###,###,###,##0.0000"
        If pLargo > 0 Then
            If Not pFormatSimbol Then
                lstrFormat = StrDup(pLargo, "#")
            Else
                '+ Nota: La parte decimal est� contenida en el Largo
                lintParteEntera = pLargo - pDecimales

                For lintPos = 1 To lintParteEntera
                    lstrFormat = "#" & lstrFormat
                    If lintPos Mod 3 = 0 Then
                        lstrFormat = "," & lstrFormat
                    End If
                Next

                If Mid(lstrFormat, 1, 1) = "," Then
                    lstrFormat = Mid(lstrFormat, 2, Len(lstrFormat))
                End If

                lstrFormat = Mid(lstrFormat, 1, Len(lstrFormat) - 1) & "0"

                If pDecimales > 0 Then
                    lstrFormat = lstrFormat & "." & StrDup(pDecimales, "0")
                End If
            End If
        End If
        getNumberFormat = lstrFormat
    End Function

    'Objetivo: Crear campo para un DataTable pasado como par�metro.
    Public Sub CrearCampoDataTableGeneral(ByRef dtbTable As DataTable, ByVal strNombreCampo As String, ByVal strTipoCampo As System.Type)
        Dim dtcColumna As DataColumn = New DataColumn(strNombreCampo)
        dtcColumna.DataType = strTipoCampo
        dtcColumna.AllowDBNull = True
        dtbTable.Columns.Add(dtcColumna)
    End Sub

    'Objetivo: Crear un DataTable pasado como par�metro de acuerdo a una cadena que contedr� los campos a crear en formato <NumbreCampo><,><Tipo><|>. Con separador de l�nea el "|".
    Public Sub CrearDataTableGeneral(ByRef pDataTable As DataTable, ByVal pstrCampos As String, Optional ByVal pSepElemento As String = ",", Optional ByVal pSepLinea As String = "|")
        Dim lArrCampos() As String
        Dim lArrElementos() As String
        Try
            '+ Si viene el separador de l�nea al principio se elimina porque no debe estar
            If Mid(pstrCampos, 1, 1) = pSepLinea Then
                pstrCampos = Mid(pstrCampos, 2, Len(pstrCampos))
            End If

            '+ Si viene el separador de l�nea al final se elimina porque no debe estar
            If Mid(pstrCampos, Len(pstrCampos), 1) = pSepLinea Then
                pstrCampos = Mid(pstrCampos, 1, Len(pstrCampos) - 1)
            End If

            lArrCampos = pstrCampos.Split(pSepLinea)

            pDataTable.Columns.Clear()
            For lintIndice = 0 To lArrCampos.Length - 1
                lArrElementos = lArrCampos(lintIndice).Split(pSepElemento)
                Select Case lArrElementos(1)
                    Case 0
                        '+ Num�rico
                        CrearCampoDataTableGeneral(pDataTable, lArrElementos(0), System.Type.GetType("System.Double"))
                    Case 1
                        '+ String
                        CrearCampoDataTableGeneral(pDataTable, lArrElementos(0), System.Type.GetType("System.String"))
                    Case 2
                        '+ Date
                        CrearCampoDataTableGeneral(pDataTable, lArrElementos(0), System.Type.GetType("System.String"))
                    Case Else
                        '+ String
                        CrearCampoDataTableGeneral(pDataTable, lArrElementos(0), System.Type.GetType("System.String"))
                End Select
            Next
        Catch ex As Exception
            gFun_ResultadoMsg("1|" & ex.Message, "CrearDataTableGeneral")
        End Try
    End Sub

    Public Function AgregarEspaciosIntermedios(ByVal pCadena As String) As String
        Dim lstrCadena As String = ""

        For lIndex = 0 To Len(pCadena) - 1
            lstrCadena = lstrCadena + pCadena(lIndex).ToString + " "
        Next
        AgregarEspaciosIntermedios = lstrCadena
    End Function

    Public Function getNombreCampoValido(ByVal pDataRow As DataRow, ByVal pNombreCampo As String) As String
        Dim lstrCampoIn As String = pNombreCampo & "_IN"
        getNombreCampoValido = ""
        Try
            If Not pDataRow(lstrCampoIn) Is Nothing Then
                getNombreCampoValido = lstrCampoIn
            End If
        Catch ex As Exception
            Try
                If Not pDataRow(pNombreCampo) Is Nothing Then
                    getNombreCampoValido = pNombreCampo
                End If
            Catch ex1 As Exception
                getNombreCampoValido = ""
            End Try
        End Try
    End Function

#End Region

#Region "Funciones Juan Mazo"

    Public Sub gSubCargaComboEstado(
                ByRef objetoControl As C1.Win.C1List.C1Combo)
        Try
            objetoControl.ComboStyle = ComboStyleEnum.DropdownList
            objetoControl.DataMode = DataModeEnum.AddItem
            objetoControl.ClearItems()
            objetoControl.AddItem("VIGENTE;VIG")
            objetoControl.AddItem("ELIMINADO;ELI")
            objetoControl.Splits(0).DisplayColumns(0).Visible = True
            objetoControl.Splits(0).DisplayColumns(1).Visible = False
            objetoControl.SelectedIndex = -1
        Catch ex As Exception
        End Try
    End Sub

    Public Sub gSubCargaCombo_FOC(ByRef objetoControl As C1.Win.C1List.C1Combo)
        Try
            objetoControl.ComboStyle = ComboStyleEnum.DropdownList
            objetoControl.DataMode = DataModeEnum.AddItem
            objetoControl.ClearItems()
            objetoControl.AddItem("TASA;TASA")
            objetoControl.AddItem("FIFO;FIFO")
            objetoControl.AddItem("HPR;HPR")
            objetoControl.Splits(0).DisplayColumns(0).Visible = True
            objetoControl.Splits(0).DisplayColumns(1).Visible = False
            objetoControl.SelectedIndex = -1
        Catch ex As Exception
        End Try
    End Sub

    Public Sub CargaMeses(ByRef objetoControl As C1.Win.C1List.C1Combo)
        Try
            objetoControl.ComboStyle = ComboStyleEnum.DropdownList
            objetoControl.DataMode = DataModeEnum.AddItem
            objetoControl.ClearItems()
            objetoControl.AddItem("ENERO;01")
            objetoControl.AddItem("FEBRERO;02")
            objetoControl.AddItem("MARZO;03")
            objetoControl.AddItem("ABRIL;04")
            objetoControl.AddItem("MAYO;05")
            objetoControl.AddItem("JUNIO;06")
            objetoControl.AddItem("JULIO;07")
            objetoControl.AddItem("AGOSTO;08")
            objetoControl.AddItem("SEPTIEMBRE;09")
            objetoControl.AddItem("OCTUBRE;10")
            objetoControl.AddItem("NOVIEMBRE;11")
            objetoControl.AddItem("DICIEMBRE;12")
            objetoControl.Splits(0).DisplayColumns(0).Visible = True
            objetoControl.Splits(0).DisplayColumns(1).Visible = False
            objetoControl.SelectedIndex = -1
        Catch ex As Exception
        End Try
    End Sub

    Public Sub gSubCargaComboTipoBusqueda(
        ByRef objetoControl As C1.Win.C1List.C1Combo)
        Try
            objetoControl.ComboStyle = ComboStyleEnum.DropdownList
            objetoControl.DataMode = DataModeEnum.AddItem
            objetoControl.ClearItems()
            objetoControl.AddItem("CUENTAS")
            objetoControl.AddItem("CLIENTES")
            objetoControl.AddItem("GRUPOS")
            objetoControl.Splits(0).DisplayColumns(0).Visible = True
            'objetoControl.Splits(0).DisplayColumns(1).Visible = False
            objetoControl.SelectedIndex = -1
        Catch ex As Exception
        End Try
    End Sub

    ''' <summary>
    ''' Metodo creado especialmente para cargar estados para security
    ''' </summary>
    ''' <param name="objetoControl"></param>
    ''' <param name="strTipoEstado"></param>
    ''' <remarks></remarks>

    Public Sub Carga_Combos_estado_Security(ByRef objetoControl As C1.Win.C1List.C1Combo, ByVal strTipoEstado As String, Optional ByVal bIsBlank As Boolean = False)
        Dim lobjClaseEstados As ClsEstados = New ClsEstados
        Dim DS_Estados As DataSet = New DataSet
        Dim strRetorno As String = ""
        Dim EstadoTemp As DataRow

        Try
            objetoControl.ClearItems()
            DS_Estados = lobjClaseEstados.Estados_Ver("", strTipoEstado, "", "DSC_ESTADO,COD_ESTADO", strRetorno)
            If DS_Estados.Tables(0).Rows.Count > 0 Then
                '+ Si se indico que acepta blanco se incluye un registro en blanco en el combo
                If bIsBlank Then
                    EstadoTemp = DS_Estados.Tables("Estados").NewRow()
                    EstadoTemp("DSC_ESTADO") = ""
                    EstadoTemp("COD_ESTADO") = ""
                    DS_Estados.Tables("Estados").Rows.Add(EstadoTemp)
                    DS_Estados.AcceptChanges()
                End If

                objetoControl.ComboStyle = ComboStyleEnum.DropdownList
                objetoControl.DataMode = DataModeEnum.AddItem
                For i As Integer = 0 To DS_Estados.Tables(0).Rows.Count - 1
                    If DS_Estados.Tables(0).Rows(i).Item("COD_ESTADO") = "P" Or DS_Estados.Tables(0).Rows(i).Item("COD_ESTADO") = "A" Or DS_Estados.Tables(0).Rows(i).Item("COD_ESTADO") = "" Then
                        Dim lstrDescripcion As String = DS_Estados.Tables(0).Rows(i).Item("DSC_ESTADO")
                        Dim lstrCod As String = DS_Estados.Tables(0).Rows(i).Item("COD_ESTADO")
                        objetoControl.AddItem(lstrDescripcion & ";" & lstrCod)
                    End If
                Next i

                objetoControl.Sort(0, C1.Win.C1List.SortDirEnum.ASC)
                objetoControl.Splits(0).DisplayColumns(0).Visible = True
                objetoControl.Splits(0).DisplayColumns(1).Visible = False
                objetoControl.SelectedIndex = -1
            End If
        Catch ex As Exception

        End Try
    End Sub

    Public Sub Carga_Combos_estado(ByRef objetoControl As C1.Win.C1List.C1Combo, ByVal strTipoEstado As String, Optional ByVal bIsBlank As Boolean = True)
        Dim lobjClaseEstados As ClsEstados = New ClsEstados
        Dim DS_Estados As DataSet = New DataSet
        Dim strRetorno As String = ""
        Try
            objetoControl.ClearItems()
            DS_Estados = lobjClaseEstados.Estados_Ver("", strTipoEstado, "", "DSC_ESTADO,COD_ESTADO", strRetorno)
            If DS_Estados.Tables(0).Rows.Count > 0 Then
                objetoControl.ComboStyle = ComboStyleEnum.DropdownList
                objetoControl.DataMode = DataModeEnum.AddItem
                If bIsBlank Then
                    objetoControl.AddItem("" & ";" & "")
                End If
                For i As Integer = 0 To DS_Estados.Tables(0).Rows.Count - 1
                    Dim lstrDescripcion As String = DS_Estados.Tables(0).Rows(i).Item("DSC_ESTADO")
                    Dim lstrCod As String = DS_Estados.Tables(0).Rows(i).Item("COD_ESTADO")
                    objetoControl.AddItem(lstrDescripcion & ";" & lstrCod)
                Next i

                objetoControl.Splits(0).DisplayColumns(0).Visible = True
                objetoControl.Splits(0).DisplayColumns(1).Visible = False
                objetoControl.SelectedIndex = -1
            End If
        Catch ex As Exception

        End Try
    End Sub

    Public Sub Carga_Combo_Ingresos(ByRef objetoControl As C1.Win.C1List.C1Combo, Optional ByVal bIsBlank As Boolean = False)
        Dim lobjClaseIngresos As ClsGeneral = New ClsGeneral
        Dim DS_General As DataSet = New DataSet
        Dim strRetorno As String = ""
        Try
            objetoControl.ClearItems()
            DS_General = lobjClaseIngresos.Ingresos_Ver("DESCRIPCION,FORMULARIO", strRetorno)
            If DS_General.Tables(0).Rows.Count > 0 Then
                objetoControl.ComboStyle = ComboStyleEnum.DropdownList
                objetoControl.DataMode = DataModeEnum.AddItem
                If bIsBlank Then
                    objetoControl.AddItem("" & ";" & "")
                End If
                For i As Integer = 0 To DS_General.Tables(0).Rows.Count - 1
                    Dim lstrDescripcion As String = DS_General.Tables(0).Rows(i).Item("DESCRIPCION")
                    Dim lstrCod As String = DS_General.Tables(0).Rows(i).Item("FORMULARIO")
                    objetoControl.AddItem(lstrDescripcion & ";" & lstrCod)
                Next i

                objetoControl.Splits(0).DisplayColumns(0).Visible = True
                objetoControl.Splits(0).DisplayColumns(1).Visible = False
                objetoControl.SelectedIndex = -1
            End If
        Catch ex As Exception

        End Try
    End Sub

    Public Sub gSubCargaComboTipoCheckList(
            ByRef objetoControl As C1.Win.C1List.C1Combo,
            Optional ByVal bIfBlank As Boolean = False)
        Try
            objetoControl.ComboStyle = ComboStyleEnum.DropdownList
            objetoControl.DataMode = DataModeEnum.AddItem
            objetoControl.ClearItems()
            If bIfBlank Then
                objetoControl.AddItem(";0")
            End If
            objetoControl.AddItem("DOCUMENTACI�N;1")
            objetoControl.AddItem("TIPO DE APORTE;2")
            objetoControl.Sort(0, C1.Win.C1List.SortDirEnum.ASC)
            objetoControl.Splits(0).DisplayColumns(0).Visible = True
            objetoControl.Splits(0).DisplayColumns(1).Visible = False
            objetoControl.SelectedIndex = -1
        Catch ex As Exception

        End Try
    End Sub

    Public Sub gSubCargaComboEstadoSolicitud(
        ByRef objetoControl As C1.Win.C1List.C1Combo)
        Try
            objetoControl.ComboStyle = ComboStyleEnum.DropdownList
            objetoControl.DataMode = DataModeEnum.AddItem
            objetoControl.ClearItems()
            objetoControl.AddItem("PENDIENTE;PEN")
            objetoControl.AddItem("CONFIRMADA;CON")
            objetoControl.AddItem("RECHAZADA;RCH")
            objetoControl.Splits(0).DisplayColumns(0).Visible = True
            objetoControl.Splits(0).DisplayColumns(1).Visible = False
            objetoControl.SelectedIndex = -1
        Catch ex As Exception

        End Try
    End Sub

    Public Sub gSubTraspasoGrillas(ByRef TDG_No_Inc As C1.Win.C1TrueDBGrid.C1TrueDBGrid, ByRef TDG_Inc As C1.Win.C1TrueDBGrid.C1TrueDBGrid, ByRef DS1 As DataSet, ByVal accion As String, Optional ByVal Rama As String = "", Optional ByRef DS_Instrumento As DataSet = Nothing, Optional ByVal UPD As String = "")
        Dim intCantidad As Integer
        Select Case accion
            Case "INCLUIR"
                If TDG_No_Inc.RowCount > 0 Then

                    For intCantidad = 0 To TDG_No_Inc.SelectedRows.Count - 1
                        Dim DT_Temp As DataTable = Nothing
                        Dim row As DataRow = DS1.Tables(0).NewRow
                        row.ItemArray = DS1.Tables(1).Rows(TDG_No_Inc.SelectedRows.Item(intCantidad)).ItemArray
                        If Rama = "" Then
                            DS1.Tables(0).Rows.Add(row)
                        Else
                            Dim DATAROW As DataRow() = DS_Instrumento.Tables(0).Select("ID_INSTRUMENTO = " + row.Item(1).ToString)
                            DATAROW(0).BeginEdit()
                            DATAROW(0).Item(2) = CInt(Rama)
                            DATAROW(0).Item(3) = CInt(UPD)
                            DATAROW(0).EndEdit()
                            DS1.Tables(0).Rows.Add(row)
                        End If
                    Next
                    intCantidad = TDG_No_Inc.SelectedRows.Count - 1
                    Dim ArrIndices(intCantidad) As Integer
                    Dim i As Integer
                    For i = 0 To intCantidad
                        ArrIndices(i) = TDG_No_Inc.SelectedRows.Item(i)
                    Next i
                    Array.Sort(ArrIndices)
                    While intCantidad >= 0
                        '+ Borrar de la Lista el traspasado
                        TDG_No_Inc.Delete(ArrIndices(intCantidad))
                        intCantidad = intCantidad - 1
                    End While
                End If
            Case "NO INCLUIR"
                If TDG_Inc.RowCount > 0 Then

                    For intCantidad = 0 To TDG_Inc.SelectedRows.Count - 1
                        Dim DT_Temp As DataTable = Nothing
                        Dim row As DataRow = DS1.Tables(1).NewRow
                        row.ItemArray = DS1.Tables(0).Rows(TDG_Inc.SelectedRows.Item(intCantidad)).ItemArray
                        If Rama = "" Then
                            DS1.Tables(1).Rows.Add(row)
                        Else
                            Dim DATAROW As DataRow() = DS_Instrumento.Tables(0).Select("ID_INSTRUMENTO = " + row.Item(1).ToString)
                            DATAROW(0).BeginEdit()
                            DATAROW(0).Item(2) = CInt(Rama)
                            DATAROW(0).Item(3) = CInt(UPD)
                            DATAROW(0).EndEdit()
                            DS1.Tables(1).Rows.Add(row)
                        End If
                    Next

                    intCantidad = TDG_Inc.SelectedRows.Count - 1
                    Dim ArrIndices(intCantidad) As Integer
                    Dim i As Integer
                    For i = 0 To intCantidad
                        ArrIndices(i) = TDG_Inc.SelectedRows.Item(i)
                    Next i
                    Array.Sort(ArrIndices)
                    While intCantidad >= 0
                        '+ Borrar de la Lista el traspasado
                        TDG_Inc.Delete(ArrIndices(intCantidad))
                        intCantidad = intCantidad - 1
                    End While
                End If
        End Select
    End Sub


    Public Sub CargaComboClase(ByRef meComboBox As C1.Win.C1List.C1Combo, Optional ByVal bIfBlank As Boolean = False)
        Dim lobjClases As ClsClaseInstrumento = New ClsClaseInstrumento
        Dim DS_General As New DataSet
        Dim lstrColumnas As String
        Dim lstrResultadoTransaccion As String = ""
        Dim DatoTemp As DataRow

        lstrColumnas = "DSC_CLASE, COD_CLASE"
        '...Limpia Combo
        meComboBox.ClearItems()
        meComboBox.Text = ""
        '...Inicializa Data Set Combo
        DS_General = Nothing
        DS_General = lobjClases.ClaseInst_Ver("", lstrColumnas, lstrResultadoTransaccion)
        Try
            If lstrResultadoTransaccion.ToUpper <> "OK" Then
                MsgBox("Error al cargar combo clase instrumentos ", MsgBoxStyle.Information, gtxtNombreSistema)
            Else
                If bIfBlank Then
                    DatoTemp = DS_General.Tables("ClaseInstrumento").NewRow()
                    DatoTemp("VALOR_STRING") = ""
                    DS_General.Tables("ClaseInstrumento").Rows.Add(DatoTemp)
                    DS_General.AcceptChanges()
                End If
                '...Asigna Data Source
                meComboBox.DataSource = DS_General.Tables("ClaseInstrumento")
                meComboBox.Sort(0, C1.Win.C1List.SortDirEnum.ASC)
                '...Ordena y visibilidad de columnas del combo
                meComboBox.Columns(0).DataField = "VALOR_STRING"
                meComboBox.Splits(0).DisplayColumns(0).Visible = True
                'meComboBox.Columns(1).DataField = "COD_CLASE"
                'meComboBox.Splits(0).DisplayColumns(1).Visible = False
            End If
        Catch ex As Exception
            MsgBox("Error al cargar combo sub-clase de instrumentos ", MsgBoxStyle.Information, gtxtNombreSistema)
        End Try
    End Sub

    Public Sub Carga_Combo_Normativo(ByRef objetoControl As C1.Win.C1List.C1Combo)
        Dim lobjClaseNormativo As ClsNormativo = New ClsNormativo
        Dim DS_Normativo As DataSet = New DataSet
        Dim strRetorno As String = ""
        Try
            objetoControl.ClearItems()
            DS_Normativo = lobjClaseNormativo.Normativo_Ver(0, "", "", "", strRetorno)
            If DS_Normativo.Tables(0).Rows.Count > 0 Then
                objetoControl.ComboStyle = ComboStyleEnum.DropdownList
                objetoControl.DataMode = DataModeEnum.AddItem
                objetoControl.AddItem("" & ";" & "")
                For i As Integer = 0 To DS_Normativo.Tables(0).Rows.Count - 1
                    Dim lstrDescripcion As String = DS_Normativo.Tables(0).Rows(i).Item("DSC_NORMATIVO")
                    Dim lstrCod As String = DS_Normativo.Tables(0).Rows(i).Item("ID_NORMATIVO")
                    objetoControl.AddItem(lstrDescripcion & ";" & lstrCod)
                Next i

                objetoControl.Splits(0).DisplayColumns(0).Visible = True
                objetoControl.Splits(0).DisplayColumns(1).Visible = False
                objetoControl.SelectedIndex = -1
            End If
        Catch ex As Exception
            MsgBox("Error al cargar combo Normativo", MsgBoxStyle.Exclamation, gtxtNombreSistema)
        End Try
    End Sub

    Public Sub CargaTipoEntidad(ByVal strCodigoTipoEntidad As String, ByRef meComboBox As C1.Win.C1List.C1Combo, Optional ByVal Bln_Blank As Boolean = False)
        Dim lstrColumnas As String = "DSC_TIPO_ENTIDAD,COD_TIPO_ENTIDAD"
        Dim lobjTipoEntidad As ClsTipoEntidad = New ClsTipoEntidad
        Dim lstrResultadoTransaccion As String = ""
        Dim TipoEntidadTemp As DataRow
        Dim DS_TipoEntidad As DataSet = New DataSet
        '...Limpia Combo
        meComboBox.ClearItems()
        meComboBox.Text = ""
        '...Inicializa Data Set Combo
        DS_TipoEntidad = Nothing
        DS_TipoEntidad = lobjTipoEntidad.BuscarTiposEntidades("VIG", lstrColumnas, lstrResultadoTransaccion)
        Try
            If lstrResultadoTransaccion.ToUpper <> "OK" Then
                MsgBox("Error al cargar Combo Tipo : " & strCodigoTipoEntidad, MsgBoxStyle.Information, gtxtNombreSistema & " Mantenedor de Clientes / Cuentas.")
            Else
                If Bln_Blank Then
                    TipoEntidadTemp = DS_TipoEntidad.Tables("TipoEntidad").NewRow()
                    TipoEntidadTemp("DSC_TIPO_ENTIDAD") = ""
                    TipoEntidadTemp("COD_TIPO_ENTIDAD") = ""
                    DS_TipoEntidad.Tables("TipoEntidad").Rows.Add(TipoEntidadTemp)
                    DS_TipoEntidad.AcceptChanges()
                End If

                '...Asigna Data Source
                meComboBox.DataSource = DS_TipoEntidad.Tables("TipoEntidad")
                meComboBox.Sort(0, C1.Win.C1List.SortDirEnum.ASC)
                '...Ordena y visibilidad de columnas del combo
                meComboBox.Columns(0).DataField = "DSC_TIPO_ENTIDAD"
                meComboBox.Splits(0).DisplayColumns(0).Visible = True
                meComboBox.Columns(1).DataField = "COD_TIPO_ENTIDAD"
                meComboBox.Splits(0).DisplayColumns(1).Visible = False

                meComboBox.SelectedIndex = gFunSeteaItemComboBox(meComboBox, "N")
            End If
        Catch ex As Exception
            MsgBox("Error al cargar Combo Tipo : " & strCodigoTipoEntidad, MsgBoxStyle.Information, gtxtNombreSistema & " Mantenedor de Clientes / Cuentas.")
        End Try
    End Sub

    Public Sub Carga_Combo_Reporte(ByRef meComboBox As C1.Win.C1List.C1Combo, ByVal Id_Contraparte As Integer, Optional ByVal Bln_Blank As Boolean = False)
        Dim lobjArchivoM As New ClsArchivo
        Dim DS_Plantilla_Reporte As DataSet = New DataSet
        Dim strRetorno As String = ""
        Dim ltxtColumnas As String = ""
        Dim TipoRepTemp As DataRow

        Try
            ltxtColumnas = "NOMBRE_H, NOMBRE_A, ID_SIS_ARCHIVO_IMPORTA_M"
            meComboBox.ClearItems()
            DS_Plantilla_Reporte = Nothing
            DS_Plantilla_Reporte = lobjArchivoM.Archivo_Importa_M_PorPlantilla("", Id_Contraparte, 0, "MAESTRA", ltxtColumnas, strRetorno = "OK")
            If DS_Plantilla_Reporte.Tables(0).Rows.Count > 0 Then

                If Not DS_Plantilla_Reporte Is Nothing Then
                    If Bln_Blank Then
                        TipoRepTemp = DS_Plantilla_Reporte.Tables("ARCHIVO_IMPORTA_D").NewRow()
                        TipoRepTemp("NOMBRE_H") = ""
                        TipoRepTemp("NOMBRE_A") = ""
                        TipoRepTemp("ID_SIS_ARCHIVO_IMPORTA_M") = ""
                        DS_Plantilla_Reporte.Tables("TipoEntidad").Rows.Add(TipoRepTemp)
                        DS_Plantilla_Reporte.AcceptChanges()
                    End If

                    '...Asigna Data Source
                    meComboBox.DataSource = DS_Plantilla_Reporte.Tables("ARCHIVO_IMPORTA_D")
                    meComboBox.Sort(0, C1.Win.C1List.SortDirEnum.ASC)
                    '...Ordena y visibilidad de columnas del combo

                    meComboBox.Columns(0).DataField = "NOMBRE_H"
                    meComboBox.Splits(0).DisplayColumns(0).Visible = True
                    meComboBox.Columns(1).DataField = "NOMBRE_A"
                    meComboBox.Splits(0).DisplayColumns(1).Visible = False
                    meComboBox.Columns(2).DataField = "ID_SIS_ARCHIVO_IMPORTA_M"
                    meComboBox.Splits(0).DisplayColumns(2).Visible = False
                    'meComboBox.Columns(1).DataField = "Nombre_A"
                    'meComboBox.Splits(0).DisplayColumns(1).Visible = True
                    meComboBox.SelectedIndex = -1
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub

    Public Function gFun_BuscarDiasRetencion(Optional ByVal strCodPais As String = "CHI", Optional ByVal strFechaDesde As String = "", Optional ByVal strFechaHasta As String = "") As Integer
        Dim lstrResultado As String = ""
        Dim objGeneral As ClsGeneral = New ClsGeneral

        Return objGeneral.BuscarDiasRetencion(strCodPais, strFechaDesde, strFechaHasta, lstrResultado)
    End Function

    Public Sub ObtenerComisionGeneralDP(ByRef DS_EsquemaComision As DataSet, ByVal lstrClase As String, ByVal lstrSubClase As String, ByVal lstrTipoOperacionMov As String,
                                      ByVal lstrIdCuenta As String, ByVal dtbDetalle As DataTable, ByVal lstrCodMoneda As String, ByVal dtbComision As DataTable,
                                      ByRef ldblSubTotal As Double, ByRef ldblMontoComisionConImpuesto As Double, ByRef ldblMontoComisionSinImpuesto As Double, ByRef ldblTotal As Double,
                                      Optional ByVal bReload As Boolean = False, Optional ByVal lstrTransaccion As String = "ObtenerComisionGeneral",
                                      Optional ByVal txtTipOperacionCompraVentaEtc As String = "")

        Dim lstrConceptoComision As Integer = 0
        Dim lstrTipoOperacion As String = IIf(lstrTipoOperacionMov = "I", "COMPRA", "VENTA")
        Dim lintIdContraparte As Integer = 0
        Dim ldblMontoOperacion As Double = 0
        Dim lstrCodigoEstado As String = "V"
        Dim lstrColumnas As String = ""
        Dim lstrResultado As String = ""
        Dim lstrSubClaseInstrumento As String = lstrSubClase

        Dim lblnAplicaPorcentaje As Boolean = True
        Dim lblnAfectoImpuesto As Boolean = True
        Dim DS_EsquemaComisionAux As DataSet = New DataSet
        Dim lobjComision As ClsComision = New ClsComision

        If bReload Then
            lstrColumnas = "ID_COMISION," &
               "ID_GRUPO_COMISIONES," &
               "ID_CONCEPTO_COMISION," &
               "DSC_CONCEPTO_COMISION," &
               "COD_CLASE_INSTRUMENTO," &
               "COD_SUB_CLASE_INSTRUMENTO," &
               "ID_CONTRAPARTE," &
               "DSC_CONTRAPARTE," &
               "TIPO_OPERACION," &
               "PORCENTAJE," &
               "MONTO," &
               "MONTO_IMPUESTO," &
               "AFECTO_IMPUESTO," &
               "COBRO_MINIMO," &
               "RANGO_DESDE," &
               "RANGO_HASTA," &
               "FECHA_VIGENCIA," &
               "APLICA_PORCENTAJE," &
               "DECIMALES_COMISION"
            DS_EsquemaComisionAux = lobjComision.Comision_BuscarEsquema(lstrIdCuenta, 0, "TRANS", lstrConceptoComision, lstrTipoOperacion, lstrClase, lstrSubClaseInstrumento, lintIdContraparte, lstrCodMoneda, ldblMontoOperacion, lstrCodigoEstado, lstrColumnas, lstrResultado)
        Else
            lstrResultado = "OK"
        End If
        If lstrResultado = "OK" Then
            If DS_EsquemaComisionAux.Tables.Count > 0 Then
                If DS_EsquemaComisionAux.Tables(0).Rows.Count > 0 Then
                    If (DS_EsquemaComision Is Nothing) OrElse (DS_EsquemaComisionAux.Tables(0).Columns.Item("ID_COMISION").ToString.Trim <> DS_EsquemaComision.Tables(0).Columns.Item("ID_COMISION").ToString.Trim) Then
                        DS_EsquemaComision = DS_EsquemaComisionAux.Copy
                        'MsgBox("El esquema de comisiones a cambiado", MsgBoxStyle.Information, "Ingerso Dep�sitos Nacionales")
                    End If
                    CalcularComisionEncabezadoDP(DS_EsquemaComision, dtbDetalle,
                           dtbComision, ldblSubTotal, ldblMontoComisionConImpuesto, ldblMontoComisionSinImpuesto)
                Else
                    If dtbDetalle.Rows.Count > 0 Then
                        ldblSubTotal = dtbDetalle.Compute("SUM(MONTO2)", "")
                    Else
                        ldblSubTotal = 0
                    End If
                End If
                '+ Si es un ingreso entonces al monto a invertir se le suman los cobros
                If lstrTipoOperacionMov = "I" Then
                    ldblTotal = ldblSubTotal + (ldblMontoComisionConImpuesto + ldblMontoComisionSinImpuesto)
                Else
                    '+ Si es un egreso entonces al monto a invertir se le restan los cobros
                    ldblTotal = ldblSubTotal - (ldblMontoComisionConImpuesto + ldblMontoComisionSinImpuesto)
                End If
            End If
        Else
            gFun_ResultadoMsg("1|No pudo obtener las informaci�n de comisiones" & vbCrLf & lstrResultado, gtxtNombreSistema & "-" & lstrTransaccion)
        End If
    End Sub

    Public Sub CalcularComisionImpuestos_SCDP(ByVal pDataComision As DataSet, ByVal pstrTipoOperacionMov As String,
                                            ByRef pdblSubTotal As Double, ByRef pdblMontoComisionConImpuesto As Double, ByRef pdblMontoComisionSinImpuesto As Double, ByRef pdblTotal As Double)
        Dim lComisionConImpuesto As Double = 0
        Dim lComisionSinImpuesto As Double = 0

        For lintRowCom As Integer = 0 To pDataComision.Tables(0).Rows.Count - 1

            If CDbl("0" & pDataComision.Tables(0).Rows(lintRowCom).Item("MONTO_IMPUESTO").ToString.Trim) <> 0 Then
                lComisionConImpuesto = lComisionConImpuesto + (CDbl(pDataComision.Tables(0).Rows(lintRowCom).Item("MONTO_IMPUESTO").ToString.Trim) + CDbl(pDataComision.Tables(0).Rows(lintRowCom).Item("MONTO").ToString.Trim))
            Else
                lComisionSinImpuesto = lComisionSinImpuesto + (CDbl(pDataComision.Tables(0).Rows(lintRowCom).Item("MONTO").ToString.Trim))
            End If
        Next
        pdblMontoComisionConImpuesto = lComisionConImpuesto
        pdblMontoComisionSinImpuesto = lComisionSinImpuesto

        If pstrTipoOperacionMov = "I" Then
            pdblTotal = pdblSubTotal + (pdblMontoComisionConImpuesto + pdblMontoComisionSinImpuesto)
        Else
            pdblTotal = pdblSubTotal - (pdblMontoComisionConImpuesto + pdblMontoComisionSinImpuesto)
        End If
    End Sub

    Public Sub CalcularComisionEncabezadoDP(ByVal pDS_Comision As DataSet, ByVal dtbDetalle As DataTable,
                                          ByVal dtbComision As DataTable,
                                          ByRef pdblSubTotal As Double,
                                          ByRef pdblMontoComisionConImpuesto As Double,
                                          ByRef pdblMontoComisionSinImpuesto As Double,
                                          Optional ByVal pOperacion As String = "0")
        Dim dtrComision As DataRow
        Dim ldblMonto As Double
        Dim lblnAplicaPorcentaje As Boolean
        Dim lblnAfectoImpuesto As Boolean
        Dim ldblPorcComision As Double
        Dim ldblMontoImpuesto As Double
        Dim dtrComisiones As DataRow
        Dim lsrtEncabezado As String = "0"

        '+ Siempre se borra el dateTable de Comisi�n
        dtbComision.Clear()

        pdblMontoComisionConImpuesto = 0
        pdblMontoComisionSinImpuesto = 0

        If dtbDetalle.Rows.Count <= 0 Then
            pdblSubTotal = 0
            pdblMontoComisionConImpuesto = 0
            pdblMontoComisionSinImpuesto = 0
        Else
            For Each dtrComisiones In dtbDetalle.Rows

                If (lsrtEncabezado <> CStr(dtrComisiones("OPERACION")) And pOperacion = 0) Or CStr(dtrComisiones("OPERACION")) = pOperacion Then
                    lsrtEncabezado = CStr(dtrComisiones("OPERACION"))
                    pdblSubTotal = 0
                    pdblMontoComisionConImpuesto = 0
                    pdblMontoComisionSinImpuesto = 0
                    pdblSubTotal = dtbDetalle.Compute("SUM(MONTO2)", "OPERACION = " & lsrtEncabezado)

                    '+ Se verifica la existencia de informaci�n en el dataset de las comisiones
                    If pDS_Comision.Tables.Count > 0 Then
                        '+ Se recorre el dataset de las comisiones
                        For lintIndex As Integer = 0 To pDS_Comision.Tables(0).Rows.Count - 1
                            dtrComision = pDS_Comision.Tables(0).Rows(lintIndex)

                            If Not IsNothing(dtrComision) Then
                                lblnAplicaPorcentaje = dtrComision("APLICA_PORCENTAJE")
                                lblnAfectoImpuesto = dtrComision("AFECTO_IMPUESTO")

                                If lblnAplicaPorcentaje Then
                                    ldblPorcComision = dtrComision("PORCENTAJE")
                                    ldblMonto = CDbl(pdblSubTotal) * (ldblPorcComision / 100)
                                Else
                                    ldblPorcComision = 0
                                    ldblMonto = dtrComision("MONTO")
                                End If
                                If Not (IsDBNull(dtrComision("AFECTO_IMPUESTO"))) AndAlso dtrComision("AFECTO_IMPUESTO") = True Then
                                    'ldblMontoImpuesto = Math.Round(ldblMonto * (gobjParametro.PorcentajeIva.ValorParametro / 100), CInt(dtrComision("DECIMALES_COMISION")))
                                    ldblMontoImpuesto = ldblMonto * (gobjParametro.PorcentajeIva.ValorParametro / 100)
                                    pdblMontoComisionConImpuesto = pdblMontoComisionConImpuesto + (ldblMonto + ldblMontoImpuesto)
                                Else
                                    pdblMontoComisionSinImpuesto = pdblMontoComisionSinImpuesto + ldblMonto
                                    ldblMontoImpuesto = 0
                                End If
                            End If

                            '+ Si se trata de agregar informaci�n
                            Dim dtrFila As DataRow
                            Dim lintNroDetalle As Integer = 0
                            'If Rdb_TipoOper_Orden.Checked Or Rdb_Solicitud_Inversion.Checked Then
                            lintNroDetalle = 1

                            dtrFila = dtbComision.NewRow
                            dtrFila("ID_COMISION") = dtrComision("ID_COMISION")
                            dtrFila("OPERACION") = dtrComisiones("OPERACION")
                            dtrFila("ID_NUMEROLINEA_DETALLE") = lintNroDetalle
                            dtrFila("PORCENTAJE") = ldblPorcComision
                            dtrFila("MONTO") = ldblMonto
                            dtrFila("IMPUESTO") = ldblMontoImpuesto
                            'dtrFila("DECIMALES") = dtrComision("DECIMALES_COMISION")
                            dtrFila("AFECTO_IMPUESTO") = dtrComision("AFECTO_IMPUESTO")
                            dtrFila("APLICA_PORCENTAJE") = dtrComision("APLICA_PORCENTAJE")
                            dtbComision.Rows.Add(dtrFila)
                        Next
                    End If
                End If
            Next
        End If
    End Sub

    Public Sub ReCalcularComisionGeneralDP(ByVal pDS_Comision As DataSet, ByVal dtbDetalle As DataTable, ByRef dtbComision As DataTable,
                                         ByVal lstrTipoOperacionMov As String, ByRef pdblSubTotal As Double,
                                         ByRef pdblMontoComisionConImpuesto As Double, ByRef pdblMontoComisionSinImpuesto As Double,
                                         ByRef pdblTotal As Double, Optional ByVal pOperacion As String = "")
        Dim dtrComision As DataRow
        Dim ldblMonto As Double
        Dim ldblMontoOperacion As Double
        Dim lintNroDetalle As Integer
        Dim lblnAplicaPorcentaje As Boolean
        Dim lblnAfectoImpuesto As Boolean
        Dim ldblPorcComision As Double
        Dim ldblMontoComisionConImpuesto As Double = 0
        Dim ldblMontoComisionSinImpuesto As Double = 0
        Dim ldblMontoImpuesto As Double

        dtbComision.Clear()

        If dtbDetalle.Rows.Count > 0 Then
            ldblMontoOperacion = dtbDetalle.Compute("SUM(MONTO2)", "OPERACION = " & pOperacion)
            '+ Se verifica la existencia de informaci�n en el dataset de las comisiones
            If pDS_Comision.Tables.Count > 0 Then
                '+ Se recorre el dataset de las comisiones
                For lintIndex As Integer = 0 To pDS_Comision.Tables(0).Rows.Count - 1
                    dtrComision = pDS_Comision.Tables(0).Rows(lintIndex)

                    If Not IsNothing(dtrComision) Then
                        lblnAplicaPorcentaje = dtrComision("APLICA_PORCENTAJE")
                        lblnAfectoImpuesto = dtrComision("AFECTO_IMPUESTO")

                        If Not IsDBNull(dtrComision("PORCENTAJE")) Then
                            ldblPorcComision = dtrComision("PORCENTAJE")
                            ldblMonto = CDbl(ldblMontoOperacion) * (ldblPorcComision / 100)
                        Else
                            ldblPorcComision = 0
                            ldblMonto = dtrComision("MONTO")
                        End If
                        If Not (IsDBNull(dtrComision("AFECTO_IMPUESTO"))) AndAlso dtrComision("AFECTO_IMPUESTO") = True Then
                            ldblMontoImpuesto = ldblMonto * (gobjParametro.PorcentajeIva.ValorParametro / 100)
                            ldblMontoComisionConImpuesto = ldblMontoComisionConImpuesto + (ldblMonto + ldblMontoImpuesto)
                        Else
                            '+ Si el concepto es por encabezado no por linea (Not pblnAplicaPorcentaje); es decir se suma s�lo una vez
                            If lblnAplicaPorcentaje Then
                                ldblMontoComisionSinImpuesto = ldblMontoComisionSinImpuesto + ldblMonto
                            Else
                                ldblMontoComisionSinImpuesto = ldblMontoComisionSinImpuesto + ldblMonto
                                ldblMontoImpuesto = 0
                            End If
                        End If

                        '+ Si se trata de agregar informaci�n
                        Dim dtrFila As DataRow

                        dtrFila = dtbComision.NewRow
                        dtrFila("ID_COMISION") = dtrComision("ID_COMISION")
                        dtrFila("ID_NUMEROLINEA_DETALLE") = lintNroDetalle
                        dtrFila("PORCENTAJE") = ldblPorcComision
                        dtrFila("MONTO") = ldblMonto
                        dtrFila("IMPUESTO") = ldblMontoImpuesto
                        dtrFila("DECIMALES") = dtrComision("DECIMALES_COMISION")
                        dtrFila("AFECTO_IMPUESTO") = dtrComision("AFECTO_IMPUESTO")
                        dtrFila("APLICA_PORCENTAJE") = dtrComision("APLICA_PORCENTAJE")
                        dtbComision.Rows.Add(dtrFila)
                    End If
                Next
            End If
            pdblMontoComisionConImpuesto = ldblMontoComisionConImpuesto
            pdblMontoComisionSinImpuesto = ldblMontoComisionSinImpuesto
            pdblSubTotal = ldblMontoOperacion
            If lstrTipoOperacionMov = "I" Then
                pdblTotal = pdblSubTotal + (pdblMontoComisionConImpuesto + pdblMontoComisionSinImpuesto)
            Else
                pdblTotal = pdblSubTotal - (pdblMontoComisionConImpuesto + pdblMontoComisionSinImpuesto)
            End If
        End If
    End Sub

    Public Function formatDecStr(ByVal Dato As String, ByVal Decimales As String) As String
        Dato = Dato.Replace(IIf(gtxtSeparadorDecimal = ".", ",", "."), gtxtSeparadorDecimal)
        formatDecStr = Format(CDbl(Dato), "###,###,###,##0" & Decimales)
    End Function


    Public Sub DetalleEstilo_Excel(ByRef Estilo_Excel As Excel.Style,
                                    ByRef lHojaExcel As Excel.Worksheet,
                                    ByVal Nombre As String,
                                    ByVal Font_Name As String,
                                    ByVal Font_Size As Integer,
                                    ByVal Font_Bold As Boolean,
                                    ByVal Font_Color As System.Drawing.Color,
                                    ByVal Interior_Color As System.Drawing.Color)

        Estilo_Excel = lHojaExcel.Application.ActiveWorkbook.Styles.Add(Nombre)
        Estilo_Excel.VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
        Estilo_Excel.Font.Name = Font_Name
        Estilo_Excel.Font.Size = Font_Size
        Estilo_Excel.Font.Bold = Font_Bold
        Estilo_Excel.Interior.Color = System.Drawing.ColorTranslator.ToOle(Interior_Color)
        Estilo_Excel.Font.Color = System.Drawing.ColorTranslator.ToOle(Font_Color)
        Estilo_Excel.Borders.LineStyle = Excel.XlLineStyle.xlContinuous
        Estilo_Excel.Borders.Weight = Excel.XlBorderWeight.xlThin
        Estilo_Excel.Borders(Excel.XlBordersIndex.xlEdgeTop).LineStyle = Excel.XlLineStyle.xlContinuous
        Estilo_Excel.Borders(Excel.XlBordersIndex.xlEdgeBottom).LineStyle = Excel.XlLineStyle.xlContinuous
        Estilo_Excel.Borders(Excel.XlBordersIndex.xlEdgeLeft).LineStyle = Excel.XlLineStyle.xlContinuous
        Estilo_Excel.Borders(Excel.XlBordersIndex.xlEdgeRight).LineStyle = Excel.XlLineStyle.xlContinuous
        Estilo_Excel.Borders(Excel.XlBordersIndex.xlDiagonalDown).LineStyle = Excel.XlLineStyle.xlLineStyleNone
        Estilo_Excel.Borders(Excel.XlBordersIndex.xlDiagonalUp).LineStyle = Excel.XlLineStyle.xlLineStyleNone
    End Sub

    Public Sub ConvierteColor(ByRef color As System.Drawing.Color, ByVal strColor As String)
        Select Case UCase(strColor)
            Case "BLUE"
                color = Drawing.Color.Blue
            Case "RED"
                color = Drawing.Color.Red
            Case "GREEN"
                color = Drawing.Color.Green
            Case "WHITE"
                color = Drawing.Color.White
            Case "YELLOW"
                color = Drawing.Color.Yellow
            Case "BROWN"
                color = Drawing.Color.Brown
            Case "DIMGRAY"
                color = Drawing.Color.DimGray
            Case "GRAY"
                color = Drawing.Color.Gray
            Case "PURPLE"
                color = Drawing.Color.Purple
            Case "BICE.TITULO.FORE"
                color = Color.FromArgb(&H9, &H3F, &H72)
            Case "BICE.TITULO.BACK"
                color = Color.FromArgb(&HDF, &HDF, &HDF)
            Case "BICE.DATA.FORE"
                color = Drawing.Color.Black
            Case "BICE.DATA.BACK"
                color = Color.FromArgb(&HDF, &HDF, &HDF)
            Case "BICE.SUBTOTAL.FORE"
                color = Drawing.Color.Black
            Case "BICE.SUBTOTAL.BACK"
                color = Color.FromArgb(&HDF, &HDF, &HDF)
            Case "BICE.TOTAL.FORE"
                color = Color.FromArgb(&H6, &H3D, &H70)
            Case "BICE.TOTAL.BACK"
                color = Color.FromArgb(&HCA, &HDA, &HE8)
            Case Else
                color = Drawing.Color.Black
        End Select
    End Sub


    'Public Sub Excel_TablaDinamica()

    '    Dim xlapp As New Excel.Application
    '    Dim xlBook As Excel.Workbook
    '    Dim xlSheet As Excel.Worksheet
    '    Dim xlSheet1 As Excel.Worksheet
    '    Dim PRange As Excel.Range
    '    Dim FinalRow As Long
    '    Dim FinalCol As Long

    '    Try
    '        ' Modify the File Name as necessary
    '        xlapp.Workbooks.Open("E:\XLS\InformeGestion2.xls")

    '        xlBook = xlapp.ActiveWorkbook()



    '        xlSheet.SaveAs("C:\Informe Diario\InforDiario-201011rafafa.xls", _
    '                                Microsoft.Office.Interop.Excel.XlFileFormat.xlExcel7)
    '        xlapp.Workbooks.Close()

    '        xlSheet = Nothing
    '        xlBook = Nothing

    'End Sub
#End Region

#Region "Funciones - Edo"

    Public Sub gSubGrabarAgrupacionGrilla(ByVal MyGrilla As C1.Win.C1TrueDBGrid.C1TrueDBGrid, ByVal strCodConfiguracion As String)
        '...Graba las agrupaciones de una grilla en la configuracion del usuario (para ser recuperada con "GSub_GuardaAgrupacionGrilla")
        Dim strAgrupacionOrden As String = ""
        Dim LTxt_Resultado As String = ""
        Dim ClsGeneral As ClsGeneral = New ClsGeneral

        Try
            Dim int_Index As Integer
            Dim arr_GroupBy(MyGrilla.GroupedColumns.Count - 1) As String

            For int_Index = 0 To MyGrilla.GroupedColumns.Count - 1
                strAgrupacionOrden = strAgrupacionOrden & MyGrilla.GroupedColumns.Item(int_Index).DataField.ToString() & "|"
                If MyGrilla.GroupedColumns.Item(int_Index).SortDirection.ToString.Trim() = "Ascending" Then
                    strAgrupacionOrden = strAgrupacionOrden & "1;"
                Else
                    strAgrupacionOrden = strAgrupacionOrden & "2;"
                End If
            Next

            If strAgrupacionOrden.Length > 0 Then
                strAgrupacionOrden = strAgrupacionOrden.Substring(0, strAgrupacionOrden.Trim.Length - 1)
            End If

            'ClsGeneral = GTxt_URLServiciosSigCO & GTxt_Generales_asmx

            LTxt_Resultado = ClsGeneral.GuardarConfiguracionUsuario(glngIdUsuario, strCodConfiguracion, strAgrupacionOrden)

        Catch ex As Exception
            MsgBox("Error al grabar configuracion Usuario.", vbCritical, My.Application.Info.Title & " : GSub_GuardarConfiguracionUsuario")
        End Try
    End Sub

    Public Sub gSubGrabarFiltrosGrilla(ByVal MyGrilla As C1.Win.C1TrueDBGrid.C1TrueDBGrid, ByVal strCodConfiguracion As String)
        '...Graba los filtros de una grilla en la configuracion del usuario (para ser recuperada con "GSub_GuardaAgrupacionGrilla")

        Dim strFiltrosOrden As String = ""
        Dim LTxt_Resultado As String = ""
        Dim ClsGeneral As ClsGeneral = New ClsGeneral

        Try
            Dim int_Index As Integer

            For int_Index = 0 To MyGrilla.Columns.Count - 1
                strFiltrosOrden = strFiltrosOrden & MyGrilla.Columns.Item(int_Index).FilterText.Trim & "|"
            Next

            LTxt_Resultado = ClsGeneral.GuardarConfiguracionUsuario(glngIdUsuario, strCodConfiguracion, strFiltrosOrden)

        Catch ex As Exception
            MsgBox("Error al grabar filtros de la lista.", vbCritical, My.Application.Info.Title & " : GSub_GrabarFiltrosGrilla")
        End Try
    End Sub

    Public Sub gSubGrabarOrdenGrilla(ByVal Grilla As C1.Win.C1TrueDBGrid.C1TrueDBGrid, ByVal strCodConfiguracion As String)
        '...Graba los ordenes de una grilla en la configuracion del usuario (para ser recuperada con "GSub_GuardaAgrupacionGrilla")
        Dim strOrden As String = ""
        Dim LTxt_Resultado As String = ""
        Dim ClsGeneral As ClsGeneral = New ClsGeneral

        Try
            Dim Col As C1.Win.C1TrueDBGrid.C1DataColumn
            Dim Index As Integer = 0

            For Index = 0 To Grilla.Columns.Count - 1
                Col = Grilla.Columns(Index)

                If Col.SortDirection <> C1.Win.C1List.SortDirEnum.None Then
                    strOrden = Index.ToString.Trim & "|"
                    strOrden = strOrden & Col.SortDirection
                    Exit For
                End If
            Next
            LTxt_Resultado = ClsGeneral.GuardarConfiguracionUsuario(glngIdUsuario, strCodConfiguracion, strOrden)
        Catch ex As Exception
            Exit Sub
        End Try
    End Sub

    Public Sub gSubRecuperarConfiguracionUsuario(ByRef Grilla As C1.Win.C1TrueDBGrid.C1TrueDBGrid, ByVal strCodConfiguracion As String, ByRef DatasetGrilla As DataSet)
        Dim ClsGeneral As ClsGeneral = New ClsGeneral
        Dim LTxt_Resultado As String = ""
        Dim strConfiguracion As String = ""

        LTxt_Resultado = ClsGeneral.RecuperaConfiguracionUsuario(glngIdUsuario, strCodConfiguracion, strConfiguracion)

        Dim Datos() = strConfiguracion.Split("|")

        Select Case strCodConfiguracion.Substring(3, 11)
            Case "AGRUP_VISTA"
                If strConfiguracion.Length > 0 And strConfiguracion <> "null" Then
                    garrColumnaAgrupacion = strConfiguracion.Split(";")
                    gSubRecuperaAgrupacionGrilla(Grilla, "Op")
                End If
            Case "FILTR_VISTA"
                If strConfiguracion.Length > 0 And strConfiguracion <> "null" Then
                    gSubRecuperaFiltrosGrilla(Grilla, Datos)
                End If
            Case "ORDEN_VISTA"
                Dim LOrdenGrilla As C1.Win.C1List.SortDirEnum
                Dim LColumna As Integer

                LOrdenGrilla = C1.Win.C1List.SortDirEnum.None
                LColumna = 0

                If strConfiguracion.Length > 0 And strConfiguracion <> "null" Then
                    LColumna = CInt(Datos(0))
                    If Datos(1) = "1" Then
                        LOrdenGrilla = C1.Win.C1TrueDBGrid.SortDirEnum.Ascending
                    Else
                        LOrdenGrilla = C1.Win.C1TrueDBGrid.SortDirEnum.Descending
                    End If
                    gSubRecuperarOrdenGrilla(LOrdenGrilla, LColumna, Grilla, DatasetGrilla)
                End If
        End Select
    End Sub

    'Public Sub gSubGrabarEstadoColumnasGrilla(ByVal strCodConfiguracion As String, ByVal strConfiguracion As String)
    '    Dim LTxt_Resultado As String = ""
    '    Dim ClsGeneral As ClsGeneral = New ClsGeneral

    '    Try
    '        LTxt_Resultado = ClsGeneral.GuardarConfiguracionUsuario(glngIdUsuario, strCodConfiguracion, strConfiguracion)

    '    Catch ex As Exception
    '        Exit Sub
    '    End Try
    'End Sub

    Public Function gFunIndiceElemento(ByVal Arreglo() As String, ByVal strElemento As String, Optional ByVal intIndiceDondeBusca As Integer = 0) As Integer
        Dim i As Int16
        Dim Retorna As Int16 = -1

        For i = 0 To Arreglo.Length - 1
            If (InStr(Arreglo(i).Split("|")(intIndiceDondeBusca), strElemento) > 0) And _
               (Arreglo(i).Split("|")(intIndiceDondeBusca).Trim.Length = strElemento.Trim.Length) Then
                Retorna = i
                Exit For
            End If
        Next
        Return Retorna
    End Function

    Public Function gFunEsColumnaVisible(ByVal ArregloColumnas() As String, ByVal strTituloColumna As String) As Boolean
        Dim intColIndex As Int16 = -1
        Dim strColEstado() As String
        Dim Retorno As Boolean = True

        If ArregloColumnas.Length > 0 Then
            intColIndex = gFunIndiceElemento(ArregloColumnas, strTituloColumna, 1)

            If intColIndex <> -1 Then
                strColEstado = ArregloColumnas(intColIndex).Split("|")
                If strColEstado(2) = "0" Then
                    Retorno = False
                End If
            End If
        End If
        Return Retorno
    End Function

    Public Function gFunEsColumnaFija(ByVal ArregloColumnas() As String, ByVal strTituloColumna As String) As Boolean
        Dim intColIndex As Int16 = -1
        Dim strColEstado() As String
        Dim Retorno As Boolean = True

        If ArregloColumnas.Length > 0 Then
            intColIndex = gFunIndiceElemento(ArregloColumnas, strTituloColumna, 1)

            If intColIndex <> -1 Then
                strColEstado = ArregloColumnas(intColIndex).Split("|")
                If strColEstado(3) = "0" Then
                    Retorno = False
                End If
            End If
        End If

        Return Retorno
    End Function

    Public Function gFunEsColumnaConfigurable(ByVal ArregloColumnas() As String, ByVal strTituloColumna As String) As Boolean
        Dim intColIndex As Int16 = -1
        Dim strColEstado() As String
        Dim Retorno As Boolean = True

        If ArregloColumnas.Length > 0 Then
            intColIndex = gFunIndiceElemento(ArregloColumnas, strTituloColumna, 1)

            If intColIndex <> -1 Then
                strColEstado = ArregloColumnas(intColIndex).Split("|")
                If strColEstado(4) = "N" Then
                    Retorno = False
                End If
            End If
        End If

        Return Retorno
    End Function

#End Region 'Eduardo - Funciones

#Region "Funciones Victoria Cortes"

    Private sBuffer As String ' Para usarla en las funciones GetSection(s)
    Private Declare Function GetPrivateProfileSection Lib "kernel32" Alias "GetPrivateProfileSectionA" (ByVal lpAppName As String, ByVal lpReturnedString As String, ByVal nSize As Integer, ByVal lpFileName As String) As Integer
    Private Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Integer, ByVal lpFileName As String) As Integer
    Private Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Integer, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Integer, ByVal lpFileName As String) As Integer

    Public Function ObtieneValor_ArchivoIni(ByVal sFileName As String, ByVal sSection As String, ByVal sKeyName As String, Optional ByVal sDefault As String = "") As String
        '--------------------------------------------------------------------------
        ' Devuelve el valor de una clave de un fichero INI
        ' Los par�metros son:
        '   sFileName   El fichero INI
        '   sSection    La secci�n de la que se quiere leer
        '   sKeyName    Clave
        '   sDefault    Valor opcional que devolver� si no se encuentra la clave
        '--------------------------------------------------------------------------
        Dim ret As Integer
        Dim sRetVal As String
        '
        sRetVal = New String(Chr(0), 255)
        '
        ret = GetPrivateProfileString(sSection, sKeyName, sDefault, sRetVal, Len(sRetVal), sFileName)
        If ret = 0 Then
            Return sDefault
        Else
            Return Microsoft.VisualBasic.Strings.Left(sRetVal, ret)
        End If
    End Function

    Public Function ObtieneSeccion_ArchivoIni(ByVal sFileName As String, ByVal sSection As String) As String()
        '--------------------------------------------------------------------------
        ' Lee una secci�n entera de un fichero INI                      (27/Feb/99)
        ' Adaptada para devolver un array de string                     (04/Abr/01)
        '
        ' Esta funci�n devolver� un array de �ndice cero
        ' con las claves y valores de la secci�n
        '
        ' Par�metros de entrada:
        '   sFileName   Nombre del fichero INI
        '   sSection    Nombre de la secci�n a leer
        ' Devuelve:
        '   Un array con el nombre de la clave y el valor
        '   Para leer los datos:
        '       For i = 0 To UBound(elArray) -1 Step 2
        '           sClave = elArray(i)
        '           sValor = elArray(i+1)
        '       Next
        '
        Dim aSeccion() As String
        Dim n As Integer
        '
        ReDim aSeccion(0)
        '
        ' El tama�o m�ximo para Windows 95
        sBuffer = New String(ChrW(0), 32767)
        '
        n = GetPrivateProfileSection(sSection, sBuffer, sBuffer.Length, sFileName)
        '
        If n > 0 Then
            sBuffer = sBuffer.Substring(0, n - 1).TrimEnd()
            ' Cada elemento estar� separado por un Chr(0)
            ' y cada valor estar� en la forma: clave = valor
            'aSeccion = sBuffer.Split(New Char() {ChrW(0), "="c})
            aSeccion = sBuffer.Split(ChrW(0))
        End If
        ' Devolver el array
        Return aSeccion
    End Function

    Function gFunEncripta(ByVal strCadena As String, ByVal blnModo As Boolean) As String
        Dim X As Single
        Dim xPdw As String
        Dim Caracteres As String
        Dim Codificacion As String

        Caracteres = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890abcdefghijklmnopqrstuvwxyz"
        Codificacion = "<?>zTcklop}IZiqLmatr@uxAOW[{nMGKXSysDBCP_h\EvNRw`]QHfbdeJgVFYU"

        xPdw = ""
        gFunEncripta = ""

        For X = 1 To Len(strCadena)
            If blnModo Then
                xPdw = xPdw + Chr((Asc(Mid(Codificacion, InStr(1, Caracteres, Mid(strCadena, X, 1)), 1)) - X))
            Else
                xPdw = xPdw + Mid(Caracteres, InStr(1, Codificacion, Chr(Asc(Mid(strCadena, X, 1)) + X)), 1)
            End If
        Next
        gFunEncripta = xPdw
    End Function

    Public Function gFunInitCapital(ByVal strTexto As String) As String
        Dim lstrTextoIniCapital As String = ""
        If strTexto.Length = 1 Then
            lstrTextoIniCapital = UCase(strTexto)
        Else
            Dim lstrTexto() As String = strTexto.Split(" ")
            Dim lintIndice As Integer = 0
            For lintIndice = 0 To lstrTexto.Length - 1
                If lstrTexto(lintIndice) <> "" Then
                    lstrTextoIniCapital = lstrTextoIniCapital & " " & UCase(lstrTexto(lintIndice).Substring(0, 1)) & LCase(lstrTexto(lintIndice).Substring(1, lstrTexto(lintIndice).Length - 1))
                End If
            Next
        End If

        Return lstrTextoIniCapital
    End Function

    Public Function gFechaUltimoCierre(ByVal intIdCuenta As String) As String
        Dim lobjCierre As ClsCierre = New ClsCierre
        Dim lstrResultado As String = ""
        Dim lstrFechaUltimoCierre As String = ""

        lstrFechaUltimoCierre = lobjCierre.FechaUltimoCierreConsultas(intIdCuenta, lstrResultado)
        If lstrResultado <> "OK" Then
            lstrFechaUltimoCierre = ""
            'MsgBox(lstrResultado, MsgBoxStyle.Exclamation, "Fecha Ultimo Cierre")
        End If

        Return lstrFechaUltimoCierre
    End Function

    Public Sub gsubGeneraGrillaConFormatoPredise�ado(ByVal strNomMaestro As String, ByVal intIdContraparte As Integer, ByRef tdg_grilla As C1.Win.C1TrueDBGrid.C1TrueDBGrid, ByVal dtDatosGrilla As DataTable)
        Dim DS_Columnas As New DataSet
        Dim DrColumna As DataRow = Nothing
        Dim lintcol As Integer = 0
        Dim lobjArchivo As ClsArchivo = New ClsArchivo
        Dim lstrResultadoTransaccion As String = ""
        Dim lblnVisible As Boolean = True

        DS_Columnas = lobjArchivo.Archivo_Importa_D_ParaGrilla(strNomMaestro, intIdContraparte, "", lstrResultadoTransaccion)
        If lstrResultadoTransaccion = "OK" Then

            If Not IsNothing(DS_Columnas) And DS_Columnas.Tables(0).Rows.Count > 0 Then
                If DS_Columnas.Tables(0).Rows.Count = dtDatosGrilla.Columns.Count Then
                    tdg_grilla.DataSource = dtDatosGrilla
                    lintcol = 0
                    For Each DrColumna In DS_Columnas.Tables(0).Rows
                        With tdg_grilla
                            .Columns(lintcol).Caption = DrColumna("CAMPO_ETIQUETA")
                            If DrColumna("TIPO_DATO") = 2 Then
                                .Columns(lintcol).Tag = "@D"
                            End If
                            .Splits(0).DisplayColumns(lintcol).HeadingStyle.HorizontalAlignment = getValorFormatoArr(DrColumna("Formato"), Est_Formato_XLS.enuAlignment)
                            .Splits(0).DisplayColumns(lintcol).Width = getValorFormatoArr(DrColumna("Formato"), Est_Formato_XLS.enuWidth)
                            .Columns(lintcol).DataField = DrColumna("CAMPO")
                            .Splits(0).DisplayColumns(lintcol).Style.HorizontalAlignment = getValorFormatoArr(DrColumna("Formato"), Est_Formato_XLS.enuAlignment)
                            lblnVisible = getValorFormatoArr(DrColumna("Formato"), Est_Formato_XLS.enuVisible)
                            .Splits(0).DisplayColumns(lintcol).Visible = getValorFormatoArr(DrColumna("Formato"), Est_Formato_XLS.enuVisible)
                            .Splits(0).DisplayColumns(lintcol).AllowSizing = getValorFormatoArr(DrColumna("Formato"), Est_Formato_XLS.enuAllowSizing)
                            .Splits(0).DisplayColumns(lintcol).FetchStyle = getValorFormatoArr(DrColumna("Formato"), Est_Formato_XLS.enuFetchStyle)
                            If lblnVisible Then
                                If Not IsDBNull(DrColumna("TIPO_DATO")) And DrColumna("TIPO_DATO") = 0 And DrColumna("LARGO") > 0 Then
                                    .Columns(lintcol).NumberFormat = getNumberFormat(DrColumna("LARGO"), DrColumna("DECIMALES"), _
                                                                                     IIf(DrColumna("FORMATO_CAMPO") = 0, False, True))
                                End If
                            End If
                        End With
                        lintcol = lintcol + 1
                    Next
                Else
                    gFun_ResultadoMsg("1|N�mero de columnas predise�adas no coincide con n�mero de columnas en los datos" & vbCrLf & lstrResultadoTransaccion, "Generando Datos")
                End If
            Else
                gFun_ResultadoMsg("1|No se encontr� definici�n de predise�o", "Generando Datos")
            End If
        Else
            gFun_ResultadoMsg("1|Error al buscar formato de predise�o" & vbCrLf & lstrResultadoTransaccion, "Generando Datos")
        End If
    End Sub

#End Region

#Region "Funciones - HAF: 8==3~X_O!!"

    Public Sub gsubFormatTextBox(ByRef Textbox As C1.Win.C1Input.C1TextBox, ByVal Formato As String)
        Textbox.CustomFormat = Formato
        Textbox.DisplayFormat.CustomFormat = Formato
        Textbox.EditFormat.CustomFormat = Formato
    End Sub

    Public Sub AddCampo(ByRef dtbTabla As DataTable, ByVal strNombreCampo As String, ByVal strTipoCampo As String)
        Dim dtcColumna As DataColumn = New DataColumn(strNombreCampo)
        dtcColumna.DataType = System.Type.GetType(strTipoCampo)
        dtcColumna.AllowDBNull = True
        dtbTabla.Columns.Add(dtcColumna)
    End Sub

    Public Sub gsubLimpiar_FiltroGrillas(ByRef Grilla As C1.Win.C1TrueDBGrid.C1TrueDBGrid)
        Dim intColumna As Integer
        Grilla.FilterActive = False
        For intColumna = 0 To Grilla.Columns.Count - 1
            If Grilla.Splits(0).DisplayColumns(intColumna).Visible Then
                Grilla.Columns(intColumna).FilterText = ""
            End If
        Next
        Grilla.FilterActive = True
    End Sub

    Public Sub gsubConfiguraDataTable(ByRef dtbTabla As DataTable, ByVal strColumnas As String)
        Dim lArr_NombreColumna() As String
        Dim ldtbTabla_Aux As New DataTable
        Dim Remove As Boolean
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String

        lArr_NombreColumna = Strings.Split(strColumnas, ",")

        ldtbTabla_Aux = dtbTabla

        If strColumnas.Trim = "" Then
            ldtbTabla_Aux = dtbTabla
        Else
            ldtbTabla_Aux = dtbTabla.Copy
            For Each Columna As DataColumn In dtbTabla.Columns
                Remove = True
                For LInt_Col = 0 To lArr_NombreColumna.Length - 1
                    LInt_NomCol = lArr_NombreColumna(LInt_Col).Trim
                    If Columna.ColumnName = LInt_NomCol Then
                        Remove = False
                        Exit For
                    End If
                Next
                If Remove Then
                    ldtbTabla_Aux.Columns.Remove(Columna.ColumnName)
                End If
            Next
        End If

        For LInt_Col = 0 To lArr_NombreColumna.Length - 1
            LInt_NomCol = lArr_NombreColumna(LInt_Col).Trim
            For Each Columna In ldtbTabla_Aux.Columns
                If Columna.ColumnName = LInt_NomCol Then
                    Columna.SetOrdinal(LInt_Col)
                    Exit For
                End If
            Next
        Next
        dtbTabla = ldtbTabla_Aux
    End Sub

    'Public Sub NotifyTextPopUp(ByVal pTexto As String _
    '                         , ByVal pTitulo As String _
    '                         , Optional ByVal pIcono As ToolTipIcon = ToolTipIcon.None)
    '    If Not goNotifyIcon Is Nothing Then
    '        If Not goNotifyIcon.Visible Then
    '            Try
    '                goNotifyIcon.Visible = True
    '            Catch ex As Exception
    '            End Try
    '        End If

    '        'If goNotifyIcon.Tag Then
    '        'goNotifyIcon.BalloonTipText = pTexto
    '        'goNotifyIcon.BalloonTipTitle = pTitulo
    '        'goNotifyIcon.BalloonTipIcon = pIcono
    '        'goNotifyIcon.ShowBalloonTip(50)
    '        'Else
    '        goNotifyIcon.ShowBalloonTip(100, pTitulo, pTexto, pIcono)
    '        'End If
    '    End If

    '    With Frm_PopUp
    '        .TopMost = True
    '        .Show()
    '        .Actualiza(pTitulo, pTexto)
    '        .Refresh()
    '    End With
    'End Sub

    Public Function NVL(ByVal varValor, ByVal varIsNull)
        If IsDBNull(varValor) Then
            Return varIsNull
        Else
            Return varValor
        End If
    End Function

    Public Function NTL(ByVal varValor, ByVal varIsNothing)
        If varValor Is Nothing Then
            Return varIsNothing
        Else
            Return varValor
        End If
    End Function

#End Region


    Public Function crearCsvDesdeDatatable(ByVal nombreArchivo As String, _
                                       ByVal dt As DataTable, _
                                       ByVal separatorChar As Char, _
                                       ByVal primeraFilaContieneNombres As Boolean, _
                                       ByVal textDelimiter As Boolean, _
                                       ByVal strDirectorio As String) As String
        'validaciones

        If dt.Rows.Count = 0 Then
            crearCsvDesdeDatatable = "Error al generar csv,sin filas en set de datos.- "
            Exit Function
        End If

        If separatorChar = "" Then
            crearCsvDesdeDatatable = "Error al generar csv,sin separador definido.- "
            Exit Function
        End If

        If nombreArchivo = "" Then
            crearCsvDesdeDatatable = "Error al generar csv,falta definir nombre archivo.- "
            Exit Function
        End If
        If strDirectorio = "" Then
            crearCsvDesdeDatatable = "Error al generar csv,falta ruta archivo.- "
            Exit Function
        End If
        ' Si no se ha especificado un nombre de archivo,
        ' o el objeto DataTable no es v�lido, provocamos
        ' una excepci�n de argumentos no v�lidos.
        If (nombreArchivo = String.Empty) Or (dt Is Nothing) Then
            crearCsvDesdeDatatable = "Argumentos no v�lidos."
            Exit Function
        End If
        ' Si el archivo existe, solicito confirmaci�n para sobreescribirlo.

        If (File.Exists(strDirectorio & "\" + nombreArchivo)) Then
            If (MessageBox.Show("Ya existe un archivo de texto con el mismo nombre." & Environment.NewLine & _
                               "�Desea sobrescribirlo?", _
                               "Crear archivo de texto delimitado", _
                               MessageBoxButtons.YesNo, _
                               MessageBoxIcon.Information) = DialogResult.No) Then Return False
        End If


        Dim sw As System.IO.StreamWriter

        Try
            Dim col As Integer = 0
            Dim value As String = String.Empty

            ' Creamos el archivo de texto con la codificaci�n por defecto.
            '
            sw = New StreamWriter(strDirectorio & "\" + nombreArchivo, False, System.Text.Encoding.Default)

            If (primeraFilaContieneNombres) Then
                ' La primera l�nea del archivo de texto contiene
                ' el nombre de los campos.
                For Each dc As DataColumn In dt.Columns

                    If (textDelimiter) Then
                        ' Incluimos el nombre del campo entre el caracter
                        ' delimitador de texto especificado.
                        '
                        value &= """" & dc.ColumnName & """" & separatorChar

                    Else
                        ' No se incluye caracter delimitador de texto alguno.
                        '
                        value &= dc.ColumnName & separatorChar

                    End If

                Next

                sw.WriteLine(value.Remove(value.Length - 1, 1))
                value = String.Empty

            End If

            ' Recorremos todas las filas del objeto DataTable
            ' incluido en el conjunto de datos.
            '
            For Each dr As DataRow In dt.Rows

                For Each dc As DataColumn In dt.Columns

                    If ((dc.DataType Is System.Type.GetType("System.String")) And _
                       (textDelimiter = True)) Then

                        ' Incluimos el dato alfanum�rico entre el caracter
                        ' delimitador de texto especificado.
                        '
                        value &= """" & dr.Item(col).ToString & """" & separatorChar

                    Else
                        ' No se incluye caracter delimitador de texto alguno
                        '
                        value &= dr.Item(col).ToString & separatorChar

                    End If

                    ' Siguiente columna
                    col += 1

                Next

                ' Al escribir los datos en el archivo, elimino el
                ' �ltimo car�cter delimitador de la fila.
                '
                sw.WriteLine(value.Remove(value.Length - 1, 1))
                value = String.Empty
                col = 0

            Next ' Siguiente fila

            ' Nos aseguramos de cerrar el archivo
            '
            sw.Close()

            crearCsvDesdeDatatable = "OK"

        Catch ex As Exception
            crearCsvDesdeDatatable = ex.Message

        Finally
            sw = Nothing

        End Try

    End Function
    Public Function GeneraCadena(ByVal dtTabla As DataTable, _
                                 ByVal strColumnas As String, _
                                 ByVal strSeparador As String, _
                                 ByRef strCadena1 As String, _
                                 Optional ByRef strCadena2 As String = "*", _
                                 Optional ByRef strCadena3 As String = "*") As String
        Dim lstrResultado As String = "OK"

        If strColumnas = "" Then
            lstrResultado = "No se han definido las columnas"
            Return lstrResultado
        End If

        Dim larr_NombreColumna() As String = strColumnas.Split(strSeparador)
        Dim lstrColumna As String = ""
        Dim lstrCadena As String = ""
        Dim lintNumeroCadena As Integer = 1
        strCadena1 = ""
        Dim lstrvalor As String = ""
        Dim DfechaPaso As Date
        Dim lstrCadena_paso As String = ""

        For Each DtFila As DataRow In dtTabla.Rows
            lstrCadena_paso = ""
            For lintindice As Integer = 0 To larr_NombreColumna.Length - 1
                lstrColumna = Trim(larr_NombreColumna(lintindice))

                If dtTabla.Columns.IndexOf(lstrColumna) = -1 Then
                    lstrvalor = "null"
                Else
                    If IsDBNull(DtFila(lstrColumna)) OrElse DtFila(lstrColumna).ToString.Trim = "" Then
                        lstrvalor = "null"
                    Else
                        'TypeOf(value) Is DateTime
                        If TypeOf (DtFila(lstrColumna)) Is DateTime Or InStr(lstrColumna, "FECHA") Or InStr(lstrColumna, "FCH_") Then
                            DfechaPaso = DtFila(lstrColumna)
                            lstrvalor = DfechaPaso.ToString.Substring(0, 10)
                        Else
                            lstrvalor = DtFila(lstrColumna).ToString.Trim
                        End If
                        'lstrvalor = DtFila(lstrColumna).ToString.Trim
                    End If
                End If
                lstrCadena_paso = lstrCadena_paso & lstrvalor & strSeparador
            Next

            If (lstrCadena & lstrCadena_paso).ToString.Length > 8000 Then
                If strCadena2 = "*" And strCadena3 = "*" Then
                    lstrResultado = "Detalle es demasiado largo"
                    Return lstrResultado
                End If
                Select Case lintNumeroCadena
                    Case 1
                        strCadena1 = lstrCadena
                        lintNumeroCadena = lintNumeroCadena + 1
                    Case 2
                        strCadena2 = lstrCadena
                        lintNumeroCadena = lintNumeroCadena + 1
                    Case 3
                        strCadena3 = lstrCadena
                        lstrResultado = "Detalle es demasiado largo"
                        Return lstrResultado
                End Select
                lstrCadena = ""
            End If
            lstrCadena = lstrCadena & lstrCadena_paso
        Next

        Select Case lintNumeroCadena
            Case 1
                strCadena1 = lstrCadena
            Case 2
                strCadena2 = lstrCadena
            Case 3
                strCadena3 = lstrCadena
        End Select

        Return lstrResultado
    End Function

    Public Function GeneraCadenaText(ByVal dtTabla As DataTable, _
                             ByVal strColumnas As String, _
                             ByVal strSeparador As String, _
                             ByRef strCadena As String) As String
        Dim lstrResultado As String = "OK"

        If strColumnas = "" Then
            lstrResultado = "No se han definido las columnas"
            Return lstrResultado
        End If

        Dim larr_NombreColumna() As String = strColumnas.Split(strSeparador)
        Dim lstrColumna As String = ""
        Dim lstrCadena As String = ""
        Dim lintNumeroCadena As Integer = 1
        strCadena = ""
        Dim lstrvalor As String = ""
        Dim DfechaPaso As Date

        For Each DtFila As DataRow In dtTabla.Rows
            For lintindice As Integer = 0 To larr_NombreColumna.Length - 1
                lstrColumna = Trim(larr_NombreColumna(lintindice))
                If dtTabla.Columns.IndexOf(lstrColumna) = -1 Then
                    lstrvalor = "null"
                Else
                    If IsDBNull(DtFila(lstrColumna)) OrElse DtFila(lstrColumna).ToString.Trim = "" Then
                        lstrvalor = "null"
                    Else
                        'TypeOf(value) Is DateTime
                        If TypeOf (DtFila(lstrColumna)) Is DateTime Or InStr(lstrColumna, "FECHA") Or InStr(lstrColumna, "FCH_") Then
                            DfechaPaso = DtFila(lstrColumna)
                            lstrvalor = DfechaPaso.ToString.Substring(0, 10)
                        Else
                            lstrvalor = DtFila(lstrColumna).ToString.Trim
                        End If
                        'lstrvalor = DtFila(lstrColumna).ToString.Trim
                    End If
                End If

                lstrCadena = lstrCadena & lstrvalor.ToString.Trim & strSeparador
            Next
            lstrCadena = lstrCadena & strSeparador
        Next
        strCadena = lstrCadena
        Return lstrResultado
    End Function

    ''Function EsFecha(ByVal sFecha As String) As Boolean
    ''    Dim sCodigoFecha As String = ""
    ''    Dim pos1 As Integer

    ''    EsFecha = True

    ''    'If CDate(sFecha) Then Exit Function
    ''    'MsgBox(sFecha)
    ''    If Len(sFecha) > 10 Then
    ''        pos1 = InStr(sFecha, "12:00:00")
    ''        sCodigoFecha = sFecha.ToString.Substring(pos1 - 1, (Len(sFecha) + 1 - pos1))
    ''        If sCodigoFecha = "12:00:00 AM" Or sCodigoFecha = "12:00:00 PM" Then
    ''            Exit Function
    ''        End If
    ''    End If
    ''    EsFecha = False
    ''End Function

    ''' <summary>
    ''' Funcion para enviar correos a N personas, con los nombres separados por coma (,).
    ''' </summary>
    ''' <param name="para">Parametro que integra a quien se Dirige el correo en PARA (se separan por coma , )</param>
    ''' <param name="cc">Parametro que integra a quien se Dirige el correo en COPIA (se separan por coma , )</param>
    ''' <param name="bcc">Parametro que integra a quien se Dirige el correo con COPIA OCULTA (se separan por coma , )</param>
    ''' <param name="asunto">Asunto del Correo</param>
    ''' <param name="contenido">El Cuerpo del correo</param>
    ''' <param name="filePath">Parametro que integra un archivo para enviar</param>
    ''' <param name="EsHtml">True or False si el contenido del correo es en HTML</param>
    ''' <returns>Si el envio resulto OK</returns>
    ''' <remarks></remarks>
    Public Function EnviarCorreo(ByVal para As String,
                                 ByVal cc As String,
                                 ByVal bcc As String,
                                 ByVal asunto As String,
                                 ByVal contenido As String,
                                 ByVal filePath As Dictionary(Of String, String),
                                 ByVal EsHtml As Boolean) As String
        Dim respuesta = String.Empty
        Try
            Dim smtpServer = ObtenerValorParametro("SERVIDOR_CORREO")
            Dim puerto = ObtenerValorParametro("SERVIDOR_CORREO_PORT")
            Dim correoWebMaster = ObtenerValorParametro("ENVIO_MAIL_CORREO")
            Dim correoUserWebMaster = ObtenerValorParametro("ENVIO_USER_CORREO")
            Dim validaPassword = ObtenerValorParametro("ENVIO_PASS_CORREO_SN")
            Dim enableSsl = ObtenerValorParametro("ENABLE_SSL_SERVIDOR")
            Dim correoPasswordWebMaster = String.Empty
            Dim correoDisplayName = String.Empty
            If validaPassword.Trim = "S" Then
                Try
                    correoPasswordWebMaster = ObtenerValorParametro("ENVIO_PASS_CORREO")
                    correoPasswordWebMaster = ClsEncriptacion.DesEncriptar(correoPasswordWebMaster)
                Catch
                    correoPasswordWebMaster = String.Empty
                End Try
            End If
            correoDisplayName = ObtenerValorParametro("ENVIO_DISPLAY_CORREO")
            Dim smtp As New SmtpClient With {
                .Host = smtpServer,
                .Port = puerto,
                .DeliveryMethod = SmtpDeliveryMethod.Network,
                .EnableSsl = enableSsl.Equals("S")
            }
            Dim mail As New MailMessage With {
                .From = New MailAddress(correoWebMaster.Trim(), correoDisplayName.Trim()),
                .SubjectEncoding = System.Text.Encoding.UTF8,
                .Subject = asunto,
                .Priority = MailPriority.Normal,
                .IsBodyHtml = EsHtml,
                .Body = contenido
            }

            Dim altView = AlternateView.CreateAlternateViewFromString(
                contenido,
                Nothing,
                MediaTypeNames.Text.Html)

            For Each path In filePath
                Dim img = New LinkedResource(path.Value, MediaTypeNames.Image.Jpeg) With {
                    .ContentId = path.Key,
                    .TransferEncoding = TransferEncoding.Base64
                }
                altView.LinkedResources.Add(img)
            Next

            mail.AlternateViews.Add(altView)

            mail.To.Add(para)
            If Not String.IsNullOrEmpty(cc) Then
                mail.CC.Add(cc)
            End If

            If Not String.IsNullOrEmpty(bcc) Then
                mail.Bcc.Add(bcc)
            End If

            Dim smtp As New SmtpClient With {
                .Host = smtpServer,
                .Port = puerto,
                .DeliveryMethod = SmtpDeliveryMethod.Network,
                .EnableSsl = enableSsl.Equals("S")
            }

            Console.WriteLine("funcion password inicio")

            If validaPassword.Trim = "S" Then
                Console.WriteLine("funcion password S")
                smtp.UseDefaultCredentials = False
                smtp.Credentials = New NetworkCredential(correoUserWebMaster.Trim(), correoPasswordWebMaster.Trim())
            Else
                Console.WriteLine("funcion password N")
                smtp.Credentials = CredentialCache.DefaultNetworkCredentials
            End If

            Console.WriteLine("funcion password FIN")

            Console.WriteLine("funcion smtp inicio")
            smtp.Port = 465
            smtp.Timeout = 1000000
            smtp.Send(mail)
            Console.WriteLine("funcion smtp fin")

            respuesta = "OK"
        Catch ex As SmtpException
            respuesta = ex.Message.ToString
            respuesta = String.Format("{0}{1}Source: {2}", respuesta, vbCrLf, ex.Source)
            respuesta = String.Format("{0}{1}StatusCode: {2}", respuesta, vbCrLf, ex.StatusCode)
            If Not IsNothing(ex.InnerException) Then
                respuesta = String.Format("{0}{1}InnerException: {2}", respuesta, vbCrLf, ex.InnerException.Message)
                respuesta = String.Format("{0}{1}TargetSite: {2}", respuesta, vbCrLf, ex.InnerException.Source)
                respuesta = String.Format("{0}{1}StackTrace: {2}{1}", respuesta, vbCrLf, ex.InnerException.StackTrace)
            End If
            Console.WriteLine("primer catch: " + respuesta)
        Catch ex As Exception
            respuesta = ex.Message.ToString
            Console.WriteLine("segundo catch: " + respuesta)
        End Try
        Return respuesta
    End Function

#Region "Funciones Esteban Reinoso"

    Public Sub VerificaExisteEnRelContraparteGPI(ByVal dblIdContraparte As Long, _
                                                 ByVal dblIdEntidadGPI As Double, _
                                                 ByVal strCodExterno As String, _
                                                 ByRef strMensaje As String)
        Dim strResultado As String = ""
        Dim dblSaldoCaja As Double = 0
        Dim DS_Alias As DataSet
        Dim lobjAlias As ClsAlias = New ClsAlias

        DS_Alias = lobjAlias.Alias_Ver("", CStr(dblIdEntidadGPI), CStr(dblIdContraparte), "", strResultado)

        If strResultado = "OK" Then
            If DS_Alias.Tables.Count > 0 Then
                If DS_Alias.Tables(0).Rows.Count > 0 Then
                    For Each dr As DataRow In DS_Alias.Tables(0).Rows
                        If dr("VALOR") = strCodExterno Then
                            strMensaje = "Ya existe la operaci�n con c�digo externo nro: " & strCodExterno
                            Exit Sub
                        End If
                    Next
                    strMensaje = "OK"
                Else
                    strMensaje = "OK"
                End If
            Else
                strMensaje = "OK"
            End If
        Else
            strMensaje = "OK"
        End If

    End Sub

#End Region

#Region "Funciones"

    Public Sub CargaComboTipoControl(ByRef meComboBox As C1.Win.C1List.C1Combo, _
                                     Optional ByVal strTransaccion As String = "", _
                                     Optional ByVal bIfBlank As Boolean = False, _
                                     Optional ByVal intIdTipoControl As Long = 0, _
                                     Optional ByVal strNombreTipoControl As String = "")

        Dim lobjTipoUsuario As ClsTipoUsuario = New ClsTipoUsuario
        Dim DS_General As New DataSet
        Dim DataTemp As DataRow
        Dim ltxtColumnas As String
        Dim ltxtResultadoTransaccion As String = Nothing

        Try
            ltxtColumnas = "NOMBRE_TIPO_CONTROL,ID_TIPO_CONTROL"
            '...Limpia Combo
            meComboBox.ClearItems()
            meComboBox.Text = ""
            '...Inicializa Data Set Combo
            DS_General = Nothing
            DS_General = lobjTipoUsuario.RetornaTipoControl(ltxtResultadoTransaccion, ltxtColumnas, intIdTipoControl, strNombreTipoControl)

            If ltxtResultadoTransaccion.ToUpper = "OK" Then
                '+ Si se indico que acepta blanco se incluye un registro en blanco en el combo
                If bIfBlank Then
                    DataTemp = DS_General.Tables("TIPOCONTROL").NewRow()
                    DataTemp("NOMBRE_TIPO_CONTROL") = ""
                    DataTemp("ID_TIPO_CONTROL") = 0
                    DS_General.Tables("TIPOCONTROL").Rows.Add(DataTemp)
                    DS_General.AcceptChanges()
                End If

                '...Asigna Data Source
                meComboBox.AddItem(";")
                meComboBox.DataSource = DS_General.Tables("TIPOCONTROL")
                meComboBox.Sort(0, C1.Win.C1List.SortDirEnum.ASC)

                '...Ordena y visibilidad de columnas del combo
                meComboBox.Columns(0).DataField = "NOMBRE_TIPO_CONTROL"
                meComboBox.Columns(1).DataField = "ID_TIPO_CONTROL"

                meComboBox.Splits(0).DisplayColumns(0).Visible = True
                meComboBox.Splits(0).DisplayColumns(1).Visible = False
                meComboBox.TextAlign = HorizontalAlignment.Left
            End If
        Catch ex As Exception
            ltxtResultadoTransaccion = ex.Message
        Finally
            If ltxtResultadoTransaccion <> "OK" Then
                MsgBox("Error al cargar combo Tipo Control : " & ltxtResultadoTransaccion, MsgBoxStyle.Information, gtxtNombreSistema & strTransaccion)
            End If
        End Try
    End Sub

    Public Sub Carga_Combos_Conceptos(ByRef meComboBox As C1.Win.C1List.C1Combo, _
                                      ByVal strConcepto As String, _
                                      Optional ByVal bIsBlank As Boolean = True, _
                                      Optional ByVal strEstado As String = "VIG")
        Dim DS_Concepto As New DataSet
        Dim DataTemp As DataRow

        Dim strColumnas As String = ""
        Dim ltxtResultadoTransaccion As String = ""
        strColumnas = "DSC_CONCEPTO, ID_CONCEPTO"
        ltxtResultadoTransaccion = ""

        Try
            '...Limpia Combo
            meComboBox.ClearItems()
            meComboBox.Text = ""
            '...Inicializa Data Set Combo
            DS_Concepto = Nothing
            DS_Concepto = lobjConcepto.Conceptos_Consultar(0, "", strColumnas, ltxtResultadoTransaccion, strConcepto, strEstado)

            '+ Si se indico que acepta blanco se incluye un registro en blanco en el combo
            If bIsBlank Then
                DataTemp = DS_Concepto.Tables("CONCEPTOS").NewRow()
                DataTemp("DSC_CONCEPTO") = ""
                DataTemp("ID_CONCEPTO") = 0
                DS_Concepto.Tables("CONCEPTOS").Rows.Add(DataTemp)
                DS_Concepto.AcceptChanges()
            End If
            '...Asigna Data Source
            meComboBox.AddItem(";")
            meComboBox.DataSource = DS_Concepto.Tables("CONCEPTOS")
            'meComboBox.Sort(0, C1.Win.C1List.SortDirEnum.ASC)
            '...Ordena y visibilidad de columnas del combo
            meComboBox.Columns(0).DataField = "DSC_CONCEPTO"
            meComboBox.Columns(1).DataField = "ID_CONCEPTO"

            If DS_Concepto.Tables(0).Rows.Count > 0 Then
                meComboBox.DataSource = DS_Concepto.Tables(0)
                meComboBox.Splits(0).DisplayColumns(0).Visible = True
                meComboBox.Splits(0).DisplayColumns(1).Visible = False
                meComboBox.SelectedIndex = -1
                meComboBox.TextAlign = HorizontalAlignment.Left
                meComboBox.Sort(1, C1.Win.C1List.SortDirEnum.ASC)
            End If
        Catch ex As Exception

        End Try
    End Sub

    Public Sub Carga_Combo_Tipo_Bloqueos(ByRef meComboBox As C1.Win.C1List.C1Combo, Optional ByVal Filtro As String = "", Optional ByVal bIsBlank As Boolean = True)
        '...Limpia Combo
        meComboBox.ClearItems()
        meComboBox.Text = ""
        '...Inicializa Data Set Combo
        Dim DataTemp As DataRow

        Dim DS_Tipo_Bloqueo As DataSet
        Dim ltxtColumnas As String = ""
        Dim ltxtResultadoTransaccion As String = ""
        ltxtColumnas = " DSC_TIPO_CONCEPTO,ID_TIPO_CONCEPTO,EST_TIPO_CONCEPTO"

        DS_Tipo_Bloqueo = Nothing
        DS_Tipo_Bloqueo = lobjConcepto.TipoConcepto_Ver(0, ltxtColumnas, ltxtResultadoTransaccion, Filtro)

        '+ Si se indico que acepta blanco se incluye un registro en blanco en el combo
        If bIsBlank Then
            DataTemp = DS_Tipo_Bloqueo.Tables("TIPO_CONCEPTOS").NewRow()
            DataTemp("DSC_TIPO_CONCEPTO") = ""
            DataTemp("ID_TIPO_CONCEPTO") = 0
            DataTemp("EST_TIPO_CONCEPTO") = ""
            DS_Tipo_Bloqueo.Tables("TIPO_CONCEPTOS").Rows.Add(DataTemp)
            DS_Tipo_Bloqueo.AcceptChanges()
        End If

        Try
            If ltxtResultadoTransaccion.ToUpper <> "OK" Then
                'MsgBox("Error al cargar combo Tipo Bloqueos : " & , MsgBoxStyle.Information, gtxtNombreSistema & " Mantenedor Conceptos de Bloqueos.")
            Else
                '...Asigna Data Source
                meComboBox.DataSource = DS_Tipo_Bloqueo.Tables(0)
                meComboBox.Sort(0, C1.Win.C1List.SortDirEnum.ASC)

                '...Ordena y visibilidad de columnas del combo
                meComboBox.Columns(0).DataField = "DSC_TIPO_CONCEPTO"
                meComboBox.Splits(0).DisplayColumns(0).Visible = True

                meComboBox.Columns(1).DataField = "ID_TIPO_CONCEPTO"
                meComboBox.Splits(0).DisplayColumns(1).Visible = False

                meComboBox.Columns(2).DataField = "EST_TIPO_CONCEPTO"
                meComboBox.Splits(0).DisplayColumns(2).Visible = False
            End If
        Catch ex As Exception
            'MsgBox("Error al cargar combo Tipo Bloqueos : " & , MsgBoxStyle.Information, gtxtNombreSistema & " Mantenedor Conceptos de Bloqueos.")
        End Try
    End Sub

#End Region

#Region "Funciones GPI++ Rodolfo Diaz"

    ' Obejto Multitarea para exportacion Excel
    Public bw As BackgroundWorker = New BackgroundWorker
    Public Sh_Thread As Thread
    Public Led_Thread As Thread
    Public Db_Led As Form

    Public Function Fnt_Resize_Controls(ByRef FrmCtrl As Object, ByRef Frm As Object) As Boolean
        ' ---------------------------------------------------------------------------------------------------------
        ' Auto-Ajuste de controles Resize
        ' RDiazG
        '
        ' Requerimientos : La funcion Requiere la declaracion de estos 2 elementos publicos de formulario
        ' Public Class {Formulario}
        '     Public PrevSizeX As Integer = Me.Width
        '     Public PrevSizeY As Integer = Me.Height
        '
        ' En el Form_Load Aplicar Tama�o Minimo del form:
        '
        '   Me.MinimumSize = Me.Size
        '
        '   
        ' Parametros : 
        ' Frm_Ctrl      Objeto Control de recorrido raiz, inicialmente siempre sera el form(Me) al cual se desea autoajustar
        '               La Propia Funcion crea la recursividad necesaria para recorrer los demas controles del set
        ' Frm           Objeto Formulario al cual se desea auto-ajustar
        '
        ' Ej Comun de uso: 
        '   
        '   Event Form_Resize
        '       Fnt_Resize_Controls(Me,Me)
        '   
        ' Nota: No modificar la propiedad anchor de los controles del formulario, dejar en default : top, Left
        '       Provoca Conflictos en el evento Resize
        ' -----------------------------------------------------------------------------------------------------------
        ' Versiones :
        '   RdiazG 1.0      : Version Inicial
        '   RdiazG 1.1      : Se Corrige descuadre al evitar recalcular controles al minimizar
        '   RdiazG 1.2      : Se a�ade C1DockingTab y C1DockingTabPage a Controles Contenedor
        '   RdiazG 1.3      : Se Elimina Ajuste de Formularios dependientes, no se reajustaran mas Forms dentro de Forms
        '   RdiazG 1.4      : Se Agrega a las Bases el Tipo TabControl
        '
        '
        ' -----------------------------------------------------------------------------------------------------------
        'Verificar Estado de formulario, si esta minimizado no recalcular controles
        If Frm.windowstate = FormWindowState.Minimized Then
            Frm.bringtofront()
            Exit Function
        End If

        Dim xCalc_Temp As Double = Frm.Width / Frm.PrevSizeX
        Dim yCalc_Temp As Double = Frm.Height / Frm.PrevSizeY

        ' Recorre Controles en su base
        For Each tmpControl In FrmCtrl.Controls

            ' Verifica Controles Contenedores y aplica recursividad
            If TypeOf (tmpControl) Is GroupBox Then Fnt_Resize_Controls(tmpControl, Frm)
            If TypeOf (tmpControl) Is TableLayoutPanel Then Fnt_Resize_Controls(tmpControl, Frm)
            If TypeOf (tmpControl) Is Panel Then Fnt_Resize_Controls(tmpControl, Frm)
            If TypeOf (tmpControl) Is TabControl Then Fnt_Resize_Controls(tmpControl, Frm)
            If TypeOf (tmpControl) Is TableLayoutPanel Then Fnt_Resize_Controls(tmpControl, Frm)
            If TypeOf (tmpControl) Is C1.Win.C1Command.C1DockingTab Then Fnt_Resize_Controls(tmpControl, Frm)
            If TypeOf (tmpControl) Is C1.Win.C1Command.C1DockingTabPage Then Fnt_Resize_Controls(tmpControl, Frm)
            If TypeOf (tmpControl) Is Form Then Exit Function
            ' Configuracion especifica por control Agregar mas segun Necesidad
            If TypeOf (tmpControl) Is C1.Win.C1List.C1Combo Then
                tmpControl.Left = tmpControl.Left * xCalc_Temp
                tmpControl.Top = tmpControl.Top * yCalc_Temp
                tmpControl.Width = tmpControl.Width * xCalc_Temp
            ElseIf TypeOf (tmpControl) Is C1.Win.C1Input.C1DateEdit Then
                tmpControl.Left = tmpControl.Left * xCalc_Temp
                tmpControl.Top = tmpControl.Top * yCalc_Temp
                tmpControl.Width = tmpControl.Width * xCalc_Temp
            ElseIf TypeOf (tmpControl) Is C1.Win.C1Input.C1TextBox Then
                tmpControl.Left = tmpControl.Left * xCalc_Temp
                tmpControl.Top = tmpControl.Top * yCalc_Temp
                tmpControl.Width = tmpControl.Width * xCalc_Temp
            ElseIf TypeOf (tmpControl) Is C1.Win.C1Input.C1Button Then
                tmpControl.Left = tmpControl.Left * xCalc_Temp
                tmpControl.Top = tmpControl.Top * yCalc_Temp
            ElseIf TypeOf (tmpControl) Is C1.Win.C1SuperTooltip.C1SuperLabel Then
                tmpControl.Left = tmpControl.Left * xCalc_Temp
                tmpControl.Top = tmpControl.Top * yCalc_Temp
            ElseIf TypeOf (tmpControl) Is ToolStrip Then
                tmpControl.Left = tmpControl.Left * xCalc_Temp
                tmpControl.Top = tmpControl.Top * yCalc_Temp
            Else
                tmpControl.Left = tmpControl.Left * xCalc_Temp
                tmpControl.Top = tmpControl.Top * yCalc_Temp
                tmpControl.Width = tmpControl.Width * xCalc_Temp
                tmpControl.Height = tmpControl.Height * yCalc_Temp
            End If
        Next tmpControl

        ' Se guarda ultimo tama�o de frm conocido, para siguiente cambio y calculo
        If FrmCtrl Is Frm Then
            Frm.PrevSizeX = Frm.Width
            Frm.PrevSizeY = Frm.Height
        End If

    End Function

    ' RdiazG :  Aplicacion de Exportacion Excel en modo Multi-Tasking
    '           Modulo de Invocacion principal
    ' Notas:    Los hilos multitarea no permiten llamadas com, ej: Msgbox, inputbox
    '           en el procedure do_work (Hilo Principal)
    '
    '   Objeto Principal de sub Tarea :     bw
    '                                       
    '       Consta de 3 eventos :   Do_Work (Codigo Principal del Hilo MT)
    '                               ProgressChanged (invocado para informar cambio de progreso del hilo)
    '                                               No usado en este caso
    '                               RunWorkerCompleted (se invoca al terminar el codigo del hilo)
    '                                           
    ' -------------------------------------------------------------------------------------------
    ' Versiones :       RdiazG 1.0  :   Version Inicial
    '
    '
    '
    '
    ' -------------------------------------------------------------------------------------------


    ' RdiazG Ejecuta un archivo externo, pero como una tarea independiente
    Public Sub Thread_Shell(ByVal pArchivo As Object)
        If MsgBox("Archivo Excel Exportado." & vbCr.ToString & "�Lo desea abrir?", MsgBoxStyle.Question + MsgBoxStyle.YesNo, "Gpi++") = MsgBoxResult.Yes Then
            Call Shell("cmd /K """ & CStr(pArchivo) & """") ' Ejecutar archivo excel
            'System.Diagnostics.Process.Start(CStr(pArchivo))
        End If
    End Sub

    Private Sub Termino_Exporta_Excel(ByVal Archivo As String)
        ''bw.Dispose() ' Limpiamos Hilo
        Try
            Sh_Thread.Abort() ' Abortamos el hilo si esque existe uno anterior
        Catch ex As Exception
        End Try
        Sh_Thread = New Thread(AddressOf Thread_Shell) ' creamos un hilo adicional en modo thread para un shell Rapido
        Sh_Thread.Start(Archivo) ' ejecutamos el hilo de ejecucion rapida de shell

        bw.CancelAsync() 'Solicitamos cancelacion del hilo en caso de perdida de sincronia
        bw = Nothing ' anulamos referencia
    End Sub

    Private Sub bw_RunWorkerCompleted(ByVal sender As Object, ByVal e As RunWorkerCompletedEventArgs)
        Dim proceso As System.Diagnostics.Process() = Nothing

        Try
            ' Eliminacion de Procesos Excel Pendientes
            proceso = System.Diagnostics.Process.GetProcesses()
            For Each opro As System.Diagnostics.Process In proceso
                If opro.ProcessName.ToUpper() = "EXCEL" Then
                    opro.Refresh()
                    If opro.StartTime >= CDate(e.Result.fechaInicioProceso) Then
                        If Not opro.HasExited Then opro.Kill()
                    End If
                End If
            Next
            ' Si el hilo no fue cancelado Consultamos al usuario su ejecucion.
        Catch ex As Exception
            ' Error en proceso de busqueda excel, sucede por desincronizacion del hilo, en caso de que suceda solo continuar
        End Try
        If Not e.Cancelled Then
            Termino_Exporta_Excel(e.Result.strDirectorioSeleccionado)
        End If
    End Sub

    Private Sub bw_ProgressChanged(ByVal sender As Object, ByVal e As ProgressChangedEventArgs)
        'Para Verificacion, No aplicable
    End Sub

    ' RdiazG    :   Proceso Principal del Hilo Multitarea
    ' ------------------------------------------------------------------------------
    '   Versiones   :   RdiazG 1.0  :   Version Inicial
    ' ------------------------------------------------------------------------------



    ' RdiazG    :   Sub proceso de exportacion Excel adaptado al hilo multitarea
    '------------------------------------------------------------------------------------------
    ' Versiones :   RdiazG 1.0  :   Version Inicial
    '------------------------------------------------------------------------------------------


    ' RdiazG    Funcion que indica si el hilo multitarea esta disponible o no
    ' ------------------------------------------------------------------------

    Public Function Fnt_Multitarea_Disponible() As Boolean
        Try
            If bw.IsBusy Then
                Return False
            Else
                Return True
            End If
        Catch ex As Exception
            Return True
        End Try
    End Function



    Public Function gstrExcel_FormatNumber(Optional ByVal intDecimales As Integer = 0 _
                                     , Optional ByVal blnObligatorioEntero As Boolean = True _
                                     , Optional ByVal blnObligatorioDecimal As Boolean = True) As String

        Dim lstrGroup As String = "." 'Application.CurrentCulture.NumberFormat.NumberGroupSeparator  'System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberGroupSeparator
        Dim lstrDecimal As String = "," 'System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator
        Dim lResultado As String = ""

        If System.Globalization.CultureInfo.CurrentCulture.Name = "es-ES" Then
            lstrGroup = "."
            lstrDecimal = ","
        End If

        lResultado = "#" & lstrGroup & "##" & IIf(blnObligatorioEntero, "0", "#")
        If intDecimales > 0 Then
            lResultado += (lstrDecimal & Strings.StrDup(intDecimales, IIf(blnObligatorioDecimal, "0", "#")))
        End If
        Return lResultado
    End Function

    Public Function gstrExcel_FormatDate() As String
        Dim lResultado As String = "dd/MM/yyyy"
        If System.Globalization.CultureInfo.CurrentCulture.Name = "es-ES" Then
            lResultado = "dd/MM/aaaa"
        Else
            lResultado = "dd/MM/yyyy"
        End If

        Return lResultado
    End Function

    Public Function Fnt_CargarDatosGrilla(ByRef pDset As DataSet, _
                                          ByRef pGrilla As C1.Win.C1TrueDBGrid.C1TrueDBGrid, _
                                          ByVal pIdContraparte As Integer, _
                                          ByVal pIdUsuario As Integer, _
                                          ByVal pNombre_Reporte As String, _
                                          ByVal pNombre_Plantilla As String) As Boolean

        'Dim objPlantillas As New ClsArchivo
        'Dim DS_Plantillas As New DataSet
        'Dim objPlantilla As New ClsArchivo
        'Dim DS_Plantilla As New DataSet

        'Dim strRetorno As String
        'Dim Matriz() As String
        'Dim indMatriz As Integer
        'Dim dtrFila As DataRow

        'DS_Plantillas = objPlantillas.Archivo_Importa_M_Consulta_Reportes(pIdContraparte, pNombre_Reporte, pIdUsuario, strRetorno) ' ("", lintIdContraparte, glngIdUsuario, "", strRetorno)
        'For Each dRow As DataRow In DS_Plantillas.Tables(0).Rows
        '    DS_Plantilla = objPlantilla.Archivo_Importa_D_PorPlantilla("", pIdContraparte, pIdUsuario, dRow("PLANTILLA_REPORTE"), strRetorno)
        '    ReDim matriz(0 To DS_Plantilla.Tables(0).Rows.Count - 1)
        '    indMatriz = 0
        '    For Each dRowPlantilla As DataRow In DS_Plantilla.Tables(0).Rows
        '        Matriz(indMatriz) = dRowPlantilla("CAMPO")
        '        indMatriz = indMatriz + 1
        '    Next
        'Next

        pGrilla.DataSource = pDset.Tables(0)

        'For Each drow As DataRow In pDset.Tables(0).Rows

        '    For indMatriz = 0 To UBound(Matriz)

        '    Next
        'Next

        ' Dim tdbGrilla As C1.Win.C1TrueDBGrid.C1TrueDBGrid
        'Dim lintcol As Integer = 0

        'tdbGrilla = pGrilla

        'tdbGrilla.Splits(0).HScrollBar.Style = C1.Win.C1TrueDBGrid.ScrollBarStyleEnum.Automatic
        'tdbGrilla.Splits(0).VScrollBar.Style = C1.Win.C1TrueDBGrid.ScrollBarStyleEnum.Always

        'With tdbGrilla

        'DS_Plantillas = objPlantillas.Archivo_Importa_M_Consulta_Reportes(lintIdContraparte, strPlantilla, glngIdUsuario, strRetorno) ' ("", lintIdContraparte, glngIdUsuario, "", strRetorno)

        'For Each dRow As DataRow In DS_Plantillas.Tables(0).Rows

        '    DS_Plantilla = objPlantilla.Archivo_Importa_D_PorPlantilla("", lintIdContraparte, glngIdUsuario, dRow("PLANTILLA_REPORTE"), strRetorno)

        '    For Each dRowPlantilla As DataRow In DS_Plantilla.Tables(0).Rows

        '        'Dim strFormato As String = dRowPlantilla("FORMATO")
        '        'Dim strWidth As String = strFormato.Substring(1, strFormato.Substring(1).IndexOf("|"))
        '        '.Splits(0).DisplayColumns(0).Locked = True
        '        '.Splits(0).DisplayColumns(lintcol).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Center
        '        '.Columns(lintcol).DataField = dRowPlantilla("CAMPO")
        '        '.Columns(lintcol).Caption = dRowPlantilla("CAMPO_ETIQUETA")
        '        '.Splits(0).DisplayColumns(0).Style.HorizontalAlignment = AlignHorzEnum.Near
        '        '.Splits(0).DisplayColumns(0).Visible = (dRowPlantilla("VISIBLE") = "T")
        '        '.Splits(0).DisplayColumns(0).Frozen = (dRowPlantilla("FIJO") = "T")
        '        '.Splits(0).DisplayColumns(0).FetchStyle = True
        '        '.Splits(0).DisplayColumns(lintcol).Width = strWidth '70
        '        'lintcol = lintcol + 1
        '    Next
        'Next
        'End With

    End Function

    Public Function Zoom_Control(ByRef Frm_Origen As Form, ByRef obj As Object, ByVal Ox As Integer, ByVal Oy As Integer, ByVal Ow As Integer, ByVal Oh As Integer, ByVal Zoom_Factor As Long) As Boolean
        Dim mw As Integer
        Dim mh As Integer
        Dim mpcx As Integer = Frm_Origen.Width / 2 ' Centro del form
        Dim mpcy As Integer = Frm_Origen.Height / 2 ' Centro del Form

        If Ox = obj.Left Then
            mw = ((obj.Width * Zoom_Factor)) ' ancho de control aumentado al factor
            mh = ((obj.Height * Zoom_Factor)) ' alto de control aumentado al factor 

            obj.Left = (mpcx - (mw / 2))
            obj.Top = (mpcy - (mh / 2)) - 50 ' - pixeles de reajuste con las barras
            obj.Width = mw
            obj.Height = mh
        Else
            obj.Left = Ox
            obj.Top = Oy
            obj.Width = Ow
            obj.Height = Oh
        End If
    End Function

#End Region

#Region "Funciones Enrique Valenzuela"
    'Public Function Consultar_ConsumirWsConsultaSaldo(ByVal strCtaCte As String, _
    '                                                   ByRef strRetorno As String) As DataRowView
    '    Dim dv As DataView = Nothing
    '    Dim drv As DataRowView = Nothing
    '    Dim dsParametro As New DataSet
    '    Dim dsConsultaSaldo As New DataSet
    '    Try
    '        Dim strOrigenWs As String = ""
    '        dsParametro = gobjParametro.RetornarValorParametro("WS_BICE", strRetorno)
    '        If dsParametro Is Nothing Or dsParametro.Tables(0).Rows.Count = 0 Then
    '            Throw New System.Exception("Error al leer Parametro WS_BICE.")
    '        Else
    '            strOrigenWs = dsParametro.Tables(0).Rows(0).Item("VALOR_PARAMETRO")
    '            If strOrigenWs.Trim = "" Then
    '                Throw New System.Exception("Par�metro WS_BICE no esta ingresado.")
    '            End If
    '        End If
    '        If strOrigenWs.ToUpper = "BICE" Then
    '            strRetorno = ConsumirWsConsultaSaldoBice(strCtaCte, dsConsultaSaldo)
    '        ElseIf strOrigenWs.ToUpper = "LOCAL" Then
    '            strRetorno = ConsumirWsConsultaSaldo(strCtaCte, dsConsultaSaldo)
    '        End If
    '        If strRetorno = "OK" Then
    '            If dsConsultaSaldo.Tables(0).Rows.Count > 0 Then
    '                dv = dsConsultaSaldo.Tables(0).DefaultView
    '                ' ''para filtrar en caso de que sean varias filas
    '                ''Dim strNombreColumna As String = ""
    '                ''Dim strValor As String = ""
    '                ''dv.RowFilter = strNombreColumna & " = '" & strValor & "'"
    '                ''For Each drvFor As DataRowView In dv
    '                ''    drv = drvFor
    '                ''Next
    '                Return dv(0)
    '            Else
    '                Throw New System.Exception("WsConsultaSaldo no ha devuelto datos")
    '            End If
    '        Else
    '            Throw New System.Exception(strRetorno)
    '        End If
    '    Catch ex As Exception
    '        strRetorno = ex.Message
    '    End Try
    '    Return drv
    'End Function


    Public Function Convierte_DataTable_XML(ByVal dt As DataTable, ByRef strRetorno As String) As String
        Try
            Dim ds As New DataSet
            ds.Tables.Add(dt.Copy)
            ds.DataSetName = "DS"
            ds.Tables(0).TableName = "XMLData"
            Dim memStream As New MemoryStream
            ds.Tables(0).WriteXml(memStream, True)
            memStream.Seek(0, SeekOrigin.Begin)
            Dim sr As New StreamReader(memStream)
            strRetorno = "OK"
            Return sr.ReadToEnd
        Catch ex As Exception
            strRetorno = ex.GetType.ToString & " " & ex.Message
            Return "NO"
        End Try
    End Function
#End Region

#Region "Funciones Varias GPI++"

    Public Function gstrFiltro_X_TipoColumna(ByRef pdcColuma As C1.Win.C1TrueDBGrid.C1DataColumn) As String
        Dim lstrFiltro As String = ""
        Dim lstrVarFiltro As String = ""
        Try
            Select Case pdcColuma.DataType.ToString.ToUpper
                Case "SYSTEM.STRING"
                    If pdcColuma.FilterText.ToString = "True" Then
                        lstrVarFiltro = "1"
                    ElseIf pdcColuma.FilterText.ToString = "False" Then
                        lstrVarFiltro = "0"
                    Else
                        lstrVarFiltro = pdcColuma.FilterText.ToString()
                    End If
                    'lstrFiltro = (pdcColuma.DataField & " like '%" & pdcColuma.FilterText & "%'")
                    lstrFiltro = ("[" & pdcColuma.DataField & "] like '%" & lstrVarFiltro & "%'")
                Case "SYSTEM.DATETIME"
                    lstrFiltro = ("CONVERT([" & pdcColuma.DataField & "], 'System.String') like '%" & pdcColuma.FilterText & "%'")
                Case "SYSTEM.DECIMAL", "SYSTEM.INT32"
                    If pdcColuma.NumberFormat.Length = 0 Then
                        'ES UN NUMERO SIN FORMATO PERO QUE LO COMPARAMOS COMO STRING
                        'lstrFiltro = ("CONVERT(" & pdcColuma.DataField & ", 'System.String') like '%" & pdcColuma.FilterText. & "%'")
                        If pdcColuma.FilterText.ToString = "True" Then
                            lstrVarFiltro = "1"
                        ElseIf pdcColuma.FilterText.ToString = "False" Then
                            lstrVarFiltro = "0"
                        Else
                            lstrVarFiltro = pdcColuma.FilterText.ToString()
                        End If
                        lstrFiltro = ("CONVERT([" & pdcColuma.DataField & "], 'System.String') like '%" & lstrVarFiltro.Replace(",", ".") & "%'")
                    Else
                        'lstrFiltro = pdcColuma.FilterText
                        lstrFiltro = pdcColuma.FilterText.ToString.Replace(",", ".")
                        If gFunFiltroValido(lstrFiltro) Then
                            lstrFiltro = pdcColuma.DataField & lstrFiltro
                        Else
                            lstrFiltro = ""
                        End If
                    End If
                Case "SYSTEM.DOUBLE"
                    If pdcColuma.NumberFormat.Length = 0 Then
                        'ES UN NUMERO SIN FORMATO PERO QUE LO COMPARAMOS COMO STRING
                        'lstrFiltro = ("CONVERT(" & pdcColuma.DataField & ", 'System.String') like '%" & pdcColuma.FilterText & "%'")
                        If pdcColuma.FilterText.ToString = "True" Then
                            lstrVarFiltro = "1"
                        ElseIf pdcColuma.FilterText.ToString = "False" Then
                            lstrVarFiltro = "0"
                        Else
                            lstrVarFiltro = pdcColuma.FilterText.ToString()
                        End If
                        lstrFiltro = ("CONVERT([" & pdcColuma.DataField & "], 'System.String') like '%" & lstrVarFiltro.Replace(",", ".") & "%'")
                    Else
                        'lstrFiltro = pdcColuma.FilterText
                        lstrFiltro = pdcColuma.FilterText.ToString.Replace(",", ".")
                        If gFunFiltroValido(lstrFiltro) Then
                            lstrFiltro = pdcColuma.DataField & lstrFiltro
                        Else
                            lstrFiltro = ""
                        End If
                    End If
                Case "SYSTEM.BOOLEAN"
                    If pdcColuma.FilterText.ToString = "True" Then
                        lstrVarFiltro = "True"
                    ElseIf pdcColuma.FilterText.ToString = "False" Then
                        lstrVarFiltro = "False"
                    Else
                        lstrVarFiltro = pdcColuma.FilterText.ToString()
                    End If
                    lstrFiltro = ("CONVERT([" & pdcColuma.DataField & "],'System.String') like '%" & lstrVarFiltro & "%'")
                Case Else
                    lstrFiltro = (pdcColuma.DataField & " like '%" & pdcColuma.FilterText & "%'")
            End Select
        Catch ex As Exception
        End Try

        Return lstrFiltro
    End Function

    Public Function CuentaFiltro(ByVal strFiltro As String) As Integer
        Dim arrFiltro() As String

        If strFiltro = "" Then
            Return 0
        Else
            arrFiltro = strFiltro.Split("�")
            Return arrFiltro.Count - 1
        End If
    End Function

    Public Function ObtenerContraparte(ByVal lstrCodContraparte As String) As Integer
        Dim lobjContraparte As New ClsContraParte
        Dim DS_Contraparte As New DataSet
        Dim strRetorno As String = ""

        DS_Contraparte = lobjContraparte.EntidadContraParte_Ver(lstrCodContraparte, "N", "VIG", "N", "", strRetorno)

        If DS_Contraparte Is Nothing Then
            MsgBox("No se encontr� la contraparte c�digo: " & lstrCodContraparte, MsgBoxStyle.Information, gtxtNombreSistema & " ")
            Return 0
        End If

        If DS_Contraparte.Tables.Count > 0 Then
            If DS_Contraparte.Tables(0).Rows.Count > 0 Then
                Return DS_Contraparte.Tables(0).Rows(0).Item("ID_CONTRAPARTE")
            Else
                MsgBox("No se encontr� la contraparte c�digo: " & lstrCodContraparte, MsgBoxStyle.Information, gtxtNombreSistema & " ")
                Return 0
            End If
        Else
            MsgBox("No se encontr� la contraparte c�digo: " & lstrCodContraparte, MsgBoxStyle.Information, gtxtNombreSistema & " ")
            Return 0
        End If
    End Function

    Public Sub TraerFamilia(ByRef objetoControl As C1.Win.C1List.C1Combo, _
                             Optional ByVal strValorizacion As String = "", _
                             Optional ByVal strCodFamilia As String = "", _
                             Optional ByVal enblanco As Boolean = False)

        Dim lobjSubClases As ClsSubClaseInstrumento = New ClsSubClaseInstrumento
        Dim DS_Familia As DataSet = New DataSet
        Dim strRetorno As String = ""
        Dim strColumnas As String = ""

        strColumnas = "DESCRIPCION, CODIGO_FAMILIA, VALORIZACION"
        '...Limpia Combo
        objetoControl.ClearItems()
        objetoControl.Text = ""

        Try
            objetoControl.ClearItems()
            '...Inicializa Data Set Combo
            DS_Familia = Nothing
            DS_Familia = lobjSubClases.Familia_Ver(strValorizacion, strCodFamilia, strColumnas, strRetorno)
            If enblanco Then DS_Familia.Tables(0).Rows.Add("")
            If DS_Familia.Tables(0).Rows.Count > 0 Then
                objetoControl.DataSource = DS_Familia.Tables(0)
                objetoControl.Sort(0, C1.Win.C1List.SortDirEnum.ASC)
                objetoControl.Columns(0).DataField = "DESCRIPCION"
                objetoControl.Splits(0).DisplayColumns(0).Visible = True
                objetoControl.Columns(1).DataField = "CODIGO_FAMILIA"
                objetoControl.Splits(0).DisplayColumns(1).Visible = False
                objetoControl.Columns(2).DataField = "VALORIZACION"
                objetoControl.Splits(0).DisplayColumns(2).Visible = False
                objetoControl.SelectedIndex = -1
            End If
        Catch ex As Exception
        End Try
    End Sub

#End Region

#Region "Funciones GPI++ Eugenio Roco"

    Public Sub LlenaComboPlantillaReporte(ByRef cmb As ComboBox, ByRef DS As DataSet)
        Try
            If DS.Tables(0).Rows.Count > 0 Then
                cmb.Items.Clear()
                For Each DrColumna In DS.Tables(0).Rows
                    cmb.Items.Add(DrColumna.Item(4))
                Next
                cmb.SelectedIndex = 0
            End If
        Catch ex As Exception

        End Try
    End Sub

    Public Sub LlenaComboPlantillaReporte(ByRef cmb As ToolStripComboBox, ByRef DS As DataSet)
        Try
            If DS.Tables(0).Rows.Count > 0 Then
                cmb.Items.Clear()
                For Each DrColumna In DS.Tables(0).Rows
                    cmb.Items.Add(DrColumna.Item(4))
                Next
                cmb.SelectedIndex = 0
            End If
        Catch ex As Exception

        End Try
    End Sub

    Public Sub ConfiguraGrillaConPlantilla(ByRef dtgGrilla As C1.Win.C1TrueDBGrid.C1TrueDBGrid, _
                                           ByVal strReporte As String, _
                                           ByVal strPlantilla As String, _
                                           ByVal lintIdContraparte As Integer, _
                                           Optional ByVal ColCheck As Boolean = False)
        Dim objPlantillas As New ClsArchivo
        Dim DS_Plantillas As New DataSet
        Dim objPlantilla As New ClsArchivo
        Dim DS_Plantilla As New DataSet

        Dim tdbGrilla As C1.Win.C1TrueDBGrid.C1TrueDBGrid
        Dim lintcol As Integer = 0
        Dim strRetorno As String = ""
        Dim lIntDecimales As Integer = 0

        Try
            tdbGrilla = dtgGrilla
            tdbGrilla.Splits(0).HScrollBar.Style = C1.Win.C1TrueDBGrid.ScrollBarStyleEnum.Automatic
            tdbGrilla.Splits(0).VScrollBar.Style = C1.Win.C1TrueDBGrid.ScrollBarStyleEnum.Always

            With tdbGrilla

                .EvenRowStyle.ForeColor = Color.Black
                .EvenRowStyle.BackColor = Color.White
                .EvenRowStyle.Font = New Font("Arial", 6.5, FontStyle.Regular)
                .EvenRowStyle.GradientMode = GradientModeEnum.None

                .OddRowStyle.ForeColor = Color.Black
                .OddRowStyle.BackColor = Color.LightCyan
                .OddRowStyle.Font = New Font("Arial", 6.5, FontStyle.Regular)
                .OddRowStyle.GradientMode = GradientModeEnum.None
                .AlternatingRows = True

                Dim intColumna
                ' RdiazG Antes Se ocultan todas las columnas
                For intColumna = 0 To (tdbGrilla.Columns.Count - 1)
                    .Splits(0).DisplayColumns(intColumna).Visible = False
                Next

                If ColCheck Then
                    .Columns(lintcol).Caption = "Sel"
                    .Splits(0).DisplayColumns(lintcol).Locked = True
                    .Splits(0).DisplayColumns(lintcol).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Center
                    .Columns(lintcol).DataField = "CHK"
                    '.Columns(lintcol).Value = True
                    .Columns(lintcol).DefaultValue = "0"
                    '.Columns(lintcol).DataType = System.Type.GetType("System.Boolean")
                    .Splits(0).DisplayColumns(lintcol).Style.HorizontalAlignment = AlignHorzEnum.Near
                    .Splits(0).DisplayColumns(lintcol).Visible = True
                    .Splits(0).DisplayColumns(lintcol).FetchStyle = True
                    .Splits(0).DisplayColumns(lintcol).Width = 20
                    '.Splits(0).DisplayColumns(lintcol)
                    gsubConfig_ColumnaCHK(.Columns("CHK"))
                    lintcol = lintcol + 1
                End If

                DS_Plantillas = objPlantillas.Archivo_Importa_M_Consulta_Reportes(lintIdContraparte, strReporte, glngIdUsuario, strRetorno) ' ("", lintIdContraparte, glngIdUsuario, "", strRetorno)
                For Each dRow As DataRow In DS_Plantillas.Tables(0).Rows
                    If strPlantilla = dRow("PLANTILLA_REPORTE") Then
                        DS_Plantilla = objPlantilla.Archivo_Importa_D_PorPlantilla(strReporte, lintIdContraparte, glngIdUsuario, strPlantilla, strRetorno)
                        For Each dRowPlantilla As DataRow In DS_Plantilla.Tables(0).Rows
                            Dim strCampos() As String = dRowPlantilla("FORMATO").ToString.Split("|")
                            Dim strWidth As String = strCampos(1)
                            Dim intAlinea As Integer = CInt(strCampos(2))
                            'Dim strWidth As String = strFormato.Substring(1, strFormato.Substring(1).IndexOf("|"))
                            'Dim intAlinea As Integer = CInt(strFormato.Substring(2, strFormato.Substring(1).IndexOf("|")))

                            .Splits(0).DisplayColumns(lintcol).Locked = True
                            .Splits(0).DisplayColumns(lintcol).HeadingStyle.HorizontalAlignment = C1.Win.C1TrueDBGrid.AlignHorzEnum.Center
                            .Columns(lintcol).DataField = dRowPlantilla("CAMPO")
                            .Columns(lintcol).Caption = dRowPlantilla("CAMPO_ETIQUETA")
                            If dRowPlantilla("TIPO_DATO") = 0 And dRowPlantilla("DECIMALES") <> 0 Then
                                .Columns(lintcol).NumberFormat = getNumberFormat(25, dRowPlantilla("DECIMALES"))
                                '"###,###,###,###,##0.0000;(###,###,###,###,##0.0000)" 
                                ';(###.###.###.###.##0,0000)" 
                                'getNumberFormat(6, dRowPlantilla("DECIMALES"))
                            ElseIf dRowPlantilla("TIPO_DATO") = 0 Then
                                .Columns(lintcol).NumberFormat = getNumberFormat(18, 0)
                            ElseIf dRowPlantilla("TIPO_DATO") = 1 Then
                                .Columns(lintcol).NumberFormat = "@"
                            ElseIf dRowPlantilla("TIPO_DATO") = 2 Then
                                .Columns(lintcol).NumberFormat = "dd/MM/yyyy"
                                .Columns(lintcol).Tag = "@D"
                            ElseIf dRowPlantilla("TIPO_DATO") = 3 Then
                                .Columns(lintcol).NumberFormat = getNumberFormat(25, lIntDecimales)
                            End If
                            .Splits(0).DisplayColumns(lintcol).Style.HorizontalAlignment = intAlinea
                            .Splits(0).DisplayColumns(lintcol).Visible = (dRowPlantilla("VISIBLE") = "T")
                            .Splits(0).DisplayColumns(lintcol).Frozen = (dRowPlantilla("FIJO") = "T")
                            .Splits(0).DisplayColumns(lintcol).AllowSizing = (dRowPlantilla("TAMANO") = "T")
                            .Splits(0).DisplayColumns(lintcol).FetchStyle = True
                            .Splits(0).DisplayColumns(lintcol).Width = strWidth
                            '.Splits(0).DisplayColumns(lintcol)
                            lintcol = lintcol + 1
                        Next
                        Exit For
                    End If
                Next
                '.SetDataBinding (
            End With
        Catch ex As Exception

        Finally

        End Try
    End Sub

    Public Sub gsubConfig_ColumnaCHK(ByRef pdcColuma As C1.Win.C1TrueDBGrid.C1DataColumn)
        '+ Codigo para traducir los Valores de la BD con el Check
        Dim items As C1.Win.C1TrueDBGrid.ValueItems = pdcColuma.ValueItems
        ' Se traducen los Valores - El Datasource necesita mantener 2 estados
        items.Translate = True
        ' muestra la celda como un Checkbox
        items.Presentation = C1.Win.C1TrueDBGrid.PresentationEnum.CheckBox
        ' Se asocian los valores de la BD con el estado del Check
        items.Values.Clear()
        items.Values.Add(New C1.Win.C1TrueDBGrid.ValueItem("0", False)) ' deschequeado
        items.Values.Add(New C1.Win.C1TrueDBGrid.ValueItem("1", True)) ' chequeado
        items.Validate = True
    End Sub

    Public Function GeneraCadenaConCheck(ByVal dtTabla As DataTable, _
                             ByVal strColumnas1 As String, _
                             ByVal strColumnas2 As String, _
                             ByVal strSeparador As String, _
                             ByRef strCadena1 As String, _
                             Optional ByRef strCadena2 As String = "*", _
                             Optional ByRef strCadena3 As String = "*", _
                             Optional ByVal bolFinal As Boolean = False) As String
        Dim lstrResultado As String = "OK"

        If strColumnas1 = "" Then
            lstrResultado = "No se han definido las columnas"
            Return lstrResultado
        End If

        Dim larr_NombreColumna() As String = strColumnas1.Split(strSeparador)
        Dim lstrColumna1 As String = ""
        Dim lstrColumna2 As String = ""
        Dim lstrCadena As String = ""
        Dim lintNumeroCadena As Integer = 1
        strCadena1 = ""
        Dim lstrvalor As String = ""
        Dim booAgrega As Boolean = True
        Dim DfechaPaso As Date

        For Each DtFila As DataRow In dtTabla.Rows
            For lintindice As Integer = 0 To larr_NombreColumna.Length - 1
                lstrColumna1 = Trim(larr_NombreColumna(lintindice))
                If dtTabla.Columns.IndexOf(lstrColumna1) = -1 Then
                    lstrvalor = "null"
                Else
                    If IsDBNull(DtFila(lstrColumna1)) OrElse DtFila(lstrColumna1).ToString.Trim = "" Then
                        lstrvalor = "null"
                    Else
                        'TypeOf(value) Is DateTime
                        If TypeOf (DtFila(lstrColumna1)) Is DateTime Or InStr(lstrColumna1, "FECHA") Or InStr(lstrColumna1, "FCH_") Then
                            DfechaPaso = DtFila(lstrColumna1)
                            lstrvalor = DfechaPaso.ToString.Substring(0, 10)
                        Else
                            lstrvalor = DtFila(lstrColumna1).ToString.Trim
                        End If

                    End If
                End If
                If dtTabla.Columns.IndexOf(strColumnas2) = -1 Then
                    booAgrega = False
                Else
                    booAgrega = DtFila(strColumnas2)
                End If
                ' RDiazG 17-11-2011 Se quita limitacion de 8000 de longitud basado en el parametro Varchar
                'If (lstrCadena & lstrvalor & strSeparador).ToString.Length > 8000 Then
                '    If strCadena2 = "*" And strCadena3 = "*" Then
                '        lstrResultado = "Detalle es demasiado largo"
                '        Return lstrResultado
                '    End If
                '    Select Case lintNumeroCadena
                '        Case 1
                '            strCadena1 = lstrCadena
                '            lintNumeroCadena = lintNumeroCadena + 1
                '        Case 2
                '            strCadena2 = lstrCadena
                '            lintNumeroCadena = lintNumeroCadena + 1
                '        Case 3
                '            strCadena3 = lstrCadena
                '            lstrResultado = "Detalle es demasiado largo"
                '            Return lstrResultado
                '    End Select

                '    lstrCadena = ""

                'End If
                If booAgrega Then
                    lstrCadena = lstrCadena & lstrvalor.ToString.Trim & strSeparador
                End If
            Next
            If bolFinal And DtFila(strColumnas2) = 1 Then
                lstrCadena = lstrCadena & strSeparador
            End If
        Next

        Select Case lintNumeroCadena
            Case 1
                strCadena1 = lstrCadena
            Case 2
                strCadena2 = lstrCadena
            Case 3
                strCadena3 = lstrCadena
        End Select

        Return lstrResultado
    End Function

    Public Sub gSubCargaComboAlineacion( _
                ByRef objetoControl As C1.Win.C1List.C1Combo)
        Try
            objetoControl.ComboStyle = ComboStyleEnum.DropdownList
            objetoControl.DataMode = DataModeEnum.AddItem
            objetoControl.ClearItems()
            objetoControl.AddItem("General;0")
            objetoControl.AddItem("Izquierda;1")
            objetoControl.AddItem("Centrado;2")
            objetoControl.AddItem("Derecha;3")
            objetoControl.AddItem("Justificado;4")
            objetoControl.Splits(0).DisplayColumns(0).Visible = True
            objetoControl.Splits(0).DisplayColumns(1).Visible = False
            objetoControl.SelectedIndex = 2
        Catch ex As Exception
        End Try
    End Sub

    Public Sub gSubCargaComboTipoDato( _
            ByRef objetoControl As C1.Win.C1List.C1Combo)
        Try
            objetoControl.ComboStyle = ComboStyleEnum.DropdownList
            objetoControl.DataMode = DataModeEnum.AddItem
            objetoControl.ClearItems()
            objetoControl.AddItem("Numerico;0")
            objetoControl.AddItem("Texto;1")
            objetoControl.AddItem("Fecha;2")
            objetoControl.AddItem("Monto;3")
            objetoControl.Splits(0).DisplayColumns(0).Visible = True
            objetoControl.Splits(0).DisplayColumns(1).Visible = False
            objetoControl.SelectedIndex = 2
        Catch ex As Exception
        End Try
    End Sub

    Public Sub gCargaComboTipoPeriodoComision(ByRef objetoControl As C1.Win.C1List.C1Combo)
        Dim lobjTipoPeriodo As ClsComision = New ClsComision
        Dim DS_TipoPeriodo As DataSet = New DataSet
        Dim strRetorno As String = ""
        Try
            objetoControl.ClearItems()
            DS_TipoPeriodo = lobjTipoPeriodo.Comision_Tipo_Periodo_Consultar("DSC_TIPO_PERIODO,ID_TIPO_PERIODO,COD_TIPO_PERIODO", "", "", strRetorno)
            If DS_TipoPeriodo.Tables(0).Rows.Count > 0 Then
                objetoControl.ComboStyle = ComboStyleEnum.DropdownList
                objetoControl.DataMode = DataModeEnum.AddItem
                objetoControl.DataSource = DS_TipoPeriodo.Tables(0)

                For i As Integer = 0 To DS_TipoPeriodo.Tables(0).Rows.Count - 1
                    Dim lstrDescripcion As String = DS_TipoPeriodo.Tables(0).Rows(i).Item("DSC_TIPO_PERIODO")
                    Dim lstrCod As String = DS_TipoPeriodo.Tables(0).Rows(i).Item("ID_TIPO_PERIODO")
                    Dim lstrCodTipoeriodo As String = DS_TipoPeriodo.Tables(0).Rows(i).Item("COD_TIPO_PERIODO")
                    objetoControl.AddItem(lstrDescripcion & ";" & lstrCod & ";" & lstrCodTipoeriodo)
                Next i

                objetoControl.Splits(0).DisplayColumns(0).Visible = True
                objetoControl.Splits(0).DisplayColumns(1).Visible = False
                objetoControl.Splits(0).DisplayColumns(2).Visible = False
                objetoControl.SelectedIndex = -1
            End If
        Catch ex As Exception

        End Try
    End Sub

    Public Sub gCargaComboTipoMedioPagoCobroComision(ByRef objetoControl As C1.Win.C1List.C1Combo, _
                                                     ByVal strTipoPagoCobro As String)
        Dim lobjTipoPeriodo As ClsComision = New ClsComision
        Dim DS_TipoPeriodo As DataSet = New DataSet
        Dim strRetorno As String = ""
        Try
            objetoControl.ClearItems()
            DS_TipoPeriodo = lobjTipoPeriodo.Comision_Tipo_Medio_Pago_Cobro_Consultar("DSC_MEDIO_PAGO_COBRO,COD_MEDIO_PAGO_COBRO", "", "", strTipoPagoCobro, strRetorno)
            If DS_TipoPeriodo.Tables(0).Rows.Count > 0 Then
                objetoControl.ComboStyle = ComboStyleEnum.DropdownList
                objetoControl.DataMode = DataModeEnum.AddItem

                objetoControl.AddItem(";")
                For i As Integer = 0 To DS_TipoPeriodo.Tables(0).Rows.Count - 1
                    Dim lstrDescripcion As String = DS_TipoPeriodo.Tables(0).Rows(i).Item("DSC_MEDIO_PAGO_COBRO")
                    Dim lstrCod As String = DS_TipoPeriodo.Tables(0).Rows(i).Item("COD_MEDIO_PAGO_COBRO")
                    objetoControl.AddItem(lstrDescripcion & ";" & lstrCod)
                Next i

                objetoControl.Splits(0).DisplayColumns(0).Visible = True
                objetoControl.Splits(0).DisplayColumns(1).Visible = False
                objetoControl.SelectedIndex = -1
            End If
        Catch ex As Exception

        End Try
    End Sub

    Public Sub gCargaComboModalidadComision(ByRef objetoControl As C1.Win.C1List.C1Combo)
        Dim lobjModalidad As ClsComision = New ClsComision
        Dim DS_Modalidad As DataSet = New DataSet
        Dim strRetorno As String = ""
        Try
            objetoControl.ClearItems()
            DS_Modalidad = lobjModalidad.Comision_Modalidad_Consultar("COD_MODALIDAD,ID_MODALIDAD", 0, "", strRetorno)
            If DS_Modalidad.Tables(0).Rows.Count > 0 Then
                objetoControl.ComboStyle = ComboStyleEnum.DropdownList
                objetoControl.DataMode = DataModeEnum.AddItem

                For i As Integer = 0 To DS_Modalidad.Tables(0).Rows.Count - 1
                    Dim lstrDescripcion As String = DS_Modalidad.Tables(0).Rows(i).Item("COD_MODALIDAD")
                    Dim lstrCod As String = DS_Modalidad.Tables(0).Rows(i).Item("ID_MODALIDAD")
                    objetoControl.AddItem(lstrDescripcion & ";" & lstrCod)
                Next i

                objetoControl.Splits(0).DisplayColumns(0).Visible = True
                objetoControl.Splits(0).DisplayColumns(1).Visible = False
                objetoControl.SelectedIndex = -1
            End If
        Catch ex As Exception

        End Try
    End Sub


    Public Function ValidaPlantillaRangoComisiones(ByRef Tbl_Input As DataTable, ByRef strCodMoneda As String) As String
        Dim strReturn As String = "OK"
        Dim DS_Temp As New DataSet
        Dim DS_Moneda As New DataSet
        Dim objMoneda As ClsMoneda = New ClsMoneda
        Dim intDecimales As Integer = 0
        Dim dblUnidadMinima As Double = 1
        Dim strRetorno As String = "OK"

        Dim filterExp As String = "1=1"
        Dim sortExp As String = "DESDE"
        Dim drarray() As DataRow
        Dim intCount As Integer

        DS_Moneda = objMoneda.Moneda_Ver(strCodMoneda, "MONEDA_DECIMALES", "VIG", strRetorno)

        If strRetorno = "OK" AndAlso Not (DS_Moneda Is Nothing) Then

            intDecimales = DS_Moneda.Tables(0).Rows(0).Item("MONEDA_DECIMALES")

            For intCount = 1 To intDecimales
                dblUnidadMinima /= 10
            Next
            drarray = Tbl_Input.Select(Nothing, sortExp)
            DS_Temp.Tables.Add()
            For intCount = 1 To (drarray.Length - 1)
                If drarray(intCount)("DESDE") < drarray(intCount - 1)("HASTA") + dblUnidadMinima Then
                    strReturn = "Rangos Sobrepuestos"
                    Exit For
                End If
            Next
            If strReturn = "OK" Then
                For intCount = 1 To (drarray.Length - 1)
                    If drarray(intCount)("DESDE") <> drarray(intCount - 1)("HASTA") + dblUnidadMinima Then
                        strReturn = "Rango No Definido"
                        Exit For
                    End If
                Next
            End If
        Else
            strReturn = "Datos de la m�neda no encontrados"
        End If

        Return strReturn
    End Function

#End Region

    Public Sub CalcularTotales(ByVal tdgGrilla As C1.Win.C1TrueDBGrid.C1TrueDBGrid, ByVal lstrColumnasTotales As String)
        Dim lIntColumna As Integer = 0
        Dim lintFila As Integer
        Dim ldblTotalColumna As Double
        Dim larrNombreCol() As String = lstrColumnasTotales.Split("|")
        Dim lblnTituloTotal As Boolean = False

        tdgGrilla.ColumnFooters = True

        For lIntColumna = 0 To tdgGrilla.Columns.Count - 1
            If Array.IndexOf(larrNombreCol, tdgGrilla.Columns(lIntColumna).DataField) <> -1 Then
                If Not lblnTituloTotal Then
                    If lIntColumna > 0 Then
                        'Tdg_Reporte.Columns(lIntColumna - 1).FooterText = "TOTALES"
                        tdgGrilla.Columns(1).FooterText = "TOTALES"
                    End If
                    lblnTituloTotal = True
                End If

                ldblTotalColumna = 0
                For lintFila = 0 To tdgGrilla.RowCount - 1
                    If IsDBNull(tdgGrilla.Columns(lIntColumna).CellValue(lintFila)) Then
                        ldblTotalColumna = ldblTotalColumna + 0
                    Else
                        ldblTotalColumna = ldblTotalColumna + tdgGrilla.Columns(lIntColumna).CellValue(lintFila)
                    End If
                Next
                tdgGrilla.Columns(lIntColumna).FooterText = Format(ldblTotalColumna, tdgGrilla.Columns(lIntColumna).NumberFormat)
            End If
        Next
        tdgGrilla.ColumnFooters = True
        tdgGrilla.FooterStyle.Font = New Font(tdgGrilla.Font.FontFamily, 8, FontStyle.Bold, GraphicsUnit.Point)
        'Tdg_Reporte.FooterStyle.BackgroundImage = My.Resources.Resources.Img_BarraStatus_MDIPrincipal
        tdgGrilla.FooterStyle.HorizontalAlignment = C1.Win.C1TrueDBGrid.AlignHorzEnum.Far

        tdgGrilla.FooterStyle.ForeColor = Color.Black
        'Tdg_ComisionesPorCobrar.Refresh()
    End Sub

    Public Sub ConfiguraGrillaConPlantillaUMS(ByRef dtgGrilla As C1.Win.C1TrueDBGrid.C1TrueDBGrid, _
                                           ByVal strReporte As String, _
                                           ByVal strPlantilla As String, _
                                           ByVal lintIdContraparte As Integer, _
                                           ByVal StrMoneda As String, _
                                           ByVal lIntDecimales As Integer, _
                                           Optional ByVal ColCheck As Boolean = False)
        Dim objPlantillas As New ClsArchivo
        Dim DS_Plantillas As New DataSet
        Dim objPlantilla As New ClsArchivo
        Dim DS_Plantilla As New DataSet

        Dim tdbGrilla As C1.Win.C1TrueDBGrid.C1TrueDBGrid
        Dim lintcol As Integer = 0
        Dim strRetorno As String = ""


        Try
            tdbGrilla = dtgGrilla
            tdbGrilla.Splits(0).HScrollBar.Style = C1.Win.C1TrueDBGrid.ScrollBarStyleEnum.Automatic
            tdbGrilla.Splits(0).VScrollBar.Style = C1.Win.C1TrueDBGrid.ScrollBarStyleEnum.Always

            With tdbGrilla

                .EvenRowStyle.ForeColor = Color.Black
                .EvenRowStyle.BackColor = Color.White
                .EvenRowStyle.Font = New Font("Arial", 6.5, FontStyle.Regular)
                .EvenRowStyle.GradientMode = GradientModeEnum.None

                .OddRowStyle.ForeColor = Color.Black
                .OddRowStyle.BackColor = Color.LightCyan
                .OddRowStyle.Font = New Font("Arial", 6.5, FontStyle.Regular)
                .OddRowStyle.GradientMode = GradientModeEnum.None
                .AlternatingRows = True

                Dim intColumna
                ' RdiazG Antes Se ocultan todas las columnas
                For intColumna = 0 To (tdbGrilla.Columns.Count - 1)
                    .Splits(0).DisplayColumns(intColumna).Visible = False
                Next

                If ColCheck Then
                    .Columns(lintcol).Caption = "Sel"
                    .Splits(0).DisplayColumns(lintcol).Locked = True
                    .Splits(0).DisplayColumns(lintcol).HeadingStyle.HorizontalAlignment = AlignHorzEnum.Center
                    .Columns(lintcol).DataField = "CHK"
                    '.Columns(lintcol).Value = True
                    .Columns(lintcol).DefaultValue = "0"
                    '.Columns(lintcol).DataType = System.Type.GetType("System.Boolean")
                    .Splits(0).DisplayColumns(lintcol).Style.HorizontalAlignment = AlignHorzEnum.Near
                    .Splits(0).DisplayColumns(lintcol).Visible = True
                    .Splits(0).DisplayColumns(lintcol).FetchStyle = True
                    .Splits(0).DisplayColumns(lintcol).Width = 20
                    '.Splits(0).DisplayColumns(lintcol)
                    gsubConfig_ColumnaCHK(.Columns("CHK"))
                    lintcol = lintcol + 1
                End If

                DS_Plantillas = objPlantillas.Archivo_Importa_M_Consulta_Reportes(lintIdContraparte, strReporte, glngIdUsuario, strRetorno) ' ("", lintIdContraparte, glngIdUsuario, "", strRetorno)
                For Each dRow As DataRow In DS_Plantillas.Tables(0).Rows
                    If strPlantilla = dRow("PLANTILLA_REPORTE") Then
                        DS_Plantilla = objPlantilla.Archivo_Importa_D_PorPlantilla(strReporte, lintIdContraparte, glngIdUsuario, strPlantilla, strRetorno)
                        For Each dRowPlantilla As DataRow In DS_Plantilla.Tables(0).Rows
                            Dim strCampos() As String = dRowPlantilla("FORMATO").ToString.Split("|")
                            Dim strWidth As String = strCampos(1)
                            Dim intAlinea As Integer = CInt(strCampos(2))
                            'Dim strWidth As String = strFormato.Substring(1, strFormato.Substring(1).IndexOf("|"))
                            'Dim intAlinea As Integer = CInt(strFormato.Substring(2, strFormato.Substring(1).IndexOf("|")))

                            .Splits(0).DisplayColumns(lintcol).Locked = True
                            .Splits(0).DisplayColumns(lintcol).HeadingStyle.HorizontalAlignment = C1.Win.C1TrueDBGrid.AlignHorzEnum.Center
                            .Columns(lintcol).DataField = dRowPlantilla("CAMPO")
                            .Columns(lintcol).Caption = dRowPlantilla("CAMPO_ETIQUETA")
                            If dRowPlantilla("TIPO_DATO") = 0 And dRowPlantilla("DECIMALES") <> 0 Then
                                .Columns(lintcol).NumberFormat = getNumberFormat(25, dRowPlantilla("DECIMALES"))
                            ElseIf dRowPlantilla("TIPO_DATO") = 0 Then
                                .Columns(lintcol).NumberFormat = getNumberFormat(18, 0)
                            ElseIf dRowPlantilla("TIPO_DATO") = 1 Then
                                .Columns(lintcol).NumberFormat = "@"
                            ElseIf dRowPlantilla("TIPO_DATO") = 2 Then
                                .Columns(lintcol).NumberFormat = "dd/MM/yyyy"
                                .Columns(lintcol).Tag = "@D"
                            ElseIf dRowPlantilla("TIPO_DATO") = 3 Then
                                .Columns(lintcol).NumberFormat = getNumberFormat(25, lIntDecimales)
                            End If
                            .Splits(0).DisplayColumns(lintcol).Style.HorizontalAlignment = intAlinea
                            .Splits(0).DisplayColumns(lintcol).Visible = (dRowPlantilla("VISIBLE") = "T")
                            .Splits(0).DisplayColumns(lintcol).Frozen = (dRowPlantilla("FIJO") = "T")
                            .Splits(0).DisplayColumns(lintcol).AllowSizing = (dRowPlantilla("TAMANO") = "T")
                            .Splits(0).DisplayColumns(lintcol).FetchStyle = True
                            .Splits(0).DisplayColumns(lintcol).Width = strWidth
                            '.Splits(0).DisplayColumns(lintcol)
                            lintcol = lintcol + 1
                        Next
                        Exit For
                    End If
                Next
                '.SetDataBinding (
            End With
        Catch ex As Exception

        Finally

        End Try
    End Sub

#Region "Funciones Comisiones"

    Public Sub CargaComboMeses(ByRef meComboBox As C1.Win.C1List.C1Combo, Optional ByVal strTransaccion As String = "", Optional ByVal strCodigoMoneda As String = "", Optional ByVal strEstadoMoneda As String = "", Optional ByVal bIfBlank As Boolean = True)
        Dim lobjMoneda As ClsMoneda = New ClsMoneda
        Dim DS_General As New DataSet
        Dim MonedaTemp As DataRow
        Dim ltxtColumnas As String
        Dim ltxtResultadoTransaccion As String = Nothing

        Try
            ltxtColumnas = "MONEDA_CODIGO,MONEDA_DESCRIPCION"
            '...Limpia Combo
            meComboBox.ClearItems()
            meComboBox.Text = ""
            '...Inicializa Data Set Combo
            DS_General = Nothing
            DS_General = lobjMoneda.Moneda_Ver(strCodigoMoneda, ltxtColumnas, strEstadoMoneda, ltxtResultadoTransaccion)

            If ltxtResultadoTransaccion.ToUpper = "OK" Then
                '+ Si se indico que acepta blanco se incluye un registro en blanco en el combo
                If bIfBlank Then
                    MonedaTemp = DS_General.Tables("Moneda").NewRow()
                    MonedaTemp("MONEDA_DESCRIPCION") = ""
                    MonedaTemp("MONEDA_CODIGO") = ""
                    DS_General.Tables("Moneda").Rows.Add(MonedaTemp)
                    DS_General.AcceptChanges()
                End If

                '...Asigna Data Source
                meComboBox.AddItem(";")
                meComboBox.DataSource = DS_General.Tables("Moneda")
                meComboBox.Sort(0, C1.Win.C1List.SortDirEnum.ASC)
                '...Ordena y visibilidad de columnas del combo
                meComboBox.Columns(0).DataField = "MONEDA_DESCRIPCION"
                meComboBox.Splits(0).DisplayColumns(0).Visible = True

                meComboBox.Columns(1).DataField = "MONEDA_CODIGO"
                meComboBox.Splits(0).DisplayColumns(1).Visible = False

            End If
        Catch ex As Exception
            ltxtResultadoTransaccion = ex.Message
        Finally
            If ltxtResultadoTransaccion <> "OK" Then
                MsgBox("Error al cargar combo monedas : " & ltxtResultadoTransaccion, MsgBoxStyle.Information, gtxtNombreSistema & strTransaccion)
            End If
        End Try
    End Sub

    Public Sub gSubCargaComboTipoValorCalculo( _
           ByRef objetoControl As C1.Win.C1List.C1Combo)
        Try
            objetoControl.ComboStyle = ComboStyleEnum.DropdownList
            objetoControl.DataMode = DataModeEnum.AddItem
            objetoControl.ClearItems()
            objetoControl.AddItem("MERCADO;M")
            objetoControl.AddItem("COMPRA;C")
            objetoControl.Splits(0).DisplayColumns(0).Visible = True
            objetoControl.Splits(0).DisplayColumns(1).Visible = False
            objetoControl.SelectedIndex = -1
        Catch ex As Exception
        End Try
    End Sub

    Public Sub gSubCargaComboModalidadCalculo( _
            ByRef objetoControl As C1.Win.C1List.C1Combo)
        Try
            objetoControl.ComboStyle = ComboStyleEnum.DropdownList
            objetoControl.DataMode = DataModeEnum.AddItem
            objetoControl.ClearItems()
            objetoControl.AddItem("SIMPLE;S")
            objetoControl.AddItem("COMPUESTA;C")
            objetoControl.Splits(0).DisplayColumns(0).Visible = True
            objetoControl.Splits(0).DisplayColumns(1).Visible = False
            objetoControl.SelectedIndex = -1
        Catch ex As Exception
        End Try
    End Sub

    Public Sub gSubCargaComboTipoSeleccion( _
        ByRef objetoControl As C1.Win.C1List.C1Combo)
        Try
            objetoControl.ComboStyle = ComboStyleEnum.DropdownList
            objetoControl.DataMode = DataModeEnum.AddItem
            objetoControl.ClearItems()
            objetoControl.AddItem("CUENTAS;C")
            objetoControl.AddItem("CLIENTES;R")
            objetoControl.AddItem("GRUPOS;G")
            objetoControl.Splits(0).DisplayColumns(0).Visible = True
            objetoControl.Splits(0).DisplayColumns(1).Visible = False
            objetoControl.SelectedIndex = 0
        Catch ex As Exception
        End Try
    End Sub

    Public Sub CargaComboMonedasCobroMinimo(ByRef meComboBox As C1.Win.C1List.C1Combo)
        Dim lobjMoneda As ClsMoneda = New ClsMoneda
        Dim DS_General As New DataSet
        Dim ltxtColumnas As String
        Dim ltxtResultadoTransaccion As String = Nothing
        Dim strTransaccion As String = ""

        Try
            ltxtColumnas = "MONEDA_CODIGO,MONEDA_DESCRIPCION,MONEDA_DECIMALES"
            '...Limpia Combo
            meComboBox.ClearItems()
            meComboBox.Text = ""
            '...Inicializa Data Set Combo
            DS_General = Nothing
            DS_General = lobjMoneda.Moneda_Ver("", ltxtColumnas, "VIG", ltxtResultadoTransaccion)

            If ltxtResultadoTransaccion.ToUpper = "OK" Then
                '+ Si se indico que acepta blanco se incluye un registro en blanco en el combo
                '...Asigna Data Source
                meComboBox.AddItem(";")
                meComboBox.DataSource = DS_General.Tables("Moneda")
                meComboBox.Sort(0, C1.Win.C1List.SortDirEnum.ASC)
                '...Ordena y visibilidad de columnas del combo
                meComboBox.Columns(0).DataField = "MONEDA_DESCRIPCION"
                meComboBox.Splits(0).DisplayColumns(0).Visible = True

                meComboBox.Columns(1).DataField = "MONEDA_CODIGO"
                meComboBox.Splits(0).DisplayColumns(1).Visible = False

                meComboBox.Columns(2).DataField = "MONEDA_DECIMALES"
                meComboBox.Splits(0).DisplayColumns(2).Visible = False

            End If
        Catch ex As Exception
            ltxtResultadoTransaccion = ex.Message
        Finally
            If ltxtResultadoTransaccion <> "OK" Then
                MsgBox("Error al cargar combo monedas : " & ltxtResultadoTransaccion, MsgBoxStyle.Information, gtxtNombreSistema & strTransaccion)
            End If
        End Try
    End Sub

    Public Sub CargaCombo_tipo_generacion(ByRef meComboBox As C1.Win.C1List.C1Combo, _
                                          ByVal llngid_tipo_generacion As Long, _
                                 Optional ByVal bIsBlank As Boolean = True)

        Dim lobjComisionDet As ClsComisionDetalle = New ClsComisionDetalle
        Dim ds_tipo_generacion As New DataSet
        Dim lstrColumnas As String = ""
        Dim lstrRetorno As String = ""

        Try
            meComboBox.ClearItems()
            ds_tipo_generacion = Nothing
            ds_tipo_generacion = lobjComisionDet.tipo_generacion_Busca(lstrColumnas, llngid_tipo_generacion, lstrRetorno)
            If ds_tipo_generacion.Tables(0).Rows.Count > 0 Then
                'objetoControl.ComboStyle = ComboStyleEnum.DropdownList
                'objetoControl.DataMode = DataModeEnum.AddItem
                If bIsBlank Then
                    Dim TipoEntidadTemp As DataRow = ds_tipo_generacion.Tables("tipo_generacion").NewRow()
                    TipoEntidadTemp("DSC_TIPO_GENERACION") = ""
                    TipoEntidadTemp("ID_TIPO_GENERACION") = 0
                    TipoEntidadTemp("COD_TIPO_GENERACION") = ""

                    ds_tipo_generacion.Tables("tipo_generacion").Rows.Add(TipoEntidadTemp)
                    ds_tipo_generacion.AcceptChanges()
                End If

                '...Asigna Data Source
                meComboBox.DataSource = ds_tipo_generacion.Tables("tipo_generacion")
                'objetoControl.Sort(0, C1.Win.C1List.SortDirEnum.ASC)

                meComboBox.Columns(0).DataField = "DSC_TIPO_GENERACION"
                meComboBox.Columns(1).DataField = "ID_TIPO_GENERACION"
                meComboBox.Columns(2).DataField = "COD_TIPO_GENERACION"

                meComboBox.Splits(0).DisplayColumns(0).Visible = True
                meComboBox.Splits(0).DisplayColumns(1).Visible = False
                meComboBox.Splits(0).DisplayColumns(2).Visible = False

                meComboBox.SelectedIndex = -1
            End If
        Catch ex As Exception

        End Try
    End Sub

    Public Sub CargaCombo_tipo_mov_comision(ByRef objetoControl As C1.Win.C1List.C1Combo, _
                                            ByVal lngid_tipo_mov_comision As Long, _
                                   Optional ByVal bIsBlank As Boolean = True)

        Dim lobjComisionDet As ClsComisionDetalle = New ClsComisionDetalle
        Dim lstrColumnas As String = ""
        Dim lstrRetorno As String = ""

        Dim ds_tipo_mov_comision As DataSet = New DataSet

        Try
            objetoControl.ClearItems()
            ds_tipo_mov_comision = lobjComisionDet.tipo_mov_comision_Busca(lstrColumnas, lngid_tipo_mov_comision, lstrRetorno)
            If ds_tipo_mov_comision.Tables(0).Rows.Count > 0 Then

                'objetoControl.ComboStyle = ComboStyleEnum.DropdownList
                'objetoControl.DataMode = DataModeEnum.AddItem

                If bIsBlank Then
                    Dim TipoEntidadTemp As DataRow = ds_tipo_mov_comision.Tables("tipo_mov_comision").NewRow()
                    TipoEntidadTemp("DSC_TIPO_MOV_COMISION") = ""
                    TipoEntidadTemp("ID_TIPO_MOV_COMISION") = 0
                    TipoEntidadTemp("COD_TIPO_MOV_COMISION") = ""
                    TipoEntidadTemp("COD_ORIGEN_MOV_CAJA") = ""
                    ds_tipo_mov_comision.Tables("tipo_mov_comision").Rows.Add(TipoEntidadTemp)
                    ds_tipo_mov_comision.AcceptChanges()
                End If

                '...Asigna Data Source
                objetoControl.DataSource = ds_tipo_mov_comision.Tables("tipo_mov_comision")
                'objetoControl.Sort(0, C1.Win.C1List.SortDirEnum.ASC)

                objetoControl.Columns(0).DataField = "DSC_TIPO_MOV_COMISION"
                objetoControl.Columns(1).DataField = "ID_TIPO_MOV_COMISION"
                objetoControl.Columns(2).DataField = "COD_TIPO_MOV_COMISION"
                objetoControl.Columns(3).DataField = "COD_ORIGEN_MOV_CAJA"

                objetoControl.Splits(0).DisplayColumns(0).Visible = True
                objetoControl.Splits(0).DisplayColumns(1).Visible = False
                objetoControl.Splits(0).DisplayColumns(2).Visible = False
                objetoControl.Splits(0).DisplayColumns(3).Visible = False
                objetoControl.SelectedIndex = -1
            End If
        Catch ex As Exception

        End Try

    End Sub

    Public Sub CargaCombo_ConceptosComision(ByRef objetoControl As C1.Win.C1List.C1Combo, _
                                         Optional ByVal strTipoComisionAdministracion As String = vbNullString, _
                                         Optional ByVal bIsBlank As Boolean = True)

        Dim lobjConceptoComm As ClsConceptoComision = New ClsConceptoComision
        Dim lstrColumnas As String = ""
        Dim lstrRetorno As String = ""

        Dim DS_ConceptoComision As DataSet = New DataSet

        Try
            objetoControl.ClearItems()
            DS_ConceptoComision = lobjConceptoComm.ConceptoComision_Buscar(gstrTipoComisionAdministracion, 0, 0, "", lstrColumnas, lstrRetorno)

            If DS_ConceptoComision.Tables(0).Rows.Count > 0 Then

                'objetoControl.ComboStyle = ComboStyleEnum.DropdownList
                'objetoControl.DataMode = DataModeEnum.AddItem

                If bIsBlank Then
                    Dim TipoEntidadTemp As DataRow = DS_ConceptoComision.Tables("tipo_mov_comision").NewRow()

                    TipoEntidadTemp("DSC_CONCEPTO_COMISION") = ""
                    TipoEntidadTemp("ID_CONCEPTO_COMISION") = ""
                    TipoEntidadTemp("ID_TIPO_COMISION") = ""
                    TipoEntidadTemp("DSC_TIPO_COMISION") = ""
                    TipoEntidadTemp("COD_TIPO_CONCEPTO") = ""

                    DS_ConceptoComision.Tables("ConceptoComision").Rows.Add(TipoEntidadTemp)
                    DS_ConceptoComision.AcceptChanges()
                End If

                '...Asigna Data Source
                objetoControl.DataSource = DS_ConceptoComision.Tables("ConceptoComision")
                'objetoControl.Sort(0, C1.Win.C1List.SortDirEnum.ASC)

                objetoControl.Columns(0).DataField = "DSC_CONCEPTO_COMISION"
                objetoControl.Columns(1).DataField = "ID_CONCEPTO_COMISION"
                objetoControl.Columns(2).DataField = "ID_TIPO_COMISION"
                objetoControl.Columns(3).DataField = "DSC_TIPO_COMISION"
                objetoControl.Columns(4).DataField = "COD_TIPO_CONCEPTO"

                objetoControl.Splits(0).DisplayColumns(0).Visible = True
                objetoControl.Splits(0).DisplayColumns(1).Visible = False
                objetoControl.Splits(0).DisplayColumns(2).Visible = False
                objetoControl.Splits(0).DisplayColumns(3).Visible = False
                objetoControl.Splits(0).DisplayColumns(4).Visible = False
                objetoControl.SelectedIndex = -1
            End If
        Catch ex As Exception

        End Try

    End Sub

#End Region



    Public Sub gSubCargaComboConsolidadas( _
           ByRef objetoControl As C1.Win.C1List.C1Combo)
        Try
            objetoControl.ComboStyle = ComboStyleEnum.DropdownList
            objetoControl.DataMode = DataModeEnum.AddItem
            objetoControl.ClearItems()
            objetoControl.AddItem("RUT;R")
            objetoControl.AddItem("CUENTA;C")
            objetoControl.Splits(0).DisplayColumns(0).Visible = True
            objetoControl.Splits(0).DisplayColumns(1).Visible = False
            objetoControl.SelectedIndex = -1
        Catch ex As Exception
        End Try
    End Sub

    Public Sub gSubCargaComboEstadosFactor( _
           ByRef objetoControl As C1.Win.C1List.C1Combo)
        Try
            objetoControl.ComboStyle = ComboStyleEnum.DropdownList
            objetoControl.DataMode = DataModeEnum.AddItem
            objetoControl.ClearItems()
            objetoControl.AddItem("TODOS;ALL")
            objetoControl.AddItem("VIGENTES;VIG")
            objetoControl.AddItem("ANULADOS;ANU")
            objetoControl.Splits(0).DisplayColumns(0).Visible = True
            objetoControl.Splits(0).DisplayColumns(1).Visible = False
            objetoControl.Width = 80
            objetoControl.SelectedIndex = -1
        Catch ex As Exception
        End Try
    End Sub

#Region "Funciones Comunes Juan Pablo Antillanca M."
    ''' <summary>
    ''' Configura el formato de entrada y visualizaci�n de TextBox de ComponentOne
    ''' Ej:
    '''   Para N�meros enteros;
    '''     SetC1TextBox(Txt_LimiteInicial, C1.Win.C1Input.FormatTypeEnum.Integer, "#,##0", 6, 0, 999999)
    '''   Para porcentajes %
    '''     SetC1TextBox(Txt_LimiteFinal, C1.Win.C1Input.FormatTypeEnum.CustomFormat, "##0.00", 6, 0, 100)
    ''' </summary>
    ''' <param name="textBox">TextBox de ComponentOne</param>
    ''' <param name="type">Tipo del valor, Entero, Decimal, etc.</param>
    ''' <param name="format">Formato de visualizacion, ej: #,###, #.##, etc</param>
    ''' <param name="maxLength">M�xima cantidad de caracteres</param>
    ''' <param name="minValue">Valor M�nimo a validar</param>
    ''' <param name="maxValue">Valor M�ximo a validar</param>
    ''' <remarks></remarks>
    Public Sub SetC1TextBox(ByRef textBox As C1.Win.C1Input.C1TextBox, _
                                ByRef type As C1.Win.C1Input.FormatTypeEnum, _
                                ByVal format As String, _
                                ByVal maxLength As Integer, _
                                ByVal minValue As Double, _
                                ByVal maxValue As Double, _
                                Optional ByVal flg As Boolean = False)
        If flg = True Then Exit Sub
        textBox.DataType = GetType(System.Decimal)
        textBox.EditFormat.FormatType = type
        textBox.EditFormat.CustomFormat = format
        textBox.CustomFormat = format
        textBox.DisplayFormat.CustomFormat = format
        textBox.MaxLength = maxLength
        textBox.PostValidation.Intervals.Add(New C1.Win.C1Input.ValueInterval(minValue, maxValue, True, True))
        textBox.PostValidation.ErrorMessage = String.Format("El valor debe estar entre {0} y {1}.", minValue, maxValue)
    End Sub

    ''' <summary>
    ''' Genera el Footer para la TrueDBGrid y columna dada.
    ''' </summary>
    ''' <param name="tdgGrilla">Grilla en la que se genera el Footer</param>
    ''' <param name="nombreColumna">Nombre de la Columna a agregar Footer</param>
    ''' <remarks></remarks>
    Public Sub GeneraFooter(ByVal tdgGrilla As C1.Win.C1TrueDBGrid.C1TrueDBGrid, ByVal nombreColumna As String)
        tdgGrilla.ColumnFooters = True
        Dim total As Object = DBNull.Value
        If Not tdgGrilla.DataSource Is Nothing AndAlso tdgGrilla.RowCount > 0 Then
            total = tdgGrilla.DataSource.compute("SUM(" & nombreColumna & ")", "")
        Else
            total = 0
        End If
        tdgGrilla.Columns(nombreColumna).FooterText = IIf(Not total Is DBNull.Value, Format(total, "#,##0.00"), 0)
        With tdgGrilla.Splits(0).DisplayColumns(nombreColumna)
            .AutoSize()
            If .Width <= 120 Then
                .Width = 120
            End If
            .Style.Padding.Right = 2
            .Style.Padding.Left = 2
            .FooterStyle.HorizontalAlignment = C1.Win.C1TrueDBGrid.AlignHorzEnum.Far
        End With
    End Sub

    ''' <summary>
    ''' Obtiene una tabla con un nombre seg�n las columnas requeridas, y 
    ''' realizando un eliminaci�n de datos repetidos si asi se requiere.
    ''' </summary>
    ''' <param name="dtTabla">Tabla de datos a procesar</param>
    ''' <param name="nombreTabla">nombre para la nueva tabla</param>
    ''' <param name="noRepetirDatos">elimina los repetidos si es True</param>
    ''' <param name="columnas">Columnas a obtener "COLUMNA1,COLUMNA2,....ETC"</param>
    ''' <returns>La tabla con las columnas requeridas.</returns>
    ''' <remarks></remarks>
    Public Function ObtenerDataTable(ByVal dtTabla As DataTable, _
                                     ByVal nombreTabla As String, _
                                     ByVal noRepetirDatos As Boolean, _
                                     ByVal columnas As String) As DataTable
        columnas = columnas.Replace(" ", "")
        Dim arrColumnas As String() = columnas.Split(",")
        Dim dvTmp As New DataView(dtTabla)
        Return dvTmp.ToTable(nombreTabla, noRepetirDatos, arrColumnas)
    End Function

    ''' <summary>
    ''' Nuevo metodo de exportaci�n de excel que optimiza el performance de la generaci�n del archivo.
    ''' </summary>
    Public Function GeneraExcelPorHojas(ByRef dtbDatosReporte As DataTable, _
                                        ByRef appExcel As Excel.Application, _
                                        ByRef libroExcel As Excel.Workbook, _
                                        ByRef directorioSeleccionado As String, _
                                        ByRef nombreReporteExcel As String, _
                                        ByVal tituloReporte As String, _
                                        ByVal columnas As ClsColumnaExcel(), _
                                        ByVal filaTitulos As Integer, _
                                        ByVal fechaInicioProceso As Date, _
                                        ByVal nombreHoja As String, _
                                        Optional ByVal numeroHoja As Integer = 1, _
                                        Optional ByVal cantidadHojas As Integer = 1, _
                                        Optional ByVal subTituloReporte As String = "", _
                                        Optional ByVal colinicio0 As String = "", _
                                        Optional ByVal MostrarLogo As String = "S" _
                                        ) As Boolean

        Dim proceso As Process() = Nothing
        Dim hojaExcel As Excel.Worksheet
        Dim objGuardaArchivos As New SaveFileDialog
        Dim estaExcelAbierto As Boolean = False

        Try
            ' si pasa por primera vez, buscara la direcion 
            If numeroHoja = 1 Then
                directorioSeleccionado = CurDir()
                If directorioSeleccionado = "" Then
                    MsgBox("Error al intentar leer la unidad de disco...", MsgBoxStyle.Critical, gtxtNombreSistema)
                    Exit Function
                End If
                directorioSeleccionado = directorioSeleccionado.Substring(0, 3)
                objGuardaArchivos.FileName = nombreReporteExcel
                objGuardaArchivos.AddExtension = True
                objGuardaArchivos.CheckPathExists = True
                objGuardaArchivos.OverwritePrompt = True
                objGuardaArchivos.SupportMultiDottedExtensions = True
                objGuardaArchivos.Filter = "Archivos Excel(*.xls;*.xlsx)|*.xls;*.xlsx"

                If Val(appExcel.Version) < 12 Then
                    If dtbDatosReporte.Rows.Count < 65501 Then
                        objGuardaArchivos.Filter = "Excel 97-2003|*.xls"
                    Else
                        MsgBox("Para mas de 65500 Lineas necesita Office 2007 o superior...", MsgBoxStyle.Critical, gtxtNombreSistema)
                        Exit Function
                    End If
                Else
                    objGuardaArchivos.Filter = "Excel|*.xlsx|Excel 97-2003|*.xls"
                End If

                If objGuardaArchivos.ShowDialog() = DialogResult.OK Then
                    directorioSeleccionado = objGuardaArchivos.FileName.ToString
                Else
                    '                    Me.Cursor = Cursors.Default
                    Exit Function
                End If

            End If

            'preparacion de array
            Dim fila, col As Long
            Dim filasReporte As Long = dtbDatosReporte.Rows.Count
            Dim columnasReporte As Long = columnas.Count
            Dim datosReporte As Object(,)
            ReDim datosReporte(filasReporte, columnasReporte)

            Dim titulosReporte As Object()
            ReDim titulosReporte(columnasReporte)

            'array de titulos provenientes de dataset
            Dim i As Integer = 0
            For Each colExcel As ClsColumnaExcel In columnas
                titulosReporte(i) = colExcel.TituloColumna
                i += 1
            Next

            ' array de la informacion de dataset 
            fila = 0
            For Each filaReporte As DataRow In dtbDatosReporte.Rows
                col = 0
                For Each colExcel As ClsColumnaExcel In columnas
                    datosReporte(fila, col) = filaReporte(colExcel.ColumnaVinculada)
                    col += 1
                Next
                fila += 1
            Next

            'salida de excel
            Dim Filainicio As Long

            If cantidadHojas > 1 Then
                If numeroHoja = 1 Then
                    'appExcel = CreateObject("Excel.Application")
                    appExcel.DisplayAlerts = False
                    'appExcel.visible = True
                    libroExcel = appExcel.Workbooks.Add
                    libroExcel.Windows(1).Caption = nombreReporteExcel
                    hojaExcel = libroExcel.Worksheets.Item(numeroHoja)
                    hojaExcel.Name = Mid(nombreHoja, 1, 30)
                    hojaExcel.Application.ActiveWindow.DisplayGridlines = False
                    hojaExcel.Activate()
                Else
                    'libroExcel = appExcel.Workbooks.Open(NombreDoc)
                    If numeroHoja >= 2 Then
                        hojaExcel = libroExcel.Worksheets.Add(After:=libroExcel.Worksheets(libroExcel.Worksheets.Count))
                    End If
                    hojaExcel = libroExcel.Worksheets.Item(numeroHoja)
                    hojaExcel.Name = Mid(nombreHoja, 1, 30)
                    hojaExcel.Activate()
                    hojaExcel.Application.ActiveWindow.DisplayGridlines = False
                End If
            Else
                'appExcel = CreateObject("Excel.Application")
                appExcel.DisplayAlerts = False
                'appExcel.Visible = True
                libroExcel = appExcel.Workbooks.Add
                libroExcel.Windows(1).Caption = nombreReporteExcel
                'libroExcel = appExcel.Workbooks.Ope(directorioSeleccionado, UpdateLinks:=False, ReadOnly:=False)
                hojaExcel = libroExcel.Worksheets.Item(numeroHoja)
                hojaExcel.Name = Mid(nombreHoja, 1, 30)
                hojaExcel.Application.ActiveWindow.DisplayGridlines = False
                hojaExcel.Activate()
            End If

            Dim rutaLogo As String = gPathApp & "\" & IIf(gLogoCliente = "" Or gLogoCliente = "CREASYS", "Recursos Comunes", "Recursos_" & gLogoCliente) & "\Recursos\Img_Logo.gif"
            Try
                If MostrarLogo = "S" Then
                    hojaExcel.Shapes.AddPicture(rutaLogo, Microsoft.Office.Core.MsoTriState.msoFalse, Microsoft.Office.Core.MsoTriState.msoCTrue, 60, 0, 77, 21)
                End If
            Catch exx As Exception
            End Try

            Dim columnaInicio As String

            If colinicio0 = "" Then
                columnaInicio = "B"
            Else
                columnaInicio = colinicio0
            End If

            Dim colorInterior As Color = Color.FromArgb(31, 73, 125)
            Dim rangoExcel, columnaActual, ultimaColumnaReporte As String

            ultimaColumnaReporte = (Char.ConvertFromUtf32(Char.ConvertToUtf32(columnaInicio, 0) + (CInt(columnasReporte - 1)))).ToString
            'asignar titulos
            hojaExcel.Range("A1").RowHeight = 22
            hojaExcel.Range(String.Format("{0}{1}", columnaInicio, 1)).RowHeight = 22

            Filainicio = filaTitulos + 1
            rangoExcel = String.Format("{0}{2}:{1}{2}", columnaInicio, ultimaColumnaReporte, Filainicio)
            hojaExcel.Range(rangoExcel).Merge()
            hojaExcel.Range(rangoExcel).Value = tituloReporte
            hojaExcel.Range(rangoExcel).Font.Bold = True
            hojaExcel.Range(rangoExcel).HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter

            rangoExcel = String.Format("{0}{1}:{0}{1}", columnaInicio, Filainicio + 1)
            hojaExcel.Range(rangoExcel).Value = "Negocio : " & gStrAbrNegocio
            hojaExcel.Range(rangoExcel).Font.Bold = True
            If subTituloReporte <> "" Then
                Filainicio = Filainicio + 2
                rangoExcel = String.Format("{0}{1}:{0}{1}", columnaInicio, Filainicio)
                hojaExcel.Range(rangoExcel).Value = subTituloReporte
                hojaExcel.Range(rangoExcel).Font.Bold = True
            End If

            'asignar cabeceras de tabla
            Filainicio = Filainicio + 2
            hojaExcel.Range(columnaInicio + (Filainicio).ToString).Resize(filasReporte, columnasReporte).Value = titulosReporte

            'asignar datos de tabla
            Filainicio = Filainicio + 1
            i = 0
            For Each colExcel As ClsColumnaExcel In columnas
                columnaActual = (Char.ConvertFromUtf32(Char.ConvertToUtf32(columnaInicio, 0) + (CInt(i)))).ToString
                rangoExcel = String.Format("{0}{1}:{0}{2}", columnaActual, Filainicio, filasReporte + Filainicio + 1)
                hojaExcel.Range(rangoExcel).NumberFormat = colExcel.FormatoColumna
                hojaExcel.Range(rangoExcel).Columns.AutoFit()
                i += 1
            Next
            hojaExcel.Range(columnaInicio + (Filainicio).ToString).Resize(filasReporte, columnasReporte).Value = datosReporte

            'formato de excel
            Filainicio = Filainicio - 1
            rangoExcel = columnaInicio & (Filainicio).ToString & ":"
            rangoExcel = rangoExcel & ultimaColumnaReporte & (Filainicio).ToString
            With hojaExcel.Range(rangoExcel)
                .RowHeight = 21 'Cambia el alto de celda 
                .Font.Bold = True 'Pone negritas al font
                .HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter 'Alinea el texto en el centro de la celda 
                .VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter 'Alinea el texto en el fonto de la celda                
                .NumberFormat = "@"
                .Interior.Color = ColorTranslator.ToOle(colorInterior)
                .Font.Color = ColorTranslator.ToOle(Color.White)
            End With

            rangoExcel = columnaInicio + (Filainicio).ToString + ":"
            rangoExcel = rangoExcel + (Char.ConvertFromUtf32(Char.ConvertToUtf32(columnaInicio, 0) + (CInt(columnasReporte - 1)))).ToString + (filasReporte + Filainicio + 1).ToString
            hojaExcel.Range(rangoExcel).Font.Size = 10
            hojaExcel.Range(rangoExcel).AutoFilter(Field:=1, Operator:=Excel.XlAutoFilterOperator.xlFilterValues)
            hojaExcel.Range(rangoExcel).Columns.AutoFit()
            appExcel.ActiveWindow.Zoom = 85
            'seccion para guardar
            Try
                appExcel.DisplayAlerts = False
                hojaExcel = Nothing
                If Dir(directorioSeleccionado) <> "" Then
                    Kill(directorioSeleccionado)
                End If

                If numeroHoja = cantidadHojas Then
                    Dim FileFormatNum As Integer
                    'Determine the Excel version and file extension/format
                    If Val(appExcel.Version) < 12 Then
                        'You use Excel 97-2003 FileExtStr = ".xls" :
                        FileFormatNum = -4143
                        libroExcel.SaveAs(directorioSeleccionado, Excel.XlFileFormat.xlAddIn)
                    Else
                        'use Excel 2007-2013
                        Select Case LCase(Right(directorioSeleccionado, Len(directorioSeleccionado) - InStrRev(directorioSeleccionado, ".", , 1)))
                            Case "xls" : FileFormatNum = 56
                            Case "xlsx" : FileFormatNum = 51
                            Case "xlsm" : FileFormatNum = 52
                            Case "xlsb" : FileFormatNum = 50
                            Case Else : FileFormatNum = 0
                        End Select
                        libroExcel.SaveAs(directorioSeleccionado, FileFormatNum)
                    End If
                    libroExcel.Close()
                    libroExcel = Nothing
                    appExcel.Workbooks.Close()
                    appExcel.Quit()
                    appExcel = Nothing
                End If

                'para visualizar el excel creado
                If cantidadHojas = 1 Or (cantidadHojas > 1 And numeroHoja = cantidadHojas) Then
                    GC.Collect()
                    proceso = Process.GetProcessesByName("EXCEL")
                    For Each opro As Process In proceso
                        If opro.StartTime >= fechaInicioProceso Then
                            opro.Kill()
                        End If
                    Next
                    If Not estaExcelAbierto Then
                        Process.Start(directorioSeleccionado)
                    End If
                    MsgBox("Exportacion Excel Terminada.", MsgBoxStyle.Information)
                End If
                Return True
            Catch ex As Exception
                MsgBox("Error al generar excel" & vbCrLf & "Por favor verifique que el archivo no est� abierto", MsgBoxStyle.Critical, gtxtNombreSistema & " -  Cobro Comisiones por Administraci�n")
                estaExcelAbierto = True
                proceso = Process.GetProcessesByName("EXCEL")
                For Each opro As Process In proceso
                    If opro.StartTime >= fechaInicioProceso Then
                        opro.Kill()
                    End If
                Next
                Return False
            End Try

        Catch ex As Exception
            Dim lstrError As String = ex.Message
            appExcel = Nothing
            libroExcel = Nothing
            hojaExcel = Nothing

            If ex.Message = "Excepci�n de HRESULT: 0x800A03EC" Then
                lstrError = "Sobrepasa el m�ximo de filasReporte de su versi�n de excel"
            End If
            MsgBox("Error al crear la exportacion a Excel debido a: " & lstrError, MsgBoxStyle.Information, gtxtNombreSistema)
            proceso = Process.GetProcessesByName("EXCEL")
            For Each opro As Process In proceso
                If opro.StartTime >= fechaInicioProceso Then
                    opro.Kill()
                End If
            Next
            Return False
        End Try

    End Function

    ''' <summary>
    ''' Funcion que permite decidir si eliminar o no un proceso en ejecuci�n.
    ''' </summary>
    ''' <remarks></remarks>
    Public Function TerminaProcesosActivos(ByVal procesoBuscado As Thread) As Boolean
        Dim nombreProcesos As New List(Of String)()
        If Not procesoBuscado Is Nothing AndAlso gListaProcesos.Exists(Function(proceso As Thread) proceso.ManagedThreadId = procesoBuscado.ManagedThreadId) Then
            If procesoBuscado.IsAlive Then
                nombreProcesos.Add(procesoBuscado.Name)
            Else
                gListaProcesos.Remove(procesoBuscado)
            End If
        Else
            For Each proceso As Thread In gListaProcesos.ToList()
                If proceso.IsAlive Then
                    nombreProcesos.Add(proceso.Name)
                End If
                If Not proceso.IsAlive Then
                    Dim procesoMuerto As Thread = proceso
                    gListaProcesos.Remove(procesoMuerto)
                End If
            Next
        End If

        Dim texto As String = "El(Los) siguiente(s) proceso(s) est�(n) en ejecuci�n:" & vbNewLine & vbNewLine
        If nombreProcesos.Count = 0 Then
            Return True
        Else
            For Each nombre As String In nombreProcesos
                texto = texto & nombre & vbNewLine
            Next
        End If
        texto = texto & vbNewLine
        texto = texto & "�Desea terminar los procesos en ejecuci�n?"

        If MessageBox.Show(texto, _
                           "Gpi+ L�mites", _
                           MessageBoxButtons.YesNo, _
                           MessageBoxIcon.Warning, _
                           MessageBoxDefaultButton.Button2) = DialogResult.No Then
            Return False
        Else
            If Not procesoBuscado Is Nothing AndAlso gListaProcesos.Exists(Function(proceso As Thread) proceso.ManagedThreadId = procesoBuscado.ManagedThreadId) Then
                If procesoBuscado.IsAlive Then
                    procesoBuscado.Abort()
                    procesoBuscado.Join()
                    gListaProcesos.Remove(procesoBuscado)
                End If
            Else
                For Each proceso As Thread In gListaProcesos.ToList()
                    If proceso.IsAlive Then
                        proceso.Abort()
                        proceso.Join()
                    End If
                Next
                gListaProcesos.Clear()
            End If
            Return True
        End If

    End Function

    ''' <summary>
    ''' Recupera solo el valor de un par�metro dado el c�digo, en caso de que haya 
    ''' un error o no venga el valor del par�metro se lanzara una excepci�n.
    ''' </summary>
    ''' <param name="codigo">c�digo del par�metro.</param>
    ''' <returns></returns>
    Public Function ObtenerValorParametro(codigo As String) As String
        Dim parametros As New ClsParametrosSistema
        Dim resultado = String.Empty
        Dim valorParametro = String.Empty
        Dim ds = parametros.RetornarValorParametro(codigo, resultado)
        If resultado <> "OK" Then
            Throw New Exception(resultado)
        End If
        If ds Is Nothing Or ds.Tables(0).Rows.Count = 0 Then
            Throw New Exception(String.Format("El Par�metro {0} NO fue encontrado en el sistema, comun�quese con el administrador.", codigo))
        Else
            valorParametro = ds.Tables(0).Rows(0).Item("VALOR")
            If String.IsNullOrEmpty(valorParametro.Trim) Then
                Throw New Exception(String.Format("El Par�metro {0} NO tiene valor asignado, comun�quese con el administrador.", codigo))
            End If
        End If
        Return valorParametro
    End Function

    ''' <summary>
    ''' Guarda una imagen desde los recursos de la aplicaci�n.
    ''' </summary>
    ''' <param name="imageName"></param>
    ''' <returns></returns>
    Public Function SaveImage(imageName As String) As String
        Dim pathImage = String.Format("{0}\{1}", Application.StartupPath, imageName)
        If Not File.Exists(pathImage) Then
            Dim nameResource = imageName.
                Replace(".png", "").
                Replace(".bmp", "").
                Replace(".jpg", "")
            Dim image = My.Resources.ResourceManager.GetObject(nameResource)
            If Not image Is Nothing Then
                image.Save(pathImage, Imaging.ImageFormat.Png)
            End If
        End If
        Return pathImage
    End Function

    ''' <summary>
    ''' Reemplaza caracteres especiales para envio de correos hacia outlook.
    ''' </summary>
    ''' <param name="text"></param>
    ''' <returns></returns>
    Public Function ReplaceSpecialCharacter(text As String) As String
        Return text.
            Replace("<", "&lt;").
            Replace(">", "&gt;").
            Replace("@", "&#64;")
    End Function
#End Region

#Region "Filtros Genericos Grillas"
    Public Sub FiltroGrilla(ByVal Grilla As C1.Win.C1TrueDBGrid.C1TrueDBGrid, ByVal sender As C1.Win.C1TrueDBGrid.C1TrueDBGrid, ByVal e As System.EventArgs)
        Dim sb As New System.Text.StringBuilder
        Dim dc As C1.Win.C1TrueDBGrid.C1DataColumn

        Try
            sender.SelectedRows.Clear()
            For Each dc In sender.Columns
                If dc.FilterText.Trim.Length > 0 Then
                    If sb.Length > 0 Then
                        sb.Append(" AND ")
                    End If
                    sb.Append(gstrFiltro_X_TipoColumna(dc))
                End If
            Next dc
            '... Realiza el filtro sobre el dataset
            sender.DataSource.DefaultView.RowFilter = sb.ToString
            sender.Refresh()

            If sender.RowCount > 0 Then
                sender.SelectedRows.Add(0)
            End If
        Catch ex As Exception
            Debug.Print(ex.Message)
        End Try
    End Sub

    Public Sub FiltroChangeGrillaGenerico(ByVal sender As C1.Win.C1TrueDBGrid.C1TrueDBGrid, ByRef dc As C1.Win.C1TrueDBGrid.C1DataColumn, ByRef sb As System.Text.StringBuilder, ByRef Filtro As String)
        Select Case dc.DataType.ToString.ToUpper
            'Case ""
            '    sb.Append((dc.DataField + " like " + "'%" + dc.FilterText + "%'"))
            'Case ""
            '    sb.Append((dc.DataField + " like " + "'" + dc.FilterText + "%'"))
            Case "SYSTEM.STRING", "SYSTEM.DATETIME", "SYSTEM.BOOLEAN"
                sb.Append(gstrFiltro_X_TipoColumna(dc))
            Case "SYSTEM.DECIMAL", "SYSTEM.INT32", "SYSTEM.DOUBLE" 'n�meros
                Filtro = dc.FilterText
                If gFunFiltroValido(Filtro) Then
                    '  sb.Append((dc.DataField + Filtro))
                    ' sb.Append((dc.DataField + " like " + "'" + Filtro + "%'"))
                    sb.Append(gstrFiltro_X_TipoColumna(dc))
                Else
                    MsgBox("Ingrese solo valores num�ricos para filtrar la columna" + dc.Caption, MsgBoxStyle.Information, gtxtNombreSistema & " ")
                    Exit Sub
                End If
            Case Else
                sb.Append((dc.DataField + " like " + "'%" + dc.FilterText + "%'"))
        End Select
    End Sub

    Public Sub NRegistros(ByVal dtArchivos As DataTable)
        Dim strItems() As String = {"Registros de Cartera", "Cantidad de Archivos"}
        Dim strExtra As String = ""
        Dim intOk As Integer = 0
        Dim intErr As Integer = 0

        '...>variable filtro grillas generico
        'Dim dtArchivos As New DataTable

        Try
            For i As Integer = 0 To dtArchivos.Rows.Count - 1
                If dtArchivos(i)(0) = "OK" Then
                    intOk += 1
                Else
                    intErr += 1
                End If
            Next
            strExtra = " de los que son v�lidos: " & intOk.ToString & " y tienen errores: " & intErr.ToString
        Catch ex As Exception
            Debug.Print("Tdg_Archivos aun no tiene datos: " & ex.Message)
        End Try

        'NRegistros(strItems, strExtra)
    End Sub

    Function ConvertToLetter(ByVal iCol As Integer) As String
        Dim iAlpha As Integer
        Dim iRemainder As Integer
        Dim letra As String = ""
        iAlpha = Int(iCol / 27)
        iRemainder = iCol - (iAlpha * 26)
        If iAlpha > 0 Then
            letra = Chr(iAlpha + 64)
        End If
        If iRemainder > 0 Then
            letra = letra & Chr(iRemainder + 64)
        End If
        Return letra
    End Function

#End Region
End Module
