﻿Public Class ClsCierre
  Dim sProcedimiento As String


    Public Function ProcesoCierre_Ejecutar(ByVal lngNegocio As Long, _
                                         ByVal strFechaCierre As String) As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_ProcesoCierre", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_ProcesoCierre"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pNegocio", SqlDbType.Float, 10).Value = lngNegocio
            SQLCommand.Parameters.Add("pFechaCierre", SqlDbType.Char, 10).Value = strFechaCierre

            '...Resultado
            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error al procesar cierre " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            ProcesoCierre_Ejecutar = strDescError
        End Try
    End Function

    Public Function ProcesoCierre_Verificar(ByVal strFechaDesde As String, _
                                            ByVal strFechaHasta As String, _
                                            ByVal lstrListaC As String, _
                                            ByVal strCodSubClase As String, _
                                            ByVal dblIdInstrumento As Double, _
                                            ByVal dblIdAsesor As Double, _
                                            ByVal strProceso As String, _
                                            ByVal dblIdNegocio As Double, _
                                            ByRef strRetorno As String) As DataSet

        Dim LDS_ProcesoCierre As New DataSet
        Dim LDS_ProcesoCierre_Aux As New DataSet

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        'If gLogoCliente = "SECURITY" Then
        '  sProcedimiento = "Rcp_ProcesoCierre_Verifica_Security"
        'Else
        sProcedimiento = "Rcp_ProcesoCierre_Verifica"
        'End If

        Connect.Abrir()

        Try

            SQLCommand = New SqlClient.SqlCommand(sProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = sProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pFechaDesde", SqlDbType.Char, 10).Value = strFechaDesde
            SQLCommand.Parameters.Add("pFechaHasta", SqlDbType.Char, 10).Value = strFechaHasta
            SQLCommand.Parameters.Add("pListaCuentas", SqlDbType.Text, 600000).Value = lstrListaC
            SQLCommand.Parameters.Add("pCodSubClase", SqlDbType.Char, 15).Value = IIf(strCodSubClase = "", DBNull.Value, strCodSubClase)
            SQLCommand.Parameters.Add("pIdInstrumento", SqlDbType.Float, 10).Value = IIf(dblIdInstrumento = 0, DBNull.Value, dblIdInstrumento)
            SQLCommand.Parameters.Add("pIdAsesor", SqlDbType.Float, 10).Value = IIf(dblIdAsesor = 0, DBNull.Value, dblIdAsesor)
            SQLCommand.Parameters.Add("pProceso", SqlDbType.Char, 1).Value = IIf(strProceso = "", "N", strProceso)
            SQLCommand.Parameters.Add("pNegocio", SqlDbType.Float, 10).Value = dblIdNegocio
            SQLCommand.Parameters.Add("pID_USUARIO", SqlDbType.Float, 10).Value = glngIdUsuario
            SQLCommand.Parameters.Add("pCodEmpresa", SqlDbType.VarChar, 25).Value = gLogoCliente

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_ProcesoCierre)

            LDS_ProcesoCierre_Aux = LDS_ProcesoCierre
            strRetorno = "OK"
            Return LDS_ProcesoCierre_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function FechaUltimoCierreConsultas(ByVal intIdCuenta As Integer, _
                                             ByRef strResultado As String) As String

        Dim LDS_FechaCierre As New DataSet
        Dim LDS_FechaCierre_Aux As New DataSet
        Dim lstrFechaUltimoCierre As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        sProcedimiento = "Rcp_General_BuscarFechaUltimoCierre"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(sProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = sProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Int, 10).Value = IIf(intIdCuenta = 0, DBNull.Value, intIdCuenta)

            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Int, 10).Value = glngNegocioOperacion

            '...Fecha
            Dim ParametroFecha As New SqlClient.SqlParameter("pFechaCierre", SqlDbType.VarChar, 200)
            ParametroFecha.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroFecha)

            '...Resultado
            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_FechaCierre)

            strResultado = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strResultado = "OK" Then
                lstrFechaUltimoCierre = SQLCommand.Parameters("pFechaCierre").Value
            End If


            Return lstrFechaUltimoCierre

        Catch ex As Exception
            lstrFechaUltimoCierre = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function VerificaInterfazTesoreria(ByRef strResultado As String) As Double

        Dim LDS_FechaCierre As New DataSet
        Dim LDS_FechaCierre_Aux As New DataSet

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        sProcedimiento = "CBA_PROCESO_PRECIERRE_Verifica_Interfaz_Tesoreria"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(sProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = sProcedimiento
            SQLCommand.Parameters.Clear()
            '...@PAutoriza
            Dim ParamPautoriza As New SqlClient.SqlParameter("PAutoriza", SqlDbType.Int, 4)
            ParamPautoriza.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParamPautoriza)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_FechaCierre)

            VerificaInterfazTesoreria = SQLCommand.Parameters("PAutoriza").Value
            strResultado = "OK"
            Return VerificaInterfazTesoreria

        Catch ex As Exception
            strResultado = ex.Message
            VerificaInterfazTesoreria = 99
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function VerificaExportadosTesoreria(ByRef strResultado As String) As Double

        Dim LDS_FechaCierre As New DataSet
        Dim LDS_FechaCierre_Aux As New DataSet

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        sProcedimiento = "CBA_PROCESO_PRECIERRE_Verifica_Exportados_a_Tesoreria"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(sProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = sProcedimiento
            SQLCommand.Parameters.Clear()
            '...@PAutoriza
            Dim ParamPautoriza As New SqlClient.SqlParameter("PAutoriza", SqlDbType.Int, 4)
            ParamPautoriza.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParamPautoriza)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_FechaCierre)

            VerificaExportadosTesoreria = SQLCommand.Parameters("PAutoriza").Value
            strResultado = "OK"
            Return VerificaExportadosTesoreria

        Catch ex As Exception
            strResultado = ex.Message
            VerificaExportadosTesoreria = 99
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function RespaldoMovimientosTesoreria(ByRef strResultado As String) As Double

        Dim LDS_FechaCierre As New DataSet
        Dim LDS_FechaCierre_Aux As New DataSet

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        sProcedimiento = "Cba_Proceso_Cierres_Respaldo_Movimientos_Tesoreria"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(sProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = sProcedimiento
            SQLCommand.Parameters.Clear()
            '...@PAutoriza
            Dim ParamPautoriza As New SqlClient.SqlParameter("PAutoriza", SqlDbType.Int, 4)
            ParamPautoriza.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParamPautoriza)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_FechaCierre)

            RespaldoMovimientosTesoreria = SQLCommand.Parameters("PAutoriza").Value
            strResultado = "OK"
            Return RespaldoMovimientosTesoreria

        Catch ex As Exception
            strResultado = ex.Message
            RespaldoMovimientosTesoreria = 99
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function TraeInstrumentosYTasas(ByVal strFechaCierre As String, ByRef strResultado As String) As DataSet

        Dim LDS_InstrumentoTasa As New DataSet
        Dim LDS_InstrumentoTasa_Aux As New DataSet
        Dim Lstr_NombreTabla As String
        Dim LstrProcedimiento
        Dim strDescError As String = ""
        Dim strRetorno As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "CBA_CSIMPTASARISK_Buscar_RF_XMLRISK"
        Lstr_NombreTabla = "INSTRUTASA"

        Connect.Abrir()

        Try

            SQLCommand = New SqlClient.SqlCommand(sProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = sProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pFechaCierre", SqlDbType.Char, 10).Value = strFechaCierre

            Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultado.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_InstrumentoTasa, Lstr_NombreTabla)

            LDS_InstrumentoTasa_Aux = LDS_InstrumentoTasa

            'strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            strResultado = "OK"
            Return LDS_InstrumentoTasa_Aux

        Catch ex As Exception
            strResultado = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function ConsultaCuentasCerradas(ByVal strFechaCierre As String, _
                                            ByRef LDS_FechaCierre As DataSet, _
                                            ByVal dblIdCuenta As Double, _
                                            ByRef strCarpetaCartolas As String, _
                                            ByRef strCartolaCierre As String) As String

        Dim LDS_FechaCierre_Aux As New DataSet
        Dim lstrResultado As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        sProcedimiento = "Cba_Cuentas_x_Cierre_Buscar"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(sProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = sProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Float, 10).Value = glngNegocioOperacion
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = IIf(dblIdCuenta = 0, DBNull.Value, dblIdCuenta)
            SQLCommand.Parameters.Add("pFechaCierre", SqlDbType.Char, 10).Value = strFechaCierre

            '...Resultado
            Dim ParametroSal2 As New SqlClient.SqlParameter("pCartolaCierre", SqlDbType.VarChar, 1)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            Dim ParametroSal3 As New SqlClient.SqlParameter("pCarpetaCartolas", SqlDbType.VarChar, 200)
            ParametroSal3.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal3)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_FechaCierre)

            strCartolaCierre = SQLCommand.Parameters("pCartolaCierre").Value.ToString.Trim
            strCarpetaCartolas = SQLCommand.Parameters("pCarpetaCartolas").Value.ToString.Trim

            If Not LDS_FechaCierre Is Nothing Then
                lstrResultado = "OK"
            Else
                lstrResultado = "Se produjo un error al buscar cuentas cerradas."
            End If
            Return lstrResultado
        Catch ex As Exception
            lstrResultado = ex.Message
            Return lstrResultado
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function Cierre_ConsultaLog(ByVal strFechaConsultaIni As String, _
                                       ByVal strFechaConsultaFin As String, _
                                       ByRef strTipoRegistro As String, _
                                       ByRef strConcepto As String, _
                                       ByRef lstrResultado As String) As DataSet

        Dim LDS_Cierre_Log As New DataSet
        Dim LDS_Cierre_Log_Aux As New DataSet
        Dim Lstr_NombreTabla As String
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim strDescError As String = ""
        Dim Connect As Cls_Conexion = New Cls_Conexion

        lstrResultado = "OK"
        sProcedimiento = "Sis_RegistroEventos_consultar"
        Lstr_NombreTabla = "REGISTRO_LOG"
        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(sProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = sProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Float, 10).Value = glngIdUsuario
            SQLCommand.Parameters.Add("pTipoRegistro", SqlDbType.VarChar, 50).Value = Trim(strTipoRegistro)
            SQLCommand.Parameters.Add("pConceptoRegistro", SqlDbType.VarChar, 50).Value = Trim(strConcepto)
            SQLCommand.Parameters.Add("pFechaRegistroIni", SqlDbType.Char, 10).Value = strFechaConsultaIni
            SQLCommand.Parameters.Add("pFechaRegistroFin", SqlDbType.Char, 10).Value = strFechaConsultaFin
            SQLCommand.Parameters.Add("pListaCuentas", SqlDbType.Text, 600000).Value = DBNull.Value

            '...Resultado
            Dim ParametroSal1 As New SqlClient.SqlParameter("pMsjRetorno", SqlDbType.VarChar, 2)
            ParametroSal1.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal1)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Cierre_Log, Lstr_NombreTabla)

            LDS_Cierre_Log_Aux = LDS_Cierre_Log

            strDescError = SQLCommand.Parameters("pMsjRetorno").Value.ToString.Trim

            Return LDS_Cierre_Log_Aux

        Catch ex As Exception
            lstrResultado = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function ProcesoDevengo_Ejecutar(ByVal strFechaDesde As String, _
                                            ByVal strFechaHasta As String, _
                                            ByVal lstrListaC As String, _
                                            Optional ByVal strNemotecnico As String = "") As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction
        Dim lstrProcedimiento As String

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion
        lstrProcedimiento = "Rcp_Devengo_Cartera_RF"

        Try
            SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = lstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.CommandTimeout = 0

            SQLCommand.Parameters.Add("pFechaConsulta_Ini", SqlDbType.Char, 10).Value = strFechaDesde
            SQLCommand.Parameters.Add("pFechaConsulta_Fin", SqlDbType.Char, 10).Value = strFechaHasta
            SQLCommand.Parameters.Add("pListaCuentas", SqlDbType.Text, 600000).Value = lstrListaC
            SQLCommand.Parameters.Add("PID_USUARIO", SqlDbType.Float, 10).Value = glngIdUsuario
            SQLCommand.Parameters.Add("pNemotecnico", SqlDbType.VarChar, 50).Value = IIf(strNemotecnico = "", DBNull.Value, strNemotecnico)

            '...Resultado
            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            'If strDescError.ToUpper.Trim = "OK" Then
            MiTransaccionSQL.Commit()
            'Else
            '   MiTransaccionSQL.Rollback()
            'End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error al procesar cierre, se reversa " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            ProcesoDevengo_Ejecutar = strDescError
        End Try

    End Function

    Public Function Update_Parametros(ByVal NomParametro As String, _
                                         ByVal ValorParametro As String, _
                                         ByRef strRetorno As String) As String

       
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        'If gLogoCliente = "SECURITY" Then
        '  sProcedimiento = "Rcp_ProcesoCierre_Verifica_Security"
        'Else
        sProcedimiento = "Rcp_Update_Parametros"
        'End If

        Connect.Abrir()

        Try

            SQLCommand = New SqlClient.SqlCommand(sProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = sProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pNomParametro", SqlDbType.NVarChar, 50).Value = NomParametro
            SQLCommand.Parameters.Add("pValorParametro", SqlDbType.NVarChar, 50).Value = ValorParametro


            '...Resultado
            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLCommand.ExecuteNonQuery()

            strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            Return Nothing

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function ProcesoCierre_Automatico(ByVal lngNegocio As Long, _
                                     ByVal strFechaCierre As String) As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Lim_PROCESOCIERRE", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Lim_PROCESOCIERRE"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Float, 10).Value = lngNegocio
            SQLCommand.Parameters.Add("pFecha", SqlDbType.Char, 10).Value = strFechaCierre

            '...Resultado
            Dim ParametroSal2 As New SqlClient.SqlParameter("DG_RESULTADO", SqlDbType.VarChar, 1000)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("DG_RESULTADO").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error al procesar cierre " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            ProcesoCierre_Automatico = strDescError
        End Try
    End Function
    Public Function ProcesoCierre_Guardar(ByVal strFechaDesde As String, _
                                            ByVal strFechaHasta As String, _
                                            ByVal dtbIds As DataTable, _
                                            ByVal strCodSubClase As String, _
                                            ByVal dblIdInstrumento As Double, _
                                            ByVal dblIdAsesor As Double, _
                                            ByVal strProceso As String, _
                                            ByVal dblIdNegocio As Double, _
                                            ByRef strRetorno As String) As DataSet
        Dim LDS_ProcesoCierre As New DataSet
        Dim LDS_ProcesoCierre_Aux As New DataSet
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim lstrColumna As String = ""
        Dim lstrResultadoCadena As String = ""
        Dim lstrIds As String = ""
        lstrColumna = "ID_CUENTA"
        lstrResultadoCadena = GeneraCadena(dtbIds, lstrColumna, "©", lstrIds)
        sProcedimiento = "Rcp_ProcesoCierre_Verifica"
        Connect.Abrir()
        Try
            SQLCommand = New SqlClient.SqlCommand(sProcedimiento, Connect.Conexion)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = sProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("pFechaDesde", SqlDbType.Char, 10).Value = strFechaDesde
            SQLCommand.Parameters.Add("pFechaHasta", SqlDbType.Char, 10).Value = strFechaHasta
            SQLCommand.Parameters.Add("pListaCuentas", SqlDbType.Text, 600000).Value = lstrIds
            SQLCommand.Parameters.Add("pCodSubClase", SqlDbType.Char, 15).Value = IIf(strCodSubClase = "", DBNull.Value, strCodSubClase)
            SQLCommand.Parameters.Add("pIdInstrumento", SqlDbType.Float, 10).Value = IIf(dblIdInstrumento = 0, DBNull.Value, dblIdInstrumento)
            SQLCommand.Parameters.Add("pIdAsesor", SqlDbType.Float, 10).Value = IIf(dblIdAsesor = 0, DBNull.Value, dblIdAsesor)
            SQLCommand.Parameters.Add("pProceso", SqlDbType.Char, 1).Value = IIf(strProceso = "", "N", strProceso)
            SQLCommand.Parameters.Add("pNegocio", SqlDbType.Float, 10).Value = dblIdNegocio
            SQLCommand.Parameters.Add("pID_USUARIO", SqlDbType.Float, 10).Value = glngIdUsuario
            SQLCommand.Parameters.Add("pCodEmpresa", SqlDbType.VarChar, 25).Value = gLogoCliente
            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_ProcesoCierre)
            LDS_ProcesoCierre_Aux = LDS_ProcesoCierre
            strRetorno = "OK"
            Return LDS_ProcesoCierre_Aux
        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function
End Class
