﻿Public Class ClsTipoOperacion

    Public Function TraerTipoOperacion(ByVal strCodTipoOperacion As String, _
                         ByVal strColumnas As String, _
                         ByRef strRetorno As String) As DataSet

        Dim LDS_TipoOperacion As New DataSet
        Dim LDS_TipoOperacion_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_TipoOperacion_Buscar"
        Lstr_NombreTabla = "TIPOOPERACION"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pCodTipoOperacion", SqlDbType.VarChar, 10).Value = IIf(strCodTipoOperacion = "", DBNull.Value, strCodTipoOperacion)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_TipoOperacion, Lstr_NombreTabla)

            LDS_TipoOperacion_Aux = LDS_TipoOperacion

            If strColumnas.Trim = "" Then
                LDS_TipoOperacion_Aux = LDS_TipoOperacion
            Else
                LDS_TipoOperacion_Aux = LDS_TipoOperacion.Copy
                For Each Columna In LDS_TipoOperacion.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_TipoOperacion_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_TipoOperacion_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            Return LDS_TipoOperacion_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

  End Function

  Public Function TipoOperacion_Ver(ByVal strCodigo As String, _
                                    ByVal strColumnas As String, _
                                    ByRef strRetorno As String) As DataSet

    Dim LDS_TipoOperacion As New DataSet
    Dim LDS_TipoOperacion_Aux As New DataSet
    Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
    Dim LInt_Col As Integer = 0
    Dim LInt_NomCol As String = ""
    Dim Lstr_NombreTabla As String = ""
    Dim Columna As DataColumn
    Dim Remove As Boolean = True
    Dim LstrProcedimiento As String = ""

    Dim SQLCommand As New SqlClient.SqlCommand
    Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

    Dim Connect As Cls_Conexion = New Cls_Conexion

    LstrProcedimiento = "Rcp_TipoOperacion_Ver"
    Lstr_NombreTabla = "TIPO_OPERACION"

    Connect.Abrir()

    Try
      SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

      SQLCommand.CommandType = CommandType.StoredProcedure
      SQLCommand.CommandText = LstrProcedimiento
      SQLCommand.Parameters.Clear()

      SQLCommand.Parameters.Add("pCodTipoOperacion", SqlDbType.VarChar, 10).Value = IIf(strCodigo.Trim = "", DBNull.Value, strCodigo.Trim)

      SQLDataAdapter.SelectCommand = SQLCommand
      SQLDataAdapter.Fill(LDS_TipoOperacion, Lstr_NombreTabla)

      LDS_TipoOperacion_Aux = LDS_TipoOperacion

      If strColumnas.Trim = "" Then
        LDS_TipoOperacion_Aux = LDS_TipoOperacion
      Else
        LDS_TipoOperacion_Aux = LDS_TipoOperacion.Copy
        For Each Columna In LDS_TipoOperacion.Tables(Lstr_NombreTabla).Columns
          Remove = True
          For LInt_Col = 0 To LArr_NombreColumna.Length - 1
            LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
            If Columna.ColumnName = LInt_NomCol Then
              Remove = False
            End If
          Next
          If Remove Then
            LDS_TipoOperacion_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
          End If
        Next
      End If

      For LInt_Col = 0 To LArr_NombreColumna.Length - 1
        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
        For Each Columna In LDS_TipoOperacion_Aux.Tables(Lstr_NombreTabla).Columns
          If Columna.ColumnName = LInt_NomCol Then
            Columna.SetOrdinal(LInt_Col)
            Exit For
          End If
        Next
      Next

      strRetorno = "OK"
      LDS_TipoOperacion.Dispose()
      Return LDS_TipoOperacion_Aux

    Catch ex As Exception
      strRetorno = ex.Message
      Return Nothing
    Finally
      Connect.Cerrar()
    End Try
  End Function

End Class
