﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports Microsoft.VisualBasic
Public Class ClsPerfilesAcciones

    Public Function Insertar(ByRef strRetorno As String, _
                             ByVal intIdAccion As Integer, _
                             ByVal intIdPerfilUsuario As Integer, _
                             ByVal intDerechoAccion As Integer) As DataSet

        Dim lDS_PerfilesAcciones As New DataSet
        Dim lDS_PerfilesAcciones_Aux As New DataSet

        Dim Sqlcommand As New SqlClient.SqlCommand
        Dim SqlLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim Lstr_Procedimiento As String = "Sis_PerfilesAcciones_Mantencion"
        Dim Lstr_NombreTabla As String = "PERFILESACCIONES"

        Connect.Abrir()

        Try
            Sqlcommand = New SqlCommand(Lstr_Procedimiento, Connect.Conexion)

            Sqlcommand.CommandType = CommandType.StoredProcedure
            Sqlcommand.CommandText = Lstr_Procedimiento
            Sqlcommand.Parameters.Clear()

            Sqlcommand.Parameters.Add("@pCodigoOperacion", SqlDbType.Char, 15).Value = "INSERTAR"

            Dim oParamSalida1 As New SqlClient.SqlParameter("@pIdAccion", SqlDbType.Int)
            oParamSalida1.Direction = ParameterDirection.Output
            Sqlcommand.Parameters.Add(oParamSalida1)

            Sqlcommand.Parameters.Add("@pIdPerfilUsuario", SqlDbType.Int).Value = intIdPerfilUsuario
            Sqlcommand.Parameters.Add("@pDerechoAccion", SqlDbType.Int).Value = intDerechoAccion

            Dim oParamSalida2 As New SqlClient.SqlParameter("@pMsjRetorno", SqlDbType.Char, 60)
            oParamSalida2.Direction = ParameterDirection.Output
            Sqlcommand.Parameters.Add(oParamSalida2)

            SqlLDataAdapter.SelectCommand = Sqlcommand
            SqlLDataAdapter.Fill(lDS_PerfilesAcciones, Lstr_NombreTabla)

            lDS_PerfilesAcciones_Aux = lDS_PerfilesAcciones

            strRetorno = Sqlcommand.Parameters("@pMsjRetorno").Value.ToString.Trim

            Return lDS_PerfilesAcciones_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function
    Public Function Modificar(ByRef strRetorno As String, _
                              ByVal intIdAccion As Integer, _
                              ByVal intIdPerfilUsuario As Integer, _
                              ByVal intDerechoAccion As Integer) As DataSet

        Dim lDS_PerfilesAcciones As New DataSet
        Dim lDS_PerfilesAcciones_Aux As New DataSet

        Dim Sqlcommand As New SqlClient.SqlCommand
        Dim SqlLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim Lstr_Procedimiento As String = "Sis_PerfilesAcciones_Mantencion"
        Dim Lstr_NombreTabla As String = "PERFILESACCIONES"

        Connect.Abrir()

        Try
            Sqlcommand = New SqlCommand(Lstr_Procedimiento, Connect.Conexion)

            Sqlcommand.CommandType = CommandType.StoredProcedure
            Sqlcommand.CommandText = Lstr_Procedimiento
            Sqlcommand.Parameters.Clear()

            Sqlcommand.Parameters.Add("@pCodigoOperacion", SqlDbType.Char, 15).Value = "MODIFICAR"
            Sqlcommand.Parameters.Add("@pIdAccion", SqlDbType.Int).Value = intIdAccion
            Sqlcommand.Parameters.Add("@pIdPerfilUsuario", SqlDbType.Int).Value = intIdPerfilUsuario
            Sqlcommand.Parameters.Add("@pDerechoAccion", SqlDbType.Int).Value = intDerechoAccion


            Dim oParamSalida As New SqlClient.SqlParameter("@pMsjRetorno", SqlDbType.Char, 60)
            oParamSalida.Direction = ParameterDirection.Output
            Sqlcommand.Parameters.Add(oParamSalida)

            SqlLDataAdapter.SelectCommand = Sqlcommand
            SqlLDataAdapter.Fill(lDS_PerfilesAcciones, Lstr_NombreTabla)

            lDS_PerfilesAcciones_Aux = lDS_PerfilesAcciones

            strRetorno = "OK"

            Return lDS_PerfilesAcciones_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function
    Public Function Eliminar(ByRef strRetorno As String, _
                             ByVal intIdPerfilUsuario As Integer) As DataSet

        Dim lDS_PerfilesAcciones As New DataSet
        Dim lDS_PerfilesAcciones_Aux As New DataSet

        Dim Sqlcommand As New SqlClient.SqlCommand
        Dim SqlLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim Lstr_Procedimiento As String = "Sis_PerfilesAcciones_Mantencion"
        Dim Lstr_NombreTabla As String = "PERFILESACCIONES"

        Connect.Abrir()

        Try
            Sqlcommand = New SqlCommand(Lstr_Procedimiento, Connect.Conexion)

            Sqlcommand.CommandType = CommandType.StoredProcedure
            Sqlcommand.CommandText = Lstr_Procedimiento
            Sqlcommand.Parameters.Clear()

            Sqlcommand.Parameters.Add("@pCodigoOperacion", SqlDbType.Char, 15).Value = "ELIMINAR"
            Sqlcommand.Parameters.Add("@pIdPerfilUsuario", SqlDbType.Int).Value = intIdPerfilUsuario

            Dim oParamSalida As New SqlClient.SqlParameter("@pMsjRetorno", SqlDbType.Char, 60)
            oParamSalida.Direction = ParameterDirection.Output
            Sqlcommand.Parameters.Add(oParamSalida)


            SqlLDataAdapter.SelectCommand = Sqlcommand
            SqlLDataAdapter.Fill(lDS_PerfilesAcciones, Lstr_NombreTabla)

            lDS_PerfilesAcciones_Aux = lDS_PerfilesAcciones

            strRetorno = "OK"

            Return lDS_PerfilesAcciones_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function
    Public Function Buscar(ByRef strRetorno As String, _
                           ByVal intIdAccion As Integer, _
                           ByVal intIdPerfilUsuario As Integer) As DataSet

        Dim lDS_PerfilesAcciones As New DataSet
        Dim lDS_PerfilesAcciones_Aux As New DataSet

        Dim Sqlcommand As New SqlClient.SqlCommand
        Dim SqlLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim Lstr_Procedimiento As String = "Sis_PerfilesAcciones_Mantencion"
        Dim Lstr_NombreTabla As String = "PERFILESACCIONES"

        Connect.Abrir()

        Try
            Sqlcommand = New SqlCommand(Lstr_Procedimiento, Connect.Conexion)

            Sqlcommand.CommandType = CommandType.StoredProcedure
            Sqlcommand.CommandText = Lstr_Procedimiento
            Sqlcommand.Parameters.Clear()

            Sqlcommand.Parameters.Add("@pCodigoOperacion", SqlDbType.Char, 15).Value = "BUSCAR"
            Sqlcommand.Parameters.Add("@pIdAccion", SqlDbType.Int).Value = IIf(intIdAccion = 0, DBNull.Value, intIdAccion)
            Sqlcommand.Parameters.Add("@pIdPerfilUsuario", SqlDbType.Int).Value = IIf(intIdPerfilUsuario = 0, DBNull.Value, intIdPerfilUsuario)

            Dim oParamSalida As New SqlClient.SqlParameter("@pMsjRetorno", SqlDbType.Char, 60)
            oParamSalida.Direction = ParameterDirection.Output
            Sqlcommand.Parameters.Add(oParamSalida)

            SqlLDataAdapter.SelectCommand = Sqlcommand
            SqlLDataAdapter.Fill(lDS_PerfilesAcciones, Lstr_NombreTabla)

            lDS_PerfilesAcciones_Aux = lDS_PerfilesAcciones

            strRetorno = "OK"

            Return lDS_PerfilesAcciones_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function
    Public Function TraeAccesosDelUsuario(ByRef strRetorno As String, _
                                          ByVal intIdUsuario As Integer) As DataSet

        Dim lDS_PerfilesAcciones As New DataSet
        Dim lDS_PerfilesAcciones_Aux As New DataSet

        Dim Sqlcommand As New SqlClient.SqlCommand
        Dim SqlLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim Lstr_Procedimiento As String = "Sis_PerfilesAcciones_Mantencion"
        Dim Lstr_NombreTabla As String = "PERFILESACCIONES"

        Connect.Abrir()
        Try

            Sqlcommand = New SqlCommand(Lstr_Procedimiento, Connect.Conexion)

            Sqlcommand.CommandType = CommandType.StoredProcedure
            Sqlcommand.CommandText = Lstr_Procedimiento
            Sqlcommand.Parameters.Clear()

            Sqlcommand.Parameters.Add("@pCodigoOperacion", SqlDbType.Char, 15).Value = "DERECHOSUSUARIO"
            Sqlcommand.Parameters.Add("@pIdUsuario", SqlDbType.Int).Value = IIf(intIdUsuario, DBNull.Value, intIdUsuario)

            Dim oParamSalida As New SqlClient.SqlParameter("@pMsjRetorno", SqlDbType.Char, 60)
            oParamSalida.Direction = ParameterDirection.Output
            Sqlcommand.Parameters.Add(oParamSalida)

            SqlLDataAdapter.SelectCommand = Sqlcommand
            SqlLDataAdapter.Fill(lDS_PerfilesAcciones, Lstr_NombreTabla)

            lDS_PerfilesAcciones_Aux = lDS_PerfilesAcciones

            strRetorno = "OK"

            Return lDS_PerfilesAcciones_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function
    Public Function TraeAccesosDelPerfil(ByRef strRetorno As String, _
                                         ByVal intIdPerfilUsuario As Integer, _
                                         ByVal intIdNegocio As Integer) As DataSet

        Dim lDS_PerfilesAcciones As New DataSet
        Dim lDS_PerfilesAcciones_Aux As New DataSet

        Dim Sqlcommand As New SqlClient.SqlCommand
        Dim SqlLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim Lstr_Procedimiento As String = "Sis_PerfilesAcciones_Mantencion"
        Dim Lstr_NombreTabla As String = "PERFILESACCIONES"

        Connect.Abrir()

        Try
            Sqlcommand = New SqlCommand(Lstr_Procedimiento, Connect.Conexion)

            Sqlcommand.CommandType = CommandType.StoredProcedure
            Sqlcommand.CommandText = Lstr_Procedimiento
            Sqlcommand.Parameters.Clear()


            Sqlcommand.Parameters.Add("@pCodigoOperacion", SqlDbType.Char, 15).Value = "ACCIONESPERFIL"
            Sqlcommand.Parameters.Add("@pIdPerfilUsuario", SqlDbType.Int).Value = intIdPerfilUsuario
            Sqlcommand.Parameters.Add("@pIdNegocio", SqlDbType.Int).Value = intIdNegocio

            Dim oParamSalida As New SqlClient.SqlParameter("@pMsjRetorno", SqlDbType.Char, 60)
            oParamSalida.Direction = ParameterDirection.Output
            Sqlcommand.Parameters.Add(oParamSalida)

            SqlLDataAdapter.SelectCommand = Sqlcommand
            SqlLDataAdapter.Fill(lDS_PerfilesAcciones, Lstr_NombreTabla)

            lDS_PerfilesAcciones_Aux = lDS_PerfilesAcciones

            strRetorno = Sqlcommand.Parameters("@pMsjRetorno").Value.ToString.Trim

            Return lDS_PerfilesAcciones_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    'Public Function TraerControles(ByVal intNumeroPantalla As Integer, _
    '                               ByRef aOpciones(,) As String) As Boolean

    '    Dim lDS_PerfilesAcciones As New DataSet
    '    Dim lDS_PerfilesAcciones_Aux As New DataSet

    '    Dim Sqlcommand As New SqlClient.SqlCommand
    '    Dim SqlLDataAdapter As New SqlClient.SqlDataAdapter
    '    Dim Connect As Cls_Conexion = New Cls_Conexion
    '    Dim Lstr_Procedimiento As String = "Sis_PerfilesAcciones_Mantencion"
    '    Dim Lstr_NombreTabla As String = "PERFILESACCIONES"
    '    Dim bValRet As Boolean = False

    '    Connect.Abrir()

    '    Try

    '        Sqlcommand = New SqlCommand(Lstr_Procedimiento, Connect.Conexion)

    '        Sqlcommand.CommandType = CommandType.StoredProcedure
    '        Sqlcommand.CommandText = Lstr_Procedimiento
    '        Sqlcommand.Parameters.Clear()

    '        Sqlcommand.Parameters.Add("@pCodigoOperacion", SqlDbType.Char, 15).Value = "CONTROLES"
    '        Sqlcommand.Parameters.Add("@pIdUsuario", SqlDbType.Int).Value = glngIdUsuario
    '        Sqlcommand.Parameters.Add("@pIdNegocio", SqlDbType.Int).Value = glngNegocioOperacion
    '        Sqlcommand.Parameters.Add("@pNumeroPantalla", SqlDbType.Int).Value = intNumeroPantalla

    '        Dim oParamSalida As New SqlClient.SqlParameter("@pMsjRetorno", SqlDbType.Char, 60)
    '        oParamSalida.Direction = ParameterDirection.Output
    '        Sqlcommand.Parameters.Add(oParamSalida)

    '        SqlLDataAdapter.SelectCommand = Sqlcommand
    '        SqlLDataAdapter.Fill(lDS_PerfilesAcciones, Lstr_NombreTabla)

    '        If Trim(Sqlcommand.Parameters("@pMsjRetorno").Value.ToString) = "OK" Then
    '            lDS_PerfilesAcciones_Aux = lDS_PerfilesAcciones

    '            'Dim oDerechoAccion As New ClsPerfilesAcciones
    '            Dim lintIndice As Integer = 0
    '            Dim Row As Data.DataRow
    '            'With oDerechoAccion
    '            If lDS_PerfilesAcciones_Aux.Tables(0).Rows.Count > 0 Then
    '                bValRet = True
    '                ReDim aOpciones(lDS_PerfilesAcciones_Aux.Tables(0).Rows.Count - 1, 2)
    '                For Each Row In lDS_PerfilesAcciones_Aux.Tables(0).Rows
    '                    aOpciones(lintIndice, 0) = Row("ID_ACCION")
    '                    aOpciones(lintIndice, 1) = IIf(IsDBNull(Row("NOMBRE_CONTROL")), "", Row("NOMBRE_CONTROL"))
    '                    aOpciones(lintIndice, 2) = IIf(IsDBNull(Row("DERECHO_ACCION")), "False", Row("DERECHO_ACCION"))
    '                    lintIndice += 1
    '                Next
    '            End If
    '            'End With
    '            Return bValRet
    '        End If

    '    Catch ex As Exception
    '        Return False
    '        aOpciones = Nothing
    '    Finally
    '        Connect.Cerrar()
    '    End Try

    'End Function
    Public Function TraeModulos(ByRef strRetorno As String, _
                                ByVal intIdUsuario As Integer, _
                                ByVal intIdNegocio As Integer) As DataSet

        Dim lDS_PerfilesAcciones As New DataSet
        Dim lDS_PerfilesAcciones_Aux As New DataSet

        Dim Sqlcommand As New SqlClient.SqlCommand
        Dim SqlLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim Lstr_Procedimiento As String = "Sis_PerfilesAcciones_Mantencion"
        Dim Lstr_NombreTabla As String = "MODULOS"

        Connect.Abrir()

        Try

            Sqlcommand = New SqlCommand(Lstr_Procedimiento, Connect.Conexion)

            Sqlcommand.CommandType = CommandType.StoredProcedure
            Sqlcommand.CommandText = Lstr_Procedimiento
            Sqlcommand.Parameters.Clear()


            Sqlcommand.Parameters.Add("@pCodigoOperacion", SqlDbType.Char, 15).Value = "MODULOS"
            Sqlcommand.Parameters.Add("@pIdUsuario", SqlDbType.Int).Value = intIdUsuario
            Sqlcommand.Parameters.Add("@pIdNegocio", SqlDbType.Int).Value = intIdNegocio

            Dim oParamSalida As New SqlClient.SqlParameter("@pMsjRetorno", SqlDbType.Char, 60)
            oParamSalida.Direction = ParameterDirection.Output
            Sqlcommand.Parameters.Add(oParamSalida)

            SqlLDataAdapter.SelectCommand = Sqlcommand
            SqlLDataAdapter.Fill(lDS_PerfilesAcciones, Lstr_NombreTabla)

            lDS_PerfilesAcciones_Aux = lDS_PerfilesAcciones

            strRetorno = "OK"

            Return lDS_PerfilesAcciones_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function
    Public Function ProcesarAccionesPerfil(ByVal strAcciones As String, _
                                           ByVal intIdPerfilUsuario As Integer, _
                                           ByVal intIdNegocio As Integer, _
                                           ByVal intDerechoAccion As Integer) As String


        Dim lDS_PerfilesAcciones As New DataSet
        Dim lDS_PerfilesAcciones_Aux As New DataSet

        Dim Sqlcommand As New SqlClient.SqlCommand
        Dim SqlLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim Lstr_Procedimiento As String = "Sis_PerfilesAcciones_Procesa"
        Dim Lstr_NombreTabla As String = "PERFILESACCIONES"

        Connect.Abrir()

        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        MiTransaccionSQL = Connect.Transaccion

        Try
            Sqlcommand = New SqlClient.SqlCommand(Lstr_Procedimiento, MiTransaccionSQL.Connection, MiTransaccionSQL)

            Sqlcommand.CommandType = CommandType.StoredProcedure
            Sqlcommand.CommandText = Lstr_Procedimiento
            Sqlcommand.Parameters.Clear()


            Sqlcommand.Parameters.Add("@pListaAcciones", SqlDbType.VarChar, 8000).Value = strAcciones
            Sqlcommand.Parameters.Add("@pIdPerfilUsuario", SqlDbType.Int).Value = intIdPerfilUsuario
            Sqlcommand.Parameters.Add("@pIdNegocio", SqlDbType.Int).Value = intIdNegocio
            Sqlcommand.Parameters.Add("@pDerechoAccion", SqlDbType.Int).Value = intDerechoAccion



            Dim oParamSalida As New SqlClient.SqlParameter("@pMsjRetorno", SqlDbType.Char, 60)
            oParamSalida.Direction = ParameterDirection.Output
            Sqlcommand.Parameters.Add(oParamSalida)

            Sqlcommand.ExecuteNonQuery()

            If Trim(Sqlcommand.Parameters("@pMsjRetorno").Value.ToString) = "OK" Then
                MiTransaccionSQL.Commit()
                Return ("OK")
            Else
                MiTransaccionSQL.Rollback()
                Return (Trim(Sqlcommand.Parameters("@pMsjRetorno").Value.ToString))
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            Return ("Error en el Servicio de Grabación de Datos. " & vbCr & Ex.Message)
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function
    

End Class
