﻿Public Class ClsEMailCliente

    Public Function EMailPersonaCliente_Ver(ByVal strIdEMail As String, _
                                            ByVal strIdCliente As String, _
                                            ByVal strColumnas As String, _
                                            ByRef strRetorno As String) As DataSet

        Dim LDS_DireccionCliente As New DataSet
        Dim LDS_DireccionCliente_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = "EMAIL_CLIENTE"
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = "Rcp_EMailCliente_Consultar"

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("@pIdEMail", SqlDbType.VarChar, 9).Value = IIf(strIdEMail.Trim = "", DBNull.Value, strIdEMail.Trim)
            SQLCommand.Parameters.Add("@pIdPersona", SqlDbType.VarChar, 9).Value = IIf(strIdCliente.Trim = "", DBNull.Value, strIdCliente.Trim)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_DireccionCliente, Lstr_NombreTabla)


            LDS_DireccionCliente_Aux = LDS_DireccionCliente

            If strColumnas.Trim = "" Then
                LDS_DireccionCliente_Aux = LDS_DireccionCliente
            Else
                LDS_DireccionCliente_Aux = LDS_DireccionCliente.Copy
                For Each Columna In LDS_DireccionCliente.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_DireccionCliente_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_DireccionCliente_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_DireccionCliente.Dispose()
            Return LDS_DireccionCliente_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function EMailPersona_Mantenedor(ByVal strAccion As String, ByVal strIdEMail As String, ByVal strIdPersona As String, ByVal strCodTipoEMail As String, ByVal strEMail As String) As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_EMail_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_EMail_Mantencion"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("@pAccion", SqlDbType.VarChar, 10).Value = strAccion
            SQLCommand.Parameters.Add("@pIdEMail", SqlDbType.Float, 10).Value = IIf(strIdEMail = "", DBNull.Value, IIf(strIdEMail <> "-1", CLng("0" & strIdEMail), -1))
            SQLCommand.Parameters.Add("@pId_Persona", SqlDbType.Float, 10).Value = IIf(strIdPersona = "", DBNull.Value, IIf(strIdPersona <> "-1", CLng("0" & strIdPersona), -1))
            SQLCommand.Parameters.Add("@pCod_Tipo", SqlDbType.VarChar, 30).Value = strCodTipoEMail
            SQLCommand.Parameters.Add("@pEMail", SqlDbType.VarChar, 150).Value = strEMail

            'MessageBox.Show(strAccion & "::" & strIdDireccion & "::" & strIdPersona & "::" & strCodTipoDireccion & "::" & strDireccion & "::" & strFono & "::" & strFax & "::" & strIdComunaCiudad)

            '...Resultado
            Dim pSalInstPort As New SqlClient.SqlParameter("@pResultado", SqlDbType.VarChar, 200)
            pSalInstPort.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalInstPort)

            SQLCommand.ExecuteNonQuery()
            strDescError = SQLCommand.Parameters("@pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            EMailPersona_Mantenedor = strDescError
        End Try
    End Function

End Class
