﻿Public Class ClsPersona
    Public Function Persona_Ver(ByVal strRut As String, _
                                 ByVal strNombre As String, _
                                 ByVal strTipoPersona As String, _
                                 ByVal strColumnas As String, _
                                 ByRef strRetorno As String, _
                                 Optional ByVal strEstado As String = "V" _
                                 ) As DataSet

        Dim LDS_Persona As New DataSet
        Dim LDS_Persona_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento
        Dim strDescError As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Persona_Buscar"
        Lstr_NombreTabla = "PERSONA"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pRut", SqlDbType.VarChar, 12).Value = IIf(strRut = "", DBNull.Value, strRut)
            SQLCommand.Parameters.Add("pNombre", SqlDbType.VarChar, 50).Value = IIf(strNombre = "", DBNull.Value, strNombre)
            SQLCommand.Parameters.Add("pTipoPersona", SqlDbType.Char, 1).Value = IIf(strTipoPersona = "", DBNull.Value, strTipoPersona)
            SQLCommand.Parameters.Add("pEstadoPersona", SqlDbType.Char, 1).Value = strEstado

            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()
            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Persona, Lstr_NombreTabla)

            LDS_Persona_Aux = LDS_Persona

            If strDescError.ToUpper.Trim <> "OK" Then
                'gFun_ResultadoMsg("2|" & strDescError, "Buscar Personas")
                strRetorno = strDescError '"OK"
                LDS_Persona_Aux = Nothing
            Else
                If strColumnas.Trim = "" Then
                    LDS_Persona_Aux = LDS_Persona
                Else
                    LDS_Persona_Aux = LDS_Persona.Copy
                    For Each Columna In LDS_Persona.Tables(Lstr_NombreTabla).Columns
                        Remove = True
                        For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                            LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                            If Columna.ColumnName = LInt_NomCol Then
                                Remove = False
                            End If
                        Next
                        If Remove Then
                            LDS_Persona_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                        End If
                    Next
                End If

                For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                    LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                    For Each Columna In LDS_Persona_Aux.Tables(Lstr_NombreTabla).Columns
                        If Columna.ColumnName = LInt_NomCol Then
                            Columna.SetOrdinal(LInt_Col)
                            Exit For
                        End If
                    Next
                Next

                strRetorno = strDescError
            End If
            Return LDS_Persona_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function Persona_Mantenedor(ByVal strAccion As String _
                                            , ByVal strIdUsuario As String _
                                            , ByVal strIdPersona As String _
                                            , ByVal strTipoEntidad As String _
                                            , ByVal strNombre As String _
                                            , ByVal strPaterno As String _
                                            , ByVal strMaterno As String _
                                            , ByVal strSexo As String _
                                            , ByVal strFechaNacimiento As String _
                                            , ByVal strIdentificador As String _
                                            , ByVal strTipoIdentificador As String _
                                            , ByVal strCodPais As String _
                                            , ByVal strIdEstadoCivil As String _
                                            , ByVal strProfesion As String _
                                            , ByVal strTelefono As String _
                                            , ByVal strCelular As String _
                                            , ByVal strEmail As String _
                                            , ByVal strEmpleador As String _
                                            , ByVal strCargo As String _
                                            , ByVal strFlgMayorEdad As String _
                                            , ByVal strIdTipoEstado As String _
                                            , ByVal strCodEstado As String _
                                            , ByVal strGiro As String _
                                            , ByVal strIdTipoSociedad As String _
                                            , ByVal strDscDocumento As String _
                                            , ByVal strObservaciones As String _
                                            , ByVal strCopiaEscritura As String _
                                            , ByVal strFechaFormacion As String _
                                            , ByVal strLugar As String _
                                            , ByVal strNotaria As String _
                                            , ByVal strRegistro As String _
                                            , ByVal strNumero As String _
                                            , ByVal strAno As String _
                                            , ByVal strAntecedentes As String _
                                            , ByVal strSitioWeb As String) As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_Persona_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Persona_Mantencion"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 10).Value = strAccion
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.VarChar, 10).Value = strIdUsuario
            SQLCommand.Parameters.Add("pIdPersona", SqlDbType.Float, 10).Value = IIf(strIdPersona = "", DBNull.Value, CLng("0" & strIdPersona))
            SQLCommand.Parameters.Add("pTipoEntidad", SqlDbType.VarChar, 1).Value = IIf(strTipoEntidad = "", DBNull.Value, strTipoEntidad)
            SQLCommand.Parameters.Add("pNombre", SqlDbType.VarChar, 50).Value = IIf(strNombre = "", DBNull.Value, strNombre)
            SQLCommand.Parameters.Add("pPaterno", SqlDbType.VarChar, 50).Value = IIf(strPaterno = "", DBNull.Value, strPaterno)
            SQLCommand.Parameters.Add("pMaterno", SqlDbType.VarChar, 50).Value = IIf(strMaterno = "", DBNull.Value, strMaterno)
            SQLCommand.Parameters.Add("pSexo", SqlDbType.VarChar, 1).Value = IIf(strSexo = "", DBNull.Value, strSexo)
            SQLCommand.Parameters.Add("pFechaNacimiento", SqlDbType.DateTime).Value = IIf(strFechaNacimiento = "", DBNull.Value, strFechaNacimiento)
            SQLCommand.Parameters.Add("pIdentificador", SqlDbType.VarChar, 12).Value = IIf(strIdentificador = "", DBNull.Value, strIdentificador)
      SQLCommand.Parameters.Add("pTipoIdentificador", SqlDbType.VarChar, 1).Value = IIf(strTipoIdentificador = "", "R", strTipoIdentificador)
            SQLCommand.Parameters.Add("pCodPais", SqlDbType.VarChar, 3).Value = IIf(strCodPais = "", DBNull.Value, strCodPais)
            SQLCommand.Parameters.Add("pIdEstadoCivil", SqlDbType.Int, 32).Value = IIf(strIdEstadoCivil = "", DBNull.Value, strIdEstadoCivil)
            SQLCommand.Parameters.Add("pProfesion", SqlDbType.VarChar, 20).Value = IIf(strProfesion = "", DBNull.Value, strProfesion)
            SQLCommand.Parameters.Add("pTelefono", SqlDbType.VarChar, 20).Value = IIf(strTelefono = "", DBNull.Value, strTelefono)
            SQLCommand.Parameters.Add("pCelular", SqlDbType.VarChar, 20).Value = IIf(strCelular = "", DBNull.Value, strCelular)
            SQLCommand.Parameters.Add("pEmail", SqlDbType.VarChar, 50).Value = IIf(strEmail = "", DBNull.Value, strEmail)
            SQLCommand.Parameters.Add("pEmpleador", SqlDbType.VarChar, 50).Value = IIf(strEmpleador = "", DBNull.Value, strEmpleador)
            SQLCommand.Parameters.Add("pCargo", SqlDbType.VarChar, 50).Value = IIf(strCargo = "", DBNull.Value, strCargo)
            SQLCommand.Parameters.Add("pFlgMayorEdad", SqlDbType.VarChar, 1).Value = IIf(strFlgMayorEdad = "", DBNull.Value, strFlgMayorEdad)
            SQLCommand.Parameters.Add("pIdTipoEstado", SqlDbType.Int, 32).Value = IIf(strIdTipoEstado = "", DBNull.Value, strIdTipoEstado)
            SQLCommand.Parameters.Add("pCodEstado", SqlDbType.VarChar, 1).Value = IIf(strCodEstado = "", DBNull.Value, strCodEstado)
            SQLCommand.Parameters.Add("pGiro", SqlDbType.VarChar, 50).Value = IIf(strGiro = "", DBNull.Value, strGiro)
            SQLCommand.Parameters.Add("pIdTipoSociedad", SqlDbType.Int, 32).Value = IIf(strIdTipoSociedad = "", DBNull.Value, strIdTipoSociedad)
            SQLCommand.Parameters.Add("pDscDocumento", SqlDbType.VarChar, 20).Value = IIf(strDscDocumento = "", DBNull.Value, strDscDocumento)
            SQLCommand.Parameters.Add("pObservaciones", SqlDbType.VarChar, 50).Value = IIf(strObservaciones = "", DBNull.Value, strObservaciones)
            SQLCommand.Parameters.Add("pCopiaEscritura", SqlDbType.VarChar, 1).Value = IIf(strCopiaEscritura = "", DBNull.Value, strCopiaEscritura)
            SQLCommand.Parameters.Add("pFechaFormacion", SqlDbType.DateTime).Value = IIf(strFechaFormacion = "", DBNull.Value, strFechaFormacion)
            SQLCommand.Parameters.Add("pLugar", SqlDbType.VarChar, 20).Value = IIf(strLugar = "", DBNull.Value, strLugar)
            SQLCommand.Parameters.Add("pNotaria", SqlDbType.VarChar, 100).Value = IIf(strNotaria = "", DBNull.Value, strNotaria)
            SQLCommand.Parameters.Add("pRegistro", SqlDbType.VarChar, 100).Value = IIf(strRegistro = "", DBNull.Value, strRegistro)
            SQLCommand.Parameters.Add("pNumero", SqlDbType.VarChar, 100).Value = IIf(strNumero = "", DBNull.Value, strNumero)
            SQLCommand.Parameters.Add("pAno", SqlDbType.VarChar, 100).Value = IIf(strAno = "", DBNull.Value, strAno)
            SQLCommand.Parameters.Add("pAntecedentes", SqlDbType.VarChar, 1).Value = IIf(strAntecedentes = "", DBNull.Value, strAntecedentes)
            SQLCommand.Parameters.Add("pSitioWeb", SqlDbType.VarChar, 50).Value = IIf(strSitioWeb = "", DBNull.Value, strSitioWeb)

            'MessageBox.Show(strAccion & "::" & strIdDireccion & "::" & strIdPersona & "::" & strCodTipoDireccion & "::" & strDireccion & "::" & strFono & "::" & strFax & "::" & strIdComunaCiudad)

            '...Resultado
            Dim pSalInstPort As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalInstPort.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalInstPort)

            SQLCommand.ExecuteNonQuery()
            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            Persona_Mantenedor = strDescError
        End Try
    End Function


    Public Function Persona_Eliminar(ByVal strAccion As String _
                                   , ByVal strIdUsuario As String _
                                   , ByVal strIdPersona As String _
                                   , ByVal strIdentificador As String _
                                     ) As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        'Dim MiTransaccionSQL As SqlClient.SqlTransaction

        SQLConnect.Abrir()
        'MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_Persona_Mantencion", SQLConnect.Conexion) ', MiTransaccionSQL.Connection, MiTransaccionSQL)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Persona_Mantencion"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 10).Value = strAccion
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.VarChar, 10).Value = strIdUsuario
            SQLCommand.Parameters.Add("pIdPersona", SqlDbType.Float, 10).Value = strIdPersona
            SQLCommand.Parameters.Add("pTipoEntidad", SqlDbType.VarChar, 1).Value = DBNull.Value
            SQLCommand.Parameters.Add("pNombre", SqlDbType.VarChar, 50).Value = DBNull.Value
            SQLCommand.Parameters.Add("pPaterno", SqlDbType.VarChar, 50).Value = DBNull.Value
            SQLCommand.Parameters.Add("pMaterno", SqlDbType.VarChar, 50).Value = DBNull.Value
            SQLCommand.Parameters.Add("pSexo", SqlDbType.VarChar, 1).Value = DBNull.Value
            SQLCommand.Parameters.Add("pFechaNacimiento", SqlDbType.DateTime).Value = DBNull.Value
            SQLCommand.Parameters.Add("pIdentificador", SqlDbType.VarChar, 12).Value = IIf(strIdentificador = "", DBNull.Value, strIdentificador)
            SQLCommand.Parameters.Add("pTipoIdentificador", SqlDbType.VarChar, 1).Value = DBNull.Value
            SQLCommand.Parameters.Add("pCodPais", SqlDbType.VarChar, 3).Value = DBNull.Value
            SQLCommand.Parameters.Add("pIdEstadoCivil", SqlDbType.Int, 32).Value = DBNull.Value
            SQLCommand.Parameters.Add("pProfesion", SqlDbType.VarChar, 20).Value = DBNull.Value
            SQLCommand.Parameters.Add("pTelefono", SqlDbType.VarChar, 20).Value = DBNull.Value
            SQLCommand.Parameters.Add("pCelular", SqlDbType.VarChar, 20).Value = DBNull.Value
            SQLCommand.Parameters.Add("pEmail", SqlDbType.VarChar, 50).Value = DBNull.Value
            SQLCommand.Parameters.Add("pEmpleador", SqlDbType.VarChar, 50).Value = DBNull.Value
            SQLCommand.Parameters.Add("pCargo", SqlDbType.VarChar, 50).Value = DBNull.Value
            SQLCommand.Parameters.Add("pFlgMayorEdad", SqlDbType.VarChar, 1).Value = DBNull.Value
            SQLCommand.Parameters.Add("pIdTipoEstado", SqlDbType.Int, 32).Value = DBNull.Value
            SQLCommand.Parameters.Add("pCodEstado", SqlDbType.VarChar, 1).Value = DBNull.Value
            SQLCommand.Parameters.Add("pGiro", SqlDbType.VarChar, 50).Value = DBNull.Value
            SQLCommand.Parameters.Add("pIdTipoSociedad", SqlDbType.Int, 32).Value = DBNull.Value
            SQLCommand.Parameters.Add("pDscDocumento", SqlDbType.VarChar, 20).Value = DBNull.Value
            SQLCommand.Parameters.Add("pObservaciones", SqlDbType.VarChar, 50).Value = DBNull.Value
            SQLCommand.Parameters.Add("pCopiaEscritura", SqlDbType.VarChar, 1).Value = DBNull.Value
            SQLCommand.Parameters.Add("pFechaFormacion", SqlDbType.DateTime).Value = DBNull.Value
            SQLCommand.Parameters.Add("pLugar", SqlDbType.VarChar, 20).Value = DBNull.Value
            SQLCommand.Parameters.Add("pNotaria", SqlDbType.VarChar, 100).Value = DBNull.Value
            SQLCommand.Parameters.Add("pRegistro", SqlDbType.VarChar, 100).Value = DBNull.Value
            SQLCommand.Parameters.Add("pNumero", SqlDbType.VarChar, 100).Value = DBNull.Value
            SQLCommand.Parameters.Add("pAno", SqlDbType.VarChar, 100).Value = DBNull.Value
            SQLCommand.Parameters.Add("pAntecedentes", SqlDbType.VarChar, 1).Value = DBNull.Value
            SQLCommand.Parameters.Add("pSitioWeb", SqlDbType.VarChar, 50).Value = DBNull.Value

            'MessageBox.Show(strAccion & "::" & strIdDireccion & "::" & strIdPersona & "::" & strCodTipoDireccion & "::" & strDireccion & "::" & strFono & "::" & strFax & "::" & strIdComunaCiudad)

            '...Resultado
            Dim pSalInstPort As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalInstPort.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalInstPort)

            SQLCommand.ExecuteNonQuery()
            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            'If strDescError.ToUpper.Trim = "OK" Then
            '    MiTransaccionSQL.Commit()
            'Else
            '    MiTransaccionSQL.Rollback()
            'End If

        Catch Ex As Exception
            'MiTransaccionSQL.Rollback()
            strDescError = "Error " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            Persona_Eliminar = strDescError
        End Try
    End Function



End Class

