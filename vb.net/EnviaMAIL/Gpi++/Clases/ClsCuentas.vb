﻿Public Class ClsCuentas
    Public Function TraerCuentas(ByVal strNegocio As String, _
                                 Optional ByVal strNombreCuenta As String = "", _
                                 Optional ByVal strNombreCortoCuenta As String = "", _
                                 Optional ByVal strNumeroCuenta As String = "", _
                                 Optional ByVal strNombreCliente As String = "", _
                                 Optional ByVal strApellidoPaterno As String = "", _
                                 Optional ByVal strApellidoMaterno As String = "", _
                                 Optional ByVal strRazonSocial As String = "", _
                                 Optional ByVal strColumnas As String = "", _
                                 Optional ByRef strRetorno As String = "", _
                                 Optional ByVal strRutCliente As String = "", _
                                 Optional ByVal strTipoCliente As String = "", _
                                 Optional ByVal strCuentaExacta As String = "") As DataSet

        Dim LDS_Cuentas As New DataSet
        Dim Lstr_NombreTabla As String
        Dim LstrProcedimiento
        Dim strDescError As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Cuenta_Buscar"
        Lstr_NombreTabla = "CUENTAS"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Float, 9).Value = IIf(strNegocio = "", DBNull.Value, CLng(strNegocio))
            SQLCommand.Parameters.Add("pNombreCuenta", SqlDbType.VarChar, 50).Value = IIf(strNombreCuenta = "", DBNull.Value, strNombreCuenta)
            SQLCommand.Parameters.Add("pNombreCortoCuenta", SqlDbType.VarChar, 20).Value = IIf(strNombreCortoCuenta = "", DBNull.Value, strNombreCortoCuenta)
            SQLCommand.Parameters.Add("pNumeroCuenta", SqlDbType.VarChar, 15).Value = IIf(strNumeroCuenta = "", DBNull.Value, strNumeroCuenta)
            SQLCommand.Parameters.Add("pNombreCliente", SqlDbType.VarChar, 50).Value = IIf(strNombreCliente = "", DBNull.Value, strNombreCliente)
            SQLCommand.Parameters.Add("pApellidoPaterno", SqlDbType.VarChar, 50).Value = IIf(strApellidoPaterno = "", DBNull.Value, strApellidoPaterno)
            SQLCommand.Parameters.Add("pApellidoMaterno", SqlDbType.VarChar, 50).Value = IIf(strApellidoMaterno = "", DBNull.Value, strApellidoMaterno)
            SQLCommand.Parameters.Add("pRazonSocial", SqlDbType.VarChar, 50).Value = IIf(strRazonSocial = "", DBNull.Value, strRazonSocial)
            SQLCommand.Parameters.Add("pRutCliente", SqlDbType.VarChar, 12).Value = IIf(strRutCliente = "", DBNull.Value, strRutCliente)
            SQLCommand.Parameters.Add("pTipoCliente", SqlDbType.VarChar, 10).Value = IIf(strTipoCliente = "", DBNull.Value, strTipoCliente)
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Float, 6).Value = IIf(glngIdUsuario = 0, DBNull.Value, glngIdUsuario)
            SQLCommand.Parameters.Add("pCuentaExacta", SqlDbType.VarChar, 1).Value = IIf(strCuentaExacta = "", DBNull.Value, strCuentaExacta)

            '...Resultado

            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()
            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Cuentas, Lstr_NombreTabla)

            If strDescError.ToUpper.Trim <> "OK" Then
                gFun_ResultadoMsg("2|" & strDescError, "Buscar Cuentas")
            Else
                Call gsubConfiguraDataTable(LDS_Cuentas.Tables(Lstr_NombreTabla), strColumnas)

                strRetorno = "OK"
            End If
            Return LDS_Cuentas

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function BuscadorCuentas(ByVal strNegocio As String, _
                                   ByVal strNombreCuenta As String, _
                                   ByVal strNumeroCuenta As String, _
                                   ByVal strRutCliente As String, _
                                   ByVal strNombreCliente As String, _
                                   ByVal strColumnas As String, _
                                   ByRef strRetorno As String, _
                                   Optional ByVal dblIdAsesor As Double = 0, _
                                   Optional ByVal strcodMercado As String = "") As DataTable

        Dim ldtCuentas As New DataTable
        Dim LstrProcedimiento = "RCP_CUENTA_BUSCARDOR"
        Dim strDescError As String = ""
        Dim idNegocio As Integer

        If strNegocio = "" Then
            idNegocio = 0
        Else
            idNegocio = CLng(strNegocio)
        End If

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Float, 9).Value = IIf(idNegocio = 0, DBNull.Value, idNegocio)
            SQLCommand.Parameters.Add("pNombreCuenta", SqlDbType.VarChar, 50).Value = IIf(strNombreCuenta = "", DBNull.Value, strNombreCuenta)
            SQLCommand.Parameters.Add("pNumeroCuenta", SqlDbType.VarChar, 15).Value = IIf(strNumeroCuenta = "", DBNull.Value, strNumeroCuenta)
            SQLCommand.Parameters.Add("pNombreCliente", SqlDbType.VarChar, 50).Value = IIf(strNombreCliente = "", DBNull.Value, strNombreCliente)
            SQLCommand.Parameters.Add("pRutCliente", SqlDbType.VarChar, 12).Value = IIf(strRutCliente = "", DBNull.Value, strRutCliente)
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Float, 6).Value = IIf(glngIdUsuario = 0, DBNull.Value, glngIdUsuario)
            SQLCommand.Parameters.Add("pIdAsesor", SqlDbType.Float, 6).Value = IIf(dblIdAsesor = 0, DBNull.Value, dblIdAsesor)
            SQLCommand.Parameters.Add("@pCodMercado", SqlDbType.VarChar, 1).Value = IIf(strcodMercado = "", DBNull.Value, strcodMercado)

            '...Resultado
            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()
            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(ldtCuentas)

            If strDescError.ToUpper.Trim <> "OK" Then
                gFun_ResultadoMsg("2|" & strDescError, "Buscar Cuentas")
                ldtCuentas = Nothing
            Else
                Call gsubConfiguraDataTable(ldtCuentas, strColumnas)
                strRetorno = "OK"
            End If
        Catch ex As Exception
            strRetorno = ex.Message
            ldtCuentas = Nothing
        Finally
            Connect.Cerrar()
        End Try

        Return ldtCuentas
    End Function

    Public Function TraerCuentasxClienteMantenedor(ByVal strNegocio As String, _
                                         ByVal strIdCliente As String, _
                                         ByVal strColumnas As String, _
                                         ByRef strRetorno As String) As DataSet

        Dim LDS_Cuentas As New DataSet
        Dim LDS_Cuentas_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Cuenta_MostrarPorClienteParaMantener"
        Lstr_NombreTabla = "CUENTAS"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Float, 9).Value = IIf(strNegocio.Trim = "", DBNull.Value, CLng("0" & strNegocio))
            SQLCommand.Parameters.Add("pIdCliente", SqlDbType.Float, 9).Value = IIf(strIdCliente.Trim = "", DBNull.Value, CLng("0" & strIdCliente))

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Cuentas, Lstr_NombreTabla)

            LDS_Cuentas_Aux = LDS_Cuentas


            If strColumnas.Trim = "" Then
                LDS_Cuentas_Aux = LDS_Cuentas
            Else
                LDS_Cuentas_Aux = LDS_Cuentas.Copy
                For Each Columna In LDS_Cuentas.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Cuentas_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Cuentas_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            Return LDS_Cuentas_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function TraerCuentasxCliente(ByVal strNegocio As String, _
                                         ByVal strIdCliente As String, _
                                         ByVal strColumnas As String, _
                                         ByRef strRetorno As String) As DataSet

        Dim LDS_Cuentas As New DataSet
        Dim LDS_Cuentas_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento
        Dim strDescError As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Cuenta_BuscarPorCliente"
        Lstr_NombreTabla = "CUENTAS"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Float, 9).Value = IIf(strNegocio.Trim = "", DBNull.Value, CLng("0" & strNegocio))
            SQLCommand.Parameters.Add("pIdCliente", SqlDbType.Float, 9).Value = IIf(strIdCliente.Trim = "", DBNull.Value, CLng(0 & strIdCliente))
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Float, 6).Value = IIf(glngIdUsuario = 0, DBNull.Value, glngIdUsuario)

            '...Resultado

            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()
            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Cuentas, Lstr_NombreTabla)

            LDS_Cuentas_Aux = LDS_Cuentas

            If strDescError.ToUpper.Trim <> "OK" Then
                gFun_ResultadoMsg("2|" & strDescError, "Buscar Cuentas")
            Else
                If strColumnas.Trim = "" Then
                    LDS_Cuentas_Aux = LDS_Cuentas
                Else
                    LDS_Cuentas_Aux = LDS_Cuentas.Copy
                    For Each Columna In LDS_Cuentas.Tables(Lstr_NombreTabla).Columns
                        Remove = True
                        For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                            LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                            If Columna.ColumnName = LInt_NomCol Then
                                Remove = False
                            End If
                        Next
                        If Remove Then
                            LDS_Cuentas_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                        End If
                    Next
                End If

                For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                    LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                    For Each Columna In LDS_Cuentas_Aux.Tables(Lstr_NombreTabla).Columns
                        If Columna.ColumnName = LInt_NomCol Then
                            Columna.SetOrdinal(LInt_Col)
                            Exit For
                        End If
                    Next
                Next
                strRetorno = "OK"
            End If
            Return LDS_Cuentas_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function TraerCuentasxId(ByVal lngNegocio As Long, _
                                    ByVal lngIdCuenta As Long, _
                                    ByVal strColumnas As String, _
                                    ByRef strRetorno As String) As DataSet

        Dim LDS_Cuentas As New DataSet
        Dim LDS_Cuentas_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Cuenta_BuscarPorId"
        Lstr_NombreTabla = "CUENTAS"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Float, 9).Value = IIf(lngNegocio = 0, DBNull.Value, lngNegocio)
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 15).Value = IIf(lngIdCuenta = 0, DBNull.Value, lngIdCuenta)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Cuentas, Lstr_NombreTabla)

            LDS_Cuentas_Aux = LDS_Cuentas

            If strColumnas.Trim = "" Then
                LDS_Cuentas_Aux = LDS_Cuentas
            Else
                LDS_Cuentas_Aux = LDS_Cuentas.Copy
                For Each Columna In LDS_Cuentas.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Cuentas_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Cuentas_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            Return LDS_Cuentas_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    '+ Directo a Eliminar

    Public Function Cuenta_Mantenedor(ByVal strAccion As String, _
                                      ByRef IntIdCuenta As Integer, _
                                      ByVal IntIdCliente As Integer, _
                                      ByVal IntIdContratoCuenta As Integer, _
                                      ByVal IntIdAsesor As Integer, _
                                      ByVal IntIdTipoEstado As Integer, _
                                      ByVal StrCodEstado As String, _
                                      ByVal IntIdNegocio As Integer, _
                                      ByVal StrCodMoneda As String, _
                                      ByVal StrCodTipoAdministracion As String, _
                                      ByVal StrNumCuenta As String, _
                                      ByVal StrAbrCuenta As String, _
                                      ByVal StrDscCuenta As String, _
                                      ByVal StrObservacion As String, _
                                      ByVal StrFlgBloqueado As String, _
                                      ByVal StrObsBloqueo As String, _
                                      ByVal StrFlgImpInstrucciones As String, _
                                      ByVal StrFechaOperativa As String, _
                                      ByVal StrFlgMovDescubiertos As String, _
                                      ByVal StrFchUpdate As String, _
                                      ByVal IntIdUsuarioUpdate As Integer, _
                                      ByVal StrFchInsert As String, _
                                      ByVal IntIdUsuarioInsert As Integer, _
                                      ByVal IntPorcenRf As Integer, _
                                      ByVal IntPorcenRv As Integer, _
                                      ByVal IntIdPerfilRiesgo As Integer, _
                                      ByVal StrFechaCierreCuenta As String, _
                                      ByVal StrFechaContrato As String, _
                                      ByVal IntIdTipocuenta As Integer, _
                                      ByVal IntNumeroFolio As Integer, _
                                      ByVal StrTipoAhorro As String, _
                                      ByVal StrFlgConsideraComVc As String, _
                                      ByVal StrCodigoExternoCuenta As String, _
                                      Optional ByVal IntIdAsesor_Banco As Integer = 0, _
                                      Optional ByVal StrCuentaCorriente As String = "", _
                                      Optional ByVal StrIdentificador As String = "", _
                                      Optional ByVal IntNum_Mac As Double = 0, _
                                      Optional ByVal IntCorrSub_Margen As Double = 0, _
                                      Optional ByVal StrCodSubMargen As String = "", _
                                      Optional ByVal StrGlosaSubMargen As String = "", _
                                      Optional ByVal StrCodUnicoSubMargen As String = "", _
                                      Optional ByVal IntMontoAprobado As Double = 0, _
                                      Optional ByVal StrFechaVencimiento As String = "") As String



        Dim strDescError As String = "OK"
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_Cuenta_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Cuenta_Mantencion"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 10).Value = strAccion

            Dim ParametroSal1 As New SqlClient.SqlParameter("pIdCuenta", SqlDbType.Float, 10)
            ParametroSal1.Value = IntIdCuenta
            ParametroSal1.Direction = Data.ParameterDirection.InputOutput
            SQLCommand.Parameters.Add(ParametroSal1)

            SQLCommand.Parameters.Add("@pIdCliente", SqlDbType.Float, 10).Value = IntIdCliente
            SQLCommand.Parameters.Add("@pIdContratoCuenta", SqlDbType.Float, 10).Value = IIf(IntIdContratoCuenta = 0, DBNull.Value, IntIdContratoCuenta)
            SQLCommand.Parameters.Add("@pIdAsesor", SqlDbType.Float, 4).Value = IIf(IntIdAsesor = 0, DBNull.Value, IntIdAsesor)
            SQLCommand.Parameters.Add("@pIdTipoEstado", SqlDbType.Float, 4).Value = IntIdTipoEstado
            SQLCommand.Parameters.Add("@pCodEstado", SqlDbType.VarChar, 3).Value = IIf(StrCodEstado.Trim = "", DBNull.Value, StrCodEstado.Trim)
            SQLCommand.Parameters.Add("@pIdNegocio", SqlDbType.Float, 10).Value = IntIdNegocio
            SQLCommand.Parameters.Add("@pCodMoneda", SqlDbType.VarChar, 3).Value = IIf(StrCodMoneda.Trim = "", DBNull.Value, StrCodMoneda.Trim)
            SQLCommand.Parameters.Add("@pCodTipoAdministracion", SqlDbType.VarChar, 5).Value = IIf(StrCodTipoAdministracion.Trim = "", DBNull.Value, StrCodTipoAdministracion.Trim)
            SQLCommand.Parameters.Add("@pNumCuenta", SqlDbType.VarChar, 15).Value = IIf(StrNumCuenta.Trim = "", DBNull.Value, StrNumCuenta.Trim)
            SQLCommand.Parameters.Add("@pAbrCuenta", SqlDbType.VarChar, 20).Value = IIf(StrAbrCuenta.Trim = "", DBNull.Value, StrAbrCuenta.Trim)
            SQLCommand.Parameters.Add("@pDscCuenta", SqlDbType.VarChar, 50).Value = IIf(StrDscCuenta.Trim = "", DBNull.Value, StrDscCuenta.Trim)
            SQLCommand.Parameters.Add("@pObservacion", SqlDbType.VarChar, 180).Value = IIf(StrObservacion.Trim = "", DBNull.Value, StrObservacion.Trim)
            SQLCommand.Parameters.Add("@pFlgBloqueado", SqlDbType.VarChar, 1).Value = IIf(StrFlgBloqueado.Trim = "", DBNull.Value, StrFlgBloqueado.Trim)
            SQLCommand.Parameters.Add("@pObsBloqueo", SqlDbType.VarChar, 120).Value = IIf(StrObsBloqueo.Trim = "", DBNull.Value, StrObsBloqueo.Trim)
            SQLCommand.Parameters.Add("@pFlgImpInstrucciones", SqlDbType.VarChar, 1).Value = IIf(StrFlgImpInstrucciones.Trim = "", DBNull.Value, StrFlgImpInstrucciones.Trim)
            SQLCommand.Parameters.Add("@pFechaOperativa", SqlDbType.VarChar, 10).Value = IIf(StrFechaOperativa.Trim = "", DBNull.Value, StrFechaOperativa.Trim)
            SQLCommand.Parameters.Add("@pFlgMovDescubiertos", SqlDbType.VarChar, 1).Value = IIf(StrFlgMovDescubiertos.Trim = "", DBNull.Value, StrFlgMovDescubiertos.Trim)
            'SQLCommand.Parameters.Add("@pFchUpdate", SqlDbType.VarChar, 10).Value = IIf(StrFchUpdate.Trim = "", DBNull.Value, StrFchUpdate.Trim)
            SQLCommand.Parameters.Add("@pIdUsuarioUpdate", SqlDbType.Float, 10).Value = IntIdUsuarioUpdate
            'SQLCommand.Parameters.Add("@pFchInsert", SqlDbType.VarChar, 10).Value = IIf(StrFchInsert.Trim = "", DBNull.Value, StrFchInsert.Trim)
            SQLCommand.Parameters.Add("@pIdUsuarioInsert", SqlDbType.Float, 10).Value = IntIdUsuarioInsert
            SQLCommand.Parameters.Add("@pPorcenRf", SqlDbType.Float, 18).Value = IIf(IntPorcenRf = 0, DBNull.Value, IntPorcenRf)
            SQLCommand.Parameters.Add("@pPorcenRv", SqlDbType.Float, 18).Value = IIf(IntPorcenRv = 0, DBNull.Value, IntPorcenRv)
            SQLCommand.Parameters.Add("@pIdPerfilRiesgo", SqlDbType.Float, 3).Value = IIf(IntIdPerfilRiesgo = 0, DBNull.Value, IntIdPerfilRiesgo)
            SQLCommand.Parameters.Add("@pFechaCierreCuenta", SqlDbType.VarChar, 10).Value = IIf(StrFechaCierreCuenta.Trim = "", DBNull.Value, StrFechaCierreCuenta.Trim)
            SQLCommand.Parameters.Add("@pFechaContrato", SqlDbType.VarChar, 10).Value = IIf(StrFechaContrato.Trim = "", DBNull.Value, StrFechaContrato.Trim)
            SQLCommand.Parameters.Add("@pIdTipocuenta", SqlDbType.Decimal, 18).Value = IIf(IntIdTipocuenta = 0, DBNull.Value, IntIdTipocuenta)
            SQLCommand.Parameters.Add("@pNumeroFolio", SqlDbType.Float, 18).Value = IIf(IntNumeroFolio = 0, DBNull.Value, IntNumeroFolio)
            SQLCommand.Parameters.Add("@pTipoAhorro", SqlDbType.VarChar, 3).Value = IIf(StrTipoAhorro.Trim = "", DBNull.Value, StrTipoAhorro.Trim)
            SQLCommand.Parameters.Add("@pFlgConsideraComVc", SqlDbType.VarChar, 1).Value = IIf(StrFlgConsideraComVc.Trim = "", DBNull.Value, StrFlgConsideraComVc.Trim)
            SQLCommand.Parameters.Add("@pCodigoExternoCuenta", SqlDbType.VarChar, 20).Value = IIf(StrCodigoExternoCuenta.Trim = "", DBNull.Value, StrCodigoExternoCuenta.Trim)
            ' SQLCommand.Parameters.Add("@pCuentaCorriente", SqlDbType.VarChar, 20).Value = IIf(StrCuentaCorriente.Trim = "", DBNull.Value, StrCuentaCorriente.Trim)

            ''''''''''''''''''''''''''''''''''''''''
            'SQLCommand.Parameters.Add("@pIdAsesor_Banco", SqlDbType.Float, 4).Value = IIf(IntIdAsesor_Banco = 0, DBNull.Value, IntIdAsesor_Banco)
            'SQLCommand.Parameters.Add("@pIdentificador", SqlDbType.VarChar, 20).Value = IIf(StrIdentificador.Trim = "", DBNull.Value, StrIdentificador.Trim)
            'SQLCommand.Parameters.Add("@pNumero_Mac", SqlDbType.Float, 10).Value = IIf(IntNum_Mac = 0, DBNull.Value, IntNum_Mac)
            'SQLCommand.Parameters.Add("@pCorr_Sub_Margen", SqlDbType.Float, 2).Value = IIf(IntCorrSub_Margen = 0, DBNull.Value, IntCorrSub_Margen)
            'SQLCommand.Parameters.Add("@pCod_Sub_Margen", SqlDbType.VarChar, 20).Value = IIf(StrCodSubMargen.Trim = "", DBNull.Value, StrCodSubMargen.Trim)
            'SQLCommand.Parameters.Add("@pGlosa_Sub_Margen", SqlDbType.VarChar, 80).Value = IIf(StrGlosaSubMargen.Trim = "", DBNull.Value, StrGlosaSubMargen.Trim)
            'SQLCommand.Parameters.Add("@pCod_Unico_Sub_Margen", SqlDbType.VarChar, 45).Value = IIf(StrCodUnicoSubMargen.Trim = "", DBNull.Value, StrCodUnicoSubMargen.Trim)
            'SQLCommand.Parameters.Add("@pMonto_Aprobado", SqlDbType.Float).Value = IIf(IntMontoAprobado = 0, DBNull.Value, IntMontoAprobado)
            'SQLCommand.Parameters.Add("@pCod_Moneda", SqlDbType.VarChar, 3).Value = IIf(StrCodMoneda.Trim = "", DBNull.Value, StrCodMoneda.Trim)
            'SQLCommand.Parameters.Add("@pFecha_Vencimiento", SqlDbType.VarChar, 10).Value = IIf(StrFechaVencimiento.Trim = "", DBNull.Value, StrFechaVencimiento.Trim)
            'SQLCommand.Parameters.Add("@pCod_Estado", SqlDbType.VarChar, 3).Value = IIf(StrCodEstado.Trim = "", DBNull.Value, StrCodEstado.Trim)


            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            '...Resultado

            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()
            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
                IntIdCuenta = SQLCommand.Parameters("pIdCuenta").Value.ToString.Trim
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            Cuenta_Mantenedor = strDescError
        End Try

    End Function

    Public Function TraerUltimosCierres(ByVal lngNegocio As Long, _
                                        ByRef strFechaUltimoCierre As String, _
                                        ByVal strColumnas As String, _
                                        ByRef strRetorno As String) As DataSet

        Dim LDS_UltimosCierres As New DataSet
        Dim LDS_UltimosCierres_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Cuenta_ConsultarUltimosCierres"
        Lstr_NombreTabla = "ULTIMOSCIERRES"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Float, 10).Value = IIf(lngNegocio = 0, DBNull.Value, lngNegocio)
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Float, 6).Value = IIf(glngIdUsuario = 0, DBNull.Value, glngIdUsuario)

            Dim ParamFechaCierre As New SqlClient.SqlParameter("pFechaUltimoCierre", SqlDbType.VarChar, 10)
            ParamFechaCierre.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParamFechaCierre)


            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_UltimosCierres, Lstr_NombreTabla)

            If IsDBNull(SQLCommand.Parameters("pFechaUltimoCierre")) Then
                strFechaUltimoCierre = ""
            Else
                strFechaUltimoCierre = SQLCommand.Parameters("pFechaUltimoCierre").Value.ToString.Trim
            End If


            LDS_UltimosCierres_Aux = LDS_UltimosCierres

            If strColumnas.Trim = "" Then
                LDS_UltimosCierres_Aux = LDS_UltimosCierres
            Else
                LDS_UltimosCierres_Aux = LDS_UltimosCierres.Copy
                For Each Columna In LDS_UltimosCierres.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_UltimosCierres_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_UltimosCierres_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            Return LDS_UltimosCierres_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function
    Public Function TraerCierreMinimo(ByVal lngNegocio As Long, _
                                        ByRef strFechaMinimoCierre As String, _
                                        ByVal strColumnas As String, _
                                        ByRef strRetorno As String) As String


        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Cuenta_ConsultarMinimoCierre"

        Connect.Abrir()
        strRetorno = "OK"
        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Float, 10).Value = IIf(lngNegocio = 0, DBNull.Value, lngNegocio)
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Float, 6).Value = IIf(glngIdUsuario = 0, DBNull.Value, glngIdUsuario)

            Dim ParamFechaCierre As New SqlClient.SqlParameter("pFechaUltimoCierre", SqlDbType.VarChar, 10)
            ParamFechaCierre.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParamFechaCierre)

            SQLCommand.ExecuteNonQuery()

            If IsDBNull(SQLCommand.Parameters("pFechaUltimoCierre")) Then
                strFechaMinimoCierre = ""
            Else
                strFechaMinimoCierre = SQLCommand.Parameters("pFechaUltimoCierre").Value.ToString.Trim
            End If

            Return strFechaMinimoCierre

        Catch ex As Exception
            strRetorno = ex.Message
            Return ""
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function CuentaEmisor_Ver(ByVal lngIdCuenta As Long, _
                                 ByVal strColumnas As String, _
                                 ByRef strRetorno As String) As DataSet

        Dim LDS_CuentaEmisor As New DataSet
        Dim LDS_CuentaEmisor_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_CuentaEmisor_Consultar"
        Lstr_NombreTabla = "CUENTA"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()



            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = IIf(lngIdCuenta = 0, DBNull.Value, lngIdCuenta)
            'SQLCommand.Parameters.Add("pIdEmisor ", SqlDbType.Float, 10).Value = IIf(lngIdEmisor = 0, DBNull.Value, lngIdEmisor)
            'SQLCommand.Parameters.Add("pIdEntidadGpi ", SqlDbType.Float, 10).Value = IIf(lngIdEntidadGpi.Trim = 0, DBNull.Value, lngIdEntidadGpi.Trim)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_CuentaEmisor, Lstr_NombreTabla)

            LDS_CuentaEmisor_Aux = LDS_CuentaEmisor

            If strColumnas.Trim = "" Then
                LDS_CuentaEmisor_Aux = LDS_CuentaEmisor
            Else
                LDS_CuentaEmisor_Aux = LDS_CuentaEmisor.Copy
                For Each Columna In LDS_CuentaEmisor.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_CuentaEmisor_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_CuentaEmisor.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            Return LDS_CuentaEmisor_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function CustodioEmisor_Mantenedor(ByVal strAccion As String, _
                                              ByVal intIdCuenta As Integer, _
                                              ByVal intIdEmisorOrigen As Integer, _
                                              ByVal intIdEmisorFinal As Integer) As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_CuentaEmisor_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_CuentaEmisor_Mantencion"
            SQLCommand.Parameters.Clear()


            SQLCommand.Parameters.Add("@pAccion", SqlDbType.VarChar, 10).Value = strAccion
            SQLCommand.Parameters.Add("@pIdCuenta", SqlDbType.Float, 10).Value = intIdCuenta
            SQLCommand.Parameters.Add("@pIdEmisorOrigen", SqlDbType.Float, 10).Value = intIdEmisorOrigen
            SQLCommand.Parameters.Add("@pIdEmisorFinal", SqlDbType.Float, 10).Value = intIdEmisorFinal


            '...Resultado
            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            CustodioEmisor_Mantenedor = strDescError
        End Try
    End Function


    Public Function CuentaValorCuota_Patrimonio_Ver(ByVal strFechadesde As String, _
                                                    ByVal strFechaHasta As String, _
                                                    ByVal strTipoReporte As String, _
                                                    ByVal strMonedaSalida As String, _
                                                    ByVal strFiltro As String, _
                                                    ByRef strRetorno As String) As DataSet

        Dim LDS_CuentaPatrimonio As New DataSet
        Dim LDS_CuentaPatrimonio_Aux As New DataSet
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim LstrProcedimiento As String
        Dim Lstr_NombreTabla As String

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Reporte_ValorCuotaPatrimonio_Buscar"
        Lstr_NombreTabla = "CuentaPatrimonio"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Int).Value = glngNegocioOperacion
            SQLCommand.Parameters.Add("@pIdUsuario", SqlDbType.Float, 10).Value = glngIdUsuario
            SQLCommand.Parameters.Add("@pTipoReporte", SqlDbType.Char, 1).Value = strTipoReporte
            SQLCommand.Parameters.Add("@pFechaDesde", SqlDbType.VarChar, 10).Value = strFechadesde
            SQLCommand.Parameters.Add("@pFechaHasta", SqlDbType.VarChar, 10).Value = strFechaHasta
            SQLCommand.Parameters.Add("@pMonedaSalida", SqlDbType.VarChar, 3).Value = IIf(strMonedaSalida = "", DBNull.Value, strMonedaSalida)
            SQLCommand.Parameters.Add("@pFiltro", SqlDbType.Text, 60000).Value = IIf(strFiltro = "", DBNull.Value, strFiltro)
            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_CuentaPatrimonio, Lstr_NombreTabla)

            LDS_CuentaPatrimonio_Aux = LDS_CuentaPatrimonio

            strRetorno = "OK"
            Return LDS_CuentaPatrimonio_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function Consolidadas_Ver(ByVal strFechadesde As String, _
                                     ByVal strFechaHasta As String, _
                                     ByVal IdCliente As Integer, _
                                     ByVal IdCuenta As Integer, _
                                     ByVal strRango As String, _
                                                  ByVal strTipoReporte As String, _
                                                  ByVal strFiltro As String, _
                                                  ByRef strRetorno As String) As DataSet

        Dim LDS_CuentaPatrimonio As New DataSet
        Dim LDS_CuentaPatrimonio_Aux As New DataSet
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim LstrProcedimiento As String
        Dim Lstr_NombreTabla As String

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Reporte_Consolidadas_Consultar"
        Lstr_NombreTabla = "Consolidadas"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Int).Value = glngNegocioOperacion
            SQLCommand.Parameters.Add("@pIdUsuario", SqlDbType.Float, 10).Value = glngIdUsuario
            SQLCommand.Parameters.Add("@pTipoReporte", SqlDbType.Char, 1).Value = strTipoReporte
            SQLCommand.Parameters.Add("@pFecha", SqlDbType.VarChar, 10).Value = strFechadesde
            SQLCommand.Parameters.Add("@pFechaHasta", SqlDbType.VarChar, 10).Value = strFechaHasta
            SQLCommand.Parameters.Add("@pFiltro", SqlDbType.Text, 60000).Value = IIf(strFiltro = "", DBNull.Value, strFiltro)
            SQLCommand.Parameters.Add("@pIdCliente", SqlDbType.Int).Value = IdCliente
            SQLCommand.Parameters.Add("@pIdCuenta", SqlDbType.Int).Value = IdCuenta
            SQLCommand.Parameters.Add("@pRango", SqlDbType.VarChar, 1).Value = strRango
            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_CuentaPatrimonio, Lstr_NombreTabla)

            LDS_CuentaPatrimonio_Aux = LDS_CuentaPatrimonio

            strRetorno = "OK"
            Return LDS_CuentaPatrimonio_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function
    Public Function ValorCoutaConsolidado_Consultar(ByVal Id_Cliente As Integer, _
                                                 ByVal Id_Negocio As Integer, _
                                                 ByVal StrMonedaSolicitada As String, _
                                                 ByVal StrFechaHasta As String, _
                                                 ByRef StrConsolidado As String, _
                                                 ByRef StrNacional As String, _
                                                 ByRef StrInternacional As String, _
                                                  ByRef StrColumnas As String, _
                                                 ByRef strRetorno As String) As DataSet

        Dim LDS_CuentaPatrimonio As New DataSet
        Dim LDS_CuentaPatrimonio_Aux As New DataSet
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim LstrProcedimiento As String
        Dim Lstr_NombreTabla As String
        Dim strDescError As String = ""
        Dim LInt_NomCol As String
        Dim LArr_NombreColumna() As String = Split(StrColumnas, ",")
        Dim Columna As DataColumn
        Dim Remove As Boolean

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Reporte_ValorCuotaConsolidado_Consultar"
        Lstr_NombreTabla = "Consolidadas"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("@pIdCliente", SqlDbType.Int).Value = Id_Cliente
            SQLCommand.Parameters.Add("@pIdNegocio", SqlDbType.Int).Value = Id_Negocio
            SQLCommand.Parameters.Add("@pMonedaSolicitada", SqlDbType.Char, 10).Value = StrMonedaSolicitada
            SQLCommand.Parameters.Add("@pFechaHasta", SqlDbType.VarChar, 10).Value = StrFechaHasta
            Dim ParametroSal2 As New SqlClient.SqlParameter("@pConsolidado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)
            Dim ParametroSal3 As New SqlClient.SqlParameter("@pInternacional", SqlDbType.VarChar, 200)
            ParametroSal3.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal3)
            Dim ParametroSal4 As New SqlClient.SqlParameter("@pNacional", SqlDbType.VarChar, 200)
            ParametroSal4.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal4)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_CuentaPatrimonio, Lstr_NombreTabla)

            LDS_CuentaPatrimonio_Aux = LDS_CuentaPatrimonio


            ''''''''''''''''''''''''''''''''''''''''''''''''''






            ''''''''''''''''''''''''''''''''''''''''''''''''''






            StrConsolidado = SQLCommand.Parameters("@pConsolidado").Value.ToString.Trim
            StrInternacional = SQLCommand.Parameters("@pInternacional").Value.ToString.Trim
            StrNacional = SQLCommand.Parameters("@pNacional").Value.ToString.Trim
            If StrNacional = "OK" Then
                LDS_CuentaPatrimonio.Tables(0).TableName = "NACIONAL"

                If StrColumnas.Trim = "" Then
                    LDS_CuentaPatrimonio_Aux = LDS_CuentaPatrimonio
                Else
                    LDS_CuentaPatrimonio_Aux = LDS_CuentaPatrimonio.Copy
                    For Each Columna In LDS_CuentaPatrimonio.Tables("NACIONAL").Columns
                        Remove = True
                        For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                            LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                            If Columna.ColumnName = LInt_NomCol Then
                                Remove = False
                            End If
                        Next
                        If Remove Then
                            LDS_CuentaPatrimonio_Aux.Tables("NACIONAL").Columns.Remove(Columna.ColumnName)
                        End If
                    Next
                End If

                For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                    LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                    For Each Columna In LDS_CuentaPatrimonio.Tables("NACIONAL").Columns
                        If Columna.ColumnName = LInt_NomCol Then
                            Columna.SetOrdinal(LInt_Col)
                            Exit For
                        End If
                    Next
                Next


            End If
            If StrInternacional = "OK" Then
                LDS_CuentaPatrimonio.Tables(1).TableName = "INTERNACIONAL"

                If StrColumnas.Trim = "" Then
                    LDS_CuentaPatrimonio_Aux = LDS_CuentaPatrimonio
                Else
                    LDS_CuentaPatrimonio_Aux = LDS_CuentaPatrimonio.Copy
                    For Each Columna In LDS_CuentaPatrimonio.Tables("INTERNACIONAL").Columns
                        Remove = True
                        For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                            LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                            If Columna.ColumnName = LInt_NomCol Then
                                Remove = False
                            End If
                        Next
                        If Remove Then
                            LDS_CuentaPatrimonio_Aux.Tables("INTERNACIONAL").Columns.Remove(Columna.ColumnName)
                        End If
                    Next
                End If

                For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                    LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                    For Each Columna In LDS_CuentaPatrimonio.Tables("INTERNACIONAL").Columns
                        If Columna.ColumnName = LInt_NomCol Then
                            Columna.SetOrdinal(LInt_Col)
                            Exit For
                        End If
                    Next
                Next



            End If


            If StrConsolidado = "OK" Then
                LDS_CuentaPatrimonio.Tables(2).TableName = "CONSOLIDADO"

                If StrColumnas.Trim = "" Then
                    LDS_CuentaPatrimonio_Aux = LDS_CuentaPatrimonio
                Else
                    LDS_CuentaPatrimonio_Aux = LDS_CuentaPatrimonio.Copy
                    For Each Columna In LDS_CuentaPatrimonio.Tables("CONSOLIDADO").Columns
                        Remove = True
                        For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                            LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                            If Columna.ColumnName = LInt_NomCol Then
                                Remove = False
                            End If
                        Next
                        If Remove Then
                            LDS_CuentaPatrimonio_Aux.Tables("CONSOLIDADO").Columns.Remove(Columna.ColumnName)
                        End If
                    Next
                End If

                For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                    LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                    For Each Columna In LDS_CuentaPatrimonio.Tables("CONSOLIDADO").Columns
                        If Columna.ColumnName = LInt_NomCol Then
                            Columna.SetOrdinal(LInt_Col)
                            Exit For
                        End If
                    Next
                Next
            End If
            LDS_CuentaPatrimonio_Aux = LDS_CuentaPatrimonio

            strRetorno = "OK"
            Return LDS_CuentaPatrimonio_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function DetalleValorCuota_Patrimonio(ByVal strFechaDesde As String, _
                                                 ByVal strFechaHasta As String, _
                                                 ByVal dblIdCuenta As Double, _
                                                 ByVal strCodMoneda As String, _
                                                 ByVal strColumnas As String, _
                                                 ByRef strRetorno As String) As DataSet

        Dim LDS_CuentaPatrimonio As New DataSet
        Dim LDS_CuentaPatrimonio_Aux As New DataSet
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim LstrProcedimiento As String
        Dim Lstr_NombreTabla As String

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Reporte_DetalleValorCuotaPatrimonio_Buscar"
        Lstr_NombreTabla = "DETALLE"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pFechaDesde", SqlDbType.Char, 10).Value = strFechaDesde
            SQLCommand.Parameters.Add("pFechaHasta", SqlDbType.Char, 10).Value = strFechaHasta
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = dblIdCuenta
            SQLCommand.Parameters.Add("pCodMoneda", SqlDbType.Char, 3).Value = strCodMoneda

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_CuentaPatrimonio, Lstr_NombreTabla)

            LDS_CuentaPatrimonio_Aux = LDS_CuentaPatrimonio

            strRetorno = "OK"
            Return LDS_CuentaPatrimonio_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function ConsultarCuentas2(ByVal strNegocio As String, _
                                 ByVal strColumnas As String, _
                                 ByRef strRetorno As String, _
                                 ByVal strLista As String, _
                                 Optional ByVal StrTipoEntidad As String = "") As DataSet

        Dim LDS_Cuentas As New DataSet
        Dim LDS_Cuentas_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento
        Dim strDescError As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Cuenta_Consultar2"
        Lstr_NombreTabla = "CUENTAS"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Float, 9).Value = IIf(strNegocio = "", DBNull.Value, CLng(strNegocio))
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Float, 6).Value = IIf(glngIdUsuario = 0, DBNull.Value, glngIdUsuario)
            SQLCommand.Parameters.Add("pTipoPersona", SqlDbType.Char, 1).Value = IIf(StrTipoEntidad = "", DBNull.Value, StrTipoEntidad)
            SQLCommand.Parameters.Add("pListaCuentas", SqlDbType.Text, 60000).Value = strLista

            '...Resultado

            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            'SQLCommand.ExecuteNonQuery()
            'strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Cuentas, Lstr_NombreTabla)
            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            LDS_Cuentas_Aux = LDS_Cuentas
            If strDescError.ToUpper.Trim <> "OK" Then
                gFun_ResultadoMsg("2|" & strDescError, "Buscar Cuentas")
            Else
                If strColumnas.Trim = "" Then
                    LDS_Cuentas_Aux = LDS_Cuentas
                Else
                    LDS_Cuentas_Aux = LDS_Cuentas.Copy
                    For Each Columna In LDS_Cuentas.Tables(Lstr_NombreTabla).Columns
                        Remove = True
                        For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                            LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                            If Columna.ColumnName = LInt_NomCol Then
                                Remove = False
                            End If
                        Next
                        If Remove Then
                            LDS_Cuentas_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                        End If
                    Next
                End If

                For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                    LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                    For Each Columna In LDS_Cuentas_Aux.Tables(Lstr_NombreTabla).Columns
                        If Columna.ColumnName = LInt_NomCol Then
                            Columna.SetOrdinal(LInt_Col)
                            Exit For
                        End If
                    Next
                Next
                strRetorno = "OK"
            End If
            Return LDS_Cuentas_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function




    Public Function ConsultarCuentas(ByVal strNegocio As String, _
                                     ByVal strColumnas As String, _
                                     ByRef strRetorno As String, _
                                     Optional ByVal StrTipoEntidad As String = "") As DataSet

        Dim LDS_Cuentas As New DataSet
        Dim LDS_Cuentas_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento
        Dim strDescError As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Cuenta_Consultar"
        Lstr_NombreTabla = "CUENTAS"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Float, 9).Value = IIf(strNegocio = "", DBNull.Value, CLng(strNegocio))
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Float, 6).Value = IIf(glngIdUsuario = 0, DBNull.Value, glngIdUsuario)
            SQLCommand.Parameters.Add("pTipoPersona", SqlDbType.Char, 1).Value = IIf(StrTipoEntidad = "", DBNull.Value, StrTipoEntidad)

            '...Resultado

            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()
            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim



            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Cuentas, Lstr_NombreTabla)

            LDS_Cuentas_Aux = LDS_Cuentas
            If strDescError.ToUpper.Trim <> "OK" Then
                gFun_ResultadoMsg("2|" & strDescError, "Buscar Cuentas")
            Else
                If strColumnas.Trim = "" Then
                    LDS_Cuentas_Aux = LDS_Cuentas
                Else
                    LDS_Cuentas_Aux = LDS_Cuentas.Copy
                    For Each Columna In LDS_Cuentas.Tables(Lstr_NombreTabla).Columns
                        Remove = True
                        For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                            LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                            If Columna.ColumnName = LInt_NomCol Then
                                Remove = False
                            End If
                        Next
                        If Remove Then
                            LDS_Cuentas_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                        End If
                    Next
                End If

                For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                    LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                    For Each Columna In LDS_Cuentas_Aux.Tables(Lstr_NombreTabla).Columns
                        If Columna.ColumnName = LInt_NomCol Then
                            Columna.SetOrdinal(LInt_Col)
                            Exit For
                        End If
                    Next
                Next
                strRetorno = "OK"
            End If
            Return LDS_Cuentas_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function
    Public Function ConsultarGrupoCuenta(ByVal strNegocio As String, _
                                         ByVal strColumnas As String, _
                                         ByRef strRetorno As String, _
                                         Optional ByVal StrTipoEntidad As String = "", _
                                         Optional ByVal StrIdGrupo As String = "") As DataSet

        Dim LDS_Cuentas As New DataSet
        Dim LDS_Cuentas_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento
        Dim strDescError As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_GrupoCuenta_buscar"
        Lstr_NombreTabla = "GRUPO_CUENTAS"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()
            If StrIdGrupo = "" Then StrIdGrupo = "0"
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Float, 9).Value = IIf(strNegocio = "", DBNull.Value, CLng(strNegocio))
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Float, 6).Value = IIf(glngIdUsuario = 0, DBNull.Value, glngIdUsuario)
            SQLCommand.Parameters.Add("pTipoPersona", SqlDbType.Char, 1).Value = IIf(StrTipoEntidad = "", DBNull.Value, StrTipoEntidad)
            SQLCommand.Parameters.Add("pIdGrupo", SqlDbType.Float, 9).Value = IIf(StrIdGrupo = "0", DBNull.Value, CLng(StrIdGrupo))

            '...Resultado

            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()
            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Cuentas, Lstr_NombreTabla)

            LDS_Cuentas_Aux = LDS_Cuentas
            If strDescError.ToUpper.Trim <> "OK" Then
                gFun_ResultadoMsg("2|" & strDescError, "Buscar Grupo Cuentas")
            Else
                If strColumnas.Trim = "" Then
                    LDS_Cuentas_Aux = LDS_Cuentas
                Else
                    LDS_Cuentas_Aux = LDS_Cuentas.Copy
                    For Each Columna In LDS_Cuentas.Tables(Lstr_NombreTabla).Columns
                        Remove = True
                        For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                            LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                            If Columna.ColumnName = LInt_NomCol Then
                                Remove = False
                            End If
                        Next
                        If Remove Then
                            LDS_Cuentas_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                        End If
                    Next
                End If

                For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                    LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                    For Each Columna In LDS_Cuentas_Aux.Tables(Lstr_NombreTabla).Columns
                        If Columna.ColumnName = LInt_NomCol Then
                            Columna.SetOrdinal(LInt_Col)
                            Exit For
                        End If
                    Next
                Next
                strRetorno = "OK"
            End If
            Return LDS_Cuentas_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function TraeCuentasXMercado(ByVal strcodMercado As String, _
                                        ByVal strColumnas As String, _
                                        ByRef strRetorno As String) As DataTable

        Dim ldtCuentas As New DataTable
        Dim LstrProcedimiento = "Rcp_Cuenta_BuscarPorMercado"
        Dim strDescError As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            ' SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Float, 9).Value = IIf(strNegocio = "", DBNull.Value, CLng(strNegocio))
            SQLCommand.Parameters.Add("@pCodMercado", SqlDbType.VarChar, 1).Value = IIf(strcodMercado = "", DBNull.Value, strcodMercado)

            '...Resultado
            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()
            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(ldtCuentas)

            If strDescError.ToUpper.Trim <> "OK" Then
                gFun_ResultadoMsg("2|" & strDescError, "Buscar Cuentas")
                ldtCuentas = Nothing
            Else
                Call gsubConfiguraDataTable(ldtCuentas, strColumnas)
                strRetorno = "OK"
            End If
        Catch ex As Exception
            strRetorno = ex.Message
            ldtCuentas = Nothing
        Finally
            Connect.Cerrar()
        End Try

        Return ldtCuentas
    End Function

    Public Function ConsultarGrupoCuenta2(ByVal strNegocio As String, _
                                     ByVal strColumnas As String, _
                                     ByRef strRetorno As String, _
                                     ByVal strLista As String, _
                                    Optional ByVal StrTipoEntidad As String = "", _
                                     Optional ByVal StrIdGrupo As String = "") As DataSet

        Dim LDS_Cuentas As New DataSet
        Dim LDS_Cuentas_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento
        Dim strDescError As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_GrupoCuenta_buscar2"
        Lstr_NombreTabla = "GRUPO_CUENTAS"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()
            If StrIdGrupo = "" Then StrIdGrupo = "0"
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Float, 9).Value = IIf(strNegocio = "", DBNull.Value, CLng(strNegocio))
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Float, 6).Value = IIf(glngIdUsuario = 0, DBNull.Value, glngIdUsuario)
            SQLCommand.Parameters.Add("pTipoPersona", SqlDbType.Char, 1).Value = IIf(StrTipoEntidad = "", DBNull.Value, StrTipoEntidad)
            'SQLCommand.Parameters.Add("pIdGrupo", SqlDbType.Float, 9).Value = IIf(StrIdGrupo = "0", DBNull.Value, CLng(StrIdGrupo))
            SQLCommand.Parameters.Add("pListaCuentas", SqlDbType.Text, 60000).Value = strLista


            '...Resultado

            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()
            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Cuentas, Lstr_NombreTabla)

            LDS_Cuentas_Aux = LDS_Cuentas
            If strDescError.ToUpper.Trim <> "OK" Then
                gFun_ResultadoMsg("2|" & strDescError, "Buscar Grupo Cuentas")
            Else
                If strColumnas.Trim = "" Then
                    LDS_Cuentas_Aux = LDS_Cuentas
                Else
                    LDS_Cuentas_Aux = LDS_Cuentas.Copy
                    For Each Columna In LDS_Cuentas.Tables(Lstr_NombreTabla).Columns
                        Remove = True
                        For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                            LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                            If Columna.ColumnName = LInt_NomCol Then
                                Remove = False
                            End If
                        Next
                        If Remove Then
                            LDS_Cuentas_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                        End If
                    Next
                End If

                For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                    LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                    For Each Columna In LDS_Cuentas_Aux.Tables(Lstr_NombreTabla).Columns
                        If Columna.ColumnName = LInt_NomCol Then
                            Columna.SetOrdinal(LInt_Col)
                            Exit For
                        End If
                    Next
                Next
                strRetorno = "OK"
            End If
            Return LDS_Cuentas_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function


    Public Function Buscar_Cuenta_DCV(ByVal intIdCuenta As Integer, ByVal strCod_Clase_Instrumento As String) As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_Buscar_Cuenta_DCV", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Buscar_Cuenta_DCV"
            SQLCommand.Parameters.Clear()


            SQLCommand.Parameters.Add("@pId_Cuenta", SqlDbType.Float, 10).Value = intIdCuenta
            SQLCommand.Parameters.Add("@pCOD_CLASE_INSTRUMENTO", SqlDbType.VarChar, 50).Value = strCod_Clase_Instrumento

            '...Resultado
            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim


        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            Buscar_Cuenta_DCV = strDescError
        End Try
    End Function

    Public Function BuscarCuentasPorUsuario(ByVal intIdUsuario As Integer, _
                                             ByVal cadena As String) As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion
        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_LeerCuentasWS", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_LeerCuentasWS"
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("@pIdUsuario", SqlDbType.Int, 5).Value = intIdUsuario
            SQLCommand.Parameters.Add("@pcadena", SqlDbType.VarChar, 9000).Value = cadena

            '...Resultado
            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            BuscarCuentasPorUsuario = strDescError
        End Try
    End Function


    Public Function ConsultarCuentaCMA(ByVal IntIdCuenta As Integer, _
                                      ByRef strResultado As String) As String
        Dim strDescError As String = "OK"
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction
        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion
        Try
            SQLCommand = New SqlClient.SqlCommand("CMA_Consulta_Existe_CMA_CUENTA", MiTransaccionSQL.Connection, MiTransaccionSQL)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "CMA_Consulta_Existe_CMA_CUENTA"
            SQLCommand.Parameters.Clear()
            ''''''''''''''''''''''''''''''''''''''''

            SQLCommand.Parameters.Add("@pidCuenta", SqlDbType.Int, 10).Value = IIf(IntIdCuenta = 0, DBNull.Value, IntIdCuenta)

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            '...Resultado
            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()
            strResultado = SQLCommand.Parameters("pResultado").Value.ToString.Trim



        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            ConsultarCuentaCMA = strResultado
        End Try

    End Function
    Public Function BuscadorCuentasCMA(ByVal strNegocio As String, _
                               ByVal strNombreCuenta As String, _
                               ByVal strNumeroCuenta As String, _
                               ByVal strRutCliente As String, _
                               ByVal strNombreCliente As String, _
                               ByVal strCuentaCMA As String, _
                               ByVal strColumnas As String, _
                               ByRef strRetorno As String, _
                               Optional ByVal dblIdAsesor As Double = 0, _
                               Optional ByVal strcodMercado As String = "" _
                                ) As DataTable

        Dim ldtCuentas As New DataTable
        Dim LstrProcedimiento = "CMA_BUSCADOR_CUENTAS"
        Dim strDescError As String = ""
        Dim idNegocio As Integer

        If strNegocio = "" Then
            idNegocio = 0
        Else
            idNegocio = CLng(strNegocio)
        End If

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Float, 9).Value = IIf(idNegocio = 0, DBNull.Value, idNegocio)
            SQLCommand.Parameters.Add("pNombreCuenta", SqlDbType.VarChar, 50).Value = IIf(strNombreCuenta = "", DBNull.Value, strNombreCuenta)
            SQLCommand.Parameters.Add("pNumeroCuenta", SqlDbType.VarChar, 15).Value = IIf(strNumeroCuenta = "", DBNull.Value, strNumeroCuenta)
            SQLCommand.Parameters.Add("pNombreCliente", SqlDbType.VarChar, 50).Value = IIf(strNombreCliente = "", DBNull.Value, strNombreCliente)
            SQLCommand.Parameters.Add("pRutCliente", SqlDbType.VarChar, 12).Value = IIf(strRutCliente = "", DBNull.Value, strRutCliente)
            SQLCommand.Parameters.Add("@pCuentaCMA", SqlDbType.VarChar, 20).Value = IIf(strCuentaCMA = "", DBNull.Value, strCuentaCMA)
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Float, 6).Value = IIf(glngIdUsuario = 0, DBNull.Value, glngIdUsuario)
            SQLCommand.Parameters.Add("pIdAsesor", SqlDbType.Float, 6).Value = IIf(dblIdAsesor = 0, DBNull.Value, dblIdAsesor)
            SQLCommand.Parameters.Add("@pCodMercado", SqlDbType.VarChar, 1).Value = IIf(strcodMercado = "", DBNull.Value, strcodMercado)

            '...Resultado
            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()
            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(ldtCuentas)

            If strDescError.ToUpper.Trim <> "OK" Then
                gFun_ResultadoMsg("2|" & strDescError, "Buscar Cuentas")
                ldtCuentas = Nothing
            Else
                Call gsubConfiguraDataTable(ldtCuentas, strColumnas)
                strRetorno = "OK"
            End If
        Catch ex As Exception
            strRetorno = ex.Message
            ldtCuentas = Nothing
        Finally
            Connect.Cerrar()
        End Try

        Return ldtCuentas
    End Function

    Public Function ObtenerFondosVigentes(ByVal fechaConsulta As String, ByRef resultado As String) As DataTable
        Dim dtbFondos As New DataTable
        Dim procedimiento As String = "Lim_FondosVigentes_Consultar"
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion
        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(procedimiento, Connect.Conexion)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = procedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pFechaConsulta", SqlDbType.VarChar, 50).Value = fechaConsulta

            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()
            resultado = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(dtbFondos)

            Return dtbFondos

        Catch ex As Exception
            resultado = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function
End Class
