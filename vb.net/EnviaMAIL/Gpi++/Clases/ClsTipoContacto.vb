﻿Public Class ClsTipoContacto
    Public Function Consultar_Tipo_Contacto(ByVal lIntIdTipoContacto As Integer, _
                                            ByVal lstrDescTipoContacto As String, _
                                            ByRef strRetorno As String) As DataSet



        Dim LDS_TipoContacto As New DataSet
        Dim LDS_TipoContacto_Aux As New DataSet
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_TipoContacto_Consultar"
        Lstr_NombreTabla = "TIPO_CONTACTO"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("@pIdTipoContacto", SqlDbType.Float, 10).Value = IIf(lIntIdTipoContacto = 0, DBNull.Value, lIntIdTipoContacto)
            SQLCommand.Parameters.Add("@pDscTipoContacto", SqlDbType.VarChar, 50).Value = IIf(lstrDescTipoContacto = "", DBNull.Value, lstrDescTipoContacto)


            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_TipoContacto, Lstr_NombreTabla)

            LDS_TipoContacto_Aux = LDS_TipoContacto

            LDS_TipoContacto_Aux = LDS_TipoContacto

            strRetorno = "OK"
            LDS_TipoContacto.Dispose()
            Return LDS_TipoContacto_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function


    Public Function TipoContacto_Mantenedor(ByVal strAccion As String, _
                                            ByVal lngIdTipoContacto As Long, _
                                            ByVal strCodTipoContacto As String, _
                                            ByVal strDescripcion As String) As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_TipoContacto_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_TipoContacto_Mantencion"
            SQLCommand.Parameters.Clear()

            '@pAccion varchar(10), 
            '@pIdTipoContacto numeric(10), 
            '@pDscTipoContacto varchar(50), 
            '@pResultado varchar(200) OUTPUT

            SQLCommand.Parameters.Add("@pAccion", SqlDbType.VarChar, 10).Value = strAccion
            SQLCommand.Parameters.Add("@pIdTipoContacto", SqlDbType.Float, 10).Value = IIf(lngIdTipoContacto = 0, DBNull.Value, lngIdTipoContacto)
            SQLCommand.Parameters.Add("@pCodTipoContacto", SqlDbType.VarChar, 30).Value = strCodTipoContacto
            SQLCommand.Parameters.Add("@pDscTipoContacto", SqlDbType.VarChar, 50).Value = strDescripcion

            '...Resultado
            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            TipoContacto_Mantenedor = strDescError
        End Try
    End Function


End Class
