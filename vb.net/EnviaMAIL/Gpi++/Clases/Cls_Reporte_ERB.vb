﻿Public Class Cls_Reporte_ERB
    Public Function Reporte_RPT015(ByVal strFecha As String, _
                                   ByVal strUMS As String, _
                                   ByVal strLista As String, _
                                   ByVal strTipoLista As String, _
                                   ByVal intIdEjecutivo As Integer, _
                                   ByVal strNemotecnico As String, _
                                   ByVal strCodClase As String, _
                                   ByVal strCodSubclase As String, _
                                   ByVal strCodUMEmision As String, _
                                   ByVal intIdCustodio As Integer, _
                                   ByVal intIdNegocio As Integer, _
                                   ByRef strRetorno As String) As DataSet



        Dim lds_OperacionesMagic As New DataSet
        Dim lds_OperacionesMagic_Aux As New DataSet
        Dim Lstr_NombreTabla As String
        Dim LstrProcedimiento As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Reporte_PRT015"
        Lstr_NombreTabla = "REPORTE_PRT015"

        SQLConnect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, SQLConnect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.CommandTimeout = 0

            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pFecha", SqlDbType.VarChar, 10).Value = strFecha
            SQLCommand.Parameters.Add("pUMS", SqlDbType.VarChar, 50).Value = IIf(strUMS = "", DBNull.Value, strUMS)
            SQLCommand.Parameters.Add("pLista", SqlDbType.Text).Value = strLista
            SQLCommand.Parameters.Add("pTipoLista", SqlDbType.VarChar, 10).Value = strTipoLista
            SQLCommand.Parameters.Add("pIdEjecutivo", SqlDbType.Decimal, 10).Value = IIf(intIdEjecutivo = 0, DBNull.Value, intIdEjecutivo)
            SQLCommand.Parameters.Add("pNemotecnico", SqlDbType.VarChar, 50).Value = IIf(strNemotecnico = "", DBNull.Value, strNemotecnico)
            SQLCommand.Parameters.Add("pCodClase", SqlDbType.VarChar, 15).Value = IIf(strCodClase = "", DBNull.Value, strCodClase)
            SQLCommand.Parameters.Add("pCodSubClase", SqlDbType.VarChar, 15).Value = IIf(strCodSubclase = "", DBNull.Value, strCodSubclase)
            SQLCommand.Parameters.Add("pCodUMEmision", SqlDbType.VarChar, 3).Value = IIf(strCodUMEmision = "", DBNull.Value, strCodUMEmision)
            SQLCommand.Parameters.Add("pIdCustodio", SqlDbType.Decimal, 10).Value = IIf(intIdCustodio = 0, DBNull.Value, intIdCustodio)
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Decimal, 10).Value = IIf(intIdNegocio = 0, DBNull.Value, intIdNegocio)
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Decimal, 10).Value = glngIdUsuario

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(lds_OperacionesMagic, Lstr_NombreTabla)

            lds_OperacionesMagic_Aux = lds_OperacionesMagic

            strRetorno = "OK"
            Return lds_OperacionesMagic_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            SQLConnect.Cerrar()
        End Try
    End Function

    Public Function Reporte_RPT026(ByVal intIdMovCaja As String, _
                                   ByRef strRetorno As String) As DataSet

        Dim lds_OperacionesMagic As New DataSet
        Dim lds_OperacionesMagic_Aux As New DataSet
        Dim Lstr_NombreTabla As String
        Dim LstrProcedimiento As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Reporte_PRT026"
        Lstr_NombreTabla = "REPORTE_PRT026"

        SQLConnect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, SQLConnect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.CommandTimeout = 0

            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdMovCaja", SqlDbType.Decimal, 10).Value = intIdMovCaja
            '...Resultado
            Dim ParametroSal As New SqlClient.SqlParameter("@pResultado", SqlDbType.VarChar, 200)
            ParametroSal.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(lds_OperacionesMagic, Lstr_NombreTabla)

            lds_OperacionesMagic_Aux = lds_OperacionesMagic

            strRetorno = SQLCommand.Parameters("@pResultado").Value.ToString.Trim

            Return lds_OperacionesMagic_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            SQLConnect.Cerrar()
        End Try
    End Function

    Public Function Reporte_RPT038(ByVal strFechaDesde As String, _
                                   ByVal strFechaHasta As String, _
                                   ByVal strCodMonedaUMS As String, _
                                   ByVal strLista As String, _
                                   ByVal strTipoLista As String, _
                                   ByVal strCodSubClase As String, _
                                   ByVal intIdEmisor As Integer, _
                                   ByVal intIdInstrumento As Integer, _
                                   ByVal strCodMonedaEmi As String, _
                                   ByRef strRetorno As String) As DataSet

        Dim lds_OperacionesMagic As New DataSet
        Dim lds_OperacionesMagic_Aux As New DataSet
        Dim Lstr_NombreTabla As String
        Dim LstrProcedimiento As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Reporte_Vencimiento_Instrumentos"
        Lstr_NombreTabla = "REPORTE_VENCIMIENTO_INSTRUMENTOS"

        SQLConnect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, SQLConnect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.CommandTimeout = 0

            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pFechaDesde", SqlDbType.VarChar, 10).Value = strFechaDesde
            SQLCommand.Parameters.Add("pFechaHasta", SqlDbType.VarChar, 10).Value = strFechaHasta
            SQLCommand.Parameters.Add("pCodMonedaUMS", SqlDbType.VarChar, 3).Value = IIf(strCodMonedaUMS = "", "CLP", strCodMonedaUMS)
            SQLCommand.Parameters.Add("pLista", SqlDbType.Text).Value = strLista
            SQLCommand.Parameters.Add("pTipoLista", SqlDbType.Char, 1).Value = strTipoLista
            SQLCommand.Parameters.Add("pCodSubClase", SqlDbType.VarChar, 15).Value = IIf(strCodSubClase = "", DBNull.Value, strCodSubClase)
            SQLCommand.Parameters.Add("pIdEmisor", SqlDbType.Decimal, 10).Value = IIf(intIdEmisor = 0, DBNull.Value, intIdEmisor)
            SQLCommand.Parameters.Add("pIdInstrumento", SqlDbType.Decimal, 10).Value = IIf(intIdInstrumento = 0, DBNull.Value, intIdInstrumento)
            SQLCommand.Parameters.Add("pCodMonedaEmi", SqlDbType.VarChar, 3).Value = IIf(strCodMonedaEmi = "", DBNull.Value, strCodMonedaEmi)
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Decimal, 10).Value = glngNegocioOperacion
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Decimal, 10).Value = glngIdUsuario
            '...Resultado
            Dim ParametroSal As New SqlClient.SqlParameter("@pResultado", SqlDbType.VarChar, 200)
            ParametroSal.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(lds_OperacionesMagic, Lstr_NombreTabla)

            lds_OperacionesMagic_Aux = lds_OperacionesMagic

            strRetorno = SQLCommand.Parameters("@pResultado").Value.ToString.Trim

            Return lds_OperacionesMagic_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            SQLConnect.Cerrar()
        End Try
    End Function

    Public Function Reporte_RPT052(ByVal strFecha As String, _
                                   ByVal strCodMonedaUMS As String, _
                                   ByVal intIdNegocio As Integer, _
                                   ByRef strRetorno As String) As DataSet

        Dim lds_SaldoContable As New DataSet
        Dim lds_SaldoContable_Aux As New DataSet
        Dim Lstr_NombreTabla As String
        Dim LstrProcedimiento As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Saldo_Contable_MX"
        Lstr_NombreTabla = "SALDO_CONTABLE_MX"

        SQLConnect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, SQLConnect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.CommandTimeout = 0

            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pFecha", SqlDbType.VarChar, 10).Value = strFecha
            SQLCommand.Parameters.Add("pCodMoneda", SqlDbType.VarChar, 3).Value = IIf(strCodMonedaUMS = "", "CLP", strCodMonedaUMS)
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Decimal, 10).Value = IIf(intIdNegocio = 0, DBNull.Value, intIdNegocio)
            '...Resultado
            Dim ParametroSal As New SqlClient.SqlParameter("@pResultado", SqlDbType.VarChar, 200)
            ParametroSal.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(lds_SaldoContable, Lstr_NombreTabla)

            lds_SaldoContable_Aux = lds_SaldoContable

            strRetorno = SQLCommand.Parameters("@pResultado").Value.ToString.Trim

            Return lds_SaldoContable_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            SQLConnect.Cerrar()
        End Try
    End Function
End Class
