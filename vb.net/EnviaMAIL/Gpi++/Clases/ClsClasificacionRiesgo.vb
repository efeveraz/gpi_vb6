﻿Imports Excel = Microsoft.Office.Interop.Excel
Imports Marshal = System.Runtime.InteropServices.Marshal

Public Class ClsClasificacionRiesgo

    Public Function ClasificacionRiesgo_Consultar2(ByVal intIdClasificacionRiesgo As Integer, _
                                        ByVal strColumnas As String, _
                                        ByRef strRetorno As String, _
                                        Optional ByVal intIdEmisor As Integer = 0, _
                                        Optional ByVal strFechaVigencia As String = "") As DataSet

        Dim LDS_ClasificacionRiesgo As New DataSet
        Dim LDS_ClasificacionRiesgo_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_ClasificacionRiesgo_Consultar2"
        Lstr_NombreTabla = "ClasificacionRiesgo"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdClasificacionRiesgo", SqlDbType.Int, 10).Value = IIf(intIdClasificacionRiesgo = 0, DBNull.Value, intIdClasificacionRiesgo)
            SQLCommand.Parameters.Add("pIdEmisor", SqlDbType.Int, 10).Value = IIf(intIdEmisor = 0, DBNull.Value, intIdEmisor)
            SQLCommand.Parameters.Add("pFecha_Vigencia", SqlDbType.VarChar, 10).Value = IIf(strFechaVigencia = "", DBNull.Value, strFechaVigencia)

            '...Resultado
            Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultado.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_ClasificacionRiesgo, Lstr_NombreTabla)

            LDS_ClasificacionRiesgo_Aux = LDS_ClasificacionRiesgo

            If strColumnas.Trim = "" Then
                LDS_ClasificacionRiesgo_Aux = LDS_ClasificacionRiesgo
            Else
                LDS_ClasificacionRiesgo_Aux = LDS_ClasificacionRiesgo.Copy
                For Each Columna In LDS_ClasificacionRiesgo.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_ClasificacionRiesgo_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_ClasificacionRiesgo_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            LDS_ClasificacionRiesgo.Dispose()
            Return LDS_ClasificacionRiesgo_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function ClasificacionRiesgo_Consultar(ByVal intIdClasificacionRiesgo As Integer, _
                                            ByVal strColumnas As String, _
                                            ByRef strRetorno As String, _
                                            Optional ByVal intIdEmpresa As Integer = 0, _
                                            Optional ByVal intIdGrupo As Integer = 0) As DataSet

        Dim LDS_ClasificacionRiesgo As New DataSet
        Dim LDS_ClasificacionRiesgo_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_ClasificacionRiesgo_Consultar"
        Lstr_NombreTabla = "ClasificacionRiesgo"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdClasificacionRiesgo", SqlDbType.Int, 10).Value = IIf(intIdClasificacionRiesgo = 0, DBNull.Value, intIdClasificacionRiesgo)

            '...Resultado
            Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultado.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_ClasificacionRiesgo, Lstr_NombreTabla)

            LDS_ClasificacionRiesgo_Aux = LDS_ClasificacionRiesgo

            If strColumnas.Trim = "" Then
                LDS_ClasificacionRiesgo_Aux = LDS_ClasificacionRiesgo
            Else
                LDS_ClasificacionRiesgo_Aux = LDS_ClasificacionRiesgo.Copy
                For Each Columna In LDS_ClasificacionRiesgo.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_ClasificacionRiesgo_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_ClasificacionRiesgo_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            LDS_ClasificacionRiesgo.Dispose()
            Return LDS_ClasificacionRiesgo_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function ClasificacionRiesgo_Buscar(ByVal intIdClasificacionRiesgo As Integer, _
                                               ByVal strDscClasificacionRiesgo As String, _
                                               ByVal intIdEmpresaClasificadora As Integer, _
                                               ByVal intIdEmisor As Integer, _
                                               ByVal intIdSubClaseInstrumentoGrupo As Integer, _
                                               ByVal intIdInstrumento As Integer, _
                                               ByVal strCodPlazo As String, _
                                               ByVal intIdClasificacion As Integer, _
                                               ByVal strEstClasificacionRiesgo As String, _
                                               ByVal strColumnas As String, _
                                               ByRef strRetorno As String) As DataSet

        Dim LDS_ClasificacionRiesgo As New DataSet
        Dim LDS_ClasificacionRiesgo_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_ClasificacionRiesgo_Buscar"
        Lstr_NombreTabla = "ClasificacionRiesgo"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdClasificacionRiesgo", SqlDbType.Int).Value = IIf(intIdClasificacionRiesgo = 0, DBNull.Value, intIdClasificacionRiesgo)
            SQLCommand.Parameters.Add("pDscClasificacionRiesgo", SqlDbType.VarChar, 50).Value = IIf(strDscClasificacionRiesgo = "", DBNull.Value, strDscClasificacionRiesgo)
            SQLCommand.Parameters.Add("pIdEmpresaClasificadora", SqlDbType.Int).Value = IIf(intIdEmpresaClasificadora = 0, DBNull.Value, intIdEmpresaClasificadora)
            SQLCommand.Parameters.Add("pIdEmisor", SqlDbType.Int).Value = IIf(intIdEmisor = 0, DBNull.Value, intIdEmisor)
            SQLCommand.Parameters.Add("pIdSubClaseInstrumentoGrupo", SqlDbType.Int).Value = IIf(intIdSubClaseInstrumentoGrupo = 0, DBNull.Value, intIdSubClaseInstrumentoGrupo)
            SQLCommand.Parameters.Add("pIdInstrumento", SqlDbType.Int).Value = IIf(intIdInstrumento = 0, DBNull.Value, intIdInstrumento)
            SQLCommand.Parameters.Add("pCodPlazo", SqlDbType.VarChar, 5).Value = IIf(strCodPlazo = "", DBNull.Value, strCodPlazo)
            SQLCommand.Parameters.Add("pIdClasificacion", SqlDbType.Int).Value = IIf(intIdClasificacion = 0, DBNull.Value, intIdClasificacion)
            SQLCommand.Parameters.Add("pCodEstado", SqlDbType.VarChar, 3).Value = IIf(strEstClasificacionRiesgo = "", DBNull.Value, strEstClasificacionRiesgo)

            '...Resultado
            Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultado.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_ClasificacionRiesgo, Lstr_NombreTabla)

            LDS_ClasificacionRiesgo_Aux = LDS_ClasificacionRiesgo

            If strColumnas.Trim = "" Then
                LDS_ClasificacionRiesgo_Aux = LDS_ClasificacionRiesgo
            Else
                LDS_ClasificacionRiesgo_Aux = LDS_ClasificacionRiesgo.Copy
                For Each Columna In LDS_ClasificacionRiesgo.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_ClasificacionRiesgo_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_ClasificacionRiesgo_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            LDS_ClasificacionRiesgo.Dispose()
            Return LDS_ClasificacionRiesgo_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function ClasificacionRiesgo_Mantencion(ByVal strAccion As String, _
                                                   ByVal intIdClasificacionRiesgo As Integer, _
                                                   ByVal strDscClasificacionRiesgo As String, _
                                                   ByVal intIdEmpresaClasificadora As Integer, _
                                                   ByVal intIdEmisor As Integer, _
                                                   ByVal intIdSubClaseInstrumentoGrupo As Integer, _
                                                   ByVal intIdInstrumento As Integer, _
                                                   ByVal strCodPlazo As String, _
                                                   ByVal intIdClasificacion As Integer, _
                                                   ByVal strEstClasificacionRiesgo As String, _
                                                   Optional ByVal strFecha_Vigencia As String = "") As String

        Dim strDescError As String = "OK"
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try


            SQLCommand = New SqlClient.SqlCommand("Rcp_ClasificacionRiesgo_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_ClasificacionRiesgo_Mantencion"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 20).Value = UCase(strAccion.Trim)
            '...IdClasificacion
            Dim pSalIdClasificacionRiesgo As New SqlClient.SqlParameter("pIdClasificacionRiesgo", SqlDbType.Int, 10)
            pSalIdClasificacionRiesgo.Direction = Data.ParameterDirection.InputOutput
            pSalIdClasificacionRiesgo.Value = IIf(intIdClasificacionRiesgo = 0, DBNull.Value, intIdClasificacionRiesgo)
            SQLCommand.Parameters.Add(pSalIdClasificacionRiesgo)

            SQLCommand.Parameters.Add("pDscClasificacionRiesgo", SqlDbType.VarChar, 50).Value = strDscClasificacionRiesgo
            SQLCommand.Parameters.Add("pIdEmpresaClasificadora", SqlDbType.Int).Value = intIdEmpresaClasificadora
            SQLCommand.Parameters.Add("pIdEmisor", SqlDbType.Int).Value = intIdEmisor
            SQLCommand.Parameters.Add("pIdSubClaseInstrumentoGrupo", SqlDbType.Int).Value = intIdSubClaseInstrumentoGrupo
            SQLCommand.Parameters.Add("pIdInstrumento", SqlDbType.Int).Value = IIf(intIdInstrumento = 0, DBNull.Value, intIdInstrumento)
            SQLCommand.Parameters.Add("pCodPlazo", SqlDbType.VarChar, 5).Value = strCodPlazo
            SQLCommand.Parameters.Add("pIdClasificacion", SqlDbType.Int).Value = intIdClasificacion
            SQLCommand.Parameters.Add("pCodEstado", SqlDbType.VarChar, 3).Value = strEstClasificacionRiesgo
            SQLCommand.Parameters.Add("pFechaVigencia", SqlDbType.VarChar, 10).Value = strFecha_Vigencia

            '...Resultado
            Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultado.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                If Trim(strDescError) = "OK" Then
                    MiTransaccionSQL.Commit()
                    Return ("OK")
                End If
            Else
                GoTo Salir
            End If

Salir:
            If Trim(strDescError) = "OK" Then
                MiTransaccionSQL.Commit()
                Return ("OK")
            Else
                MiTransaccionSQL.Rollback()
                Return strDescError
            End If


        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en la mantención de clasificacion de riesgo." & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            ClasificacionRiesgo_Mantencion = strDescError
        End Try
    End Function

    Public Function ClasRiesgoInst_Consultar(ByVal strFechaDesde As String, _
                                             ByVal strFechaHasta As String, _
                                             ByVal strColumnas As String, _
                                             ByRef strRetorno As String, _
                                             Optional ByVal intIdEmisor As Integer = 0, _
                                             Optional ByVal intIdInstrumento As Integer = 0) As DataSet

        Dim LDS_ClasificacionRiesgo As New DataSet
        Dim LDS_ClasificacionRiesgo_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        'Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_ClasRiesgoInst_Consultar"
        Lstr_NombreTabla = "ClasificacionRiesgo"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pFecha_Desde", SqlDbType.VarChar, 10).Value = strFechaDesde
            SQLCommand.Parameters.Add("pFecha_Hasta", SqlDbType.VarChar, 10).Value = strFechaHasta
            SQLCommand.Parameters.Add("pIdEmisor", SqlDbType.Int, 10).Value = IIf(intIdEmisor = 0, DBNull.Value, intIdEmisor)
            SQLCommand.Parameters.Add("pIdInstrumento", SqlDbType.Int, 10).Value = IIf(intIdInstrumento = 0, DBNull.Value, intIdInstrumento)

            '...Resultado
            Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultado.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_ClasificacionRiesgo, Lstr_NombreTabla)

            LDS_ClasificacionRiesgo_Aux = LDS_ClasificacionRiesgo

            strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            LDS_ClasificacionRiesgo.Dispose()
            Return LDS_ClasificacionRiesgo_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function ClasRiesgoInst_Mantencion(ByVal strAccion As String, _
                                                   ByVal intIdClasificacionRiesgo As Integer, _
                                                   ByVal strDscClasificacionRiesgo As String, _
                                                   ByVal intIdEmpresaClasificadora As Integer, _
                                                   ByVal intIdEmisor As Integer, _
                                                   ByVal intIdSubClaseInstrumentoGrupo As Integer, _
                                                   ByVal intIdInstrumento As Integer, _
                                                   ByVal strCodPlazo As String, _
                                                   ByVal intIdClasificacion As Integer, _
                                                   ByVal strEstClasificacionRiesgo As String, _
                                                   Optional ByVal strFecha_Vigencia As String = "") As String

        Dim strDescError As String = "OK"
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction
        Dim LstrProcedimiento As String = ""

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion
        LstrProcedimiento = "ClasRiesgoInst_Mantencion"
        Try

            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, MiTransaccionSQL.Connection, MiTransaccionSQL)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 20).Value = UCase(strAccion.Trim)
            '...IdClasificacion
            Dim pSalIdClasificacionRiesgo As New SqlClient.SqlParameter("pIdClasificacionRiesgo", SqlDbType.Int, 10)
            pSalIdClasificacionRiesgo.Direction = Data.ParameterDirection.InputOutput
            pSalIdClasificacionRiesgo.Value = IIf(intIdClasificacionRiesgo = 0, DBNull.Value, intIdClasificacionRiesgo)
            SQLCommand.Parameters.Add(pSalIdClasificacionRiesgo)

            SQLCommand.Parameters.Add("pDscClasificacionRiesgo", SqlDbType.VarChar, 50).Value = strDscClasificacionRiesgo
            SQLCommand.Parameters.Add("pIdEmpresaClasificadora", SqlDbType.Int).Value = IIf(intIdEmpresaClasificadora = 0, DBNull.Value, intIdEmpresaClasificadora)
            SQLCommand.Parameters.Add("pIdEmisor", SqlDbType.Int).Value = IIf(intIdEmisor = 0, DBNull.Value, intIdEmisor)
            SQLCommand.Parameters.Add("pIdSubClaseInstrumentoGrupo", SqlDbType.Int).Value = IIf(intIdSubClaseInstrumentoGrupo = 0, DBNull.Value, intIdSubClaseInstrumentoGrupo)
            SQLCommand.Parameters.Add("pIdInstrumento", SqlDbType.Int).Value = IIf(intIdInstrumento = 0, DBNull.Value, intIdInstrumento)
            SQLCommand.Parameters.Add("pCodPlazo", SqlDbType.VarChar, 5).Value = strCodPlazo
            SQLCommand.Parameters.Add("pIdClasificacion", SqlDbType.Int).Value = intIdClasificacion
            SQLCommand.Parameters.Add("pCodEstado", SqlDbType.VarChar, 3).Value = strEstClasificacionRiesgo
            SQLCommand.Parameters.Add("pFechaVigencia", SqlDbType.VarChar, 10).Value = strFecha_Vigencia

            '...Resultado
            Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultado.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                If Trim(strDescError) = "OK" Then
                    MiTransaccionSQL.Commit()
                    Return ("OK")
                End If
            Else
                GoTo Salir
            End If

Salir:
            If Trim(strDescError) = "OK" Then
                MiTransaccionSQL.Commit()
                Return ("OK")
            Else
                MiTransaccionSQL.Rollback()
                Return strDescError
            End If


        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en la mantención de clasificacion de riesgo." & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            ClasRiesgoInst_Mantencion = strDescError
        End Try
    End Function


    ' <summary>
    ' Clase que contiene Funciones que permiten Generar Archivos en formato Excel con Tablas Dinámicas.
    ' </summary>
    ' <remarks>Perimte generar documentos Excel con Tablas Dinámicas incluídas.</remarks>

    ' <summary>
    ' Enumerador de los Tipos de de datos devueltos por la función Generar Excel.
    ' </summary>
    ' <remarks>Permite determinar el tipo de información que contiene el Diccionario de Respuesta.</remarks>
    Public Enum datoResp
        OK = 0
        MSG = 1
    End Enum

    ' <summary>
    ' Enumerador de las Versiones que soporta el Exportar (v2000=Excel 2000 : v10=Excel XP : v11=Excel 2003 : v12=Excel 2007 : v14=Excel 2010).
    ' </summary>
    ' <remarks>Permite determinar el tipo de Archivo a Generar, y las propiedades del mismo según la versión.</remarks>
    Public Enum VersionExcel
        v10 = 0
        v11 = 1
        v12 = 2
        v14 = 3
        v2000 = 4
    End Enum

    ' <summary>
    ' Función que Genera un Archivo Excel a partir de un objeto DataTable.
    ' </summary>
    ' <param name="nombreArch">El nombre y la dirección del archivo a generar, sin la extensión.</param>
    ' <param name="dt">Objeto de tipo DataTable con los datos del informe.</param>
    ' <param name="titulosColumnas">ArrayList con los Títulos para las Columnas de la Hoja de Datos.</param>
    ' <param name="datosFilas">ArrayList con los Títulos de los datos a ser mostrador en las Filas.</param>
    ' <param name="datosColumnas">ArrayList con los Títulos de los datos a ser mostrador en las Columnas.</param>
    ' <param name="datosFiltros">ArrayList con los Títulos de los datos a ser mostrador en los Filtros.</param>
    ' <param name="datosDatoCalculado">ArrayList con los Títulos de los datos a ser mostrador en las la sección de Datos.</param>
    ' <param name="version">Versión del archivo generado (v12[Excel2007] : v14[Excel2010]).</param>
    ' <returns>Diccionario con la información para determinar si se pudo Generar el archivo o no.</returns>
    ' <remarks>Permite generar un archivo Excel con una Tabla Dinámica con los datos Solicitados.</remarks>
    Public Function GenerarExcel(ByVal nombreArch As String, ByVal dt As Data.DataTable, _
                                        ByVal titulosColumnas As ArrayList, _
                                        ByVal datosFilas As ArrayList, _
                                        ByVal datosColumnas As ArrayList, _
                                        ByVal datosFiltros As ArrayList, _
                                        ByVal datosDatoCalculado As ArrayList, _
                                        ByVal version As VersionExcel) As Dictionary(Of datoResp, Object)
        Dim dict As New Dictionary(Of datoResp, Object)

        Dim extArchivo As String = ""
        Dim versionExcel As Excel.XlPivotTableVersionList
        If version = ClsClasificacionRiesgo.VersionExcel.v10 Then
            versionExcel = Excel.XlPivotTableVersionList.xlPivotTableVersion10
            extArchivo = ".xls"
        ElseIf version = ClsClasificacionRiesgo.VersionExcel.v11 Then
            versionExcel = Excel.XlPivotTableVersionList.xlPivotTableVersion11
            extArchivo = ".xls"
        ElseIf version = ClsClasificacionRiesgo.VersionExcel.v12 Then
            versionExcel = Excel.XlPivotTableVersionList.xlPivotTableVersion12
            extArchivo = ".xlsx"
        ElseIf version = ClsClasificacionRiesgo.VersionExcel.v14 Then
            versionExcel = Excel.XlPivotTableVersionList.xlPivotTableVersionCurrent
            extArchivo = ".xlsx"
        ElseIf version = ClsClasificacionRiesgo.VersionExcel.v2000 Then
            versionExcel = Excel.XlPivotTableVersionList.xlPivotTableVersion2000
            extArchivo = ".xls"
        End If

        Dim col As Integer = 0
        Dim fila As Integer = 1
        Dim colmax As Integer = titulosColumnas.Count

        Dim app As Excel.Application
        Dim book As Excel.Workbook
        Dim hojaDatos As Excel.Worksheet
        Dim hojaTabla As Excel.Worksheet

        Try
            app = New Excel.Application()
            app.Visible = False

            book = app.Workbooks.Add()
            CType(book.Sheets.Item(3), Excel.Worksheet).Delete()

            hojaDatos = book.Sheets.Item(2)
            hojaDatos.Name = "Datos"

            For Each titulo As String In titulosColumnas
                col += 1
                hojaDatos.Cells(fila, col) = titulo.ToUpper().Trim()
            Next

            For Each dr As Data.DataRow In dt.Rows
                fila += 1
                col = 0
                For i As Integer = 0 To colmax - 1
                    col += 1
                    hojaDatos.Cells(fila, col) = dr.Item(i)
                Next
            Next

            hojaTabla = book.Sheets.Item(1)
            hojaTabla.Name = "Tabla"

            Dim pc As Excel.PivotCache
            pc = book.PivotCaches.Create(Excel.XlPivotTableSourceType.xlDatabase, _
                                         "Datos!R1C1:R" & fila.ToString().Trim() & _
                                         "C" & colmax.ToString().Trim(), _
                                         versionExcel)
            Dim pt As Excel.PivotTable
            pt = pc.CreatePivotTable("Tabla!R1C1", "PT", , _
                                     versionExcel)

            hojaTabla.Select()

            For Each dato As String In datosDatoCalculado
                pt.AddDataField(pt.PivotFields(dato), "Suma de " & dato, _
                                Excel.XlConsolidationFunction.xlSum)
            Next

            Dim posCol As Integer = 0
            For Each dato As String In datosColumnas
                posCol += 1
                With CType(pt.PivotFields(dato), Excel.PivotField)
                    .Orientation = Excel.XlPivotFieldOrientation.xlColumnField
                    .Position = posCol
                    .LayoutBlankLine = True
                End With
            Next

            Dim posFila As Integer = 0
            For Each dato As String In datosFilas
                posFila += 1
                With CType(pt.PivotFields(dato), Excel.PivotField)
                    .Orientation = Excel.XlPivotFieldOrientation.xlRowField
                    .Position = posFila
                    .LayoutBlankLine = True
                End With
            Next

            For Each dato As String In datosFiltros
                With CType(pt.PivotFields(dato), Excel.PivotField)
                    .Orientation = Excel.XlPivotFieldOrientation.xlPageField
                    .Position = 1
                    .LayoutBlankLine = True
                End With
            Next

            For Each dato As String In datosDatoCalculado
                With CType(pt.PivotFields(dato), Excel.PivotField)
                    .NumberFormat = "$ #.##0;[Rojo]$ #.##0"
                    .LayoutBlankLine = True
                End With
            Next

            Try
                pt.TableStyle2 = "PivotStyleMedium2"
            Catch : End Try
            pt.ShowTableStyleRowStripes = True
            pt.ShowTableStyleColumnStripes = True
            pt.SubtotalLocation(Excel.XlSubtototalLocationType.xlAtBottom)
            pt.NullString = "0"

            book.SaveAs(nombreArch & extArchivo)
            app.Quit()

            liberarRecursos(hojaDatos)
            liberarRecursos(hojaTabla)
            liberarRecursos(book)
            liberarRecursos(app)

            GC.Collect()

            dict.Add(datoResp.OK, True)
            dict.Add(datoResp.MSG, "Archivo generado exitosamente.")
        Catch ex As Exception
            dict.Add(datoResp.OK, False)
            dict.Add(datoResp.MSG, "Ha ocurrido un error al generar el archivo: " & vbCrLf & _
                                   "" & vbTab & ex.Message)
        End Try

        Return dict
    End Function

    ' <summary>
    ' Función que liberar los recursos utilizados para generar el archivo.
    ' </summary>
    ' <param name="obj">El objeto a ser anulado y liberado de memoria.</param>
    ' <remarks>Permite cerrar completamente los Procesos de Excel.</remarks>
    Private Sub liberarRecursos(ByVal obj As Object)
        Try
            Marshal.ReleaseComObject(obj)
            obj = Nothing
        Catch ex As Exception
            obj = Nothing
        Finally
            GC.Collect()
        End Try
    End Sub

End Class
