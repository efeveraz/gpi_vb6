﻿Public Class ClsClientes

    Public Function TraerCliente(ByVal strIdNegocio As String, _
                                 ByVal strRutCliente As String, _
                                 ByVal strNombreCliente As String, _
                                 ByVal strApellidoPaterno As String, _
                                 ByVal strApellidoMaterno As String, _
                                 ByVal strRazonSocial As String, _
                                 ByVal strTipoPersona As String, _
                                 ByVal strColumnas As String, _
                                 ByRef strRetorno As String) As DataSet

        Dim LDS_Clientes As New DataSet
        Dim LDS_Clientes_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento
        Dim strDescError As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Clientes_Buscar"
        Lstr_NombreTabla = "CLIENTES"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Float, 9).Value = IIf(strIdNegocio = "", DBNull.Value, CLng(0 & strIdNegocio))
            SQLCommand.Parameters.Add("pIdentificador", SqlDbType.VarChar, 12).Value = IIf(strRutCliente = "", DBNull.Value, strRutCliente)
            SQLCommand.Parameters.Add("pNombreCliente", SqlDbType.VarChar, 50).Value = IIf(strNombreCliente = "", DBNull.Value, strNombreCliente)
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Float, 6).Value = IIf(glngIdUsuario = 0, DBNull.Value, glngIdUsuario)
            'SQLCommand.Parameters.Add("pApellidoPaterno", SqlDbType.VarChar, 50).Value = IIf(strApellidoPaterno = "", DBNull.Value, strApellidoPaterno)
            'SQLCommand.Parameters.Add("pApellidoMaterno", SqlDbType.VarChar, 50).Value = IIf(strApellidoMaterno = "", DBNull.Value, strApellidoMaterno)
            'SQLCommand.Parameters.Add("pRazonSocial", SqlDbType.VarChar, 50).Value = IIf(strRazonSocial = "", DBNull.Value, strRazonSocial)
            'SQLCommand.Parameters.Add("pTipoPersona", SqlDbType.Char, 1).Value = strTipoPersona

            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()
            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Clientes, Lstr_NombreTabla)

            LDS_Clientes_Aux = LDS_Clientes

            If strDescError.ToUpper.Trim <> "OK" Then
                gFun_ResultadoMsg("2|" & strDescError, "Buscar Cuentas")
                strRetorno = "OK"
                LDS_Clientes_Aux = Nothing
                Else
                If strColumnas.Trim = "" Then
                    LDS_Clientes_Aux = LDS_Clientes
                Else
                    LDS_Clientes_Aux = LDS_Clientes.Copy
                    For Each Columna In LDS_Clientes.Tables(Lstr_NombreTabla).Columns
                        Remove = True
                        For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                            LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                            If Columna.ColumnName = LInt_NomCol Then
                                Remove = False
                            End If
                        Next
                        If Remove Then
                            LDS_Clientes_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                        End If
                    Next
                End If

                For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                    LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                    For Each Columna In LDS_Clientes_Aux.Tables(Lstr_NombreTabla).Columns
                        If Columna.ColumnName = LInt_NomCol Then
                            Columna.SetOrdinal(LInt_Col)
                            Exit For
                        End If
                    Next
                Next

                strRetorno = "OK"
            End If
            Return LDS_Clientes_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function
    Public Function TraerCuenta_GPI_x_FFMM(ByVal strRutCliente As String, _
                                           ByVal intIdInstrumento As Integer, _
                                           ByVal strCtaExterna As String, _
                                           ByVal strColumnas As String, _
                                           ByRef strRetorno As String) As DataSet

        Dim LDS_Clientes As New DataSet
        Dim LDS_Clientes_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento
        Dim strDescError As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Cta_GPI_x_FFMM"
        Lstr_NombreTabla = "CUENTAS"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()


            SQLCommand.Parameters.Add("pIdentificador", SqlDbType.VarChar, 12).Value = IIf(strRutCliente = "", DBNull.Value, strRutCliente)
            SQLCommand.Parameters.Add("pIdInstrumento", SqlDbType.Int).Value = intIdInstrumento
            SQLCommand.Parameters.Add("pCtaExterna", SqlDbType.VarChar, 10).Value = strCtaExterna

            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()
            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Clientes, Lstr_NombreTabla)

            LDS_Clientes_Aux = LDS_Clientes

            If strDescError.ToUpper.Trim <> "OK" Then
                strRetorno = strDescError.ToUpper.Trim
                LDS_Clientes_Aux = Nothing
            Else
                If strColumnas.Trim = "" Then
                    LDS_Clientes_Aux = LDS_Clientes
                Else
                    LDS_Clientes_Aux = LDS_Clientes.Copy
                    For Each Columna In LDS_Clientes.Tables(Lstr_NombreTabla).Columns
                        Remove = True
                        For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                            LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                            If Columna.ColumnName = LInt_NomCol Then
                                Remove = False
                            End If
                        Next
                        If Remove Then
                            LDS_Clientes_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                        End If
                    Next
                End If

                For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                    LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                    For Each Columna In LDS_Clientes_Aux.Tables(Lstr_NombreTabla).Columns
                        If Columna.ColumnName = LInt_NomCol Then
                            Columna.SetOrdinal(LInt_Col)
                            Exit For
                        End If
                    Next
                Next
                strRetorno = "OK"
            End If
            Return LDS_Clientes_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function TraerRepresentantes(ByVal strIdCliente As String, _
                                        ByVal strColumnas As String, _
                                        ByRef strRetorno As String) As DataSet

        Dim LDS_Representates As New DataSet
        Dim LDS_Representantes_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Representante_Buscar"
        Lstr_NombreTabla = "REPRESENTANTES"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdCliente", SqlDbType.Float, 9).Value = IIf(strIdCliente = "", DBNull.Value, CLng(0 & strIdCliente))

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Representates, Lstr_NombreTabla)

            LDS_Representantes_Aux = LDS_Representates


            If strColumnas.Trim = "" Then
                LDS_Representantes_Aux = LDS_Representates
            Else
                LDS_Representantes_Aux = LDS_Representates.Copy
                For Each Columna In LDS_Representates.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Representantes_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Representantes_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            Return LDS_Representantes_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function TraerClientexId(ByVal lngIdCliente As Long, _
                                    ByVal strColumnas As String, _
                                    ByRef strRetorno As String) As DataSet

        Dim LDS_Clientes As New DataSet
        Dim LDS_Clientes_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Clientes_BuscarPorId"
        Lstr_NombreTabla = "CLIENTES"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdCliente", SqlDbType.Float, 10).Value = IIf(lngIdCliente = 0, DBNull.Value, lngIdCliente)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Clientes, Lstr_NombreTabla)

            LDS_Clientes_Aux = LDS_Clientes

            If strColumnas.Trim = "" Then
                LDS_Clientes_Aux = LDS_Clientes
            Else
                LDS_Clientes_Aux = LDS_Clientes.Copy
                For Each Columna In LDS_Clientes.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Clientes_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Clientes_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            Return LDS_Clientes_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function Cliente_Mantenedor_Full(ByVal strAccion As String, _
                                            ByRef strId_Cliente As String, _
                                            ByVal strId_Persona As String, _
                                            ByVal strIdentificador As String, _
                                            ByVal strNombre As String, _
                                            ByVal intId_Tipo_Estado As Integer, _
                                            ByVal strCod_Estado As String, _
                                            ByVal intId_Tipo_Referido As Integer, _
                                            ByVal intId_Contrato_Nupcial As Integer, _
                                            ByVal strCod_Sucursal As String, _
                                            ByVal strCod_Ejecutivo As String, _
                                            ByVal intEstado_Civil As Integer, _
                                            ByVal strObservaciones As String, _
                                            ByVal strFlg_Cliente_Bancario As String, _
                                            ByVal strOrigen_Referido As String, _
                                            ByVal strContacto_Referido As String, _
                                            ByVal strFlg_Contrato_Bpi As String, _
                                            ByVal intSegmento_Cliente As Integer, _
                                            ByVal strNum_Cta_Cte As String, _
                                            ByVal strCod_Moneda As String, _
                                            ByVal txtRelacionBanco As String, _
                                            ByVal txtDirecciones As String, _
                                            ByVal txtTelefonos As String, _
                                            ByVal txtEMails As String, _
                                            ByVal txtCuentaCorriente As String, _
                                            ByVal txtAsesores As String, _
                                            ByVal txtFormaOperar As String, _
                                            ByVal txtChecklist As String, _
                                            ByVal txtAporteInicial As String, _
                                            ByVal txtAlias As String, _
                                            ByVal txtProcedenciaFondos As String, _
                                            ByVal txtRestriccionOP As String, _
                                            ByVal txtPersonaAutOrden As String, _
                                            ByVal txtAutRetirarDoc As String, _
                                            ByVal txtRelacionados As String, _
                                            ByVal txtObservaciones As String) As String

        Dim strDescError As String = "OK"
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction
        Dim intId_Cliente As Integer = CLng("0" & strId_Cliente)
        Dim intId_Usuario As Integer = gDeclaracionesRecursosComunes.glngIdUsuario
        Dim intId_Negocio As Integer = gDeclaracionesRecursosComunes.glngNegocioOperacion

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_Cliente_Mantencion_Full", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Cliente_Mantencion_Full"
            SQLCommand.Parameters.Clear()


            SQLCommand.Parameters.Add("@pAccion", SqlDbType.VarChar, 10).Value = strAccion
            Dim ParametroSal1 As New SqlClient.SqlParameter("Pid_cliente", SqlDbType.Float, 10)
            ParametroSal1.Value = intId_Cliente
            ParametroSal1.Direction = Data.ParameterDirection.InputOutput
            SQLCommand.Parameters.Add(ParametroSal1)
            SQLCommand.Parameters.Add("@pId_persona", SqlDbType.Float, 10).Value = IIf(strId_Persona.Trim = "", DBNull.Value, strId_Persona)
            SQLCommand.Parameters.Add("@PIdentificador", SqlDbType.VarChar, 12).Value = IIf(strIdentificador.Trim = "", DBNull.Value, strIdentificador)
            SQLCommand.Parameters.Add("@Pnombre", SqlDbType.VarChar, 100).Value = IIf(strNombre.Trim = "", DBNull.Value, strNombre)
            SQLCommand.Parameters.Add("@Pid_tipo_estado", SqlDbType.Float, 4).Value = 1
            SQLCommand.Parameters.Add("@Pcod_estado", SqlDbType.VarChar, 3).Value = IIf(strCod_Estado.Trim = "", DBNull.Value, strCod_Estado)
            SQLCommand.Parameters.Add("@Pid_tipo_referido", SqlDbType.Float, 2).Value = IIf(intId_Tipo_Referido = 0, DBNull.Value, intId_Tipo_Referido)
            SQLCommand.Parameters.Add("@Pid_contrato_nupcial", SqlDbType.Float, 3).Value = IIf(intId_Contrato_Nupcial = 0, DBNull.Value, intId_Contrato_Nupcial)
            SQLCommand.Parameters.Add("@Pcod_sucursal", SqlDbType.VarChar, 10).Value = IIf(strCod_Sucursal.Trim = "", DBNull.Value, strCod_Sucursal)
            SQLCommand.Parameters.Add("@Pcod_ejecutivo", SqlDbType.VarChar, 10).Value = IIf(strCod_Ejecutivo.Trim = "", DBNull.Value, strCod_Ejecutivo)
            SQLCommand.Parameters.Add("@Pestado_civil", SqlDbType.Float, 3).Value = IIf(intEstado_Civil = 0, DBNull.Value, intEstado_Civil)
            SQLCommand.Parameters.Add("@Pobservaciones", SqlDbType.VarChar, 150).Value = IIf(strObservaciones.Trim = "", DBNull.Value, strObservaciones)
            SQLCommand.Parameters.Add("@Pflg_cliente_bancario", SqlDbType.VarChar, 1).Value = IIf(strFlg_Cliente_Bancario.Trim = "", DBNull.Value, strFlg_Cliente_Bancario)
            SQLCommand.Parameters.Add("@Porigen_referido", SqlDbType.VarChar, 100).Value = IIf(strOrigen_Referido.Trim = "", DBNull.Value, strOrigen_Referido)
            SQLCommand.Parameters.Add("@Pcontacto_referido", SqlDbType.VarChar, 100).Value = IIf(strContacto_Referido.Trim = "", DBNull.Value, strContacto_Referido)
            SQLCommand.Parameters.Add("@Pflg_contrato_bpi", SqlDbType.VarChar, 1).Value = IIf(strFlg_Contrato_Bpi.Trim = "", DBNull.Value, strFlg_Contrato_Bpi)
            SQLCommand.Parameters.Add("@Psegmento_cliente", SqlDbType.Float, 3).Value = IIf(intSegmento_Cliente = 0, DBNull.Value, intSegmento_Cliente)
            SQLCommand.Parameters.Add("@Pnum_cta_cte", SqlDbType.VarChar, 20).Value = IIf(strNum_Cta_Cte.Trim = "", DBNull.Value, strNum_Cta_Cte)
            SQLCommand.Parameters.Add("@Pcod_Moneda", SqlDbType.VarChar, 3).Value = IIf(strCod_Moneda.Trim = "", DBNull.Value, strCod_Moneda)
            SQLCommand.Parameters.Add("@Pid_usuario", SqlDbType.Float, 5).Value = intId_Usuario
            SQLCommand.Parameters.Add("@pid_negocio", SqlDbType.Float, 10).Value = intId_Negocio
            SQLCommand.Parameters.Add("@pRelacionBanco", SqlDbType.Text).Value = txtRelacionBanco.Trim
            SQLCommand.Parameters.Add("@pDirecciones", SqlDbType.Text).Value = txtDirecciones.Trim
            SQLCommand.Parameters.Add("@pTelefonos", SqlDbType.Text).Value = txtTelefonos.Trim
            SQLCommand.Parameters.Add("@pEMails", SqlDbType.Text).Value = txtEMails.Trim
            SQLCommand.Parameters.Add("@pCuentaCorriente", SqlDbType.Text).Value = txtCuentaCorriente.Trim
            SQLCommand.Parameters.Add("@pASesores", SqlDbType.Text).Value = txtAsesores.Trim
            SQLCommand.Parameters.Add("@pFormaOperar", SqlDbType.Text).Value = txtFormaOperar.Trim
            SQLCommand.Parameters.Add("@pChecklist", SqlDbType.Text).Value = txtChecklist.Trim
            SQLCommand.Parameters.Add("@pAporteInicial", SqlDbType.Text).Value = txtAporteInicial.Trim
            SQLCommand.Parameters.Add("@pAlias", SqlDbType.Text).Value = txtAlias.Trim
            SQLCommand.Parameters.Add("@pProcedenciaFondos", SqlDbType.Text).Value = txtProcedenciaFondos.Trim
            SQLCommand.Parameters.Add("@pRestriccionOP", SqlDbType.Text).Value = txtRestriccionOP.Trim
            SQLCommand.Parameters.Add("@pPersonasAutOrden", SqlDbType.Text).Value = txtPersonaAutOrden.Trim
            SQLCommand.Parameters.Add("@pAutorizarRetirarDocumentos", SqlDbType.Text).Value = txtAutRetirarDoc.Trim
            SQLCommand.Parameters.Add("@pRelacionados", SqlDbType.Text).Value = txtRelacionados.Trim

            '...Resultado
            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()
            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
                strId_Cliente = SQLCommand.Parameters("Pid_cliente").Value.ToString.Trim
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en  " & vbCr & ex.Message
        Finally
            SQLConnect.Cerrar()
        End Try

        Return strDescError
    End Function

    Public Function Cliente_Mantenedor(ByVal strAccion As String, _
                                           ByRef strId_Cliente As String, _
                                           ByRef intId_Persona As Integer, _
                                           ByVal strIdentificador As String, _
                                           ByVal strNombre As String, _
                                           ByVal intId_Tipo_Estado As Integer, _
                                           ByVal strCod_Estado As String, _
                                           ByVal intId_Tipo_Referido As Integer, _
                                           ByVal intId_Contrato_Nupcial As Integer, _
                                           ByVal strCod_Sucursal As String, _
                                           ByVal strCod_Ejecutivo As String, _
                                           ByVal intEstado_Civil As Integer, _
                                           ByVal strObservaciones As String, _
                                           ByVal strFlg_Cliente_Bancario As String, _
                                           ByVal strOrigen_Referido As String, _
                                           ByVal strContacto_Referido As String, _
                                           ByVal strFlg_Contrato_Bpi As String, _
                                           ByVal intSegmento_Cliente As Integer, _
                                           ByVal strNum_Cta_Cte As String, _
                                           ByVal strCod_Moneda As String) As String

        Dim strDescError As String = "OK"
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction
        Dim intId_Cliente As Integer = CLng("0" & strId_Cliente)
        Dim intId_Usuario As Integer = gDeclaracionesRecursosComunes.glngIdUsuario


        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_Cliente_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Cliente_Mantencion"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 10).Value = strAccion

            Dim ParametroSal1 As New SqlClient.SqlParameter("Pid_cliente", SqlDbType.Float, 10)
            ParametroSal1.Value = intId_Cliente
            ParametroSal1.Direction = Data.ParameterDirection.InputOutput
            SQLCommand.Parameters.Add(ParametroSal1)

            SQLCommand.Parameters.Add("Pid_Persona ", SqlDbType.Float, 10).Value = intId_Persona
            SQLCommand.Parameters.Add("PIdentificador ", SqlDbType.VarChar, 10).Value = IIf(strIdentificador.Trim = "", DBNull.Value, strIdentificador)
            SQLCommand.Parameters.Add("Pnombre", SqlDbType.VarChar, 100).Value = IIf(strNombre.Trim = "", DBNull.Value, strNombre)
            SQLCommand.Parameters.Add("Pid_tipo_estado", SqlDbType.Float, 4).Value = 1
            SQLCommand.Parameters.Add("Pcod_estado", SqlDbType.VarChar, 3).Value = IIf(strCod_Estado.Trim = "", DBNull.Value, strCod_Estado)
            SQLCommand.Parameters.Add("Pid_tipo_referido", SqlDbType.Float, 2).Value = IIf(intId_Tipo_Referido = 0, DBNull.Value, intId_Tipo_Referido)
            SQLCommand.Parameters.Add("Pid_contrato_nupcial", SqlDbType.Float, 3).Value = IIf(intId_Contrato_Nupcial = 0, DBNull.Value, intId_Contrato_Nupcial)
            SQLCommand.Parameters.Add("Pcod_sucursal", SqlDbType.VarChar, 10).Value = IIf(strCod_Sucursal.Trim = "", DBNull.Value, strCod_Sucursal)
            SQLCommand.Parameters.Add("Pcod_ejecutivo", SqlDbType.VarChar, 10).Value = IIf(strCod_Ejecutivo.Trim = "", DBNull.Value, strCod_Ejecutivo)
            SQLCommand.Parameters.Add("Pestado_civil", SqlDbType.Float, 3).Value = IIf(intEstado_Civil = 0, DBNull.Value, intEstado_Civil)
            SQLCommand.Parameters.Add("Pobservaciones", SqlDbType.VarChar, 150).Value = IIf(strObservaciones.Trim = "", DBNull.Value, strObservaciones)
            SQLCommand.Parameters.Add("Pflg_cliente_bancario", SqlDbType.VarChar, 1).Value = IIf(strFlg_Cliente_Bancario.Trim = "", DBNull.Value, strFlg_Cliente_Bancario)
            SQLCommand.Parameters.Add("Porigen_referido", SqlDbType.VarChar, 100).Value = IIf(strOrigen_Referido.Trim = "", DBNull.Value, strOrigen_Referido)
            SQLCommand.Parameters.Add("Pcontacto_referido", SqlDbType.VarChar, 100).Value = IIf(strContacto_Referido.Trim = "", DBNull.Value, strContacto_Referido)
            SQLCommand.Parameters.Add("Pflg_contrato_bpi", SqlDbType.VarChar, 1).Value = IIf(strFlg_Contrato_Bpi.Trim = "", DBNull.Value, strFlg_Contrato_Bpi)
            SQLCommand.Parameters.Add("Psegmento_cliente", SqlDbType.Float, 3).Value = IIf(intSegmento_Cliente = 0, DBNull.Value, intSegmento_Cliente)
            SQLCommand.Parameters.Add("Pnum_cta_cte", SqlDbType.VarChar, 20).Value = IIf(strNum_Cta_Cte.Trim = "", DBNull.Value, strNum_Cta_Cte)
            SQLCommand.Parameters.Add("Pcod_Moneda", SqlDbType.VarChar, 3).Value = IIf(strCod_Moneda.Trim = "", DBNull.Value, strCod_Moneda)
            SQLCommand.Parameters.Add("Pid_usuario", SqlDbType.Float, 5).Value = intId_Usuario

            '...Resultado
            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()
            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
                strId_Cliente = SQLCommand.Parameters("Pid_cliente").Value.ToString.Trim
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en  " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            Cliente_Mantenedor = strDescError
        End Try

    End Function

    Public Function ContratoNupcial_Ver(ByVal strCodigo As String, _
                         ByVal strColumnas As String, _
                         ByRef strRetorno As String) As DataSet

        Dim LDS_ContratoNupcial As New DataSet
        Dim LDS_ContratoNupcial_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_ContratoNupcial_Consultar"
        Lstr_NombreTabla = "ContratoNupcial"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdContratoNupcial", SqlDbType.VarChar, 3).Value = IIf(strCodigo.Trim = "", DBNull.Value, strCodigo.Trim)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_ContratoNupcial, Lstr_NombreTabla)

            LDS_ContratoNupcial_Aux = LDS_ContratoNupcial

            If strColumnas.Trim = "" Then
                LDS_ContratoNupcial_Aux = LDS_ContratoNupcial
            Else
                LDS_ContratoNupcial_Aux = LDS_ContratoNupcial.Copy
                For Each Columna In LDS_ContratoNupcial.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_ContratoNupcial_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_ContratoNupcial_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_ContratoNupcial.Dispose()
            Return LDS_ContratoNupcial_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function EstadoCivil_Ver(ByVal strCodigo As String, _
                         ByVal strColumnas As String, _
                         ByRef strRetorno As String) As DataSet

        Dim LDS_EstadoCivil As New DataSet
        Dim LDS_EstadoCivil_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_EstadoCivil_Consultar"
        Lstr_NombreTabla = "EstadoCivil"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdEstadoCivil", SqlDbType.VarChar, 3).Value = IIf(strCodigo.Trim = "", DBNull.Value, strCodigo.Trim)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_EstadoCivil, Lstr_NombreTabla)

            LDS_EstadoCivil_Aux = LDS_EstadoCivil

            If strColumnas.Trim = "" Then
                LDS_EstadoCivil_Aux = LDS_EstadoCivil
            Else
                LDS_EstadoCivil_Aux = LDS_EstadoCivil.Copy
                For Each Columna In LDS_EstadoCivil.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_EstadoCivil_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_EstadoCivil_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_EstadoCivil.Dispose()
            Return LDS_EstadoCivil_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function Segmento_Ver(ByVal strCodigo As String, _
                         ByVal strColumnas As String, _
                         ByRef strRetorno As String) As DataSet

        Dim LDS_Segmento As New DataSet
        Dim LDS_Segmento_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Segmento_Consultar"
        Lstr_NombreTabla = "Segmento"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdSegmento", SqlDbType.VarChar, 3).Value = IIf(strCodigo.Trim = "", DBNull.Value, strCodigo.Trim)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Segmento, Lstr_NombreTabla)

            LDS_Segmento_Aux = LDS_Segmento

            If strColumnas.Trim = "" Then
                LDS_Segmento_Aux = LDS_Segmento
            Else
                LDS_Segmento_Aux = LDS_Segmento.Copy
                For Each Columna In LDS_Segmento.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Segmento_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Segmento_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_Segmento.Dispose()
            Return LDS_Segmento_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function RestriccionOP_Mantenedor(ByVal strId_Cliente As String, ByRef dstRestriccionOP As DataSet) As String

        Dim strDescError As String = "OK"
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try

            '       ELIMINAMOS LAS RESTRICCIONES YA EXISTENTES

            SQLCommand = New SqlClient.SqlCommand("Rcp_RestriccionOP_MantencionPorCliente", MiTransaccionSQL.Connection, MiTransaccionSQL)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_RestriccionOP_MantencionPorCliente"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = "ELIMINAR"
            SQLCommand.Parameters.Add("pIdRestriccionOP", SqlDbType.Float, 6).Value = DBNull.Value
            SQLCommand.Parameters.Add("pIdCliente", SqlDbType.Float, 10).Value = strId_Cliente
            '...Resultado
            Dim pSalElimIntPort As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalElimIntPort.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalElimIntPort)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            If Not strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Rollback()
                Exit Function
            End If

            '       INSERTAMOS LaS NUEVaS RESTRICCION OPERACION

            If dstRestriccionOP.Tables(0).Rows.Count <> Nothing Then
                For Each pRow As DataRow In dstRestriccionOP.Tables(0).Rows
                    If (pRow("INCLUIDO").ToString = "1") Then
                        SQLCommand = New SqlClient.SqlCommand("Rcp_RestriccionOP_MantencionPorCliente", MiTransaccionSQL.Connection, MiTransaccionSQL)
                        SQLCommand.CommandType = CommandType.StoredProcedure
                        SQLCommand.CommandText = "Rcp_RestriccionOP_MantencionPorCliente"
                        SQLCommand.Parameters.Clear()

                        SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = "INSERTAR"
                        SQLCommand.Parameters.Add("pIdRestriccionOP", SqlDbType.Float, 6).Value = pRow("ID_RESTRICCION_OPERACION").ToString
                        SQLCommand.Parameters.Add("pIdCliente", SqlDbType.Float, 10).Value = strId_Cliente

                        '...Resultado
                        Dim pSalInstPort As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                        pSalInstPort.Direction = Data.ParameterDirection.Output
                        SQLCommand.Parameters.Add(pSalInstPort)

                        SQLCommand.ExecuteNonQuery()

                        strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
                    End If
                    If Not strDescError.ToUpper.Trim = "OK" Then
                        Exit For
                    End If
                Next
            End If

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            RestriccionOP_Mantenedor = strDescError
        End Try
    End Function

    Public Function BuscarRestriccionOPPorCliente(ByVal strIdCliente As String, _
                                                      ByVal strColumnas As String, _
                                                      ByRef strRetorno As String) As DataSet

        Dim LDS_Banco As New DataSet
        Dim LDS_Banco_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_RestriccionOP_BuscarPorCte"
        Lstr_NombreTabla = "RestriccionOP"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdCliente", SqlDbType.VarChar, 9).Value = IIf(strIdCliente.Trim = "", DBNull.Value, strIdCliente.Trim)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Banco, Lstr_NombreTabla)

            LDS_Banco_Aux = LDS_Banco

            If strColumnas.Trim = "" Then
                LDS_Banco_Aux = LDS_Banco
            Else
                LDS_Banco_Aux = LDS_Banco.Copy
                For Each Columna In LDS_Banco.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Banco_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Banco_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_Banco.Dispose()
            Return LDS_Banco_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function
    Public Function TraerRutAutomatico(ByVal pTipoIdentificacion As String) As String
        Dim strDescError As String = ""
        Dim lstrValor As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion

        '...Abre la conexion
        SQLConnect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_Cliente_RutAutomatico", SQLConnect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Cliente_RutAutomatico"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pTipoIdentificacion", SqlDbType.VarChar, 1).Value = pTipoIdentificacion

            '...pCantidad
            Dim ParametroSal As New SqlClient.SqlParameter("pRutAutomatico", SqlDbType.VarChar, 10)
            ParametroSal.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal)

            SQLCommand.ExecuteNonQuery()

            lstrValor = SQLCommand.Parameters("pRutAutomatico").Value

        Catch Ex As Exception
            strDescError = "|1|Error al obtener el rut automático." & vbCr & "[" & Ex.Message & "]|"
        Finally
            If strDescError <> "OK" Then
                If Not gFun_ResultadoMsg(strDescError, "TraerRutAutomatico") Then
                    lstrValor = ""
                End If
            End If
            SQLConnect.Cerrar()
            TraerRutAutomatico = lstrValor
        End Try
    End Function
    Public Function Clientes_Consultar(ByVal strIdNegocio As String, _
                                       ByVal strTipoPersona As String, _
                                       ByVal strColumnas As String, _
                                       ByRef strRetorno As String, _
                                       Optional ByVal lngIdCliente As Long = 0) As DataSet

        Dim LDS_Clientes As New DataSet
        Dim LDS_Clientes_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento
        Dim strDescError As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Clientes_Consultar"
        Lstr_NombreTabla = "CLIENTES"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Float, 9).Value = IIf(strIdNegocio = "", DBNull.Value, CLng(0 & strIdNegocio))
            SQLCommand.Parameters.Add("pTipoPersona", SqlDbType.Char, 1).Value = IIf(strTipoPersona = "", DBNull.Value, strTipoPersona)
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Float, 6).Value = IIf(glngIdUsuario = 0, DBNull.Value, glngIdUsuario)
            SQLCommand.Parameters.Add("pIdCliente", SqlDbType.Float, 9).Value = IIf(lngIdCliente = 0, DBNull.Value, lngIdCliente)

            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()
            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Clientes, Lstr_NombreTabla)

            LDS_Clientes_Aux = LDS_Clientes

            If strDescError.ToUpper.Trim <> "OK" Then
                gFun_ResultadoMsg("2|" & strDescError, "Buscar Cuentas")
                strRetorno = "OK"
                LDS_Clientes_Aux = Nothing
            Else
                If strColumnas.Trim = "" Then
                    LDS_Clientes_Aux = LDS_Clientes
                Else
                    LDS_Clientes_Aux = LDS_Clientes.Copy
                    For Each Columna In LDS_Clientes.Tables(Lstr_NombreTabla).Columns
                        Remove = True
                        For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                            LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                            If Columna.ColumnName = LInt_NomCol Then
                                Remove = False
                            End If
                        Next
                        If Remove Then
                            LDS_Clientes_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                        End If
                    Next
                End If

                For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                    LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                    For Each Columna In LDS_Clientes_Aux.Tables(Lstr_NombreTabla).Columns
                        If Columna.ColumnName = LInt_NomCol Then
                            Columna.SetOrdinal(LInt_Col)
                            Exit For
                        End If
                    Next
                Next

                strRetorno = "OK"
            End If
            Return LDS_Clientes_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function


    Public Function Clientes_Consultar2(ByVal strIdNegocio As String, _
                                   ByVal strTipoPersona As String, _
                                   ByVal strColumnas As String, _
                                   ByRef strRetorno As String, _
                                   ByVal strLista As String, _
                                   Optional ByVal lngIdCliente As Long = 0) As DataSet

        Dim LDS_Clientes As New DataSet
        Dim LDS_Clientes_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento
        Dim strDescError As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Clientes_Consultar2"
        Lstr_NombreTabla = "CLIENTES"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Float, 9).Value = IIf(strIdNegocio = "", DBNull.Value, CLng(0 & strIdNegocio))
            SQLCommand.Parameters.Add("pTipoPersona", SqlDbType.Char, 1).Value = IIf(strTipoPersona = "", DBNull.Value, strTipoPersona)
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Float, 9).Value = IIf(glngIdUsuario = 0, DBNull.Value, glngIdUsuario)
            'SQLCommand.Parameters.Add("pIdCliente", SqlDbType.Float, 6).Value = IIf(lngIdCliente = 0, DBNull.Value, lngIdCliente)
            SQLCommand.Parameters.Add("pListaCuentas", SqlDbType.Text, 60000).Value = strLista


            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            'SQLCommand.ExecuteNonQuery()
            'strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Clientes, Lstr_NombreTabla)
            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            LDS_Clientes_Aux = LDS_Clientes

            If strDescError.ToUpper.Trim <> "OK" Then
                gFun_ResultadoMsg("2|" & strDescError, "Buscar Cuentas")
                strRetorno = "OK"
                LDS_Clientes_Aux = Nothing
            Else
                If strColumnas.Trim = "" Then
                    LDS_Clientes_Aux = LDS_Clientes
                Else
                    LDS_Clientes_Aux = LDS_Clientes.Copy
                    For Each Columna In LDS_Clientes.Tables(Lstr_NombreTabla).Columns
                        Remove = True
                        For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                            LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                            If Columna.ColumnName = LInt_NomCol Then
                                Remove = False
                            End If
                        Next
                        If Remove Then
                            LDS_Clientes_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                        End If
                    Next
                End If

                For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                    LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                    For Each Columna In LDS_Clientes_Aux.Tables(Lstr_NombreTabla).Columns
                        If Columna.ColumnName = LInt_NomCol Then
                            Columna.SetOrdinal(LInt_Col)
                            Exit For
                        End If
                    Next
                Next

                strRetorno = "OK"
            End If
            Return LDS_Clientes_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function


End Class
