﻿Public Class ClsSorteoLetras
  Public Function Trae_Cuentas_E_Instrumento(ByVal strNemotecnico As String, _
                                             ByVal dteFecha As Date, _
                                             ByVal strColumnas As String, _
                                             ByRef strRetorno As String) As DataTable
    Dim ldsDataTable As New DataTable
    Dim LstrProcedimiento As String

    Dim SQLCommand As New SqlClient.SqlCommand
    Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

    Dim Connect As Cls_Conexion = New Cls_Conexion

    LstrProcedimiento = "Rcp_SorteoLetras_Trae_Cuentas_Por_Instrumento"

    Connect.Abrir()

    Try
      SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

      SQLCommand.CommandType = CommandType.StoredProcedure
      SQLCommand.CommandText = LstrProcedimiento
      SQLCommand.Parameters.Clear()

      SQLCommand.Parameters.Add("pNemotecnico", SqlDbType.VarChar, 50).Value = strNemotecnico
      SQLCommand.Parameters.Add("PFECHA", SqlDbType.VarChar, 10).Value = dteFecha.ToString("yyyyMMdd")

      SQLDataAdapter.SelectCommand = SQLCommand
      SQLDataAdapter.Fill(ldsDataTable)

      'HAF 20101124: Configura el datatable segun lo informado en strColumnas
      Call gsubConfiguraDataTable(dtbTabla:=ldsDataTable, strColumnas:=strColumnas)

      strRetorno = "OK"
      Return ldsDataTable

    Catch ex As Exception
      strRetorno = ex.Message
      Return Nothing
    Finally
      Connect.Cerrar()
    End Try
    End Function
    Public Function Trae_Carteras_Sorteo_o_Prepago(ByVal strNemotecnico As String, _
                                           ByVal strOperacion As String, _
                                           ByVal dteFecha As Date, _
                                           ByVal strColumnas As String, _
                                           ByRef strRetorno As String) As DataTable
        Dim ldsDataTable As New DataTable
        Dim LstrProcedimiento As String

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Carteras_Sorteo_o_Prepago"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pNemotecnico", SqlDbType.VarChar, 50).Value = strNemotecnico
            SQLCommand.Parameters.Add("PFECHA", SqlDbType.VarChar, 10).Value = dteFecha.ToString("yyyyMMdd")
            SQLCommand.Parameters.Add("PTIPO_OPER", SqlDbType.VarChar, 10).Value = strOperacion

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(ldsDataTable)

            'HAF 20101124: Configura el datatable segun lo informado en strColumnas
            Call gsubConfiguraDataTable(dtbTabla:=ldsDataTable, strColumnas:=strColumnas)

            strRetorno = "OK"
            Return ldsDataTable

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function Consultar_Carteras_Sorteo_Prepago(ByVal strNemotecnico As String, _
                                           ByVal strOperacion As String, _
                                           ByVal dteFecha As Date, _
                                           ByVal strNumCta As String, _
                                           ByVal strCustodio As String, _
                                           ByVal strColumnas As String, _
                                           ByRef strRetorno As String) As DataTable
        Dim ldsDataTable As New DataTable
        Dim LstrProcedimiento As String

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "rcp_carga_sorteo_prepago"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pNemotecnico", SqlDbType.VarChar, 50).Value = strNemotecnico
            SQLCommand.Parameters.Add("PFECHA", SqlDbType.VarChar, 10).Value = dteFecha
            SQLCommand.Parameters.Add("PTIPO_OPER", SqlDbType.VarChar, 10).Value = strOperacion
            SQLCommand.Parameters.Add("PNUMCUENTA", SqlDbType.VarChar, 50).Value = strNumCta
            SQLCommand.Parameters.Add("PCUSTODIO", SqlDbType.VarChar, 50).Value = strCustodio

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(ldsDataTable)

            'HAF 20101124: Configura el datatable segun lo informado en strColumnas
            Call gsubConfiguraDataTable(dtbTabla:=ldsDataTable, strColumnas:=strColumnas)

            strRetorno = "OK"
            Return ldsDataTable

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

End Class
