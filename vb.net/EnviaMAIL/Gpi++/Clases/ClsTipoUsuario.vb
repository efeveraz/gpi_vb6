﻿Public Class ClsTipoUsuario
    Public Function RetornaTiposUsuarios(ByRef strRetorno As String, _
                                         ByVal strColumnas As String, _
                                         ByVal intIdTipoUsuario As Integer, _
                                         ByVal strNombreTipoUsuario As String, _
                                         ByVal strNombreCortoTipoUsuario As String, _
                                         ByVal strCodigoExternoTipoUsuario As String, _
                                         ByVal strEstadoTipoUsuario As String) As DataSet

        Dim lblnRemove As Boolean = True
        Dim larr_NombreColumna() As String = Split(strColumnas, ",")
        Dim lInt_Col As Integer = 0
        Dim lInt_NomCol As String = ""

        Dim lDS_TipoUsuario As New DataSet
        Dim lDS_TipoUsuario_Aux As New DataSet

        Dim Sqlcommand As New SqlClient.SqlCommand
        Dim SqlDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim LstrProcedimiento As String = ""
        Dim LstrNombreTabla As String = ""


        'Lstr_Procedimiento = "SGC_SP_CONSULTA_TIPOUSUARIO"
        LstrProcedimiento = "Sis_TipoUsuario_Consultar"
        LstrNombreTabla = "TIPOUSUARIO"
        Connect.Abrir()

        'Abre la conexion 
        Connect.Abrir()

        Try
            Sqlcommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            Sqlcommand.CommandType = CommandType.StoredProcedure
            Sqlcommand.CommandText = LstrProcedimiento
            Sqlcommand.Parameters.Clear()

            'If intIdTipoUsuario <> 0 Then
            Sqlcommand.Parameters.Add("@pIdTipoUsuario", SqlDbType.Int).Value = IIf(intIdTipoUsuario = 0, DBNull.Value, intIdTipoUsuario)
            'End If

            'If strNombreTipoUsuario <> "" Then
            Sqlcommand.Parameters.Add("@pNombreTipoUsuario", SqlDbType.Int).Value = IIf(strNombreTipoUsuario.Trim = "", DBNull.Value, strNombreTipoUsuario.Trim)
            'End If

            'If strNombreCortoTipoUsuario <> "" Then
            Sqlcommand.Parameters.Add("@pNombreCortoTipoUsuario", SqlDbType.Int).Value = IIf(strNombreCortoTipoUsuario.Trim = "", DBNull.Value, strNombreCortoTipoUsuario.Trim)
            'End If

            'If strEstadoTipoUsuario <> "" Then
            Sqlcommand.Parameters.Add("@pEstadoTipoUsuario", SqlDbType.Int).Value = IIf(strEstadoTipoUsuario.Trim = "", DBNull.Value, strEstadoTipoUsuario.Trim)
            'End If

            Dim oParamSal As New SqlClient.SqlParameter("@pMsjRetorno", SqlDbType.Char, 60)
            oParamSal.Direction = ParameterDirection.Output
            Sqlcommand.Parameters.Add(oParamSal)


            SqlDataAdapter.SelectCommand = Sqlcommand
            SqlDataAdapter.Fill(lDS_TipoUsuario, LstrNombreTabla)

            If strColumnas.Trim = "" Then
                lDS_TipoUsuario_Aux = lDS_TipoUsuario
            Else
                lDS_TipoUsuario_Aux = lDS_TipoUsuario.Copy
                For Each lstrColumna As DataColumn In lDS_TipoUsuario.Tables(LstrNombreTabla).Columns
                    lblnRemove = True
                    For lInt_Col = 0 To larr_NombreColumna.Length - 1
                        lInt_NomCol = larr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                        If lstrColumna.ColumnName = lInt_NomCol Then
                            lblnRemove = False
                        End If
                    Next
                    If lblnRemove Then
                        lDS_TipoUsuario_Aux.Tables(LstrNombreTabla).Columns.Remove(lstrColumna.ColumnName)
                    End If
                Next
            End If

            For lInt_Col = 0 To larr_NombreColumna.Length - 1
                lInt_NomCol = larr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                For Each lstrColumna As DataColumn In lDS_TipoUsuario_Aux.Tables(LstrNombreTabla).Columns
                    If lstrColumna.ColumnName = lInt_NomCol Then
                        lstrColumna.SetOrdinal(lInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = Sqlcommand.Parameters("@pMsjRetorno").Value.ToString.Trim

            Return lDS_TipoUsuario_Aux
        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function Insertar(ByRef intIdTipoUsuario As Integer, _
                             ByVal strNombreTipoUsuario As String, _
                             ByVal strNombreCortoTipoUsuario As String, _
                             ByVal strCodigoExternoTipoUsuario As String, _
                             ByVal strEstadoTipoUsuario As String) As String


        Dim Sqlcommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim MiTransaccionSQL As SqlClient.SqlTransaction
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim lstrProcedimiento As String = ""
        Dim lstrRetorno As String = "OK"

        lstrProcedimiento = "Sis_TipoUsuario_Mantencion"
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try

            Sqlcommand = New SqlClient.SqlCommand(lstrProcedimiento, MiTransaccionSQL.Connection, MiTransaccionSQL)
            Sqlcommand.CommandType = CommandType.StoredProcedure
            Sqlcommand.CommandText = lstrProcedimiento
            Sqlcommand.Parameters.Clear()

            Sqlcommand.Parameters.Add("@pCodigoOperacion", SqlDbType.Char, 10).Value = "INSERTAR"

            'IdTipoUsuario...(Identity)
            Dim ParametroIdUsuario As New SqlClient.SqlParameter("@pIdTipoUsuario", SqlDbType.Int)
            ParametroIdUsuario.Direction = Data.ParameterDirection.InputOutput
            Sqlcommand.Parameters.Add(ParametroIdUsuario)


            Sqlcommand.Parameters.Add("@pNombreTipoUsuario", SqlDbType.Char, 50).Value = IIf(strNombreTipoUsuario = "", DBNull.Value, strNombreTipoUsuario)
            Sqlcommand.Parameters.Add("@pNombreCortoTipoUsuario", SqlDbType.Char, 15).Value = IIf(strNombreCortoTipoUsuario = "", DBNull.Value, strNombreCortoTipoUsuario)
            Sqlcommand.Parameters.Add("@pCodigoExternoTipoUsuario", SqlDbType.Char, 20).Value = IIf(strCodigoExternoTipoUsuario = "", DBNull.Value, strCodigoExternoTipoUsuario)
            Sqlcommand.Parameters.Add("@pEstadoTipoUsuario", SqlDbType.Char, 200).Value = IIf(strEstadoTipoUsuario = "", DBNull.Value, strEstadoTipoUsuario)

            Dim oParamSal As New SqlClient.SqlParameter("@pMsjRetorno", SqlDbType.Char, 60)
            oParamSal.Direction = ParameterDirection.Output
            Sqlcommand.Parameters.Add(oParamSal)

            Sqlcommand.ExecuteNonQuery()

            lstrRetorno = Sqlcommand.Parameters("@pMsjRetorno").Value.ToString.Trim


            If lstrRetorno = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

            If lstrRetorno = "OK" Then
                intIdTipoUsuario = Sqlcommand.Parameters("@pIdTipoUsuario").Value
            End If
            Return lstrRetorno

        Catch ex As Exception
            lstrRetorno = ex.Message
            Return lstrRetorno
        Finally
            SQLConnect.Cerrar()
        End Try

    End Function

    Public Function Modificar(ByVal intIdTipoUsuario As Integer, _
                             ByVal strNombreTipoUsuario As String, _
                             ByVal strNombreCortoTipoUsuario As String, _
                             ByVal strCodigoExternoTipoUsuario As String, _
                             ByVal strEstadoTipoUsuario As String) As String


        Dim Sqlcommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim MiTransaccionSQL As SqlClient.SqlTransaction
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim lstrProcedimiento As String = ""
        Dim lstrRetorno As String = "OK"

        lstrProcedimiento = "Sis_TipoUsuario_Mantencion"
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try

            Sqlcommand = New SqlClient.SqlCommand(lstrProcedimiento, MiTransaccionSQL.Connection, MiTransaccionSQL)
            Sqlcommand.CommandType = CommandType.StoredProcedure
            Sqlcommand.CommandText = lstrProcedimiento
            Sqlcommand.Parameters.Clear()

            Sqlcommand.Parameters.Add("@pCodigoOperacion", SqlDbType.Char, 10).Value = "MODIFICAR"
            Sqlcommand.Parameters.Add("@pIdTipoUsuario", SqlDbType.Char, 50).Value = intIdTipoUsuario

            Sqlcommand.Parameters.Add("@pNombreTipoUsuario", SqlDbType.Char, 50).Value = IIf(strNombreTipoUsuario = "", DBNull.Value, strNombreTipoUsuario)
            Sqlcommand.Parameters.Add("@pNombreCortoTipoUsuario", SqlDbType.Char, 15).Value = IIf(strNombreCortoTipoUsuario = "", DBNull.Value, strNombreCortoTipoUsuario)
            Sqlcommand.Parameters.Add("@pCodigoExternoTipoUsuario", SqlDbType.Char, 20).Value = IIf(strCodigoExternoTipoUsuario = "", DBNull.Value, strCodigoExternoTipoUsuario)
            Sqlcommand.Parameters.Add("@pEstadoTipoUsuario", SqlDbType.Char, 200).Value = IIf(strEstadoTipoUsuario = "", DBNull.Value, strEstadoTipoUsuario)

            Dim oParamSal As New SqlClient.SqlParameter("@pMsjRetorno", SqlDbType.Char, 60)
            oParamSal.Direction = ParameterDirection.Output
            Sqlcommand.Parameters.Add(oParamSal)

            Sqlcommand.ExecuteNonQuery()

            lstrRetorno = Sqlcommand.Parameters("@pMsjRetorno").Value.ToString.Trim
            If lstrRetorno = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If
            Return lstrRetorno

        Catch ex As Exception
            lstrRetorno = ex.Message
            Return lstrRetorno
        Finally
            SQLConnect.Cerrar()
        End Try

    End Function

    Public Function Eliminar(ByVal intIdTipoUsuario As Integer) As String

        Dim Sqlcommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim MiTransaccionSQL As SqlClient.SqlTransaction
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim lstrProcedimiento As String = ""
        Dim lstrRetorno As String = "OK"

        lstrProcedimiento = "Sis_TipoUsuario_Mantencion"
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try

            Sqlcommand = New SqlClient.SqlCommand(lstrProcedimiento, MiTransaccionSQL.Connection, MiTransaccionSQL)
            Sqlcommand.CommandType = CommandType.StoredProcedure
            Sqlcommand.CommandText = lstrProcedimiento
            Sqlcommand.Parameters.Clear()

            Sqlcommand.Parameters.Add("@pCodigoOperacion", SqlDbType.Char, 10).Value = "ELIMINAR"
            Sqlcommand.Parameters.Add("@pIdTipoUsuario", SqlDbType.Char, 50).Value = intIdTipoUsuario

            Dim oParamSal As New SqlClient.SqlParameter("@pMsjRetorno", SqlDbType.Char, 60)
            oParamSal.Direction = ParameterDirection.Output
            Sqlcommand.Parameters.Add(oParamSal)

            Sqlcommand.ExecuteNonQuery()

            lstrRetorno = Sqlcommand.Parameters("@pMsjRetorno").Value.ToString.Trim
            If lstrRetorno = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If
            Return lstrRetorno

        Catch ex As Exception
            lstrRetorno = ex.Message
            Return lstrRetorno
        Finally
            SQLConnect.Cerrar()
        End Try

    End Function

    Public Function RetornaTipoControl(ByRef strRetorno As String, _
                                       ByVal strColumnas As String, _
                                       ByVal intIdTipoControl As Integer, _
                                       ByVal strNombreTipoControl As String) As DataSet

        Dim lblnRemove As Boolean = True
        Dim larr_NombreColumna() As String = Split(strColumnas, ",")
        Dim lInt_Col As Integer = 0
        Dim lInt_NomCol As String = ""

        Dim lDS_TipoControl As New DataSet
        Dim lDS_TipoControl_Aux As New DataSet

        Dim Sqlcommand As New SqlClient.SqlCommand
        Dim SqlDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim LstrProcedimiento As String = ""
        Dim LstrNombreTabla As String = ""

        LstrProcedimiento = "Sis_TipoControl_Ver"
        LstrNombreTabla = "TIPOCONTROL"

        'Abre la conexion 
        Connect.Abrir()

        Try
            Sqlcommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            Sqlcommand.CommandType = CommandType.StoredProcedure
            Sqlcommand.CommandText = LstrProcedimiento
            Sqlcommand.Parameters.Clear()

            'If intIdTipoControl <> 0 Then
            Sqlcommand.Parameters.Add("@pIdTipoControl", SqlDbType.Int).Value = IIf(intIdTipoControl = 0, DBNull.Value, intIdTipoControl)
            'End If

            'If strNombreTipoControl <> "" Then
            Sqlcommand.Parameters.Add("@pNombreTipoControl", SqlDbType.Int).Value = IIf(strNombreTipoControl.Trim = "", DBNull.Value, strNombreTipoControl.Trim)
            'End If

            Dim oParamSal As New SqlClient.SqlParameter("@pMsjRetorno", SqlDbType.Char, 60)
            oParamSal.Direction = ParameterDirection.Output
            Sqlcommand.Parameters.Add(oParamSal)


            SqlDataAdapter.SelectCommand = Sqlcommand
            SqlDataAdapter.Fill(lDS_TipoControl, LstrNombreTabla)

            If strColumnas.Trim = "" Then
                lDS_TipoControl_Aux = lDS_TipoControl
            Else
                lDS_TipoControl_Aux = lDS_TipoControl.Copy
                For Each lstrColumna As DataColumn In lDS_TipoControl.Tables(LstrNombreTabla).Columns
                    lblnRemove = True
                    For lInt_Col = 0 To larr_NombreColumna.Length - 1
                        lInt_NomCol = larr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                        If lstrColumna.ColumnName = lInt_NomCol Then
                            lblnRemove = False
                        End If
                    Next
                    If lblnRemove Then
                        lDS_TipoControl_Aux.Tables(LstrNombreTabla).Columns.Remove(lstrColumna.ColumnName)
                    End If
                Next
            End If

            For lInt_Col = 0 To larr_NombreColumna.Length - 1
                lInt_NomCol = larr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                For Each lstrColumna As DataColumn In lDS_TipoControl_Aux.Tables(LstrNombreTabla).Columns
                    If lstrColumna.ColumnName = lInt_NomCol Then
                        lstrColumna.SetOrdinal(lInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = Sqlcommand.Parameters("@pMsjRetorno").Value.ToString.Trim

            Return lDS_TipoControl_Aux
        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

End Class
