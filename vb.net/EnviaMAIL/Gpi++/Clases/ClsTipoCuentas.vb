﻿Public Class ClsTipoCuentas
    Public Function TipoCuentas_Ver(ByVal lngIdTipoCuenta As Long, _
                                    ByVal strColumnas As String, _
                                    ByVal strEstado As String, _
                                    ByRef strRetorno As String) As DataSet

        Dim LDS_TipoCuentas As New DataSet
        Dim LDS_TipoCuentas_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_TipoCuentas_Consultar"
        Lstr_NombreTabla = "Tipo_Cuentas"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdTipoCuenta", SqlDbType.Float, 10).Value = IIf(lngIdTipoCuenta = 0, DBNull.Value, lngIdTipoCuenta)
            SQLCommand.Parameters.Add("pEstado", SqlDbType.VarChar, 3).Value = strEstado

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_TipoCuentas, Lstr_NombreTabla)

            LDS_TipoCuentas_Aux = LDS_TipoCuentas

            If strColumnas.Trim = "" Then
                LDS_TipoCuentas_Aux = LDS_TipoCuentas
            Else
                LDS_TipoCuentas_Aux = LDS_TipoCuentas.Copy
                For Each Columna In LDS_TipoCuentas.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_TipoCuentas_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_TipoCuentas_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_TipoCuentas.Dispose()
            Return LDS_TipoCuentas_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function
    Public Function TipoCuenta_Mantenedor(ByVal strAccion As String, _
                              ByVal IntIdTipoCuenta As String, _
                              ByVal strDescripcion_Corta As String, _
                              ByVal strDescripcion_Larga As String, _
                              ByVal strEstado As String) As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_TipoCuenta_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_TipoCuenta_Mantencion"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 10).Value = strAccion
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.VarChar, 2).Value = IIf(IntIdTipoCuenta.Trim = "0", DBNull.Value, IntIdTipoCuenta.Trim)
            SQLCommand.Parameters.Add("pDescripcion_Corta", SqlDbType.VarChar, 50).Value = IIf(strDescripcion_Corta.Trim = "", DBNull.Value, strDescripcion_Corta.Trim)
            SQLCommand.Parameters.Add("pDescripcion_Larga", SqlDbType.VarChar, 50).Value = IIf(strDescripcion_Larga.Trim = "", DBNull.Value, strDescripcion_Larga.Trim)
            SQLCommand.Parameters.Add("pEstado", SqlDbType.VarChar, 3).Value = strEstado

            '...Resultado
            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            TipoCuenta_Mantenedor = strDescError
        End Try
    End Function

End Class
