﻿Public Class ClsSectorEconomico

    Public Function SectorEconomico_Ver(ByVal strCodigo As String, _
                                        ByVal strColumnas As String, _
                                        ByVal strEstado As String, _
                                        ByRef strRetorno As String) As DataSet

        Dim LDS_SectorEconomico As New DataSet
        Dim LDS_SectorEconomico_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_SectorEconomico_Consultar"
        Lstr_NombreTabla = "Sector_Economico"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("pIdSectorEconomico", SqlDbType.VarChar, 3).Value = IIf(strCodigo.Trim = "", DBNull.Value, strCodigo.Trim)
            SQLCommand.Parameters.Add("pEstado", SqlDbType.VarChar, 3).Value = IIf(strEstado.Trim = "", DBNull.Value, strEstado.Trim)
            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_SectorEconomico, Lstr_NombreTabla)

            LDS_SectorEconomico_Aux = LDS_SectorEconomico

            If strColumnas.Trim = "" Then
                LDS_SectorEconomico_Aux = LDS_SectorEconomico
            Else
                LDS_SectorEconomico_Aux = LDS_SectorEconomico.Copy
                For Each Columna In LDS_SectorEconomico.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_SectorEconomico_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_SectorEconomico_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_SectorEconomico.Dispose()
            Return LDS_SectorEconomico_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function
    Public Function SectorEconomico_Mantenedor(ByVal strAccion As String, _
                                 ByVal intCodigo As Integer, _
                                 ByVal strDescripcion As String, _
                                 ByVal strNombreCorto As String, _
                                 ByVal strCodigoNorma As String, _
                                 ByVal strCodigoExterno As String, _
                                 ByRef strEstado As String) As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_SectorEconomico_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_SectorEconomico_Mantencion"
            SQLCommand.Parameters.Clear()

            '@pAccion varchar(10),
            '@pDscSectorEconomico varchar(60), 
            '@pNombreCorto varchar(15), 
            '@pCod_Norma varchar(20), 
            '@pCod_Externo varchar(20), 
            '@pEstado char(3), 
            '@pIdSectorEconomico int OUTPUT, 
            '@pResultado varchar(200) OUTPUT 

            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 10).Value = strAccion
            SQLCommand.Parameters.Add("pDscSectorEconomico", SqlDbType.VarChar, 60).Value = strDescripcion
            SQLCommand.Parameters.Add("pNombreCorto", SqlDbType.VarChar, 15).Value = strNombreCorto
            SQLCommand.Parameters.Add("pCod_Norma", SqlDbType.VarChar, 20).Value = strCodigoNorma
            SQLCommand.Parameters.Add("pCod_Externo", SqlDbType.VarChar, 20).Value = strCodigoExterno
            SQLCommand.Parameters.Add("pEstado", SqlDbType.VarChar, 3).Value = strEstado
            SQLCommand.Parameters.Add("pIdSectorEconomico", SqlDbType.Float, 3).Value = intCodigo

            '...Resultado
            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            SectorEconomico_Mantenedor = strDescError
        End Try
    End Function


End Class
