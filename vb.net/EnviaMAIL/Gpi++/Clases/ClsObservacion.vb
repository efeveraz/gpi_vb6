﻿Public Class ClsObservacion
    Public Function ObservacionCliente_Ver(ByVal strIdObservacion As String, _
                                         ByVal strIdNegocio As String, _
                                         ByVal strIdCliente As String, _
                                         ByVal strColumnas As String, _
                                         ByRef strRetorno As String) As DataSet

        Dim LDS_ObservacionCliente As New DataSet
        Dim LDS_ObservacionCliente_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Observaciones_Consultar"
        Lstr_NombreTabla = "ObsCliente"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdObservacion", SqlDbType.VarChar, 9).Value = IIf(strIdObservacion.Trim = "", DBNull.Value, strIdObservacion.Trim)
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.VarChar, 9).Value = IIf(strIdNegocio.Trim = "", DBNull.Value, strIdNegocio.Trim)
            SQLCommand.Parameters.Add("pIdCliente", SqlDbType.VarChar, 9).Value = IIf(strIdCliente.Trim = "", DBNull.Value, strIdCliente.Trim)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_ObservacionCliente, Lstr_NombreTabla)

            LDS_ObservacionCliente_Aux = LDS_ObservacionCliente

            If strColumnas.Trim = "" Then
                LDS_ObservacionCliente_Aux = LDS_ObservacionCliente
            Else
                LDS_ObservacionCliente_Aux = LDS_ObservacionCliente.Copy
                For Each Columna In LDS_ObservacionCliente.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_ObservacionCliente_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_ObservacionCliente_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_ObservacionCliente.Dispose()
            Return LDS_ObservacionCliente_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function ObservacionXCliente_Mantenedor(ByVal strId_Cliente As String, _
                                                   ByVal strId_Negocio As String, _
                                                   ByRef dstObservacionXCliente As DataSet) As String

        Dim strDescError As String = "OK"
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            '       ELIMINAMOS LAS OBSERVACION X CLIENTE YA EXISTENTES

            SQLCommand = New SqlClient.SqlCommand("Rcp_Observaciones_MantencionPorCliente", MiTransaccionSQL.Connection, MiTransaccionSQL)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Observaciones_MantencionPorCliente"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = "ELIMINAR"
            SQLCommand.Parameters.Add("pIdcliente", SqlDbType.Float, 10).Value = strId_Cliente
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Float, 9).Value = strId_Negocio
            SQLCommand.Parameters.Add("pIdObservacion", SqlDbType.Float, 9).Value = DBNull.Value
            SQLCommand.Parameters.Add("pGlsClienteObservacion", SqlDbType.VarChar, 300).Value = DBNull.Value

            '...Resultado
            Dim pSalElimIntPort As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalElimIntPort.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalElimIntPort)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            If Not strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Rollback()
                Exit Function
            End If

            '       INSERTAMOS LAS NUEVAS OBSERVACIONES PARA EL CLIENTE

            If dstObservacionXCliente.Tables(0).Rows.Count <> Nothing Then
                For Each pRow As DataRow In dstObservacionXCliente.Tables(0).Rows
                    SQLCommand = New SqlClient.SqlCommand("Rcp_Observaciones_MantencionPorCliente", MiTransaccionSQL.Connection, MiTransaccionSQL)
                    SQLCommand.CommandType = CommandType.StoredProcedure
                    SQLCommand.CommandText = "Rcp_Observaciones_MantencionPorCliente"
                    SQLCommand.Parameters.Clear()

                    '[Rcp_Observaciones_MantencionPorCliente]()
                    '(@pAccion varchar(10), 
                    '@pIdcliente numeric(10, 0), 
                    '@pIdNegocio numeric(9, 0),
                    '@pIdObservacion numeric(9, 0),
                    '@pGlsClienteObservacion varchar(150), 
                    '@pResultado varchar(200) OUTPUT)  ID_CLIENTE_OBSERVACION, ID_CLIENTE, ID_NEGOCIO,GLS_CLIENTE_OBSERVACION"

                    SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = "INSERTAR"
                    SQLCommand.Parameters.Add("pIdcliente", SqlDbType.Float, 10).Value = strId_Cliente
                    SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Float, 9).Value = strId_Negocio
                    SQLCommand.Parameters.Add("pIdObservacion", SqlDbType.Float, 9).Value = DBNull.Value
                    SQLCommand.Parameters.Add("pGlsClienteObservacion", SqlDbType.VarChar, 300).Value = pRow("GLS_CLIENTE_OBSERVACION").ToString

                    '...Resultado
                    Dim pSalInstPort As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                    pSalInstPort.Direction = Data.ParameterDirection.Output
                    SQLCommand.Parameters.Add(pSalInstPort)

                    SQLCommand.ExecuteNonQuery()

                    strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
                    If Not strDescError.ToUpper.Trim = "OK" Then
                        Exit For
                    End If
                Next
            End If

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            ObservacionXCliente_Mantenedor = strDescError
        End Try
    End Function
End Class
