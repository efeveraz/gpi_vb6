Imports Microsoft.VisualBasic
Imports System.Security.Cryptography
Imports System.Text
Imports System.IO

Public Class ClsEncriptacion

    Private Shared ReadOnly IV As Byte() = {240, 3, 45, 29, 0, 76, 173, 59}

    Const NumCryptokey As Integer = 6
    Const NumExtraClave As Integer = 8
    Const NumsKey As Integer = 7

    Shared Function Generate(ByVal KeyChars As Integer) As String
        Dim i_key As Integer
        Dim Random1 As Single
        Dim arrIndex As Int16
        Dim sb As New StringBuilder
        Dim RandomLetter As String
        Dim KeyLetters As String = "abcdefghijklmnopqrstuvwxyz"
        Dim KeyNumbers As String = "0123456789"
        Dim LettersArray As Char()
        Dim NumbersArray As Char()
        ' CONVERT LettersArray & NumbersArray TO CHARACTR ARRAYS
        LettersArray = KeyLetters.ToCharArray
        NumbersArray = KeyNumbers.ToCharArray

        For i_key = 1 To KeyChars
            Randomize()
            Random1 = Rnd()
            arrIndex = -1
            If (CType(Random1 * 111, Integer)) Mod 2 = 0 Then
                Do While arrIndex < 0
                    arrIndex = Convert.ToInt16(LettersArray.GetUpperBound(0) * Random1)
                Loop
                RandomLetter = LettersArray(arrIndex)
                If (CType(arrIndex * Random1 * 99, Integer)) Mod 2 <> 0 Then
                    RandomLetter = LettersArray(arrIndex).ToString
                    RandomLetter = RandomLetter.ToUpper
                End If
                sb.Append(RandomLetter)
            Else
                Do While arrIndex < 0
                    arrIndex = Convert.ToInt16(NumbersArray.GetUpperBound(0) * Random1)
                Loop
                sb.Append(NumbersArray(arrIndex))
            End If
        Next
        Return sb.ToString
    End Function

    Public Shared Function Encriptar(ByVal serializedQueryString As String) As String
        Dim sRetorno As String
        Try
            Dim cryptokey As String = ""
            Dim ExtraClave As String = ""
            Dim sKey As String = ""
            cryptokey = Generate(NumCryptokey)
            ExtraClave = Generate(NumExtraClave)
            sKey = Generate(NumsKey)
            Dim buffer As Byte() = Encoding.ASCII.GetBytes(serializedQueryString & ExtraClave)
            Dim des As TripleDESCryptoServiceProvider = New TripleDESCryptoServiceProvider
            Dim MD5 As MD5CryptoServiceProvider = New MD5CryptoServiceProvider
            des.Key = MD5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(sKey & cryptokey))
            des.IV = IV
            sRetorno = cryptokey & ExtraClave & sKey & Convert.ToBase64String(des.CreateEncryptor().TransformFinalBlock(buffer, 0, buffer.Length()))
            Return sRetorno
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function

    Public Shared Function DesEncriptar(ByVal encryptedQueryString As String) As String
        Dim buffer As Byte()
        Dim DES As New TripleDESCryptoServiceProvider
        Dim MD5 As New MD5CryptoServiceProvider
        Dim cryptokey As String = ""
        Dim ExtraClave As String = ""
        Dim sKey As String = ""
        cryptokey = Left(encryptedQueryString, NumCryptokey)
        ExtraClave = Mid(encryptedQueryString, NumCryptokey + 1, NumExtraClave)
        sKey = Mid(encryptedQueryString, NumCryptokey + NumExtraClave + 1, NumsKey)
        encryptedQueryString = Mid(encryptedQueryString, NumCryptokey + NumExtraClave + NumsKey + 1, Len(encryptedQueryString))
        Try
            buffer = Convert.FromBase64String(encryptedQueryString)
            DES.Key = MD5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(sKey & cryptokey))
            DES.IV = IV
            Return (Replace(Encoding.ASCII.GetString(DES.CreateDecryptor().TransformFinalBlock(buffer, 0, buffer.Length())), ExtraClave, ""))
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

End Class
