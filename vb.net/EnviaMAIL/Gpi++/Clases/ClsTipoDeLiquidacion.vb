﻿Public Class ClsTipoDeLiquidacion

    Public Function TipoDeLiquidacion_Ver(ByVal intIdTipoLiquidacion As Integer, _
                                          ByVal intIdNegocio As Integer, _
                                          ByVal strCodSubClaseIns As String, _
                                          ByVal strPagoCobro As String, _
                                          ByVal strColumnas As String, _
                                          ByRef strRetorno As String, _
                                          Optional ByVal intIdInstrumento As Integer = 0) As DataSet

        Dim LDS_TipoDeLiquidacion As New DataSet
        Dim LDS_TipoDeLiquidacion_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_TipoDeLiquidacion_Consultar"
        Lstr_NombreTabla = "TipoDeLiquidacion"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("pIdTipoLiquidacion", SqlDbType.Float, 10).Value = IIf(intIdTipoLiquidacion = 0, DBNull.Value, intIdTipoLiquidacion)
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Float, 9).Value = IIf(intIdNegocio = 0, DBNull.Value, intIdNegocio)
            SQLCommand.Parameters.Add("pCodSubClaseIns", SqlDbType.VarChar, 10).Value = IIf(strCodSubClaseIns.Trim = "", DBNull.Value, strCodSubClaseIns.Trim)
            SQLCommand.Parameters.Add("pPagoCobro", SqlDbType.VarChar, 1).Value = IIf(strPagoCobro.Trim = "", DBNull.Value, strPagoCobro.Trim)
            SQLCommand.Parameters.Add("pIdInstrumento", SqlDbType.Float, 10).Value = IIf(intIdInstrumento = 0, DBNull.Value, intIdInstrumento)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_TipoDeLiquidacion, Lstr_NombreTabla)

            LDS_TipoDeLiquidacion_Aux = LDS_TipoDeLiquidacion

            If strColumnas.Trim = "" Then
                LDS_TipoDeLiquidacion_Aux = LDS_TipoDeLiquidacion
            Else
                LDS_TipoDeLiquidacion_Aux = LDS_TipoDeLiquidacion.Copy
                For Each Columna In LDS_TipoDeLiquidacion.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_TipoDeLiquidacion_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_TipoDeLiquidacion_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_TipoDeLiquidacion.Dispose()
            Return LDS_TipoDeLiquidacion_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function TipoDeLiquidacion_Mantenedor(ByVal strAccion As String, _
                                               ByVal intIdTipoDeLiquidacion As Integer, _
                                               ByVal intIdNegocio As Integer, _
                                               ByVal strCodSubClaseIns As String, _
                                               ByVal dblIdInstrumento As Double, _
                                               ByVal strPagoCobro As String, _
                                               ByVal intDiasRetencion As Integer) As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_TipoDeLiquidacion_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_TipoDeLiquidacion_Mantencion"
            SQLCommand.Parameters.Clear()

            '(@pAccion varchar(10), 
            '@pIdTipoLiquidacion numeric(10, 0), 
            '@pIdNegocio numeric(9, 0), 
            '@pCodSubClaseIns varchar(10)=NULL, 
            '@pPagoCobro varchar(1)=NULL, 
            '@pDiasRetencion numeric(4, 0), 
            '@pResultado varchar(200) OUTPUT)

            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 10).Value = strAccion
            SQLCommand.Parameters.Add("pIdTipoLiquidacion", SqlDbType.Float, 10).Value = intIdTipoDeLiquidacion
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Float, 9).Value = intIdNegocio
            SQLCommand.Parameters.Add("pCodSubClaseIns", SqlDbType.VarChar, 10).Value = strCodSubClaseIns
            SQLCommand.Parameters.Add("pPagoCobro", SqlDbType.VarChar, 1).Value = strPagoCobro
            SQLCommand.Parameters.Add("pDiasRetencion", SqlDbType.Float, 4).Value = intDiasRetencion
            SQLCommand.Parameters.Add("pIdInstrumento", SqlDbType.Float, 10).Value = IIf(dblIdInstrumento = 0, DBNull.Value, dblIdInstrumento)

            '...Resultado
            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            TipoDeLiquidacion_Mantenedor = strDescError
        End Try
    End Function

End Class


