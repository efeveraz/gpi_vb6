﻿Public Class ClsTipoIdentificacion
    Public Function BuscarTiposIdentificacion(ByVal strEstadoIdentificacion As String, _
                                               ByVal strColumnas As String, _
                                              ByRef strRetorno As String) As DataSet

        Dim LDS_TipoIdentificacion As New DataSet
        Dim LDS_TipoIdentificacion_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_TipoIdentificacion_Consultar"
        Lstr_NombreTabla = "TipoIdentificacion"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("pEstado", SqlDbType.VarChar, 3).Value = IIf(strEstadoIdentificacion.Trim = "", DBNull.Value, strEstadoIdentificacion.Trim)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_TipoIdentificacion, Lstr_NombreTabla)

            LDS_TipoIdentificacion_Aux = LDS_TipoIdentificacion

            If strColumnas.Trim = "" Then
                LDS_TipoIdentificacion_Aux = LDS_TipoIdentificacion
            Else
                LDS_TipoIdentificacion_Aux = LDS_TipoIdentificacion.Copy
                For Each Columna In LDS_TipoIdentificacion.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_TipoIdentificacion_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_TipoIdentificacion_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next


            strRetorno = "OK"
            LDS_TipoIdentificacion.Dispose()

            Return LDS_TipoIdentificacion_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function
End Class
