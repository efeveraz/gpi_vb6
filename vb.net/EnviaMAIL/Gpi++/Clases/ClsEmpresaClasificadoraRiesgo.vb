﻿Public Class ClsEmpresaClasificadoraRiesgo

    Public Function EmpresaClasificadoraRiesgo_Consultar(ByVal intIdEmpresaClasificadora As Integer, _
                                                         ByVal strColumnas As String, _
                                                         ByRef strRetorno As String) As DataSet

        Dim LDS_EmpresaClasificadoraRiesgo As New DataSet
        Dim LDS_EmpresaClasificadoraRiesgo_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_EmpresaClasificadoraRiesgo_Consultar"
        Lstr_NombreTabla = "EmpresasClasificadorasRiesgo"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdEmpresaClasificadora", SqlDbType.Int, 10).Value = IIf(intIdEmpresaClasificadora = 0, DBNull.Value, intIdEmpresaClasificadora)

            '...Resultado
            Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultado.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_EmpresaClasificadoraRiesgo, Lstr_NombreTabla)

            LDS_EmpresaClasificadoraRiesgo_Aux = LDS_EmpresaClasificadoraRiesgo

            If strColumnas.Trim = "" Then
                LDS_EmpresaClasificadoraRiesgo_Aux = LDS_EmpresaClasificadoraRiesgo
            Else
                LDS_EmpresaClasificadoraRiesgo_Aux = LDS_EmpresaClasificadoraRiesgo.Copy
                For Each Columna In LDS_EmpresaClasificadoraRiesgo.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_EmpresaClasificadoraRiesgo_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_EmpresaClasificadoraRiesgo_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            LDS_EmpresaClasificadoraRiesgo.Dispose()
            Return LDS_EmpresaClasificadoraRiesgo_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function EmpresaClasificadoraRiesgo_Buscar(ByVal intIdEmpresaClasificadora As Integer, _
                                                      ByVal strDscEmpresaClasificadora As String, _
                                                      ByVal intPriorEmpresaClasificadora As Integer, _
                                                      ByVal strEstEmpresaClasificadora As String, _
                                                      ByVal strColumnas As String, _
                                                      ByRef strRetorno As String) As DataSet

        Dim LDS_EmpresaClasificadoraRiesgo As New DataSet
        Dim LDS_EmpresaClasificadoraRiesgo_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_EmpresaClasificadoraRiesgo_Buscar"
        Lstr_NombreTabla = "EmpresasClasificadorasRiesgo"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdEmpresaClasificadora", SqlDbType.Int, 10).Value = IIf(intIdEmpresaClasificadora = 0, DBNull.Value, intIdEmpresaClasificadora)
            SQLCommand.Parameters.Add("pDscEmpresaClasificadora", SqlDbType.VarChar, 100).Value = IIf(strDscEmpresaClasificadora = "", DBNull.Value, strDscEmpresaClasificadora)
            SQLCommand.Parameters.Add("pPrioridadEmpresaClasificadora", SqlDbType.Int, 10).Value = IIf(intPriorEmpresaClasificadora = 0, DBNull.Value, intPriorEmpresaClasificadora)
            SQLCommand.Parameters.Add("pEstEmpresaClasificadora", SqlDbType.VarChar, 3).Value = IIf(strEstEmpresaClasificadora = "", DBNull.Value, strEstEmpresaClasificadora)


            '...Resultado
            Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultado.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_EmpresaClasificadoraRiesgo, Lstr_NombreTabla)

            LDS_EmpresaClasificadoraRiesgo_Aux = LDS_EmpresaClasificadoraRiesgo

            If strColumnas.Trim = "" Then
                LDS_EmpresaClasificadoraRiesgo_Aux = LDS_EmpresaClasificadoraRiesgo
            Else
                LDS_EmpresaClasificadoraRiesgo_Aux = LDS_EmpresaClasificadoraRiesgo.Copy
                For Each Columna In LDS_EmpresaClasificadoraRiesgo.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_EmpresaClasificadoraRiesgo_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_EmpresaClasificadoraRiesgo_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            LDS_EmpresaClasificadoraRiesgo.Dispose()
            Return LDS_EmpresaClasificadoraRiesgo_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function EmpresaClasificadoraRiesgo_Mantencion(ByVal strAccion As String, _
                                                          ByVal intIdEmpresaClasificadora As Integer, _
                                                          ByVal strDscEmpresaClasificadora As String, _
                                                          ByVal intPriorEmpresaClasificadora As Integer, _
                                                          ByVal strEstEmpresaClasificadora As String, _
                                                          Optional ByVal strFechaCaducacion As String = "") As String


        Dim strDescError As String = "OK"
        Dim lintIdEmpresaClasificadora As Integer
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try


            SQLCommand = New SqlClient.SqlCommand("Rcp_EmpresaClasificadoraRiesgo_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_EmpresaClasificadoraRiesgo_Mantencion"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 20).Value = UCase(strAccion.Trim)

            '...IdEmpresaClasificadora
            Dim pSalIdEmpresaClasificadora As New SqlClient.SqlParameter("pIdEmpresaClasificadora", SqlDbType.Int, 10)
            pSalIdEmpresaClasificadora.Direction = Data.ParameterDirection.InputOutput
            pSalIdEmpresaClasificadora.Value = IIf(intIdEmpresaClasificadora = 0, DBNull.Value, intIdEmpresaClasificadora)
            SQLCommand.Parameters.Add(pSalIdEmpresaClasificadora)


            SQLCommand.Parameters.Add("pDscEmpresaClasificadora", SqlDbType.VarChar, 100).Value = strDscEmpresaClasificadora
            SQLCommand.Parameters.Add("pPrioridadEmpresaClasificadora", SqlDbType.Int, 10).Value = IIf(intPriorEmpresaClasificadora = 0, DBNull.Value, intPriorEmpresaClasificadora)
            SQLCommand.Parameters.Add("pEstEmpresaClasificadora", SqlDbType.VarChar, 3).Value = strEstEmpresaClasificadora
            SQLCommand.Parameters.Add("pFechaCaducacion", SqlDbType.VarChar, 10).Value = IIf(strFechaCaducacion = "", "01-01-3000", strFechaCaducacion)


            '...Resultado
            Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultado.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                lintIdEmpresaClasificadora = CInt(SQLCommand.Parameters("pIdEmpresaClasificadora").Value.ToString.Trim)
                If Trim(strDescError) = "OK" Then
                    MiTransaccionSQL.Commit()
                    Return ("OK")
                End If
            Else
                GoTo Salir
            End If

Salir:
            If Trim(strDescError) = "OK" Then
                MiTransaccionSQL.Commit()
                Return ("OK")
            Else
                MiTransaccionSQL.Rollback()
                Return strDescError
            End If


        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en la mantención Empresas clasificadoras de riesgo." & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            EmpresaClasificadoraRiesgo_Mantencion = strDescError
        End Try
    End Function

End Class
