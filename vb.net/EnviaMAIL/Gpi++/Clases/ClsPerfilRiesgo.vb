﻿Imports System.Data.SqlClient
Imports System.Data

Public Class ClsPerfilRiesgo
#Region "Declaraciones de variables"
    Dim sProcedimiento As String = ""

#End Region

#Region "Metodos de la Clase"

    Public Function TraeDatosPerfilRiesgo(ByRef strRetorno As String, Optional ByVal dblIdNegocio As Double = 0, Optional ByVal dblIdPerfilRiesgo As Double = 0) As DataSet

        Dim LDS_PerfilRiesgo As New DataSet
        Dim LDS_PerfilRiesgo_Aux As New DataSet

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        sProcedimiento = "Rcp_PerfilRiesgo_Buscar"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(sProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = sProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("@pIdNegocio", SqlDbType.Float, 10).Value = IIf(dblIdNegocio = 0, DBNull.Value, dblIdNegocio)
            SQLCommand.Parameters.Add("@pIdPerfilRiesgo", SqlDbType.Float, 10).Value = IIf(dblIdPerfilRiesgo = 0, DBNull.Value, dblIdPerfilRiesgo)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_PerfilRiesgo)

            LDS_PerfilRiesgo_Aux = LDS_PerfilRiesgo
            strRetorno = "OK"
            Return LDS_PerfilRiesgo_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function TraeDatosPerfilRiesgoPorUsuario(ByRef strRetorno As String, ByVal Id_Usuario As Integer, Optional ByVal dblIdNegocio As Double = 0, Optional ByVal dblIdPerfilRiesgo As Double = 0) As DataSet

        Dim LDS_PerfilRiesgo As New DataSet
        Dim LDS_PerfilRiesgo_Aux As New DataSet

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        sProcedimiento = "Rcp_PerfilRiesgo_BuscarPorUsuario"
        Connect.Abrir()
        Try
            SQLCommand = New SqlClient.SqlCommand(sProcedimiento, Connect.Conexion)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = sProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("@pIdNegocio", SqlDbType.Float, 10).Value = IIf(dblIdNegocio = 0, DBNull.Value, dblIdNegocio)
            SQLCommand.Parameters.Add("@pIdPerfilRiesgo", SqlDbType.Float, 10).Value = IIf(dblIdPerfilRiesgo = 0, DBNull.Value, dblIdPerfilRiesgo)
            SQLCommand.Parameters.Add("@pIdUsuario", SqlDbType.Float, 10).Value = IIf(Id_Usuario = 0, DBNull.Value, Id_Usuario)
            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_PerfilRiesgo)

            LDS_PerfilRiesgo_Aux = LDS_PerfilRiesgo
            strRetorno = "OK"
            Return LDS_PerfilRiesgo_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function
    Public Function ConsultarEditarPerfiles(ByRef strRetorno As String, ByVal Id_Usuario As Integer, ByVal dblIdPerfilRiesgo As Double) As String
        Dim LDS_PerfilRiesgo As New DataSet
        Dim LDS_PerfilRiesgo_Aux As New DataSet
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        sProcedimiento = "Rcp_PerfilRiesgo_Permiso_Modificar"
        Connect.Abrir()
        Try
            SQLCommand = New SqlClient.SqlCommand(sProcedimiento, Connect.Conexion)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = sProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("@pIdPerfilRiesgo", SqlDbType.Float, 10).Value = IIf(dblIdPerfilRiesgo = 0, DBNull.Value, dblIdPerfilRiesgo)
            SQLCommand.Parameters.Add("@pIdUsuario", SqlDbType.Float, 10).Value = IIf(Id_Usuario = 0, DBNull.Value, Id_Usuario)
            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)
            SQLCommand.ExecuteNonQuery()
            strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            Return strRetorno
        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function PerfilRiesgo_Ver(ByVal lngIdNegocio As Long,
                                     ByVal lngIdPerfilRiesgo As Long,
                                     ByVal strEstadoPerfil As String,
                                     ByVal strColumnas As String,
                                     ByRef strRetorno As String,
                                     Optional ByRef strTipoPerfil As String = "",
                                     Optional ByRef strDscPerfil As String = "") As DataSet

        Dim LDS_PerfilRiesgo As New DataSet
        Dim LDS_PerfilRiesgo_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_PerfilRiesgo_Consultar"
        Lstr_NombreTabla = "Perfil_Riesgo"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Float, 10).Value = IIf(lngIdNegocio = 0, DBNull.Value, lngIdNegocio)
            SQLCommand.Parameters.Add("pIdPerfilRiesgo", SqlDbType.Float, 10).Value = IIf(lngIdPerfilRiesgo = 0, DBNull.Value, lngIdPerfilRiesgo)
            SQLCommand.Parameters.Add("pEstadoPerfil", SqlDbType.VarChar, 3).Value = IIf(strEstadoPerfil.Trim = "", DBNull.Value, strEstadoPerfil.Trim)
            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 10).Value = IIf(strTipoPerfil.Trim = "", DBNull.Value, strTipoPerfil.Trim)
            SQLCommand.Parameters.Add("pDscPerfilRiesgo", SqlDbType.VarChar, 50).Value = IIf(strDscPerfil.Trim = "", DBNull.Value, strDscPerfil.Trim)


            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_PerfilRiesgo, Lstr_NombreTabla)

            LDS_PerfilRiesgo_Aux = LDS_PerfilRiesgo

            If strColumnas.Trim = "" Then
                LDS_PerfilRiesgo_Aux = LDS_PerfilRiesgo
            Else
                LDS_PerfilRiesgo_Aux = LDS_PerfilRiesgo.Copy
                For Each Columna In LDS_PerfilRiesgo.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_PerfilRiesgo_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If
            If strColumnas.Trim = "" Then
                LDS_PerfilRiesgo_Aux = LDS_PerfilRiesgo
            Else
                For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                    LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                    For Each Columna In LDS_PerfilRiesgo_Aux.Tables(Lstr_NombreTabla).Columns
                        If Columna.ColumnName = LInt_NomCol Then
                            Columna.SetOrdinal(LInt_Col)
                            Exit For
                        End If
                    Next
                Next
            End If

            strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            LDS_PerfilRiesgo.Dispose()
            Return LDS_PerfilRiesgo_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function
    Public Function PerfilRiesgo_Mantenedor(ByVal strAccion As String, _
                                            ByVal lngIdNegocio As Long, _
                                            ByVal lngIdPerfilRiesgo As Long, _
                                            ByVal strDescPerfilRiesgo As String, _
                                            ByVal strDescAbrevPerfilRiesgo As String, _
                                            ByVal dblSaltoCuotaPorcentaje As Double, _
                                            ByVal dblPonderacion As Integer, _
                                            ByVal strEstadoPerfilRiesgo As String) As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_PerfilRiesgo_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_PerfilRiesgo_Mantencion"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 10).Value = strAccion
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Float, 10).Value = IIf(lngIdNegocio = 0, DBNull.Value, lngIdNegocio)
            SQLCommand.Parameters.Add("pIdPerfilRiesgo", SqlDbType.Float, 10).Value = IIf(lngIdPerfilRiesgo = 0, DBNull.Value, lngIdPerfilRiesgo)
            SQLCommand.Parameters.Add("pDscPerfilRiesgo", SqlDbType.VarChar, 50).Value = strDescPerfilRiesgo
            SQLCommand.Parameters.Add("pDscAbrevPerfilRiesgo", SqlDbType.VarChar, 15).Value = strDescAbrevPerfilRiesgo
            SQLCommand.Parameters.Add("pSaltoCuotaPorcentaje", SqlDbType.Float, 10).Value = dblSaltoCuotaPorcentaje
            SQLCommand.Parameters.Add("pPonderacion", SqlDbType.Float, 3).Value = IIf(dblPonderacion = 0, DBNull.Value, dblPonderacion)
            SQLCommand.Parameters.Add("pEstadoPerfilRiesgo", SqlDbType.VarChar, 3).Value = strEstadoPerfilRiesgo

            '...Resultado
            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            PerfilRiesgo_Mantenedor = strDescError
        End Try
    End Function

    Public Function TraeDatosPerfilRiesgoNuevos(ByRef strRetorno As String, ByVal Id_Usuario As Integer, Optional ByVal dblIdNegocio As Double = 0, Optional ByVal dblIdPerfilRiesgo As Double = 0) As DataSet

        Dim LDS_PerfilRiesgo As New DataSet
        Dim LDS_PerfilRiesgo_Aux As New DataSet

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        sProcedimiento = "Rcp_PerfilInversionista_PerfilNuevo"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(sProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = sProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("@pIdNegocio", SqlDbType.Float, 10).Value = IIf(dblIdNegocio = 0, DBNull.Value, dblIdNegocio)
            SQLCommand.Parameters.Add("@pIdUsuario", SqlDbType.Float, 10).Value = IIf(Id_Usuario = 0, DBNull.Value, Id_Usuario)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_PerfilRiesgo)

            LDS_PerfilRiesgo_Aux = LDS_PerfilRiesgo
            strRetorno = "OK"
            Return LDS_PerfilRiesgo_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function PerfilPorInstrumento_Ver(ByVal lngIdInstrumento As Long, _
                                     ByVal strCodSubClaseInstrumento As String, _
                                     ByVal strColumnas As String, _
                                     ByRef strRetorno As String) As DataSet

        Dim LDS_PerfilRiesgo As New DataSet
        Dim LDS_PerfilRiesgo_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_PerfilInstrumento_Buscar"
        Lstr_NombreTabla = "Perfil_Instrumento"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("pIdInstrumento", SqlDbType.Float, 3).Value = IIf(lngIdInstrumento = 0, DBNull.Value, lngIdInstrumento)
            SQLCommand.Parameters.Add("pCodSubclaseInstrumento", SqlDbType.VarChar, 10).Value = IIf(strCodSubClaseInstrumento = "", DBNull.Value, strCodSubClaseInstrumento)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_PerfilRiesgo, Lstr_NombreTabla)

            LDS_PerfilRiesgo_Aux = LDS_PerfilRiesgo

            If strColumnas.Trim = "" Then
                LDS_PerfilRiesgo_Aux = LDS_PerfilRiesgo
            Else
                LDS_PerfilRiesgo_Aux = LDS_PerfilRiesgo.Copy
                For Each Columna In LDS_PerfilRiesgo.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_PerfilRiesgo_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_PerfilRiesgo_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_PerfilRiesgo.Dispose()
            Return LDS_PerfilRiesgo_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function InstrumentoPorPerfil_Ver(ByVal lngIdPerfilRiesgo As Long, _
                                     ByVal strCodSubclaseInstrumento As String, _
                                     ByVal strColumnas As String, _
                                     ByRef strRetorno As String) As DataSet

        Dim LDS_PerfilRiesgo As New DataSet
        Dim LDS_PerfilRiesgo_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTablaIncluida As String = ""
        Dim Lstr_NombreTablaNoIncluida As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_InstrumentoPerfil_Buscar"
        Lstr_NombreTablaIncluida = "Instrumento_Perfil_Incluido"
        Lstr_NombreTablaNoIncluida = "Instrumento_Perfil_No_Incluido"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("pIdPerfilRiesgo", SqlDbType.Float, 3).Value = IIf(lngIdPerfilRiesgo = 0, DBNull.Value, lngIdPerfilRiesgo)
            SQLCommand.Parameters.Add("pCodSubClaseInstrumento", SqlDbType.VarChar, 10).Value = IIf(strCodSubclaseInstrumento.Trim = "", DBNull.Value, strCodSubclaseInstrumento.Trim)
            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_PerfilRiesgo)

            LDS_PerfilRiesgo.Tables(0).TableName = Lstr_NombreTablaIncluida
            LDS_PerfilRiesgo.Tables(1).TableName = Lstr_NombreTablaNoIncluida

            LDS_PerfilRiesgo_Aux = LDS_PerfilRiesgo

            If strColumnas.Trim = "" Then
                LDS_PerfilRiesgo_Aux = LDS_PerfilRiesgo
            Else
                LDS_PerfilRiesgo_Aux = LDS_PerfilRiesgo.Copy
                For Each Columna In LDS_PerfilRiesgo.Tables(Lstr_NombreTablaIncluida).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_PerfilRiesgo_Aux.Tables(Lstr_NombreTablaIncluida).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_PerfilRiesgo_Aux.Tables(Lstr_NombreTablaIncluida).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_PerfilRiesgo.Dispose()
            Return LDS_PerfilRiesgo_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function PerfilPorInstrumento_Mantenedor(ByVal dtbPerfilRiesgo As DataTable, _
                                                    ByVal lngIdInstrumento As Long, _
                                                    ByVal strCodSubclaseInstrumento As String) As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        '+ Se eliminan todos los relacionados anteriores
        SQLCommand = New SqlClient.SqlCommand("Rcp_PerfilInstrumento_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)
        SQLCommand.CommandType = CommandType.StoredProcedure
        SQLCommand.CommandText = "Rcp_PerfilInstrumento_Mantencion"
        SQLCommand.Parameters.Clear()

        SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 10).Value = "ELIMINAR"
        SQLCommand.Parameters.Add("pIdPerfilRiesgo", SqlDbType.Float, 3).Value = DBNull.Value
        SQLCommand.Parameters.Add("pIdInstrumento", SqlDbType.Float, 10).Value = IIf(lngIdInstrumento = 0, DBNull.Value, lngIdInstrumento)
        SQLCommand.Parameters.Add("pCodSubClaseInstrumento", SqlDbType.VarChar, 10).Value = IIf(strCodSubclaseInstrumento = "", DBNull.Value, strCodSubclaseInstrumento)

        '...Resultado
        Dim pParametroSalida1 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
        pParametroSalida1.Direction = Data.ParameterDirection.Output
        SQLCommand.Parameters.Add(pParametroSalida1)

        SQLCommand.ExecuteNonQuery()

        strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
        If strDescError.ToUpper.Trim <> "OK" Then
            MiTransaccionSQL.Rollback()
            PerfilPorInstrumento_Mantenedor = strDescError
            Exit Function
        End If

        Try

            '+ Se guardan todos los nuevos perfiles asociados
            For Each dtrRegistro As DataRow In dtbPerfilRiesgo.Rows

                If dtrRegistro("DETALLE_ELIMINA") = "1" Then
                    SQLCommand = New SqlClient.SqlCommand("Rcp_PerfilInstrumento_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)
                    SQLCommand.CommandType = CommandType.StoredProcedure
                    SQLCommand.CommandText = "Rcp_PerfilInstrumento_Mantencion"
                    SQLCommand.Parameters.Clear()

                    SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 10).Value = "INSERTAR"
                    SQLCommand.Parameters.Add("pIdPerfilRiesgo", SqlDbType.Float, 3).Value = dtrRegistro("ID_PERFIL_RIESGO")
                    SQLCommand.Parameters.Add("pIdInstrumento", SqlDbType.Float, 10).Value = IIf(lngIdInstrumento = 0, DBNull.Value, lngIdInstrumento)
                    SQLCommand.Parameters.Add("pCodSubClaseInstrumento", SqlDbType.VarChar, 10).Value = IIf(strCodSubclaseInstrumento = "", DBNull.Value, strCodSubclaseInstrumento)

                    '...Resultado
                    Dim pParametroSalida As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                    pParametroSalida.Direction = Data.ParameterDirection.Output
                    SQLCommand.Parameters.Add(pParametroSalida)

                    SQLCommand.ExecuteNonQuery()

                    strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
                    If strDescError.ToUpper.Trim <> "OK" Then
                        Exit For
                    End If
                End If

            Next

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error al guardar Perfiles por Instrumentos" & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            PerfilPorInstrumento_Mantenedor = strDescError
        End Try
    End Function

    Public Function InstrumentoPorPerfil_Mantenedor(ByVal lngIdPerfilRiesgo As Long, _
                                                    ByVal dtbInstrumento As DataTable, _
                                                    ByVal strCodSubclaseInstrumento As String) As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        '+ Se eliminan todos los relacionados anteriores
        SQLCommand = New SqlClient.SqlCommand("Rcp_PerfilInstrumento_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)
        SQLCommand.CommandType = CommandType.StoredProcedure
        SQLCommand.CommandText = "Rcp_PerfilInstrumento_Mantencion"
        SQLCommand.Parameters.Clear()

        SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 10).Value = "ELIMINAR"
        SQLCommand.Parameters.Add("pIdPerfilRiesgo", SqlDbType.Float, 3).Value = lngIdPerfilRiesgo
        SQLCommand.Parameters.Add("pIdInstrumento", SqlDbType.Float, 10).Value = DBNull.Value
        SQLCommand.Parameters.Add("pCodSubClaseInstrumento", SqlDbType.VarChar, 10).Value = IIf(strCodSubclaseInstrumento = "", DBNull.Value, strCodSubclaseInstrumento)

        '...Resultado
        Dim pParametroSalida1 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
        pParametroSalida1.Direction = Data.ParameterDirection.Output
        SQLCommand.Parameters.Add(pParametroSalida1)

        SQLCommand.ExecuteNonQuery()

        strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
        If strDescError.ToUpper.Trim <> "OK" Then
            MiTransaccionSQL.Rollback()
            InstrumentoPorPerfil_Mantenedor = strDescError
            Exit Function
        End If

        Try

            For Each dtrRegistro As DataRow In dtbInstrumento.Rows
                SQLCommand = New SqlClient.SqlCommand("Rcp_PerfilInstrumento_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)
                SQLCommand.CommandType = CommandType.StoredProcedure
                SQLCommand.CommandText = "Rcp_PerfilInstrumento_Mantencion"
                SQLCommand.Parameters.Clear()

                SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 10).Value = "INSERTAR"
                SQLCommand.Parameters.Add("pIdPerfilRiesgo", SqlDbType.Float, 3).Value = IIf(lngIdPerfilRiesgo = 0, DBNull.Value, lngIdPerfilRiesgo)
                SQLCommand.Parameters.Add("pIdInstrumento", SqlDbType.Float, 10).Value = dtrRegistro("ID_INSTRUMENTO")
                SQLCommand.Parameters.Add("pCodSubClaseInstrumento", SqlDbType.VarChar, 10).Value = IIf(strCodSubclaseInstrumento = "", DBNull.Value, strCodSubclaseInstrumento)

                '...Resultado
                Dim pParametroSalida As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                pParametroSalida.Direction = Data.ParameterDirection.Output
                SQLCommand.Parameters.Add(pParametroSalida)

                SQLCommand.ExecuteNonQuery()

                strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
                If strDescError.ToUpper.Trim <> "OK" Then
                    Exit For
                End If
            Next

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error al guardar Instrumentos por Perfiles" & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            InstrumentoPorPerfil_Mantenedor = strDescError
        End Try
    End Function
#End Region

End Class
