﻿Public Class ClsFormaOperar
    Public Function FormaOperar_Ver(ByVal strCodigo As String, _
                                       ByVal strColumnas As String, _
                                       ByRef strRetorno As String) As DataSet

        Dim LDS_FormaOperar As New DataSet
        Dim LDS_FormaOperar_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_FormaOperar_Consultar"
        Lstr_NombreTabla = "FormaOperar"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("pIdFormaOperar", SqlDbType.VarChar, 5).Value = IIf(strCodigo.Trim = "", DBNull.Value, strCodigo.Trim)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_FormaOperar, Lstr_NombreTabla)

            LDS_FormaOperar_Aux = LDS_FormaOperar

            If strColumnas.Trim = "" Then
                LDS_FormaOperar_Aux = LDS_FormaOperar
            Else
                LDS_FormaOperar_Aux = LDS_FormaOperar.Copy
                For Each Columna In LDS_FormaOperar.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_FormaOperar_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_FormaOperar_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_FormaOperar.Dispose()
            Return LDS_FormaOperar_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function FormaOperar_Mantenedor(ByVal strAccion As String, _
                                              ByVal strDescripcion As String, _
                                              ByVal intIdFormaOperar As Integer, _
                                              ByRef strEstado As String) As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_FormaOperar_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_FormaOperar_Mantencion"
            SQLCommand.Parameters.Clear()

            ' @pAccion varchar(10),
            ' @pDscFormaOperar varchar(60),
            ' @pIdFormaOperar inT output , 
            ' @pResultado varchar(200) OUTPUT)

            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 10).Value = strAccion
            SQLCommand.Parameters.Add("pDscFormaOperar", SqlDbType.VarChar, 60).Value = strDescripcion
            SQLCommand.Parameters.Add("pIdFormaOperar", SqlDbType.Float, 3).Value = intIdFormaOperar
            SQLCommand.Parameters.Add("pEstado", SqlDbType.VarChar, 3).Value = strEstado

            '...Resultado
            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            FormaOperar_Mantenedor = strDescError
        End Try
    End Function

    Public Function FormaOperarXCliente_Ver(ByVal strCodigoCliente As String, _
                              ByVal strColumnas As String, _
                              ByRef strRetorno As String) As DataSet

        Dim LDS_FormaOperarXCliente As New DataSet
        Dim LDS_FormaOperarXCliente_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_FormaOperar_ConsultarPorCliente"
        Lstr_NombreTabla = "FormaOperarXCliente"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdCliente", SqlDbType.VarChar, 9).Value = IIf(strCodigoCliente.Trim = "", DBNull.Value, strCodigoCliente.Trim)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_FormaOperarXCliente, Lstr_NombreTabla)

            LDS_FormaOperarXCliente_Aux = LDS_FormaOperarXCliente

            If strColumnas.Trim = "" Then
                LDS_FormaOperarXCliente_Aux = LDS_FormaOperarXCliente
            Else
                LDS_FormaOperarXCliente_Aux = LDS_FormaOperarXCliente.Copy
                For Each Columna In LDS_FormaOperarXCliente.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_FormaOperarXCliente_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_FormaOperarXCliente_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_FormaOperarXCliente.Dispose()
            Return LDS_FormaOperarXCliente_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function FormaOperarXCliente_Mantenedor(ByVal strId_Cliente As String, ByRef dstFormaOperarXCliente As DataSet) As String

        Dim strDescError As String = "OK"
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try

            '       ELIMINAMOS LAS FORMA OPERAR X CLIENTE

            SQLCommand = New SqlClient.SqlCommand("Rcp_FormaOperar_MantencionPorCliente", MiTransaccionSQL.Connection, MiTransaccionSQL)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_FormaOperar_MantencionPorCliente"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = "ELIMINAR"
            SQLCommand.Parameters.Add("pIdcontraparte", SqlDbType.Float, 10).Value = DBNull.Value
            SQLCommand.Parameters.Add("pDscContraparte", SqlDbType.VarChar, 100).Value = DBNull.Value
            SQLCommand.Parameters.Add("pIdcliente", SqlDbType.Float, 10).Value = strId_Cliente
            SQLCommand.Parameters.Add("pIdforma_Operacion", SqlDbType.Float, 4).Value = DBNull.Value
            SQLCommand.Parameters.Add("pDscFormaOperar", SqlDbType.VarChar, 100).Value = DBNull.Value

            '...Resultado
            Dim pSalElimIntPort As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalElimIntPort.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalElimIntPort)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            If Not strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Rollback()
                Exit Function
            End If

            '       INSERTAMOS LAS NUEVOS FORMA OPERAR X CLIENTE

            If dstFormaOperarXCliente.Tables(0).Rows.Count <> Nothing Then
                For Each pRow As DataRow In dstFormaOperarXCliente.Tables(0).Rows
                    SQLCommand = New SqlClient.SqlCommand("Rcp_FormaOperar_MantencionPorCliente", MiTransaccionSQL.Connection, MiTransaccionSQL)
                    SQLCommand.CommandType = CommandType.StoredProcedure
                    SQLCommand.CommandText = "Rcp_FormaOperar_MantencionPorCliente"
                    SQLCommand.Parameters.Clear()

                    '[Rcp_FormaOperar_MantencionPorCliente]()
                    '(@pAccion varchar(10), 
                    '@pIdcontraparte numeric(10, 0),
                    '@pDscContraparte varchar(100),
                    '@pIdcliente numeric(10, 0), 
                    '@pIdforma_Operacion numeric(4, 0),
                    '@pDscFormaOperar varchar(100),
                    '@pResultado varchar(200) OUTPUT) "ID_CLIENTE,ID_CONTRAPARTE,ID_FORMA_OPERACION,DSC_FORMA_OPERACION,DSC_CONTRAPARTE"

                    SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = "INSERTAR"
                    SQLCommand.Parameters.Add("pIdcontraparte", SqlDbType.Float, 10).Value = pRow("ID_CONTRAPARTE").ToString
                    SQLCommand.Parameters.Add("pDscContraparte", SqlDbType.VarChar, 100).Value = pRow("DSC_CONTRAPARTE").ToString
                    SQLCommand.Parameters.Add("pIdcliente", SqlDbType.Float, 10).Value = strId_Cliente
                    SQLCommand.Parameters.Add("pIdforma_Operacion", SqlDbType.Float, 4).Value = pRow("ID_FORMA_OPERACION").ToString
                    SQLCommand.Parameters.Add("pDscFormaOperar", SqlDbType.VarChar, 100).Value = pRow("DSC_FORMA_OPERACION").ToString

                    '...Resultado
                    Dim pSalInstPort As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                    pSalInstPort.Direction = Data.ParameterDirection.Output
                    SQLCommand.Parameters.Add(pSalInstPort)

                    SQLCommand.ExecuteNonQuery()

                    strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
                    If Not strDescError.ToUpper.Trim = "OK" Then
                        Exit For
                    End If
                Next
            End If

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            FormaOperarXCliente_Mantenedor = strDescError
        End Try
    End Function
End Class
