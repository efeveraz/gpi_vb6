﻿Public Class ClsSucursal
    Public Function Sucursal_Ver(ByVal strCodigoSucursal As String, _
                                 ByVal strEstadoSucursal As String, _
                                 ByVal strColumnas As String, _
                                 ByRef strRetorno As String) As DataSet

        Dim LDS_Sucursal As New DataSet
        Dim LDS_Sucursal_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Sucursal_Consultar"
        Lstr_NombreTabla = "Sucursal"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdSucursal", SqlDbType.VarChar, 10).Value = IIf(strCodigoSucursal.Trim = "", DBNull.Value, strCodigoSucursal.Trim)
            SQLCommand.Parameters.Add("pEstadoSucursal", SqlDbType.Char, 3).Value = IIf(strEstadoSucursal.Trim = "", DBNull.Value, strEstadoSucursal.Trim)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Sucursal, Lstr_NombreTabla)

            LDS_Sucursal_Aux = LDS_Sucursal

            If strColumnas.Trim = "" Then
                LDS_Sucursal_Aux = LDS_Sucursal
            Else
                LDS_Sucursal_Aux = LDS_Sucursal.Copy
                For Each Columna In LDS_Sucursal.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Sucursal_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Sucursal_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_Sucursal.Dispose()
            Return LDS_Sucursal_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function Sucursal_Mantenedor(ByVal strAccion As String, _
                                        ByVal strIdSucursal As String, _
                                        ByVal strDscSucursal As String, _
                                        ByVal strEstSucursal As String, _
                                        ByVal lngIdRegionSucursal As Long) As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_Sucursal_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Sucursal_Mantencion"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 10).Value = strAccion
            SQLCommand.Parameters.Add("pIdSucursal", SqlDbType.VarChar, 10).Value = strIdSucursal
            SQLCommand.Parameters.Add("pDscSucursal", SqlDbType.VarChar, 100).Value = strDscSucursal
            SQLCommand.Parameters.Add("pEstSucursal", SqlDbType.VarChar, 3).Value = strEstSucursal
            SQLCommand.Parameters.Add("pIdRegionSucursal", SqlDbType.Float, 10).Value = IIf(lngIdRegionSucursal = 0, DBNull.Value, lngIdRegionSucursal)

            '...Resultado
            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            Sucursal_Mantenedor = strDescError
        End Try
    End Function

    Public Function RetornaSucursal(ByRef strRetorno As String, _
                                ByVal strColumnas As String, _
                                Optional ByVal strCodSucursal As String = "", _
                                Optional ByVal strDscSucursal As String = "") As DataSet
        Dim lblnRemove As Boolean = True
        Dim larr_NombreColumna() As String = Split(strColumnas, ",")
        Dim lInt_Col As Integer = 0
        Dim lInt_NomCol As String = ""

        Dim lDS_Sucursal As New DataSet
        Dim lDS_Sucursal_Aux As New DataSet

        Dim Sqlcommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim lstrProcedimiento As String = ""
        Dim LstrNombreTabla As String = ""

        lstrProcedimiento = "Sis_Sucursal_Ver"
        LstrNombreTabla = "Sucursal"
        Connect.Abrir()

        Try

            Sqlcommand = New SqlClient.SqlCommand(lstrProcedimiento, Connect.Conexion)

            Sqlcommand.CommandType = CommandType.StoredProcedure
            Sqlcommand.CommandText = lstrProcedimiento
            Sqlcommand.Parameters.Clear()

            Sqlcommand.Parameters.Add("@pCodSucursal", SqlDbType.VarChar, 10).Value = IIf(strCodSucursal = "", DBNull.Value, strCodSucursal)
            Sqlcommand.Parameters.Add("@pDscSucursal", SqlDbType.VarChar, 100).Value = IIf(strDscSucursal = "", DBNull.Value, strDscSucursal)

            Dim oParamSal As New SqlClient.SqlParameter("@pMsjRetorno", SqlDbType.Char, 60)
            oParamSal.Direction = ParameterDirection.Output
            Sqlcommand.Parameters.Add(oParamSal)


            SQLDataAdapter.SelectCommand = Sqlcommand
            SQLDataAdapter.Fill(lDS_Sucursal, LstrNombreTabla)

            If strColumnas.Trim = "" Then
                lDS_Sucursal_Aux = lDS_Sucursal
            Else
                lDS_Sucursal_Aux = lDS_Sucursal.Copy
                For Each lstrColumna As DataColumn In lDS_Sucursal.Tables(LstrNombreTabla).Columns
                    lblnRemove = True
                    For lInt_Col = 0 To larr_NombreColumna.Length - 1
                        lInt_NomCol = larr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                        If lstrColumna.ColumnName = lInt_NomCol Then
                            lblnRemove = False
                        End If
                    Next
                    If lblnRemove Then
                        lDS_Sucursal_Aux.Tables(LstrNombreTabla).Columns.Remove(lstrColumna.ColumnName)
                    End If
                Next
            End If

            For lInt_Col = 0 To larr_NombreColumna.Length - 1
                lInt_NomCol = larr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                For Each lstrColumna As DataColumn In lDS_Sucursal_Aux.Tables(LstrNombreTabla).Columns
                    If lstrColumna.ColumnName = lInt_NomCol Then
                        lstrColumna.SetOrdinal(lInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = Sqlcommand.Parameters("@pMsjRetorno").Value.ToString.Trim

            Return lDS_Sucursal_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function
End Class
