﻿Public Class ClsTiposContraPartes


    Public Function TiposContrapartes_Ver(ByVal strCodigo As String, _
                                          ByVal strColumnas As String, _
                                          ByRef strRetorno As String) As DataSet

        Dim LDS_TipoContraParte As New DataSet
        Dim LDS_TipoContraParte_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_TipoContraParte_Consultar"
        Lstr_NombreTabla = "TIPO_CONTRAPARTE"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdTipoContraparte", SqlDbType.VarChar, 4).Value = IIf(strCodigo.Trim = "", DBNull.Value, strCodigo.Trim)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_TipoContraParte, Lstr_NombreTabla)

            LDS_TipoContraParte_Aux = LDS_TipoContraParte

            If strColumnas.Trim = "" Then
                LDS_TipoContraParte_Aux = LDS_TipoContraParte
            Else
                LDS_TipoContraParte_Aux = LDS_TipoContraParte.Copy
                For Each Columna In LDS_TipoContraParte.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_TipoContraParte_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_TipoContraParte_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_TipoContraParte.Dispose()
            Return LDS_TipoContraParte_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function
  Public Function TipoContraparte_Mantenedor(ByVal strAccion As String, _
                                             ByVal IntIdTipoContraparte As Integer, _
                                             ByVal strDescripcion As String) As String

    Dim strDescError As String = ""
    Dim SQLCommand As New SqlClient.SqlCommand
    Dim SQLConnect As Cls_Conexion = New Cls_Conexion
    Dim MiTransaccionSQL As SqlClient.SqlTransaction

    '...Abre la conexion
    SQLConnect.Abrir()
    MiTransaccionSQL = SQLConnect.Transaccion

    Try
      SQLCommand = New SqlClient.SqlCommand("Rcp_TipoContraparte_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)

      SQLCommand.CommandType = CommandType.StoredProcedure
      SQLCommand.CommandText = "Rcp_TipoContraparte_Mantencion"
      SQLCommand.Parameters.Clear()

      SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 10).Value = strAccion
      SQLCommand.Parameters.Add("pIdTipoContraparte", SqlDbType.Float, 4).Value = IntIdTipoContraparte
      SQLCommand.Parameters.Add("pDescripcion", SqlDbType.VarChar, 50).Value = IIf(strDescripcion.Trim = "", DBNull.Value, strDescripcion.Trim)


      '...Resultado
      Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
      ParametroSal2.Direction = Data.ParameterDirection.Output
      SQLCommand.Parameters.Add(ParametroSal2)

      SQLCommand.ExecuteNonQuery()

      strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

      If strDescError.ToUpper.Trim = "OK" Then
        MiTransaccionSQL.Commit()
      Else
        MiTransaccionSQL.Rollback()
      End If

    Catch Ex As Exception
      MiTransaccionSQL.Rollback()
      strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
    Finally
      SQLConnect.Cerrar()
      TipoContraparte_Mantenedor = strDescError
    End Try
  End Function

End Class
