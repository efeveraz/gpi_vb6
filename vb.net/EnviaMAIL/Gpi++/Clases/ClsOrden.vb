﻿Imports Microsoft.Office.Interop
Public Class ClsOrden

    Public Function GuardarOrdenesHsbc(ByVal dtbSolicitudInversion As DataTable, _
                                       ByVal dtbOrdenes As DataTable, _
                                       Optional ByVal dtbFormaPagoCobro As DataTable = Nothing, _
                                       Optional ByRef lngIdSolicitudInversion As Long = 0, _
                                       Optional ByVal pblnManana As Boolean = False, _
                                       Optional ByVal dtbComision As DataTable = Nothing) As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction
        Dim llngIdDolInversion As Long = 0
        Dim llngIdOrden As Long = 0
        Dim llngIdMovCaja As Long = 0
        Dim llngIdCuenta As Long
        Dim lstrTipoOper As String

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try

            SQLCommand = New SqlClient.SqlCommand("Hsb_SolicitudInversion_Guardar", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Hsb_SolicitudInversion_Guardar"
            SQLCommand.Parameters.Clear()

            Dim dtrSolInversion As DataRow = dtbSolicitudInversion.Rows(0)

            llngIdCuenta = dtrSolInversion("ID_CUENTA")
            lstrTipoOper = dtrSolInversion("COD_TIPO_OPERACION")

            SQLCommand.Parameters.Add("pIdSistemaOrigen", SqlDbType.Float, 10).Value = dtrSolInversion("ID_SISTEMA_ORIGEN")
            SQLCommand.Parameters.Add("pCodMoneda", SqlDbType.VarChar, 3).Value = dtrSolInversion("COD_MONEDA")
            SQLCommand.Parameters.Add("pCodTipoOperacion", SqlDbType.VarChar, 10).Value = lstrTipoOper
            SQLCommand.Parameters.Add("pIdRepresentante", SqlDbType.Float, 10).Value = dtrSolInversion("ID_REPRESENTANTE")
            SQLCommand.Parameters.Add("pIdSistemaConfirmacion", SqlDbType.Float, 10).Value = dtrSolInversion("ID_SISTEMA_CONFIRMACION")
            SQLCommand.Parameters.Add("pIdTipoEstado", SqlDbType.Float, 10).Value = dtrSolInversion("ID_TIPO_ESTADO")
            SQLCommand.Parameters.Add("pCodEstado", SqlDbType.VarChar, 3).Value = dtrSolInversion("COD_ESTADO")
            SQLCommand.Parameters.Add("pIdTrader", SqlDbType.Float, 10).Value = IIf(dtrSolInversion("ID_TRADER") = 0, DBNull.Value, dtrSolInversion("ID_TRADER"))
            'SQLCommand.Parameters.Add("pCodSubClaseInstrumento", SqlDbType.VarChar, 10).Value = dtrSolInversion("COD_SUB_CLASE_INSTRUMENTO")
            SQLCommand.Parameters.Add("pIdContraparte", SqlDbType.Float, 10).Value = dtrSolInversion("ID_CONTRAPARTE")
            SQLCommand.Parameters.Add("pFlgTipoMovimiento", SqlDbType.Char, 1).Value = dtrSolInversion("FLG_TIPO_MOVIMIENTO")
            SQLCommand.Parameters.Add("pFechaOperacion", SqlDbType.Char, 10).Value = IIf(pblnManana, gFun_BuscarDiaHabil(gobjParametro.Cod_Pais.ValorParametro, dtrSolInversion("FECHA_OPERACION"), 1), dtrSolInversion("FECHA_OPERACION"))
            SQLCommand.Parameters.Add("pFechaVigencia", SqlDbType.Char, 10).Value = IIf(pblnManana, gFun_BuscarDiaHabil(gobjParametro.Cod_Pais.ValorParametro, dtrSolInversion("FECHA_VIGENCIA"), 1), dtrSolInversion("FECHA_VIGENCIA"))
            SQLCommand.Parameters.Add("pFechaLiquidacion", SqlDbType.Char, 10).Value = IIf(pblnManana, gFun_BuscarDiaHabil(gobjParametro.Cod_Pais.ValorParametro, dtrSolInversion("FECHA_LIQUIDACION"), 1), dtrSolInversion("FECHA_LIQUIDACION"))
            SQLCommand.Parameters.Add("pDscOperacion", SqlDbType.VarChar, 200).Value = dtrSolInversion("DSC_OPERACION")
            SQLCommand.Parameters.Add("pFechaConfirmacion", SqlDbType.Char, 10).Value = dtrSolInversion("FECHA_CONFIRMACION")
            SQLCommand.Parameters.Add("pPorcComision", SqlDbType.Decimal, 20).Value = dtrSolInversion("PORC_COMISION")
            SQLCommand.Parameters.Add("pComision", SqlDbType.Float, 20).Value = dtrSolInversion("COMISION")
            SQLCommand.Parameters.Add("pDerechos", SqlDbType.Float, 20).Value = dtrSolInversion("DERECHOS")
            SQLCommand.Parameters.Add("pGastos", SqlDbType.Float, 20).Value = dtrSolInversion("GASTOS")
            SQLCommand.Parameters.Add("pIva", SqlDbType.Float, 20).Value = dtrSolInversion("IVA")
            SQLCommand.Parameters.Add("pMontoOperacion", SqlDbType.Float, 20).Value = dtrSolInversion("MONTO_OPERACION")
            SQLCommand.Parameters.Add("pFlgLimitePrecio", SqlDbType.VarChar, 1).Value = dtrSolInversion("FLG_LIMITE_PRECIO")
            SQLCommand.Parameters.Add("pFlgTipoOrigen", SqlDbType.Char, 1).Value = dtrSolInversion("FLG_TIPO_ORIGEN")
            SQLCommand.Parameters.Add("pConfirmaComision", SqlDbType.Float, 20).Value = dtrSolInversion("CONFIRMA_COMISION")
            SQLCommand.Parameters.Add("pConfirmaDerechos", SqlDbType.Float, 20).Value = dtrSolInversion("CONFIRMA_DERECHOS")
            SQLCommand.Parameters.Add("pConfirmaGastos", SqlDbType.Float, 20).Value = dtrSolInversion("CONFIRMA_GASTOS")
            SQLCommand.Parameters.Add("pConfirmaIva", SqlDbType.Float, 20).Value = dtrSolInversion("CONFIRMA_IVA")
            SQLCommand.Parameters.Add("pConfirmaMontoOperacion", SqlDbType.Float, 20).Value = dtrSolInversion("CONFIRMA_MONTO_OPERACION")
            SQLCommand.Parameters.Add("pInterest", SqlDbType.Float, 18).Value = dtrSolInversion("INTEREST")
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = llngIdCuenta

            '...Id Solicitud de Inversion
            Dim ParametroSal1 As New SqlClient.SqlParameter("pIdSolInversion", SqlDbType.Float, 10)
            ParametroSal1.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal1)

            '...Resultado
            Dim ParametroSal As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then

                llngIdDolInversion = SQLCommand.Parameters("pIdSolInversion").Value
                lngIdSolicitudInversion = llngIdDolInversion

                If dtbOrdenes.Rows.Count > 0 Then
                    For Each dtrOrden As DataRow In dtbOrdenes.Rows

                        '+ Controlar por Medio de Paramteros si se llama al PL Hsb o al Rcp

                        SQLCommand = New SqlClient.SqlCommand("Hsb_Orden_Guardar", MiTransaccionSQL.Connection, MiTransaccionSQL)

                        SQLCommand.CommandType = CommandType.StoredProcedure
                        SQLCommand.CommandText = "Hsb_Orden_Guardar"
                        SQLCommand.Parameters.Clear()

                        SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = llngIdCuenta
                        SQLCommand.Parameters.Add("pCodTipoOperacion", SqlDbType.VarChar, 10).Value = lstrTipoOper
                        SQLCommand.Parameters.Add("pIdTipoEstado", SqlDbType.Float, 10).Value = dtrOrden("ID_TIPO_ESTADO")
                        SQLCommand.Parameters.Add("pCodEstado", SqlDbType.VarChar, 3).Value = dtrOrden("COD_ESTADO")
                        SQLCommand.Parameters.Add("pIdInstrumento", SqlDbType.Float, 10).Value = dtrOrden("ID_INSTRUMENTO")
                        SQLCommand.Parameters.Add("pCodMoneda", SqlDbType.VarChar, 3).Value = dtrOrden("COD_MONEDA")
                        SQLCommand.Parameters.Add("pIdSolInversion", SqlDbType.Float, 10).Value = llngIdDolInversion
                        SQLCommand.Parameters.Add("pCodMonedaPago", SqlDbType.VarChar, 3).Value = dtrOrden("COD_MONEDA_PAGO")
                        SQLCommand.Parameters.Add("pCantidad", SqlDbType.Decimal, 20).Value = dtrOrden("CANTIDAD")
                        SQLCommand.Parameters.Add("pPrecio", SqlDbType.Decimal, 20).Value = dtrOrden("PRECIO")
                        SQLCommand.Parameters.Add("pPrecioGestion", SqlDbType.Decimal, 24).Value = dtrOrden("PRECIO_GESTION")
                        SQLCommand.Parameters.Add("pPrecioHistoricoCompra", SqlDbType.Decimal, 20).Value = dtrOrden("PRECIO_HISTORICO_COMPRA")
                        SQLCommand.Parameters.Add("pTasa", SqlDbType.Decimal, 20).Value = dtrOrden("TASA")
                        SQLCommand.Parameters.Add("pPlazo", SqlDbType.Decimal, 6).Value = dtrOrden("PLAZO")
                        SQLCommand.Parameters.Add("pBase", SqlDbType.Decimal, 18).Value = dtrOrden("BASE")
                        SQLCommand.Parameters.Add("pValorDivisa", SqlDbType.Decimal, 16).Value = dtrOrden("VALOR_DIVISA")
                        SQLCommand.Parameters.Add("pComision", SqlDbType.Float, 20).Value = dtrOrden("COMISION")
                        SQLCommand.Parameters.Add("pInteres", SqlDbType.Float, 20).Value = dtrOrden("INTERES")
                        SQLCommand.Parameters.Add("pUtilidad", SqlDbType.Float, 20).Value = dtrOrden("UTILIDAD")
                        SQLCommand.Parameters.Add("pPorcComision", SqlDbType.Decimal, 20).Value = dtrOrden("PORC_COMISION")
                        SQLCommand.Parameters.Add("pMonto", SqlDbType.Float, 20).Value = dtrOrden("MONTO")
                        SQLCommand.Parameters.Add("pMontoPago", SqlDbType.Float, 10).Value = dtrOrden("MONTO_PAGO")
                        SQLCommand.Parameters.Add("pMontoBruto", SqlDbType.Float, 20).Value = dtrOrden("MONTO_BRUTO")
                        SQLCommand.Parameters.Add("pMontoTotal", SqlDbType.Float, 20).Value = dtrOrden("MONTO_TOTAL")
                        SQLCommand.Parameters.Add("pFlgMontoReferenciado", SqlDbType.VarChar, 1).Value = dtrOrden("FLG_MONTO_REFERENCIADO")
                        SQLCommand.Parameters.Add("pFlgTipoDeposito", SqlDbType.VarChar, 1).Value = dtrOrden("FLG_TIPO_DEPOSITO")
                        SQLCommand.Parameters.Add("pFlgVendeTodo", SqlDbType.VarChar, 1).Value = IIf(dtrOrden("FLG_VENDE_TODO") = "1", "S", "N")
                        SQLCommand.Parameters.Add("pFlgLimitePrecio", SqlDbType.VarChar, 1).Value = dtrOrden("FLG_LIMITE_PRECIO")
                        SQLCommand.Parameters.Add("pFlgTipoMovimiento", SqlDbType.Char, 1).Value = dtrOrden("FLG_TIPO_MOVIMIENTO")
                        SQLCommand.Parameters.Add("pFechaValuta", SqlDbType.Char, 10).Value = dtrOrden("FECHA_VALUTA")
                        SQLCommand.Parameters.Add("pFechaLiquidacion", SqlDbType.Char, 10).Value = IIf(pblnManana, gFun_BuscarDiaHabil(gobjParametro.Cod_Pais.ValorParametro, dtrOrden("FECHA_LIQUIDACION"), 1), dtrOrden("FECHA_LIQUIDACION"))
                        SQLCommand.Parameters.Add("pFechaVencimiento", SqlDbType.Char, 10).Value = dtrOrden("FECHA_VENCIMIENTO")
                        SQLCommand.Parameters.Add("pFechaOperacion", SqlDbType.Char, 10).Value = IIf(pblnManana, gFun_BuscarDiaHabil(gobjParametro.Cod_Pais.ValorParametro, dtrOrden("FECHA_OPERACION"), 1), dtrOrden("FECHA_OPERACION"))
                        SQLCommand.Parameters.Add("pFechaMovimiento", SqlDbType.Char, 10).Value = dtrOrden("FECHA_MOVIMIENTO")
                        SQLCommand.Parameters.Add("pFechaConfirmacion", SqlDbType.Char, 10).Value = dtrOrden("FECHA_CONFIRMACION")
                        SQLCommand.Parameters.Add("pFechaCierre", SqlDbType.Char, 10).Value = dtrOrden("FECHA_CIERRE")
                        SQLCommand.Parameters.Add("pEsOrdenOmnibus", SqlDbType.Char, 1).Value = dtrOrden("ES_ORDEN_OMNIBUS")
                        SQLCommand.Parameters.Add("pIdNormativo", SqlDbType.Float, 10).Value = dtrOrden("ID_NORMATIVO")
                        SQLCommand.Parameters.Add("pIdPortafolio", SqlDbType.Float, 10).Value = dtrOrden("ID_PORTAFOLIO")
                        SQLCommand.Parameters.Add("pNumDoctoPago", SqlDbType.Float, 10).Value = 0
                        SQLCommand.Parameters.Add("pIdCartera", SqlDbType.Float, 18).Value = dtrOrden("ID_CARTERA")
                        SQLCommand.Parameters.Add("pIdContraparte", SqlDbType.Float, 10).Value = dtrOrden("ID_CONTRAPARTE")
                        SQLCommand.Parameters.Add("pIdTrader", SqlDbType.Float, 18).Value = IIf(dtrOrden("ID_TRADER") = 0, DBNull.Value, dtrOrden("ID_TRADER"))
                        SQLCommand.Parameters.Add("pCodSubClaseInstrumento", SqlDbType.VarChar, 10).Value = dtrOrden("COD_SUB_CLASE_INSTRUMENTO")

                        '...Id Orden
                        Dim ParametroSal4 As New SqlClient.SqlParameter("pIdOrden", SqlDbType.Float, 10)
                        ParametroSal4.Direction = Data.ParameterDirection.Output
                        SQLCommand.Parameters.Add(ParametroSal4)

                        '...Resultado
                        Dim ParametroSal3 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                        ParametroSal3.Direction = Data.ParameterDirection.Output
                        SQLCommand.Parameters.Add(ParametroSal3)

                        SQLCommand.ExecuteNonQuery()

                        strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

                        If Trim(strDescError) = "OK" Then

                            llngIdOrden = SQLCommand.Parameters("pIdOrden").Value

                            If IsNothing(dtbComision) = False AndAlso dtbComision.Rows.Count > 0 Then

                                For Each dtrComision As DataRow In dtbComision.Select("ID_NUMEROLINEA_DETALLE=" & dtrOrden("ID_NUMEROLINEA_DETALLE"), "")


                                    SQLCommand = New SqlClient.SqlCommand("Rcp_ComisionDetalle_Guardar", MiTransaccionSQL.Connection, MiTransaccionSQL)

                                    SQLCommand.CommandType = CommandType.StoredProcedure
                                    SQLCommand.CommandText = "Rcp_ComisionDetalle_Guardar"
                                    SQLCommand.Parameters.Clear()

                                    SQLCommand.Parameters.Add("pIdComision", SqlDbType.Int, 10).Value = dtrComision("ID_COMISION")
                                    SQLCommand.Parameters.Add("pIdOrden", SqlDbType.Int, 10).Value = llngIdOrden
                                    SQLCommand.Parameters.Add("pIdOperacionDetalle", SqlDbType.Int, 8).Value = DBNull.Value
                                    SQLCommand.Parameters.Add("pIdPatrimonioCuenta", SqlDbType.Int, 8).Value = DBNull.Value
                                    SQLCommand.Parameters.Add("pPorcentaje", SqlDbType.Float, 7).Value = dtrComision("PORCENTAJE")
                                    SQLCommand.Parameters.Add("pMonto", SqlDbType.Float, 24).Value = dtrComision("MONTO")
                                    SQLCommand.Parameters.Add("pImpuesto", SqlDbType.Float, 24).Value = dtrComision("IMPUESTO")
                                    SQLCommand.Parameters.Add("pCodEstado", SqlDbType.VarChar, 3).Value = "V"

                                    '...Resultado
                                    Dim ParametroSalComm As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                                    ParametroSalComm.Direction = Data.ParameterDirection.Output
                                    SQLCommand.Parameters.Add(ParametroSalComm)

                                    SQLCommand.ExecuteNonQuery()

                                    strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

                                    If Trim(strDescError) <> "OK" Then
                                        GoTo Salir
                                    End If
                                Next

                            End If

                            If IsNothing(dtbFormaPagoCobro) = False AndAlso dtbFormaPagoCobro.Rows.Count > 0 Then

                                For Each dtrFormaPago As DataRow In dtbFormaPagoCobro.Rows

                                    SQLCommand = New SqlClient.SqlCommand("Rcp_MedioPagoCobro_Guardar", MiTransaccionSQL.Connection, MiTransaccionSQL)

                                    SQLCommand.CommandType = CommandType.StoredProcedure
                                    SQLCommand.CommandText = "Rcp_MedioPagoCobro_Guardar"
                                    SQLCommand.Parameters.Clear()

                                    SQLCommand.Parameters.Add("pOrigenMovimiento", SqlDbType.VarChar, 20).Value = "ORDEN"
                                    SQLCommand.Parameters.Add("pIdMedioPagoCobro", SqlDbType.Float, 10).Value = 0
                                    SQLCommand.Parameters.Add("pIdTipoEstado", SqlDbType.Float, 10).Value = dtrFormaPago("ID_TIPO_ESTADO")
                                    SQLCommand.Parameters.Add("pCodEstado", SqlDbType.VarChar, 3).Value = dtrFormaPago("COD_ESTADO")

                                    SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = dtrFormaPago("ID_CUENTA")
                                    SQLCommand.Parameters.Add("pIdBanco", SqlDbType.Float, 10).Value = dtrFormaPago("BANCO_ID")
                                    SQLCommand.Parameters.Add("pCodMedioPagoCobro", SqlDbType.VarChar, 10).Value = dtrFormaPago("COD_MEDIO_PAGO_COBRO")
                                    SQLCommand.Parameters.Add("pFechaMovimiento", SqlDbType.Char, 10).Value = dtrFormaPago("FECHA_MOVIMIENTO")
                                    SQLCommand.Parameters.Add("pFechaDocumento", SqlDbType.Char, 10).Value = dtrFormaPago("FECHA_DOCUMENTO")
                                    SQLCommand.Parameters.Add("pNumDocumento", SqlDbType.VarChar, 20).Value = dtrFormaPago("NUM_DOCUMENTO")
                                    SQLCommand.Parameters.Add("pRetencion", SqlDbType.Float, 10).Value = dtrFormaPago("RETENCION")
                                    SQLCommand.Parameters.Add("pMonto", SqlDbType.Float, 10).Value = dtrFormaPago("MONTO")
                                    SQLCommand.Parameters.Add("pCtaCteBancaria", SqlDbType.VarChar, 60).Value = dtrFormaPago("CTA_CTE_BANCARIA")
                                    SQLCommand.Parameters.Add("pIdOrden", SqlDbType.Float, 10).Value = llngIdOrden
                                    SQLCommand.Parameters.Add("pIdMovCaja", SqlDbType.Float, 10).Value = llngIdMovCaja

                                    '...Resultado
                                    Dim ParametroSal5 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                                    ParametroSal5.Direction = Data.ParameterDirection.Output
                                    SQLCommand.Parameters.Add(ParametroSal5)

                                    SQLCommand.ExecuteNonQuery()

                                    strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

                                    If Trim(strDescError) <> "OK" Then
                                        GoTo Salir
                                    End If
                                Next
                            End If


                        Else
                            GoTo Salir
                        End If
                    Next
                End If

            End If

Salir:
            If Trim(strDescError) = "OK" Then
                MiTransaccionSQL.Commit()
                Return ("OK")
            Else
                MiTransaccionSQL.Rollback()
                Return strDescError
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error al Grabar Solicitud de Inversión/Órden " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            GuardarOrdenesHsbc = strDescError
        End Try

    End Function

    Public Function GuardarOrdenes(ByVal dtbSolicitudInversion As DataTable, _
                                   ByVal dtbOrdenes As DataTable, _
                                   Optional ByVal dtbFormaPagoCobro As DataTable = Nothing, _
                                   Optional ByVal dtbComision As DataTable = Nothing, _
                                   Optional ByVal blnMultiDetalle As Boolean = False) As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction
        Dim llngIdDolInversion As Long = 0
        Dim llngIdOrden As Long = 0
        Dim llngIdMovCaja As Long = 0

        Dim ldblPorcComision As Double = 0
        Dim ldblMontoComision As Double = 0
        Dim ldblMontoImpuesto As Double = 0
        Dim ldblMontoComisionMasImpuesto As Double = 0

        Dim ldblPorcComisionDetalle As Double = 0
        Dim ldblMontoComisionDetalle As Double = 0
        Dim ldblMontoImpuestoDetalle As Double = 0
        Dim ldblMontoComisionMasImpuestoDetalle As Double = 0


        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try

            SQLCommand = New SqlClient.SqlCommand("Rcp_SolicitudInversion_Guardar", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_SolicitudInversion_Guardar"
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            Dim dtrSolInversion As DataRow = dtbSolicitudInversion.Rows(0)

            SQLCommand.Parameters.Add("pIdSistemaOrigen", SqlDbType.Float, 10).Value = dtrSolInversion("ID_SISTEMA_ORIGEN")
            SQLCommand.Parameters.Add("pCodMoneda", SqlDbType.VarChar, 3).Value = dtrSolInversion("COD_MONEDA")
            SQLCommand.Parameters.Add("pCodTipoOperacion", SqlDbType.VarChar, 10).Value = dtrSolInversion("COD_TIPO_OPERACION")
            SQLCommand.Parameters.Add("pIdRepresentante", SqlDbType.Float, 10).Value = dtrSolInversion("ID_REPRESENTANTE")
            SQLCommand.Parameters.Add("pIdSistemaConfirmacion", SqlDbType.Float, 10).Value = dtrSolInversion("ID_SISTEMA_CONFIRMACION")
            SQLCommand.Parameters.Add("pIdTipoEstado", SqlDbType.Float, 10).Value = dtrSolInversion("ID_TIPO_ESTADO")
            SQLCommand.Parameters.Add("pCodEstado", SqlDbType.VarChar, 3).Value = dtrSolInversion("COD_ESTADO")
            SQLCommand.Parameters.Add("pIdTrader", SqlDbType.Float, 10).Value = dtrSolInversion("ID_TRADER")
            'SQLCommand.Parameters.Add("pCodSubClaseInstrumento", SqlDbType.VarChar, 10).Value = dtrSolInversion("COD_SUB_CLASE_INSTRUMENTO")
            SQLCommand.Parameters.Add("pIdContraparte", SqlDbType.Float, 10).Value = dtrSolInversion("ID_CONTRAPARTE")
            SQLCommand.Parameters.Add("pFlgTipoMovimiento", SqlDbType.Char, 1).Value = dtrSolInversion("FLG_TIPO_MOVIMIENTO")
            SQLCommand.Parameters.Add("pFechaOperacion", SqlDbType.Char, 10).Value = dtrSolInversion("FECHA_OPERACION")
            SQLCommand.Parameters.Add("pFechaVigencia", SqlDbType.Char, 10).Value = dtrSolInversion("FECHA_VIGENCIA")
            SQLCommand.Parameters.Add("pFechaLiquidacion", SqlDbType.Char, 10).Value = dtrSolInversion("FECHA_LIQUIDACION")
            SQLCommand.Parameters.Add("pDscOperacion", SqlDbType.VarChar, 200).Value = dtrSolInversion("DSC_OPERACION")
            SQLCommand.Parameters.Add("pFechaConfirmacion", SqlDbType.Char, 10).Value = dtrSolInversion("FECHA_CONFIRMACION")
            SQLCommand.Parameters.Add("pPorcComision", SqlDbType.Decimal, 20).Value = dtrSolInversion("PORC_COMISION")
            SQLCommand.Parameters.Add("pComision", SqlDbType.Float, 20).Value = dtrSolInversion("COMISION")
            SQLCommand.Parameters.Add("pDerechos", SqlDbType.Float, 20).Value = dtrSolInversion("DERECHOS")
            SQLCommand.Parameters.Add("pGastos", SqlDbType.Float, 20).Value = dtrSolInversion("GASTOS")
            SQLCommand.Parameters.Add("pIva", SqlDbType.Float, 20).Value = dtrSolInversion("IVA")
            SQLCommand.Parameters.Add("pMontoOperacion", SqlDbType.Float, 20).Value = dtrSolInversion("MONTO_OPERACION")
            SQLCommand.Parameters.Add("pFlgLimitePrecio", SqlDbType.VarChar, 1).Value = dtrSolInversion("FLG_LIMITE_PRECIO")
            SQLCommand.Parameters.Add("pFlgTipoOrigen", SqlDbType.Char, 1).Value = dtrSolInversion("FLG_TIPO_ORIGEN")
            SQLCommand.Parameters.Add("pConfirmaComision", SqlDbType.Float, 20).Value = dtrSolInversion("CONFIRMA_COMISION")
            SQLCommand.Parameters.Add("pConfirmaDerechos", SqlDbType.Float, 20).Value = dtrSolInversion("CONFIRMA_DERECHOS")
            SQLCommand.Parameters.Add("pConfirmaGastos", SqlDbType.Float, 20).Value = dtrSolInversion("CONFIRMA_GASTOS")
            SQLCommand.Parameters.Add("pConfirmaIva", SqlDbType.Float, 20).Value = dtrSolInversion("CONFIRMA_IVA")
            SQLCommand.Parameters.Add("pConfirmaMontoOperacion", SqlDbType.Float, 20).Value = dtrSolInversion("CONFIRMA_MONTO_OPERACION")
            SQLCommand.Parameters.Add("pInterest", SqlDbType.Float, 18).Value = dtrSolInversion("INTEREST")
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = dtrSolInversion("ID_CUENTA")

            '...Id Solicitud de Inversion
            Dim ParametroSal1 As New SqlClient.SqlParameter("pIdSolInversion", SqlDbType.Float, 10)
            ParametroSal1.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal1)

            '...Resultado
            Dim ParametroSal As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then

                llngIdDolInversion = SQLCommand.Parameters("pIdSolInversion").Value

                Try
                    llngIdMovCaja = SQLCommand.Parameters("pIdMovCaja").Value
                Catch ex As Exception
                    llngIdMovCaja = 0
                End Try

                If dtbOrdenes.Rows.Count > 0 Then
                    'AGREGO LA COLUMNA ID_ORDEN, PARA IR GUARDANDO EL ID GENERADO
                    'QUE DESPUES SE OCUPA AL GUARDAR LA COMISION
                    If dtbOrdenes.Columns.IndexOf("ID_ORDEN") = -1 Then
                        dtbOrdenes.Columns.Add("ID_ORDEN", System.Type.GetType("System.Int64"))
                    End If

                    For Each dtrOrden As DataRow In dtbOrdenes.Rows

                        '+ Controlar por Medio de Paramteros si se llama al PL Hsb o al Rcp

                        SQLCommand = New SqlClient.SqlCommand("Rcp_Orden_Guardar", MiTransaccionSQL.Connection, MiTransaccionSQL)

                        SQLCommand.CommandType = CommandType.StoredProcedure
                        SQLCommand.CommandText = "Rcp_Orden_Guardar"
                        SQLCommand.Parameters.Clear()

                        SQLCommand.Parameters.Add("pIdTipoEstado", SqlDbType.Float, 10).Value = dtrOrden("ID_TIPO_ESTADO")
                        SQLCommand.Parameters.Add("pCodEstado", SqlDbType.VarChar, 3).Value = dtrOrden("COD_ESTADO")
                        SQLCommand.Parameters.Add("pIdInstrumento", SqlDbType.Float, 10).Value = dtrOrden("ID_INSTRUMENTO")
                        SQLCommand.Parameters.Add("pCodMoneda", SqlDbType.VarChar, 3).Value = dtrOrden("COD_MONEDA")
                        SQLCommand.Parameters.Add("pIdSolInversion", SqlDbType.Float, 10).Value = llngIdDolInversion
                        SQLCommand.Parameters.Add("pCodMonedaPago", SqlDbType.VarChar, 3).Value = dtrOrden("COD_MONEDA_PAGO")
                        SQLCommand.Parameters.Add("pCantidad", SqlDbType.Decimal, 20).Value = dtrOrden("CANTIDAD")
                        SQLCommand.Parameters.Add("pPrecio", SqlDbType.Decimal, 20).Value = dtrOrden("PRECIO")
                        SQLCommand.Parameters.Add("pPrecioGestion", SqlDbType.Decimal, 24).Value = dtrOrden("PRECIO_GESTION")
                        SQLCommand.Parameters.Add("pPrecioHistoricoCompra", SqlDbType.Decimal, 20).Value = dtrOrden("PRECIO_HISTORICO_COMPRA")
                        SQLCommand.Parameters.Add("pTasa", SqlDbType.Decimal, 20).Value = dtrOrden("TASA")
                        SQLCommand.Parameters.Add("pPlazo", SqlDbType.Decimal, 6).Value = dtrOrden("PLAZO")
                        SQLCommand.Parameters.Add("pBase", SqlDbType.Decimal, 18).Value = dtrOrden("BASE")
                        SQLCommand.Parameters.Add("pValorDivisa", SqlDbType.Decimal, 16).Value = dtrOrden("VALOR_DIVISA")
                        SQLCommand.Parameters.Add("pComision", SqlDbType.Float, 20).Value = dtrOrden("COMISION")
                        SQLCommand.Parameters.Add("pInteres", SqlDbType.Float, 20).Value = dtrOrden("INTERES")
                        SQLCommand.Parameters.Add("pUtilidad", SqlDbType.Float, 20).Value = dtrOrden("UTILIDAD")
                        SQLCommand.Parameters.Add("pPorcComision", SqlDbType.Decimal, 20).Value = dtrOrden("PORC_COMISION")
                        SQLCommand.Parameters.Add("pMonto", SqlDbType.Float, 20).Value = dtrOrden("MONTO")
                        SQLCommand.Parameters.Add("pMontoPago", SqlDbType.Float, 10).Value = dtrOrden("MONTO_PAGO")
                        SQLCommand.Parameters.Add("pMontoBruto", SqlDbType.Float, 20).Value = dtrOrden("MONTO_BRUTO")
                        SQLCommand.Parameters.Add("pMontoTotal", SqlDbType.Float, 20).Value = dtrOrden("MONTO_TOTAL")
                        SQLCommand.Parameters.Add("pFlgMontoReferenciado", SqlDbType.VarChar, 1).Value = dtrOrden("FLG_MONTO_REFERENCIADO")
                        SQLCommand.Parameters.Add("pFlgTipoDeposito", SqlDbType.VarChar, 1).Value = dtrOrden("FLG_TIPO_DEPOSITO")
                        SQLCommand.Parameters.Add("pFlgVendeTodo", SqlDbType.VarChar, 1).Value = IIf(dtrOrden("FLG_VENDE_TODO") = "1", "S", "N")
                        SQLCommand.Parameters.Add("pFlgLimitePrecio", SqlDbType.VarChar, 1).Value = dtrOrden("FLG_LIMITE_PRECIO")
                        SQLCommand.Parameters.Add("pFlgTipoMovimiento", SqlDbType.Char, 1).Value = dtrOrden("FLG_TIPO_MOVIMIENTO")
                        SQLCommand.Parameters.Add("pFechaValuta", SqlDbType.Char, 10).Value = dtrOrden("FECHA_VALUTA")
                        SQLCommand.Parameters.Add("pFechaLiquidacion", SqlDbType.Char, 10).Value = dtrOrden("FECHA_LIQUIDACION")
                        SQLCommand.Parameters.Add("pFechaVencimiento", SqlDbType.Char, 10).Value = dtrOrden("FECHA_VENCIMIENTO")
                        SQLCommand.Parameters.Add("pFechaOperacion", SqlDbType.Char, 10).Value = dtrOrden("FECHA_OPERACION")
                        SQLCommand.Parameters.Add("pFechaMovimiento", SqlDbType.Char, 10).Value = dtrOrden("FECHA_MOVIMIENTO")
                        SQLCommand.Parameters.Add("pFechaConfirmacion", SqlDbType.Char, 10).Value = dtrOrden("FECHA_CONFIRMACION")
                        SQLCommand.Parameters.Add("pFechaCierre", SqlDbType.Char, 10).Value = dtrOrden("FECHA_CIERRE")
                        SQLCommand.Parameters.Add("pEsOrdenOmnibus", SqlDbType.Char, 1).Value = dtrOrden("ES_ORDEN_OMNIBUS")
                        SQLCommand.Parameters.Add("pIdNormativo", SqlDbType.Float, 10).Value = dtrOrden("ID_NORMATIVO")
                        SQLCommand.Parameters.Add("pIdPortafolio", SqlDbType.Float, 10).Value = dtrOrden("ID_PORTAFOLIO")
                        SQLCommand.Parameters.Add("pNumDoctoPago", SqlDbType.Float, 10).Value = 0
                        SQLCommand.Parameters.Add("pIdCartera", SqlDbType.Float, 18).Value = dtrOrden("ID_CARTERA")
                        SQLCommand.Parameters.Add("pIdContraparte", SqlDbType.Float, 10).Value = dtrOrden("ID_CONTRAPARTE")
                        SQLCommand.Parameters.Add("pIdTrader", SqlDbType.Float, 18).Value = dtrOrden("ID_TRADER")
                        SQLCommand.Parameters.Add("pCodSubClaseInstrumento", SqlDbType.VarChar, 10).Value = dtrOrden("COD_SUB_CLASE_INSTRUMENTO")
                        '...Id Orden
                        Dim ParametroSal4 As New SqlClient.SqlParameter("pIdOrden", SqlDbType.Float, 10)
                        ParametroSal4.Direction = Data.ParameterDirection.Output
                        SQLCommand.Parameters.Add(ParametroSal4)

                        '...Resultado
                        Dim ParametroSal3 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                        ParametroSal3.Direction = Data.ParameterDirection.Output
                        SQLCommand.Parameters.Add(ParametroSal3)

                        SQLCommand.ExecuteNonQuery()

                        strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

                        If Trim(strDescError) = "OK" Then

                            llngIdOrden = SQLCommand.Parameters("pIdOrden").Value
                            dtrOrden("ID_ORDEN") = llngIdOrden

                            If IsNothing(dtbFormaPagoCobro) = False AndAlso dtbFormaPagoCobro.Rows.Count > 0 Then

                                For Each dtrFormaPago As DataRow In dtbFormaPagoCobro.Rows

                                    SQLCommand = New SqlClient.SqlCommand("Rcp_MedioPagoCobro_Guardar", MiTransaccionSQL.Connection, MiTransaccionSQL)

                                    SQLCommand.CommandType = CommandType.StoredProcedure
                                    SQLCommand.CommandText = "Rcp_MedioPagoCobro_Guardar"
                                    SQLCommand.Parameters.Clear()

                                    SQLCommand.Parameters.Add("pOrigenMovimiento", SqlDbType.VarChar, 20).Value = "ORDEN"
                                    SQLCommand.Parameters.Add("pIdMedioPagoCobro", SqlDbType.Float, 10).Value = 0
                                    SQLCommand.Parameters.Add("pIdTipoEstado", SqlDbType.Float, 10).Value = dtrFormaPago("ID_TIPO_ESTADO")
                                    SQLCommand.Parameters.Add("pCodEstado", SqlDbType.VarChar, 3).Value = dtrFormaPago("COD_ESTADO")

                                    SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = dtrFormaPago("ID_CUENTA")
                                    SQLCommand.Parameters.Add("pIdBanco", SqlDbType.Float, 10).Value = dtrFormaPago("BANCO_ID")
                                    SQLCommand.Parameters.Add("pCodMedioPagoCobro", SqlDbType.VarChar, 10).Value = dtrFormaPago("COD_MEDIO_PAGO_COBRO")
                                    SQLCommand.Parameters.Add("pFechaMovimiento", SqlDbType.Char, 10).Value = dtrFormaPago("FECHA_MOVIMIENTO")
                                    SQLCommand.Parameters.Add("pFechaDocumento", SqlDbType.Char, 10).Value = dtrFormaPago("FECHA_DOCUMENTO")
                                    SQLCommand.Parameters.Add("pNumDocumento", SqlDbType.VarChar, 20).Value = dtrFormaPago("NUM_DOCUMENTO")
                                    SQLCommand.Parameters.Add("pRetencion", SqlDbType.Float, 10).Value = dtrFormaPago("RETENCION")
                                    SQLCommand.Parameters.Add("pMonto", SqlDbType.Float, 10).Value = dtrFormaPago("MONTO")
                                    SQLCommand.Parameters.Add("pCtaCteBancaria", SqlDbType.VarChar, 60).Value = dtrFormaPago("CTA_CTE_BANCARIA")
                                    SQLCommand.Parameters.Add("pIdOrden", SqlDbType.Float, 10).Value = llngIdOrden
                                    SQLCommand.Parameters.Add("pIdMovCaja", SqlDbType.Float, 10).Value = DBNull.Value

                                    '...Resultado
                                    Dim ParametroSal5 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                                    ParametroSal5.Direction = Data.ParameterDirection.Output
                                    SQLCommand.Parameters.Add(ParametroSal5)

                                    SQLCommand.ExecuteNonQuery()

                                    strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

                                    If Trim(strDescError) <> "OK" Then
                                        GoTo Salir
                                    End If
                                Next

                            End If

                        Else
                            GoTo Salir
                        End If
                    Next

                    If strDescError = "OK" Then
                        If IsNothing(dtbComision) = False AndAlso dtbComision.Rows.Count > 0 Then
                            Dim ldblTotalOperacion As Double = dtbOrdenes.Compute("SUM(MONTO)", "")
                            Dim ldblTotalCobros As Double = 0
                            For Each dtrComision As DataRow In dtbComision.Rows

                                Dim lstrFiltro As String = ""
                                Dim lintFilasDetalle As Integer = 0

                                ldblPorcComision = 0
                                ldblMontoComision = 0
                                ldblMontoImpuesto = 0
                                ldblMontoComisionMasImpuesto = 0


                                ldblPorcComisionDetalle = 0
                                ldblMontoComisionDetalle = 0
                                ldblMontoImpuestoDetalle = 0
                                ldblMontoComisionMasImpuestoDetalle = 0

                                If blnMultiDetalle Then
                                    lstrFiltro = ""
                                    lintFilasDetalle = dtbOrdenes.Rows.Count
                                    ldblPorcComision = dtrComision("PORCENTAJE")
                                    If dtrComision("APLICA_PORCENTAJE") = True Then
                                        ldblTotalCobros = ldblTotalOperacion * dtrComision("PORCENTAJE")
                                        ldblPorcComisionDetalle = ldblPorcComision
                                    Else
                                        ldblTotalCobros = dtrComision("MONTO")
                                    End If

                                Else
                                    lstrFiltro = "ID_NUMEROLINEA_DETALLE=" & dtrComision("ID_NUMEROLINEA_DETALLE")
                                    ldblPorcComisionDetalle = dtrComision("PORCENTAJE")
                                    ldblMontoComisionDetalle = dtrComision("MONTO")
                                    ldblMontoImpuestoDetalle = dtrComision("IMPUESTO")
                                End If
                                Dim ldblTotalComisionDetalle As Double = 0
                                Dim lintFila As Integer = 0

                                For Each dtrOrden As DataRow In dtbOrdenes.Select(lstrFiltro, "")
                                    If blnMultiDetalle Then
                                        lintFila = lintFila + 1
                                        If lintFila = lintFilasDetalle Then
                                            ldblMontoComisionDetalle = ldblTotalCobros - ldblTotalComisionDetalle
                                        Else
                                            ldblMontoComisionDetalle = dtrOrden("MONTO") * ldblTotalCobros / ldblTotalOperacion
                                            ldblTotalComisionDetalle = ldblTotalComisionDetalle + ldblMontoComisionDetalle
                                        End If

                                        ''SI APLICA PORCENTAJE, CALCULO EL MONTO DE COMISION DE ACUERDO AL %
                                        'If dtrComision("APLICA_PORCENTAJE") = True Then
                                        '    ldblPorcComisionDetalle = dtrComision("PORCENTAJE")
                                        '    ldblMontoComisionDetalle = dtrOperacionDetalle("MONTO_BRUTO") * dtrComision("PORCENTAJE")
                                        'Else
                                        '    ldblPorcComisionDetalle = -1
                                        '    If lintFila = lintFilasDetalle Then
                                        '        ' SE CALCULA ASI PARA LA ULTIMA LINEA DE DETALLA PARA QUE NO QUEDEN RESIDUOS
                                        '        ldblMontoComisionDetalle = (ldblMontoComision - (ldblMontoComisionDetalle * (lintFilasDetalle - 1)))
                                        '    End If
                                        'End If

                                        'CALCULO IMPUESTO SI APLICA
                                        If dtrComision("AFECTO_IMPUESTO") = True Then
                                            ldblMontoImpuestoDetalle = ldblMontoComisionDetalle * (gobjParametro.PorcentajeIva.ValorParametro / 100)
                                        Else
                                            ldblMontoImpuestoDetalle = 0
                                        End If

                                        ldblMontoComisionMasImpuestoDetalle = ldblMontoComisionDetalle + ldblMontoImpuestoDetalle

                                    End If

                                    SQLCommand = New SqlClient.SqlCommand("Rcp_ComisionDetalle_Guardar", MiTransaccionSQL.Connection, MiTransaccionSQL)

                                    SQLCommand.CommandType = CommandType.StoredProcedure
                                    SQLCommand.CommandText = "Rcp_ComisionDetalle_Guardar"
                                    SQLCommand.Parameters.Clear()

                                    SQLCommand.Parameters.Add("pIdComision", SqlDbType.Int, 10).Value = dtrComision("ID_COMISION")
                                    SQLCommand.Parameters.Add("pIdOrden", SqlDbType.Int, 10).Value = dtrOrden("ID_ORDEN")
                                    SQLCommand.Parameters.Add("pIdOperacionDetalle", SqlDbType.Int, 8).Value = DBNull.Value
                                    SQLCommand.Parameters.Add("pIdPatrimonioCuenta", SqlDbType.Int, 8).Value = DBNull.Value
                                    SQLCommand.Parameters.Add("pPorcentaje", SqlDbType.Float, 7).Value = IIf(ldblPorcComisionDetalle = -1, DBNull.Value, ldblPorcComisionDetalle)
                                    SQLCommand.Parameters.Add("pMonto", SqlDbType.Float, 24).Value = ldblMontoComisionDetalle
                                    SQLCommand.Parameters.Add("pImpuesto", SqlDbType.Float, 24).Value = ldblMontoImpuestoDetalle
                                    SQLCommand.Parameters.Add("pCodEstado", SqlDbType.VarChar, 3).Value = "V"

                                    '...Resultado
                                    Dim ParametroSalComm As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                                    ParametroSalComm.Direction = Data.ParameterDirection.Output
                                    SQLCommand.Parameters.Add(ParametroSalComm)

                                    SQLCommand.ExecuteNonQuery()

                                    strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

                                    If Trim(strDescError) <> "OK" Then
                                        GoTo Salir
                                    End If
                                Next
                            Next
                        End If
                    Else
                        GoTo Salir
                    End If
                End If
            End If
Salir:
            If Trim(strDescError) = "OK" Then
                MiTransaccionSQL.Commit()
                Return ("OK")
            Else
                MiTransaccionSQL.Rollback()
                Return strDescError
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error al Grabar Solicitud de Inversión/Órden " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            GuardarOrdenes = strDescError
        End Try

    End Function

    Public Function ModificarOrdenesHsbc(ByVal dtbSolicitudInversion As DataTable, _
                                         ByVal dtbOrdenes As DataTable, _
                                         Optional ByVal dtbFormaPagoCobro As DataTable = Nothing) As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction
        Dim llngIdMovCaja As Long = 0
        Dim lstrTipoOper As String
        Dim llngIdCuenta As Long

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try

            SQLCommand = New SqlClient.SqlCommand("Hsb_SolicitudInversion_Modificar", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Hsb_SolicitudInversion_Modificar"
            SQLCommand.Parameters.Clear()

            Dim dtrSolInversion As DataRow = dtbSolicitudInversion.Rows(0)

            lstrTipoOper = dtrSolInversion("COD_TIPO_OPERACION")
            llngIdCuenta = dtrSolInversion("ID_CUENTA")

            SQLCommand.Parameters.Add("pIdSistemaOrigen", SqlDbType.Float, 10).Value = dtrSolInversion("ID_SISTEMA_ORIGEN")
            SQLCommand.Parameters.Add("pCodMoneda", SqlDbType.VarChar, 3).Value = dtrSolInversion("COD_MONEDA")
            SQLCommand.Parameters.Add("pCodTipoOperacion", SqlDbType.VarChar, 10).Value = lstrTipoOper
            SQLCommand.Parameters.Add("pIdRepresentante", SqlDbType.Float, 10).Value = dtrSolInversion("ID_REPRESENTANTE")
            SQLCommand.Parameters.Add("pIdSistemaConfirmacion", SqlDbType.Float, 10).Value = dtrSolInversion("ID_SISTEMA_CONFIRMACION")
            SQLCommand.Parameters.Add("pIdTipoEstado", SqlDbType.Float, 10).Value = dtrSolInversion("ID_TIPO_ESTADO")
            SQLCommand.Parameters.Add("pCodEstado", SqlDbType.VarChar, 3).Value = dtrSolInversion("COD_ESTADO")
            SQLCommand.Parameters.Add("pIdTrader", SqlDbType.Float, 10).Value = dtrSolInversion("ID_TRADER")
            'SQLCommand.Parameters.Add("pCodSubClaseInstrumento", SqlDbType.VarChar, 10).Value = dtrSolInversion("COD_SUB_CLASE_INSTRUMENTO")
            SQLCommand.Parameters.Add("pIdContraparte", SqlDbType.Float, 10).Value = dtrSolInversion("ID_CONTRAPARTE")
            SQLCommand.Parameters.Add("pFlgTipoMovimiento", SqlDbType.Char, 1).Value = dtrSolInversion("FLG_TIPO_MOVIMIENTO")
            SQLCommand.Parameters.Add("pFechaOperacion", SqlDbType.Char, 10).Value = dtrSolInversion("FECHA_OPERACION")
            SQLCommand.Parameters.Add("pFechaVigencia", SqlDbType.Char, 10).Value = dtrSolInversion("FECHA_VIGENCIA")
            SQLCommand.Parameters.Add("pFechaLiquidacion", SqlDbType.Char, 10).Value = dtrSolInversion("FECHA_LIQUIDACION")
            SQLCommand.Parameters.Add("pDscOperacion", SqlDbType.VarChar, 200).Value = dtrSolInversion("DSC_OPERACION")
            SQLCommand.Parameters.Add("pFechaConfirmacion", SqlDbType.Char, 10).Value = dtrSolInversion("FECHA_CONFIRMACION")
            SQLCommand.Parameters.Add("pPorcComision", SqlDbType.Decimal, 20).Value = dtrSolInversion("PORC_COMISION")
            SQLCommand.Parameters.Add("pComision", SqlDbType.Float, 20).Value = dtrSolInversion("COMISION")
            SQLCommand.Parameters.Add("pDerechos", SqlDbType.Float, 20).Value = dtrSolInversion("DERECHOS")
            SQLCommand.Parameters.Add("pGastos", SqlDbType.Float, 20).Value = dtrSolInversion("GASTOS")
            SQLCommand.Parameters.Add("pIva", SqlDbType.Float, 20).Value = dtrSolInversion("IVA")
            SQLCommand.Parameters.Add("pMontoOperacion", SqlDbType.Float, 20).Value = dtrSolInversion("MONTO_OPERACION")
            SQLCommand.Parameters.Add("pFlgLimitePrecio", SqlDbType.VarChar, 1).Value = dtrSolInversion("FLG_LIMITE_PRECIO")
            SQLCommand.Parameters.Add("pFlgTipoOrigen", SqlDbType.Char, 1).Value = dtrSolInversion("FLG_TIPO_ORIGEN")
            SQLCommand.Parameters.Add("pConfirmaComision", SqlDbType.Float, 20).Value = dtrSolInversion("CONFIRMA_COMISION")
            SQLCommand.Parameters.Add("pConfirmaDerechos", SqlDbType.Float, 20).Value = dtrSolInversion("CONFIRMA_DERECHOS")
            SQLCommand.Parameters.Add("pConfirmaGastos", SqlDbType.Float, 20).Value = dtrSolInversion("CONFIRMA_GASTOS")
            SQLCommand.Parameters.Add("pConfirmaIva", SqlDbType.Float, 20).Value = dtrSolInversion("CONFIRMA_IVA")
            SQLCommand.Parameters.Add("pConfirmaMontoOperacion", SqlDbType.Float, 20).Value = dtrSolInversion("CONFIRMA_MONTO_OPERACION")
            SQLCommand.Parameters.Add("pInterest", SqlDbType.Float, 18).Value = dtrSolInversion("INTEREST")
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = llngIdCuenta
            SQLCommand.Parameters.Add("pIdSolInversion", SqlDbType.Float, 10).Value = dtrSolInversion("ID_SOL_INVERSION")

            '...Resultado
            Dim ParametroSal As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then

                Try
                    llngIdMovCaja = SQLCommand.Parameters("pIdMovCaja").Value
                Catch ex As Exception
                    llngIdMovCaja = 0
                End Try

                If dtbOrdenes.Rows.Count > 0 Then
                    For Each dtrOrden As DataRow In dtbOrdenes.Rows

                        '+ Controlar por Medio de Paramteros si se llama al PL Hsb o al Rcp

                        SQLCommand = New SqlClient.SqlCommand("Hsb_Orden_Modificar", MiTransaccionSQL.Connection, MiTransaccionSQL)

                        SQLCommand.CommandType = CommandType.StoredProcedure
                        SQLCommand.CommandText = "Hsb_Orden_Modificar"
                        SQLCommand.Parameters.Clear()

                        SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = llngIdCuenta
                        SQLCommand.Parameters.Add("pCodTipoOperacion", SqlDbType.VarChar, 10).Value = lstrTipoOper
                        SQLCommand.Parameters.Add("pIdTipoEstado", SqlDbType.Float, 10).Value = dtrOrden("ID_TIPO_ESTADO")
                        SQLCommand.Parameters.Add("pCodEstado", SqlDbType.VarChar, 3).Value = dtrOrden("COD_ESTADO")
                        SQLCommand.Parameters.Add("pIdInstrumento", SqlDbType.Float, 10).Value = dtrOrden("ID_INSTRUMENTO")
                        SQLCommand.Parameters.Add("pCodMoneda", SqlDbType.VarChar, 3).Value = dtrOrden("COD_MONEDA")
                        SQLCommand.Parameters.Add("pIdSolInversion", SqlDbType.Float, 10).Value = dtrSolInversion("ID_SOL_INVERSION")
                        SQLCommand.Parameters.Add("pCodMonedaPago", SqlDbType.VarChar, 3).Value = dtrOrden("COD_MONEDA_PAGO")
                        SQLCommand.Parameters.Add("pCantidad", SqlDbType.Decimal, 20).Value = dtrOrden("CANTIDAD")
                        SQLCommand.Parameters.Add("pPrecio", SqlDbType.Decimal, 20).Value = dtrOrden("PRECIO")
                        SQLCommand.Parameters.Add("pPrecioGestion", SqlDbType.Decimal, 24).Value = dtrOrden("PRECIO_GESTION")
                        SQLCommand.Parameters.Add("pPrecioHistoricoCompra", SqlDbType.Decimal, 20).Value = dtrOrden("PRECIO_HISTORICO_COMPRA")
                        SQLCommand.Parameters.Add("pTasa", SqlDbType.Decimal, 20).Value = dtrOrden("TASA")
                        SQLCommand.Parameters.Add("pPlazo", SqlDbType.Decimal, 6).Value = dtrOrden("PLAZO")
                        SQLCommand.Parameters.Add("pBase", SqlDbType.Decimal, 18).Value = dtrOrden("BASE")
                        SQLCommand.Parameters.Add("pValorDivisa", SqlDbType.Decimal, 16).Value = dtrOrden("VALOR_DIVISA")
                        SQLCommand.Parameters.Add("pComision", SqlDbType.Float, 20).Value = dtrOrden("COMISION")
                        SQLCommand.Parameters.Add("pInteres", SqlDbType.Float, 20).Value = dtrOrden("INTERES")
                        SQLCommand.Parameters.Add("pUtilidad", SqlDbType.Float, 20).Value = dtrOrden("UTILIDAD")
                        SQLCommand.Parameters.Add("pPorcComision", SqlDbType.Decimal, 20).Value = dtrOrden("PORC_COMISION")
                        SQLCommand.Parameters.Add("pMonto", SqlDbType.Float, 20).Value = dtrOrden("MONTO")
                        SQLCommand.Parameters.Add("pMontoPago", SqlDbType.Float, 10).Value = dtrOrden("MONTO_PAGO")
                        SQLCommand.Parameters.Add("pMontoBruto", SqlDbType.Float, 20).Value = 0 'dtrOrden("MONTO_BRUTO")
                        SQLCommand.Parameters.Add("pMontoTotal", SqlDbType.Float, 20).Value = dtrOrden("MONTO_TOTAL")
                        SQLCommand.Parameters.Add("pFlgMontoReferenciado", SqlDbType.VarChar, 1).Value = dtrOrden("FLG_MONTO_REFERENCIADO")
                        SQLCommand.Parameters.Add("pFlgTipoDeposito", SqlDbType.VarChar, 1).Value = dtrOrden("FLG_TIPO_DEPOSITO")
                        SQLCommand.Parameters.Add("pFlgVendeTodo", SqlDbType.VarChar, 1).Value = IIf(dtrOrden("FLG_VENDE_TODO") = "1", "S", "N")
                        SQLCommand.Parameters.Add("pFlgLimitePrecio", SqlDbType.VarChar, 1).Value = dtrOrden("FLG_LIMITE_PRECIO")
                        SQLCommand.Parameters.Add("pFlgTipoMovimiento", SqlDbType.Char, 1).Value = dtrOrden("FLG_TIPO_MOVIMIENTO")
                        SQLCommand.Parameters.Add("pFechaValuta", SqlDbType.Char, 10).Value = dtrOrden("FECHA_VALUTA")
                        SQLCommand.Parameters.Add("pFechaLiquidacion", SqlDbType.Char, 10).Value = dtrOrden("FECHA_LIQUIDACION")
                        SQLCommand.Parameters.Add("pFechaVencimiento", SqlDbType.Char, 10).Value = dtrOrden("FECHA_VENCIMIENTO")
                        SQLCommand.Parameters.Add("pFechaOperacion", SqlDbType.Char, 10).Value = dtrOrden("FECHA_OPERACION")
                        SQLCommand.Parameters.Add("pFechaMovimiento", SqlDbType.Char, 10).Value = dtrOrden("FECHA_MOVIMIENTO")
                        SQLCommand.Parameters.Add("pFechaConfirmacion", SqlDbType.Char, 10).Value = dtrOrden("FECHA_CONFIRMACION")
                        SQLCommand.Parameters.Add("pFechaCierre", SqlDbType.Char, 10).Value = dtrOrden("FECHA_CIERRE")
                        SQLCommand.Parameters.Add("pEsOrdenOmnibus", SqlDbType.Char, 1).Value = dtrOrden("ES_ORDEN_OMNIBUS")
                        SQLCommand.Parameters.Add("pIdNormativo", SqlDbType.Float, 10).Value = dtrOrden("ID_NORMATIVO")
                        SQLCommand.Parameters.Add("pIdPortafolio", SqlDbType.Float, 10).Value = dtrOrden("ID_PORTAFOLIO")
                        SQLCommand.Parameters.Add("pNumDoctoPago", SqlDbType.Float, 10).Value = 0
                        SQLCommand.Parameters.Add("pIdCartera", SqlDbType.Float, 18).Value = dtrOrden("ID_CARTERA")
                        SQLCommand.Parameters.Add("pIdContraparte", SqlDbType.Float, 10).Value = dtrOrden("ID_CONTRAPARTE")
                        SQLCommand.Parameters.Add("pIdTrader", SqlDbType.Float, 18).Value = dtrOrden("ID_TRADER")
                        SQLCommand.Parameters.Add("pSaldoporAsignar", SqlDbType.Float, 20).Value = dtrOrden("SALDO_POR_ASIGNAR")
                        SQLCommand.Parameters.Add("pIdOrden", SqlDbType.Float, 10).Value = dtrOrden("ID_ORDEN")
                        SQLCommand.Parameters.Add("pCodSubClaseInstrumento", SqlDbType.VarChar, 10).Value = dtrOrden("COD_SUB_CLASE_INSTRUMENTO")
                        '...Resultado
                        Dim ParametroSal3 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                        ParametroSal3.Direction = Data.ParameterDirection.Output
                        SQLCommand.Parameters.Add(ParametroSal3)

                        SQLCommand.ExecuteNonQuery()

                        strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

                        If Trim(strDescError) = "OK" Then

                            If IsNothing(dtbFormaPagoCobro) = False AndAlso dtbFormaPagoCobro.Rows.Count > 0 Then

                                For Each dtrFormaPago As DataRow In dtbFormaPagoCobro.Rows

                                    SQLCommand = New SqlClient.SqlCommand("Rcp_MedioPagoCobro_Guardar", MiTransaccionSQL.Connection, MiTransaccionSQL)

                                    SQLCommand.CommandType = CommandType.StoredProcedure
                                    SQLCommand.CommandText = "Rcp_MedioPagoCobro_Guardar"
                                    SQLCommand.Parameters.Clear()

                                    SQLCommand.Parameters.Add("pOrigenMovimiento", SqlDbType.VarChar, 20).Value = "ORDEN"
                                    SQLCommand.Parameters.Add("pIdMedioPagoCobro", SqlDbType.Float, 10).Value = 0
                                    SQLCommand.Parameters.Add("pIdTipoEstado", SqlDbType.Float, 10).Value = dtrFormaPago("ID_TIPO_ESTADO")
                                    SQLCommand.Parameters.Add("pCodEstado", SqlDbType.VarChar, 3).Value = dtrFormaPago("COD_ESTADO")

                                    SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = dtrFormaPago("ID_CUENTA")
                                    SQLCommand.Parameters.Add("pIdBanco", SqlDbType.Float, 10).Value = dtrFormaPago("BANCO_ID")
                                    SQLCommand.Parameters.Add("pCodMedioPagoCobro", SqlDbType.VarChar, 10).Value = dtrFormaPago("COD_MEDIO_PAGO_COBRO")
                                    SQLCommand.Parameters.Add("pFechaMovimiento", SqlDbType.Char, 10).Value = dtrFormaPago("FECHA_MOVIMIENTO")
                                    SQLCommand.Parameters.Add("pFechaDocumento", SqlDbType.Char, 10).Value = dtrFormaPago("FECHA_DOCUMENTO")
                                    SQLCommand.Parameters.Add("pNumDocumento", SqlDbType.VarChar, 20).Value = dtrFormaPago("NUM_DOCUMENTO")
                                    SQLCommand.Parameters.Add("pRetencion", SqlDbType.Float, 10).Value = dtrFormaPago("RETENCION")
                                    SQLCommand.Parameters.Add("pMonto", SqlDbType.Float, 10).Value = dtrFormaPago("MONTO")
                                    SQLCommand.Parameters.Add("pCtaCteBancaria", SqlDbType.VarChar, 60).Value = dtrFormaPago("CTA_CTE_BANCARIA")
                                    SQLCommand.Parameters.Add("pIdOrden", SqlDbType.Float, 10).Value = dtrOrden("ID_ORDEN")
                                    SQLCommand.Parameters.Add("pIdMovCaja", SqlDbType.Float, 10).Value = DBNull.Value

                                    '...Resultado
                                    Dim ParametroSal5 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                                    ParametroSal5.Direction = Data.ParameterDirection.Output
                                    SQLCommand.Parameters.Add(ParametroSal5)

                                    SQLCommand.ExecuteNonQuery()

                                    strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

                                    If Trim(strDescError) <> "OK" Then
                                        GoTo Salir
                                    End If
                                Next
                            End If
                        Else
                            GoTo Salir
                        End If
                    Next
                End If

            End If

Salir:
            If Trim(strDescError) = "OK" Then
                MiTransaccionSQL.Commit()
                Return ("OK")
            Else
                MiTransaccionSQL.Rollback()
                Return strDescError
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error al Grabar Solicitud de Inversión/Órden " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            ModificarOrdenesHsbc = strDescError
        End Try

    End Function

    Public Function ModificarOrdenes(ByVal dtbSolicitudInversion As DataTable, _
                                   ByVal dtbOrdenes As DataTable, _
                                   Optional ByVal dtbFormaPagoCobro As DataTable = Nothing) As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try

            SQLCommand = New SqlClient.SqlCommand("Rcp_SolicitudInversion_Modificar", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_SolicitudInversion_Modificar"
            SQLCommand.Parameters.Clear()

            Dim dtrSolInversion As DataRow = dtbSolicitudInversion.Rows(0)

            SQLCommand.Parameters.Add("pIdSistemaOrigen", SqlDbType.Float, 10).Value = dtrSolInversion("ID_SISTEMA_ORIGEN")
            SQLCommand.Parameters.Add("pCodMoneda", SqlDbType.VarChar, 3).Value = dtrSolInversion("COD_MONEDA")
            SQLCommand.Parameters.Add("pCodTipoOperacion", SqlDbType.VarChar, 10).Value = dtrSolInversion("COD_TIPO_OPERACION")
            SQLCommand.Parameters.Add("pIdRepresentante", SqlDbType.Float, 10).Value = dtrSolInversion("ID_REPRESENTANTE")
            SQLCommand.Parameters.Add("pIdSistemaConfirmacion", SqlDbType.Float, 10).Value = dtrSolInversion("ID_SISTEMA_CONFIRMACION")
            SQLCommand.Parameters.Add("pIdTipoEstado", SqlDbType.Float, 10).Value = dtrSolInversion("ID_TIPO_ESTADO")
            SQLCommand.Parameters.Add("pCodEstado", SqlDbType.VarChar, 3).Value = dtrSolInversion("COD_ESTADO")
            SQLCommand.Parameters.Add("pIdTrader", SqlDbType.Float, 10).Value = dtrSolInversion("ID_TRADER")
            'SQLCommand.Parameters.Add("pCodSubClaseInstrumento", SqlDbType.VarChar, 10).Value = dtrSolInversion("COD_SUB_CLASE_INSTRUMENTO")
            SQLCommand.Parameters.Add("pIdContraparte", SqlDbType.Float, 10).Value = dtrSolInversion("ID_CONTRAPARTE")
            SQLCommand.Parameters.Add("pFlgTipoMovimiento", SqlDbType.Char, 1).Value = dtrSolInversion("FLG_TIPO_MOVIMIENTO")
            SQLCommand.Parameters.Add("pFechaOperacion", SqlDbType.Char, 10).Value = dtrSolInversion("FECHA_OPERACION")
            SQLCommand.Parameters.Add("pFechaVigencia", SqlDbType.Char, 10).Value = dtrSolInversion("FECHA_VIGENCIA")
            SQLCommand.Parameters.Add("pFechaLiquidacion", SqlDbType.Char, 10).Value = dtrSolInversion("FECHA_LIQUIDACION")
            SQLCommand.Parameters.Add("pDscOperacion", SqlDbType.VarChar, 200).Value = dtrSolInversion("DSC_OPERACION")
            SQLCommand.Parameters.Add("pFechaConfirmacion", SqlDbType.Char, 10).Value = dtrSolInversion("FECHA_CONFIRMACION")
            SQLCommand.Parameters.Add("pPorcComision", SqlDbType.Decimal, 20).Value = dtrSolInversion("PORC_COMISION")
            SQLCommand.Parameters.Add("pComision", SqlDbType.Float, 20).Value = dtrSolInversion("COMISION")
            SQLCommand.Parameters.Add("pDerechos", SqlDbType.Float, 20).Value = dtrSolInversion("DERECHOS")
            SQLCommand.Parameters.Add("pGastos", SqlDbType.Float, 20).Value = dtrSolInversion("GASTOS")
            SQLCommand.Parameters.Add("pIva", SqlDbType.Float, 20).Value = dtrSolInversion("IVA")
            SQLCommand.Parameters.Add("pMontoOperacion", SqlDbType.Float, 20).Value = dtrSolInversion("MONTO_OPERACION")
            SQLCommand.Parameters.Add("pFlgLimitePrecio", SqlDbType.VarChar, 1).Value = dtrSolInversion("FLG_LIMITE_PRECIO")
            SQLCommand.Parameters.Add("pFlgTipoOrigen", SqlDbType.Char, 1).Value = dtrSolInversion("FLG_TIPO_ORIGEN")
            SQLCommand.Parameters.Add("pConfirmaComision", SqlDbType.Float, 20).Value = dtrSolInversion("CONFIRMA_COMISION")
            SQLCommand.Parameters.Add("pConfirmaDerechos", SqlDbType.Float, 20).Value = dtrSolInversion("CONFIRMA_DERECHOS")
            SQLCommand.Parameters.Add("pConfirmaGastos", SqlDbType.Float, 20).Value = dtrSolInversion("CONFIRMA_GASTOS")
            SQLCommand.Parameters.Add("pConfirmaIva", SqlDbType.Float, 20).Value = dtrSolInversion("CONFIRMA_IVA")
            SQLCommand.Parameters.Add("pConfirmaMontoOperacion", SqlDbType.Float, 20).Value = dtrSolInversion("CONFIRMA_MONTO_OPERACION")
            SQLCommand.Parameters.Add("pInterest", SqlDbType.Float, 18).Value = dtrSolInversion("INTEREST")
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = dtrSolInversion("ID_CUENTA")
            SQLCommand.Parameters.Add("pIdSolInversion", SqlDbType.Float, 10).Value = dtrSolInversion("ID_SOL_INVERSION")

            '...Resultado
            Dim ParametroSal As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then

                If dtbOrdenes.Rows.Count > 0 Then
                    For Each dtrOrden As DataRow In dtbOrdenes.Rows

                        '+ Controlar por Medio de Paramteros si se llama al PL Hsb o al Rcp

                        SQLCommand = New SqlClient.SqlCommand("Rcp_Orden_Modificar", MiTransaccionSQL.Connection, MiTransaccionSQL)

                        SQLCommand.CommandType = CommandType.StoredProcedure
                        SQLCommand.CommandText = "Rcp_Orden_Modificar"
                        SQLCommand.Parameters.Clear()

                        SQLCommand.Parameters.Add("pIdTipoEstado", SqlDbType.Float, 10).Value = dtrOrden("ID_TIPO_ESTADO")
                        SQLCommand.Parameters.Add("pCodEstado", SqlDbType.VarChar, 3).Value = dtrOrden("COD_ESTADO")
                        SQLCommand.Parameters.Add("pIdInstrumento", SqlDbType.Float, 10).Value = dtrOrden("ID_INSTRUMENTO")
                        SQLCommand.Parameters.Add("pCodMoneda", SqlDbType.VarChar, 3).Value = dtrOrden("COD_MONEDA")
                        SQLCommand.Parameters.Add("pIdSolInversion", SqlDbType.Float, 10).Value = dtrSolInversion("ID_SOL_INVERSION")
                        SQLCommand.Parameters.Add("pCodMonedaPago", SqlDbType.VarChar, 3).Value = dtrOrden("COD_MONEDA_PAGO")
                        SQLCommand.Parameters.Add("pCantidad", SqlDbType.Decimal, 20).Value = dtrOrden("CANTIDAD")
                        SQLCommand.Parameters.Add("pPrecio", SqlDbType.Decimal, 20).Value = dtrOrden("PRECIO")
                        SQLCommand.Parameters.Add("pPrecioGestion", SqlDbType.Decimal, 24).Value = dtrOrden("PRECIO_GESTION")
                        SQLCommand.Parameters.Add("pPrecioHistoricoCompra", SqlDbType.Decimal, 20).Value = dtrOrden("PRECIO_HISTORICO_COMPRA")
                        SQLCommand.Parameters.Add("pTasa", SqlDbType.Decimal, 20).Value = dtrOrden("TASA")
                        SQLCommand.Parameters.Add("pPlazo", SqlDbType.Decimal, 6).Value = dtrOrden("PLAZO")
                        SQLCommand.Parameters.Add("pBase", SqlDbType.Decimal, 18).Value = dtrOrden("BASE")
                        SQLCommand.Parameters.Add("pValorDivisa", SqlDbType.Decimal, 16).Value = dtrOrden("VALOR_DIVISA")
                        SQLCommand.Parameters.Add("pComision", SqlDbType.Float, 20).Value = dtrOrden("COMISION")
                        SQLCommand.Parameters.Add("pInteres", SqlDbType.Float, 20).Value = dtrOrden("INTERES")
                        SQLCommand.Parameters.Add("pUtilidad", SqlDbType.Float, 20).Value = dtrOrden("UTILIDAD")
                        SQLCommand.Parameters.Add("pPorcComision", SqlDbType.Decimal, 20).Value = dtrOrden("PORC_COMISION")
                        SQLCommand.Parameters.Add("pMonto", SqlDbType.Float, 20).Value = dtrOrden("MONTO")
                        SQLCommand.Parameters.Add("pMontoPago", SqlDbType.Float, 10).Value = dtrOrden("MONTO_PAGO")
                        SQLCommand.Parameters.Add("pMontoBruto", SqlDbType.Float, 20).Value = dtrOrden("MONTO_BRUTO")
                        SQLCommand.Parameters.Add("pMontoTotal", SqlDbType.Float, 20).Value = dtrOrden("MONTO_TOTAL")
                        SQLCommand.Parameters.Add("pFlgMontoReferenciado", SqlDbType.VarChar, 1).Value = dtrOrden("FLG_MONTO_REFERENCIADO")
                        SQLCommand.Parameters.Add("pFlgTipoDeposito", SqlDbType.VarChar, 1).Value = dtrOrden("FLG_TIPO_DEPOSITO")
                        SQLCommand.Parameters.Add("pFlgVendeTodo", SqlDbType.VarChar, 1).Value = IIf(dtrOrden("FLG_VENDE_TODO") = "1", "S", "N")
                        SQLCommand.Parameters.Add("pFlgLimitePrecio", SqlDbType.VarChar, 1).Value = dtrOrden("FLG_LIMITE_PRECIO")
                        SQLCommand.Parameters.Add("pFlgTipoMovimiento", SqlDbType.Char, 1).Value = dtrOrden("FLG_TIPO_MOVIMIENTO")
                        SQLCommand.Parameters.Add("pFechaValuta", SqlDbType.Char, 10).Value = dtrOrden("FECHA_VALUTA")
                        SQLCommand.Parameters.Add("pFechaLiquidacion", SqlDbType.Char, 10).Value = dtrOrden("FECHA_LIQUIDACION")
                        SQLCommand.Parameters.Add("pFechaVencimiento", SqlDbType.Char, 10).Value = dtrOrden("FECHA_VENCIMIENTO")
                        SQLCommand.Parameters.Add("pFechaOperacion", SqlDbType.Char, 10).Value = dtrOrden("FECHA_OPERACION")
                        SQLCommand.Parameters.Add("pFechaMovimiento", SqlDbType.Char, 10).Value = dtrOrden("FECHA_MOVIMIENTO")
                        SQLCommand.Parameters.Add("pFechaConfirmacion", SqlDbType.Char, 10).Value = dtrOrden("FECHA_CONFIRMACION")
                        SQLCommand.Parameters.Add("pFechaCierre", SqlDbType.Char, 10).Value = dtrOrden("FECHA_CIERRE")
                        SQLCommand.Parameters.Add("pEsOrdenOmnibus", SqlDbType.Char, 1).Value = dtrOrden("ES_ORDEN_OMNIBUS")
                        SQLCommand.Parameters.Add("pIdNormativo", SqlDbType.Float, 10).Value = dtrOrden("ID_NORMATIVO")
                        SQLCommand.Parameters.Add("pIdPortafolio", SqlDbType.Float, 10).Value = dtrOrden("ID_PORTAFOLIO")
                        SQLCommand.Parameters.Add("pNumDoctoPago", SqlDbType.Float, 10).Value = 0
                        SQLCommand.Parameters.Add("pIdCartera", SqlDbType.Float, 18).Value = dtrOrden("ID_CARTERA")
                        SQLCommand.Parameters.Add("pIdContraparte", SqlDbType.Float, 10).Value = dtrOrden("ID_CONTRAPARTE")
                        SQLCommand.Parameters.Add("pIdTrader", SqlDbType.Float, 18).Value = dtrOrden("ID_TRADER")
                        SQLCommand.Parameters.Add("pIdOrden", SqlDbType.Float, 10).Value = dtrOrden("ID_ORDEN")
                        SQLCommand.Parameters.Add("pCodSubClaseInstrumento", SqlDbType.VarChar, 10).Value = dtrOrden("COD_SUB_CLASE_INSTRUMENTO")

                        '...Resultado
                        Dim ParametroSal3 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                        ParametroSal3.Direction = Data.ParameterDirection.Output
                        SQLCommand.Parameters.Add(ParametroSal3)

                        SQLCommand.ExecuteNonQuery()

                        strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

                        If Trim(strDescError) = "OK" Then

                            If IsNothing(dtbFormaPagoCobro) = False AndAlso dtbFormaPagoCobro.Rows.Count > 0 Then

                                For Each dtrFormaPago As DataRow In dtbFormaPagoCobro.Rows

                                    SQLCommand = New SqlClient.SqlCommand("Rcp_MedioPagoCobro_Guardar", MiTransaccionSQL.Connection, MiTransaccionSQL)

                                    SQLCommand.CommandType = CommandType.StoredProcedure
                                    SQLCommand.CommandText = "Rcp_MedioPagoCobro_Guardar"
                                    SQLCommand.Parameters.Clear()

                                    SQLCommand.Parameters.Add("pOrigenMovimiento", SqlDbType.VarChar, 20).Value = "ORDEN"
                                    SQLCommand.Parameters.Add("pIdMedioPagoCobro", SqlDbType.Float, 10).Value = 0
                                    SQLCommand.Parameters.Add("pIdTipoEstado", SqlDbType.Float, 10).Value = dtrFormaPago("ID_TIPO_ESTADO")
                                    SQLCommand.Parameters.Add("pCodEstado", SqlDbType.VarChar, 3).Value = dtrFormaPago("COD_ESTADO")

                                    SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = dtrFormaPago("ID_CUENTA")
                                    SQLCommand.Parameters.Add("pIdBanco", SqlDbType.Float, 10).Value = dtrFormaPago("BANCO_ID")
                                    SQLCommand.Parameters.Add("pCodMedioPagoCobro", SqlDbType.VarChar, 10).Value = dtrFormaPago("COD_MEDIO_PAGO_COBRO")
                                    SQLCommand.Parameters.Add("pFechaMovimiento", SqlDbType.Char, 10).Value = dtrFormaPago("FECHA_MOVIMIENTO")
                                    SQLCommand.Parameters.Add("pFechaDocumento", SqlDbType.Char, 10).Value = dtrFormaPago("FECHA_DOCUMENTO")
                                    SQLCommand.Parameters.Add("pNumDocumento", SqlDbType.VarChar, 20).Value = dtrFormaPago("NUM_DOCUMENTO")
                                    SQLCommand.Parameters.Add("pRetencion", SqlDbType.Float, 10).Value = dtrFormaPago("RETENCION")
                                    SQLCommand.Parameters.Add("pMonto", SqlDbType.Float, 10).Value = dtrFormaPago("MONTO")
                                    SQLCommand.Parameters.Add("pCtaCteBancaria", SqlDbType.VarChar, 60).Value = dtrFormaPago("CTA_CTE_BANCARIA")
                                    SQLCommand.Parameters.Add("pIdOrden", SqlDbType.Float, 10).Value = dtrOrden("ID_ORDEN")
                                    SQLCommand.Parameters.Add("pIdMovCaja", SqlDbType.Float, 10).Value = DBNull.Value

                                    '...Resultado
                                    Dim ParametroSal5 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                                    ParametroSal5.Direction = Data.ParameterDirection.Output
                                    SQLCommand.Parameters.Add(ParametroSal5)

                                    SQLCommand.ExecuteNonQuery()

                                    strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

                                    If Trim(strDescError) <> "OK" Then
                                        GoTo Salir
                                    End If
                                Next
                            End If
                        Else
                            GoTo Salir
                        End If
                    Next
                End If

            End If

Salir:
            If Trim(strDescError) = "OK" Then
                MiTransaccionSQL.Commit()
                Return ("OK")
            Else
                MiTransaccionSQL.Rollback()
                Return strDescError
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error al Grabar Solicitud de Inversión/Órden " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            ModificarOrdenes = strDescError
        End Try

    End Function

    Public Function Ordenes_Ver(ByVal dblpIdOpeacion As Double, _
                                ByVal strpFechaDesde As String, _
                                ByVal strpFechaHasta As String, _
                                ByVal strpColumnas As String, _
                                ByRef strpRetorno As String, _
                                Optional ByRef strCodEstado As String = "", _
                                Optional ByVal StrListaCuentas As String = "") As DataSet

        Dim DS_Auxiliar As New DataSet
        Dim DS_Auxiliar_aux As New DataSet
        Dim larr_NombreColumna() As String = Split(strpColumnas, ",")
        Dim lInt_Col As Integer = 0
        Dim lInt_NomCol As String = ""
        Dim lstr_NombreTabla As String = ""
        Dim lstrColumna As DataColumn
        Dim lblnRemove As Boolean = True
        Dim lstrProcedimiento As String = ""
        Dim strDescError As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        lstrProcedimiento = "Rcp_Orden_Consultar2"
        lstr_NombreTabla = "ORDENES"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = lstrProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("pIdOrden", SqlDbType.Float, 10).Value = IIf(dblpIdOpeacion = 0, DBNull.Value, dblpIdOpeacion)
            SQLCommand.Parameters.Add("pFechaDesde", SqlDbType.Char, 10).Value = strpFechaDesde
            SQLCommand.Parameters.Add("pFechaHasta", SqlDbType.Char, 10).Value = strpFechaHasta
            SQLCommand.Parameters.Add("pCodEstado", SqlDbType.Char, 1).Value = IIf(strCodEstado = "", DBNull.Value, strCodEstado)
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Float, 6).Value = IIf(glngIdUsuario = 0, DBNull.Value, glngIdUsuario)
            'SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.VarChar, 20).Value = IIf(intIdCuenta = 0, DBNull.Value, intIdCuenta)
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Int).Value = glngNegocioOperacion
            SQLCommand.Parameters.Add("pListaCuentas", SqlDbType.Text).Value = StrListaCuentas


            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()
            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(DS_Auxiliar, lstr_NombreTabla)

            DS_Auxiliar_aux = DS_Auxiliar
            If strDescError.ToUpper.Trim <> "OK" Then
                gFun_ResultadoMsg("2|" & strDescError, "Buscar Cuentas")
                strpRetorno = "OK"
                DS_Auxiliar_aux = Nothing
            Else
                If strpColumnas.Trim = "" Then
                    DS_Auxiliar_aux = DS_Auxiliar
                Else
                    DS_Auxiliar_aux = DS_Auxiliar.Copy
                    For Each lstrColumna In DS_Auxiliar.Tables(lstr_NombreTabla).Columns
                        lblnRemove = True
                        For lInt_Col = 0 To larr_NombreColumna.Length - 1
                            lInt_NomCol = larr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                            If lstrColumna.ColumnName = lInt_NomCol Then
                                lblnRemove = False
                            End If
                        Next
                        If lblnRemove Then
                            DS_Auxiliar_aux.Tables(lstr_NombreTabla).Columns.Remove(lstrColumna.ColumnName)
                        End If
                    Next
                End If

                For lInt_Col = 0 To larr_NombreColumna.Length - 1
                    lInt_NomCol = larr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                    For Each lstrColumna In DS_Auxiliar_aux.Tables(lstr_NombreTabla).Columns
                        If lstrColumna.ColumnName = lInt_NomCol Then
                            lstrColumna.SetOrdinal(lInt_Col)
                            Exit For
                        End If
                    Next
                Next

                strpRetorno = "OK"
            End If
            DS_Auxiliar.Dispose()
            Return DS_Auxiliar_aux

        Catch ex As Exception
            strpRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function OrdenesConsolidadas_Ver(ByVal strAccion As String, _
                                            ByVal dblpIdOpeacion As Double, _
                                            ByVal strpFechaDesde As String, _
                                            ByVal strpFechaHasta As String, _
                                            ByVal dblpInstrumento As Double, _
                                            ByVal strpColumnas As String, _
                                            ByRef strpRetorno As String, _
                                            Optional ByRef strCodEstado As String = "", _
                                            Optional ByVal intIdCuenta As Integer = 0) As DataSet

        Dim DS_Auxiliar As New DataSet
        Dim DS_Auxiliar_Aux As New DataSet
        Dim larr_NombreColumna() As String = Split(strpColumnas, ",")
        Dim lInt_Col As Integer = 0
        Dim lInt_NomCol As String = ""
        Dim lstr_NombreTabla As String = ""
        Dim lstrColumna As DataColumn
        Dim lblnRemove As Boolean = True
        Dim lstrProcedimiento As String = ""
        Dim strDescError As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        lstrProcedimiento = "Rcp_OrdenConsolidada_Consultar"
        lstr_NombreTabla = "ORDENESCONSOLIDADAS"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = lstrProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 10).Value = strAccion.Trim
            SQLCommand.Parameters.Add("pIdOrden", SqlDbType.Float, 10).Value = IIf(dblpIdOpeacion = 0, DBNull.Value, dblpIdOpeacion)
            SQLCommand.Parameters.Add("pFechaDesde", SqlDbType.Char, 10).Value = strpFechaDesde
            SQLCommand.Parameters.Add("pFechaHasta", SqlDbType.Char, 10).Value = strpFechaHasta
            SQLCommand.Parameters.Add("pInstrumento", SqlDbType.Float, 10).Value = IIf(dblpInstrumento = 0, DBNull.Value, dblpInstrumento)
            SQLCommand.Parameters.Add("pCodEstado", SqlDbType.Char, 1).Value = IIf(strCodEstado = "", DBNull.Value, strCodEstado)
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Float, 6).Value = IIf(glngIdUsuario = 0, DBNull.Value, glngIdUsuario)
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 6).Value = IIf(intIdCuenta = 0, DBNull.Value, intIdCuenta)
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Float, 10).Value = glngNegocioOperacion
            'SQLCommand.Parameters.Add("pIdNormativo", SqlDbType.Float, 10).Value = IIf(strNormativo = "", DBNull.Value, CDbl(strNormativo))

            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()
            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(DS_Auxiliar, lstr_NombreTabla)

            DS_Auxiliar_Aux = DS_Auxiliar
            If strDescError.ToUpper.Trim <> "OK" Then
                gFun_ResultadoMsg("2|" & strDescError, "Buscar Cuentas")
                strpRetorno = "OK"
                DS_Auxiliar_Aux = Nothing
            Else
                If strpColumnas.Trim = "" Then
                    DS_Auxiliar_Aux = DS_Auxiliar
                Else
                    DS_Auxiliar_Aux = DS_Auxiliar.Copy
                    For Each lstrColumna In DS_Auxiliar.Tables(lstr_NombreTabla).Columns
                        lblnRemove = True
                        For lInt_Col = 0 To larr_NombreColumna.Length - 1
                            lInt_NomCol = larr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                            If lstrColumna.ColumnName = lInt_NomCol Then
                                lblnRemove = False
                            End If
                        Next
                        If lblnRemove Then
                            DS_Auxiliar_Aux.Tables(lstr_NombreTabla).Columns.Remove(lstrColumna.ColumnName)
                        End If
                    Next
                End If

                For lInt_Col = 0 To larr_NombreColumna.Length - 1
                    lInt_NomCol = larr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                    For Each lstrColumna In DS_Auxiliar_Aux.Tables(lstr_NombreTabla).Columns
                        If lstrColumna.ColumnName = lInt_NomCol Then
                            lstrColumna.SetOrdinal(lInt_Col)
                            Exit For
                        End If
                    Next
                Next
                strpRetorno = "OK"
            End If
            DS_Auxiliar.Dispose()
            Return DS_Auxiliar_Aux

        Catch ex As Exception
            strpRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function OrdenesPorConsolidada_Ver(ByVal dblpIdOrdenConsolidada As Double, _
                                              ByVal strpAccion As String, _
                                              ByVal strpColumnas As String, _
                                              ByRef strpRetorno As String) As DataSet

        Dim DS_Auxiliar As New DataSet
        Dim DS_Auxiliar_aux As New DataSet
        Dim larr_NombreColumna() As String = Split(strpColumnas, ",")
        Dim lInt_Col As Integer = 0
        Dim lInt_NomCol As String = ""
        Dim lstr_NombreTabla As String = ""
        Dim lstrColumna As DataColumn
        Dim lblnRemove As Boolean = True
        Dim lstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        lstrProcedimiento = "Rcp_OrdenesPorConsolidada_Buscar"
        lstr_NombreTabla = "ORDENES"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = lstrProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("pIdOrdenConsolidada", SqlDbType.Float, 10).Value = IIf(dblpIdOrdenConsolidada = 0, DBNull.Value, dblpIdOrdenConsolidada)
            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 10).Value = strpAccion

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(DS_Auxiliar, lstr_NombreTabla)

            DS_Auxiliar_aux = DS_Auxiliar

            If strpColumnas.Trim = "" Then
                DS_Auxiliar_aux = DS_Auxiliar
            Else
                DS_Auxiliar_aux = DS_Auxiliar.Copy
                For Each lstrColumna In DS_Auxiliar.Tables(lstr_NombreTabla).Columns
                    lblnRemove = True
                    For lInt_Col = 0 To larr_NombreColumna.Length - 1
                        lInt_NomCol = larr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                        If lstrColumna.ColumnName = lInt_NomCol Then
                            lblnRemove = False
                        End If
                    Next
                    If lblnRemove Then
                        DS_Auxiliar_aux.Tables(lstr_NombreTabla).Columns.Remove(lstrColumna.ColumnName)
                    End If
                Next
            End If

            For lInt_Col = 0 To larr_NombreColumna.Length - 1
                lInt_NomCol = larr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                For Each lstrColumna In DS_Auxiliar_aux.Tables(lstr_NombreTabla).Columns
                    If lstrColumna.ColumnName = lInt_NomCol Then
                        lstrColumna.SetOrdinal(lInt_Col)
                        Exit For
                    End If
                Next
            Next

            strpRetorno = "OK"
            DS_Auxiliar.Dispose()
            Return DS_Auxiliar_aux

        Catch ex As Exception
            strpRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function GuardarOrdenesConsolidadas(ByVal dtbSolicitudInversion As DataTable, _
                                               ByVal dtbOrdenes As DataTable, _
                                               ByVal dtbRelacion As DataTable) As String
        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction
        Dim llngIdSolInversion As Long = 0
        Dim llngIdCuenta As Long
        Dim lstrTipoOper As String


        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try

            SQLCommand = New SqlClient.SqlCommand("Rcp_SolicitudInversion_Guardar", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_SolicitudInversion_Guardar"
            SQLCommand.Parameters.Clear()

            Dim dtrSolInversion As DataRow = dtbSolicitudInversion.Rows(0)

            llngIdCuenta = dtrSolInversion("ID_CUENTA")
            lstrTipoOper = dtrSolInversion("COD_TIPO_OPERACION")

            SQLCommand.Parameters.Add("pIdSistemaOrigen", SqlDbType.Float, 10).Value = dtrSolInversion("ID_SISTEMA_ORIGEN")
            SQLCommand.Parameters.Add("pCodMoneda", SqlDbType.VarChar, 3).Value = dtrSolInversion("COD_MONEDA")
            SQLCommand.Parameters.Add("pCodTipoOperacion", SqlDbType.VarChar, 10).Value = lstrTipoOper
            SQLCommand.Parameters.Add("pIdRepresentante", SqlDbType.Float, 10).Value = dtrSolInversion("ID_REPRESENTANTE")
            SQLCommand.Parameters.Add("pIdSistemaConfirmacion", SqlDbType.Float, 10).Value = dtrSolInversion("ID_SISTEMA_CONFIRMACION")
            SQLCommand.Parameters.Add("pIdTipoEstado", SqlDbType.Float, 10).Value = dtrSolInversion("ID_TIPO_ESTADO")
            SQLCommand.Parameters.Add("pCodEstado", SqlDbType.VarChar, 3).Value = dtrSolInversion("COD_ESTADO")
            SQLCommand.Parameters.Add("pIdTrader", SqlDbType.Float, 10).Value = dtrSolInversion("ID_TRADER")
            'SQLCommand.Parameters.Add("pCodSubClaseInstrumento", SqlDbType.VarChar, 10).Value = dtrSolInversion("COD_SUB_CLASE_INSTRUMENTO")
            SQLCommand.Parameters.Add("pIdContraparte", SqlDbType.Float, 10).Value = dtrSolInversion("ID_CONTRAPARTE")
            SQLCommand.Parameters.Add("pFlgTipoMovimiento", SqlDbType.Char, 1).Value = dtrSolInversion("FLG_TIPO_MOVIMIENTO")
            SQLCommand.Parameters.Add("pFechaOperacion", SqlDbType.Char, 10).Value = dtrSolInversion("FECHA_OPERACION")
            SQLCommand.Parameters.Add("pFechaVigencia", SqlDbType.Char, 10).Value = dtrSolInversion("FECHA_VIGENCIA")
            SQLCommand.Parameters.Add("pFechaLiquidacion", SqlDbType.Char, 10).Value = dtrSolInversion("FECHA_LIQUIDACION")
            SQLCommand.Parameters.Add("pDscOperacion", SqlDbType.VarChar, 200).Value = dtrSolInversion("DSC_OPERACION")
            SQLCommand.Parameters.Add("pFechaConfirmacion", SqlDbType.Char, 10).Value = dtrSolInversion("FECHA_CONFIRMACION")
            SQLCommand.Parameters.Add("pPorcComision", SqlDbType.Float, 20).Value = dtrSolInversion("PORC_COMISION")
            SQLCommand.Parameters.Add("pComision", SqlDbType.Float, 20).Value = dtrSolInversion("COMISION")
            SQLCommand.Parameters.Add("pDerechos", SqlDbType.Float, 20).Value = dtrSolInversion("DERECHOS")
            SQLCommand.Parameters.Add("pGastos", SqlDbType.Float, 20).Value = dtrSolInversion("GASTOS")
            SQLCommand.Parameters.Add("pIva", SqlDbType.Float, 20).Value = dtrSolInversion("IVA")
            SQLCommand.Parameters.Add("pMontoOperacion", SqlDbType.Float, 20).Value = dtrSolInversion("MONTO_OPERACION")
            SQLCommand.Parameters.Add("pFlgLimitePrecio", SqlDbType.VarChar, 1).Value = dtrSolInversion("FLG_LIMITE_PRECIO")
            SQLCommand.Parameters.Add("pFlgTipoOrigen", SqlDbType.Char, 1).Value = dtrSolInversion("FLG_TIPO_ORIGEN")
            SQLCommand.Parameters.Add("pConfirmaComision", SqlDbType.Float, 20).Value = dtrSolInversion("CONFIRMA_COMISION")
            SQLCommand.Parameters.Add("pConfirmaDerechos", SqlDbType.Float, 20).Value = dtrSolInversion("CONFIRMA_DERECHOS")
            SQLCommand.Parameters.Add("pConfirmaGastos", SqlDbType.Float, 20).Value = dtrSolInversion("CONFIRMA_GASTOS")
            SQLCommand.Parameters.Add("pConfirmaIva", SqlDbType.Float, 20).Value = dtrSolInversion("CONFIRMA_IVA")
            SQLCommand.Parameters.Add("pConfirmaMontoOperacion", SqlDbType.Float, 20).Value = dtrSolInversion("CONFIRMA_MONTO_OPERACION")
            SQLCommand.Parameters.Add("pInterest", SqlDbType.Float, 18).Value = dtrSolInversion("INTEREST")
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = llngIdCuenta

            '...Id Solicitud de Inversion
            Dim ParametroSal1 As New SqlClient.SqlParameter("pIdSolInversion", SqlDbType.Float, 10)
            ParametroSal1.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal1)

            '...Resultado
            Dim ParametroSal As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then

                llngIdSolInversion = SQLCommand.Parameters("pIdSolInversion").Value

                If llngIdSolInversion > 0 Then
                    If dtbOrdenes.Rows.Count > 0 Then
                        For Each dtrOrden As DataRow In dtbOrdenes.Rows
                            Dim NumOrden As Long = 0

                            SQLCommand = New SqlClient.SqlCommand("Hsb_Orden_Guardar", MiTransaccionSQL.Connection, MiTransaccionSQL)

                            SQLCommand.CommandType = CommandType.StoredProcedure
                            SQLCommand.CommandText = "Hsb_Orden_Guardar"
                            SQLCommand.Parameters.Clear()

                            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = llngIdCuenta
                            SQLCommand.Parameters.Add("pCodTipoOperacion", SqlDbType.VarChar, 10).Value = lstrTipoOper
                            SQLCommand.Parameters.Add("pIdTipoEstado", SqlDbType.Float, 10).Value = dtrOrden("ID_TIPO_ESTADO")
                            SQLCommand.Parameters.Add("pCodEstado", SqlDbType.VarChar, 3).Value = dtrOrden("COD_ESTADO")
                            SQLCommand.Parameters.Add("pIdInstrumento", SqlDbType.Float, 10).Value = dtrOrden("ID_INSTRUMENTO")
                            SQLCommand.Parameters.Add("pCodMoneda", SqlDbType.VarChar, 3).Value = dtrOrden("COD_MONEDA")
                            SQLCommand.Parameters.Add("pIdSolInversion", SqlDbType.Float, 10).Value = llngIdSolInversion
                            SQLCommand.Parameters.Add("pCodMonedaPago", SqlDbType.VarChar, 3).Value = dtrOrden("COD_MONEDA_PAGO")
                            SQLCommand.Parameters.Add("pCantidad", SqlDbType.Float, 20).Value = dtrOrden("CANTIDAD")
                            SQLCommand.Parameters.Add("pPrecio", SqlDbType.Float, 20).Value = dtrOrden("PRECIO")
                            SQLCommand.Parameters.Add("pPrecioGestion", SqlDbType.Float, 24).Value = dtrOrden("PRECIO_GESTION")
                            SQLCommand.Parameters.Add("pPrecioHistoricoCompra", SqlDbType.Float, 20.1).Value = dtrOrden("PRECIO_HISTORICO_COMPRA")
                            SQLCommand.Parameters.Add("pTasa", SqlDbType.Float, 20.1).Value = dtrOrden("TASA")
                            SQLCommand.Parameters.Add("pPlazo", SqlDbType.Float, 6).Value = dtrOrden("PLAZO")
                            SQLCommand.Parameters.Add("pBase", SqlDbType.Float, 18).Value = dtrOrden("BASE")
                            SQLCommand.Parameters.Add("pValorDivisa", SqlDbType.Float, 16).Value = dtrOrden("VALOR_DIVISA")
                            SQLCommand.Parameters.Add("pComision", SqlDbType.Float, 20).Value = dtrOrden("COMISION")
                            SQLCommand.Parameters.Add("pInteres", SqlDbType.Float, 20).Value = dtrOrden("INTERES")
                            SQLCommand.Parameters.Add("pUtilidad", SqlDbType.Float, 20).Value = dtrOrden("UTILIDAD")
                            SQLCommand.Parameters.Add("pPorcComision", SqlDbType.Float, 20).Value = dtrOrden("PORC_COMISION")
                            SQLCommand.Parameters.Add("pMonto", SqlDbType.Float, 20.1).Value = dtrOrden("MONTO")
                            SQLCommand.Parameters.Add("pMontoPago", SqlDbType.Float, 10).Value = dtrOrden("MONTO_PAGO")
                            SQLCommand.Parameters.Add("pMontoBruto", SqlDbType.Float, 20).Value = dtrOrden("MONTO_BRUTO")
                            SQLCommand.Parameters.Add("pMontoTotal", SqlDbType.Float, 20).Value = dtrOrden("MONTO_TOTAL")
                            SQLCommand.Parameters.Add("pFlgMontoReferenciado", SqlDbType.VarChar, 1).Value = dtrOrden("FLG_MONTO_REFERENCIADO")
                            SQLCommand.Parameters.Add("pFlgTipoDeposito", SqlDbType.VarChar, 1).Value = dtrOrden("FLG_TIPO_DEPOSITO")
                            SQLCommand.Parameters.Add("pFlgVendeTodo", SqlDbType.VarChar, 1).Value = dtrOrden("FLG_VENDE_TODO")
                            SQLCommand.Parameters.Add("pFlgLimitePrecio", SqlDbType.VarChar, 1).Value = dtrOrden("FLG_LIMITE_PRECIO")
                            SQLCommand.Parameters.Add("pFlgTipoMovimiento", SqlDbType.Char, 1).Value = dtrOrden("FLG_TIPO_MOVIMIENTO")
                            SQLCommand.Parameters.Add("pFechaValuta", SqlDbType.Char, 10).Value = dtrOrden("FECHA_VALUTA")
                            SQLCommand.Parameters.Add("pFechaLiquidacion", SqlDbType.Char, 10).Value = dtrOrden("FECHA_LIQUIDACION")
                            SQLCommand.Parameters.Add("pFechaVencimiento", SqlDbType.Char, 10).Value = dtrOrden("FECHA_VENCIMIENTO")
                            SQLCommand.Parameters.Add("pFechaOperacion", SqlDbType.Char, 10).Value = dtrOrden("FECHA_OPERACION")
                            SQLCommand.Parameters.Add("pFechaMovimiento", SqlDbType.Char, 10).Value = dtrOrden("FECHA_MOVIMIENTO")
                            SQLCommand.Parameters.Add("pFechaConfirmacion", SqlDbType.Char, 10).Value = dtrOrden("FECHA_CONFIRMACION")
                            SQLCommand.Parameters.Add("pFechaCierre", SqlDbType.Char, 10).Value = dtrOrden("FECHA_CIERRE")
                            SQLCommand.Parameters.Add("pEsOrdenOmnibus", SqlDbType.Char, 1).Value = dtrOrden("ES_ORDEN_OMNIBUS")
                            SQLCommand.Parameters.Add("pIdNormativo", SqlDbType.Float, 10).Value = dtrOrden("ID_NORMATIVO")
                            SQLCommand.Parameters.Add("pIdPortafolio", SqlDbType.Float, 10).Value = dtrOrden("ID_PORTAFOLIO")
                            SQLCommand.Parameters.Add("pNumDoctoPago", SqlDbType.Float, 10).Value = 0
                            '+ Rabreu Se agrega parametro Id cartera para saber cuando esta comprometida la Orden
                            SQLCommand.Parameters.Add("pIdCartera", SqlDbType.Float, 18).Value = DBNull.Value
                            SQLCommand.Parameters.Add("pIdContraparte", SqlDbType.Float, 10).Value = IIf(dtrOrden("ID_CONTRAPARTE") = 0, DBNull.Value, dtrOrden("ID_CONTRAPARTE"))
                            SQLCommand.Parameters.Add("pIdTrader", SqlDbType.Float, 18).Value = dtrOrden("ID_TRADER")
                            SQLCommand.Parameters.Add("pCodSubClaseInstrumento", SqlDbType.VarChar, 10).Value = dtrOrden("COD_SUB_CLASE_INSTRUMENTO")

                            '...Id Orden
                            Dim ParametroOrden As New SqlClient.SqlParameter("pIdOrden", SqlDbType.Float, 10)
                            ParametroOrden.Direction = Data.ParameterDirection.Output
                            SQLCommand.Parameters.Add(ParametroOrden)

                            '...Resultado
                            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                            ParametroSal2.Direction = Data.ParameterDirection.Output
                            SQLCommand.Parameters.Add(ParametroSal2)

                            SQLCommand.ExecuteNonQuery()

                            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

                            If Trim(strDescError) <> "OK" Then
                                GoTo Salir
                            Else
                                NumOrden = SQLCommand.Parameters("pIdOrden").Value
                            End If

                            For Each dtrRelacion As DataRow In dtbRelacion.Rows
                                SQLCommand = New SqlClient.SqlCommand("Rcp_OrdenRelacionConso_Guardar", MiTransaccionSQL.Connection, MiTransaccionSQL)

                                SQLCommand.CommandType = CommandType.StoredProcedure
                                SQLCommand.CommandText = "Rcp_OrdenRelacionConso_Guardar"
                                SQLCommand.Parameters.Clear()

                                SQLCommand.Parameters.Add("pIdOrdenOmnibus", SqlDbType.Float, 10).Value = NumOrden
                                SQLCommand.Parameters.Add("pIdOrdenDetalle", SqlDbType.Float, 10).Value = dtrRelacion("ID_ORDEN_DETALLE")

                                '...Resultado
                                Dim ParametroSal3 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                                ParametroSal3.Direction = Data.ParameterDirection.Output
                                SQLCommand.Parameters.Add(ParametroSal3)

                                SQLCommand.ExecuteNonQuery()

                                strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

                                If Trim(strDescError) <> "OK" Then
                                    GoTo Salir
                                End If
                            Next

                        Next
                    End If
                Else
                    strDescError = "No se pudo crear Solicitud de Inversión."
                End If
            End If

Salir:
            If Trim(strDescError) = "OK" Then
                MiTransaccionSQL.Commit()
                Return ("OK")
            Else
                MiTransaccionSQL.Rollback()
                Return strDescError
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error al Grabar Solicitud de Inversión/Órden " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            GuardarOrdenesConsolidadas = strDescError
        End Try
    End Function

    Public Function ConfirmarOrdenes(ByVal dblNumeroDocumento As Double, _
                                     ByVal strIdOrdenes As String, _
                                     ByVal strCodigoEstado As String) As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        Dim intIndiceFor As Integer = 0
        Dim arr_Ordenes() As String

        arr_Ordenes = strIdOrdenes.Split(";")

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try

            For intIndiceFor = 0 To arr_Ordenes.Length - 1
                If arr_Ordenes(intIndiceFor).Trim <> "" Then

                    SQLCommand = New SqlClient.SqlCommand("Rcp_Orden_Confirmar", MiTransaccionSQL.Connection, MiTransaccionSQL)

                    SQLCommand.CommandType = CommandType.StoredProcedure
                    SQLCommand.CommandText = "Rcp_Orden_Confirmar"
                    SQLCommand.Parameters.Clear()

                    SQLCommand.Parameters.Add("pIdOrden", SqlDbType.Float, 10).Value = CDbl(arr_Ordenes(intIndiceFor).Trim)
                    SQLCommand.Parameters.Add("pNumeroDoctoPago", SqlDbType.Float, 10).Value = dblNumeroDocumento
                    SQLCommand.Parameters.Add("pCodigoEstado", SqlDbType.VarChar, 3).Value = strCodigoEstado.Trim

                    '...Resultado
                    Dim ParametroSal As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                    ParametroSal.Direction = Data.ParameterDirection.Output
                    SQLCommand.Parameters.Add(ParametroSal)
                    SQLCommand.ExecuteNonQuery()

                    strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

                    If strDescError.ToUpper.Trim <> "OK" Then
                        GoTo Salir
                    End If
                End If
            Next

Salir:
            If Trim(strDescError) = "OK" Then
                MiTransaccionSQL.Commit()
                Return ("OK")
            Else
                MiTransaccionSQL.Rollback()
                Return strDescError
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error al Confirmar Órden." & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            ConfirmarOrdenes = strDescError
        End Try
    End Function

    Public Function SolicitudInversion_Buscar(ByVal dblIdSolInversion As Double, _
                                ByVal strColumnas As String, _
                                ByRef strRetorno As String) As DataSet

        Dim DS_Auxiliar As New DataSet
        Dim DS_Auxiliar_aux As New DataSet
        Dim larr_NombreColumna() As String = Split(strColumnas, ",")
        Dim lInt_Col As Integer = 0
        Dim lInt_NomCol As String = ""
        Dim lstr_NombreTabla As String = ""
        Dim lstrColumna As DataColumn
        Dim lblnRemove As Boolean = True
        Dim lstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        lstrProcedimiento = "Rcp_SolicitudInversion_Buscar"
        lstr_NombreTabla = "OPERACIONES"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = lstrProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("pIdSolInversion", SqlDbType.Float, 10).Value = dblIdSolInversion

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(DS_Auxiliar, lstr_NombreTabla)

            DS_Auxiliar_aux = DS_Auxiliar

            If strColumnas.Trim = "" Then
                DS_Auxiliar_aux = DS_Auxiliar
            Else
                DS_Auxiliar_aux = DS_Auxiliar.Copy
                For Each lstrColumna In DS_Auxiliar.Tables(lstr_NombreTabla).Columns
                    lblnRemove = True
                    For lInt_Col = 0 To larr_NombreColumna.Length - 1
                        lInt_NomCol = larr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                        If lstrColumna.ColumnName = lInt_NomCol Then
                            lblnRemove = False
                        End If
                    Next
                    If lblnRemove Then
                        DS_Auxiliar_aux.Tables(lstr_NombreTabla).Columns.Remove(lstrColumna.ColumnName)
                    End If
                Next
            End If

            For lInt_Col = 0 To larr_NombreColumna.Length - 1
                lInt_NomCol = larr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                For Each lstrColumna In DS_Auxiliar_aux.Tables(lstr_NombreTabla).Columns
                    If lstrColumna.ColumnName = lInt_NomCol Then
                        lstrColumna.SetOrdinal(lInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            DS_Auxiliar.Dispose()
            Return DS_Auxiliar_aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function Ordenes_Buscar(ByVal dblIdSolInversion As Double, _
                                ByVal strColumnas As String, _
                                ByRef strRetorno As String) As DataSet

        Dim DS_Auxiliar As New DataSet
        Dim DS_Auxiliar_aux As New DataSet
        Dim larr_NombreColumna() As String = Split(strColumnas, ",")
        Dim lInt_Col As Integer = 0
        Dim lInt_NomCol As String = ""
        Dim lstr_NombreTabla As String = ""
        Dim lstrColumna As DataColumn
        Dim lblnRemove As Boolean = True
        Dim lstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        lstrProcedimiento = "Rcp_Orden_Buscar"
        lstr_NombreTabla = "OPERACIONES"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = lstrProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("pIdsolInversion", SqlDbType.Float, 10).Value = dblIdSolInversion

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(DS_Auxiliar, lstr_NombreTabla)

            DS_Auxiliar_aux = DS_Auxiliar

            If strColumnas.Trim = "" Then
                DS_Auxiliar_aux = DS_Auxiliar
            Else
                DS_Auxiliar_aux = DS_Auxiliar.Copy
                For Each lstrColumna In DS_Auxiliar.Tables(lstr_NombreTabla).Columns
                    lblnRemove = True
                    For lInt_Col = 0 To larr_NombreColumna.Length - 1
                        lInt_NomCol = larr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                        If lstrColumna.ColumnName = lInt_NomCol Then
                            lblnRemove = False
                        End If
                    Next
                    If lblnRemove Then
                        DS_Auxiliar_aux.Tables(lstr_NombreTabla).Columns.Remove(lstrColumna.ColumnName)
                    End If
                Next
            End If

            For lInt_Col = 0 To larr_NombreColumna.Length - 1
                lInt_NomCol = larr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                For Each lstrColumna In DS_Auxiliar_aux.Tables(lstr_NombreTabla).Columns
                    If lstrColumna.ColumnName = lInt_NomCol Then
                        lstrColumna.SetOrdinal(lInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            DS_Auxiliar.Dispose()
            Return DS_Auxiliar_aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function AnularOrden(ByVal dblNumeroDocumento As Double) As String
        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_Orden_Anular", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Orden_Anular"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdOrden", SqlDbType.Float, 10).Value = dblNumeroDocumento

            '...Resultado
            Dim ParametroSal As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal)
            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

        Catch Ex As Exception
            strDescError = "|1|Error al Anular la orden." & vbCr & "[" & Ex.Message & "]|"
        Finally
            If strDescError = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                If gFun_ResultadoMsg(strDescError, "", True) Then
                    MiTransaccionSQL.Commit()
                Else
                    MiTransaccionSQL.Rollback()
                End If
            End If
            SQLConnect.Cerrar()
            AnularOrden = strDescError
        End Try
    End Function

    Public Function ObtenerCantidadOrden_OrdenConsolidada(ByVal pIdOrdenConsolidada As Double) As Long
        Dim strDescError As String = ""
        Dim llngCantidad As Long
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion

        '...Abre la conexion
        SQLConnect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_Orden_Cantidad_AsoOrdenCon", SQLConnect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Orden_Cantidad_AsoOrdenCon"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdOrdenConsolidada", SqlDbType.Float, 10).Value = pIdOrdenConsolidada

            '...pCantidad
            Dim ParametroSal As New SqlClient.SqlParameter("pCantidad", SqlDbType.Float, 10)
            ParametroSal.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal)

            '...Resultado
            Dim ParametroSal1 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal1.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal1)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError = "OK" Then
                llngCantidad = SQLCommand.Parameters("pCantidad").Value
            End If

        Catch Ex As Exception
            strDescError = "|1|Error al obtener la cantidad de ordenes asociadas a una orden consolidada." & vbCr & "[" & Ex.Message & "]|"
        Finally
            If strDescError <> "OK" Then
                If Not gFun_ResultadoMsg(strDescError, "ObtenerCantidadOrden_OrdenConsolidada") Then
                    llngCantidad = 0
                End If
            End If
            SQLConnect.Cerrar()
            ObtenerCantidadOrden_OrdenConsolidada = llngCantidad
        End Try
    End Function

    Public Function AnularOrdenConsolidada(ByVal dblNumeroDocumento As Double) As String
        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_Orden_Consolidada_Anular", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Orden_Consolidada_Anular"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdOrden", SqlDbType.Float, 10).Value = dblNumeroDocumento

            '...Resultado
            Dim ParametroSal As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal)
            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

        Catch Ex As Exception
            strDescError = "|1|Error al Anular la orden consolidada." & vbCr & "[" & Ex.Message & "]|"
        Finally
            If strDescError = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                If gFun_ResultadoMsg(strDescError, "", True) Then
                    MiTransaccionSQL.Commit()
                Else
                    MiTransaccionSQL.Rollback()
                End If
            End If
            SQLConnect.Cerrar()
            AnularOrdenConsolidada = strDescError
        End Try
    End Function

    Public Function AnularOperacion(ByVal dblIdOper As Double, _
                                    ByVal dblIdOperDet As Double) As String
        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_Operacion_Anular", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Operacion_Anular"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdOper", SqlDbType.Float, 10).Value = IIf(dblIdOper = 0, DBNull.Value, dblIdOper)
            SQLCommand.Parameters.Add("pIdOperDet", SqlDbType.Float, 10).Value = IIf(dblIdOperDet = 0, DBNull.Value, dblIdOperDet)

            '...Resultado
            Dim ParametroSal As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal)
            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

        Catch Ex As Exception
            strDescError = "|1|Error al Anular la operación." & vbCr & "[" & Ex.Message & "]|"
        Finally
            If strDescError = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                If gFun_ResultadoMsg(strDescError, "", True) Then
                    MiTransaccionSQL.Commit()
                Else
                    MiTransaccionSQL.Rollback()
                End If
            End If
            SQLConnect.Cerrar()
            AnularOperacion = strDescError
        End Try
    End Function

    Public Function AnularOperacionAll(ByVal DS As DataSet) As String
        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            Dim dr As DataRow
            Dim i As Integer

            For i = 0 To DS.Tables(0).Rows.Count - 1
                dr = DS.Tables(0).Rows(i)

                SQLCommand = New SqlClient.SqlCommand("Rcp_Operacion_Anular", MiTransaccionSQL.Connection, MiTransaccionSQL)

                SQLCommand.CommandType = CommandType.StoredProcedure
                SQLCommand.CommandText = "Rcp_Operacion_Anular"
                SQLCommand.Parameters.Clear()

                SQLCommand.Parameters.Add("pIdOper", SqlDbType.Float, 10).Value = dr("ID_OPERACION")
                'SQLCommand.Parameters.Add("pIdOperDet", SqlDbType.Float, 10).Value = dr("ID_OPERACION_DETALLE")

                '...Resultado
                Dim ParametroSal As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                ParametroSal.Direction = Data.ParameterDirection.Output
                SQLCommand.Parameters.Add(ParametroSal)
                SQLCommand.ExecuteNonQuery()

            Next
            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

        Catch Ex As Exception
            strDescError = "|1|Error al Anular la operación." & vbCr & "[" & Ex.Message & "]|"
        Finally
            If strDescError.Substring(0, 2) = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                If gFun_ResultadoMsg(strDescError, "", True) Then
                    MiTransaccionSQL.Commit()
                Else
                    MiTransaccionSQL.Rollback()
                End If
            End If
            SQLConnect.Cerrar()
            AnularOperacionAll = strDescError
        End Try
    End Function

    Public Function AnularOperacionUnaAUna(ByVal dblIdOper As Double) As String
        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_Operacion_Anular", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Operacion_Anular"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdOper", SqlDbType.Float, 10).Value = dblIdOper
            'SQLCommand.Parameters.Add("pIdOperDet", SqlDbType.Float, 10).Value = dr("ID_OPERACION_DETALLE")

            '...Resultado
            Dim ParametroSal As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal)
            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

        Catch Ex As Exception
            strDescError = "|1|Error al Anular la operación." & vbCr & "[" & Ex.Message & "]|"
        Finally
            If strDescError.Substring(0, 2) = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                If gFun_ResultadoMsg(strDescError, "", True) Then
                    MiTransaccionSQL.Commit()
                Else
                    MiTransaccionSQL.Rollback()
                End If
            End If
            SQLConnect.Cerrar()
            AnularOperacionUnaAUna = strDescError
        End Try
    End Function

    Public Function Ordenes_Libro(ByVal strTipoOrden As String, _
                                    ByVal dblIdCliente As Double, _
                                    ByVal strCodClase As String, _
                                    ByVal strCodSubClase As String, _
                                    ByVal dblIdInstrumento As Double, _
                                    ByVal strFechaDesde As String, _
                                    ByVal strFechaHasta As String, _
                                    ByVal strEstado As String, _
                                    ByRef strRetorno As String) As DataSet

        Dim lDS_Ordenes As New DataSet
        Dim lDS_Ordenes_Aux As New DataSet
        Dim lstr_NombreTabla As String = ""
        Dim lstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        lstrProcedimiento = "Rcp_Orden_LibroOrdenes"
        lstr_NombreTabla = "LIBRO_ORDENES"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = lstrProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("pCodTipoOrden", SqlDbType.VarChar, 1).Value = strTipoOrden
            SQLCommand.Parameters.Add("pIdCliente", SqlDbType.Float, 10).Value = IIf(dblIdCliente = 0, DBNull.Value, dblIdCliente)
            SQLCommand.Parameters.Add("pCodClaseInstrumento", SqlDbType.VarChar, 15).Value = IIf(strCodClase = "", DBNull.Value, strCodClase)
            SQLCommand.Parameters.Add("pCodSubClaseInstrumento", SqlDbType.VarChar, 15).Value = IIf(strCodSubClase = "", DBNull.Value, strCodSubClase)
            SQLCommand.Parameters.Add("pIdInstrumento", SqlDbType.Float, 10).Value = IIf(dblIdInstrumento = 0, DBNull.Value, dblIdInstrumento)
            SQLCommand.Parameters.Add("pFechaDesde", SqlDbType.Char, 10).Value = strFechaDesde
            SQLCommand.Parameters.Add("pFechaHasta", SqlDbType.Char, 10).Value = strFechaHasta
            SQLCommand.Parameters.Add("pEstado", SqlDbType.Char, 1).Value = IIf(strEstado = "", DBNull.Value, strEstado)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(lDS_Ordenes, lstr_NombreTabla)

            lDS_Ordenes_Aux = lDS_Ordenes

            strRetorno = "OK"
            lDS_Ordenes.Dispose()
            Return lDS_Ordenes_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function Ordenes_Exportacion(ByVal strIdOrden As String, _
                                    ByVal strReprocesa As String, _
                                    ByVal strArchivo As String) As String
        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_Rel_Orden_Exportacion", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Rel_Orden_Exportacion"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdOrden", SqlDbType.Float, 10).Value = CDbl("0" & strIdOrden)
            SQLCommand.Parameters.Add("pReprocesa", SqlDbType.Char, 1).Value = IIf(strReprocesa = "", "N", strReprocesa)
            SQLCommand.Parameters.Add("pArchivo", SqlDbType.VarChar, 50).Value = strArchivo

            '...Resultado
            Dim ParametroSal As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal)
            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

        Catch Ex As Exception
            strDescError = "|1|Error al Anular la operación." & vbCr & "[" & Ex.Message & "]|"
        Finally
            If strDescError = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                If gFun_ResultadoMsg(strDescError, "", True) Then
                    MiTransaccionSQL.Commit()
                Else
                    MiTransaccionSQL.Rollback()
                End If
            End If
            SQLConnect.Cerrar()
            Ordenes_Exportacion = strDescError
        End Try
    End Function

    Public Function GuardarOrdenesCompleta(ByVal dtbSolicitudInversion As DataTable, _
                                           ByVal dtbOrdenes As DataTable, _
                                           Optional ByVal dtbFormaPagoCobro As DataTable = Nothing, _
                                           Optional ByVal dtbComision As DataTable = Nothing, _
                                           Optional ByVal blnMultiDetalle As Boolean = False, _
                                           Optional ByRef intSolInversion As Integer = 0, _
                                           Optional ByRef intOrden As Integer = 0) As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction
        Dim llngIdDolInversion As Long = 0
        Dim llngIdOrden As Long = 0
        Dim llngIdMovCaja As Long = 0

        Dim lstrDetalle1 As String = ""
        Dim lstrDetalle2 As String = ""
        Dim lstrDetalle3 As String = ""
        Dim lstrComisiones As String = ""
        Dim lstrMediosCobroPago As String = ""
        Dim llngIdOperacion As Integer = 0
        Dim lstrColumnasDetalle As String = "ID_TIPO_ESTADO © " & _
                                            "COD_ESTADO © " & _
                                            "ID_INSTRUMENTO © " & _
                                            "COD_MONEDA © " & _
                                            "COD_MONEDA_PAGO © " & _
                                            "CANTIDAD © " & _
                                            "PRECIO © " & _
                                            "PRECIO_GESTION © " & _
                                            "PRECIO_HISTORICO_COMPRA © " & _
                                            "TASA © " & _
                                            "PLAZO © " & _
                                            "BASE © " & _
                                            "VALOR_DIVISA © " & _
                                            "INTERES © " & _
                                            "UTILIDAD © " & _
                                            "MONTO © " & _
                                            "MONTO_PAGO © " & _
                                            "MONTO_BRUTO © " & _
                                            "MONTO_TOTAL © " & _
                                            "FLG_MONTO_REFERENCIADO © " & _
                                            "FLG_TIPO_DEPOSITO © " & _
                                            "FLG_VENDE_TODO © " & _
                                            "FLG_LIMITE_PRECIO © " & _
                                            "FECHA_VALUTA © " & _
                                            "FECHA_LIQUIDACION © " & _
                                            "FECHA_VENCIMIENTO © " & _
                                            "FECHA_OPERACION © " & _
                                            "FECHA_MOVIMIENTO © " & _
                                            "FECHA_CONFIRMACION © " & _
                                            "FECHA_CIERRE © " & _
                                            "ES_ORDEN_OMNIBUS © " & _
                                            "ID_NORMATIVO © " & _
                                            "ID_PORTAFOLIO © " & _
                                            "NUM_DOCTOPAGO © " & _
                                            "ID_CARTERA © " & _
                                            "SALDO_POR_ASIGNAR © " & _
                                            "ID_CONTRAPARTE © " & _
                                            "ID_TRADER © " & _
                                            "COD_SUB_CLASE_INSTRUMENTO © " & _
                                            "COD_TIPO_REAJUSTE © " & _
                                            "FACTOR © " & _
                                            "ID_CUSTODIO © " & _
                                            "ID_EMISOR © " & _
                                            "ID_INSTRUMENTO_DESTINO © " & _
                                            "TIPO_CUSTODIA © " & _
                                            "ING_CANTIDAD_INICIAL © " & _
                                            "ING_PORC_VALOR_PAR "


        Dim lstrColumnasComision As String = "PORCENTAJE © " & _
                                             "MONTO © " & _
                                             "IMPUESTO © " & _
                                             "ID_COMISION"


        Dim lstrColumnasFormaPago As String = "ID_TIPO_ESTADO © " & _
                                              "COD_ESTADO © " & _
                                              "BANCO_ID © " & _
                                              "COD_MEDIO_PAGO_COBRO © " & _
                                              "FECHA_MOVIMIENTO © " & _
                                              "FECHA_DOCUMENTO © " & _
                                              "NUM_DOCUMENTO © " & _
                                              "RETENCION © " & _
                                              "MONTO © " & _
                                              "CTA_CTE_BANCARIA"


        Dim lstrResultadoCadena As String = ""

        ' GENERA CADENA(S) DE DETALLE DE OPERACIONES
        lstrResultadoCadena = GeneraCadena(dtbOrdenes, lstrColumnasDetalle, "©", lstrDetalle1, lstrDetalle2, lstrDetalle3)
        If lstrResultadoCadena <> "OK" Then
            Return lstrResultadoCadena
        End If

        lstrResultadoCadena = ""
        'GENERA CADENA DE COMISIONES
        If IsNothing(dtbComision) OrElse dtbComision.Rows.Count = 0 Then
            lstrComisiones = ""
        Else
            lstrResultadoCadena = GeneraCadena(dtbComision, lstrColumnasComision, "©", lstrComisiones)
            If lstrResultadoCadena <> "OK" Then
                Return lstrResultadoCadena & vbCrLf & "En Comisiones"
            End If
        End If

        'GENERA CADENA DE MEDIOS DE COBRO/PAGOS
        lstrResultadoCadena = ""
        If IsNothing(dtbFormaPagoCobro) OrElse dtbFormaPagoCobro.Rows.Count = 0 Then
            lstrMediosCobroPago = ""
        Else
            lstrResultadoCadena = GeneraCadena(dtbFormaPagoCobro, lstrColumnasFormaPago, "©", lstrMediosCobroPago)
            If lstrResultadoCadena <> "OK" Then
                Return lstrResultadoCadena & vbCrLf & "En Medios de Cobro/Pago"
            End If
        End If

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try

            SQLCommand = New SqlClient.SqlCommand("Rcp_Orden_Guardar_Completa", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Orden_Guardar_Completa"
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            Dim dtrSolInversion As DataRow = dtbSolicitudInversion.Rows(0)
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Int).Value = glngNegocioOperacion
            SQLCommand.Parameters.Add("pCodEstado", SqlDbType.VarChar, 3).Value = dtrSolInversion("COD_ESTADO")
            SQLCommand.Parameters.Add("pCodMoneda", SqlDbType.VarChar, 3).Value = dtrSolInversion("COD_MONEDA")
            SQLCommand.Parameters.Add("pCodTipoOperacion", SqlDbType.VarChar, 10).Value = dtrSolInversion("COD_TIPO_OPERACION")
            SQLCommand.Parameters.Add("pDscOperacion", SqlDbType.VarChar, 200).Value = dtrSolInversion("DSC_OPERACION")
            SQLCommand.Parameters.Add("pFechaLiquidacion", SqlDbType.Char, 10).Value = IIf(dtrSolInversion("FECHA_LIQUIDACION") Is DBNull.Value, dtrSolInversion("FECHA_OPERACION"), dtrSolInversion("FECHA_LIQUIDACION"))
            SQLCommand.Parameters.Add("pFechaOperacion", SqlDbType.Char, 10).Value = dtrSolInversion("FECHA_OPERACION")
            SQLCommand.Parameters.Add("pFechaVigencia", SqlDbType.Char, 10).Value = dtrSolInversion("FECHA_VIGENCIA")
            SQLCommand.Parameters.Add("pFlgTipoMovimiento", SqlDbType.Char, 1).Value = dtrSolInversion("FLG_TIPO_MOVIMIENTO")
            SQLCommand.Parameters.Add("pFlgTipoOrigen", SqlDbType.Char, 1).Value = dtrSolInversion("FLG_TIPO_ORIGEN")
            SQLCommand.Parameters.Add("pIdContraparte", SqlDbType.Int).Value = dtrSolInversion("ID_CONTRAPARTE")
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Int).Value = dtrSolInversion("ID_CUENTA")
            SQLCommand.Parameters.Add("pIdRepresentante", SqlDbType.Int).Value = dtrSolInversion("ID_REPRESENTANTE")
            SQLCommand.Parameters.Add("pIdSistemaConfirmacion", SqlDbType.Int).Value = dtrSolInversion("ID_SISTEMA_CONFIRMACION")
            SQLCommand.Parameters.Add("pIdSistemaOrigen", SqlDbType.Int).Value = dtrSolInversion("ID_SISTEMA_ORIGEN")
            SQLCommand.Parameters.Add("pIdTipoEstado", SqlDbType.Int).Value = dtrSolInversion("ID_TIPO_ESTADO")
            SQLCommand.Parameters.Add("pIdTrader", SqlDbType.Int).Value = dtrSolInversion("ID_TRADER")
            SQLCommand.Parameters.Add("pMontoOperacion", SqlDbType.Float, 20).Value = dtrSolInversion("MONTO_OPERACION")
            SQLCommand.Parameters.Add("pConfirmaComision", SqlDbType.Float, 20).Value = dtrSolInversion("CONFIRMA_COMISION")
            SQLCommand.Parameters.Add("pConfirmaDerechos", SqlDbType.Float, 20).Value = dtrSolInversion("CONFIRMA_DERECHOS")
            SQLCommand.Parameters.Add("pConfirmaGastos", SqlDbType.Float, 20).Value = dtrSolInversion("CONFIRMA_GASTOS")
            SQLCommand.Parameters.Add("pConfirmaIva", SqlDbType.Float, 20).Value = dtrSolInversion("CONFIRMA_IVA")
            SQLCommand.Parameters.Add("pConfirmaMontoOperacion", SqlDbType.Float, 20).Value = dtrSolInversion("CONFIRMA_MONTO_OPERACION")
            SQLCommand.Parameters.Add("pFechaConfirmacion", SqlDbType.Char, 10).Value = dtrSolInversion("FECHA_CONFIRMACION")
            SQLCommand.Parameters.Add("pFlgLimitePrecio", SqlDbType.VarChar, 1).Value = dtrSolInversion("FLG_LIMITE_PRECIO")
            SQLCommand.Parameters.Add("pInterest", SqlDbType.Float, 18).Value = dtrSolInversion("INTEREST")
            SQLCommand.Parameters.Add("pIdOperacionConcepto", SqlDbType.Int).Value = dtrSolInversion("ID_OPERACION_CONCEPTO")
            SQLCommand.Parameters.Add("pLineas", SqlDbType.Int, 5).Value = dtbOrdenes.Rows.Count
            SQLCommand.Parameters.Add("pDetalle1", SqlDbType.VarChar, 8000).Value = lstrDetalle1
            SQLCommand.Parameters.Add("pComisiones", SqlDbType.VarChar, 8000).Value = IIf(lstrComisiones = "", DBNull.Value, lstrComisiones)
            SQLCommand.Parameters.Add("pMediosCobroPago", SqlDbType.VarChar, 8000).Value = IIf(lstrMediosCobroPago = "", DBNull.Value, lstrMediosCobroPago)
            SQLCommand.Parameters.Add("pDetalle2", SqlDbType.VarChar, 8000).Value = IIf(lstrDetalle2 = "", DBNull.Value, lstrDetalle2)
            SQLCommand.Parameters.Add("pDetalle3", SqlDbType.VarChar, 8000).Value = IIf(lstrDetalle3 = "", DBNull.Value, lstrDetalle3)

            '...Id Solicitud de Inversion
            Dim ParametroSal1 As New SqlClient.SqlParameter("pIdSolInversion", SqlDbType.Int)
            ParametroSal1.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal1)

            '...Id Orden
            Dim ParametroSal2 As New SqlClient.SqlParameter("pIdOrden", SqlDbType.Int)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            '...Resultado
            Dim ParametroSal As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                llngIdDolInversion = SQLCommand.Parameters("pIdSolInversion").Value
                intSolInversion = llngIdDolInversion
                llngIdOrden = SQLCommand.Parameters("pIdOrden").Value
                intOrden = llngIdOrden
                MiTransaccionSQL.Commit()
            Else
                intSolInversion = 0
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error al Grabar Órden(es) " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            GuardarOrdenesCompleta = strDescError
        End Try

    End Function

    Public Function EnviaCorreoDesdeBaseDatos(ByRef intSolInversion As Integer, _
                                              ByRef TipoDocumento As String, _
                                              ByRef TipoOperacion As String, _
                                              ByVal FechaOperacion As String, _
                                              ByRef NacionalInternacional As String, _
                                              ByRef CorreoAsunto As String, _
                                              ByRef CorreoDestino As String, _
                                              Optional ByRef MailEditable As String = "NO") As String
        Dim DS_Auxiliar As New DataSet
        Dim DS_Auxiliar_aux As New DataSet
        Dim strDescError As String = "OK"
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        Dim intIndiceFor As Integer = 0

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try

            SQLCommand = New SqlClient.SqlCommand("CBA_Envio_Correo_Ordenes", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "CBA_Envio_Correo_Ordenes"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pintSolInversion", SqlDbType.Float, 10).Value = intSolInversion
            SQLCommand.Parameters.Add("pTipoDocumento", SqlDbType.VarChar, 100).Value = TipoDocumento
            SQLCommand.Parameters.Add("pTipoOperacion", SqlDbType.VarChar, 100).Value = TipoOperacion
            SQLCommand.Parameters.Add("pFechaOperacion", SqlDbType.VarChar, 10).Value = FechaOperacion
            SQLCommand.Parameters.Add("pNacionalInternacional", SqlDbType.VarChar, 1).Value = NacionalInternacional
            SQLCommand.Parameters.Add("pCorreoAsunto", SqlDbType.VarChar, 100).Value = CorreoAsunto
            SQLCommand.Parameters.Add("pCorreoDestino", SqlDbType.VarChar, 100).Value = CorreoDestino


            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(DS_Auxiliar, "BODY")

            DS_Auxiliar_aux = DS_Auxiliar
            DS_Auxiliar.Dispose()

            If DS_Auxiliar.Tables.Count < 1 Then
                strDescError = "No se encontro Orden para generar correo."
                Return strDescError.Trim
            End If

            Try
                '* Creamos un Objeto tipo Mail
                Dim m_OutLook As Outlook.Application
                Dim objMail As Outlook.MailItem

                '* Inicializamos nuestra apliación OutLook
                m_OutLook = New Outlook.Application

                '* Creamos una instancia de un objeto tipo MailItem
                objMail = m_OutLook.CreateItem(Outlook.OlItemType.olMailItem)

                '* Asignamos las propiedades a nuestra Instancial del objeto
                '* MailItem
                objMail.To = CorreoDestino
                'objMail.CC = "Margarita Mardones <mmardones@creasys.cl>;Gerardo Buenrostro <gbuenrostro@creasys.cl>"
                objMail.Subject = CorreoAsunto
                objMail.HTMLBody = DS_Auxiliar.Tables("BODY").Rows(0).Item(0).ToString
                objMail.BodyFormat = Outlook.OlBodyFormat.olFormatHTML

                objMail.SentOnBehalfOfName = NVL(DS_Auxiliar.Tables("BODY1").Rows(0).Item(0).ToString.Trim, "") '"GPI+ <gpi+@corp.cl>"
                objMail.Importance = Outlook.OlImportance.olImportanceHigh
                MailEditable = NVL(DS_Auxiliar.Tables("BODY2").Rows(0).Item(0).ToString.Trim, "")
                If MailEditable = "SI" Then
                    objMail.Display()
                Else
                    objMail.Send()
                End If
                '* Enviamos nuestro Mail y listo!
                '

                '* Desplegamos un mensaje indicando que todo fue exitoso
                'MessageBox.Show("Mail Enviado", "Integración con OutLook", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

            Catch ex As Exception
                strDescError = "Error no contemplado al Enviar Correo Contraparte. " & vbCr & ex.Message
            End Try

            'Return DS_Auxiliar_aux
            ''...Resultado
            'Dim ParametroSal As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            'ParametroSal.Direction = Data.ParameterDirection.Output
            'SQLCommand.Parameters.Add(ParametroSal)
            'SQLCommand.ExecuteNonQuery()

            'strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            'If strDescError.Trim = "OK" Then
            '    MiTransaccionSQL.Commit()
            'Else
            '    MiTransaccionSQL.Rollback()
            'End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error no contemplado al Enviar Correo Contraparte. " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
        End Try
        Return strDescError.Trim
    End Function

    Public Function OrdenesSec_Ver(ByVal dblpIdOpeacion As Double, _
                                    ByVal strpFechaDesde As String, _
                                    ByVal strpFechaHasta As String, _
                                    ByVal strpColumnas As String, _
                                    ByVal dblpIdNegocio As Double, _
                                    ByRef strpRetorno As String, _
                                    Optional ByRef strCodEstado As String = "", _
                                    Optional ByVal intIdCuenta As Integer = 0) As DataSet

        Dim DS_Auxiliar As New DataSet
        Dim DS_Auxiliar_aux As New DataSet
        Dim larr_NombreColumna() As String = Split(strpColumnas, ",")
        Dim lInt_Col As Integer = 0
        Dim lInt_NomCol As String = ""
        Dim lstr_NombreTabla As String = ""
        Dim lstrColumna As DataColumn
        Dim lblnRemove As Boolean = True
        Dim lstrProcedimiento As String = ""
        Dim strDescError As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        lstrProcedimiento = "Rcp_Orden_Consultar_Security"
        lstr_NombreTabla = "ORDENES"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = lstrProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("pIdOrden", SqlDbType.Float, 10).Value = IIf(dblpIdOpeacion = 0, DBNull.Value, dblpIdOpeacion)
            SQLCommand.Parameters.Add("pFechaDesde", SqlDbType.Char, 10).Value = strpFechaDesde
            SQLCommand.Parameters.Add("pFechaHasta", SqlDbType.Char, 10).Value = strpFechaHasta
            SQLCommand.Parameters.Add("pCodEstado", SqlDbType.Char, 1).Value = IIf(strCodEstado = "", DBNull.Value, strCodEstado)
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Float, 6).Value = IIf(glngIdUsuario = 0, DBNull.Value, glngIdUsuario)
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.VarChar, 20).Value = IIf(intIdCuenta = 0, DBNull.Value, intIdCuenta)
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Float, 10).Value = IIf(dblpIdNegocio = 0, DBNull.Value, dblpIdNegocio)

            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()
            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(DS_Auxiliar, lstr_NombreTabla)

            DS_Auxiliar_aux = DS_Auxiliar
            If strDescError.ToUpper.Trim <> "OK" Then
                gFun_ResultadoMsg("2|" & strDescError, "Buscar Cuentas")
                strpRetorno = "OK"
                DS_Auxiliar_aux = Nothing
            Else
                If strpColumnas.Trim = "" Then
                    DS_Auxiliar_aux = DS_Auxiliar
                Else
                    DS_Auxiliar_aux = DS_Auxiliar.Copy
                    For Each lstrColumna In DS_Auxiliar.Tables(lstr_NombreTabla).Columns
                        lblnRemove = True
                        For lInt_Col = 0 To larr_NombreColumna.Length - 1
                            lInt_NomCol = larr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                            If lstrColumna.ColumnName = lInt_NomCol Then
                                lblnRemove = False
                            End If
                        Next
                        If lblnRemove Then
                            DS_Auxiliar_aux.Tables(lstr_NombreTabla).Columns.Remove(lstrColumna.ColumnName)
                        End If
                    Next
                End If

                For lInt_Col = 0 To larr_NombreColumna.Length - 1
                    lInt_NomCol = larr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                    For Each lstrColumna In DS_Auxiliar_aux.Tables(lstr_NombreTabla).Columns
                        If lstrColumna.ColumnName = lInt_NomCol Then
                            lstrColumna.SetOrdinal(lInt_Col)
                            Exit For
                        End If
                    Next
                Next

                strpRetorno = "OK"
            End If
            DS_Auxiliar.Dispose()
            Return DS_Auxiliar_aux

        Catch ex As Exception
            strpRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function Ordenes_Buscar_X_Familia(ByVal strFecha As String, _
                                             ByVal strFamilia As String, _
                                             ByVal strColumnas As String, _
                                             ByRef strRetorno As String) As DataSet

        Dim DS_Auxiliar As New DataSet
        Dim DS_Auxiliar_aux As New DataSet
        Dim larr_NombreColumna() As String = Split(strColumnas, ",")
        Dim lInt_Col As Integer = 0
        Dim lInt_NomCol As String = ""
        Dim lstr_NombreTabla As String = ""
        Dim lstrColumna As DataColumn
        Dim lblnRemove As Boolean = True
        Dim lstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        lstrProcedimiento = "Rcp_Orden_Buscar_X_Familia"
        lstr_NombreTabla = "ORDENES"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = lstrProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("pFecha", SqlDbType.VarChar, 10).Value = strFecha
            SQLCommand.Parameters.Add("pCodigoFamilia", SqlDbType.VarChar, 10).Value = strFamilia

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(DS_Auxiliar, lstr_NombreTabla)

            DS_Auxiliar_aux = DS_Auxiliar

            If strColumnas.Trim = "" Then
                DS_Auxiliar_aux = DS_Auxiliar
            Else
                DS_Auxiliar_aux = DS_Auxiliar.Copy
                For Each lstrColumna In DS_Auxiliar.Tables(lstr_NombreTabla).Columns
                    lblnRemove = True
                    For lInt_Col = 0 To larr_NombreColumna.Length - 1
                        lInt_NomCol = larr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                        If lstrColumna.ColumnName = lInt_NomCol Then
                            lblnRemove = False
                        End If
                    Next
                    If lblnRemove Then
                        DS_Auxiliar_aux.Tables(lstr_NombreTabla).Columns.Remove(lstrColumna.ColumnName)
                    End If
                Next
            End If

            For lInt_Col = 0 To larr_NombreColumna.Length - 1
                lInt_NomCol = larr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                For Each lstrColumna In DS_Auxiliar_aux.Tables(lstr_NombreTabla).Columns
                    If lstrColumna.ColumnName = lInt_NomCol Then
                        lstrColumna.SetOrdinal(lInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            DS_Auxiliar.Dispose()
            Return DS_Auxiliar_aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function


    Public Function ActualizaSaldoOrdenes(ByVal intIdOrden As Integer, _
                                       ByVal intSaldoOrden As Integer, _
                                       ByVal strEstado As String) As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction
        Dim llngIdOperacion As Long = 0
        Dim llngIdMovCaja As Long = 0

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_CM_Actualiza_Ordenes_Bac", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_CM_Actualiza_Ordenes_Bac"
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()
            'Dim asdf As String = ""
            'If asdf = "asdasdas" Then
            '    intIdOrden = 0
            'End If
            SQLCommand.Parameters.Add("@pIdOrden", SqlDbType.Int).Value = intIdOrden
            SQLCommand.Parameters.Add("@pSaldoOrden", SqlDbType.Int).Value = intSaldoOrden
            SQLCommand.Parameters.Add("@pEstado", SqlDbType.Char, 1).Value = strEstado
            Dim ParametroSal1 As New SqlClient.SqlParameter("pError", SqlDbType.VarChar, 200)
            ParametroSal1.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal1)


            SQLCommand.ExecuteNonQuery()
            strDescError = SQLCommand.Parameters("pError").Value.ToString.Trim
            If strDescError = "OK" Then

                MiTransaccionSQL.Commit()
            Else

                MiTransaccionSQL.Rollback()
            End If

            'strDescError = "OK"

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error: " & Ex.Message & " (Rcp_CM_Actualiza_Ordenes_Bac)."
        Finally
            SQLConnect.Cerrar()
            ActualizaSaldoOrdenes = strDescError
        End Try
    End Function
End Class