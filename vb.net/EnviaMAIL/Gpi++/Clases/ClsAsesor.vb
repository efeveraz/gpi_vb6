﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports Microsoft.VisualBasic

Public Class ClsAsesor
    Public Function Asesor_Ver(ByVal strCodigoAsesor As String, _
                               ByVal strCodigoUsuario As String, _
                               ByVal strEstado As String, _
                               ByVal strBloqueo As String, _
                               ByVal strColumnas As String, _
                               ByRef strRetorno As String) As DataSet

        Dim LDS_Asesor As New DataSet
        Dim LDS_Asesor_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Asesor_Consultar"
        Lstr_NombreTabla = "Asesor"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdAsesor", SqlDbType.VarChar, 9).Value = IIf(strCodigoAsesor.Trim = "", DBNull.Value, strCodigoAsesor.Trim)
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.VarChar, 9).Value = IIf(strCodigoUsuario.Trim = "", DBNull.Value, strCodigoUsuario.Trim)
            SQLCommand.Parameters.Add("pEstado", SqlDbType.VarChar, 3).Value = IIf(strEstado.Trim = "", DBNull.Value, strEstado.Trim)
            SQLCommand.Parameters.Add("pBloqueo", SqlDbType.VarChar, 1).Value = IIf(strBloqueo.Trim = "", DBNull.Value, strBloqueo.Trim)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Asesor, Lstr_NombreTabla)

            LDS_Asesor_Aux = LDS_Asesor

            If strColumnas.Trim = "" Then
                LDS_Asesor_Aux = LDS_Asesor
            Else
                LDS_Asesor_Aux = LDS_Asesor.Copy
                For Each Columna In LDS_Asesor.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Asesor_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Asesor_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_Asesor.Dispose()
            Return LDS_Asesor_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function Asesor_Mantenedor(ByVal strAccion As String, _
                              ByVal strCodPais As String, _
                              ByVal strCodMercado As String, _
                              ByVal strDescAsesor As String, _
                              ByVal strEstAsesor As String, _
                              ByVal intIdAsesor As Integer) As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_Asesor_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Asesor_Mantencion"
            SQLCommand.Parameters.Clear()

            '@pAccion varchar(10),
            '@pCod_Pais varchar(3),
            '@pCod_Mercado char(4),
            '@pDsc_Asesor varchar(60),
            '@pEstado char(3),
            '@pIdAsesor int,
            '@pResultado varchar(200) OUTPUT)

            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 10).Value = strAccion
            SQLCommand.Parameters.Add("pCodPais", SqlDbType.VarChar, 3).Value = strCodPais
            SQLCommand.Parameters.Add("pCodMercado", SqlDbType.VarChar, 4).Value = strCodMercado
            SQLCommand.Parameters.Add("pDscAsesor", SqlDbType.VarChar, 60).Value = strDescAsesor
            SQLCommand.Parameters.Add("pEstado", SqlDbType.VarChar, 3).Value = strEstAsesor
            SQLCommand.Parameters.Add("pIdAsesor", SqlDbType.Float, 3).Value = intIdAsesor

            '...Resultado
            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            Asesor_Mantenedor = strDescError
        End Try
    End Function

    Public Function AsesorXCliente_Ver(ByVal strCodigoCliente As String, _
                              ByVal strColumnas As String, _
                              ByRef strRetorno As String) As DataSet

        Dim LDS_AsesorXCliente As New DataSet
        Dim LDS_AsesorXCliente_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Asesor_ConsultarPorCliente"
        Lstr_NombreTabla = "AsesorXCliente"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdCliente", SqlDbType.VarChar, 9).Value = IIf(strCodigoCliente.Trim = "", DBNull.Value, strCodigoCliente.Trim)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_AsesorXCliente, Lstr_NombreTabla)

            LDS_AsesorXCliente_Aux = LDS_AsesorXCliente

            If strColumnas.Trim = "" Then
                LDS_AsesorXCliente_Aux = LDS_AsesorXCliente
            Else
                LDS_AsesorXCliente_Aux = LDS_AsesorXCliente.Copy
                For Each Columna In LDS_AsesorXCliente.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_AsesorXCliente_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_AsesorXCliente_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_AsesorXCliente.Dispose()
            Return LDS_AsesorXCliente_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function AsesorXCliente_Mantenedor(ByVal strId_Cliente As String, ByRef dstAsesorXCliente As DataSet) As String

        Dim strDescError As String = "OK"
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try

            '       ELIMINAMOS LOS AsesorXCliente YA EXISTENTES

            SQLCommand = New SqlClient.SqlCommand("Rcp_Asesor_MantencionPorCliente", MiTransaccionSQL.Connection, MiTransaccionSQL)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Asesor_MantencionPorCliente"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = "ELIMINAR"
            SQLCommand.Parameters.Add("pIdcliente", SqlDbType.Float, 10).Value = strId_Cliente
            SQLCommand.Parameters.Add("pIdAsesor", SqlDbType.Float, 4).Value = DBNull.Value
            SQLCommand.Parameters.Add("pDscAsesor", SqlDbType.VarChar, 100).Value = DBNull.Value
            '...Resultado
            Dim pSalElimIntPort As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalElimIntPort.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalElimIntPort)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            If Not strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Rollback()
                Exit Function
            End If

            '       INSERTAMOS LOS NUEVOS AsesorXCliente PARA EL cliente

            If dstAsesorXCliente.Tables(0).Rows.Count <> Nothing Then
                For Each pRow As DataRow In dstAsesorXCliente.Tables(0).Rows
                    SQLCommand = New SqlClient.SqlCommand("Rcp_Asesor_MantencionPorCliente", MiTransaccionSQL.Connection, MiTransaccionSQL)
                    SQLCommand.CommandType = CommandType.StoredProcedure
                    SQLCommand.CommandText = "Rcp_Asesor_MantencionPorCliente"
                    SQLCommand.Parameters.Clear()

                    '[Rcp_Asesor_MantencionPorCliente]()
                    '(@pAccion varchar(10),
                    '@pIdcliente numeric(10, 0),
                    '@pIdAsesor numeric(4, 0),
                    '@pDscAsesor varchar(100),
                    '@pResultado varchar(200) OUTPUT) ID_CLIENTE,ID_ASESOR,NOMBRE

                    SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = "INSERTAR"
                    SQLCommand.Parameters.Add("pIdcliente", SqlDbType.Float, 10).Value = strId_Cliente
                    SQLCommand.Parameters.Add("pIdAsesor", SqlDbType.Float, 4).Value = pRow("ID_ASESOR").ToString
                    SQLCommand.Parameters.Add("pDscAsesor", SqlDbType.VarChar, 100).Value = pRow("NOMBRE").ToString

                    '...Resultado
                    Dim pSalInstPort As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                    pSalInstPort.Direction = Data.ParameterDirection.Output
                    SQLCommand.Parameters.Add(pSalInstPort)

                    SQLCommand.ExecuteNonQuery()

                    strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
                    If Not strDescError.ToUpper.Trim = "OK" Then
                        Exit For
                    End If
                Next
            End If

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            AsesorXCliente_Mantenedor = strDescError
        End Try
    End Function

    Public Function RetornaAsesoresDisponibles(ByRef strRetorno As String, ByVal intIdUsuario As Integer) As DataSet
        Dim lDS_Asesor As New DataSet
        Dim lDS_Asesor_Aux As New DataSet

        Dim Sqlcommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim Lstr_Procedimiento As String = ""
        Dim Lstr_NombreTabla As String = ""


        Lstr_Procedimiento = "Sis_Asesor_Consultar"
        Lstr_NombreTabla = "ASESOR"
        Connect.Abrir()

        Try
            Sqlcommand = New SqlCommand(Lstr_Procedimiento, Connect.Conexion)

            Sqlcommand.CommandType = CommandType.StoredProcedure
            Sqlcommand.CommandText = Lstr_Procedimiento
            Sqlcommand.Parameters.Clear()

            Sqlcommand.Parameters.Add("@pCodigoOperacion", SqlDbType.Char, 20).Value = "DISPONIBLES"
            Sqlcommand.Parameters.Add("@pIdUsuario", SqlDbType.Int).Value = IIf(intIdUsuario = 0, DBNull.Value, intIdUsuario)

            Dim oParamSal As New SqlClient.SqlParameter("@pMsjRetorno", SqlDbType.Char, 60)
            oParamSal.Direction = ParameterDirection.Output
            Sqlcommand.Parameters.Add(oParamSal)

            SQLDataAdapter.SelectCommand = Sqlcommand
            SQLDataAdapter.Fill(lDS_Asesor, Lstr_NombreTabla)

            lDS_Asesor_Aux = lDS_Asesor

            strRetorno = "OK"

            Return lDS_Asesor_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function RelacionarUsuarioAsesor(ByVal intIdAsesor As Integer, ByVal intIdUsuario As Integer, ByVal intIdAsesorNuevo As Integer) As String
        Dim lDS_Asesor As New DataSet
        Dim lDS_Asesor_Aux As New DataSet

        Dim Sqlcommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim Lstr_Procedimiento As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim lstrRetorno As String = "OK"

        Lstr_Procedimiento = "Sis_Asesor_CambiarUsuario"
        Lstr_NombreTabla = "ASESOR"
        Connect.Abrir()

        Try
            Sqlcommand = New SqlCommand(Lstr_Procedimiento, Connect.Conexion)

            Sqlcommand.CommandType = CommandType.StoredProcedure
            Sqlcommand.CommandText = Lstr_Procedimiento
            Sqlcommand.Parameters.Clear()
            Sqlcommand.Parameters.Add("@pIdAsesor", SqlDbType.Int).Value = IIf(intIdAsesor = 0, DBNull.Value, intIdAsesor)
            Sqlcommand.Parameters.Add("@pIdUsuario", SqlDbType.Int).Value = intIdUsuario
            Sqlcommand.Parameters.Add("@pIdAsesorNuevo", SqlDbType.Int).Value = IIf(intIdAsesorNuevo = 0, DBNull.Value, intIdAsesorNuevo)

            Dim oParamSal As New SqlClient.SqlParameter("@pMsjRetorno", SqlDbType.Char, 60)
            oParamSal.Direction = ParameterDirection.Output
            Sqlcommand.Parameters.Add(oParamSal)

            SQLDataAdapter.SelectCommand = Sqlcommand
            SQLDataAdapter.Fill(lDS_Asesor, Lstr_NombreTabla)

            lDS_Asesor_Aux = lDS_Asesor

            Return lstrRetorno

        Catch ex As Exception
            lstrRetorno = ex.Message
            Return lstrRetorno
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function Asesor_Mantencion(ByVal strAccion As String, _
                                       ByVal intIdAsesor As Integer, _
                                       ByVal intIdUsuario As Integer, _
                                       ByVal strRutAsesor As String, _
                                       ByVal strNombreAsesor As String, _
                                       ByVal strEmailAsesor As String, _
                                       ByVal strFonoAsesor As String, _
                                       ByVal strAbrNombreAsesor As String, _
                                       ByVal srtFlgBloqueoAsesor As String, _
                                       ByRef strEstado As String, _
                                       Optional ByVal strEjecutivo As String = "") As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_Asesor_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Asesor_Mantencion"
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 10).Value = strAccion
            SQLCommand.Parameters.Add("pId_Asesor", SqlDbType.Float, 4).Value = intIdAsesor
            SQLCommand.Parameters.Add("pCod_Usuario", SqlDbType.Float, 6).Value = intIdUsuario
            SQLCommand.Parameters.Add("pRut_Asesor", SqlDbType.VarChar, 10).Value = strRutAsesor
            SQLCommand.Parameters.Add("pNombre_Asesor", SqlDbType.VarChar, 150).Value = strNombreAsesor
            SQLCommand.Parameters.Add("pEmail_Asesor", SqlDbType.VarChar, 50).Value = strEmailAsesor
            SQLCommand.Parameters.Add("pFono_Asesor", SqlDbType.VarChar, 50).Value = strFonoAsesor
            SQLCommand.Parameters.Add("pAbr_Asesor", SqlDbType.VarChar, 6).Value = strAbrNombreAsesor
            SQLCommand.Parameters.Add("pFlg_Bloqueo_Asesor", SqlDbType.VarChar, 1).Value = srtFlgBloqueoAsesor
            SQLCommand.Parameters.Add("pEstado_Asesor", SqlDbType.VarChar, 3).Value = strEstado
            SQLCommand.Parameters.Add("pCodEjecutivo", SqlDbType.VarChar, 10).Value = IIf(strEjecutivo = "", DBNull.Value, strEjecutivo)
            '...Resultado
            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            Asesor_Mantencion = strDescError
        End Try
    End Function

    Public Function CustodiosAsesor_Consultar(ByVal lngIdAsesor As Long, _
                                              ByVal strTipoCustodio As String, _
                                              ByVal strEstado As String, _
                                              ByVal strColumnas As String, _
                                              ByRef strRetorno As String) As DataSet

        Dim LDS_CustodiosAsesor As New DataSet
        Dim LDS_CustodiosAsesor_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_CustodiosAsesor_Consultar"
        Lstr_NombreTabla = "CustodiosAsesor"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdAsesor", SqlDbType.Int, 4).Value = IIf(lngIdAsesor = 0, DBNull.Value, lngIdAsesor)
            SQLCommand.Parameters.Add("pTipoCustodio", SqlDbType.VarChar, 1).Value = IIf(strTipoCustodio.Trim = "", DBNull.Value, strTipoCustodio.Trim)
            SQLCommand.Parameters.Add("pEstado", SqlDbType.VarChar, 3).Value = IIf(strEstado.Trim = "", DBNull.Value, strEstado.Trim)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_CustodiosAsesor, Lstr_NombreTabla)

            LDS_CustodiosAsesor_Aux = LDS_CustodiosAsesor

            If strColumnas.Trim = "" Then
                LDS_CustodiosAsesor_Aux = LDS_CustodiosAsesor
            Else
                LDS_CustodiosAsesor_Aux = LDS_CustodiosAsesor.Copy
                For Each Columna In LDS_CustodiosAsesor.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_CustodiosAsesor_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_CustodiosAsesor_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_CustodiosAsesor.Dispose()
            Return LDS_CustodiosAsesor_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function CustodiosAsesor_Mantenedor(ByVal lngId_Asesor As String, ByRef dstCustodiosAsesor As DataSet) As String
        Dim strDescError As String = "OK"
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction
        Dim lstrAccion As String

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try

            SQLCommand = New SqlClient.SqlCommand("Rcp_CustodiosAsesor_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_CustodiosAsesor_Mantencion"

            '+ SE INSERTA, MODIFICA O ELIMINA LOS LAS ETIQUETAS X CUENTA 

            If dstCustodiosAsesor.Tables(0).Rows.Count <> Nothing Then
                For Each pRow As DataRow In dstCustodiosAsesor.Tables(0).Rows
                    '+ Si hubo modificación en el valor
                    If pRow("COD_ASESOR_INI").ToString.Trim <> pRow("COD_ASESOR").ToString.Trim Then
                        If pRow("COD_ASESOR_INI").ToString.Trim = "" Then
                            lstrAccion = "I"
                        Else
                            If pRow("COD_ASESOR").ToString.Trim = "" Then
                                lstrAccion = "E"
                            Else
                                lstrAccion = "M"
                            End If
                        End If
                        SQLCommand.Parameters.Clear()

                        SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 10).Value = lstrAccion
                        SQLCommand.Parameters.Add("pIdAsesor", SqlDbType.Float, 4).Value = lngId_Asesor
                        SQLCommand.Parameters.Add("pIdCustodio", SqlDbType.Float, 10).Value = pRow("ID_CUSTODIO").ToString
                        SQLCommand.Parameters.Add("pValor", SqlDbType.VarChar, 50).Value = pRow("COD_ASESOR").ToString

                        '...Resultado
                        Dim pSalInstPort As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                        pSalInstPort.Direction = Data.ParameterDirection.Output
                        SQLCommand.Parameters.Add(pSalInstPort)

                        SQLCommand.ExecuteNonQuery()

                        strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
                    End If
                    If Not strDescError.ToUpper.Trim = "OK" Then
                        Exit For
                    End If
                Next
            End If

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error al actualizar los custodios del asesor [" & vbCr & Ex.Message & "]"
        Finally
            SQLConnect.Cerrar()
            CustodiosAsesor_Mantenedor = strDescError
        End Try
    End Function

    Public Function ClientesXAsesor(ByVal dblIdAsesor As Double, _
                               ByVal strColumnas As String, _
                               ByRef strRetorno As String) As DataSet

        Dim LDS_ClientesXAsesor As New DataSet
        Dim LDS_ClientesXAsesor_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Asesor_Clientes_Consultar"
        Lstr_NombreTabla = "ClientesXAsesor"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdAsesor", SqlDbType.Float, 4).Value = dblIdAsesor

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_ClientesXAsesor, Lstr_NombreTabla)

            LDS_ClientesXAsesor_Aux = LDS_ClientesXAsesor

            If strColumnas.Trim = "" Then
                LDS_ClientesXAsesor_Aux = LDS_ClientesXAsesor
            Else
                LDS_ClientesXAsesor_Aux = LDS_ClientesXAsesor.Copy
                For Each Columna In LDS_ClientesXAsesor.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_ClientesXAsesor_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_ClientesXAsesor_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_ClientesXAsesor.Dispose()
            Return LDS_ClientesXAsesor_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function
End Class
