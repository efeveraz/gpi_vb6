﻿Imports System.Data.SqlClient
Imports System.Data

Public Class ClsClaseInstrumento

#Region "Declaraciones de variables"
    Dim sProcedimiento As String = ""

#End Region

#Region "Metodos de la Clase"
    Public Function TraeDatosClaseInstrumento(ByRef strRetorno As String, Optional ByVal strCodClaseInstrumento As String = "") As DataSet

        Dim LDS_ClassInstr As New DataSet
        Dim LDS_ClassInstr_Aux As New DataSet

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        sProcedimiento = "Rcp_ClaseInstrumento_Buscar"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(sProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = sProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("@pCodClaseInstrumento", SqlDbType.VarChar, 10).Value = IIf(strCodClaseInstrumento = "", DBNull.Value, strCodClaseInstrumento)
            SQLCommand.Parameters.Add("@pIdNegocio", SqlDbType.Int).Value = glngNegocioOperacion
            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_ClassInstr)

            LDS_ClassInstr_Aux = LDS_ClassInstr
            strRetorno = "OK"
            Return LDS_ClassInstr_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function ClaseInst_Ver(ByVal strCodClase As String, _
                                  ByVal strColumnas As String, _
                                 ByRef strRetorno As String) As DataSet

        Dim LDS_ClasesIns As New DataSet
        Dim LDS_ClasesIns_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_ClaseInstrumento_Consultar"
        Lstr_NombreTabla = "ClaseInstrumento"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pCodClaseInstrumento", SqlDbType.VarChar, 15).Value = IIf(strCodClase = "", DBNull.Value, strCodClase)
            SQLCommand.Parameters.Add("@pIdNegocio", SqlDbType.Int).Value = glngNegocioOperacion
            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_ClasesIns, Lstr_NombreTabla)

            LDS_ClasesIns_Aux = LDS_ClasesIns

            If strColumnas.Trim = "" Then
                LDS_ClasesIns_Aux = LDS_ClasesIns
            Else
                LDS_ClasesIns_Aux = LDS_ClasesIns.Copy
                For Each Columna In LDS_ClasesIns.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_ClasesIns_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_ClasesIns_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_ClasesIns.Dispose()
            Return LDS_ClasesIns_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function
#End Region
End Class
