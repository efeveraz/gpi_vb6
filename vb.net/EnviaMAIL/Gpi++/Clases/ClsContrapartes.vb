﻿Public Class ClsContraParte

    Public Function ContraParte_Ver(ByVal lngIdContraparte As Integer, _
                                    ByVal strIngresaOp As String, _
                                    ByVal strCodClaseInstrumento As String, _
                                    ByVal strCodSubClaseInstrumento As String, _
                                    ByVal strColumnas As String, _
                                    ByVal strEstado As String, _
                                    ByRef strRetorno As String, _
                                    ByVal strDscTipoContraparte As String) As DataSet

        Dim LDS_Contraparte As New DataSet
        Dim LDS_Contraparte_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_ContraParte_Consultar"
        Lstr_NombreTabla = "CONTRAPARTE"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdContraparte", SqlDbType.Float, 9).Value = IIf(lngIdContraparte = 0, DBNull.Value, lngIdContraparte)
            SQLCommand.Parameters.Add("pCodClaseInstrumento", SqlDbType.VarChar, 15).Value = IIf(strCodClaseInstrumento.Trim = "", DBNull.Value, strCodClaseInstrumento.Trim)
            SQLCommand.Parameters.Add("pCodSubClaseInstrumento", SqlDbType.VarChar, 10).Value = IIf(strCodSubClaseInstrumento.Trim = "", DBNull.Value, strCodSubClaseInstrumento.Trim)
            SQLCommand.Parameters.Add("pIngresaOp", SqlDbType.Char, 1).Value = IIf(strIngresaOp.Trim = "", DBNull.Value, strIngresaOp.Trim)
            SQLCommand.Parameters.Add("pEstado", SqlDbType.VarChar, 3).Value = IIf(strEstado.Trim = "", DBNull.Value, strEstado.Trim)
            SQLCommand.Parameters.Add("pDscTipoContraparte", SqlDbType.VarChar, 50).Value = IIf(strDscTipoContraparte.Trim = "", DBNull.Value, strDscTipoContraparte.Trim)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Contraparte, Lstr_NombreTabla)

            LDS_Contraparte_Aux = LDS_Contraparte

            If strColumnas.Trim = "" Then
                LDS_Contraparte_Aux = LDS_Contraparte
            Else
                LDS_Contraparte_Aux = LDS_Contraparte.Copy
                For Each Columna In LDS_Contraparte.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Contraparte_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Contraparte_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            Return LDS_Contraparte_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function EntidadContraParte_Ver(ByVal strCodCContraparte As String, _
                                           ByVal strIngresaOp As String, _
                                           ByVal strEstadoContraparte As String, _
                                           ByVal strAlias As String, _
                                           ByVal strColumnas As String, _
                                           ByRef strRetorno As String) As DataSet

        Dim LDS_Contraparte As New DataSet
        Dim LDS_Contraparte_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_EntidadContraparte_Consultar"
        Lstr_NombreTabla = "EntidadContraparte"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pCodContraparte", SqlDbType.VarChar, 50).Value = IIf(strCodCContraparte = "", DBNull.Value, strCodCContraparte)
            SQLCommand.Parameters.Add("pIngresaOp", SqlDbType.VarChar, 1).Value = IIf(strIngresaOp = "", DBNull.Value, strIngresaOp)
            SQLCommand.Parameters.Add("pEstadoContraparte", SqlDbType.VarChar, 3).Value = IIf(strEstadoContraparte = "", DBNull.Value, strEstadoContraparte)
            SQLCommand.Parameters.Add("pAlias", SqlDbType.VarChar, 1).Value = IIf(strAlias = "", DBNull.Value, strAlias)


            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Contraparte, Lstr_NombreTabla)

            LDS_Contraparte_Aux = LDS_Contraparte

            If strColumnas.Trim = "" Then
                LDS_Contraparte_Aux = LDS_Contraparte
            Else
                LDS_Contraparte_Aux = LDS_Contraparte.Copy
                For Each Columna In LDS_Contraparte.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Contraparte_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Contraparte_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            Return LDS_Contraparte_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

  Public Function ContraParte_Mantenedor(ByVal strAccion As String, _
                                            ByVal strDescripcion As String, _
                                            ByVal strCodigoExterno As String, _
                                            ByVal strInsertaOp As String, _
                                            ByVal strIdTipoContraparte As String, _
                                            ByVal strEstado As String, _
                                            ByVal strRut As String, _
                                            ByRef intIdContraparte As Integer, _
                                            ByVal intGenerado As Integer, _
                                            ByRef dstTrader As DataTable, _
                                            ByRef dstIncluidas As DataTable) As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_ContraParte_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_ContraParte_Mantencion"
            SQLCommand.Parameters.Clear()


            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 10).Value = strAccion
            SQLCommand.Parameters.Add("pDscContraParte", SqlDbType.VarChar, 50).Value = strDescripcion
            SQLCommand.Parameters.Add("pCodigoExterno", SqlDbType.VarChar, 50).Value = strCodigoExterno
            SQLCommand.Parameters.Add("pInsertaOp", SqlDbType.VarChar, 1).Value = strInsertaOp
            SQLCommand.Parameters.Add("pIdTipoContraParte", SqlDbType.Float, 4).Value = strIdTipoContraparte
            SQLCommand.Parameters.Add("pIdContraparte", SqlDbType.Float, 9).Value = intIdContraparte
            SQLCommand.Parameters.Add("pEstado", SqlDbType.VarChar, 3).Value = strEstado
            SQLCommand.Parameters.Add("pRut", SqlDbType.VarChar, 10).Value = strRut

            '...Resultado
            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            Dim pSalInstr As New SqlClient.SqlParameter("pGenerado", SqlDbType.Float, 9)
            pSalInstr.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalInstr)

            SQLCommand.ExecuteNonQuery()
            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                intIdContraparte = SQLCommand.Parameters("pGenerado").Value
            Else
                MiTransaccionSQL.Rollback()
                Return strDescError
                Exit Function
            End If

            If dstTrader.Rows.Count <> Nothing Then
                For Each pRow As DataRow In dstTrader.Rows
                    SQLCommand = New SqlClient.SqlCommand("Rcp_Trader_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)
                    SQLCommand.CommandType = CommandType.StoredProcedure
                    SQLCommand.CommandText = "Rcp_Trader_Mantencion"
                    SQLCommand.Parameters.Clear()
                    If pRow("ESTADO").ToString = "I" Then
                        SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = "INSERTAR"
                        SQLCommand.Parameters.Add("pIdContraParte", SqlDbType.Float, 9).Value = intIdContraparte
                        SQLCommand.Parameters.Add("pDscTrader", SqlDbType.VarChar, 100).Value = pRow("DSC_TRADER").ToString
                        SQLCommand.Parameters.Add("pMailTrader", SqlDbType.VarChar, 100.4).Value = pRow("EMAIL_TRADER").ToString
                    ElseIf pRow("ESTADO").ToString = "E" Then
                        SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = "ELIMINAR"
                        SQLCommand.Parameters.Add("pIdContraParte", SqlDbType.Float, 9).Value = intIdContraparte
                        SQLCommand.Parameters.Add("pIdTrader", SqlDbType.Float, 18).Value = pRow("ID_TRADER").ToString
                        SQLCommand.Parameters.Add("pDscTrader", SqlDbType.VarChar, 100).Value = pRow("DSC_TRADER").ToString
                        SQLCommand.Parameters.Add("pMailTrader", SqlDbType.VarChar, 100.4).Value = pRow("EMAIL_TRADER").ToString
                    ElseIf pRow("ESTADO").ToString = "M" Then
                        SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = "MODIFICAR"
                        SQLCommand.Parameters.Add("pIdContraParte", SqlDbType.Float, 9).Value = intIdContraparte
                        SQLCommand.Parameters.Add("pIdTrader", SqlDbType.Float, 18).Value = pRow("ID_TRADER").ToString
                        SQLCommand.Parameters.Add("pDscTrader", SqlDbType.VarChar, 100).Value = pRow("DSC_TRADER").ToString
                        SQLCommand.Parameters.Add("pMailTrader", SqlDbType.VarChar, 100.4).Value = pRow("EMAIL_TRADER").ToString
                    End If

                    If SQLCommand.Parameters.Count > 0 Then
                        '...Resultado
                        Dim pSalInstPort As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                        pSalInstPort.Direction = Data.ParameterDirection.Output
                        SQLCommand.Parameters.Add(pSalInstPort)

                        SQLCommand.ExecuteNonQuery()

                        strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
                        If Not strDescError.ToUpper.Trim = "OK" Then
                            Exit For
                        End If
                    End If
                Next
            End If

            If Not strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Rollback()
                Exit Function
            End If

            ' SECCION SUB CLASES INCLUIDAS EN EL CONTRAPARTE
            '       ELIMINAMOS LAS SUBCLASES ASOCIADAS AL CONTRAPARTE

            SQLCommand = New SqlClient.SqlCommand("Rcp_SubClaseInstrumentoContraParte_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_SubClaseInstrumentoContraParte_Mantencion"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = "ELIMINAR"
            SQLCommand.Parameters.Add("pIdContraParte", SqlDbType.Float, 9).Value = intIdContraparte

            '...Resultado
            Dim pSalElimPerfPort As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalElimPerfPort.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalElimPerfPort)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            If Not strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Rollback()
                Exit Function
            End If


            '       INSERTAMOS LAS NUEVAS SUBCLASES ASOCIADAS AL CONTRAPARTE
            If dstIncluidas.Rows.Count <> Nothing Then

                For Each pRow As DataRow In dstIncluidas.Rows
                    SQLCommand = New SqlClient.SqlCommand("Rcp_SubClaseInstrumentoContraParte_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)
                    SQLCommand.CommandType = CommandType.StoredProcedure
                    SQLCommand.CommandText = "Rcp_SubClaseInstrumentoContraParte_Mantencion"
                    SQLCommand.Parameters.Clear()

                    SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = "INSERTAR"
                    SQLCommand.Parameters.Add("pIdContraParte", SqlDbType.Float, 9).Value = intIdContraparte
                    SQLCommand.Parameters.Add("pCodSubclase", SqlDbType.VarChar, 10).Value = pRow("COD_SUB_CLASE")

                    '...Resultado
                    Dim pSalPerfPort As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                    pSalPerfPort.Direction = Data.ParameterDirection.Output
                    SQLCommand.Parameters.Add(pSalPerfPort)

                    SQLCommand.ExecuteNonQuery()

                    strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
                    If Not strDescError.ToUpper.Trim = "OK" Then
                        Exit For
                    End If
                Next
            End If

            'AQUI SE REALIZA EL COMMIT POR QUE ES LA ULTIMA OPERACION 


            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            ContraParte_Mantenedor = strDescError
        End Try

  End Function


    Public Function ContraParte_Buscar(ByVal lngIdContraparte As Integer, _
                                  ByVal strIngresaOp As String, _
                                  ByVal strDscContraparte As String, _
                                  ByVal strCodClaseInstrumento As String, _
                                  ByVal strCodSubClaseInstrumento As String, _
                                  ByVal strColumnas As String, _
                                  ByVal strEstado As String, _
                                  ByRef strRetorno As String, _
                                  Optional ByVal strRut As String = "", _
                                  Optional ByVal strDscTipoCont As String = "") As DataSet

        Dim LDS_Contraparte As New DataSet
        Dim LDS_Contraparte_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_ContraParte_Buscar"
        Lstr_NombreTabla = "CONTRAPARTE"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdContraparte", SqlDbType.Float, 9).Value = IIf(lngIdContraparte = 0, DBNull.Value, lngIdContraparte)
            SQLCommand.Parameters.Add("pDscContraparte", SqlDbType.VarChar, 50).Value = IIf(strDscContraparte.Trim = "", DBNull.Value, strDscContraparte.Trim)
            SQLCommand.Parameters.Add("pCodClaseInstrumento", SqlDbType.VarChar, 15).Value = IIf(strCodClaseInstrumento.Trim = "", DBNull.Value, strCodClaseInstrumento.Trim)
            SQLCommand.Parameters.Add("pCodSubClaseInstrumento", SqlDbType.VarChar, 10).Value = IIf(strCodSubClaseInstrumento.Trim = "", DBNull.Value, strCodSubClaseInstrumento.Trim)
            SQLCommand.Parameters.Add("pIngresaOp", SqlDbType.Char, 1).Value = IIf(strIngresaOp.Trim = "", DBNull.Value, strIngresaOp.Trim)
            SQLCommand.Parameters.Add("pEstado", SqlDbType.VarChar, 3).Value = IIf(strEstado.Trim = "", DBNull.Value, strEstado.Trim)
            SQLCommand.Parameters.Add("pRut", SqlDbType.VarChar, 10).Value = IIf(strRut.Trim = "", DBNull.Value, strRut.Trim)
            SQLCommand.Parameters.Add("pDscTipoContraparte", SqlDbType.VarChar, 10).Value = IIf(strDscTipoCont.Trim = "", DBNull.Value, strDscTipoCont.Trim)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Contraparte, Lstr_NombreTabla)

            LDS_Contraparte_Aux = LDS_Contraparte

            If strColumnas.Trim = "" Then
                LDS_Contraparte_Aux = LDS_Contraparte
            Else
                LDS_Contraparte_Aux = LDS_Contraparte.Copy
                For Each Columna In LDS_Contraparte.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Contraparte_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Contraparte_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            Return LDS_Contraparte_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function
End Class


