﻿Imports System.Data
Imports System.Data.SqlClient

Public Class ClsGeneral

    Public Sub TraerTipoCambio(ByVal strIdNegocio As String, _
                               ByVal strIdCuenta As String, _
                               ByVal strIdMonedaOrigen As String, _
                               ByVal stridMonedaDestino As String, _
                               ByVal strFechaConsulta As String, _
                               ByRef intVueltas As Int16, _
                               ByRef dblParidad As Double, _
                               ByRef strOperacion As String, _
                               ByRef strDescError As String)


        Dim DS_Paso As New DataSet
        Dim lstrProcedimiento As String = ""
        Dim LstrNombreTabla As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion

        'Abre la conexion
        lstrProcedimiento = "Rcp_General_BuscarTipoCambio"
        LstrNombreTabla = "PASO"
        Connect.Abrir()

        Try

            SQLCommand = New SqlCommand(lstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = lstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Float, 10).Value = CLng(strIdNegocio)
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = CLng(strIdCuenta)
            SQLCommand.Parameters.Add("pCodMonedaOrigen", SqlDbType.Char, 4).Value = strIdMonedaOrigen
            SQLCommand.Parameters.Add("pCodMonedaDestino", SqlDbType.Char, 4).Value = stridMonedaDestino
            SQLCommand.Parameters.Add("pFecha", SqlDbType.Char, 10).Value = strFechaConsulta
            SQLCommand.Parameters.Add("pVueltas", SqlDbType.Int, 1).Value = intVueltas

            Dim ParametroSal1 As New SqlClient.SqlParameter("pParidad", SqlDbType.Float, 10)
            ParametroSal1.Direction = ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal1)

            Dim ParametroSal2 As New SqlClient.SqlParameter("pOperacion", SqlDbType.Char, 1)
            ParametroSal2.Direction = ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            Dim ParametroSal3 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal3.Direction = ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal3)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(DS_Paso, LstrNombreTabla)

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                dblParidad = SQLCommand.Parameters("pParidad").Value
                strOperacion = SQLCommand.Parameters("pOperacion").Value.ToString.Trim
            Else
                dblParidad = 1
            End If

        Catch Ex As Exception
            strDescError = "Error en el Búsqueda de Precio Mercado." & vbCr & Ex.Message
        Finally
            Connect.Cerrar()
        End Try
    End Sub

    Public Function TraerTipoMedioPagoCobro(ByVal strTipoPagoCobro As String, _
                                        ByVal strColumnas As String, _
                                        ByRef strRetorno As String) As DataSet

        Dim LDS_TipoMedioPagoCobro As New DataSet
        Dim LDS_TipoMedioPagoCobro_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_TipoMedioPagoCobro_Consultar"
        Lstr_NombreTabla = "PAGO_COBRO"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pTipoPagoCobro", SqlDbType.Char, 1).Value = IIf(strTipoPagoCobro = "", DBNull.Value, strTipoPagoCobro)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_TipoMedioPagoCobro, Lstr_NombreTabla)

            LDS_TipoMedioPagoCobro_Aux = LDS_TipoMedioPagoCobro

            If strColumnas.Trim = "" Then
                LDS_TipoMedioPagoCobro_Aux = LDS_TipoMedioPagoCobro
            Else
                LDS_TipoMedioPagoCobro_Aux = LDS_TipoMedioPagoCobro.Copy
                For Each Columna In LDS_TipoMedioPagoCobro.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_TipoMedioPagoCobro_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_TipoMedioPagoCobro_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            Return LDS_TipoMedioPagoCobro_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function TraerMedioPagoCobro(ByVal lngIdMovCaja As Long, _
                               ByVal lngIdOrden As Long, _
                               ByVal lngIdCuenta As Long, _
                               ByVal llngIdComisionDetalle As Long, _
                               ByRef strColumnas As String, _
                               ByRef strDescError As String, _
                      Optional ByVal lngIdMedioPagoCobro As Long = 0) As DataSet

        Dim LDS_MedioPagoCobro As New DataSet
        Dim LDS_MedioPagoCobro_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_MedioPagoCobro_Consultar"
        Lstr_NombreTabla = "PAGO_COBRO"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdMovCaja", SqlDbType.Float, 10).Value = IIf(lngIdMovCaja = 0, DBNull.Value, lngIdMovCaja)
            SQLCommand.Parameters.Add("pIdOrden", SqlDbType.Float, 10).Value = IIf(lngIdOrden = 0, DBNull.Value, lngIdOrden)
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = IIf(lngIdCuenta = 0, DBNull.Value, lngIdCuenta)
            SQLCommand.Parameters.Add("pIdComisionDetalle", SqlDbType.Float, 10).Value = IIf(llngIdComisionDetalle = 0, DBNull.Value, llngIdComisionDetalle)
            SQLCommand.Parameters.Add("pIdMedioPagoCobro", SqlDbType.Float, 10).Value = IIf(lngIdMedioPagoCobro = 0, DBNull.Value, lngIdMedioPagoCobro)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_MedioPagoCobro, Lstr_NombreTabla)

            LDS_MedioPagoCobro_Aux = LDS_MedioPagoCobro

            If strColumnas.Trim = "" Then
                LDS_MedioPagoCobro_Aux = LDS_MedioPagoCobro
            Else
                LDS_MedioPagoCobro_Aux = LDS_MedioPagoCobro.Copy
                For Each Columna In LDS_MedioPagoCobro.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_MedioPagoCobro_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_MedioPagoCobro_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strDescError = "OK"
            Return LDS_MedioPagoCobro_Aux

        Catch ex As Exception
            strDescError = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function RetornarParametrosSistemas(ByVal strIdParametro As String, _
                                           ByVal strEstadoParametro As String, _
                                           ByVal strColumnas As String, _
                                           ByRef strResultado As String) As DataSet

        Try

            Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
            Dim LInt_Col As Integer
            Dim LTxt_NomCol As String
            Dim Columna As DataColumn
            Dim Remove As Boolean

            Dim LDS_ParametrosSistema As New DataSet
            Dim LDS_ParametrosSistema_Aux As New DataSet

            Dim SQLCommand As New SqlClient.SqlCommand
            Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

            Dim Connect As Cls_Conexion = New Cls_Conexion
            Dim LTxt_NombreTabla As String = ""
            LTxt_NombreTabla = "ParametrosSistema"

            'Abre la conexion 
            Connect.Abrir()
            Try
                SQLCommand = New SqlClient.SqlCommand("Sis_ParametrosSistema_Consultar", Connect.Conexion)

                SQLCommand.CommandType = CommandType.StoredProcedure
                SQLCommand.CommandText = "Sis_ParametrosSistema_Consultar"
                SQLCommand.Parameters.Clear()

                SQLCommand.Parameters.Add("pIdParametro", SqlDbType.Float, 10).Value = IIf(strIdParametro = "", DBNull.Value, strIdParametro)
                SQLCommand.Parameters.Add("pEstadoParametro", SqlDbType.Char, 3).Value = IIf(strEstadoParametro = "", DBNull.Value, strEstadoParametro)

                SQLDataAdapter.SelectCommand = SQLCommand
                SQLDataAdapter.Fill(LDS_ParametrosSistema, LTxt_NombreTabla)

                If strColumnas.Trim = "" Then
                    LDS_ParametrosSistema_Aux = LDS_ParametrosSistema
                Else
                    LDS_ParametrosSistema_Aux = LDS_ParametrosSistema.Copy
                    For Each Columna In LDS_ParametrosSistema.Tables(LTxt_NombreTabla).Columns
                        Remove = True
                        For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                            LTxt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                            If Columna.ColumnName = LTxt_NomCol Then
                                Remove = False
                            End If
                        Next
                        If Remove Then
                            LDS_ParametrosSistema_Aux.Tables(LTxt_NombreTabla).Columns.Remove(Columna.ColumnName)
                        End If
                    Next
                End If

                For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                    LTxt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                    For Each Columna In LDS_ParametrosSistema_Aux.Tables(LTxt_NombreTabla).Columns
                        If Columna.ColumnName = LTxt_NomCol Then
                            Columna.SetOrdinal(LInt_Col)
                            Exit For
                        End If
                    Next
                Next
                strResultado = "OK"
                Return LDS_ParametrosSistema_Aux

            Catch ex As Exception
                strResultado = ex.Message
                Return Nothing
            Finally
                Connect.Cerrar()
            End Try

        Catch ex As Exception
            strResultado = "Error : NO se  pudo establecer conección con la Base de Datos..."
            Return Nothing
        End Try
    End Function

    Public Sub TraerFechaHoraSistema(ByVal strFormatoFecha As String, _
                                     ByVal strFormatoHora As String, _
                                     ByRef strFechaSistema As String, _
                                     ByRef strHoraSistena As String, _
                                     ByRef strDescError As String)


        Dim DS_Paso As New DataSet
        Dim lstrProcedimiento As String = ""
        Dim LstrNombreTabla As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion

        'Abre la conexion
        lstrProcedimiento = "Rcp_General_BuscarFechaHoraSistema"
        LstrNombreTabla = "PASO"
        Connect.Abrir()

        Try

            SQLCommand = New SqlCommand(lstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = lstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pFormatoFecha", SqlDbType.Char, 100).Value = strFormatoFecha
            SQLCommand.Parameters.Add("pFormatoHora", SqlDbType.Char, 100).Value = strFormatoHora

            Dim ParametroSal1 As New SqlClient.SqlParameter("pFechaSistema", SqlDbType.Char, 10)
            ParametroSal1.Direction = ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal1)

            Dim ParametroSal2 As New SqlClient.SqlParameter("pHoraSistema", SqlDbType.Char, 10)
            ParametroSal2.Direction = ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            Dim ParametroSal3 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal3.Direction = ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal3)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(DS_Paso, LstrNombreTabla)

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                strFechaSistema = SQLCommand.Parameters("pFechaSistema").Value.ToString.Trim
                strHoraSistena = SQLCommand.Parameters("pHoraSistema").Value.ToString.Trim
            End If

        Catch Ex As Exception
            If Ex.Message = "SISTEMA DESACTUALIZADO, FAVOR COMUNIQUESE CON SU PROVEEDOR" Then
                strDescError = Ex.Message
            Else
                strDescError = "Error en el Búsqueda de Fecha/Hora Sistema." & vbCr & Ex.Message
            End If
        Finally
            Connect.Cerrar()
        End Try
    End Sub

    Public Sub Parametros_Guardar(ByVal intIdParametro As Integer, _
                                   ByVal strCodigoParametro As String, _
                                   ByVal strNombreParametro As String, _
                                   ByVal strTipoDato As String, _
                                   ByVal strCaracterDecimal As String, _
                                   ByVal strFormatoFecha As String, _
                                   ByVal strFormatoNumerico As String, _
                                   ByVal strEsPorcentajeBase100 As String, _
                                   ByVal strCaracteresNoPermitidos As String, _
                                   ByVal strCantidadDecimales As String, _
                                   ByVal strValorParametro As String, _
                                   ByVal strEstadoParametro As String, _
                                   ByVal strEsRango As String, _
                                   ByVal strRangoInicial As String, _
                                   ByVal strRangoFinal As String, _
                                   ByVal strEsPersistente As String, _
                                   ByVal strOperacion As String, _
                                   ByVal strDescError As String)

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        'Abre la conexion
        Connect.Abrir()

        Dim MiTransaccion As SqlClient.SqlTransaction

        MiTransaccion = Connect.Transaccion

        Try

            '+ Cambiar por el procedimeitno a que llmas
            SQLCommand = New SqlClient.SqlCommand("Sis_ParametrosSistema_Guardar", MiTransaccion.Connection, MiTransaccion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Sis_ParametrosSistema_Guardar"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Clear()


            '@pIdParametro              NUMERIC(2),
            '@pCodigoParametro          CHAR(20),
            '@pNombreParametro          VARCHAR(50),
            '@pTipoDato                 CHAR(10),
            '@pCaracterDecimal          CHAR(1),
            '@pFormatoFecha             CHAR(10),
            '@pFormatoNumerico          VARCHAR(25),
            '@pEsPorcentajeBase100      CHAR(1),
            '@pCaracteresNoPermitidos   VARCHAR(20),
            '@pCantidadDecimales        CHAR(2),
            '@pValorParametro           VARCHAR(100),
            '@pEstadoParametro          CHAR(3),
            '@pEsRango                  CHAR(1),
            '@pRangoInicial             CHAR(100),
            '@pRangoFinal               CHAR(100),
            '@pEsPersistente            CHAR(1),
            '@pOperacion                CHAR(10),
            '@pResultado                VARCHAR(200) OUTPUT


            SQLCommand.Parameters.Add("pIdParametro", SqlDbType.Int, 10).Value = intIdParametro
            SQLCommand.Parameters.Add("pCodigoParametro", SqlDbType.Char, 20).Value = Trim(strCodigoParametro)
            SQLCommand.Parameters.Add("pNombreParametro", SqlDbType.VarChar, 50).Value = Trim(strNombreParametro)
            SQLCommand.Parameters.Add("pTipoDato", SqlDbType.Char, 10).Value = Trim(strTipoDato)
            SQLCommand.Parameters.Add("pCaracterDecimal", SqlDbType.Char, 1).Value = Trim(strCaracterDecimal)
            SQLCommand.Parameters.Add("pFormatoFecha", SqlDbType.Char, 10).Value = Trim(strFormatoFecha)
            SQLCommand.Parameters.Add("pFormatoNumerico", SqlDbType.VarChar, 25).Value = Trim(strFormatoNumerico)
            SQLCommand.Parameters.Add("pEsPorcentajeBase100", SqlDbType.Char, 1).Value = Trim(strEsPorcentajeBase100)
            SQLCommand.Parameters.Add("pCaracteresNoPermitidos", SqlDbType.VarChar, 20).Value = Trim(strCaracteresNoPermitidos)
            SQLCommand.Parameters.Add("pCantidadDecimales", SqlDbType.Char, 2).Value = Trim(strCantidadDecimales)
            SQLCommand.Parameters.Add("pValorParametro", SqlDbType.VarChar, 100).Value = Trim(strValorParametro)
            SQLCommand.Parameters.Add("pEstadoParametro", SqlDbType.Char, 3).Value = Trim(strEstadoParametro)
            SQLCommand.Parameters.Add("pEsRango", SqlDbType.Char, 1).Value = Trim(strEsRango)
            SQLCommand.Parameters.Add("pRangoInicial", SqlDbType.Char, 100).Value = Trim(strRangoInicial)
            SQLCommand.Parameters.Add("pRangoFinal", SqlDbType.Char, 100).Value = Trim(strRangoFinal)
            SQLCommand.Parameters.Add("pEsPersistente", SqlDbType.Char, 1).Value = Trim(strEsPersistente)
            SQLCommand.Parameters.Add("pOperacion", SqlDbType.VarChar, 10).Value = Trim(strOperacion)

            Dim ParametroSal As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccion.Commit()
            Else
                MiTransaccion.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccion.Rollback()
            strDescError = "Error al grabar de parámetro de sistema." & vbCr & Ex.Message
        Finally
            Connect.Cerrar()
        End Try

    End Sub

    Public Function TraerControles(ByVal intNumeroPantalla As Integer, _
                                   ByRef aOpciones(,) As String) As Boolean

        Dim lDS_PerfilesAcciones As New DataSet
        Dim lDS_PerfilesAcciones_Aux As New DataSet

        Dim Sqlcommand As New SqlClient.SqlCommand
        Dim SqlLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim Lstr_Procedimiento As String = "Sis_PerfilesAcciones_Mantencion"
        Dim Lstr_NombreTabla As String = "PERFILESACCIONES"
        Dim bValRet As Boolean = False

        Connect.Abrir()

        Try

            Sqlcommand = New SqlCommand(Lstr_Procedimiento, Connect.Conexion)

            Sqlcommand.CommandType = CommandType.StoredProcedure
            Sqlcommand.CommandText = Lstr_Procedimiento
            Sqlcommand.Parameters.Clear()

            Sqlcommand.Parameters.Add("@pCodigoOperacion", SqlDbType.Char, 15).Value = "CONTROLES"
            Sqlcommand.Parameters.Add("@pIdUsuario", SqlDbType.Int).Value = glngIdUsuario
            Sqlcommand.Parameters.Add("@pIdNegocio", SqlDbType.Int).Value = glngNegocioOperacion
            Sqlcommand.Parameters.Add("@pNumeroPantalla", SqlDbType.Int).Value = intNumeroPantalla

            Dim oParamSalida As New SqlClient.SqlParameter("@pMsjRetorno", SqlDbType.Char, 60)
            oParamSalida.Direction = ParameterDirection.Output
            Sqlcommand.Parameters.Add(oParamSalida)

            SqlLDataAdapter.SelectCommand = Sqlcommand
            SqlLDataAdapter.Fill(lDS_PerfilesAcciones, Lstr_NombreTabla)

            If Trim(Sqlcommand.Parameters("@pMsjRetorno").Value.ToString) = "OK" Then
                lDS_PerfilesAcciones_Aux = lDS_PerfilesAcciones

                'Dim oDerechoAccion As New ClsPerfilesAcciones
                Dim lintIndice As Integer = 0
                Dim Row As Data.DataRow
                'With oDerechoAccion
                If lDS_PerfilesAcciones_Aux.Tables(0).Rows.Count > 0 Then
                    bValRet = True
                    ReDim aOpciones(lDS_PerfilesAcciones_Aux.Tables(0).Rows.Count - 1, 2)
                    For Each Row In lDS_PerfilesAcciones_Aux.Tables(0).Rows
                        aOpciones(lintIndice, 0) = Row("ID_ACCION")
                        aOpciones(lintIndice, 1) = IIf(IsDBNull(Row("NOMBRE_CONTROL")), "", Row("NOMBRE_CONTROL"))
                        aOpciones(lintIndice, 2) = IIf(IsDBNull(Row("DERECHO_ACCION")), "False", Row("DERECHO_ACCION"))
                        lintIndice += 1
                    Next
                End If
                'End With
                Return bValRet
            End If

        Catch ex As Exception
            Return False
            aOpciones = Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function TraerDiaHabil(ByVal strCodPais As String, _
                                  ByVal strFecha As String, _
                                  ByVal lngCantidadDias As Long, _
                                  ByRef strDescError As String) As String


        Dim DS_Paso As New DataSet
        Dim lstrProcedimiento As String = ""
        Dim LstrNombreTabla As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion

        'Abre la conexion
        lstrProcedimiento = "Rcp_General_BuscarDiaHabil"
        LstrNombreTabla = "PASO"
        Connect.Abrir()

        TraerDiaHabil = ""

        Try

            SQLCommand = New SqlCommand(lstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = lstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pCodPais", SqlDbType.VarChar, 3).Value = IIf(strCodPais = "", DBNull.Value, strCodPais)
            SQLCommand.Parameters.Add("pFecha", SqlDbType.VarChar, 10).Value = IIf(strFecha = "", gstrFechaSistema, strFecha)
            SQLCommand.Parameters.Add("pNumDias", SqlDbType.Int, 4).Value = lngCantidadDias

            Dim ParametroSal1 As New SqlClient.SqlParameter("pFechaHabil", SqlDbType.VarChar, 10)
            ParametroSal1.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal1)

            Dim ParametroSal3 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal3.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal3)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(DS_Paso, LstrNombreTabla)

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                TraerDiaHabil = SQLCommand.Parameters("pFechaHabil").Value.ToString.Trim
            End If

        Catch Ex As Exception
            strDescError = "Error al Obtener el Día Hábil Siguiente." & vbCr & Ex.Message
        Finally
            Connect.Cerrar()
        End Try
        Return TraerDiaHabil
    End Function

    Public Function TraerModoEdicionControles(ByVal strNumeroPantalla As String, _
                                              ByVal strModo As String, _
                                              ByVal strColumnas As String, _
                                              ByRef strRetorno As String) As DataSet

        Dim LDS_TipoMedioPagoCobro As New DataSet
        Dim LDS_TipoMedioPagoCobro_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Sis_ModoEdicion_Consultar"
        Lstr_NombreTabla = "ModoEdicion"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pNumPantalla", SqlDbType.VarChar, 6).Value = IIf(strNumeroPantalla = "", DBNull.Value, strNumeroPantalla)
            SQLCommand.Parameters.Add("pModo", SqlDbType.VarChar, 1).Value = IIf(strNumeroPantalla = "", DBNull.Value, strModo)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_TipoMedioPagoCobro, Lstr_NombreTabla)

            LDS_TipoMedioPagoCobro_Aux = LDS_TipoMedioPagoCobro

            If strColumnas.Trim = "" Then
                LDS_TipoMedioPagoCobro_Aux = LDS_TipoMedioPagoCobro
            Else
                LDS_TipoMedioPagoCobro_Aux = LDS_TipoMedioPagoCobro.Copy
                For Each Columna In LDS_TipoMedioPagoCobro.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_TipoMedioPagoCobro_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_TipoMedioPagoCobro_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            Return LDS_TipoMedioPagoCobro_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function PeriodosBuscar(ByVal strCodTipoPeriodo As String, _
                                   ByVal strColumnas As String, _
                                   ByRef strRetorno As String) As DataSet

        Dim LDS_Periodos As New DataSet
        Dim LDS_Periodos_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_VisorPeriodos_Buscar"
        Lstr_NombreTabla = "PERIODOS"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pTipoPeriodo", SqlDbType.VarChar, 10).Value = strCodTipoPeriodo


            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Periodos, Lstr_NombreTabla)

            LDS_Periodos_Aux = LDS_Periodos

            If strColumnas.Trim = "" Then
                LDS_Periodos_Aux = LDS_Periodos
            Else
                LDS_Periodos_Aux = LDS_Periodos.Copy
                For Each Columna In LDS_Periodos.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Periodos_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Periodos_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            Return LDS_Periodos_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    '........... CONFIGURACION USUARIO ...........'
    Public Function GuardarConfiguracionUsuario(ByVal intCod_Usuario As Integer, _
                                            ByVal strCod_Configuracion As String, _
                                            ByVal strConfiguracion As String) As String

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim Mensaje As String

        'Abre la conexion 
        Connect.Abrir()
        Dim MiTransaccion As SqlClient.SqlTransaction
        MiTransaccion = Connect.Transaccion

        Try

            SQLCommand = New SqlClient.SqlCommand("Sis_ConfigUsuario_Mantencion", MiTransaccion.Connection, MiTransaccion)

            SQLCommand.CommandType = CommandType.StoredProcedure

            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Int, 6).Value = intCod_Usuario
            SQLCommand.Parameters.Add("pCodConfiguracion", SqlDbType.VarChar, 30).Value = strCod_Configuracion
            SQLCommand.Parameters.Add("pConfiguracion", SqlDbType.VarChar, 4000).Value = strConfiguracion

            Dim ParametroSal As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal)

            SQLCommand.ExecuteNonQuery()

            Mensaje = Trim(SQLCommand.Parameters("pResultado").Value.ToString)

            If Trim(Mensaje) = "OK" Then
                MiTransaccion.Commit()
                Return Mensaje
            Else
                MiTransaccion.Rollback()
                Return Mensaje
            End If

        Catch Ex As Exception
            MiTransaccion.Rollback()
            Mensaje = "Error en el Servicio de Grabación de Configuración Datos Usuario." & vbCr & Ex.Message
            Return Mensaje
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function RecuperaConfiguracionUsuario(ByVal intCod_Usuario As Integer, _
                                            ByVal strCod_Configuracion As String, _
                                            ByRef strConfiguracion As String) As String

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim Mensaje As String

        'Abre la conexion 
        Connect.Abrir()
        Dim MiTransaccion As SqlClient.SqlTransaction
        MiTransaccion = Connect.Transaccion


        Try

            SQLCommand = New SqlClient.SqlCommand("Sis_ConfigUsuario_Ver", MiTransaccion.Connection, MiTransaccion)

            SQLCommand.CommandType = CommandType.StoredProcedure

            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Int, 6).Value = intCod_Usuario
            SQLCommand.Parameters.Add("pCodConfiguracion", SqlDbType.VarChar, 30).Value = strCod_Configuracion

            Dim ParametroSal_1 As New SqlClient.SqlParameter("pConfiguracion", SqlDbType.VarChar, 4000)
            ParametroSal_1.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal_1)

            Dim ParametroSal_2 As New SqlClient.SqlParameter("pMsjRetorno", SqlDbType.VarChar, 200)
            ParametroSal_2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal_2)

            SQLCommand.ExecuteNonQuery()

            strConfiguracion = Trim(SQLCommand.Parameters("pConfiguracion").Value.ToString)
            Mensaje = Trim(SQLCommand.Parameters("pMsjRetorno").Value.ToString)

            If Trim(Mensaje) = "OK" Then
                MiTransaccion.Commit()
                Return Mensaje
            Else
                MiTransaccion.Rollback()
                Return Mensaje
            End If

        Catch Ex As Exception
            MiTransaccion.Rollback()
            Mensaje = "Error en el Servicio de Grabación de Configuración Datos Usuario." & vbCr & Ex.Message
            Return Mensaje
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function BuscarDiasRetencion(ByVal strCodPais As String, _
                                  ByVal strFechaDesde As String, _
                                  ByVal strFechaHasta As String, _
                                  ByRef strDescError As String) As Integer


        Dim lstrProcedimiento As String = ""
        Dim LstrNombreTabla As String = ""
        Dim DS_Paso As New DataSet
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion

        lstrProcedimiento = "Rcp_General_BuscarDiasRetencion"
        LstrNombreTabla = "ACCIONES"
        'Abre la conexion
        Connect.Abrir()

        Dim intNunDias As Integer

        intNunDias = 0

        Try
            SQLCommand = New SqlCommand(lstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = lstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pCodPais", SqlDbType.VarChar, 3).Value = IIf(strCodPais = "", DBNull.Value, strCodPais)
            SQLCommand.Parameters.Add("pFechaDesde", SqlDbType.VarChar, 10).Value = IIf(strFechaDesde = "", gstrFechaSistema, strFechaDesde)
            SQLCommand.Parameters.Add("pFechaHasta", SqlDbType.VarChar, 10).Value = IIf(strFechaHasta = "", gstrFechaSistema, strFechaHasta)

            Dim ParametroSal1 As New SqlClient.SqlParameter("pDiasRetencion", SqlDbType.Float, 4)
            ParametroSal1.Direction = ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal1)

            Dim ParametroSal3 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal3.Direction = ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal3)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(DS_Paso, LstrNombreTabla)

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                intNunDias = SQLCommand.Parameters("pDiasRetencion").Value.ToString.Trim
            End If

        Catch Ex As Exception
            strDescError = "Error al Obtener el Día Hábil Siguiente." & vbCr & Ex.Message
        Finally
            Connect.Cerrar()
        End Try
        Return intNunDias
    End Function

    Public Function TraerDatosObligatorios(ByVal intNumeroPantalla As Integer, _
                                           ByRef aDatos(,) As String) As Boolean

        Dim lDS_DatosObligatorios As New DataSet
        Dim lDS_DatosObligatorios_Aux As New DataSet

        Dim Sqlcommand As New SqlClient.SqlCommand
        Dim SqlLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim Lstr_Procedimiento As String = "Sis_DatosObligatorios_Consultar"
        Dim Lstr_NombreTabla As String = "DATOS_OBLIGATORIOS"
        Dim bValRet As Boolean = False

        Connect.Abrir()

        Try

            Sqlcommand = New SqlCommand(Lstr_Procedimiento, Connect.Conexion)

            Sqlcommand.CommandType = CommandType.StoredProcedure
            Sqlcommand.CommandText = Lstr_Procedimiento
            Sqlcommand.Parameters.Clear()

            Sqlcommand.Parameters.Add("@pIdPerfilUsuario", SqlDbType.Int).Value = 0
            Sqlcommand.Parameters.Add("@pIdUsuario", SqlDbType.Int).Value = glngIdUsuario
            Sqlcommand.Parameters.Add("@pIdNegocio", SqlDbType.Int).Value = glngNegocioOperacion
            Sqlcommand.Parameters.Add("@pNumeroPantalla", SqlDbType.Int).Value = intNumeroPantalla

            Dim oParamSalida As New SqlClient.SqlParameter("@pMsjRetorno", SqlDbType.Char, 60)
            oParamSalida.Direction = ParameterDirection.Output
            Sqlcommand.Parameters.Add(oParamSalida)

            SqlLDataAdapter.SelectCommand = Sqlcommand
            SqlLDataAdapter.Fill(lDS_DatosObligatorios, Lstr_NombreTabla)

            If Trim(Sqlcommand.Parameters("@pMsjRetorno").Value.ToString) = "OK" Then
                lDS_DatosObligatorios_Aux = lDS_DatosObligatorios

                'Dim oDerechoAccion As New ClsPerfilesAcciones
                Dim lintIndice As Integer = 0
                Dim Row As Data.DataRow
                'With oDerechoAccion
                If lDS_DatosObligatorios_Aux.Tables(0).Rows.Count > 0 Then
                    bValRet = True
                    ReDim aDatos(lDS_DatosObligatorios_Aux.Tables(0).Rows.Count - 1, 0)
                    For Each Row In lDS_DatosObligatorios_Aux.Tables(0).Rows
                        aDatos(lintIndice, 0) = Row("NOMBRE")
                        lintIndice += 1
                    Next
                End If
                'End With
                Return bValRet
            End If

        Catch ex As Exception
            Return False
            aDatos = Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function Ingresos_Ver(ByVal strColumnas As String, _
                                 ByRef strRetorno As String) As DataSet

        Dim LDS_Ingresos As New DataSet
        Dim LDS_Ingresos_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Ingresos_Buscar"
        Lstr_NombreTabla = "Ingresos"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("@pIdUsuario", SqlDbType.Int).Value = glngIdUsuario
            SQLCommand.Parameters.Add("@pIdNegocio", SqlDbType.Int).Value = glngNegocioOperacion

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Ingresos, Lstr_NombreTabla)

            LDS_Ingresos_Aux = LDS_Ingresos

            If strColumnas.Trim = "" Then
                LDS_Ingresos_Aux = LDS_Ingresos
            Else
                LDS_Ingresos_Aux = LDS_Ingresos.Copy
                For Each Columna In LDS_Ingresos.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Ingresos_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Ingresos_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            Return LDS_Ingresos_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function TraerDiaHabil_Anterior(ByVal strCodPais As String, _
                              ByVal strFecha As String, _
                              ByVal lngCantidadDias As Long, _
                              ByRef strDescError As String) As String


        Dim DS_Paso As New DataSet
        Dim lstrProcedimiento As String = ""
        Dim LstrNombreTabla As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion

        'Abre la conexion
        lstrProcedimiento = "Rcp_General_BuscarDiaHabi_Anterior"
        LstrNombreTabla = "PASO"
        Connect.Abrir()

        TraerDiaHabil_Anterior = ""

        Try

            SQLCommand = New SqlCommand(lstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = lstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pCodPais", SqlDbType.VarChar, 3).Value = IIf(strCodPais = "", DBNull.Value, strCodPais)
            SQLCommand.Parameters.Add("pFecha", SqlDbType.VarChar, 10).Value = IIf(strFecha = "", gstrFechaSistema, strFecha)
            SQLCommand.Parameters.Add("pNumDias", SqlDbType.Int, 4).Value = lngCantidadDias

            Dim ParametroSal1 As New SqlClient.SqlParameter("pFechaHabil", SqlDbType.VarChar, 10)
            ParametroSal1.Direction = ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal1)

            Dim ParametroSal3 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal3.Direction = ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal3)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(DS_Paso, LstrNombreTabla)

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                TraerDiaHabil_Anterior = SQLCommand.Parameters("pFechaHabil").Value.ToString.Trim
            End If

        Catch Ex As Exception
            strDescError = "Error al Obtener el Día Hábil Siguiente." & vbCr & Ex.Message
        Finally
            Connect.Cerrar()
        End Try
        Return TraerDiaHabil_Anterior
    End Function

    Public Function BuscarSaltosCuotas(ByVal strFechaDesde As String, _
                                       ByVal strFechaHasta As String, _
                                       ByVal dblPorcStoCta As Double, _
                                       ByRef strDescError As String) As DataSet

        Dim lstrProcedimiento As String = "Rcp_Rpt_SaltoCuota"
        Dim LstrNombreTabla As String = "Saltos"
        Dim DS_Paso As New DataSet
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion

        'Abre la conexion
        Connect.Abrir()

        Try
            SQLCommand = New SqlCommand(lstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = lstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("PFechaInicio", SqlDbType.VarChar, 10).Value = IIf(strFechaDesde = "", gstrFechaSistema, strFechaDesde)
            SQLCommand.Parameters.Add("PFechaFin", SqlDbType.VarChar, 10).Value = IIf(strFechaHasta = "", gstrFechaSistema, strFechaHasta)
            SQLCommand.Parameters.Add("PVariacion", SqlDbType.Float, 3).Value = IIf(dblPorcStoCta = 0, DBNull.Value, dblPorcStoCta)

            Dim ParametroSal As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal.Direction = ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(DS_Paso, LstrNombreTabla)

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

        Catch Ex As Exception
            strDescError = "Error al Obtener el Día Hábil Siguiente." & vbCr & Ex.Message
        Finally
            Connect.Cerrar()
        End Try
        Return DS_Paso
    End Function

End Class
