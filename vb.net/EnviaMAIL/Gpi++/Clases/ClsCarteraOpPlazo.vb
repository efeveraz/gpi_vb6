﻿Public Class ClsCarteraOpPlazo

    Public Function CarteraOpPlazo_Mantenedor(ByVal strAccion As String, ByVal dblIdOpPLazo As Double, ByVal strNumeroOpPlazo As String, _
                                              ByVal strGlosa As String, ByVal dblFolioDocAdj As Double, ByVal strCodEjecutivo As String, _
                                              ByVal strEstadoOpPlazo As String, ByVal strFechaOpPlazo As String, ByVal strFechaVctoOpPlazo As String, _
                                              ByVal strFechaLiqOpPlazo As String, ByVal dblMontoCapital As Double, ByVal dblMontoPlazo As Double, ByVal dblMontoCapitalAjustado As Double, _
                                              ByVal dblPlazoDias As Double, ByVal dblSpread As Double, ByVal dblBaseDiasTasaOpPlazo As Double, _
                                              ByVal dblValorPresenteOpPlazo As Double, ByVal dblPrecioContOpPlazo As Double, ByVal dblPrecioPlazOpPlazo As Double, _
                                              ByVal dblPorcentajeGtiaOpPlazo As Double, ByVal dblTipoOpContado As Double, ByVal strCodTipoOperacion As String, _
                                              ByVal dblIdTipoCambio As Double, ByVal dblIdTipoCambioCompensacion As Double, ByVal strCodMonedaOrigen As String, ByVal strCodMonedaDestino As String, _
                                              ByVal strTipoLiquidacion As String, ByVal dblTasa As Double, ByVal dblIdContraparte As Double, ByVal dblIdCuenta As Double, _
                                              ByVal dblIdCustodio As Double, ByVal dblIdEmisor As Double, ByVal strNemotecnico As String, ByVal dblCapitalInicial As Double, _
                                              ByVal dblMontoOperacion As Double, ByVal dblIdTrader As Double, _
                                              ByVal strFlgTipoMovimiento As String, ByVal strFechaEmiOpPlazo As String, ByVal gdteFechaSistema As DateTime, ByVal glngIdUsuario As Double, _
                                              ByVal dblCantidad As Double, ByVal strSubClaseInstrumento As String, ByVal strCodMonedaOperacion As String, ByRef strDescError As String) As String

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_Cartera_Op_Plazo_Mantenedor", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Cartera_Op_Plazo_Mantenedor"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = strAccion
            SQLCommand.Parameters.Add("pIdOpPlazo", SqlDbType.Float, 18).Value = dblIdOpPLazo
            SQLCommand.Parameters.Add("pNumeroOpPlazo", SqlDbType.VarChar, 20).Value = IIf(strNumeroOpPlazo = "", DBNull.Value, strNumeroOpPlazo)
            SQLCommand.Parameters.Add("pGlosa", SqlDbType.VarChar, 50).Value = IIf(strGlosa = "", DBNull.Value, strGlosa)
            SQLCommand.Parameters.Add("pFolioDocAdj", SqlDbType.Float, 9).Value = IIf(dblFolioDocAdj = 0, DBNull.Value, dblFolioDocAdj)
            SQLCommand.Parameters.Add("pCodEjecutivo", SqlDbType.VarChar, 10).Value = IIf(strCodEjecutivo = "", DBNull.Value, strCodEjecutivo)
            SQLCommand.Parameters.Add("pEstadoOpPlazo", SqlDbType.VarChar, 3).Value = IIf(strEstadoOpPlazo = "", DBNull.Value, strEstadoOpPlazo)
            SQLCommand.Parameters.Add("pFechaOpPlazo", SqlDbType.VarChar, 10).Value = IIf(strFechaOpPlazo = "", DBNull.Value, strFechaOpPlazo)
            SQLCommand.Parameters.Add("pFechaVctoOpPlazo", SqlDbType.VarChar, 10).Value = IIf(strFechaVctoOpPlazo = "", DBNull.Value, strFechaVctoOpPlazo)
            SQLCommand.Parameters.Add("pFechaLiqOpPlazo", SqlDbType.VarChar, 10).Value = IIf(strFechaLiqOpPlazo = "", DBNull.Value, strFechaLiqOpPlazo)
            SQLCommand.Parameters.Add("pMontoCapital", SqlDbType.Float, 13).Value = IIf(dblMontoCapital = 0, DBNull.Value, dblMontoCapital)
            SQLCommand.Parameters.Add("pMontoPlazo", SqlDbType.Float, 13).Value = IIf(dblMontoPlazo = 0, DBNull.Value, dblMontoPlazo)
            SQLCommand.Parameters.Add("pMontoCapitalAjustado", SqlDbType.Float, 13).Value = IIf(dblMontoCapitalAjustado = 0, DBNull.Value, dblMontoCapitalAjustado)
            SQLCommand.Parameters.Add("pPlazoDias", SqlDbType.Float, 9).Value = IIf(dblPlazoDias = 0, DBNull.Value, dblPlazoDias)
            SQLCommand.Parameters.Add("pSpread", SqlDbType.Float, 13).Value = IIf(dblSpread = 0, DBNull.Value, dblSpread)
            SQLCommand.Parameters.Add("pBaseDiasTasaOpPlazo", SqlDbType.Float, 9).Value = IIf(dblBaseDiasTasaOpPlazo = 0, DBNull.Value, dblBaseDiasTasaOpPlazo)
            SQLCommand.Parameters.Add("pValorPresenteOpPlazo", SqlDbType.Float, 13).Value = IIf(dblValorPresenteOpPlazo = 0, DBNull.Value, dblValorPresenteOpPlazo)
            SQLCommand.Parameters.Add("pPrecioContOpPlazo", SqlDbType.Float, 13).Value = IIf(dblPrecioContOpPlazo = 0, DBNull.Value, dblPrecioContOpPlazo)
            SQLCommand.Parameters.Add("pPrecioPlazOpPlazo", SqlDbType.Float, 13).Value = IIf(dblPrecioPlazOpPlazo = 0, DBNull.Value, dblPrecioPlazOpPlazo)
            SQLCommand.Parameters.Add("pPorcentajeGtiaOpPlazo", SqlDbType.Float, 13).Value = IIf(dblPorcentajeGtiaOpPlazo = 0, DBNull.Value, dblPorcentajeGtiaOpPlazo)
            SQLCommand.Parameters.Add("pTipoOpContado", SqlDbType.Float, 9).Value = dblTipoOpContado
            SQLCommand.Parameters.Add("pCodTipoOperacion", SqlDbType.VarChar, 10).Value = IIf(strCodTipoOperacion = "", DBNull.Value, strCodTipoOperacion)
            SQLCommand.Parameters.Add("pIdTipoCambio", SqlDbType.Float, 9).Value = IIf(dblIdTipoCambio = 0, DBNull.Value, dblIdTipoCambio)
            SQLCommand.Parameters.Add("pIdTipoCambioCompensacion", SqlDbType.Float, 9).Value = IIf(dblIdTipoCambioCompensacion = 0, DBNull.Value, dblIdTipoCambioCompensacion)
            SQLCommand.Parameters.Add("pCodMonedaOrigen", SqlDbType.VarChar, 3).Value = IIf(strCodMonedaOrigen = "", DBNull.Value, strCodMonedaOrigen)
            SQLCommand.Parameters.Add("pCodMonedaDestino", SqlDbType.VarChar, 3).Value = IIf(strCodMonedaDestino = "", DBNull.Value, strCodMonedaDestino)
            SQLCommand.Parameters.Add("pTipoLiquidacion", SqlDbType.Char, 3).Value = IIf(strTipoLiquidacion = "", DBNull.Value, strTipoLiquidacion)
            SQLCommand.Parameters.Add("pTasa", SqlDbType.Float, 13).Value = IIf(dblTasa = 0, DBNull.Value, dblTasa)
            SQLCommand.Parameters.Add("pIdContraparte", SqlDbType.Float, 10).Value = IIf(dblIdContraparte = 0, DBNull.Value, dblIdContraparte)
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = IIf(dblIdCuenta = 0, DBNull.Value, dblIdCuenta)
            SQLCommand.Parameters.Add("pIdCustodio", SqlDbType.Float, 10).Value = IIf(dblIdCustodio = 0, DBNull.Value, dblIdCustodio)
            SQLCommand.Parameters.Add("pIdEmisor", SqlDbType.Float, 10).Value = IIf(dblIdEmisor = 0, DBNull.Value, dblIdEmisor)
            SQLCommand.Parameters.Add("pNemotecnico", SqlDbType.VarChar, 50).Value = IIf(strNemotecnico = "", DBNull.Value, strNemotecnico)
            SQLCommand.Parameters.Add("pCapitalInicial", SqlDbType.Float, 18).Value = IIf(dblCapitalInicial = 0, DBNull.Value, dblCapitalInicial)
            SQLCommand.Parameters.Add("pMontoOperacion", SqlDbType.Float, 18).Value = IIf(dblMontoOperacion = 0, DBNull.Value, dblMontoOperacion)
            SQLCommand.Parameters.Add("pIdTrader", SqlDbType.Float, 18).Value = IIf(dblIdTrader = 0, DBNull.Value, dblIdTrader)
            SQLCommand.Parameters.Add("pFlgTipoMovimiento", SqlDbType.VarChar, 1).Value = IIf(strFlgTipoMovimiento = "", DBNull.Value, strFlgTipoMovimiento)
            SQLCommand.Parameters.Add("pFechaEmiOpPlazo", SqlDbType.VarChar, 10).Value = IIf(strFechaEmiOpPlazo = "", DBNull.Value, strFechaEmiOpPlazo)
            SQLCommand.Parameters.Add("pInsertFch", SqlDbType.VarChar, 10).Value = gdteFechaSistema
            SQLCommand.Parameters.Add("pInsertUsuario", SqlDbType.Float, 6).Value = IIf(glngIdUsuario = 0, DBNull.Value, glngIdUsuario)
            SQLCommand.Parameters.Add("pSubClaseInstrumento", SqlDbType.VarChar, 10).Value = strSubClaseInstrumento
            SQLCommand.Parameters.Add("pCantidad", SqlDbType.Float, 13).Value = IIf(dblCantidad = 0, DBNull.Value, dblCantidad)
            SQLCommand.Parameters.Add("pCodMonedaOperacion", SqlDbType.VarChar, 3).Value = IIf(strCodMonedaOperacion = "", DBNull.Value, strCodMonedaOperacion)


            '...(Identity)
            Dim pSalInstr As New SqlClient.SqlParameter("pGenerado", SqlDbType.Float, 10)
            pSalInstr.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalInstr)

            '...Resultado
            Dim pSalResultInstr As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultInstr.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultInstr)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()

            End If
            '___________________________________________________________________________________________________________

            'If strDescError = "OK" Then
            '  If gobjParametro.Tesoreria_Envio.ValorParametro = "S" Then
            '    Dim lcInterfaceTesoreria As New clsInterfaceTesoreria
            '    lcInterfaceTesoreria.InformarIngresoOpPlazo(SQLCommand.Parameters("pGenerado").Value.ToString.Trim, "FWDCONT")
            '  End If
            'End If
        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error: " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            CarteraOpPlazo_Mantenedor = strDescError
        End Try
    End Function


    Public Function Operaciones_APlazo_Ver(ByVal dblpIdOperacion As Double, _
                                           ByVal strpFechaDesde As String, _
                                           ByVal strpFechaHasta As String, _
                                           ByVal strpTipoOperacion As String, _
                                           ByVal strpColumnas As String, _
                                           ByRef strpRetorno As String, _
                                           Optional ByRef strCodEstado As String = "", _
                                           Optional ByVal strCuenta As Integer = 0) As DataSet

        Dim lDS_Operaciones As New DataSet
        Dim lDS_Operaciones_Aux As New DataSet
        Dim larr_NombreColumna() As String = Split(strpColumnas, ",")
        Dim lInt_Col As Integer = 0
        Dim lInt_NomCol As String = ""
        Dim lstr_NombreTabla As String = ""
        Dim lstrColumna As DataColumn
        Dim lblnRemove As Boolean = True
        Dim lstrProcedimiento As String = ""
        Dim strDescError As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        lstrProcedimiento = "Rcp_Operacion_APlazo_Consultar"
        lstr_NombreTabla = "OPERACIONES_PLAZO"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = lstrProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("pIdOperacion", SqlDbType.Float, 10).Value = IIf(dblpIdOperacion = 0, DBNull.Value, dblpIdOperacion)
            SQLCommand.Parameters.Add("pFechaDesde", SqlDbType.Char, 10).Value = strpFechaDesde
            SQLCommand.Parameters.Add("pFechaHasta", SqlDbType.Char, 10).Value = strpFechaHasta
            SQLCommand.Parameters.Add("pTipoOperacion", SqlDbType.Char, 20).Value = IIf(strpTipoOperacion = "", DBNull.Value, strpTipoOperacion)
            SQLCommand.Parameters.Add("pCodEstado", SqlDbType.Char, 10).Value = IIf(strCodEstado = "", DBNull.Value, strCodEstado)
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Float, 10).Value = glngNegocioOperacion
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Float, 6).Value = IIf(glngIdUsuario = 0, DBNull.Value, glngIdUsuario)
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Int, 6).Value = IIf(strCuenta = 0, DBNull.Value, strCuenta)


            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()
            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(lDS_Operaciones, lstr_NombreTabla)

            lDS_Operaciones_Aux = lDS_Operaciones

            If strDescError.ToUpper.Trim <> "OK" Then
                gFun_ResultadoMsg("2|" & strDescError, "Buscar Cuentas")
                strpRetorno = "OK"
                lDS_Operaciones_Aux = Nothing
            Else
                If strpColumnas.Trim = "" Then
                    lDS_Operaciones_Aux = lDS_Operaciones
                Else
                    lDS_Operaciones_Aux = lDS_Operaciones.Copy

                    For Each lstrColumna In lDS_Operaciones.Tables(lstr_NombreTabla).Columns
                        lblnRemove = True
                        For lInt_Col = 0 To larr_NombreColumna.Length - 1
                            lInt_NomCol = larr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                            If lstrColumna.ColumnName = lInt_NomCol Then
                                lblnRemove = False
                            End If
                        Next
                        If lblnRemove Then
                            lDS_Operaciones_Aux.Tables(lstr_NombreTabla).Columns.Remove(lstrColumna.ColumnName)
                        End If
                    Next
                End If

                For lInt_Col = 0 To larr_NombreColumna.Length - 1
                    lInt_NomCol = larr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                    For Each lstrColumna In lDS_Operaciones_Aux.Tables(lstr_NombreTabla).Columns
                        If lstrColumna.ColumnName = lInt_NomCol Then
                            lstrColumna.SetOrdinal(lInt_Col)
                            Exit For
                        End If
                    Next
                Next

                strpRetorno = "OK"
            End If
            lDS_Operaciones.Dispose()
            Return lDS_Operaciones_Aux

        Catch ex As Exception
            strpRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function
    Public Function OperacionPlazo_BuscarEncabezado(ByVal dblIdOperacionPlazo As Double, _
                                                    ByVal strColumnas As String, _
                                                    ByRef strRetorno As String) As DataSet

        Dim lDS_Operaciones As New DataSet
        Dim lDS_Operaciones_Aux As New DataSet
        Dim larr_NombreColumna() As String = Split(strColumnas, ",")
        Dim lInt_Col As Integer = 0
        Dim lInt_NomCol As String = ""
        Dim lstr_NombreTabla As String = ""
        Dim lstrColumna As DataColumn
        Dim lblnRemove As Boolean = True
        Dim lstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        lstrProcedimiento = "Rcp_OperacionAPlazo_BuscarEncabezado"
        lstr_NombreTabla = "OPERACION_PLAZO"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = lstrProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("pIdOperacionPlazo", SqlDbType.Float, 18).Value = dblIdOperacionPlazo

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(lDS_Operaciones, lstr_NombreTabla)

            lDS_Operaciones_Aux = lDS_Operaciones

            If strColumnas.Trim = "" Then
                lDS_Operaciones_Aux = lDS_Operaciones
            Else
                lDS_Operaciones_Aux = lDS_Operaciones.Copy
                For Each lstrColumna In lDS_Operaciones.Tables(lstr_NombreTabla).Columns
                    lblnRemove = True
                    For lInt_Col = 0 To larr_NombreColumna.Length - 1
                        lInt_NomCol = larr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                        If lstrColumna.ColumnName = lInt_NomCol Then
                            lblnRemove = False
                        End If
                    Next
                    If lblnRemove Then
                        lDS_Operaciones_Aux.Tables(lstr_NombreTabla).Columns.Remove(lstrColumna.ColumnName)
                    End If
                Next
            End If

            For lInt_Col = 0 To larr_NombreColumna.Length - 1
                lInt_NomCol = larr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                For Each lstrColumna In lDS_Operaciones_Aux.Tables(lstr_NombreTabla).Columns
                    If lstrColumna.ColumnName = lInt_NomCol Then
                        lstrColumna.SetOrdinal(lInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            lDS_Operaciones.Dispose()
            Return lDS_Operaciones_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function
    Public Function AnularOperacionPlazo(ByVal llngIdOperacionPlazo As Double, _
                                         ByVal strEstadoOpPlazo As String) As String
        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_Anular_Operaciones_Plazo", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Anular_Operaciones_Plazo"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdOperacionPlazo", SqlDbType.Float, 10).Value = llngIdOperacionPlazo
            SQLCommand.Parameters.Add("pEstadoOpPlazo", SqlDbType.VarChar, 3).Value = strEstadoOpPlazo
            '...Resultado
            Dim ParametroSal As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal)
            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

        Catch Ex As Exception
            strDescError = "|1|Error al Anular la operación." & vbCr & "[" & Ex.Message & "]|"
        Finally
            If strDescError = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                If gFun_ResultadoMsg(strDescError, "", True) Then
                    MiTransaccionSQL.Commit()
                Else
                    MiTransaccionSQL.Rollback()
                End If
            End If
            SQLConnect.Cerrar()
            AnularOperacionPlazo = strDescError
        End Try
    End Function
End Class
