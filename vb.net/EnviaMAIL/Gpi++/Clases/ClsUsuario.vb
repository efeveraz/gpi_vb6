﻿Imports System.Data.SqlClient

Public Class ClsUsuario
    Private lstrPasswordNueva As String = ""

    Public Structure TAccesos
        Public strIdAccion As String
        Public strDerechoAccion As String
    End Structure

    Public Overloads Function Insertar(ByRef strRetorno As String,
                                       ByRef lngIdUsuario As Long,
                                       ByVal lngIdTipoUsuario As Long,
                                       ByVal lngIdUnidad As Long,
                                       ByVal strNombreUsuario As String,
                                       ByVal strNombreCortoUsuario As String,
                                       ByVal strCodigoExternoUsuario As String,
                                       ByVal strCargoUsuario As String,
                                       ByVal strTelefonoUsuario As String,
                                       ByVal strLoginUsuario As String,
                                       ByVal strClaveUsuario As String,
                                       ByVal strEstadoUsuario As String,
                                       ByVal datFechaVigencia As String,
                                       ByVal blnTodasCuentas As Boolean,
                                       ByVal lngIdTipoControl As Long,
                                       emailUsuario As String) As DataSet
        Dim lDS_Usuario As New DataSet
        Dim lDS_Usuario_Aux As New DataSet

        Dim Sqlcommand As New SqlCommand
        Dim SQLDataAdapter As New SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim Lstr_Procedimiento As String = ""
        Dim Lstr_NombreTabla As String = ""

        'Lstr_Procedimiento = "SGC_SP_MANTENCION_USUARIO"
        Lstr_Procedimiento = "Sis_Usuario_Mantencion"
        Lstr_NombreTabla = "USUARIO"
        Connect.Abrir()

        Try
            Sqlcommand = New SqlCommand(Lstr_Procedimiento, Connect.Conexion)

            Sqlcommand.CommandType = CommandType.StoredProcedure
            Sqlcommand.CommandText = Lstr_Procedimiento
            Sqlcommand.Parameters.Clear()

            Sqlcommand.Parameters.Add("@pCodigoOperacion", SqlDbType.Char, 10).Value = "INSERTAR"
            'IdUsuario...(Identity)
            Dim ParametroIdUsuario As New SqlParameter("@pIdUsuario", SqlDbType.Int) With {
                .Direction = ParameterDirection.InputOutput
            }
            Sqlcommand.Parameters.Add(ParametroIdUsuario)
            Sqlcommand.Parameters.Add("@pIdTipoUsuario", SqlDbType.Int).Value = IIf(lngIdTipoUsuario = 0, DBNull.Value, lngIdTipoUsuario)
            Sqlcommand.Parameters.Add("@pIdUnidad", SqlDbType.Int).Value = IIf(lngIdUnidad = 0, DBNull.Value, lngIdUnidad)
            Sqlcommand.Parameters.Add("@pNombreUsuario", SqlDbType.Char, 50).Value = IIf(strNombreUsuario = "", DBNull.Value, strNombreUsuario)
            Sqlcommand.Parameters.Add("@pIdNegocio", SqlDbType.Int).Value = glngNegocioOperacion
            Sqlcommand.Parameters.Add("@pNombreCortoUsuario", SqlDbType.Char, 15).Value = IIf(strNombreCortoUsuario = "", DBNull.Value, strNombreCortoUsuario)
            Sqlcommand.Parameters.Add("@pCodigoExternoUsuario", SqlDbType.Char, 20).Value = IIf(strCodigoExternoUsuario = "", DBNull.Value, strCodigoExternoUsuario)
            Sqlcommand.Parameters.Add("@pCargoUsuario", SqlDbType.Char, 30).Value = IIf(strCargoUsuario = "", DBNull.Value, strCargoUsuario)
            Sqlcommand.Parameters.Add("@pTelefonoUsuario", SqlDbType.Char, 30).Value = IIf(strTelefonoUsuario = "", DBNull.Value, strTelefonoUsuario)
            Sqlcommand.Parameters.Add("@pLoginUsuario", SqlDbType.Char, 15).Value = IIf(strLoginUsuario = "", DBNull.Value, strLoginUsuario)
            Sqlcommand.Parameters.Add("@pClaveUsuario", SqlDbType.Char, 200).Value = IIf(strClaveUsuario = "", DBNull.Value, strClaveUsuario)
            Sqlcommand.Parameters.Add("@pEstadoUsuario", SqlDbType.Char, 200).Value = IIf(strEstadoUsuario = "", DBNull.Value, strEstadoUsuario)
            Sqlcommand.Parameters.Add("@pFechaVigencia", SqlDbType.Char, 10).Value = IIf(datFechaVigencia.Trim = "", DBNull.Value, datFechaVigencia.Trim)
            Sqlcommand.Parameters.Add("@pTodasCuentas", SqlDbType.Bit).Value = IIf(blnTodasCuentas, 1, 0)
            Sqlcommand.Parameters.Add("@pIdTipoControl", SqlDbType.Int).Value = IIf(lngIdTipoControl = 0, DBNull.Value, lngIdTipoControl)
            Sqlcommand.Parameters.Add("@pEmailUsuario", SqlDbType.VarChar, 50).Value = emailUsuario

            Dim oParamSal As New SqlParameter("@pMsjRetorno", SqlDbType.Char, 60) With {
                .Direction = ParameterDirection.Output
            }
            Sqlcommand.Parameters.Add(oParamSal)


            SQLDataAdapter.SelectCommand = Sqlcommand
            SQLDataAdapter.Fill(lDS_Usuario, Lstr_NombreTabla)

            'strRetorno = "OK"
            strRetorno = Sqlcommand.Parameters("@pMsjRetorno").Value.trim

            lDS_Usuario_Aux = lDS_Usuario
            lngIdUsuario = Sqlcommand.Parameters("@pIdUsuario").Value

            Return lDS_Usuario_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function Modificar(ByRef strRetorno As String,
                              ByRef lngIdUsuario As Long,
                              ByVal lngIdTipoUsuario As Long,
                              ByVal lngIdUnidad As Long,
                              ByVal strNombreUsuario As String,
                              ByVal strNombreCortoUsuario As String,
                              ByVal strCodigoExternoUsuario As String,
                              ByVal strCargoUsuario As String,
                              ByVal strTelefonoUsuario As String,
                              ByVal strLoginUsuario As String,
                              ByVal strClaveUsuario As String,
                              ByVal strEstadoUsuario As String,
                              ByVal datFechaVigencia As String,
                              ByVal blnTodasCuentas As Boolean,
                              ByVal lngIdTipoControl As Long,
                              emailUsuario As String) As DataSet

        Dim LDS_Usuario As New DataSet
        Dim LDS_Usuario_Aux As New DataSet

        Dim Sqlcommand As New SqlCommand
        Dim SQLDataAdapter As New SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim Lstr_Procedimiento As String = ""
        Dim Lstr_NombreTabla As String = ""

        'Lstr_Procedimiento = "SGC_SP_MANTENCION_USUARIO"
        Lstr_Procedimiento = "Sis_Usuario_Mantencion"
        Lstr_NombreTabla = "USUARIO"
        Connect.Abrir()

        Try
            Sqlcommand = New SqlCommand(Lstr_Procedimiento, Connect.Conexion)

            Sqlcommand.CommandType = CommandType.StoredProcedure
            Sqlcommand.CommandText = Lstr_Procedimiento
            Sqlcommand.Parameters.Clear()

            Sqlcommand.Parameters.Add("@pCodigoOperacion", SqlDbType.Char, 10).Value = "MODIFICAR"
            Sqlcommand.Parameters.Add("@pIdUsuario", SqlDbType.Int).Value = lngIdUsuario
            Sqlcommand.Parameters.Add("@pIdNegocio", SqlDbType.Int).Value = glngNegocioOperacion
            Sqlcommand.Parameters.Add("@pIdTipoUsuario", SqlDbType.Int).Value = IIf(lngIdTipoUsuario = 0, DBNull.Value, lngIdTipoUsuario)
            Sqlcommand.Parameters.Add("@pIdUnidad", SqlDbType.Int).Value = IIf(lngIdUnidad = 0, DBNull.Value, lngIdUnidad)
            Sqlcommand.Parameters.Add("@pNombreUsuario", SqlDbType.Char, 50).Value = IIf(strNombreUsuario = "", DBNull.Value, strNombreUsuario)
            Sqlcommand.Parameters.Add("@pNombreCortoUsuario", SqlDbType.Char, 15).Value = IIf(strNombreCortoUsuario = "", DBNull.Value, strNombreCortoUsuario)
            Sqlcommand.Parameters.Add("@pCodigoExternoUsuario", SqlDbType.Char, 20).Value = IIf(strCodigoExternoUsuario = "", DBNull.Value, strCodigoExternoUsuario)
            Sqlcommand.Parameters.Add("@pCargoUsuario", SqlDbType.Char, 30).Value = IIf(strCargoUsuario = "", DBNull.Value, strCargoUsuario)
            Sqlcommand.Parameters.Add("@pTelefonoUsuario", SqlDbType.Char, 30).Value = IIf(strTelefonoUsuario = "", DBNull.Value, strTelefonoUsuario)
            Sqlcommand.Parameters.Add("@pLoginUsuario", SqlDbType.Char, 15).Value = IIf(strLoginUsuario = "", DBNull.Value, strLoginUsuario)
            Sqlcommand.Parameters.Add("@pClaveUsuario", SqlDbType.Char, 200).Value = IIf(strClaveUsuario = "", DBNull.Value, strClaveUsuario)
            Sqlcommand.Parameters.Add("@pEstadoUsuario", SqlDbType.Char, 200).Value = IIf(strEstadoUsuario = "", DBNull.Value, strEstadoUsuario)
            Sqlcommand.Parameters.Add("@pFechaVigencia", SqlDbType.Char, 10).Value = IIf(datFechaVigencia.Trim = "", DBNull.Value, datFechaVigencia.Trim)
            Sqlcommand.Parameters.Add("@pTodasCuentas", SqlDbType.Bit).Value = IIf(blnTodasCuentas, 1, 0)
            Sqlcommand.Parameters.Add("@pIdTipoControl", SqlDbType.Int).Value = IIf(lngIdTipoControl = 0, DBNull.Value, lngIdTipoControl)
            Sqlcommand.Parameters.Add("@pEmailUsuario", SqlDbType.VarChar, 50).Value = emailUsuario
            Dim oParamSal As New SqlParameter("@pMsjRetorno", SqlDbType.Char, 60) With {
                .Direction = ParameterDirection.Output
            }
            Sqlcommand.Parameters.Add(oParamSal)
            SQLDataAdapter.SelectCommand = Sqlcommand
            SQLDataAdapter.Fill(LDS_Usuario, Lstr_NombreTabla)

            LDS_Usuario_Aux = LDS_Usuario
            strRetorno = "OK"
            Return LDS_Usuario_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function Eliminar(ByRef strRetorno As String, ByVal lngIdUsuario As Long) As DataSet

        Dim lDS_Usuario As New DataSet
        Dim lDS_Usuario_Aux As New DataSet


        Dim Sqlcommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim Lstr_Procedimiento As String = ""
        Dim Lstr_NombreTabla As String = ""

        'Lstr_Procedimiento = "SGC_SP_MANTENCION_USUARIO"
        Lstr_Procedimiento = "Sis_Usuario_Mantencion"
        Lstr_NombreTabla = "USUARIO"
        Connect.Abrir()

        Try
            Sqlcommand = New SqlCommand(Lstr_Procedimiento, Connect.Conexion)

            Sqlcommand.CommandType = CommandType.StoredProcedure
            Sqlcommand.CommandText = Lstr_Procedimiento
            Sqlcommand.Parameters.Clear()

            Sqlcommand.Parameters.Add("@pCodigoOperacion", SqlDbType.Char, 10).Value = "ELIMINAR"
            Sqlcommand.Parameters.Add("@pIdUsuario", SqlDbType.Int).Value = lngIdUsuario
            Sqlcommand.Parameters.Add("@pIdNegocio", SqlDbType.Int).Value = glngNegocioOperacion

            Dim oParamSal As New SqlClient.SqlParameter("@pMsjRetorno", SqlDbType.Char, 60)
            oParamSal.Direction = ParameterDirection.Output
            Sqlcommand.Parameters.Add(oParamSal)


            SQLDataAdapter.SelectCommand = Sqlcommand
            SQLDataAdapter.Fill(lDS_Usuario, Lstr_NombreTabla)

            lDS_Usuario_Aux = lDS_Usuario
            strRetorno = "OK"
            Return lDS_Usuario_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function CambiarPassword(ByRef strRetorno As String,
                                    ByVal lngIdUsuario As Long,
                                    ByVal strClaveUsuario As String) As DataSet
        Dim lDS_Usuario As New DataSet
        Dim lDS_Usuario_Aux As New DataSet
        Dim Sqlcommand As New SqlCommand
        Dim SQLDataAdapter As New SqlDataAdapter
        Dim Connect As New Cls_Conexion
        Connect.Abrir()
        Try
            Sqlcommand = New SqlCommand("Sis_Usuario_Mantencion", Connect.Conexion) With {
                .CommandType = CommandType.StoredProcedure,
                .CommandText = "Sis_Usuario_Mantencion"
            }
            Sqlcommand.Parameters.Clear()
            Dim oParamSal As New SqlParameter("@pMsjRetorno", SqlDbType.Char, 60) With {
                .Direction = ParameterDirection.Output
            }
            Sqlcommand.Parameters.Add("@pCodigoOperacion", SqlDbType.Char, 10).Value = "CAMBIARPSW"
            Sqlcommand.Parameters.Add("@pIdUsuario", SqlDbType.Int).Value = lngIdUsuario
            Sqlcommand.Parameters.Add("@pClaveUsuario", SqlDbType.Char, 200).Value = strClaveUsuario
            Sqlcommand.Parameters.Add("@pEstadoUsuario", SqlDbType.Char, 200).Value = "VIG"
            Sqlcommand.Parameters.Add(oParamSal)
            SQLDataAdapter.SelectCommand = Sqlcommand
            SQLDataAdapter.Fill(lDS_Usuario, "USUARIO")
            lDS_Usuario_Aux = lDS_Usuario
            strRetorno = oParamSal.Value.trim
            Return lDS_Usuario_Aux
        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function ResetearPassword(ByRef strRetorno As String,
                                     ByVal lngIdUsuario As Long,
                                     ByVal strClaveUsuario As String,
                                     ByVal strEstadoUsuario As String) As DataSet

        Dim lDS_Usuario As New DataSet
        Dim lDS_Usuario_Aux As New DataSet

        Dim Sqlcommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim Lstr_Procedimiento As String = ""
        Dim Lstr_NombreTabla As String = ""

        'Lstr_Procedimiento = "SGC_SP_MANTENCION_USUARIO"
        Lstr_Procedimiento = "Sis_Usuario_Mantencion"
        Lstr_NombreTabla = "USUARIO"
        Connect.Abrir()

        Try
            Sqlcommand = New SqlCommand(Lstr_Procedimiento, Connect.Conexion)

            Sqlcommand.CommandType = CommandType.StoredProcedure
            Sqlcommand.CommandText = Lstr_Procedimiento
            Sqlcommand.Parameters.Clear()

            Sqlcommand.Parameters.Add("@pCodigoOperacion", SqlDbType.Char, 10).Value = "CAMBIARPSW"
            Sqlcommand.Parameters.Add("@pIdUsuario", SqlDbType.Int).Value = lngIdUsuario
            Sqlcommand.Parameters.Add("@pClaveUsuario", SqlDbType.Char, 200).Value = strClaveUsuario
            Sqlcommand.Parameters.Add("@pEstadoUsuario", SqlDbType.Char, 200).Value = strEstadoUsuario

            Dim oParamSal As New SqlClient.SqlParameter("@pMsjRetorno", SqlDbType.Char, 60)
            oParamSal.Direction = ParameterDirection.Output
            Sqlcommand.Parameters.Add(oParamSal)

            SQLDataAdapter.SelectCommand = Sqlcommand
            SQLDataAdapter.Fill(lDS_Usuario, Lstr_NombreTabla)


            lDS_Usuario_Aux = lDS_Usuario

            strRetorno = "OK"
            Return lDS_Usuario_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function TraerUsuario(ByRef strRetorno As String,
                                 ByVal strNombreUsuario As String,
                                 ByVal sPassWord As String) As DataSet

        Dim lDS_Usuario As New DataSet
        Dim lDS_Usuario_Aux As New DataSet

        Dim Sqlcommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim Lstr_Procedimiento As String = ""
        Dim Lstr_NombreTabla As String = ""

        'Lstr_Procedimiento = "SGC_SP_VALIDA_USUARIO"
        Lstr_Procedimiento = "Sis_Usuario_Valida"
        Lstr_NombreTabla = "USUARIO"
        Connect.Abrir()

        Try
            Sqlcommand = New SqlCommand(Lstr_Procedimiento, Connect.Conexion)

            Sqlcommand.CommandType = CommandType.StoredProcedure
            Sqlcommand.CommandText = Lstr_Procedimiento
            Sqlcommand.Parameters.Clear()

            Sqlcommand.Parameters.Add("@pNombreUsuario", SqlDbType.Char, 50).Value = strNombreUsuario

            Dim oParamSal As New SqlClient.SqlParameter("@pMsjRetorno", SqlDbType.Char, 60)
            oParamSal.Direction = ParameterDirection.Output
            Sqlcommand.Parameters.Add(oParamSal)

            SQLDataAdapter.SelectCommand = Sqlcommand
            SQLDataAdapter.Fill(lDS_Usuario, Lstr_NombreTabla)

            lDS_Usuario_Aux = lDS_Usuario
            strRetorno = "OK"
            Return lDS_Usuario_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function Buscar(ByRef strRetorno As String,
                           ByVal lngIdUsuario As Long,
                           ByVal strNombreUsuario As String,
                           ByVal strNombreCortoUsuario As String,
                           ByVal strEstadoUsuario As String) As DataSet

        Dim dsUsuario As New DataSet
        Dim dsUsuarioAux As New DataSet
        Dim sqlCommand As New SqlCommand
        Dim sqlDataAdapter As New SqlDataAdapter
        Dim conexion As New Cls_Conexion
        conexion.Abrir()

        Try
            sqlCommand = New SqlCommand("Sis_Usuario_Consultar", conexion.Conexion) With {
                .CommandType = CommandType.StoredProcedure,
                .CommandText = "Sis_Usuario_Consultar"
            }
            sqlCommand.Parameters.Clear()
            sqlCommand.Parameters.Add("@pIdUsuario", SqlDbType.Int).Value = IIf(lngIdUsuario = 0, DBNull.Value, lngIdUsuario)
            sqlCommand.Parameters.Add("@pNombreUsuario", SqlDbType.Char, 50).Value = IIf(strNombreUsuario = "", DBNull.Value, strNombreUsuario)
            sqlCommand.Parameters.Add("@pNombreCortoUsuario", SqlDbType.Char, 50).Value = IIf(strNombreCortoUsuario = "", DBNull.Value, strNombreCortoUsuario)
            sqlCommand.Parameters.Add("@pEstadoUsuario", SqlDbType.Char, 50).Value = IIf(strEstadoUsuario = "", DBNull.Value, strEstadoUsuario)
            sqlCommand.Parameters.Add("@pIdNegocio", SqlDbType.Int).Value = glngNegocioOperacion
            Dim oParamSal As New SqlParameter("@pMsjRetorno", SqlDbType.Char, 60) With {
                .Direction = ParameterDirection.Output
            }
            sqlCommand.Parameters.Add(oParamSal)

            sqlDataAdapter.SelectCommand = sqlCommand
            sqlDataAdapter.Fill(dsUsuario, "USUARIO")
            dsUsuarioAux = dsUsuario
            strRetorno = "OK"

            Return dsUsuarioAux
        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            conexion.Cerrar()
        End Try

    End Function

    Public Function Consultar(ByRef strRetorno As String, ByVal lngIdUsuario As Long) As DataSet

        Dim lDS_Usuario As New DataSet
        Dim lDS_Usuario_Aux As New DataSet

        Dim Sqlcommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim Lstr_Procedimiento As String = ""
        Dim Lstr_NombreTabla As String = ""

        'Lstr_Procedimiento = "SGC_SP_CONSULTA_USUARIO"
        Lstr_Procedimiento = "Sis_Usuario_Consultar"
        Lstr_NombreTabla = "USUARIO"
        Connect.Abrir()

        Try
            Sqlcommand = New SqlCommand(Lstr_Procedimiento, Connect.Conexion)

            Sqlcommand.CommandType = CommandType.StoredProcedure
            Sqlcommand.CommandText = Lstr_Procedimiento
            Sqlcommand.Parameters.Clear()

            Sqlcommand.Parameters.Add("@pIdUsuario", SqlDbType.Int).Value = lngIdUsuario
            Sqlcommand.Parameters.Add("@pIdNegocio", SqlDbType.Int).Value = glngNegocioOperacion

            Dim oParamSal As New SqlClient.SqlParameter("@pMsjRetorno", SqlDbType.Char, 60)
            oParamSal.Direction = ParameterDirection.Output
            Sqlcommand.Parameters.Add(oParamSal)


            SQLDataAdapter.SelectCommand = Sqlcommand
            SQLDataAdapter.Fill(lDS_Usuario, Lstr_NombreTabla)


            lDS_Usuario_Aux = lDS_Usuario

            strRetorno = "OK"

            Return lDS_Usuario_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function TraerOpcionesSistema(ByVal sPagina As String) As Boolean
        Return True
    End Function

    Function EstaConectado() As Boolean
        Return True
    End Function

    Public Function ValidaCuenta() As Integer
        Return 0
    End Function

    Private Function ValidaCaracteresAlfa() As Boolean
        Return True
    End Function

    Public Function EsUsuarioValido() As Boolean
        Return True
    End Function

    Public Function ValidarReglas(ByVal idUsuario As Integer, ByVal sPassWordAnt As String, ByVal lstrPasswordNueva As String, ByVal sPasswordVerifica As String) As Integer
        Dim oRs As New DataSet
        Dim lstrPassWrodBase As String = ""
        Dim lstrRetorno As String = ""
        'Dim lstrp As String = ""
        'Dim oUsuario As WSC_Usuarios= New WSC_Usuarios
        Dim PasswordArray() As String

        If lstrPasswordNueva <> sPasswordVerifica Then
            Return 300
        End If

        oRs = Consultar(lstrRetorno, idUsuario)

        If lstrRetorno <> "OK" Then
            Return 300
        End If

        lstrPassWrodBase = oRs.Tables("USUARIO").Rows(0).Item("CLAVE_USUARIO")
        lstrPassWrodBase = DesEncriptar(lstrPassWrodBase).Trim.ToUpper
        PasswordArray = lstrPassWrodBase.Split(" ")

        If sPassWordAnt <> PasswordArray(0).Trim Then 'sPassWrodBase Then
            Return 2
        End If

        If Not ValidaLargoPassWord(lstrPasswordNueva) Then
            Return 100
        End If

        If Not ValidaBlancos(lstrPasswordNueva) Then
            Return 200
        End If

        Return 1     '// Password correcta 
    End Function

    Public Function EsPassWordObvia(ByVal pPassword As String) As Boolean
        Dim bValRet As Boolean = True
        Return bValRet
    End Function

    Public Function ElstrPasswordAnterior(ByVal pCuenta As String, ByVal pPassword As String) As Boolean
        Dim bValRet As Boolean = False
        Return bValRet
    End Function

    Private Function ValidaLargoPassWord(ByVal lstrPasswordNueva As String) As Boolean
        Return (Len(lstrPasswordNueva) >= 6)
    End Function
    '**************************************************************************
    '** Respetar la vigencia
    '**************************************************************************
    Private Function ValidaVigencia() As Boolean
        Return True
    End Function

    Private Function ValidaBlancos(ByVal lstrPasswordNueva As String) As Boolean
        Dim i As Integer = 1
        Dim bExisteBlanco As Boolean = False

        Do While i < lstrPasswordNueva.Length And Not bExisteBlanco
            If lstrPasswordNueva.Chars(i) = " " Then
                bExisteBlanco = True
            End If
            i = i + 1
        Loop

        Return (Not bExisteBlanco)
    End Function

    Private Function ValidaPrimerosCaracteres(lstrPasswordAnterior As String) As Boolean
        Dim sPrimero As String
        Dim i As Integer
        ' Dim PasswordAnterior As String
        Dim bHaycaracterInvalido As Boolean

        '****************************************
        '**  Valida que los tres primeros caracteres no sean iguales 
        '****************************************
        sPrimero = lstrPasswordNueva.Chars(1)

        If sPrimero = lstrPasswordNueva.Chars(2) And sPrimero = lstrPasswordNueva.Chars(3) Then
            Return False
        End If

        '****************************************
        '**  Valida que no deben aparecen en la misma secuencia que cuenta del usuario 
        '****************************************

        '****************************************
        '**  Valida que no deben aparecen en la password anterior 
        '****************************************
        If lstrPasswordNueva.Substring(1, 3) = lstrPasswordAnterior.Substring(1, 3) Then
            Return False
        End If

        '****************************************
        '**  Valida que no sean Ñs
        '****************************************
        i = 1
        bHaycaracterInvalido = False

        Do While i < 3 And Not bHaycaracterInvalido
            If lstrPasswordNueva.Chars(i) = "ñ" Or lstrPasswordNueva.Chars(i) = "Ñ" Then
                bHaycaracterInvalido = True
            End If
            i = i + 1
        Loop

        If bHaycaracterInvalido Then
            Return False
        End If

        '****************************************
        '**  Valida que no sean valores obvios 
        '****************************************
        Return True
    End Function

    Public Function TraerAccesosUsuario(ByRef strRetorno As String, ByVal strIdUsuario As String) As List(Of TAccesos)

        Dim lobjAcciones As New ClsPerfilesAcciones
        Dim LDS_Accesos As New DataSet
        Dim listaAccesos As New List(Of TAccesos)

        LDS_Accesos = lobjAcciones.TraeAccesosDelUsuario(strRetorno, strIdUsuario)

        If strRetorno = "OK" Then
            Dim row As DataRow

            For Each row In LDS_Accesos.Tables(0).Rows
                Dim oTAccesos As New TAccesos

                oTAccesos.strIdAccion = row.Item("id_accion").ToString
                oTAccesos.strDerechoAccion = row.Item("derecho_accion").ToString
                listaAccesos.Add(oTAccesos)
                oTAccesos = Nothing
            Next

        End If

        lobjAcciones = Nothing

        Return listaAccesos

    End Function

    Public Function TraerAccesosPerfil(ByRef strRetorno As String, ByVal strIdPerfil As String, ByVal strNegocio As String) As List(Of TAccesos)
        Dim lobjAcciones As New ClsPerfilesAcciones
        Dim LDS_Accesos As New DataSet
        Dim listaAccesos As New List(Of TAccesos)

        LDS_Accesos = lobjAcciones.TraeAccesosDelPerfil(strRetorno, strIdPerfil, strNegocio)

        If strRetorno = "OK" Then

            Dim row As DataRow

            For Each row In LDS_Accesos.Tables(0).Rows
                Dim oTAccesos As New TAccesos

                oTAccesos.strIdAccion = row.Item("id_accion").ToString
                oTAccesos.strDerechoAccion = row.Item("derecho_accion").ToString
                listaAccesos.Add(oTAccesos)
                oTAccesos = Nothing
            Next
        End If

        lobjAcciones = Nothing

        Return listaAccesos

    End Function

    Public Function Encriptar(ByVal strTexto As String) As String
        Dim sRetorno As String = ""
        sRetorno = ClsEncriptacion.Encriptar(strTexto)
        Return sRetorno
    End Function

    Public Function DesEncriptar(ByVal strTexto As String) As String
        Dim sRetorno As String = ""
        sRetorno = ClsEncriptacion.DesEncriptar(strTexto)
        Return sRetorno
    End Function

    Public Function Usuarios_Ver(ByVal lngIdUsuario As Long,
                                 ByVal strColumnasDetalle As String,
                                 ByRef strResultado As String) As DataSet

        Dim LArr_NombreColumna_Detalle() As String = Split(strColumnasDetalle, ",")
        Dim LintCol As Integer
        Dim LstrNomCol As String
        Dim Columna As DataColumn
        Dim Remove As Boolean

        Dim lDS_CuentasClientes As New DataSet
        Dim lDS_CuentasClientes_Aux As New DataSet


        Dim Sqlcommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim Lstr_Procedimiento As String = ""
        Dim Lstr_NombreTabla As String = ""

        'Lstr_Procedimiento = "SGC_SP_USUARIO_VER"
        Lstr_Procedimiento = "Sis_Usuario_Ver"
        Lstr_NombreTabla = "USUARIO"

        'Abre la conexion 
        Connect.Abrir()
        Try
            Sqlcommand = New SqlCommand(Lstr_Procedimiento, Connect.Conexion)

            Sqlcommand.CommandType = CommandType.StoredProcedure
            Sqlcommand.CommandText = Lstr_Procedimiento
            Sqlcommand.Parameters.Clear()

            Sqlcommand.Parameters.Add("@pIdUsuario", SqlDbType.Int).Value = IIf(lngIdUsuario = 0, DBNull.Value, lngIdUsuario)

            Dim oParamSal As New SqlClient.SqlParameter("@pMsjRetorno", SqlDbType.Char, 60)
            oParamSal.Direction = ParameterDirection.Output
            Sqlcommand.Parameters.Add(oParamSal)

            SQLDataAdapter.SelectCommand = Sqlcommand
            SQLDataAdapter.Fill(lDS_CuentasClientes, Lstr_NombreTabla)

            If strColumnasDetalle.Trim = "" Then
                lDS_CuentasClientes_Aux = lDS_CuentasClientes
            Else
                lDS_CuentasClientes_Aux = lDS_CuentasClientes.Copy
                For Each Columna In lDS_CuentasClientes.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LintCol = 0 To LArr_NombreColumna_Detalle.Length - 1
                        LstrNomCol = LArr_NombreColumna_Detalle(LintCol).TrimEnd.TrimStart
                        If Columna.ColumnName = LstrNomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        lDS_CuentasClientes_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            strResultado = "OK"
            Return lDS_CuentasClientes_Aux

        Catch ex As Exception
            strResultado = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function Buscar_Usuario(ByRef strRetorno As String,
                           ByVal strEstadoUsuario As String,
                           ByVal strColumnas As String) As DataSet

        Dim lDS_Usuario As New DataSet
        Dim lDS_Usuario_Aux As New DataSet

        Dim Sqlcommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim Lstr_Procedimiento As String = ""
        Dim Lstr_NombreTabla As String = ""

        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True


        'Lstr_Procedimiento = "SGC_SP_CONSULTA_USUARIO"
        Lstr_Procedimiento = "Rcp_Usuario_Consultar"
        Lstr_NombreTabla = "SIS_USUARIO"
        Connect.Abrir()

        Try
            Sqlcommand = New SqlCommand(Lstr_Procedimiento, Connect.Conexion)

            Sqlcommand.CommandType = CommandType.StoredProcedure
            Sqlcommand.CommandText = Lstr_Procedimiento
            Sqlcommand.Parameters.Clear()
            Sqlcommand.Parameters.Add("@pEstadoUsuario", SqlDbType.Char, 50).Value = IIf(strEstadoUsuario = "", DBNull.Value, strEstadoUsuario)

            Dim oParamSal As New SqlClient.SqlParameter("@pMsjRetorno", SqlDbType.Char, 60)
            oParamSal.Direction = ParameterDirection.Output
            Sqlcommand.Parameters.Add(oParamSal)

            SQLDataAdapter.SelectCommand = Sqlcommand
            SQLDataAdapter.Fill(lDS_Usuario, Lstr_NombreTabla)

            lDS_Usuario_Aux = lDS_Usuario

            If strColumnas.Trim = "" Then
                lDS_Usuario_Aux = lDS_Usuario
            Else
                lDS_Usuario_Aux = lDS_Usuario.Copy
                For Each Columna In lDS_Usuario.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        lDS_Usuario_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In lDS_Usuario_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"

            Return lDS_Usuario_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function Bloquear(ByRef strRetorno As String,
                             ByVal lngIdUsuario As Long) As DataSet
        Dim lDS_Usuario As New DataSet
        Dim lDS_Usuario_Aux As New DataSet
        Dim Sqlcommand As New SqlCommand
        Dim SQLDataAdapter As New SqlDataAdapter
        Dim Connect As New Cls_Conexion
        Connect.Abrir()
        Try
            Sqlcommand = New SqlCommand("Sis_Usuario_Mantencion", Connect.Conexion) With {
                .CommandType = CommandType.StoredProcedure,
                .CommandText = "Sis_Usuario_Mantencion"
            }
            Sqlcommand.Parameters.Clear()
            Dim oParamSal As New SqlParameter("@pMsjRetorno", SqlDbType.Char, 60) With {
                .Direction = ParameterDirection.Output
            }
            Sqlcommand.Parameters.Add("@pCodigoOperacion", SqlDbType.Char, 10).Value = "BLOQUEAR"
            Sqlcommand.Parameters.Add("@pIdUsuario", SqlDbType.Int).Value = lngIdUsuario
            Sqlcommand.Parameters.Add(oParamSal)
            SQLDataAdapter.SelectCommand = Sqlcommand
            SQLDataAdapter.Fill(lDS_Usuario, "USUARIO")
            lDS_Usuario_Aux = lDS_Usuario
            strRetorno = oParamSal.Value.trim
            Return lDS_Usuario_Aux
        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function EstadoLista() As String

        Dim Sqlcommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim lstrProcedimiento As String = ""

        lstrProcedimiento = "CMA_INSERTAR_RELACION_CUENTAS"
        SQLConnect.Abrir()

        Try

            Sqlcommand = New SqlClient.SqlCommand(lstrProcedimiento, SQLConnect.Conexion)
            Sqlcommand.CommandType = CommandType.StoredProcedure
            Sqlcommand.CommandText = lstrProcedimiento
            Sqlcommand.Parameters.Clear()

            Sqlcommand.Parameters.Add("@pIdUsuario", SqlDbType.Int).Value = glngIdUsuario
            Sqlcommand.Parameters.Add("@pIdNegocio", SqlDbType.Int).Value = glngNegocioOperacion
            Dim ParametroSal2 As New SqlClient.SqlParameter("@pResultado", SqlDbType.VarChar, 250)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            Sqlcommand.Parameters.Add(ParametroSal2)

            Sqlcommand.ExecuteNonQuery()

            EstadoLista = Sqlcommand.Parameters("@pResultado").Value.ToString.Trim
        Catch ex As Exception
            EstadoLista = ex.Message

        Finally
            SQLConnect.Cerrar()
        End Try
    End Function

End Class
