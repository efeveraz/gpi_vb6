﻿Imports System.Configuration
Imports Microsoft.VisualBasic
Imports System
Imports Oracle.DataAccess.Client
Imports System.Data.Common


Public Class ClsConexionOracle

#Region "8====== CONEXION CON ORACLE FOR OLE =======3~X_O"
  'Dim OracleConnection As Object

  'Public OraSession As Object
  'Public OraDatabase As Object

  'Private Shared Sub GetConfigDB(ByRef strUser, ByRef strPass, ByRef strTNSName)
  '  strUser = "XXFOH"
  '  strPass = "XXFOH"
  '  strTNSName = "ALERCE"
  'End Sub

  'Public Sub Abrir()
  '  Dim lstrUser As String
  '  Dim lstrPass As String
  '  Dim lstrTNSName As String

  '  Call GetConfigDB(strUser:=lstrUser, strPass:=lstrPass, strTNSName:=lstrTNSName)

  '  OraSession = CreateObject("OracleInProcServer.XOraSession")

  '  ' Abre la conección.
  '  OraDatabase = OraSession.DbOpenDataBase(lstrTNSName, lstrUser & "/" & lstrPass, 0&)
  'End Sub

  'Public Sub Cerrar()
  '  OraDatabase.Close()
  '  OraDatabase = Nothing

  '  OraSession = Nothing
  'End Sub

  'Public Function BeginTransaccion() As Object
  '  Return OraSession.BeginTrans()
  'End Function

  'Public Function CommitTransaccion() As Object
  '  Return OraSession.CommitTrans()
  'End Function

  'Public Function RollbackTransaccion() As Object
  '  Return OraSession.rollback()
  'End Function


  'Public Sub ClearParam()
  '  If Not IsNothing(OraDatabase) Then
  '    Do While OraDatabase.Parameters.count > 0
  '      OraDatabase.Parameters.Remove(0)
  '    Loop
  '  End If
  'End Sub
#End Region

#Region "8====== CONEXION CON ODP ORACLE =======3~X_O"
  Dim OracleConnection As New Oracle.DataAccess.Client.OracleConnection

  Shared _Conexion As OracleConnection

  Public Property Conexion() As OracleConnection
    Get
      Return _Conexion
    End Get

    Set(ByVal valor As OracleConnection)
      _Conexion = valor
    End Set

  End Property

  Public Sub Abrir()
    Dim SCadenaConexion As String

    SCadenaConexion = GetConnectionString()

    Me.Conexion = New OracleConnection(SCadenaConexion)

    'OracleConnection(Configuration.ConfigurationManager.ConnectionStrings(Gtxt_AmbienteBD).ConnectionString)

    '' Abre la conección.
    Conexion.Open()

  End Sub

  Public Sub Cerrar()

    Conexion.Close()
    Conexion.Dispose()
    'Conexion = Nothing

  End Sub

  Public Function Transaccion() As Object 'OracleTransaction
    Return _Conexion.BeginTransaction()
  End Function

  Private Shared Function GetConnectionString() As String
    Dim lintIndice As Integer
    Dim lstrClavesConexion() As String
    Dim lstrConexion As String = ""
        Dim lobjUsuario As ClsUsuario = New ClsUsuario

        'strConeccion = "<add connectionString=""userid=XXFOH;password=XXFOH; DataSource=ALERCE/ALERCE2/>"

        'strConeccion = "Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=MyHost)(PORT=MyPort)))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=MyOracleSID)));User Id=myUsername;Password=myPassword;"

        'strConeccion = "user id=XXFOH; password=XXFOH; Data Source=ALERCE/ALERCE"     '-----CREASYS

        lstrClavesConexion = ObtieneSeccion_ArchivoIni(gstrPathArchivoIni, "CONEXION_ORACLE_ITZ_CONT")

        For lintIndice = 0 To UBound(lstrClavesConexion)
            If lstrClavesConexion(lintIndice).Contains("Password") Then

                lstrConexion = lstrConexion & lstrClavesConexion(lintIndice).Substring(0, 9)
                If gLogoCliente = "HSBC" Then
                    lstrConexion = lstrConexion & gFunEncripta(lstrClavesConexion(lintIndice).Substring(9), False)
                Else
                    lstrConexion = lstrConexion & lobjUsuario.DesEncriptar(lstrClavesConexion(lintIndice).Substring(9))
                End If

            Else
                lstrConexion = lstrConexion & lstrClavesConexion(lintIndice) & "; "
            End If
        Next

    Return lstrConexion
  End Function
#End Region

End Class