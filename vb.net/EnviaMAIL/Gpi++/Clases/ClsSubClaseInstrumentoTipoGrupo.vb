﻿Public Class ClsSubClaseInstrumentoTipoGrupo

    Public Function SubClaseInstrumentoTipoGrupo_Consultar(ByVal intIdSubClaseInstrumentoTipoGrupo As Integer, _
                                                         ByVal strColumnas As String, _
                                                         ByRef strRetorno As String) As DataSet

        Dim LDS_SubClaseInstrumentoTipoGrupo As New DataSet
        Dim LDS_SubClaseInstrumentoTipoGrupo_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_SubClaseInstrumentoTipoGrupo_Consultar"
        Lstr_NombreTabla = "SubClaseInstrumentoTipoGrupo"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdSubClaseInstrumentoTipoGrupo", SqlDbType.Int, 10).Value = IIf(intIdSubClaseInstrumentoTipoGrupo = 0, DBNull.Value, intIdSubClaseInstrumentoTipoGrupo)

            '...Resultado
            Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultado.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_SubClaseInstrumentoTipoGrupo, Lstr_NombreTabla)

            LDS_SubClaseInstrumentoTipoGrupo_Aux = LDS_SubClaseInstrumentoTipoGrupo

            If strColumnas.Trim = "" Then
                LDS_SubClaseInstrumentoTipoGrupo_Aux = LDS_SubClaseInstrumentoTipoGrupo
            Else
                LDS_SubClaseInstrumentoTipoGrupo_Aux = LDS_SubClaseInstrumentoTipoGrupo.Copy
                For Each Columna In LDS_SubClaseInstrumentoTipoGrupo.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_SubClaseInstrumentoTipoGrupo_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_SubClaseInstrumentoTipoGrupo_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            LDS_SubClaseInstrumentoTipoGrupo.Dispose()
            Return LDS_SubClaseInstrumentoTipoGrupo_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function SubClaseInstrumentoTipoGrupo_Buscar(ByVal intIdSubClaseInstrumentoTipoGrupo As Integer, _
                                                        ByVal strDscSubClaseInstrumentoTipoGrupo As String, _
                                                        ByVal strEstSubClaseInstrumentoTipoGrupo As String, _
                                                        ByVal strColumnas As String, _
                                                        ByRef strRetorno As String) As DataSet

        Dim LDS_SubClaseInstrumentoTipoGrupo As New DataSet
        Dim LDS_SubClaseInstrumentoTipoGrupo_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_SubClaseInstrumentoTipoGrupo_Buscar"
        Lstr_NombreTabla = "SubClaseInstrumentoTipoGrupo"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdSubClaseInstrumentoTipoGrupo", SqlDbType.Int, 10).Value = IIf(intIdSubClaseInstrumentoTipoGrupo = 0, DBNull.Value, intIdSubClaseInstrumentoTipoGrupo)
            SQLCommand.Parameters.Add("pDscSubClaseInstrumentoTipoGrupo", SqlDbType.VarChar, 100).Value = IIf(strDscSubClaseInstrumentoTipoGrupo = "", DBNull.Value, strDscSubClaseInstrumentoTipoGrupo)
            SQLCommand.Parameters.Add("pEstSubClaseInstrumentoTipoGrupo", SqlDbType.VarChar, 3).Value = IIf(strEstSubClaseInstrumentoTipoGrupo = "", DBNull.Value, strEstSubClaseInstrumentoTipoGrupo)

            '...Resultado
            Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultado.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_SubClaseInstrumentoTipoGrupo, Lstr_NombreTabla)

            LDS_SubClaseInstrumentoTipoGrupo_Aux = LDS_SubClaseInstrumentoTipoGrupo

            If strColumnas.Trim = "" Then
                LDS_SubClaseInstrumentoTipoGrupo_Aux = LDS_SubClaseInstrumentoTipoGrupo
            Else
                LDS_SubClaseInstrumentoTipoGrupo_Aux = LDS_SubClaseInstrumentoTipoGrupo.Copy
                For Each Columna In LDS_SubClaseInstrumentoTipoGrupo.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_SubClaseInstrumentoTipoGrupo_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_SubClaseInstrumentoTipoGrupo_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            LDS_SubClaseInstrumentoTipoGrupo.Dispose()
            Return LDS_SubClaseInstrumentoTipoGrupo_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function SubClaseInstrumentoTipoGrupo_Mantencion(ByVal strAccion As String, _
                                                          ByVal intIdSubClaseInstrumentoTipoGrupo As Integer, _
                                                          ByVal strDscSubClaseInstrumentoTipoGrupo As String, _
                                                          ByVal strEstSubClaseInstrumentoTipoGrupo As String) As String


        Dim strDescError As String = "OK"
        Dim lintIdSubClaseInstrumentoTipoGrupo As Integer
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try


            SQLCommand = New SqlClient.SqlCommand("Rcp_SubClaseInstrumentoTipoGrupo_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_SubClaseInstrumentoTipoGrupo_Mantencion"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 20).Value = UCase(strAccion.Trim)
            'SQLCommand.Parameters.Add("pIdGrupoComisiones", SqlDbType.Int, 10).Value = IIf(intIdGrupoComisiones = 0, DBNull.Value, intIdGrupoComisiones)

            '...IdSubClaseInstrumentoTipoGrupo
            Dim pSalIdSubClaseInstrumentoTipoGrupo As New SqlClient.SqlParameter("pIdSubClaseInstrumentoTipoGrupo", SqlDbType.Int, 10)
            pSalIdSubClaseInstrumentoTipoGrupo.Direction = Data.ParameterDirection.InputOutput
            pSalIdSubClaseInstrumentoTipoGrupo.Value = IIf(intIdSubClaseInstrumentoTipoGrupo = 0, DBNull.Value, intIdSubClaseInstrumentoTipoGrupo)
            SQLCommand.Parameters.Add(pSalIdSubClaseInstrumentoTipoGrupo)


            SQLCommand.Parameters.Add("pDscSubClaseInstrumentoTipoGrupo", SqlDbType.VarChar, 100).Value = strDscSubClaseInstrumentoTipoGrupo
            SQLCommand.Parameters.Add("pEstSubClaseInstrumentoTipoGrupo", SqlDbType.VarChar, 3).Value = strEstSubClaseInstrumentoTipoGrupo


            '...Resultado
            Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultado.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                lintIdSubClaseInstrumentoTipoGrupo = CInt(SQLCommand.Parameters("pIdSubClaseInstrumentoTipoGrupo").Value.ToString.Trim)
                If Trim(strDescError) = "OK" Then
                    MiTransaccionSQL.Commit()
                    Return ("OK")
                End If
            Else
                GoTo Salir
            End If

Salir:
            If Trim(strDescError) = "OK" Then
                MiTransaccionSQL.Commit()
                Return ("OK")
            Else
                MiTransaccionSQL.Rollback()
                Return strDescError
            End If


        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en la mantención tipo grupo sub clase instrumento." & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            SubClaseInstrumentoTipoGrupo_Mantencion = strDescError
        End Try
    End Function

End Class
