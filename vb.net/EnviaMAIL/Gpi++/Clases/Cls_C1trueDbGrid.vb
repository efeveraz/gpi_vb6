﻿' *********************************************************************
' ***** Creado Por      :   Jose Luis Ponce V.
' ***** Fecha Creacion  :   26-09-2012
' ***** 
' *********************************************************************
Imports C1.Win.C1TrueDBGrid
Imports System.Text

Public Class Cls_C1trueDbGrid

    Public Class EstructuraCampo
        Private _NombreCampo As String
        Private _Decimales As Integer

        Public Property NombreCampo() As String
            Get
                Return _NombreCampo
            End Get
            Set(ByVal value As String)
                _NombreCampo = value
            End Set
        End Property

        Public Property Decimales() As Integer
            Get
                Return _Decimales
            End Get
            Set(ByVal value As Integer)
                _Decimales = value
            End Set
        End Property
    End Class

#Region "Variables locales "
    Private _listaCampos As New ArrayList
    Private _glngIdUsuario As String
    Private _intIdContraparte As Integer
    Private _strNombrePlantilla As String
    Private _StrReporte As String
    Private ldataSet As New DataSet
    Private _UsarPlantilla As Boolean
    Private _Filtros As String
#End Region

#Region "Propiedades "

    Private Property idusuario() As String
        Get
            Return _glngIdUsuario
        End Get
        Set(ByVal value As String)
            _glngIdUsuario = value
        End Set
    End Property

    Private Property IdContraparte() As Integer
        Get
            Return _intIdContraparte
        End Get
        Set(ByVal value As Integer)
            _intIdContraparte = value
        End Set
    End Property

    Private Property NombrePlantilla() As String
        Get
            Return _strNombrePlantilla
        End Get
        Set(ByVal value As String)
            _strNombrePlantilla = value
        End Set
    End Property

    Private Property NombreReporte() As String
        Get
            Return _StrReporte
        End Get
        Set(ByVal value As String)
            _StrReporte = value
        End Set
    End Property

    Private Property ListaCampos() As ArrayList
        Get
            Return _listaCampos
        End Get
        Set(ByVal value As ArrayList)
            _listaCampos = value
        End Set
    End Property

    Private Property UsarPlantilla() As Boolean
        Get
            Return _UsarPlantilla
        End Get
        Set(ByVal value As Boolean)
            _UsarPlantilla = value
        End Set
    End Property

    Public Property Filtros() As String
        Get
            Return _Filtros
        End Get
        Set(ByVal value As String)
            _Filtros = value
        End Set
    End Property

#End Region

#Region "Constructores"
    Public Sub New()
    End Sub

    Public Sub New(ByVal plistCampos As ArrayList)
        ListaCampos = plistCampos
        UsarPlantilla = False
    End Sub

    Public Sub New(ByVal pStrReporte As String, ByVal pNombrePlantilla As String, ByVal pidusuario As String, ByVal pIdContraparte As Integer)
        Try
            idusuario = pidusuario
            IdContraparte = pIdContraparte
            NombrePlantilla = pNombrePlantilla
            NombreReporte = pStrReporte
            UsarPlantilla = True

            If NombreReporte.Trim.Length > 0 Then
                Me.CargaListaCamposSubtotales()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "Metodos o funciones privados "
    Private Sub LimpiarSubtotalesFooter(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            For Each col As C1DataColumn In CType(sender, C1TrueDBGrid).Columns
                col.FooterText = ""
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub SubAgregarSubtotalesFooter(ByRef pobject As Object, ByVal pitem As Object, ByVal pvalor As Object)
        Try
            pobject.Columns(pitem).FooterText = pvalor
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub CargaListaCamposSubtotales()
        Try
            Dim objPlantilla As New ClsArchivo
            Dim strRetorno As String = String.Empty
            Dim objformato() As Object
            Dim AddCampos As New EstructuraCampo

            ldataSet = objPlantilla.Archivo_Importa_D_PorPlantilla(NombreReporte, IdContraparte, idusuario, NombrePlantilla, strRetorno)
            If ldataSet.Tables(0).Rows.Count > 0 Then
                ListaCampos = New ArrayList
                For Each fila As DataRow In ldataSet.Tables(0).Rows
                    objformato = fila.Item("FORMATO").ToString.Split("|")
                    If ((fila.Item("TIPO_DATO") = 0) And (objformato(7).ToString.ToUpper = "T")) Then
                        AddCampos = New EstructuraCampo
                        AddCampos.NombreCampo = fila.Item("CAMPO")
                        AddCampos.Decimales = fila.Item("DECIMALES")
                        ListaCampos.Add(AddCampos)
                    End If
                Next
            End If
            AddCampos = Nothing
            objformato = Nothing
            ldataSet.Dispose()
            ldataSet = Nothing
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "Metodos o funciones publicos "
    Public Sub ActualizaNombreReporte(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            NombreReporte = sender.tag
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub FiltroColumnasConSubtotales(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim sb As New System.Text.StringBuilder
        Dim dc As C1.Win.C1TrueDBGrid.C1DataColumn

        Dim ldataViewFilter As New DataTable
        Dim ldataprincipal As New DataTable
        Dim objnumero As Object = Nothing
        Dim StrCondicion As New StringBuilder
        Dim lcnt As Integer = 0
        Dim SwBorraFilaSubTotal As Boolean = False

        Try
            If ListaCampos.Count <= 0 Then
                Exit Sub
            End If
            sender.SelectedRows.Clear()

            For Each dc In sender.Columns
                If dc.FilterText.Trim.Length > 0 Then
                    If sb.Length > 0 Then
                        sb.Append(" AND ")
                    End If
                    sb.Append(gstrFiltro_X_TipoColumna(dc))
                End If
            Next dc

            If IsNothing(sender.DataSource) = True Then
                Exit Sub
            End If

            If sb.ToString.Trim.Length > 0 Then
                sender.DataSource.DefaultView.RowFilter = sb.ToString
                Filtros = sender.DataSource.DefaultView.RowFilter.ToString
            Else
                sender.DataSource.DefaultView.RowFilter = Nothing
                Filtros = Nothing
            End If
            sender.Refresh()
            If sender.RowCount > 0 Then
                sender.SelectedRows.Add(0)
            End If

            ldataViewFilter = CType(sender.DataSource.DefaultView.totable, DataTable)
            Call LimpiarSubtotalesFooter(sender, System.EventArgs.Empty)
            If ldataViewFilter.Rows.Count > 0 Then

                If UsarPlantilla = True Then
                    For Each item As EstructuraCampo In ListaCampos
                        If sb.ToString.Trim.Length > 0 Then
                            objnumero = ldataViewFilter.Compute("Sum(" & item.NombreCampo & ")", "")
                        Else
                            objnumero = ldataViewFilter.Compute("sum(" & item.NombreCampo & ")", "")
                        End If
                        If IsNothing(Filtros) = False Then
                            Filtros = Filtros & " or (" & item.NombreCampo & "=" & Convert.ToInt64(objnumero) & ")"
                        End If
                        objnumero = String.Format("{0:n" & item.Decimales & "}", objnumero)
                        SubAgregarSubtotalesFooter(sender, item.NombreCampo, objnumero)
                    Next
                Else
                    For Each item In ListaCampos
                        objnumero = ldataViewFilter.Compute("sum(" & item & ")", "")
                        If IsNothing(Filtros) = False Then
                            Filtros = Filtros & " or (" & item.NombreCampo & "=" & Convert.ToInt64(objnumero) & ")"
                        End If
                        objnumero = String.Format("{0:n0}", objnumero)
                        SubAgregarSubtotalesFooter(sender, item, objnumero)
                    Next
                End If

            End If
            ldataViewFilter.AcceptChanges()
            sender.Refresh()
            lcnt = Nothing
            ldataViewFilter = Nothing
            ldataprincipal = Nothing
            objnumero = Nothing
            StrCondicion = Nothing


        Catch ex As Exception


        End Try
    End Sub

    Public Sub CambiarColumnasSubtotales(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            'NombrePlantilla = CType(sender, ToolStripComboBox).Text
            NombrePlantilla = sender.Text
            Me.CargaListaCamposSubtotales()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub DataSourceAddSubtotales(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim objnumero As New Object
        Try
            If IsNothing(sender.DataSource) = True Then
                Exit Sub
            End If

            Dim ldata As New DataTable
            Dim objPlantilla As New ClsArchivo
            Dim strRetorno As String = String.Empty
            If UsarPlantilla = True Then
                Me.CargaListaCamposSubtotales()
                ConfiguraGrillaConPlantilla(sender, NombreReporte, NombrePlantilla, IdContraparte)
                sender.Refresh()
            End If

            sender.ColumnFooters = False
            ldata = CType(sender, C1TrueDBGrid).DataSource
            'ldata = sender.DataSource
            If ListaCampos.Count > 0 Then

                sender.ColumnFooters = True
                sender.FooterStyle.Borders.BorderType = BorderTypeEnum.Raised
                sender.FooterStyle.BackColor = Color.DarkGray       ' Color de Fondo (monto)
                sender.FooterStyle.ForeColor = Color.Blue           ' Color de texto (monto)
                sender.FooterStyle.Borders.Top = 0
                sender.FooterStyle.Borders.Left = 1
                sender.FooterStyle.Borders.Right = 1
                sender.FooterStyle.Borders.Bottom = 0

                If IsNothing(ldata) = False Then
                    If ldata.Rows.Count > 0 Then
                        If UsarPlantilla = True Then
                            For Each item As EstructuraCampo In ListaCampos
                                objnumero = ldata.Compute("sum(" & item.NombreCampo & ")", "")
                                objnumero = String.Format("{0:n" & item.Decimales & "}", objnumero)
                                SubAgregarSubtotalesFooter(sender, item.NombreCampo, objnumero)
                            Next
                        Else
                            For Each item In ListaCampos
                                objnumero = ldata.Compute("sum(" & item & ")", "")
                                objnumero = String.Format("{0:n0}", objnumero)
                                SubAgregarSubtotalesFooter(sender, item, objnumero)
                            Next
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            'Throw ex
        Finally
            objnumero = Nothing
        End Try
    End Sub
#End Region

End Class



