﻿Public Class ClsComisionDetalle

    Public Function ComisionDetalle_Buscar(ByVal intIdComision As Integer, _
                                           ByVal intIdOrden As Integer, _
                                           ByVal intIdOperacionDetalle As Integer, _
                                           ByVal strCodEstado As String, _
                                           ByVal strColumnas As String, _
                                           ByRef strRetorno As String) As DataSet

        Dim LDS_Comision As New DataSet
        Dim LDS_Comision_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_ComisionDetalle_Buscar"
        Lstr_NombreTabla = "ComisionDetalle"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdComision", SqlDbType.Int, 10).Value = IIf(intIdComision = 0, DBNull.Value, intIdComision)
            SQLCommand.Parameters.Add("pIdOrden", SqlDbType.Int, 10).Value = IIf(intIdOrden = 0, DBNull.Value, intIdOrden)
            SQLCommand.Parameters.Add("pIdOperacionDetalle", SqlDbType.Int, 10).Value = IIf(intIdOperacionDetalle = 0, DBNull.Value, intIdOperacionDetalle)
            SQLCommand.Parameters.Add("pIdPatrimonioCuenta", SqlDbType.Int, 10).Value = DBNull.Value
            SQLCommand.Parameters.Add("pCodEstado", SqlDbType.VarChar, 3).Value = IIf(strCodEstado = "", DBNull.Value, strCodEstado)
            '...Resultado
            Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultado.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Comision, Lstr_NombreTabla)

            LDS_Comision_Aux = LDS_Comision

            If strColumnas.Trim = "" Then
                LDS_Comision_Aux = LDS_Comision
            Else
                LDS_Comision_Aux = LDS_Comision.Copy
                For Each Columna In LDS_Comision.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Comision_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Comision_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            LDS_Comision.Dispose()
            Return LDS_Comision_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function ComisionDetalle_Consultar(ByVal lngIdComision As Long, _
                                              ByVal lngIdSolInversion As Long, _
                                              ByVal lngIdOperacion As Long, _
                                              ByVal strCodEstado As String, _
                                              ByVal strColumnas As String, _
                                              ByRef strRetorno As String) As DataSet

        Dim LDS_Comision As New DataSet
        Dim LDS_Comision_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_ComisionDetalle_Consultar"
        Lstr_NombreTabla = "ComisionDetalle"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdComision", SqlDbType.Int, 10).Value = IIf(lngIdComision = 0, DBNull.Value, lngIdComision)
            SQLCommand.Parameters.Add("pIdSolInversion", SqlDbType.Int, 10).Value = IIf(lngIdSolInversion = 0, DBNull.Value, lngIdSolInversion)
            SQLCommand.Parameters.Add("pIdOperacion", SqlDbType.Int, 10).Value = IIf(lngIdOperacion = 0, DBNull.Value, lngIdOperacion)
            SQLCommand.Parameters.Add("pCodEstado", SqlDbType.VarChar, 3).Value = IIf(strCodEstado = "", DBNull.Value, strCodEstado)
            '...Resultado
            Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultado.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Comision, Lstr_NombreTabla)

            LDS_Comision_Aux = LDS_Comision

            If strColumnas.Trim = "" Then
                LDS_Comision_Aux = LDS_Comision
            Else
                LDS_Comision_Aux = LDS_Comision.Copy
                For Each Columna In LDS_Comision.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Comision_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Comision_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            LDS_Comision.Dispose()
            Return LDS_Comision_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function ComisionExito_ConsultarDevengadas(ByVal intIdCliente As Integer, _
                                                      ByVal intIdCuenta As Long, _
                                                      ByVal strFechaDesde As String, _
                                                      ByVal strFechaHasta As String, _
                                                      ByVal strColumnas As String, _
                                                      ByRef strRetorno As String) As DataSet

        Dim LDS_ComisionesDevengadas As New DataSet
        Dim LDS_ComisionesDevengadas_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Cba_ComisionesExito_ConsultarDevengadas"
        Lstr_NombreTabla = "ComisionesDevengadas"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Int, 10).Value = glngNegocioOperacion
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Int, 6).Value = glngIdUsuario
            SQLCommand.Parameters.Add("pIdCliente", SqlDbType.Int, 10).Value = IIf(intIdCliente = 0, DBNull.Value, intIdCliente)
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Int, 10).Value = IIf(intIdCuenta = 0, DBNull.Value, intIdCuenta)
            SQLCommand.Parameters.Add("pFechaDesde", SqlDbType.Char, 10).Value = strFechaDesde
            SQLCommand.Parameters.Add("pFechaHasta", SqlDbType.Char, 10).Value = strFechaHasta

            '...Resultado
            Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 1000)
            pSalResultado.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_ComisionesDevengadas, Lstr_NombreTabla)

            LDS_ComisionesDevengadas_Aux = LDS_ComisionesDevengadas

            If strColumnas.Trim = "" Then
                LDS_ComisionesDevengadas_Aux = LDS_ComisionesDevengadas
            Else
                LDS_ComisionesDevengadas_Aux = LDS_ComisionesDevengadas.Copy
                For Each Columna In LDS_ComisionesDevengadas.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_ComisionesDevengadas_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_ComisionesDevengadas_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            LDS_ComisionesDevengadas.Dispose()
            Return LDS_ComisionesDevengadas_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function ComisionExito_ConsultarCobradas(ByVal intIdCliente As Integer, _
                                                      ByVal intIdCuenta As Long, _
                                                      ByVal strFechaDesde As String, _
                                                      ByVal strFechaHasta As String, _
                                                      ByVal strColumnas As String, _
                                                      ByRef strRetorno As String) As DataSet

        Dim LDS_ComisionesCobradas As New DataSet
        Dim LDS_ComisionesCobradas_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Cba_ComisionesExito_ConsultarCobradas"
        Lstr_NombreTabla = "ComisionesCobradas"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Int, 10).Value = glngNegocioOperacion
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Int, 6).Value = glngIdUsuario
            SQLCommand.Parameters.Add("pIdCliente", SqlDbType.Int, 10).Value = IIf(intIdCliente = 0, DBNull.Value, intIdCliente)
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Int, 10).Value = IIf(intIdCuenta = 0, DBNull.Value, intIdCuenta)
            SQLCommand.Parameters.Add("pFechaDesde", SqlDbType.Char, 10).Value = strFechaDesde
            SQLCommand.Parameters.Add("pFechaHasta", SqlDbType.Char, 10).Value = strFechaHasta

            '...Resultado
            Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 1000)
            pSalResultado.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_ComisionesCobradas, Lstr_NombreTabla)

            LDS_ComisionesCobradas_Aux = LDS_ComisionesCobradas

            If strColumnas.Trim = "" Then
                LDS_ComisionesCobradas_Aux = LDS_ComisionesCobradas
            Else
                LDS_ComisionesCobradas_Aux = LDS_ComisionesCobradas.Copy
                For Each Columna In LDS_ComisionesCobradas.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_ComisionesCobradas_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_ComisionesCobradas_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            LDS_ComisionesCobradas.Dispose()
            Return LDS_ComisionesCobradas_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function ComisionDetalle_ConsultarParaAnular(ByVal strFechaDesde As String, _
                                                        ByVal strFechaHasta As String, _
                                                        ByVal intIdCuenta As Long, _
                                                        ByVal intIdTipoComision As Long, _
                                                        ByVal strColumnas As String, _
                                                        ByRef strRetorno As String) As DataSet

        Dim LDS_Comisiones As New DataSet
        Dim LDS_Comisiones_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_ComisionDetalle_ConsultarParaAnular"
        Lstr_NombreTabla = "Comisiones"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Int, 10).Value = glngNegocioOperacion
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Int, 6).Value = glngIdUsuario
            SQLCommand.Parameters.Add("pFechaDesde", SqlDbType.Char, 10).Value = strFechaDesde
            SQLCommand.Parameters.Add("pFechaHasta", SqlDbType.Char, 10).Value = strFechaHasta
            SQLCommand.Parameters.Add("pIdTipoComision", SqlDbType.Int, 10).Value = intIdTipoComision
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Int, 10).Value = IIf(intIdCuenta = 0, DBNull.Value, intIdCuenta)

            '...Resultado
            Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 1000)
            pSalResultado.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Comisiones, Lstr_NombreTabla)

            LDS_Comisiones_Aux = LDS_Comisiones

            If strColumnas.Trim = "" Then
                LDS_Comisiones_Aux = LDS_Comisiones
            Else
                LDS_Comisiones_Aux = LDS_Comisiones.Copy
                For Each Columna In LDS_Comisiones.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Comisiones_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Comisiones_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            LDS_Comisiones.Dispose()
            Return LDS_Comisiones_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function ComisionDetalle_BuscarCom( _
                                                ByVal lngIdComisionDetalle As Long, _
                                                ByVal strFechaDesde As String, _
                                                ByVal strFechaHasta As String, _
                                                ByVal intPerMes As Integer, _
                                                ByVal intPerAno As Integer, _
                                                ByVal strTipoBusFecha As String, _
                                                ByVal intIdConceptoComision As Integer, _
                                                ByVal strCodMonedaComision As String, _
                                                ByVal strCodMonedaPago As String, _
                                                ByVal strTipoBusqueda As String, _
                                                ByVal intGeneracion As Integer, _
                                                ByVal strTipoReporte As String, _
                                                ByVal strLista As String, _
                                                ByVal strColumnas As String, _
                                                ByRef strRetorno As String) As DataSet

        Dim lDs_Comision As New DataSet
        Dim lDs_Comision_Aux As New DataSet
        Dim lArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim lInt_Col As Integer = 0
        Dim lInt_NomCol As String = ""
        'Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim lstrNombreTabla As String = ""
        Dim lstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        lstrProcedimiento = "Rcp_DetalleComisiones_Buscar"
        lstrNombreTabla = "ComisionDetalle"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = lstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Int).Value = glngIdUsuario
            SQLCommand.Parameters.Add("pIdComisionDetalle", SqlDbType.Int).Value = IIf(lngIdComisionDetalle = 0, DBNull.Value, lngIdComisionDetalle)
            SQLCommand.Parameters.Add("pFechaDesde", SqlDbType.Char, 10).Value = strFechaDesde
            SQLCommand.Parameters.Add("pFechaHasta", SqlDbType.Char, 10).Value = strFechaHasta
            SQLCommand.Parameters.Add("pPerMes", SqlDbType.Char, 10).Value = intPerMes
            SQLCommand.Parameters.Add("pPerAno", SqlDbType.Char, 10).Value = intPerAno
            SQLCommand.Parameters.Add("pTipoBuscaFecha", SqlDbType.Char, 3).Value = strTipoBusFecha
            SQLCommand.Parameters.Add("pIdConceptoComision", SqlDbType.Int).Value = IIf(intIdConceptoComision = 0, DBNull.Value, intIdConceptoComision)
            SQLCommand.Parameters.Add("pCodMoneda", SqlDbType.VarChar, 3).Value = strCodMonedaComision
            SQLCommand.Parameters.Add("pCodMonedaPago", SqlDbType.VarChar, 3).Value = strCodMonedaPago
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Int).Value = glngNegocioOperacion
            SQLCommand.Parameters.Add("pTipoBusqueda", SqlDbType.VarChar, 3).Value = strTipoBusqueda
            SQLCommand.Parameters.Add("pIdTipoGeneracion", SqlDbType.Int).Value = IIf(intGeneracion = 0, DBNull.Value, intGeneracion)
            SQLCommand.Parameters.Add("pTipoReporte", SqlDbType.VarChar, 1).Value = strTipoReporte
            SQLCommand.Parameters.Add("pLista", SqlDbType.Text, 60000).Value = IIf(strLista = "", DBNull.Value, strLista)

            '...Resultado
            Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultado.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(lDs_Comision, lstrNombreTabla)

            lDs_Comision_Aux = lDs_Comision

            If strColumnas.Trim = "" Then
                lDs_Comision_Aux = lDs_Comision
            Else
                lDs_Comision_Aux = lDs_Comision.Copy
                For Each Columna In lDs_Comision.Tables(lstrNombreTabla).Columns
                    Remove = True
                    For lInt_Col = 0 To lArr_NombreColumna.Length - 1
                        lInt_NomCol = lArr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = lInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        lDs_Comision_Aux.Tables(lstrNombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For lInt_Col = 0 To lArr_NombreColumna.Length - 1
                lInt_NomCol = lArr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                For Each Columna In lDs_Comision_Aux.Tables(lstrNombreTabla).Columns
                    If Columna.ColumnName = lInt_NomCol Then
                        Columna.SetOrdinal(lInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            lDs_Comision.Dispose()
            Return lDs_Comision_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function tipo_mov_comision_Busca(ByVal strColumnas As String, _
                                            ByVal lngid_tipo_mov_comision As Long, _
                                            ByRef strRetorno As String) As DataSet

        Dim LDS_tipo_mov_comision As New DataSet
        Dim LDS_tipo_mov_comision_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_tipo_mov_comision_Consultar"
        Lstr_NombreTabla = "tipo_mov_comision"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pid_tipo_mov_comision", SqlDbType.Int, 10).Value = IIf(lngid_tipo_mov_comision = 0, DBNull.Value, lngid_tipo_mov_comision)
            '...Resultado
            'Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            'pSalResultado.Direction = Data.ParameterDirection.Output
            'SQLCommand.Parameters.Add(pSalResultado)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_tipo_mov_comision, Lstr_NombreTabla)

            If strColumnas.Trim = "" Then
                LDS_tipo_mov_comision_Aux = LDS_tipo_mov_comision
            Else
                LDS_tipo_mov_comision_Aux = LDS_tipo_mov_comision.Copy
                For Each Columna In LDS_tipo_mov_comision.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_tipo_mov_comision_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_tipo_mov_comision_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            'strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            strRetorno = "OK"
            LDS_tipo_mov_comision.Dispose()
            Return LDS_tipo_mov_comision_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function tipo_generacion_Busca(ByVal strColumnas As String, _
                                          ByVal lngid_tipo_generacion As Long, _
                                          ByRef strRetorno As String) As DataSet

        Dim LDS_tipo_generacion As New DataSet
        Dim LDS_tipo_generacion_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_tipo_generacion_Consultar"
        Lstr_NombreTabla = "tipo_generacion"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pid_tipo_generacion", SqlDbType.Int, 10).Value = IIf(lngid_tipo_generacion = 0, DBNull.Value, lngid_tipo_generacion)
            ''...Resultado
            'Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            'pSalResultado.Direction = Data.ParameterDirection.Output
            'SQLCommand.Parameters.Add(pSalResultado)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_tipo_generacion, Lstr_NombreTabla)

            If strColumnas.Trim = "" Then
                LDS_tipo_generacion_Aux = LDS_tipo_generacion
            Else
                LDS_tipo_generacion_Aux = LDS_tipo_generacion.Copy
                For Each Columna In LDS_tipo_generacion.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_tipo_generacion_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_tipo_generacion_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            'strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            LDS_tipo_generacion.Dispose()
            Return LDS_tipo_generacion_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function ComisionDetalle_Guarda(ByVal strAccion As String, _
                                           ByVal drCommDet As DataRow, _
                                           ByVal dtMedioPagoCobro As DataTable, _
                                           ByRef strRetorno As String) As String

        Dim strDescError As String = "OK"
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction
        Dim lstrprocedure As String

        Dim lstrResultadoCadena As String = ""

        'GENERA CADENA DE MEDIOS DE COBRO/PAGOS
        Dim lstrColumnasFormaPago As String = "ID_CUENTA © " & _
                                              "ID_TIPO_ESTADO © " & _
                                              "COD_ESTADO © " & _
                                              "BANCO_ID © " & _
                                              "COD_MEDIO_PAGO_COBRO © " & _
                                              "FECHA_MOVIMIENTO © " & _
                                              "FECHA_DOCUMENTO © " & _
                                              "NUM_DOCUMENTO © " & _
                                              "RETENCION © " & _
                                              "MONTO © " & _
                                              "CTA_CTE_BANCARIA"
        Dim lstrMediosCobroPago As String = ""
        lstrResultadoCadena = ""
        If IsNothing(dtMedioPagoCobro) OrElse dtMedioPagoCobro.Rows.Count = 0 Then
            lstrMediosCobroPago = ""
        Else
            lstrResultadoCadena = GeneraCadena(dtMedioPagoCobro, lstrColumnasFormaPago, "©", lstrMediosCobroPago)
            If lstrResultadoCadena <> "OK" Then
                Return lstrResultadoCadena & vbCrLf & "En Medios de Cobro/Pago"
            End If
        End If

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion
        lstrprocedure = "Rcp_ComisionDetalle_Mantenedor"

        Try

            SQLCommand = New SqlClient.SqlCommand(lstrprocedure, MiTransaccionSQL.Connection, MiTransaccionSQL)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = lstrprocedure
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 3).Value = strAccion
            SQLCommand.Parameters.Add("pid_comision_detalle", SqlDbType.Int, 10).Value = drCommDet("ID_COMISION_DETALLE")
            SQLCommand.Parameters.Add("pid_comision", SqlDbType.Int, 10).Value = drCommDet("ID_COMISION")
            SQLCommand.Parameters.Add("pporcentaje", SqlDbType.Float, 13).Value = drCommDet("porcentaje")
            SQLCommand.Parameters.Add("pid_orden", SqlDbType.Int, 10).Value = drCommDet("id_orden")
            SQLCommand.Parameters.Add("pid_operacion_detalle", SqlDbType.Int, 10).Value = drCommDet("id_operacion_detalle")
            SQLCommand.Parameters.Add("pid_patrimonio_cuenta", SqlDbType.Float, 18).Value = drCommDet("id_patrimonio_cuenta")
            SQLCommand.Parameters.Add("pcod_estado", SqlDbType.VarChar, 3).Value = drCommDet("cod_estado")
            SQLCommand.Parameters.Add("pid_cuenta", SqlDbType.Int, 10).Value = drCommDet("id_cuenta")
            SQLCommand.Parameters.Add("pfecha_calculo", SqlDbType.VarChar, 10).Value = drCommDet("fecha_generacion")
            SQLCommand.Parameters.Add("pmonto_fijo", SqlDbType.Float, 24).Value = drCommDet("monto_fijo")
            SQLCommand.Parameters.Add("pmes_calculo", SqlDbType.Int, 2).Value = drCommDet("mes_calculo")
            SQLCommand.Parameters.Add("pano_calculo", SqlDbType.Int, 4).Value = drCommDet("ano_calculo")
            SQLCommand.Parameters.Add("pfecha_cobro", SqlDbType.VarChar, 10).Value = drCommDet("fecha_cobro")
            SQLCommand.Parameters.Add("pperiodo_inicio", SqlDbType.VarChar, 10).Value = drCommDet("periodo_inicio")
            SQLCommand.Parameters.Add("pperiodo_fin", SqlDbType.VarChar, 10).Value = drCommDet("periodo_fin")
            SQLCommand.Parameters.Add("pid_tipo_generacion", SqlDbType.Int, 10).Value = drCommDet("id_tipo_generacion")
            SQLCommand.Parameters.Add("ppor_comision_esp", SqlDbType.Float, 13).Value = drCommDet("por_comision_esp")
            SQLCommand.Parameters.Add("pvalor_prom_cartera", SqlDbType.Float, 24).Value = drCommDet("valor_prom_cartera")
            SQLCommand.Parameters.Add("pvalor_patrimonio_int", SqlDbType.Float, 24).Value = drCommDet("valor_patrimonio_int")
            SQLCommand.Parameters.Add("pcomision_calculada", SqlDbType.Float, 24).Value = drCommDet("comision_calculada")
            SQLCommand.Parameters.Add("pcomision_preneta", SqlDbType.Float, 24).Value = drCommDet("comision_preneta")
            SQLCommand.Parameters.Add("pcomision_neta", SqlDbType.Float, 24).Value = drCommDet("comision_neta")
            SQLCommand.Parameters.Add("pcomision_impuesto", SqlDbType.Float, 24).Value = drCommDet("comision_impuesto")
            SQLCommand.Parameters.Add("pgasto_corretaje", SqlDbType.Float, 24).Value = drCommDet("gasto_corretaje")
            SQLCommand.Parameters.Add("pajuste", SqlDbType.Float, 24).Value = drCommDet("ajuste")
            SQLCommand.Parameters.Add("pdevolucion", SqlDbType.Float, 24).Value = drCommDet("devolucion")
            SQLCommand.Parameters.Add("pid_medio_pago_cobro", SqlDbType.Float, 18).Value = IIf(drCommDet("id_medio_pago_cobro").ToString = "", DBNull.Value, drCommDet("id_medio_pago_cobro"))
            SQLCommand.Parameters.Add("pfolio_factura", SqlDbType.Int, 10).Value = drCommDet("folio_factura")
            SQLCommand.Parameters.Add("pid_usuario", SqlDbType.Int, 6).Value = glngIdUsuario
            SQLCommand.Parameters.Add("pid_tipo_mov_comision", SqlDbType.Int, 10).Value = drCommDet("id_tipo_mov_comision")
            SQLCommand.Parameters.Add("pId_Comision_detalle_origen", SqlDbType.Int, 10).Value = drCommDet("Id_Comision_detalle_origen")
            SQLCommand.Parameters.Add("pObservacion", SqlDbType.VarChar, 200).Value = drCommDet("Observacion")
            SQLCommand.Parameters.Add("pObservacionGral", SqlDbType.VarChar, 300).Value = drCommDet("ObservacionGral")
            SQLCommand.Parameters.Add("pid_comision_cuenta", SqlDbType.Int, 10).Value = drCommDet("id_comision_cuenta")
            SQLCommand.Parameters.Add("pMediosCobroPago", SqlDbType.VarChar, 8000).Value = IIf(lstrMediosCobroPago = "", DBNull.Value, lstrMediosCobroPago)
            SQLCommand.Parameters.Add("pId_Cuenta_Cargo", SqlDbType.Int, 10).Value = drCommDet("Id_Cuenta_Cargo")

            '...Resultado
            Dim ParametroSalComm As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSalComm.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSalComm)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If Trim(strDescError) = "OK" Then
                MiTransaccionSQL.Commit()
                Return ("OK")
            Else
                MiTransaccionSQL.Rollback()
                Return strDescError
            End If


        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error al actualizar detalle de comisiones" & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            ComisionDetalle_Guarda = strDescError
        End Try
    End Function

    Public Function RangoComision_Consultar(ByVal lngIdGrupoComisiones As Long, _
                                            ByVal lngIdComision As Long, _
                                            ByVal strColumnas As String, _
                                            ByRef strRetorno As String) As DataSet

        Dim LDS_RangoComision As New DataSet
        Dim LDS_RangoComision_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_RangoComision_Consultar"
        Lstr_NombreTabla = "ComisionDetalle"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdGrupoComisiones", SqlDbType.Int, 10).Value = IIf(lngIdGrupoComisiones = 0, DBNull.Value, lngIdGrupoComisiones)
            SQLCommand.Parameters.Add("pIdComision", SqlDbType.Int, 10).Value = IIf(lngIdComision = 0, DBNull.Value, lngIdComision)

            '...Resultado
            Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultado.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_RangoComision, Lstr_NombreTabla)

            LDS_RangoComision_Aux = LDS_RangoComision

            If strColumnas.Trim = "" Then
                LDS_RangoComision_Aux = LDS_RangoComision
            Else
                LDS_RangoComision_Aux = LDS_RangoComision.Copy
                For Each Columna In LDS_RangoComision.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_RangoComision_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_RangoComision_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            LDS_RangoComision.Dispose()
            Return LDS_RangoComision_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function


    Public Function Comision_NotaCredito(ByVal intIdCuenta As Long, _
                                         ByVal intIdComisionDetalle As Long, _
                                         ByVal intIdComisionDetalleOrigen As Long, _
                                         ByVal strColumnas As String, _
                                         ByRef strRetorno As String) As DataSet

        Dim LDS_Comisiones As New DataSet
        Dim LDS_Comisiones_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""
        Dim Lstr_NombreTabla As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Comision_NotaCredito"
        Lstr_NombreTabla = "ComisionNotaCredito"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Int, 10).Value = IIf(intIdCuenta = 0, DBNull.Value, intIdCuenta)
            SQLCommand.Parameters.Add("pIdComisionDetalle", SqlDbType.Int, 10).Value = IIf(intIdComisionDetalle = 0, DBNull.Value, intIdComisionDetalle)
            SQLCommand.Parameters.Add("pIdComisionDetalleOrigen", SqlDbType.Int, 10).Value = IIf(intIdComisionDetalleOrigen = 0, DBNull.Value, intIdComisionDetalleOrigen)

            '...Resultado
            Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 1000)
            pSalResultado.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Comisiones, Lstr_NombreTabla)

            LDS_Comisiones_Aux = LDS_Comisiones

            If strColumnas.Trim = "" Then
                LDS_Comisiones_Aux = LDS_Comisiones
            Else
                LDS_Comisiones_Aux = LDS_Comisiones.Copy
                For Each Columna In LDS_Comisiones.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Comisiones_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Comisiones_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            LDS_Comisiones.Dispose()
            Return LDS_Comisiones_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function


    Public Function ComisionMasiva_Procesa(ByVal strAccion As String, _
                                           ByVal dsComisionDetalle As DataSet, _
                                           ByVal strCuenta_Cargo As String, _
                                           ByVal dblPorcentajeEspecial As Double, _
                                           ByVal dblajuste As Double, _
                                           ByVal dblFolioContable As Double, _
                                           ByVal strGlosa As String, _
                                           ByVal strObservacion As String, _
                                           ByVal dtMedioPagoCobro As DataTable, _
                                           ByRef strRetorno As String) As DataSet

        Dim strColumnas As String = ""
        Dim LDS_Comisiones As New DataSet
        Dim LDS_Comisiones_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""
        Dim Lstr_NombreTabla As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        'GENERA CADENA DE COMISION DETALLE MASIVA 
        Dim lstrResultadoCadena As String = ""
        Dim lstrlstrColComisionDetalle As String = "ID_COMISION_DETALLE "
        Dim lstrComisionDetalle As String = ""

        If IsNothing(dsComisionDetalle) OrElse dsComisionDetalle.Tables(0).Rows.Count = 0 Then
            lstrComisionDetalle = ""
        Else
            lstrResultadoCadena = GeneraCadenaText(dsComisionDetalle.Tables(0), lstrlstrColComisionDetalle, "©", lstrComisionDetalle)
            If lstrResultadoCadena <> "OK" Then
                strRetorno = lstrResultadoCadena & vbCrLf & "Comision Detalle"
            End If
        End If
        ''''''''''''''''''''''''
        'GENERA CADENA DE MEDIOS DE COBRO/PAGOS
        Dim lstrColumnasFormaPago As String = "ID_CUENTA © " & _
                                              "ID_TIPO_ESTADO © " & _
                                              "COD_ESTADO © " & _
                                              "BANCO_ID © " & _
                                              "COD_MEDIO_PAGO_COBRO © " & _
                                              "FECHA_MOVIMIENTO © " & _
                                              "FECHA_DOCUMENTO © " & _
                                              "NUM_DOCUMENTO © " & _
                                              "RETENCION © " & _
                                              "MONTO © " & _
                                              "CTA_CTE_BANCARIA"
        Dim lstrMediosCobroPago As String = ""
        Dim lstrResultadoCadenaMCP As String = ""
        lstrResultadoCadenaMCP = ""
        If IsNothing(dtMedioPagoCobro) OrElse dtMedioPagoCobro.Rows.Count = 0 Then
            lstrMediosCobroPago = ""
        Else
            lstrResultadoCadenaMCP = GeneraCadena(dtMedioPagoCobro, lstrColumnasFormaPago, "©", lstrMediosCobroPago)
            If lstrResultadoCadenaMCP <> "OK" Then
                strRetorno = lstrResultadoCadenaMCP & vbCrLf & "En Medios de Cobro/Pago"
            End If
        End If
        ''''''''''''''''''''''''

        LstrProcedimiento = "Rcp_ComisionMasiva_Procesa"
        Lstr_NombreTabla = "ComisionMasiva"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 3).Value = strAccion
            SQLCommand.Parameters.Add("pComisionDetalle", SqlDbType.Text).Value = lstrComisionDetalle
            SQLCommand.Parameters.Add("pCuenta_Cargo", SqlDbType.VarChar, 15).Value = IIf(strCuenta_Cargo = "" Or strCuenta_Cargo = "0", DBNull.Value, strCuenta_Cargo)
            SQLCommand.Parameters.Add("pPorcentajeEspecial", SqlDbType.Float, 13).Value = IIf(dblPorcentajeEspecial = 0, DBNull.Value, dblPorcentajeEspecial)
            SQLCommand.Parameters.Add("pajuste", SqlDbType.Float, 24).Value = IIf(dblajuste = 0, DBNull.Value, dblajuste)
            SQLCommand.Parameters.Add("pFolioContable", SqlDbType.Float, 24).Value = IIf(dblFolioContable = 0, DBNull.Value, dblFolioContable)
            SQLCommand.Parameters.Add("pGlosa", SqlDbType.VarChar, 200).Value = IIf(strGlosa = "", DBNull.Value, strGlosa)
            SQLCommand.Parameters.Add("pObservacion", SqlDbType.VarChar, 300).Value = IIf(strObservacion = "", DBNull.Value, strObservacion)
            SQLCommand.Parameters.Add("pMediosCobroPago", SqlDbType.VarChar, 8000).Value = IIf(lstrMediosCobroPago = "", DBNull.Value, lstrMediosCobroPago)
            SQLCommand.Parameters.Add("pid_usuario", SqlDbType.Int, 6).Value = glngIdUsuario
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Int, 10).Value = IIf(glngNegocioOperacion = 0, DBNull.Value, glngNegocioOperacion)

            '...Resultado
            Dim ParametroSalComm As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSalComm.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSalComm)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Comisiones, Lstr_NombreTabla)

            LDS_Comisiones_Aux = LDS_Comisiones

            If strColumnas.Trim = "" Then
                LDS_Comisiones_Aux = LDS_Comisiones
            Else
                LDS_Comisiones_Aux = LDS_Comisiones.Copy
                For Each Columna In LDS_Comisiones.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Comisiones_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Comisiones_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            LDS_Comisiones.Dispose()
            Return LDS_Comisiones_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

        '-------------------------------------------------------------
        'Dim SQLCommand As New SqlClient.SqlCommand
        'Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        'Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        'Dim MiTransaccionSQL As SqlClient.SqlTransaction
        'Dim lstrprocedure As String
        'Dim lstrDescError As String = "OK"

        ''...Abre la conexion
        'SQLConnect.Abrir()
        'MiTransaccionSQL = SQLConnect.Transaccion
        'lstrprocedure = "Rcp_ComisionMasiva_Procesa"
        'Lstr_NombreTabla = "ComisionMasiva"

        'Try

        '    SQLCommand = New SqlClient.SqlCommand(lstrprocedure, MiTransaccionSQL.Connection, MiTransaccionSQL)
        '    SQLCommand.CommandType = CommandType.StoredProcedure
        '    SQLCommand.CommandText = lstrprocedure
        '    SQLCommand.Parameters.Clear()

        '    SQLCommand.ExecuteNonQuery()

        '    lstrDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

        '    If Trim(lstrDescError) = "OK" Then
        '        MiTransaccionSQL.Commit()
        '        Return ("OK")
        '    Else
        '        MiTransaccionSQL.Rollback()
        '        Return lstrDescError
        '    End If

        'Catch Ex As Exception
        '    MiTransaccionSQL.Rollback()
        '    lstrDescError = "Error al actualizar detalle de comisiones" & vbCr & Ex.Message
        'Finally
        '    SQLConnect.Cerrar()
        '    ComisionMasiva_Procesa = lstrDescError
        'End Try
        '-------------------------------------------------------------

    End Function

    Public Function ComisionExclusionInstrumento_Buscar(ByVal strCodTipoComision As String, _
                                                        ByVal intIdGrupoComisiones As Long, _
                                                        ByVal strColumnas As String, _
                                                        ByRef strRetorno As String) As DataSet

        Dim LDS_Comisiones As New DataSet
        Dim LDS_Comisiones_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""
        Dim Lstr_NombreTabla As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_ComisionExclusionInstrumento_Buscar"
        Lstr_NombreTabla = "ComisionExclusionInstrumento"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pCodTipoComision", SqlDbType.Char, 5).Value = IIf(strCodTipoComision = "", DBNull.Value, strCodTipoComision)
            SQLCommand.Parameters.Add("pIdGrupoComisiones", SqlDbType.Int, 10).Value = IIf(intIdGrupoComisiones = 0, DBNull.Value, intIdGrupoComisiones)

            '...Resultado
            Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 1000)
            pSalResultado.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Comisiones, Lstr_NombreTabla)

            LDS_Comisiones_Aux = LDS_Comisiones

            If strColumnas.Trim = "" Then
                LDS_Comisiones_Aux = LDS_Comisiones
            Else
                LDS_Comisiones_Aux = LDS_Comisiones.Copy
                For Each Columna In LDS_Comisiones.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Comisiones_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Comisiones_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            LDS_Comisiones.Dispose()
            Return LDS_Comisiones_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function ComisionExclusionInst_Actualiza(ByVal strAccion As String, _
                                                    ByVal lngid_Comision_Exclusion_Instrumento As Long, _
                                                    ByVal lngid_Grupo_Comisiones As Long, _
                                                    ByVal strCod_Clase_Instrumento As String, _
                                                    ByVal strCodigo_Familia As String, _
                                                    ByVal lngid_Instrumento As Long, _
                                                    ByVal lngid_Mercado_Transaccion As Long, _
                                                    ByVal lngid_Emisor As Long, _
                                                    ByRef strRetorno As String) As String

        Dim strDescError As String = "OK"
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction
        Dim lstrprocedure As String

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion
        lstrprocedure = "Rcp_ExclusionComisionInst_Actualiza"

        Try

            SQLCommand = New SqlClient.SqlCommand(lstrprocedure, MiTransaccionSQL.Connection, MiTransaccionSQL)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = lstrprocedure
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 3).Value = strAccion
            SQLCommand.Parameters.Add("pid_Comision_Exclusion_Instrumento", SqlDbType.Int, 10).Value = IIf(lngid_Comision_Exclusion_Instrumento = 0, DBNull.Value, lngid_Comision_Exclusion_Instrumento)
            SQLCommand.Parameters.Add("pid_Grupo_Comisiones", SqlDbType.Int, 10).Value = IIf(lngid_Grupo_Comisiones = 0, DBNull.Value, lngid_Grupo_Comisiones)
            SQLCommand.Parameters.Add("pCod_Clase_Instrumento", SqlDbType.VarChar, 15).Value = IIf(strCod_Clase_Instrumento = "", DBNull.Value, strCod_Clase_Instrumento)
            SQLCommand.Parameters.Add("pCodigo_Familia", SqlDbType.VarChar, 10).Value = IIf(strCodigo_Familia = "", DBNull.Value, strCodigo_Familia)
            SQLCommand.Parameters.Add("pid_Instrumento", SqlDbType.Int, 10).Value = IIf(lngid_Instrumento = 0, DBNull.Value, lngid_Instrumento)
            SQLCommand.Parameters.Add("pid_Mercado_Transaccion", SqlDbType.Int, 4).Value = IIf(lngid_Mercado_Transaccion = 0, DBNull.Value, lngid_Mercado_Transaccion)
            SQLCommand.Parameters.Add("pid_Emisor", SqlDbType.Int, 10).Value = IIf(lngid_Emisor = 0, DBNull.Value, lngid_Emisor)

            '...Resultado
            Dim ParametroSalComm As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSalComm.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSalComm)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If Trim(strDescError) = "OK" Then
                MiTransaccionSQL.Commit()
                Return ("OK")
            Else
                MiTransaccionSQL.Rollback()
                Return strDescError
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error al actualizar detalle de comisiones" & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            ComisionExclusionInst_Actualiza = strDescError
        End Try
    End Function

    Public Function ComisionExclusionInst_RelComDet(ByVal lngid_Comision_Exclusion_Instrumento As Long, _
                                                    ByVal lngid_Comision_Detalle As Long, _
                                                    ByVal strEstado As String, _
                                                    ByVal strColumnas As String, _
                                                    ByRef strRetorno As String) As DataSet

        Dim LDS_Comisiones As New DataSet
        Dim LDS_Comisiones_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""
        Dim Lstr_NombreTabla As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_ComisionExclusionInstrumento_RelComDet"
        Lstr_NombreTabla = "ComisionExclusionInstrumento_RelComDet"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("@pIdComisionExclusionInstrumento", SqlDbType.Int, 10).Value = IIf(lngid_Comision_Exclusion_Instrumento = 0, DBNull.Value, lngid_Comision_Exclusion_Instrumento)
            SQLCommand.Parameters.Add("@pIdComisionDetalle", SqlDbType.Int, 10).Value = IIf(lngid_Comision_Detalle = 0, DBNull.Value, lngid_Comision_Detalle)
            SQLCommand.Parameters.Add("@pEstado", SqlDbType.VarChar, 3).Value = IIf(strEstado = "", DBNull.Value, strEstado)

            '...Resultado
            Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 1000)
            pSalResultado.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Comisiones, Lstr_NombreTabla)

            LDS_Comisiones_Aux = LDS_Comisiones

            If strColumnas.Trim = "" Then
                LDS_Comisiones_Aux = LDS_Comisiones
            Else
                LDS_Comisiones_Aux = LDS_Comisiones.Copy
                For Each Columna In LDS_Comisiones.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Comisiones_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Comisiones_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            LDS_Comisiones.Dispose()
            Return LDS_Comisiones_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function ReporteComisionDetalle_Buscar( _
                                                ByVal intPerIniMes As Integer, _
                                                ByVal intPerIniAno As Integer, _
                                                ByVal intPerFinMes As Integer, _
                                                ByVal intPerFinAno As Integer, _
                                                ByVal lngidtipomovcomision As Long, _
                                                ByVal strEstado As String, _
                                                ByVal strTipoReporte As String, _
                                                ByVal strLista As String, _
                                                ByVal strColumnas As String, _
                                                ByRef strRetorno As String) As DataSet

        Dim lDsComisionDet As New DataSet
        Dim lDsComisionDet_Aux As New DataSet
        Dim lArr_NombreColumna() As String = Split(strColumnas, ",")

        'Dim lInt_Col As Integer = 0
        Dim lInt_NomCol As String = ""
        ''Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim lstrProcedimiento As String = ""
        Dim lstrNombreTabla As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        lstrProcedimiento = "Rcp_DetalleComisiones_Report"
        lstrNombreTabla = "ComisionDetalle"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = lstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pPerIniMes", SqlDbType.Int, 2).Value = intPerIniMes
            SQLCommand.Parameters.Add("pPerIniAno", SqlDbType.Int, 4).Value = intPerIniAno
            SQLCommand.Parameters.Add("pPerFinMes", SqlDbType.Int, 2).Value = intPerFinMes
            SQLCommand.Parameters.Add("pPerFinAno", SqlDbType.Int, 4).Value = intPerFinAno
            SQLCommand.Parameters.Add("pid_tipo_mov_comision", SqlDbType.Int, 10).Value = IIf(lngidtipomovcomision = 0, DBNull.Value, lngidtipomovcomision)
            SQLCommand.Parameters.Add("pEstado", SqlDbType.VarChar, 3).Value = IIf(strEstado = "", DBNull.Value, strEstado)
            SQLCommand.Parameters.Add("pTipoReporte", SqlDbType.VarChar, 1).Value = strTipoReporte
            SQLCommand.Parameters.Add("pLista", SqlDbType.Text, 60000).Value = IIf(strLista = "", DBNull.Value, strLista)
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Int, 10).Value = glngNegocioOperacion
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Int, 6).Value = glngIdUsuario

            '...Resultado
            Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultado.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(lDsComisionDet, lstrNombreTabla)

            lDsComisionDet_Aux = lDsComisionDet

            If strColumnas.Trim = "" Then
                lDsComisionDet_Aux = lDsComisionDet
            Else
                lDsComisionDet_Aux = lDsComisionDet.Copy
                For Each Columna In lDsComisionDet.Tables(lstrNombreTabla).Columns
                    Remove = True
                    For lInt_Col = 0 To lArr_NombreColumna.Length - 1
                        lInt_NomCol = lArr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = lInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        lDsComisionDet_Aux.Tables(lstrNombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For lInt_Col = 0 To lArr_NombreColumna.Length - 1
                lInt_NomCol = lArr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                For Each Columna In lDsComisionDet_Aux.Tables(lstrNombreTabla).Columns
                    If Columna.ColumnName = lInt_NomCol Then
                        Columna.SetOrdinal(lInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            lDsComisionDet.Dispose()
            Return lDsComisionDet_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function PlantillaComisionBuscar(ByVal lngIdGrupoComisiones As Long, _
                                            ByVal lngIdComision As Long, _
                                            ByVal strColumnas As String, _
                                            ByRef strRetorno As String) As DataSet

        Dim lDsComisionDet As New DataSet
        Dim lDsComisionDet_Aux As New DataSet
        Dim lArr_NombreColumna() As String = Split(strColumnas, ",")

        'Dim lInt_Col As Integer = 0
        Dim lInt_NomCol As String = ""
        ''Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim lstrProcedimiento As String = ""
        Dim lstrNombreTabla As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        lstrProcedimiento = "Rcp_DetallePlantilla_buscar"
        lstrNombreTabla = "PlantillaDetalle"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = lstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("@pID_GRUPO_COMISIONES", SqlDbType.Int, 10).Value = IIf(lngIdGrupoComisiones = 0, DBNull.Value, lngIdGrupoComisiones)
            SQLCommand.Parameters.Add("@pID_COMISION", SqlDbType.Int, 6).Value = IIf(lngIdComision = 0, DBNull.Value, lngIdComision)

            '...Resultado
            Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultado.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(lDsComisionDet, lstrNombreTabla)

            lDsComisionDet_Aux = lDsComisionDet

            If strColumnas.Trim = "" Then
                lDsComisionDet_Aux = lDsComisionDet
            Else
                lDsComisionDet_Aux = lDsComisionDet.Copy
                For Each Columna In lDsComisionDet.Tables(lstrNombreTabla).Columns
                    Remove = True
                    For lInt_Col = 0 To lArr_NombreColumna.Length - 1
                        lInt_NomCol = lArr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = lInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        lDsComisionDet_Aux.Tables(lstrNombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For lInt_Col = 0 To lArr_NombreColumna.Length - 1
                lInt_NomCol = lArr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                For Each Columna In lDsComisionDet_Aux.Tables(lstrNombreTabla).Columns
                    If Columna.ColumnName = lInt_NomCol Then
                        Columna.SetOrdinal(lInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            lDsComisionDet.Dispose()
            Return lDsComisionDet_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

End Class


