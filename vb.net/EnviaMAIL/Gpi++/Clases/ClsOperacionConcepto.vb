﻿Imports System.Data

Public Class ClsOperacionConcepto

    Public Function TipoOperacion_Concepto_Ver(ByVal strpColumnas As String, _
                                    ByVal pIdNegocio As Integer, _
                                    ByRef strpRetorno As String) As DataSet

        Dim lDS_Operaciones As New DataSet
        Dim lDS_Operaciones_Aux As New DataSet
        Dim larr_NombreColumna() As String = Split(strpColumnas, ",")
        Dim lInt_Col As Integer = 0
        Dim lInt_NomCol As String = ""
        Dim lstr_NombreTabla As String = ""
        Dim lstrColumna As DataColumn
        Dim lblnRemove As Boolean = True
        Dim lstrProcedimiento As String = ""
        Dim strDescError As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        lstrProcedimiento = "Rcp_TipoOperacionConcepto_Ver"
        lstr_NombreTabla = "OPERACION_CONCEPTO"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, Connect.Conexion)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = lstrProcedimiento
            SQLCommand.Parameters.Clear()
            'SQLCommand.Parameters.Add("pCodEstado", SqlDbType.Char, 1).Value = IIf(strCodEstado = "", DBNull.Value, strCodEstado)
            'SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Float, 10).Value = pIdNegocio
            'SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Float, 6).Value = IIf(glngIdUsuario = 0, DBNull.Value, glngIdUsuario)

            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()
            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(lDS_Operaciones, lstr_NombreTabla)

            lDS_Operaciones_Aux = lDS_Operaciones

            If strDescError.ToUpper.Trim <> "OK" Then
                gFun_ResultadoMsg("2|" & strDescError, "Buscar Cuentas")
                strpRetorno = "OK"
                lDS_Operaciones_Aux = Nothing
            Else
                If strpColumnas.Trim = "" Then
                    lDS_Operaciones_Aux = lDS_Operaciones
                Else
                    lDS_Operaciones_Aux = lDS_Operaciones.Copy
                    For Each lstrColumna In lDS_Operaciones.Tables(lstr_NombreTabla).Columns
                        lblnRemove = True
                        For lInt_Col = 0 To larr_NombreColumna.Length - 1
                            lInt_NomCol = larr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                            If lstrColumna.ColumnName = lInt_NomCol Then
                                lblnRemove = False
                            End If
                        Next
                        If lblnRemove Then
                            lDS_Operaciones_Aux.Tables(lstr_NombreTabla).Columns.Remove(lstrColumna.ColumnName)
                        End If
                    Next
                End If

                For lInt_Col = 0 To larr_NombreColumna.Length - 1
                    lInt_NomCol = larr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                    For Each lstrColumna In lDS_Operaciones_Aux.Tables(lstr_NombreTabla).Columns
                        If lstrColumna.ColumnName = lInt_NomCol Then
                            lstrColumna.SetOrdinal(lInt_Col)
                            Exit For
                        End If
                    Next
                Next

                strpRetorno = "OK"
            End If
            lDS_Operaciones.Dispose()
            Return lDS_Operaciones_Aux

        Catch ex As Exception
            strpRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function


    Public Function TipoOperacion_Concepto_Buscar(ByVal strpColumnas As String, _
                                ByVal pIdOpConcepto As Integer, _
                                ByRef strpRetorno As String) As DataSet

        Dim lDS_Operaciones As New DataSet
        Dim lDS_Operaciones_Aux As New DataSet
        Dim larr_NombreColumna() As String = Split(strpColumnas, ",")
        Dim lInt_Col As Integer = 0
        Dim lInt_NomCol As String = ""
        Dim lstr_NombreTabla As String = ""
        Dim lstrColumna As DataColumn
        Dim lblnRemove As Boolean = True
        Dim lstrProcedimiento As String = ""
        Dim strDescError As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        lstrProcedimiento = "Rcp_OperacionConcepto_Buscar"
        lstr_NombreTabla = "OPERACION_CONCEPTO"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, Connect.Conexion)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = lstrProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("pID_Operacion_Concepto", SqlDbType.Int, 20).Value = IIf(pIdOpConcepto = 0, DBNull.Value, pIdOpConcepto)
            'SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Float, 10).Value = pIdNegocio
            'SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Float, 6).Value = IIf(glngIdUsuario = 0, DBNull.Value, glngIdUsuario)

            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()
            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(lDS_Operaciones, lstr_NombreTabla)

            lDS_Operaciones_Aux = lDS_Operaciones

            If strDescError.ToUpper.Trim <> "OK" Then
                gFun_ResultadoMsg("2|" & strDescError, "Buscar Cuentas")
                strpRetorno = "OK"
                lDS_Operaciones_Aux = Nothing
            Else
                If strpColumnas.Trim = "" Then
                    lDS_Operaciones_Aux = lDS_Operaciones
                Else
                    lDS_Operaciones_Aux = lDS_Operaciones.Copy
                    For Each lstrColumna In lDS_Operaciones.Tables(lstr_NombreTabla).Columns
                        lblnRemove = True
                        For lInt_Col = 0 To larr_NombreColumna.Length - 1
                            lInt_NomCol = larr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                            If lstrColumna.ColumnName = lInt_NomCol Then
                                lblnRemove = False
                            End If
                        Next
                        If lblnRemove Then
                            lDS_Operaciones_Aux.Tables(lstr_NombreTabla).Columns.Remove(lstrColumna.ColumnName)
                        End If
                    Next
                End If

                For lInt_Col = 0 To larr_NombreColumna.Length - 1
                    lInt_NomCol = larr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                    For Each lstrColumna In lDS_Operaciones_Aux.Tables(lstr_NombreTabla).Columns
                        If lstrColumna.ColumnName = lInt_NomCol Then
                            lstrColumna.SetOrdinal(lInt_Col)
                            Exit For
                        End If
                    Next
                Next

                strpRetorno = "OK"
            End If
            lDS_Operaciones.Dispose()
            Return lDS_Operaciones_Aux

        Catch ex As Exception
            strpRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function


    Public Function Operacion_Concepto_Ver(ByVal strpColumnas As String, _
                                ByVal pIdNegocio As Integer, _
                                ByRef strpRetorno As String) As DataSet

        Dim lDS_Operaciones As New DataSet
        Dim lDS_Operaciones_Aux As New DataSet
        Dim larr_NombreColumna() As String = Split(strpColumnas, ",")
        Dim lInt_Col As Integer = 0
        Dim lInt_NomCol As String = ""
        Dim lstr_NombreTabla As String = ""
        Dim lstrColumna As DataColumn
        Dim lblnRemove As Boolean = True
        Dim lstrProcedimiento As String = ""
        Dim strDescError As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        lstrProcedimiento = "Rcp_OperacionConcepto_Ver"
        lstr_NombreTabla = "OPERACION_CONCEPTO"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, Connect.Conexion)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = lstrProcedimiento
            SQLCommand.Parameters.Clear()
            'SQLCommand.Parameters.Add("pCodEstado", SqlDbType.Char, 1).Value = IIf(strCodEstado = "", DBNull.Value, strCodEstado)
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Float, 10).Value = pIdNegocio
            'SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Float, 6).Value = IIf(glngIdUsuario = 0, DBNull.Value, glngIdUsuario)

            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()
            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(lDS_Operaciones, lstr_NombreTabla)

            lDS_Operaciones_Aux = lDS_Operaciones

            If strDescError.ToUpper.Trim <> "OK" Then
                gFun_ResultadoMsg("2|" & strDescError, "Buscar Cuentas")
                strpRetorno = "OK"
                lDS_Operaciones_Aux = Nothing
            Else
                If strpColumnas.Trim = "" Then
                    lDS_Operaciones_Aux = lDS_Operaciones
                Else
                    lDS_Operaciones_Aux = lDS_Operaciones.Copy
                    For Each lstrColumna In lDS_Operaciones.Tables(lstr_NombreTabla).Columns
                        lblnRemove = True
                        For lInt_Col = 0 To larr_NombreColumna.Length - 1
                            lInt_NomCol = larr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                            If lstrColumna.ColumnName = lInt_NomCol Then
                                lblnRemove = False
                            End If
                        Next
                        If lblnRemove Then
                            lDS_Operaciones_Aux.Tables(lstr_NombreTabla).Columns.Remove(lstrColumna.ColumnName)
                        End If
                    Next
                End If

                For lInt_Col = 0 To larr_NombreColumna.Length - 1
                    lInt_NomCol = larr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                    For Each lstrColumna In lDS_Operaciones_Aux.Tables(lstr_NombreTabla).Columns
                        If lstrColumna.ColumnName = lInt_NomCol Then
                            lstrColumna.SetOrdinal(lInt_Col)
                            Exit For
                        End If
                    Next
                Next

                strpRetorno = "OK"
            End If
            lDS_Operaciones.Dispose()
            Return lDS_Operaciones_Aux

        Catch ex As Exception
            strpRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function


    Public Function Operacion_Concepto_Mantencion(ByVal strAccion As String, _
                                ByVal strID_OPERACION_CONCEPTO As String, _
                                ByVal strCOD_TIPO_OPERACION As String, _
                                ByVal strDSC_OPERACION_CONCEPTO As String, _
                                ByVal strEST_OPERACION_CONCEPTO As String, _
                                ByVal strID_NEGOCIO As String, _
                                ByVal strCOD_CLASE_INSTRUMENTO As String, _
                                ByVal strFLG_MUEVE_CAJA As String, _
                                ByVal strFLG_MUEVE_CARTERA As String, _
                                ByVal strCODIGO_FAMILIA As String, _
                                ByVal strFLG_FLUJO_PATRIMONIAL As String, _
                                ByVal strpColumnas As String, _
                                ByVal pIdNegocio As Integer, _
                                ByRef strpRetorno As String, _
                                ByVal strFLG_PRIMERA_EMISION As String) As DataSet

        Dim lDS_Operaciones As New DataSet
        Dim lDS_Operaciones_Aux As New DataSet
        Dim larr_NombreColumna() As String = Split(strpColumnas, ",")
        Dim lInt_Col As Integer = 0
        Dim lInt_NomCol As String = ""
        Dim lstr_NombreTabla As String = ""
        Dim lstrColumna As DataColumn
        Dim lblnRemove As Boolean = True
        Dim lstrProcedimiento As String = ""
        Dim strDescError As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        lstrProcedimiento = "Rcp_OperacionConcepto_Mantencion"
        lstr_NombreTabla = "OPERACION_CONCEPTO"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, Connect.Conexion)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = lstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 20).Value = strAccion
            SQLCommand.Parameters.Add("pID_OPERACION_CONCEPTO", SqlDbType.Float, 10).Value = strID_OPERACION_CONCEPTO
            SQLCommand.Parameters.Add("pCOD_TIPO_OPERACION", SqlDbType.VarChar, 15).Value = strCOD_TIPO_OPERACION
            SQLCommand.Parameters.Add("pDSC_OPERACION_CONCEPTO", SqlDbType.VarChar, 200).Value = strDSC_OPERACION_CONCEPTO
            SQLCommand.Parameters.Add("pEST_OPERACION_CONCEPTO", SqlDbType.VarChar, 3).Value = strEST_OPERACION_CONCEPTO
            SQLCommand.Parameters.Add("pID_NEGOCIO", SqlDbType.Float, 10).Value = strID_NEGOCIO
            SQLCommand.Parameters.Add("pCOD_CLASE_INSTRUMENTO", SqlDbType.VarChar, 15).Value = strCOD_CLASE_INSTRUMENTO
            SQLCommand.Parameters.Add("pFLG_MUEVE_CAJA", SqlDbType.Char, 1).Value = strFLG_MUEVE_CAJA
            SQLCommand.Parameters.Add("pFLG_MUEVE_CARTERA", SqlDbType.Char, 1).Value = strFLG_MUEVE_CARTERA
            SQLCommand.Parameters.Add("pCODIGO_FAMILIA", SqlDbType.VarChar, 15).Value = strCODIGO_FAMILIA
            SQLCommand.Parameters.Add("pFLG_FLUJO_PATRIMONIAL", SqlDbType.Char, 1).Value = strFLG_FLUJO_PATRIMONIAL
            SQLCommand.Parameters.Add("pFLG_PRIMERA_EMISION", SqlDbType.Char, 1).Value = IIf(strFLG_PRIMERA_EMISION = "", DBNull.Value, strFLG_PRIMERA_EMISION)

            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()
            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            SQLDataAdapter.SelectCommand = SQLCommand
            'SQLDataAdapter.Fill(lDS_Operaciones, lstr_NombreTabla)

            'lDS_Operaciones_Aux = lDS_Operaciones

            If strDescError.ToUpper.Trim <> "OK" Then
                gFun_ResultadoMsg("2|" & strDescError, "Buscar Cuentas")
                strpRetorno = "OK"
                lDS_Operaciones_Aux = Nothing
            Else
                If strpColumnas.Trim = "" Then
                    lDS_Operaciones_Aux = lDS_Operaciones
                Else
                    lDS_Operaciones_Aux = lDS_Operaciones.Copy
                    For Each lstrColumna In lDS_Operaciones.Tables(lstr_NombreTabla).Columns
                        lblnRemove = True
                        For lInt_Col = 0 To larr_NombreColumna.Length - 1
                            lInt_NomCol = larr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                            If lstrColumna.ColumnName = lInt_NomCol Then
                                lblnRemove = False
                            End If
                        Next
                        If lblnRemove Then
                            lDS_Operaciones_Aux.Tables(lstr_NombreTabla).Columns.Remove(lstrColumna.ColumnName)
                        End If
                    Next
                End If

                '    'For lInt_Col = 0 To larr_NombreColumna.Length - 1
                '    '    lInt_NomCol = larr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                '    '    For Each lstrColumna In lDS_Operaciones_Aux.Tables(lstr_NombreTabla).Columns
                '    '        If lstrColumna.ColumnName = lInt_NomCol Then
                '    '            lstrColumna.SetOrdinal(lInt_Col)
                '    '            Exit For
                '    '        End If
                '    '    Next
                '    'Next

                strpRetorno = "OK"
            End If
            lDS_Operaciones.Dispose()
            Return lDS_Operaciones_Aux

        Catch ex As Exception
            strpRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function Buscar_Operacion_Concepto(ByVal INSTRUMENTO As String, ByVal COMPRA_VENTA As String, ByVal intId_Negocio As Integer, ByRef strDescError As String, Optional ByVal strDescripcion As String = "") As Integer
        Dim lstr_NombreTabla As String = ""
        Dim lstrProcedimiento As String = ""
        Dim lint_ID_Op_Con As Integer

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion

        lstrProcedimiento = "Rcp_Operacion_Concepto_Buscar"
        lstr_NombreTabla = "OPERACIONES"
        Connect.Abrir()
        Try

            SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, Connect.Conexion)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = lstrProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("@pNEMOTECNICO", SqlDbType.VarChar, 50).Value = INSTRUMENTO
            SQLCommand.Parameters.Add("@pOPERACION", SqlDbType.VarChar, 50).Value = COMPRA_VENTA
            SQLCommand.Parameters.Add("@pId_Negocio", SqlDbType.Float, 10).Value = intId_Negocio
            SQLCommand.Parameters.Add("@pDescripcion", SqlDbType.VarChar, 20).Value = IIf(strDescripcion = "", DBNull.Value, strDescripcion.ToUpper)

            Dim ParametroSal1 As New SqlClient.SqlParameter("pID_Operacion_Concepto", SqlDbType.Float)
            ParametroSal1.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal1)

            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()
            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            lint_ID_Op_Con = SQLCommand.Parameters("pID_Operacion_Concepto").Value.ToString.Trim
            If strDescError = "OK" Then
                Return lint_ID_Op_Con
            Else
                Return 0
            End If

        Catch ex As Exception
            strDescError = ex.Message
            Return 0
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function Buscar_Operacion_Concepto_x_Clase(ByVal strClase As String, ByVal COMPRA_VENTA As String, ByVal intId_Negocio As Integer, ByRef strDescError As String) As Integer
        Dim lstr_NombreTabla As String = ""
        Dim lstrProcedimiento As String = ""
        Dim lint_ID_Op_Con As Integer

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion

        lstrProcedimiento = "Rcp_Operacion_Concepto_Buscar_x_Clase"
        lstr_NombreTabla = "OPERACIONES"
        Connect.Abrir()
        Try

            SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, Connect.Conexion)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = lstrProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("pClase", SqlDbType.VarChar, 50).Value = strClase
            SQLCommand.Parameters.Add("pOPERACION", SqlDbType.VarChar, 50).Value = COMPRA_VENTA
            SQLCommand.Parameters.Add("@pId_Negocio", SqlDbType.Float, 10).Value = intId_Negocio

            Dim ParametroSal1 As New SqlClient.SqlParameter("pID_Operacion_Concepto", SqlDbType.Float)
            ParametroSal1.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal1)

            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()
            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            lint_ID_Op_Con = SQLCommand.Parameters("pID_Operacion_Concepto").Value.ToString.Trim
            If strDescError = "OK" Then
                Return lint_ID_Op_Con
            Else
                Return 0
            End If

        Catch ex As Exception
            strDescError = ex.Message
            Return 0
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function Buscar_Flg_Mueve_Cartera(ByVal pTipoOper As String, _
                                              ByVal pIdNegocio As Integer, _
                                              ByVal pCodFamilia As String, _
                                              ByRef strDescError As String) As String
        Dim lstr_NombreTabla As String = ""
        Dim lstrProcedimiento As String = ""
        Dim lstr_flg As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion

        lstrProcedimiento = "Rcp_flg_Mueve_Cartera_Buscar"
        lstr_NombreTabla = "OPERACION_CONCEPTO"
        Connect.Abrir()
        Try

            SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, Connect.Conexion)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = lstrProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("pTipoOpe", SqlDbType.VarChar, 10).Value = pTipoOper
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Float, 10).Value = pIdNegocio
            SQLCommand.Parameters.Add("pCodFamilia", SqlDbType.VarChar, 10).Value = pCodFamilia


            Dim ParametroSal1 As New SqlClient.SqlParameter("pFlgMueveCartera", SqlDbType.VarChar, 1)
            ParametroSal1.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal1)

            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()
            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            lstr_flg = SQLCommand.Parameters("pFlgMueveCartera").Value.ToString.Trim
            If strDescError = "OK" Then
                Return lstr_flg
            Else
                Return ""
            End If

        Catch ex As Exception
            strDescError = ex.Message
            Return 0
        Finally
            Connect.Cerrar()
        End Try

    End Function

End Class