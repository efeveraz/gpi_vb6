﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports Microsoft.VisualBasic

Public Class ClsContabilidad

    Public Function LibroMayor_Consultar(ByVal lngIdEmpresa As Integer, _
                                         ByVal strCtaDsd As String, _
                                         ByVal strCtaHst As String, _
                                         ByVal strFechaDesde As String, _
                                         ByVal strFechaHasta As String, _
                                         ByVal strRetorno As String) As DataSet

        Dim LDS_AtributosInstrumento As New DataSet
        Dim LDS_AtributosInstrumento_Aux As New DataSet
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = "LibroMayor"
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = "Rcp_Contab_Reporte_Libro_Mayor"
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdEmpresa", SqlDbType.Float, 9).Value = lngIdEmpresa
            SQLCommand.Parameters.Add("cuenta_desde", SqlDbType.Char, 12).Value = strCtaDsd
            SQLCommand.Parameters.Add("cuenta_hasta", SqlDbType.Char, 12).Value = strCtaHst
            SQLCommand.Parameters.Add("fecha_ingreso", SqlDbType.Char, 10).Value = strFechaDesde
            SQLCommand.Parameters.Add("fecha_termino", SqlDbType.Char, 10).Value = strFechaHasta
            '...Resultado
            'Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            'pSalResultado.Direction = Data.ParameterDirection.Output
            'SQLCommand.Parameters.Add(pSalResultado)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_AtributosInstrumento, Lstr_NombreTabla)

            LDS_AtributosInstrumento_Aux = LDS_AtributosInstrumento

            strRetorno = "Ok" 'SQLCommand.Parameters("pResultado").Value.ToString.Trim

            LDS_AtributosInstrumento.Dispose()
            Return LDS_AtributosInstrumento_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function Empresa_Buscar(ByVal lngIdEmpresa As Integer, _
                                      ByVal strDscEmpresa As String, _
                                      ByVal strColumnas As String, _
                                      ByRef strRetorno As String, _
                                      Optional ByVal strRut As String = "") As DataSet

        Dim LDS_Empresa As New DataSet
        Dim LDS_Empresa_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim Lstr_NombreTabla As String = "Empresa"
        Dim LstrProcedimiento As String = "Rcp_Contab_Empresa_Consulta"

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdEmpresa", SqlDbType.Float, 9).Value = IIf(lngIdEmpresa = 0, DBNull.Value, lngIdEmpresa)
            SQLCommand.Parameters.Add("pDscEmpresa", SqlDbType.VarChar, 50).Value = IIf(strDscEmpresa.Trim = "", DBNull.Value, strDscEmpresa.Trim)
            SQLCommand.Parameters.Add("pRut", SqlDbType.VarChar, 10).Value = IIf(strRut.Trim = "", DBNull.Value, strRut.Trim)
            SQLCommand.Parameters.Add("pidNegocio", SqlDbType.Float, 10).Value = glngNegocioOperacion

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Empresa, Lstr_NombreTabla)

            LDS_Empresa_Aux = LDS_Empresa

            strRetorno = "OK"
            Return LDS_Empresa_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function Cuenta_Buscar(ByVal strArchivo As String, _
                                      ByVal strFiltro As String, _
                                      ByRef strRetorno As String) As DataSet
        Dim LDS_Cuenta As New DataSet
        Dim LDS_Cuenta_Aux As New DataSet
        Dim Lstr_NombreTabla As String = "Cuentas"
        Dim LstrProcedimiento As String = "Rcp_Contab_Consulta_Tabla"

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("Archivo", SqlDbType.VarChar, 40).Value = strArchivo.Trim
            SQLCommand.Parameters.Add("Filtro", SqlDbType.VarChar, 20).Value = IIf(strFiltro.Trim = "", DBNull.Value, strFiltro.Trim)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Cuenta, Lstr_NombreTabla)

            LDS_Cuenta_Aux = LDS_Cuenta

            strRetorno = "OK"
            Return LDS_Cuenta_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function Voucher_Guardar(ByVal intIdEmpresa As Integer, _
                                    ByVal IntNumVoucher As Integer, _
                                    ByVal intCorrel As Integer, _
                                    ByVal StrFechaIngreso As String, _
                                    ByVal strTipoVoucher As String, _
                                    ByVal strGlosa As String, _
                                    ByVal strCuenta As String, _
                                    ByVal strCCosto As String, _
                                    ByVal strCtaRut As String, _
                                    ByVal strTDoc As String, _
                                    ByVal strReferencia As String, _
                                    ByRef IntDebe As Double, _
                                    ByRef IntHaber As Double) As String

        Dim strDescError As String = ""
        Dim strOk As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction
        Dim LstrProcedimiento As String = "Rcp_Contab_Voucher_Grabar"
        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento

            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("IdEmpresa", SqlDbType.Int, 10).Value = intIdEmpresa
            SQLCommand.Parameters.Add("numero_voucher", SqlDbType.Int, 10).Value = IntNumVoucher
            SQLCommand.Parameters.Add("correlativo", SqlDbType.Int, 3).Value = intCorrel
            SQLCommand.Parameters.Add("fecha_ingreso", SqlDbType.VarChar, 10).Value = StrFechaIngreso
            SQLCommand.Parameters.Add("tipo_voucher", SqlDbType.VarChar, 1).Value = strTipoVoucher
            SQLCommand.Parameters.Add("glosa", SqlDbType.VarChar, 70).Value = strGlosa
            SQLCommand.Parameters.Add("cuenta", SqlDbType.VarChar, 12).Value = strCuenta
            SQLCommand.Parameters.Add("centro_costo", SqlDbType.VarChar, 6).Value = strCCosto
            SQLCommand.Parameters.Add("RutCta", SqlDbType.VarChar, 15).Value = strCtaRut
            SQLCommand.Parameters.Add("TipoDoc", SqlDbType.VarChar, 6).Value = strTDoc
            SQLCommand.Parameters.Add("referencia", SqlDbType.Float, 10).Value = CDbl(strReferencia)
            SQLCommand.Parameters.Add("debe", SqlDbType.Float, 20).Value = IntDebe
            SQLCommand.Parameters.Add("haber", SqlDbType.Float, 20).Value = IntHaber

            '...Resultado
            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            strOk = strDescError.Substring(0, 2).Trim

            If strOk.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            Voucher_Guardar = strDescError
        End Try
    End Function

    Public Function Voucher_Consultar(ByVal lngIdEmpresa As Integer, _
                                      ByVal IntNumVoucher As Integer, _
                                      ByVal strRetorno As String) As DataSet

        Dim LDS_AtributosInstrumento As New DataSet
        Dim LDS_AtributosInstrumento_Aux As New DataSet
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = "Voucher"
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = "Rcp_Contab_Voucher_Consultar"
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("numero", SqlDbType.Int, 12).Value = IntNumVoucher
            SQLCommand.Parameters.Add("IdEmpresa", SqlDbType.Float, 9).Value = lngIdEmpresa

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_AtributosInstrumento, Lstr_NombreTabla)

            LDS_AtributosInstrumento_Aux = LDS_AtributosInstrumento

            strRetorno = "Ok" 'SQLCommand.Parameters("pResultado").Value.ToString.Trim

            LDS_AtributosInstrumento.Dispose()
            Return LDS_AtributosInstrumento_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function Voucher_Buscar(ByVal lngIdEmpresa As Integer, _
                                   ByVal StrFechaDsd As String, _
                                   ByVal StrFechaHst As String, _
                                   ByVal strRetorno As String) As DataSet

        Dim LDS_AtributosInstrumento As New DataSet
        Dim LDS_AtributosInstrumento_Aux As New DataSet
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = "Voucher"
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = "Rcp_Contab_Busca_Voucher"
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("IdEmpresa", SqlDbType.Float, 10).Value = lngIdEmpresa
            SQLCommand.Parameters.Add("FechaDesde", SqlDbType.VarChar, 10).Value = StrFechaDsd
            SQLCommand.Parameters.Add("FechaHasta", SqlDbType.VarChar, 10).Value = StrFechaHst

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_AtributosInstrumento, Lstr_NombreTabla)

            LDS_AtributosInstrumento_Aux = LDS_AtributosInstrumento

            strRetorno = "Ok" 'SQLCommand.Parameters("pResultado").Value.ToString.Trim

            LDS_AtributosInstrumento.Dispose()
            Return LDS_AtributosInstrumento_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function LibroDiario_Consultar(ByVal lngIdEmpresa As Integer, _
                                          ByVal strFechaDesde As String, _
                                          ByVal strFechaHasta As String, _
                                          ByVal strRetorno As String) As DataSet

        Dim LDS_AtributosInstrumento As New DataSet
        Dim LDS_AtributosInstrumento_Aux As New DataSet
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = "LibroDiario"
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = "Rcp_Contab_Reporte_Libro_Diario"
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("IdEmpresa", SqlDbType.Float, 10).Value = lngIdEmpresa
            SQLCommand.Parameters.Add("fecha_inicio", SqlDbType.Char, 10).Value = strFechaDesde
            SQLCommand.Parameters.Add("fecha_termino", SqlDbType.Char, 10).Value = strFechaHasta

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_AtributosInstrumento, Lstr_NombreTabla)

            LDS_AtributosInstrumento_Aux = LDS_AtributosInstrumento

            strRetorno = "Ok"

            LDS_AtributosInstrumento.Dispose()
            Return LDS_AtributosInstrumento_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function BalanceTributario_Consultar(ByVal lngIdEmpresa As Integer, _
                                                ByVal strFechaHasta As String, _
                                                ByVal strTipo_Calculo As String, _
                                          ByVal strRetorno As String) As DataSet

        Dim LDS_AtributosInstrumento As New DataSet
        Dim LDS_AtributosInstrumento_Aux As New DataSet
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = "BalanceTributario"
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = "Rcp_Contab_Reporte_Balance_Tributario"
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("IdEmpresa", SqlDbType.Float, 10).Value = lngIdEmpresa
            SQLCommand.Parameters.Add("Fecha_Hasta", SqlDbType.Char, 10).Value = strFechaHasta
            SQLCommand.Parameters.Add("Tipo_Calculo", SqlDbType.Char, 1).Value = strTipo_Calculo

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_AtributosInstrumento, Lstr_NombreTabla)

            LDS_AtributosInstrumento_Aux = LDS_AtributosInstrumento

            strRetorno = "Ok"

            LDS_AtributosInstrumento.Dispose()
            Return LDS_AtributosInstrumento_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function PlanCuenta_Consultar(ByVal strRetorno As String) As DataSet

        Dim LDS_AtributosInstrumento As New DataSet
        Dim LDS_AtributosInstrumento_Aux As New DataSet
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = "PlanCuenta"
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = "Rcp_Contab_Cuenta_Contable_Consultar"
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_AtributosInstrumento, Lstr_NombreTabla)

            LDS_AtributosInstrumento_Aux = LDS_AtributosInstrumento

            strRetorno = "Ok"

            LDS_AtributosInstrumento.Dispose()
            Return LDS_AtributosInstrumento_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function Plan_Cuenta_Mantenedor(ByVal StrAccion As String, _
                                        ByVal strCuenta As String, _
                                        ByVal StrDescripcion As String, _
                                        ByVal strTipoCuenta As String, _
                                        ByVal strImputable As String, _
                                        ByVal strCorrecion As String, _
                                        ByVal strCCosto As String) As String

        Dim strDescError As String = ""
        Dim strOk As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction
        Dim LstrProcedimiento As String = "Rcp_Contab_Cuenta_Contable_Grabar"
        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento

            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("Accion", SqlDbType.VarChar, 10).Value = StrAccion
            SQLCommand.Parameters.Add("Cuenta", SqlDbType.VarChar, 12).Value = strCuenta
            SQLCommand.Parameters.Add("descripcion", SqlDbType.VarChar, 70).Value = StrDescripcion
            SQLCommand.Parameters.Add("tipo_cuenta", SqlDbType.VarChar, 3).Value = strTipoCuenta
            SQLCommand.Parameters.Add("Cuenta_Imputable", SqlDbType.VarChar, 1).Value = strImputable
            SQLCommand.Parameters.Add("Con_Correccion", SqlDbType.VarChar, 1).Value = strCorrecion
            SQLCommand.Parameters.Add("Con_Centro_Costo", SqlDbType.VarChar, 1).Value = strCCosto

            '...Resultado
            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            strOk = strDescError.Substring(0, 2).Trim

            If strOk.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error: " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            Plan_Cuenta_Mantenedor = strDescError
        End Try
    End Function
    
    Public Function Empresa_Mantenedor(ByVal StrAccion As String, _
                                       ByVal intIdEmpresa As Integer, _
                                       ByVal strRut As String, _
                                       ByVal strNombre As String, _
                                       ByVal strFecha As String, _
                                       ByVal dblNvoucher As Double, _
                                       ByVal intAnoVoucher As Integer, _
                                       ByVal strRegistro As String, _
                                       ByVal strCuenta As String, _
                                       ByVal id_Negocio As Integer, _
                                       ByVal strCalculaCuota As String, _
                                       ByVal dblCuotaEmitida As Double, _
                                       ByVal dblCuotaCirc As Double, _
                                       ByVal strTipoRemu As String, _
                                       ByVal dblPorcMonto As Double, _
                                       ByVal strCodMoneda As String, _
                                       ByVal dblBase As Double) As String

        Dim strDescError As String = ""
        Dim strOk As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction
        Dim LstrProcedimiento As String = "Rcp_Contab_Empresa_Grabar"
        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento

            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("Accion", SqlDbType.VarChar, 10).Value = StrAccion
            SQLCommand.Parameters.Add("IdEmpresa", SqlDbType.Int, 10).Value = intIdEmpresa
            SQLCommand.Parameters.Add("RutEmpresa", SqlDbType.VarChar, 15).Value = strRut
            SQLCommand.Parameters.Add("NombreEmpresa", SqlDbType.VarChar, 100).Value = strNombre
            SQLCommand.Parameters.Add("FechaHoy", SqlDbType.VarChar, 10).Value = strFecha
            SQLCommand.Parameters.Add("NumeroVoucher", SqlDbType.Float, 10).Value = IIf(dblNvoucher = 0, DBNull.Value, dblNvoucher)
            SQLCommand.Parameters.Add("AnoApertura", SqlDbType.Int, 10).Value = intAnoVoucher
            SQLCommand.Parameters.Add("Registro", SqlDbType.VarChar, 10).Value = IIf(strRegistro = "", DBNull.Value, strRegistro)
            SQLCommand.Parameters.Add("Cuenta", SqlDbType.VarChar, 12).Value = IIf(strCuenta = "", DBNull.Value, strCuenta)
            SQLCommand.Parameters.Add("idNegocio", SqlDbType.Int, 10).Value = id_Negocio
            SQLCommand.Parameters.Add("CalculaCuota", SqlDbType.VarChar, 1).Value = strCalculaCuota
            SQLCommand.Parameters.Add("CuotaEmitida", SqlDbType.Float, 10).Value = dblCuotaEmitida
            SQLCommand.Parameters.Add("CuotaCircu", SqlDbType.Float, 10).Value = dblCuotaCirc
            SQLCommand.Parameters.Add("TipoRemu", SqlDbType.VarChar, 1).Value = IIf(strTipoRemu = "", DBNull.Value, strTipoRemu)
            SQLCommand.Parameters.Add("PorcMonto", SqlDbType.Float, 10).Value = IIf(dblPorcMonto = 0, DBNull.Value, dblPorcMonto)
            SQLCommand.Parameters.Add("CodMoneda", SqlDbType.VarChar, 3).Value = IIf(strCodMoneda = "", DBNull.Value, strCodMoneda)
            SQLCommand.Parameters.Add("Base", SqlDbType.Float, 10).Value = IIf(dblBase = 0, DBNull.Value, dblBase)

            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            strOk = strDescError.Substring(0, 2).Trim

            If strOk.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error: " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            Empresa_Mantenedor = strDescError
        End Try
    End Function

    Public Function ValorCuotaPatrimonio_Consultar(ByVal lngIdEmpresa As Integer, _
                                          ByVal strFechaDesde As String, _
                                          ByVal strFechaHasta As String, _
                                          ByVal strRetorno As String) As DataSet

        Dim LDS_AtributosInstrumento As New DataSet
        Dim LDS_AtributosInstrumento_Aux As New DataSet
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = "LibroDiario"
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = "Rcp_Contab_Reporte_ValorCuota_Patrimonio"
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("IdEmpresa", SqlDbType.Float, 10).Value = lngIdEmpresa
            SQLCommand.Parameters.Add("fecha_inicio", SqlDbType.Char, 10).Value = strFechaDesde
            SQLCommand.Parameters.Add("fecha_termino", SqlDbType.Char, 10).Value = strFechaHasta

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_AtributosInstrumento, Lstr_NombreTabla)

            LDS_AtributosInstrumento_Aux = LDS_AtributosInstrumento

            strRetorno = "Ok"

            LDS_AtributosInstrumento.Dispose()
            Return LDS_AtributosInstrumento_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function UltRemu_Buscar(ByVal lngIdEmpresa As Integer, _
                                   ByVal lstrFechaCierre As String, _
                                   ByRef strRetorno As String) As DataSet

        Dim LDS_Empresa As New DataSet
        Dim LDS_Empresa_Aux As New DataSet
        Dim Lstr_NombreTabla As String = "REMU"
        Dim LstrProcedimiento As String = "Rcp_Contab_Ult_Remu_Buscar"

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdEmpresa", SqlDbType.Float, 9).Value = IIf(lngIdEmpresa = 0, DBNull.Value, lngIdEmpresa)
            SQLCommand.Parameters.Add("pFechaCierre", SqlDbType.VarChar, 10).Value = lstrFechaCierre

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Empresa, Lstr_NombreTabla)

            LDS_Empresa_Aux = LDS_Empresa

            strRetorno = "OK"
            Return LDS_Empresa_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

End Class
