﻿Public Class ClsRetiroProgramado
    Public Function Carga_Grilla_RetiroProgramado_Flujos(ByVal strFechadesde As String, _
                                                         ByVal strFechahasta As String, _
                                                         ByVal intPeriodo As Integer, _
                                                         ByVal dblMonto As Double, _
                                                         ByVal intDiasPago As Integer, _
                                                         ByVal intNumeroFlujo As Integer, _
                                                         ByVal strLiquidacion As String, _
                                                         ByVal strColumnas As String, _
                                                         ByRef strRetorno As String) As DataSet
        'Devuelve los datos a la Grilla

        Dim LDS_ImportaD As New DataSet
        Dim LDS_ImportaD_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_ApRetProgramado_Genera_Flujos"
        Lstr_NombreTabla = "Retiros"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("pFechadesde", SqlDbType.Char, 10).Value = strFechadesde
            SQLCommand.Parameters.Add("pFechaTermino", SqlDbType.Char, 10).Value = IIf(strFechahasta = "", DBNull.Value, strFechahasta)
            SQLCommand.Parameters.Add("pPeriodo", SqlDbType.Int, 1).Value = IIf(intPeriodo = 0, DBNull.Value, intPeriodo)
            SQLCommand.Parameters.Add("pMonto", SqlDbType.Float, 20).Value = IIf(dblMonto = 0, DBNull.Value, dblMonto)
            SQLCommand.Parameters.Add("pDiasPago", SqlDbType.Int, 2).Value = IIf(intDiasPago = 0, DBNull.Value, intDiasPago)
            SQLCommand.Parameters.Add("pNumeroFlujo", SqlDbType.Int, 2).Value = IIf(intNumeroFlujo = 0, DBNull.Value, intNumeroFlujo)
            SQLCommand.Parameters.Add("pTipoLiquida", SqlDbType.Char, 1).Value = IIf(strLiquidacion = "", DBNull.Value, strLiquidacion)

            Dim ParametroSal2 As New SqlClient.SqlParameter("pRetorno", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_ImportaD, Lstr_NombreTabla)

            LDS_ImportaD_Aux = LDS_ImportaD

            If strColumnas.Trim = "" Then
                LDS_ImportaD_Aux = LDS_ImportaD
            Else
                LDS_ImportaD_Aux = LDS_ImportaD.Copy
                For Each Columna In LDS_ImportaD.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_ImportaD_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_ImportaD_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next



            strRetorno = "OK"
            LDS_ImportaD.Dispose()
            Return LDS_ImportaD_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function
    Public Sub Graba_Retiros_Programados(ByVal strRetiros As String, _
                                         ByVal strlTipo_Operacion As String, _
                                         ByVal dblIdRetiro As Double, _
                                         ByVal DblIdCuena As Double, _
                                         ByVal StrFechaDesde As String, _
                                         ByVal strFlgTermino As String, _
                                         ByVal StrFechaHasta As String, _
                                         ByVal strPeriodo As String, _
                                         ByVal intDiasRetencion As Integer, _
                                         ByVal strMoneda As String, _
                                         ByVal strMonedaMonto As String, _
                                         ByVal dblcantidadoperacion As Double, _
                                         ByVal strFlgRenovacion As String, _
                                         ByVal strobservacion As String, _
                                         ByVal intnumeroflujos As Integer, _
                                         ByVal strCodMedioPago As String, _
                                         ByVal strDetallePago As String, _
                                         ByVal strFechaSolicitud As String, _
                                         ByVal strFlgFechaLiquida As String, _
                                         ByVal strEstado As String, _
                                         ByVal strFechaAnula As String, _
                                         ByVal intUsuarioAnula As Double, _
                                         ByVal strFechaSuspencion As String, _
                                         ByVal straccion As String, _
                                         ByRef Reg_Leidos As Integer, _
                                         ByRef Reg_Grabados As Integer, _
                                         ByRef strRetorno As String)
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim lstrProc As String = "Rcp_RetApo_Programado_Mantencion"

        'Abre la conexion
        Connect.Abrir()

        Dim MiTransaccion As SqlClient.SqlTransaction

        Reg_Leidos = 0
        Reg_Grabados = 0

        MiTransaccion = Connect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand(lstrProc, MiTransaccion.Connection, MiTransaccion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = lstrProc

            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pRetiros ", SqlDbType.Text).Value = strRetiros
            SQLCommand.Parameters.Add("pTIPO_RETIRO", SqlDbType.Text).Value = IIf(strlTipo_Operacion = "", DBNull.Value, strlTipo_Operacion)
            SQLCommand.Parameters.Add("pID_RETIRO_ENC ", SqlDbType.Float).Value = IIf(dblIdRetiro = 0, DBNull.Value, dblIdRetiro)
            SQLCommand.Parameters.Add("pID_CUENTA", SqlDbType.Float).Value = IIf(DblIdCuena = 0, DBNull.Value, DblIdCuena)
            SQLCommand.Parameters.Add("pFECHA_INICIO", SqlDbType.Char, 10).Value = StrFechaDesde
            SQLCommand.Parameters.Add("pFlgTermino", SqlDbType.Text).Value = IIf(strFlgTermino = "", DBNull.Value, strFlgTermino)
            SQLCommand.Parameters.Add("pFECHA_TERMINO", SqlDbType.Char, 10).Value = IIf(StrFechaHasta = "", DBNull.Value, StrFechaHasta)
            SQLCommand.Parameters.Add("pTIPO_PERIODO", SqlDbType.Text).Value = IIf(strPeriodo = "", DBNull.Value, strPeriodo)
            SQLCommand.Parameters.Add("pDIA_PAGO", SqlDbType.Int).Value = IIf(intDiasRetencion = 0, DBNull.Value, intDiasRetencion)
            SQLCommand.Parameters.Add("pCOD_MONEDA", SqlDbType.Text).Value = IIf(strMoneda = "", DBNull.Value, strMoneda)
            SQLCommand.Parameters.Add("pCOD_MONEDA_MONTO", SqlDbType.Text).Value = IIf(strMonedaMonto = "", DBNull.Value, strMonedaMonto)
            SQLCommand.Parameters.Add("pMONTO", SqlDbType.Float).Value = IIf(dblcantidadoperacion = 0, DBNull.Value, dblcantidadoperacion)
            SQLCommand.Parameters.Add("pFLG_RENOVACION", SqlDbType.Text).Value = IIf(strFlgRenovacion = "", DBNull.Value, strFlgRenovacion)
            SQLCommand.Parameters.Add("pGLOSA", SqlDbType.Text).Value = IIf(strobservacion = "", DBNull.Value, strobservacion)
            SQLCommand.Parameters.Add("pFECHA_INGRESO", SqlDbType.Text).Value = IIf(gstrFechaSistema = "", DBNull.Value, gstrFechaSistema)
            SQLCommand.Parameters.Add("pUSUARIO_INGRESO", SqlDbType.Float, 10).Value = glngIdUsuario
            SQLCommand.Parameters.Add("pNumero_Flujos", SqlDbType.Int).Value = IIf(intnumeroflujos = 0, DBNull.Value, intnumeroflujos)
            SQLCommand.Parameters.Add("pCod_Medio_Pago_cobro", SqlDbType.Text).Value = IIf(strCodMedioPago = "", DBNull.Value, strCodMedioPago)
            SQLCommand.Parameters.Add("pDetalle_Tipo_Cobro", SqlDbType.Text).Value = IIf(strDetallePago = "", DBNull.Value, strDetallePago)
            SQLCommand.Parameters.Add("pFecha_Solicitud", SqlDbType.Char, 10).Value = IIf(strFechaSolicitud = "", DBNull.Value, strFechaSolicitud)
            SQLCommand.Parameters.Add("pIndica_Liquidacion", SqlDbType.Text).Value = IIf(strFlgFechaLiquida = "", DBNull.Value, strFlgFechaLiquida)
            SQLCommand.Parameters.Add("pEstado", SqlDbType.Char, 3).Value = IIf(strEstado = "", DBNull.Value, strEstado)
            SQLCommand.Parameters.Add("pFechaAnulacion", SqlDbType.Text).Value = IIf(strFechaAnula = "", DBNull.Value, strFechaAnula)
            SQLCommand.Parameters.Add("pIDUsuarioAnula", SqlDbType.Float, 10).Value = IIf(intUsuarioAnula = 0, DBNull.Value, intUsuarioAnula)
            SQLCommand.Parameters.Add("pFechaSuspencion", SqlDbType.Text).Value = IIf(strFechaSuspencion = "", DBNull.Value, strFechaSuspencion)

            SQLCommand.Parameters.Add("pAccion", SqlDbType.Text).Value = IIf(straccion = "", DBNull.Value, straccion)

            Dim pReg_Leidos As New SqlClient.SqlParameter("pReg_Leidos", SqlDbType.Int, 10)
            pReg_Leidos.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pReg_Leidos).Value = Reg_Leidos

            Dim pReg_Grabados As New SqlClient.SqlParameter("pReg_Grabados", SqlDbType.Int, 10)
            pReg_Grabados.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pReg_Grabados).Value = Reg_Grabados

            Dim ParametroSal2 As New SqlClient.SqlParameter("pError", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()

            Reg_Leidos = SQLCommand.Parameters("pReg_Leidos").Value
            Reg_Grabados = SQLCommand.Parameters("pReg_Grabados").Value
            strRetorno = SQLCommand.Parameters("pError").Value.ToString.Trim


            If Trim(strRetorno) = "OK" Then
                MiTransaccion.Commit()
            Else
                MiTransaccion.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccion.Rollback()
            strRetorno = "Error al grabar Aportes y Retiros Programados." & vbCr & Ex.Message
        Finally
            Connect.Cerrar()
        End Try
    End Sub


    Public Function Carga_Grilla_RetiroProgramado(ByVal strFechadesde As String, _
                                                         ByVal strFechahasta As String, _
                                                         ByVal dblIDRetiro As Double, _
                                                         ByVal strAccion As String, _
                                                         ByVal StrTipo As String, _
                                                         ByVal strColumnas As String, _
                                                         ByRef strRetorno As String) As DataSet
        'Devuelve los datos a la Grilla

        Dim LDS_ImportaD As New DataSet
        Dim LDS_ImportaD_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_RetiroProgramado_Buscar"
        Lstr_NombreTabla = "AporteRetiro"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("pFechadesde", SqlDbType.Char, 10).Value = strFechadesde
            SQLCommand.Parameters.Add("pFechahasta", SqlDbType.Char, 10).Value = strFechahasta
            SQLCommand.Parameters.Add("pID_RETIRO_ENC", SqlDbType.Float, 20).Value = IIf(dblIDRetiro = 0, DBNull.Value, dblIDRetiro)
            SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 20).Value = IIf(strAccion = "", DBNull.Value, strAccion)
            SQLCommand.Parameters.Add("pTipo", SqlDbType.Char, 1).Value = IIf(StrTipo = "", DBNull.Value, StrTipo)
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Int, 10).Value = glngNegocioOperacion

            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_ImportaD, Lstr_NombreTabla)

            LDS_ImportaD_Aux = LDS_ImportaD

            If strColumnas.Trim = "" Then
                LDS_ImportaD_Aux = LDS_ImportaD
            Else
                LDS_ImportaD_Aux = LDS_ImportaD.Copy
                For Each Columna In LDS_ImportaD.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_ImportaD_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_ImportaD_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next



            strRetorno = "OK"
            LDS_ImportaD.Dispose()
            Return LDS_ImportaD_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function Vencimientos_Consultar(ByRef strRetorno As String, _
                                           ByVal strFechaInicio As String, _
                                           ByVal strFechaTermino As String, _
                                           ByVal strColumnas As String) As DataSet

        Dim lDS_Vencimiento As New DataSet
        Dim lDS_Vencimiento_Aux As New DataSet

        Dim Sqlcommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim Lstr_Procedimiento As String = ""
        Dim Lstr_NombreTabla As String = ""

        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True


        Lstr_Procedimiento = "Rcp_AporteRetiro_VencimientosBuscar"
        Lstr_NombreTabla = "VENCIMIENTO_APORTERETIRO"
        Connect.Abrir()

        Try
            Sqlcommand = New SqlClient.SqlCommand(Lstr_Procedimiento, Connect.Conexion)

            Sqlcommand.CommandType = CommandType.StoredProcedure
            Sqlcommand.CommandText = Lstr_Procedimiento
            Sqlcommand.Parameters.Clear()
            Sqlcommand.Parameters.Add("@pFechaInicio", SqlDbType.VarChar, 10).Value = strFechaInicio
            Sqlcommand.Parameters.Add("@pFechaTermino", SqlDbType.VarChar, 10).Value = strFechaTermino
            Sqlcommand.Parameters.Add("@pIDNegocio", SqlDbType.Int, 10).Value = glngNegocioOperacion

            Dim oParamSal As New SqlClient.SqlParameter("@pResultado", SqlDbType.VarChar, 200)
            oParamSal.Direction = ParameterDirection.Output
            Sqlcommand.Parameters.Add(oParamSal)

            SQLDataAdapter.SelectCommand = Sqlcommand
            SQLDataAdapter.Fill(lDS_Vencimiento, Lstr_NombreTabla)

            lDS_Vencimiento_Aux = lDS_Vencimiento

            If strColumnas.Trim = "" Then
                lDS_Vencimiento_Aux = lDS_Vencimiento
            Else
                lDS_Vencimiento_Aux = lDS_Vencimiento.Copy
                For Each Columna In lDS_Vencimiento.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        lDS_Vencimiento_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In lDS_Vencimiento_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"

            Return lDS_Vencimiento_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function Procesar_AporteRetiros(ByVal strDesde As String, _
                                           ByVal strHasta As String, _
                                           ByVal strListaAporteRetiro As String, _
                                           ByVal strColumnas As String, _
                                           ByRef strRetorno As String) As DataSet

        Dim lDS_Vencimiento As New DataSet
        Dim lDS_Vencimiento_Aux As New DataSet

        Dim Sqlcommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim Lstr_Procedimiento As String = ""
        Dim Lstr_NombreTabla As String = ""

        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim strDescError As String


        Lstr_Procedimiento = "Rcp_Procesa_Vencimiento_AporteRetiro"
        Lstr_NombreTabla = "VCTO_APORTERETIRO"
        Connect.Abrir()


        Try
            Sqlcommand = New SqlClient.SqlCommand(Lstr_Procedimiento, Connect.Conexion)

            Sqlcommand.CommandType = CommandType.StoredProcedure
            Sqlcommand.CommandTimeout = 0
            Sqlcommand.CommandText = Lstr_Procedimiento
            Sqlcommand.Parameters.Clear()

            Sqlcommand.Parameters.Add("@pFechaDesde", SqlDbType.VarChar, 10).Value = strDesde
            Sqlcommand.Parameters.Add("@pFechaHasta", SqlDbType.VarChar, 10).Value = strHasta
            Sqlcommand.Parameters.Add("@pListaAporteRetiro", SqlDbType.Text).Value = strListaAporteRetiro


            Dim oParamSal As New SqlClient.SqlParameter("@pResultado", SqlDbType.VarChar, 8000)
            oParamSal.Direction = ParameterDirection.Output
            Sqlcommand.Parameters.Add(oParamSal)

            SQLDataAdapter.SelectCommand = Sqlcommand
            SQLDataAdapter.Fill(lDS_Vencimiento, Lstr_NombreTabla)

            lDS_Vencimiento_Aux = lDS_Vencimiento

            strDescError = Sqlcommand.Parameters("@pResultado").Value.ToString.Trim

            If strDescError <> "OK" Then
                strRetorno = strDescError
                Return lDS_Vencimiento_Aux
            End If

            If strColumnas.Trim = "" Then
                lDS_Vencimiento_Aux = lDS_Vencimiento
            Else
                lDS_Vencimiento_Aux = lDS_Vencimiento.Copy
                For Each Columna In lDS_Vencimiento.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        lDS_Vencimiento_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In lDS_Vencimiento_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next



            strRetorno = "OK"

            Return lDS_Vencimiento_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function


End Class
