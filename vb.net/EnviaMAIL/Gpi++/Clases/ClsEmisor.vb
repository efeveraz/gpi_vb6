﻿Public Class ClsEmisor

    Public Function Emisor_Ver(ByVal strCodigo As String, _
                               ByVal strColumnas As String, _
                               ByVal strEstado As String, _
                               ByRef strRetorno As String) As DataSet

        Dim LDS_Emisor As New DataSet
        Dim LDS_Emisor_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Emisor_Consultar"
        Lstr_NombreTabla = "EMISOR"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("pIdEmisor", SqlDbType.VarChar, 3).Value = IIf(strCodigo.Trim = "", DBNull.Value, strCodigo.Trim)
            SQLCommand.Parameters.Add("pEstado", SqlDbType.VarChar, 3).Value = IIf(strEstado.Trim = "", DBNull.Value, strEstado.Trim)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Emisor, Lstr_NombreTabla)

            LDS_Emisor_Aux = LDS_Emisor

            If strColumnas.Trim = "" Then
                LDS_Emisor_Aux = LDS_Emisor
            Else
                LDS_Emisor_Aux = LDS_Emisor.Copy
                For Each Columna In LDS_Emisor.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Emisor_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Emisor_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_Emisor.Dispose()
            Return LDS_Emisor_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function Emisor_Mantenedor(ByVal strAccion As String, _
                                               ByVal intIdEmisor As Integer, _
                                               ByVal strCodTipo As String, _
                                               ByVal intIdGrupo As Integer, _
                                               ByVal intIdSector As Integer, _
                                               ByVal strCodPais As String, _
                                               ByVal strDescripcion As String, _
                                               ByVal strCodEmisor As String, _
                                               ByVal srtCodSvsNemo As String, _
                                               ByVal strRut As String, _
                                               ByVal strNombreCorto As String, _
                                               ByVal strCodigoNorma As String, _
                                               ByRef strEstado As String, _
                                               ByRef dblIdgenerado As Double) As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_Emisor_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Emisor_Mantencion"
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 10).Value = strAccion
            SQLCommand.Parameters.Add("pIdEmisor", SqlDbType.Float, 10).Value = intIdEmisor
            SQLCommand.Parameters.Add("pCodTipoEmisor", SqlDbType.VarChar, 1).Value = IIf(strCodTipo = "", DBNull.Value, strCodTipo)
            SQLCommand.Parameters.Add("pIdGrupoEconomico", SqlDbType.Float, 4).Value = intIdGrupo
            SQLCommand.Parameters.Add("pIdSectorEconomico", SqlDbType.Float, 4).Value = intIdSector
            SQLCommand.Parameters.Add("pCodPais", SqlDbType.VarChar, 3).Value = IIf(strCodPais.Trim = "", DBNull.Value, strCodPais.Trim)
            SQLCommand.Parameters.Add("pDscEmisor", SqlDbType.VarChar, 100).Value = strDescripcion
            SQLCommand.Parameters.Add("pCodEmisor", SqlDbType.VarChar, 10).Value = strCodEmisor
            SQLCommand.Parameters.Add("pCodSvsNemo", SqlDbType.VarChar, 10).Value = srtCodSvsNemo
            SQLCommand.Parameters.Add("pRut", SqlDbType.VarChar, 10).Value = strRut
            SQLCommand.Parameters.Add("pNombreCorto", SqlDbType.VarChar, 15).Value = strNombreCorto
            SQLCommand.Parameters.Add("pCodNorma", SqlDbType.VarChar, 20).Value = strCodigoNorma
            SQLCommand.Parameters.Add("pEstado", SqlDbType.VarChar, 3).Value = strEstado



            '...(Identity)
            Dim pSalInstr As New SqlClient.SqlParameter("pGenerado", SqlDbType.Float, 10)
            pSalInstr.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalInstr)

            '...Resultado
            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()
            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            dblIdgenerado = SQLCommand.Parameters("pGenerado").Value


            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            Emisor_Mantenedor = strDescError
        End Try
    End Function


    Public Function Buscar_Emisor(ByVal lIntIdEmisor As Integer, _
                                  ByVal StrDescripcion_Emisor As String, _
                                  ByVal StrRutEmisor As String, _
                                  ByVal strColumnas As String, _
                                  ByRef strRetorno As String) As DataSet



        Dim LDS_Emisor As New DataSet
        Dim LDS_Emisor_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Emisor_Buscar"
        Lstr_NombreTabla = "EMISOR"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("pIdEmisor", SqlDbType.Float, 10).Value = IIf(lIntIdEmisor = 0, DBNull.Value, lIntIdEmisor)
            SQLCommand.Parameters.Add("pDescripcionEmisor", SqlDbType.VarChar, 100).Value = IIf(StrDescripcion_Emisor.Trim = "", DBNull.Value, StrDescripcion_Emisor.Trim)
            SQLCommand.Parameters.Add("pRutEmisor", SqlDbType.VarChar, 3).Value = IIf(StrRutEmisor.Trim = "", DBNull.Value, StrRutEmisor.Trim)


            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Emisor, Lstr_NombreTabla)

            LDS_Emisor_Aux = LDS_Emisor

            If strColumnas.Trim = "" Then
                LDS_Emisor_Aux = LDS_Emisor
            Else
                LDS_Emisor_Aux = LDS_Emisor.Copy
                For Each Columna In LDS_Emisor.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Emisor_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Emisor_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_Emisor.Dispose()
            Return LDS_Emisor_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function Buscar_Datos_Emisor_x_CodBolsa(ByVal pCodBolsa As String, _
                              ByVal strColumnas As String, _
                              ByRef strRetorno As String) As DataSet
        'Buscar_Datos_Emisor_x_CodBolsa(lstrEmisor_Arc, lstrColumnas, strMensaje)


        Dim LDS_Emisor As New DataSet
        Dim LDS_Emisor_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "dbo.Rcp_Emisor_Buscar_Cod_Bolsa"
        Lstr_NombreTabla = "EMISOR"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("pCod_Bolsa", SqlDbType.VarChar, 100).Value = pCodBolsa
            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Emisor, Lstr_NombreTabla)

            LDS_Emisor_Aux = LDS_Emisor

            If strColumnas.Trim = "" Then
                LDS_Emisor_Aux = LDS_Emisor
            Else
                LDS_Emisor_Aux = LDS_Emisor.Copy
                For Each Columna In LDS_Emisor.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Emisor_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Emisor_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_Emisor.Dispose()
            Return LDS_Emisor_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

End Class


