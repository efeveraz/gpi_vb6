﻿Imports Microsoft.Office.Interop.Excel

Public Class ClsColumnaExcel

    Public ColumnaVinculada As String

    Public TituloColumna As String

    Public AlineacionHorizontal As XlHAlign

    Public AlineacionVertical As XlVAlign

    Public FormatoColumna As String

    Public Sub New(ByVal _columnaVinculada As String, _
                   ByVal _tituloColumna As String, _
                   ByVal _alignHorizontal As XlHAlign, _
                   ByVal _alignVertical As XlVAlign, _
                   ByVal _formatoColumna As String)
        Me.ColumnaVinculada = _columnaVinculada
        Me.TituloColumna = _tituloColumna
        Me.AlineacionHorizontal = _alignHorizontal
        Me.AlineacionVertical = _alignVertical
        Me.FormatoColumna = _formatoColumna
    End Sub

End Class
