﻿Imports System.Data.SqlClient
Imports System.Data


Public Class ClsVistaActivos


#Region "Declaraciones de variables"

    Dim sProcedimiento As String = ""
    ' variables de propiedades
    Private _strTipoBusqueda As String

    Private _strIdCuenta As String
    Private _strIdCliente As String
    Private _strIdGrupoCuenta As String
    Private _strNombreCuenta As String
    Private _strNombreCliente As String
    Private _strRutCliente As String
    Private _strFechaConsulta As String
    Private _strCodMonedaCuenta As String
    Private _intDecimalesMonedaCuenta As Integer
    Private _strSimboloMonedaCuenta As String
    Private _strTipoFondoPerfil As String
    Private _strFechaCreacion As String
    Private _strTipoVista As String
    Private _strIdcodigo As String
    Private _strCodMonedaConsulta As String
    Private _intDecimalesMonedaConsulta As Integer
    Private _strSimboloMonedaConsulta As String
    Private _strClienteConCuentas As String
    Private _intClienteConCuentasSinCierre As Integer
    Private _intCuentasCliente As Integer

#End Region

#Region "Propiedades"

    Public Property TipoBusqueda() As String
        Get
            Return _strTipoBusqueda
        End Get
        Set(ByVal value As String)
            _strTipoBusqueda = value
        End Set
    End Property

    Public Property IdCuenta() As String
        Get
            Return _strIdCuenta
        End Get
        Set(ByVal value As String)
            _strIdCuenta = value
        End Set
    End Property

    Public Property IdCliente() As String
        Get
            Return _strIdCliente
        End Get
        Set(ByVal value As String)
            _strIdCliente = value
        End Set
    End Property

    Public Property IdGrupoCuenta() As String
        Get
            Return _strIdGrupoCuenta
        End Get
        Set(ByVal value As String)
            _strIdGrupoCuenta = value
        End Set
    End Property

    Public Property RutCliente() As String
        Get
            Return _strRutCliente
        End Get
        Set(ByVal value As String)
            _strRutCliente = value
        End Set
    End Property

    Public Property FechaConsulta() As String
        Get
            Return _strFechaConsulta
        End Get
        Set(ByVal value As String)
            _strFechaConsulta = value
        End Set
    End Property

    Public Property NombreCuenta() As String
        Get
            Return _strNombreCuenta
        End Get
        Set(ByVal value As String)
            _strNombreCuenta = value
        End Set
    End Property

    Public Property NombreCliente() As String
        Get
            Return _strNombreCliente
        End Get
        Set(ByVal value As String)
            _strNombreCliente = value
        End Set
    End Property

    Public Property CodMonedaCuenta() As String
        Get
            Return _strCodMonedaCuenta
        End Get
        Set(ByVal value As String)
            _strCodMonedaCuenta = value
        End Set
    End Property

    Public Property TipoFondoPerfil() As String
        Get
            Return _strTipoFondoPerfil
        End Get
        Set(ByVal value As String)
            _strTipoFondoPerfil = value
        End Set
    End Property

    Public Property FechaCreacion() As String
        Get
            Return _strFechaCreacion
        End Get
        Set(ByVal value As String)
            _strFechaCreacion = value
        End Set
    End Property

    Public Property DecimalesMonedaCuenta() As Integer
        Get
            Return _intDecimalesMonedaCuenta
        End Get
        Set(ByVal value As Integer)
            _intDecimalesMonedaCuenta = value
        End Set
    End Property

    Public Property SimboloMonedaCuenta() As String
        Get
            Return _strSimboloMonedaCuenta
        End Get
        Set(ByVal value As String)
            _strSimboloMonedaCuenta = value
        End Set
    End Property

    Public Property TipoVista() As String
        Get
            Return _strTipoVista
        End Get
        Set(ByVal value As String)
            _strTipoVista = value
        End Set
    End Property

    Public Property Idcodigo() As String
        Get
            Return _strIdcodigo
        End Get
        Set(ByVal value As String)
            _strIdcodigo = value
        End Set
    End Property

    Public Property CodMonedaConsulta() As String
        Get
            Return _strCodMonedaConsulta
        End Get
        Set(ByVal value As String)
            _strCodMonedaConsulta = value
        End Set
    End Property

    Public Property DecimalesMonedaConsulta() As Integer
        Get
            Return _intDecimalesMonedaConsulta
        End Get
        Set(ByVal value As Integer)
            _intDecimalesMonedaConsulta = value
        End Set
    End Property

    Public Property SimboloMonedaConsulta() As String
        Get
            Return _strSimboloMonedaConsulta
        End Get
        Set(ByVal value As String)
            _strSimboloMonedaConsulta = value
        End Set
    End Property

    Public Property ClienteConCuentas() As String
        Get
            Return _strClienteConCuentas
        End Get
        Set(ByVal value As String)
            _strClienteConCuentas = value
        End Set
    End Property

    Public Property ClienteConCuentasSinCierre() As Integer
        Get
            Return _intClienteConCuentasSinCierre
        End Get
        Set(ByVal value As Integer)
            _intClienteConCuentasSinCierre = value
        End Set
    End Property

    Public Property CuentasCliente() As Integer
        Get
            Return _intCuentasCliente
        End Get
        Set(ByVal value As Integer)
            _intCuentasCliente = value
        End Set
    End Property
#End Region

#Region "Funciones mantenedor Core Acciones - Daniel Guzmán"

    Public Function GrabarActivos_CORE_ACCIONES(ByVal intIdArbolClasificacionInstrumento As Integer, _
                                             ByVal dtInstrumentos As DataTable, ByVal IdUsuario As Integer) As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        Dim lstrInstrumentos1 As String = ""
        Dim lstrInstrumentos2 As String = ""
        Dim lstrColumnas As String = "ID_INSTRUMENTO © " & _
                                     "ACCION"
        Dim lstrResultadoCadena As String = ""
        ' GENERA CADENA(S) DE DETALLE DE OPERACIONES
        lstrResultadoCadena = GeneraCadenaText(dtInstrumentos, lstrColumnas, "©", lstrInstrumentos1)
        If lstrResultadoCadena <> "OK" Then
            Return lstrResultadoCadena
        End If
        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_Core_acciones_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Core_acciones_Mantencion"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdArbolClasificacionInstrumento", SqlDbType.Int).Value = intIdArbolClasificacionInstrumento
            SQLCommand.Parameters.Add("pLineas", SqlDbType.Int).Value = dtInstrumentos.Rows.Count
            SQLCommand.Parameters.Add("pInstrumentos1", SqlDbType.Text).Value = lstrInstrumentos1
            SQLCommand.Parameters.Add("pInstrumentos2", SqlDbType.Text).Value = lstrInstrumentos2

            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Int).Value = IdUsuario
            SQLCommand.ExecuteNonQuery()
            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
                strDescError = "OK"
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            GrabarActivos_CORE_ACCIONES = strDescError
        End Try
    End Function

    Public Function TraeInstrumentos_mantcoreacciones(ByVal strTipoInstrumento As String, _
                                                        ByVal strSubClaseIsntrumento As String, _
                                                        ByRef strRetorno As String, ByRef pFecha As String, ByRef pUsuario As String) As DataSet

        Dim LDS_InstrumentosActivosConsolidados As New DataSet
        Dim LDS_InstrumentosActivosConsolidados_Aux As New DataSet
        Dim Lstr_NombreTabla As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion
        ' sProcedimiento = "Rcp_ActivosConsolidados_ConsultarInstrumentos"
        sProcedimiento = "Rcp_Mant_coreacciones_ConsultarInstrumentos"

        Connect.Abrir()
        Lstr_NombreTabla = "Instrumentos"

        Try
            SQLCommand = New SqlClient.SqlCommand(sProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = sProcedimiento
            SQLCommand.Parameters.Clear()

            'SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Int).Value = glngNegocioOperacion
            'SQLCommand.Parameters.Add("pTipoArbol", SqlDbType.Char, 1).Value = strTipoArbol
            SQLCommand.Parameters.Add("pTipoInstrumento", SqlDbType.VarChar, 15).Value = strTipoInstrumento
            SQLCommand.Parameters.Add("pCodSubClaseInstrumento", SqlDbType.VarChar, 10).Value = IIf(strSubClaseIsntrumento = "", DBNull.Value, strSubClaseIsntrumento)
            'SQLCommand.Parameters.Add("pIdArbolClasificacionInstrumento", SqlDbType.Int).Value = IIf(intIdArbolClasificacionInstrumento = 0, DBNull.Value, intIdArbolClasificacionInstrumento)

            Dim ParametroSal2 As New SqlClient.SqlParameter("pFecha", SqlDbType.Char, 20)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            Dim ParametroSal3 As New SqlClient.SqlParameter("pUsuario", SqlDbType.VarChar, 100)
            ParametroSal3.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal3)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_InstrumentosActivosConsolidados, "Instrumentos")

            LDS_InstrumentosActivosConsolidados_Aux = LDS_InstrumentosActivosConsolidados


            strRetorno = "OK"
            LDS_InstrumentosActivosConsolidados.Dispose()

            pFecha = SQLCommand.Parameters("pFecha").Value.ToString
            pUsuario = SQLCommand.Parameters("pUsuario").Value.ToString.Trim
            Return LDS_InstrumentosActivosConsolidados_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

#End Region

    Public Function TraerCuentas(ByRef strRetorno As String) As DataSet

        Dim LDS_Cuentas As New DataSet
        Dim LDS_Cuentas_Aux As New DataSet

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        sProcedimiento = "Rcp_Cuenta_Listar"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(sProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = sProcedimiento
            SQLCommand.Parameters.Clear()

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Cuentas, "DS_CUENTAS")

            LDS_Cuentas_Aux = LDS_Cuentas
            strRetorno = "OK"
            Return LDS_Cuentas_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function TraeDatosCuenta(ByRef strRetorno As String) As DataSet

        Dim LDS_Cuenta As New DataSet
        Dim LDS_Cuenta_Aux As New DataSet

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        sProcedimiento = "Rcp_Cuenta_BuscarPorFecha"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(sProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = sProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("@pIdCuenta", SqlDbType.Char, 15).Value = IdCuenta
            SQLCommand.Parameters.Add("@pFechaConsulta", SqlDbType.Char, 10).Value = FechaConsulta

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Cuenta)

            LDS_Cuenta_Aux = LDS_Cuenta
            strRetorno = "OK"
            Return LDS_Cuenta_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function TraeActivosPorProducto(ByVal strNegocio As String, ByRef strRetorno As String, Optional ByVal pIdArbol As String = "A") As DataSet

        Dim LDS_ActivosPorProducto As New DataSet
        Dim LDS_ActivosPorProducto_Aux As New DataSet

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        sProcedimiento = "Rcp_Cuenta_VistaPorProductos"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(sProcedimiento, Connect.Conexion)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = sProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Int).Value = IIf(IdCuenta = "", DBNull.Value, CInt("0" & IdCuenta))
            SQLCommand.Parameters.Add("pIdCliente", SqlDbType.Int).Value = IIf(IsNothing(IdCliente) Or IdCliente = "", DBNull.Value, CInt("0" & IdCliente))
            SQLCommand.Parameters.Add("pIdGrupo", SqlDbType.Int).Value = IIf(IsNothing(IdGrupoCuenta) Or IdGrupoCuenta = "", DBNull.Value, CInt("0" & IdGrupoCuenta))
            SQLCommand.Parameters.Add("pFechaConsulta", SqlDbType.Char, 10).Value = FechaConsulta
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Int).Value = strNegocio
            SQLCommand.Parameters.Add("pTipoVista", SqlDbType.VarChar, 20).Value = IIf(TipoVista = "", DBNull.Value, TipoVista)
            SQLCommand.Parameters.Add("pIdCodigo", SqlDbType.VarChar, 10).Value = IIf(Idcodigo = "", DBNull.Value, Idcodigo)
            SQLCommand.Parameters.Add("pCodMonedaConsulta", SqlDbType.VarChar, 3).Value = CodMonedaConsulta
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Float, 6).Value = IIf(glngIdUsuario = 0, DBNull.Value, glngIdUsuario)
            SQLCommand.Parameters.Add("pIdArbol", SqlDbType.Char, 1).Value = pIdArbol

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_ActivosPorProducto, "DS_ACTIVOS")

            LDS_ActivosPorProducto_Aux = LDS_ActivosPorProducto
            strRetorno = "OK"
            Return LDS_ActivosPorProducto_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function TraeActivosPorMoneda(ByVal strNegocio As String, ByRef strRetorno As String) As DataSet

        Dim LDS_ActivosXMoneda As New DataSet
        Dim LDS_ActivosXMoneda_Aux As New DataSet

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        sProcedimiento = "Rcp_Cuenta_VistaPorMoneda"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(sProcedimiento, Connect.Conexion, Connect.Transaccion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = sProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Int).Value = strNegocio
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Int).Value = IIf(IdCuenta = "", DBNull.Value, CInt("0" & IdCuenta))
            SQLCommand.Parameters.Add("pIdCliente", SqlDbType.Int).Value = IIf(IsNothing(IdCliente) Or IdCliente = "", DBNull.Value, CInt("0" & IdCliente))
            SQLCommand.Parameters.Add("pIdGrupo", SqlDbType.Int).Value = IIf(IsNothing(IdGrupoCuenta) Or IdGrupoCuenta = "", DBNull.Value, CInt("0" & IdGrupoCuenta))
            SQLCommand.Parameters.Add("pFechaConsulta", SqlDbType.Char, 10).Value = FechaConsulta
            SQLCommand.Parameters.Add("pCodMonedaConsulta", SqlDbType.VarChar, 3).Value = CodMonedaConsulta
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Float, 6).Value = IIf(glngIdUsuario = 0, DBNull.Value, glngIdUsuario)


            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_ActivosXMoneda)

            LDS_ActivosXMoneda_Aux = LDS_ActivosXMoneda
            strRetorno = "OK"
            Return LDS_ActivosXMoneda_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function TraeActivosPorMercado(ByVal strNegocio As String, ByRef strRetorno As String) As DataSet

        Dim LDS_ActivosXMercado As New DataSet
        Dim LDS_ActivosXMercado_Aux As New DataSet

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        sProcedimiento = "Rcp_Cuenta_VistaPorMercado"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(sProcedimiento, Connect.Conexion, Connect.Transaccion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = sProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Int).Value = strNegocio
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Int).Value = IIf(IdCuenta = "", DBNull.Value, CInt("0" & IdCuenta))
            SQLCommand.Parameters.Add("pIdCliente", SqlDbType.Int).Value = IIf(IsNothing(IdCliente) Or IdCliente = "", DBNull.Value, CInt("0" & IdCliente))
            SQLCommand.Parameters.Add("pIdGrupo", SqlDbType.Int).Value = IIf(IsNothing(IdGrupoCuenta) Or IdGrupoCuenta = "", DBNull.Value, CInt("0" & IdGrupoCuenta))
            SQLCommand.Parameters.Add("pFechaConsulta", SqlDbType.Char, 10).Value = FechaConsulta
            SQLCommand.Parameters.Add("pCodMonedaConsulta", SqlDbType.VarChar, 3).Value = CodMonedaConsulta
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Float, 6).Value = IIf(glngIdUsuario = 0, DBNull.Value, glngIdUsuario)


            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_ActivosXMercado)

            LDS_ActivosXMercado_Aux = LDS_ActivosXMercado
            strRetorno = "OK"
            Return LDS_ActivosXMercado_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function TraeActivosPorSector(ByVal strNegocio As String, ByRef strRetorno As String) As DataSet

        Dim LDS_ActivosXSector As New DataSet
        Dim LDS_ActivosXSector_Aux As New DataSet

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        sProcedimiento = "Rcp_Cuenta_VistaPorSector"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(sProcedimiento, Connect.Conexion, Connect.Transaccion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = sProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Int).Value = strNegocio
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Int).Value = IIf(IdCuenta = "", DBNull.Value, CInt("0" & IdCuenta))
            SQLCommand.Parameters.Add("pIdCliente", SqlDbType.Int).Value = IIf(IsNothing(IdCliente) Or IdCliente = "", DBNull.Value, CInt("0" & IdCliente))
            SQLCommand.Parameters.Add("pIdGrupo", SqlDbType.Int).Value = IIf(IsNothing(IdGrupoCuenta) Or IdGrupoCuenta = "", DBNull.Value, CInt("0" & IdGrupoCuenta))
            SQLCommand.Parameters.Add("pFechaConsulta", SqlDbType.Char, 10).Value = FechaConsulta
            SQLCommand.Parameters.Add("pCodMonedaConsulta", SqlDbType.VarChar, 3).Value = CodMonedaConsulta
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Float, 6).Value = IIf(glngIdUsuario = 0, DBNull.Value, glngIdUsuario)


            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_ActivosXSector)

            LDS_ActivosXSector_Aux = LDS_ActivosXSector
            strRetorno = "OK"
            Return LDS_ActivosXSector_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function TraeDetalleActivos(ByVal strIdNegocio As String, _
                                       ByVal strCodigo As String, _
                                       ByVal intIdArbolClasificacionInstrumento As Integer, _
                                       ByVal strTipoHoja As String, _
                                       ByVal strColumnas As String, _
                                       ByRef strRetorno As String) As DataSet

        Dim LDS_DetActivo As New DataSet
        Dim LDS_DetActivo_Aux As New DataSet
        'Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        'Dim LInt_Col As Integer
        'Dim LInt_NomCol As String = ""
        'Dim Lstr_NombreTabla As String
        'Dim Columna As DataColumn
        'Dim Remove As Boolean = False

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        If gLogoCliente.Trim = "MONEDA" Then
            sProcedimiento = "Mda_Cuenta_DetallePorActivo"
        Else
            sProcedimiento = "Rcp_Cuenta_DetalleActivos"
        End If
        'Lstr_NombreTabla = "DetalleActivos"
        sProcedimiento = "Rcp_Cuenta_DetalleActivos"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(sProcedimiento, Connect.Conexion, Connect.Transaccion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = sProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Int).Value = CInt("0" & Me.IdCuenta)
            SQLCommand.Parameters.Add("pIdCliente", SqlDbType.Int).Value = IIf(IsNothing(IdCliente) Or IdCliente = "", DBNull.Value, CInt("0" & IdCliente))
            SQLCommand.Parameters.Add("pIdGrupo", SqlDbType.Int).Value = IIf(IsNothing(IdGrupoCuenta) Or IdGrupoCuenta = "", DBNull.Value, CInt("0" & IdGrupoCuenta))
            SQLCommand.Parameters.Add("pFechaConsulta", SqlDbType.Char, 10).Value = Me.FechaConsulta
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Int).Value = IIf(strIdNegocio = "", DBNull.Value, CInt(strIdNegocio))
            SQLCommand.Parameters.Add("pCodigo", SqlDbType.Char, 50).Value = strCodigo
            SQLCommand.Parameters.Add("pIdArbolClasificacionInstrumento", SqlDbType.Int).Value = intIdArbolClasificacionInstrumento
            SQLCommand.Parameters.Add("pTipoHoja", SqlDbType.Char, 1).Value = strTipoHoja
            SQLCommand.Parameters.Add("pTipo", SqlDbType.VarChar, 20).Value = IIf(Me.TipoVista = "", DBNull.Value, Me.TipoVista)
            SQLCommand.Parameters.Add("pIdCodigo", SqlDbType.VarChar, 10).Value = IIf(Me.Idcodigo = "", DBNull.Value, Me.Idcodigo)
            SQLCommand.Parameters.Add("pCodMonedaConsulta", SqlDbType.VarChar, 10).Value = Me.CodMonedaConsulta
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Float, 6).Value = IIf(glngIdUsuario = 0, DBNull.Value, glngIdUsuario)


            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_DetActivo)

            strRetorno = "OK"
            If LDS_DetActivo.Tables.Count = 2 Then
                LDS_DetActivo.Tables(0).TableName = "Columnas"
                LDS_DetActivo.Tables(1).TableName = "DetalleActivo"
                LDS_DetActivo.AcceptChanges()
            End If
            Return LDS_DetActivo

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function


    Public Function TraeRentabilidad2(ByVal strNegocio As String, _
                                      ByVal strTipoConsulta As String, _
                                      ByVal strFiltro As String, _
                                 ByRef strRetorno As String) As DataSet

        Dim LDS_Rentabilidad As New DataSet
        Dim LDS_Rentabilidad_Aux As New DataSet

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion


        sProcedimiento = "RCP_REPORTE_RENTABILIDAD_CONSOLIDADA_SELECCION"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(sProcedimiento, Connect.Conexion, Connect.Transaccion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = sProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pFecha_Desde", SqlDbType.Char, 10).Value = FechaConsulta
            SQLCommand.Parameters.Add("pFecha_Hasta", SqlDbType.Char, 10).Value = FechaConsulta
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Int).Value = strNegocio
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Int).Value = glngIdUsuario
            SQLCommand.Parameters.Add("pCodMoneda", SqlDbType.VarChar, 3).Value = CodMonedaConsulta
            SQLCommand.Parameters.Add("pTipoConsulta", SqlDbType.VarChar, 30).Value = strTipoConsulta
            SQLCommand.Parameters.Add("pfiltro", SqlDbType.Text).Value = strFiltro

            'SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Int).Value = IIf(IdCuenta = "", DBNull.Value, CInt("0" & IdCuenta))
            'SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Int).Value = strNegocio
            'SQLCommand.Parameters.Add("pFechaConsulta", SqlDbType.Char, 10).Value = FechaConsulta
            'SQLCommand.Parameters.Add("pCodMonedaConsulta", SqlDbType.VarChar, 3).Value = CodMonedaConsulta


            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Rentabilidad)

            LDS_Rentabilidad_Aux = LDS_Rentabilidad
            strRetorno = "OK"
            Return LDS_Rentabilidad_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function




    Public Function TraeRentabilidad(ByVal strNegocio As String, _
                                     ByRef strRetorno As String) As DataSet

        Dim LDS_Rentabilidad As New DataSet
        Dim LDS_Rentabilidad_Aux As New DataSet

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion


        sProcedimiento = "Rcp_Cuenta_Rentabilidad"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(sProcedimiento, Connect.Conexion, Connect.Transaccion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = sProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Int).Value = IIf(IdCuenta = "", DBNull.Value, CInt("0" & IdCuenta))
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Int).Value = strNegocio
            SQLCommand.Parameters.Add("pFechaConsulta", SqlDbType.Char, 10).Value = FechaConsulta
            SQLCommand.Parameters.Add("pCodMonedaConsulta", SqlDbType.VarChar, 3).Value = CodMonedaConsulta


            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Rentabilidad)

            LDS_Rentabilidad_Aux = LDS_Rentabilidad
            strRetorno = "OK"
            Return LDS_Rentabilidad_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function TraeValoresCuota(ByVal strNegocio As String, ByRef strRetorno As String) As DataSet

        Dim LDS_ValoresCuota As New DataSet
        Dim LDS_ValoresCuota_Aux As New DataSet

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion


        sProcedimiento = "Rcp_Cuenta_ValoresCuotaRango"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(sProcedimiento, Connect.Conexion, Connect.Transaccion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = sProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Int).Value = IIf(IdCuenta = "", DBNull.Value, CInt("0" & IdCuenta))
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Int).Value = strNegocio
            SQLCommand.Parameters.Add("pFechaConsulta", SqlDbType.Char, 10).Value = FechaConsulta
            SQLCommand.Parameters.Add("pCodMonedaConsulta", SqlDbType.VarChar, 3).Value = CodMonedaConsulta


            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_ValoresCuota)

            LDS_ValoresCuota_Aux = LDS_ValoresCuota
            strRetorno = "OK"
            Return LDS_ValoresCuota_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function


    Public Function TraeValoresCuota2(ByVal strNegocio As String, ByVal strPeriodo As String, ByRef strRetorno As String) As DataSet

        Dim LDS_ValoresCuota As New DataSet
        Dim LDS_ValoresCuota_Aux As New DataSet

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion


        sProcedimiento = "RCP_REPORTE_RENTABILIDAD_CONSOLIDADA_SERIE"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(sProcedimiento, Connect.Conexion, Connect.Transaccion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = sProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pFecha_Desde", SqlDbType.Char, 10).Value = FechaConsulta
            SQLCommand.Parameters.Add("pFecha_Hasta", SqlDbType.Char, 10).Value = FechaConsulta
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Int).Value = IIf(gobjVistaActivos.IdCuenta.ToString = "", DBNull.Value, gobjVistaActivos.IdCuenta.ToString)
            SQLCommand.Parameters.Add("pIdCliente", SqlDbType.Int).Value = IIf(gobjVistaActivos.IdCliente.ToString = "", DBNull.Value, gobjVistaActivos.IdCliente.ToString)
            SQLCommand.Parameters.Add("pIdGrupo", SqlDbType.Int).Value = IIf(gobjVistaActivos.IdGrupoCuenta.ToString = "", DBNull.Value, gobjVistaActivos.IdGrupoCuenta.ToString)
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Int).Value = strNegocio

            SQLCommand.Parameters.Add("@pCodMoneda", SqlDbType.VarChar, 3).Value = CodMonedaConsulta
            SQLCommand.Parameters.Add("@pPeriodo", SqlDbType.VarChar, 15).Value = strPeriodo

            'SQLCommand.Parameters.Add("pFechaConsulta", SqlDbType.Char, 10).Value = FechaConsulta
            'SQLCommand.Parameters.Add("pCodMonedaConsulta", SqlDbType.VarChar, 3).Value = CodMonedaConsulta

            'SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Int).Value = IIf(IdCuenta = "", DBNull.Value, CInt("0" & IdCuenta))
            'SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Int).Value = strNegocio
            'SQLCommand.Parameters.Add("pFechaConsulta", SqlDbType.Char, 10).Value = FechaConsulta
            'SQLCommand.Parameters.Add("pCodMonedaConsulta", SqlDbType.VarChar, 3).Value = CodMonedaConsulta

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_ValoresCuota)

            LDS_ValoresCuota_Aux = LDS_ValoresCuota
            strRetorno = "OK"
            Return LDS_ValoresCuota_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function



    Public Function TraeVencimientos(ByVal strNegocio As String, ByRef strRetorno As String) As DataSet

        Dim LDS_Vencimientos As New DataSet
        Dim LDS_Vencimientos_Aux As New DataSet

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion


        sProcedimiento = "Rcp_Cuenta_VistaPorVencimientos"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(sProcedimiento, Connect.Conexion, Connect.Transaccion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = sProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Int).Value = strNegocio
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Int).Value = IIf(IdCuenta = "", DBNull.Value, CInt("0" & IdCuenta))
            SQLCommand.Parameters.Add("pIdCliente", SqlDbType.Int).Value = IIf(IsNothing(IdCliente) Or IdCliente = "", DBNull.Value, CInt("0" & IdCliente))
            SQLCommand.Parameters.Add("pIdGrupo", SqlDbType.Int).Value = IIf(IsNothing(IdGrupoCuenta) Or IdGrupoCuenta = "", DBNull.Value, CInt("0" & IdGrupoCuenta))
            SQLCommand.Parameters.Add("pFechaConsulta", SqlDbType.Char, 10).Value = FechaConsulta
            SQLCommand.Parameters.Add("pCodMonedaConsulta", SqlDbType.VarChar, 3).Value = CodMonedaConsulta
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Float, 6).Value = IIf(glngIdUsuario = 0, DBNull.Value, glngIdUsuario)


            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Vencimientos)

            LDS_Vencimientos_Aux = LDS_Vencimientos
            strRetorno = "OK"
            Return LDS_Vencimientos_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    '-----------------------------------------------------------------
    Public Function TraeDetalleOperaciones(ByRef strRetorno As String, ByVal strNemotecnico As String) As DataSet

        Dim LDS_DetOpe As New DataSet
        Dim LDS_DetOpe_Aux As New DataSet

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        If gLogoCliente.Trim = "MONEDA" Then 'If gLogoCliente1.Trim = "MONEDA" Then
            sProcedimiento = "Mda_Cuenta_DetalleOperaciones"
        Else
            strRetorno = "Vista no Implementada"
            TraeDetalleOperaciones = Nothing
            Exit Function
            sProcedimiento = "Mda_Cuenta_DetalleOperaciones"
        End If

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(sProcedimiento, Connect.Conexion, Connect.Transaccion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = sProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("@cuenta", SqlDbType.Char, 20).Value = IdCuenta
            SQLCommand.Parameters.Add("@fecha_desde", SqlDbType.Char, 10).Value = FechaCreacion
            SQLCommand.Parameters.Add("@fecha_hasta", SqlDbType.Char, 10).Value = FechaConsulta
            SQLCommand.Parameters.Add("@Nemotecnico", SqlDbType.Char, 15).Value = strNemotecnico

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_DetOpe)

            LDS_DetOpe_Aux = LDS_DetOpe
            strRetorno = "OK"
            Return LDS_DetOpe_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function TraeDatosOperacion(ByRef strRetorno As String, ByVal dblOperacion As Double, ByVal dblCorrelativo As Double) As DataSet

        Dim LDS_DatosOperacion As New DataSet
        Dim LDS_DatoOperacion_Aux As New DataSet

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        If gLogoCliente.Trim = "MONEDA" Then 'If gLogoCliente1.Trim = "MONEDA" Then
            sProcedimiento = "Mda_Cuenta_DatosOperacion"
        Else
            strRetorno = "Vista no Implementada"
            TraeDatosOperacion = Nothing
            Exit Function
            sProcedimiento = "Mda_Cuenta_DatosOperacion"
        End If

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(sProcedimiento, Connect.Conexion, Connect.Transaccion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = sProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("@cuenta", SqlDbType.Char, 20).Value = IdCuenta
            SQLCommand.Parameters.Add("@operacion", SqlDbType.Float, 18).Value = dblOperacion
            SQLCommand.Parameters.Add("@correlativo", SqlDbType.Float, 18).Value = dblCorrelativo
            SQLCommand.Parameters.Add("@fecha", SqlDbType.VarChar, 10).Value = FechaConsulta

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_DatosOperacion)

            LDS_DatoOperacion_Aux = LDS_DatosOperacion
            strRetorno = "OK"
            Return LDS_DatoOperacion_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function TraeDividendosPorCobrar(ByRef strRetorno As String) As DataSet

        Dim LDS_Dividendos As New DataSet
        Dim LDS_Dividendos_Aux As New DataSet

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        If gLogoCliente.Trim = "MONEDA" Then 'If gLogoCliente1.Trim = "MONEDA" Then
            sProcedimiento = "Mda_Cuenta_DividendosPorCobrar"
        Else
            sProcedimiento = "Rcp_Cuenta_DividendosPorCobrar"
        End If

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(sProcedimiento, Connect.Conexion, Connect.Transaccion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = sProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("@pIdCuenta", SqlDbType.Char, 20).Value = IdCuenta
            SQLCommand.Parameters.Add("@pFecha_Consulta", SqlDbType.VarChar, 10).Value = FechaConsulta

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Dividendos)

            LDS_Dividendos_Aux = LDS_Dividendos
            strRetorno = "OK"
            Return LDS_Dividendos_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function TraeDetalleMovimientosCajas(ByRef strRetorno As String, ByVal strCuenta As String) As DataSet

        Dim LDS_DetMovCajas As New DataSet
        Dim LDS_DetMovCajas_Aux As New DataSet

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        If gLogoCliente.Trim = "MONEDA" Then 'If gLogoCliente1.Trim = "MONEDA" Then
            sProcedimiento = "Mda_Cuenta_Detalle_MovimientosCajas"
        Else
            strRetorno = "Vista no Implementada"
            TraeDetalleMovimientosCajas = Nothing
            Exit Function
            sProcedimiento = "Mda_Cuenta_Detalle_MovimientosCajas"
        End If

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(sProcedimiento, Connect.Conexion, Connect.Transaccion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = sProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("@pEmpresa", SqlDbType.Char, 10).Value = IdCuenta
            SQLCommand.Parameters.Add("@pCuenta", SqlDbType.Char, 20).Value = strCuenta
            SQLCommand.Parameters.Add("@pFechaDesde", SqlDbType.Char, 10).Value = CStr(DateAdd(DateInterval.Month, -1, CDate(FechaConsulta)))
            SQLCommand.Parameters.Add("@pFechaHasta", SqlDbType.Char, 10).Value = FechaConsulta

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_DetMovCajas)
            LDS_DetMovCajas_Aux = (LDS_DetMovCajas)
            strRetorno = "OK"
            Return LDS_DetMovCajas_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function Instrumentos_Ver() As DataSet


        Dim LDS_Cuenta As New DataSet
        Dim LDS_Cuenta_Aux As New DataSet
        Dim Lstr_NombreTabla As String

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        sProcedimiento = "Rcp_InstrumentosVerActivos"
        Lstr_NombreTabla = "INSTRUMENTOS"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(sProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = sProcedimiento
            SQLCommand.Parameters.Clear()

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Cuenta)
            LDS_Cuenta_Aux = LDS_Cuenta

            LDS_Cuenta.Dispose()
            Return LDS_Cuenta_Aux

        Catch ex As Exception
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    'Public Function TraerActivosConsolidados(ByVal strColumnas As String, _
    '                                         ByRef strRetorno As String) As DataSet


    '    Dim LDS_ActivosConsolidados As New DataSet
    '    Dim LDS_ActivosConsolidados_Aux As New DataSet
    '    Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
    '    Dim LInt_Col As Integer
    '    Dim LInt_NomCol As String
    '    Dim Columna As DataColumn
    '    Dim Remove As Boolean
    '    Dim LstrProcedimiento
    '    Dim Lstr_NombreTabla As String
    '    Dim SQLCommand As New SqlClient.SqlCommand
    '    Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

    '    Dim Connect As Cls_Conexion = New Cls_Conexion

    '    LstrProcedimiento = "Rcp_ActivosConsolidadosConsultar"
    '    Lstr_NombreTabla = "ACTIVOS"
    '    Connect.Abrir()

    '    Try
    '        SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

    '        SQLCommand.CommandType = CommandType.StoredProcedure
    '        SQLCommand.CommandText = LstrProcedimiento
    '        SQLCommand.Parameters.Clear()

    '        ''SQLCommand.Parameters.Add("pIdInstrumento", SqlDbType.Float, 10).Value = IIf(lngIdInstrumento = 0, DBNull.Value, lngIdInstrumento)
    '        'SQLCommand.Parameters.Add("pIdArbolClas", SqlDbType.Float, 10).Value = IIf(lngIdArbolClas = 0, DBNull.Value, lngIdArbolClas)
    '        'SQLCommand.Parameters.Add("pDscArbolClas", SqlDbType.VarChar, 120).Value = IIf(strDscArbolClas = "", DBNull.Value, strDscArbolClas)

    '        SQLDataAdapter.SelectCommand = SQLCommand
    '        SQLDataAdapter.Fill(LDS_ActivosConsolidados, Lstr_NombreTabla)

    '        LDS_ActivosConsolidados.Tables(0).TableName = "PADRE"
    '        LDS_ActivosConsolidados.Tables(1).TableName = "HIJO"

    '        Dim lintTablas As Integer = LDS_ActivosConsolidados.Tables.Count
    '        For i As Integer = 0 To lintTablas - 1
    '            If strColumnas.Trim = "" Then
    '                LDS_ActivosConsolidados_Aux = LDS_ActivosConsolidados
    '            Else
    '                LDS_ActivosConsolidados_Aux = LDS_ActivosConsolidados.Copy
    '                For Each Columna In LDS_ActivosConsolidados.Tables(i).Columns
    '                    Remove = True
    '                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
    '                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
    '                        If Columna.ColumnName = LInt_NomCol Then
    '                            Remove = False
    '                        End If
    '                    Next
    '                    If Remove Then
    '                        LDS_ActivosConsolidados_Aux.Tables(i).Columns.Remove(Columna.ColumnName)
    '                    End If
    '                Next
    '            End If

    '            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
    '                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
    '                For Each Columna In LDS_ActivosConsolidados_Aux.Tables(0).Columns
    '                    If Columna.ColumnName = LInt_NomCol Then
    '                        Columna.SetOrdinal(LInt_Col)
    '                        Exit For
    '                    End If
    '                Next
    '            Next
    '        Next i
    '        strRetorno = "OK"
    '        Return LDS_ActivosConsolidados_Aux

    '    Catch ex As Exception
    '        strRetorno = ex.Message
    '        Return Nothing
    '    Finally
    '        Connect.Cerrar()
    '    End Try

    'End Function
    'Public Function ModificaActivosConsolidados(ByVal dsInstrumentos As DataSet) As String

    '    Dim strDescError As String = ""
    '    Dim lstrstring As String = ""
    '    Dim lstrstringAux As String = ""
    '    Dim lstrDetalle As String = ""
    '    Dim lstrRegistro As String = ""
    '    Dim strResultado As String = ""

    '    Dim lstrColumnasDetalle As String = "ID_INSTRUMENTO © " & _
    '                                         "RAMA"
    '    Dim lstrResultadoCadena As String = ""

    '    For Each dtrInstrumentoRel As DataRow In dsInstrumentos.Tables(0).Rows
    '        lstrRegistro = dtrInstrumentoRel("ID_INSTRUMENTO").ToString + "©" + dtrInstrumentoRel("RAMA").ToString + "©"
    '        lstrstringAux = lstrstringAux + lstrRegistro
    '        If lstrstringAux.Length < 8000 Then
    '            lstrDetalle = lstrstringAux
    '        Else
    '            Call GrabarActivos(lstrDetalle, strResultado)
    '            If strResultado = "OK" Then
    '                lstrstringAux = lstrRegistro
    '            Else
    '                MsgBox("Error al grabar un instrumento")
    '                ModificaActivosConsolidados = strResultado
    '                Exit Function
    '            End If

    '        End If
    '    Next
    '    If Len(lstrstringAux) > 0 Then
    '        Call GrabarActivos(lstrstringAux, strResultado)
    '        If strResultado <> "OK" Then
    '            MsgBox("Error al grabar los datos")
    '            ModificaActivosConsolidados = strResultado
    '            Exit Function
    '        End If
    '    End If
    '    ModificaActivosConsolidados = strResultado
    'End Function


    Public Function TraeActivosConsolidados(ByVal strColumnas As String, _
                                            ByVal strTipo As String, _
                                             ByRef strRetorno As String) As DataSet

        Dim LDS_ActivosConsolidados As New DataSet
        Dim LDS_ActivosConsolidados_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim Lstr_NombreTabla As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        sProcedimiento = "Rcp_ActivosConsolidados_Consultar"

        Connect.Abrir()
        Lstr_NombreTabla = "ActivosConsolidados"

        Try
            SQLCommand = New SqlClient.SqlCommand(sProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = sProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Int).Value = glngNegocioOperacion
            SQLCommand.Parameters.Add("@pTipoArbol", SqlDbType.Char, 1).Value = strTipo

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_ActivosConsolidados, "ActivosConsolidados")

            LDS_ActivosConsolidados_Aux = LDS_ActivosConsolidados

            If strColumnas.Trim = "" Then
                LDS_ActivosConsolidados_Aux = LDS_ActivosConsolidados
            Else
                LDS_ActivosConsolidados_Aux = LDS_ActivosConsolidados.Copy
                For Each Columna In LDS_ActivosConsolidados.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_ActivosConsolidados_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_ActivosConsolidados_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_ActivosConsolidados.Dispose()
            Return LDS_ActivosConsolidados_Aux

            strRetorno = "OK"
            Return LDS_ActivosConsolidados_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function GrabarActivosConsolidados(ByVal intIdArbolClasificacionInstrumento As Integer, _
                                              ByVal dtInstrumentos As DataTable) As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        Dim lstrInstrumentos1 As String = ""
        Dim lstrInstrumentos2 As String = ""
        Dim lstrColumnas As String = "ID_INSTRUMENTO © " & _
                                     "ACCION"
        Dim lstrResultadoCadena As String = ""
        ' GENERA CADENA(S) DE DETALLE DE OPERACIONES
        lstrResultadoCadena = GeneraCadenaText(dtInstrumentos, lstrColumnas, "©", lstrInstrumentos1)
        If lstrResultadoCadena <> "OK" Then
            Return lstrResultadoCadena
        End If
        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_ActivosConsolidados_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_ActivosConsolidados_Mantencion"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdArbolClasificacionInstrumento", SqlDbType.Int).Value = intIdArbolClasificacionInstrumento
            SQLCommand.Parameters.Add("pLineas", SqlDbType.Int).Value = dtInstrumentos.Rows.Count
            SQLCommand.Parameters.Add("pInstrumentos1", SqlDbType.Text).Value = lstrInstrumentos1
            SQLCommand.Parameters.Add("pInstrumentos2", SqlDbType.Text).Value = lstrInstrumentos2

            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
                strDescError = "OK"
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            GrabarActivosConsolidados = strDescError
        End Try
    End Function

    Public Function TraeInstrumentosActivosConsolidados(ByVal strTipoInstrumento As String, _
                                                            ByVal strSubClaseIsntrumento As String, _
                                                            ByVal intIdArbolClasificacionInstrumento As Integer, _
                                                            ByVal strTipoArbol As String, _
                                                            ByRef strRetorno As String) As DataSet

        Dim LDS_InstrumentosActivosConsolidados As New DataSet
        Dim LDS_InstrumentosActivosConsolidados_Aux As New DataSet
        Dim Lstr_NombreTabla As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        sProcedimiento = "Rcp_ActivosConsolidados_ConsultarInstrumentos"

        Connect.Abrir()
        Lstr_NombreTabla = "Instrumentos"

        Try
            SQLCommand = New SqlClient.SqlCommand(sProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = sProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Int).Value = glngNegocioOperacion
            SQLCommand.Parameters.Add("pTipoArbol", SqlDbType.Char, 1).Value = strTipoArbol
            SQLCommand.Parameters.Add("pTipoInstrumento", SqlDbType.VarChar, 15).Value = strTipoInstrumento
            SQLCommand.Parameters.Add("pCodSubClaseInstrumento", SqlDbType.VarChar, 10).Value = IIf(strSubClaseIsntrumento = "", DBNull.Value, strSubClaseIsntrumento)
            SQLCommand.Parameters.Add("pIdArbolClasificacionInstrumento", SqlDbType.Int).Value = IIf(intIdArbolClasificacionInstrumento = 0, DBNull.Value, intIdArbolClasificacionInstrumento)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_InstrumentosActivosConsolidados, "Instrumentos")

            LDS_InstrumentosActivosConsolidados_Aux = LDS_InstrumentosActivosConsolidados


            strRetorno = "OK"
            LDS_InstrumentosActivosConsolidados.Dispose()
            Return LDS_InstrumentosActivosConsolidados_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function Arbol_Instrumentos_Tipo_Ver(ByVal strColumnas As String, _
                               ByRef strRetorno As String) As DataSet

        Dim LDS_Asesor As New DataSet
        Dim LDS_Asesor_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "RCP_ARBOL_INSTRUMENTO_TIPO_VER"
        Lstr_NombreTabla = "Arbol_Instrumento_Tipo"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Asesor, Lstr_NombreTabla)

            LDS_Asesor_Aux = LDS_Asesor

            If strColumnas.Trim = "" Then
                LDS_Asesor_Aux = LDS_Asesor
            Else
                LDS_Asesor_Aux = LDS_Asesor.Copy
                For Each Columna In LDS_Asesor.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Asesor_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Asesor_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_Asesor.Dispose()
            Return LDS_Asesor_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function



    Public Function Arbol_Instrumentos_NoClasificados_Ver(ByVal pFecha As String, _
                                                          ByVal pId_Tipo_Arbol As Integer, _
                                                            ByVal strColumnas As String, _
                                                            ByRef strRetorno As String) As DataSet

        Dim LDS_Asesor As New DataSet
        Dim LDS_Asesor_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Arbol_NoClasificados_Ver"
        Lstr_NombreTabla = "Arbol_Instrumento"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pFecha", SqlDbType.Char, 10).Value = pFecha
            SQLCommand.Parameters.Add("tipo_arbol", SqlDbType.Int).Value = pId_Tipo_Arbol

            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Asesor, Lstr_NombreTabla)

            LDS_Asesor_Aux = LDS_Asesor

            If strColumnas.Trim = "" Then
                LDS_Asesor_Aux = LDS_Asesor
            Else
                LDS_Asesor_Aux = LDS_Asesor.Copy
                For Each Columna In LDS_Asesor.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Asesor_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Asesor_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_Asesor.Dispose()
            Return LDS_Asesor_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function





End Class
 

