﻿Public Class ClsSubClaseInstrumentoGrupo

    Public Function SubClaseInstrumentoGrupo_Consultar(ByVal intIdSubClaseInstrumentoGrupo As Integer, _
                                                       ByVal strColumnas As String, _
                                                       ByRef strRetorno As String) As DataSet

        Dim LDS_SubClaseInstrumentoGrupo As New DataSet
        Dim LDS_SubClaseInstrumentoGrupo_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_SubClaseInstrumentoGrupo_Consultar"
        Lstr_NombreTabla = "SubClaseInstrumentoGrupo"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdSubClaseInstrumentoGrupo", SqlDbType.Int, 10).Value = IIf(intIdSubClaseInstrumentoGrupo = 0, DBNull.Value, intIdSubClaseInstrumentoGrupo)

            '...Resultado
            Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultado.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_SubClaseInstrumentoGrupo, Lstr_NombreTabla)

            LDS_SubClaseInstrumentoGrupo_Aux = LDS_SubClaseInstrumentoGrupo

            If strColumnas.Trim = "" Then
                LDS_SubClaseInstrumentoGrupo_Aux = LDS_SubClaseInstrumentoGrupo
            Else
                LDS_SubClaseInstrumentoGrupo_Aux = LDS_SubClaseInstrumentoGrupo.Copy
                For Each Columna In LDS_SubClaseInstrumentoGrupo.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_SubClaseInstrumentoGrupo_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_SubClaseInstrumentoGrupo_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            LDS_SubClaseInstrumentoGrupo.Dispose()
            Return LDS_SubClaseInstrumentoGrupo_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function SubClaseInstrumentoGrupo_Buscar(ByVal intIdSubClaseInstrumentoGrupo As Integer, _
                                                    ByVal intIdSubClaseInstrumentoTipoGrupo As Integer, _
                                                    ByVal strCodSubClaseInstrumentoGrupo As String, _
                                                    ByVal strDscSubClaseInstrumentoGrupo As String, _
                                                    ByVal strEstSubClaseInstrumentoGrupo As String, _
                                                    ByVal strColumnas As String, _
                                                    ByRef strRetorno As String) As DataSet

        Dim LDS_SubClaseInstrumentoTipoGrupo As New DataSet
        Dim LDS_SubClaseInstrumentoTipoGrupo_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_SubClaseInstrumentoGrupo_Buscar"
        Lstr_NombreTabla = "SubClaseInstrumentoGrupo"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdSubClaseInstrumentoGrupo", SqlDbType.Int, 10).Value = IIf(intIdSubClaseInstrumentoGrupo = 0, DBNull.Value, intIdSubClaseInstrumentoGrupo)
            SQLCommand.Parameters.Add("pIdSubClaseInstrumentoTipoGrupo", SqlDbType.Int, 10).Value = IIf(intIdSubClaseInstrumentoTipoGrupo = 0, DBNull.Value, intIdSubClaseInstrumentoTipoGrupo)
            SQLCommand.Parameters.Add("pCodSubClaseInstrumentoGrupo", SqlDbType.VarChar, 100).Value = IIf(strCodSubClaseInstrumentoGrupo = "", DBNull.Value, strCodSubClaseInstrumentoGrupo)
            SQLCommand.Parameters.Add("pDscSubClaseInstrumentoGrupo", SqlDbType.VarChar, 100).Value = IIf(strDscSubClaseInstrumentoGrupo = "", DBNull.Value, strDscSubClaseInstrumentoGrupo)
            SQLCommand.Parameters.Add("pEstSubClaseInstrumentoGrupo", SqlDbType.VarChar, 3).Value = IIf(strEstSubClaseInstrumentoGrupo = "", DBNull.Value, strEstSubClaseInstrumentoGrupo)

            '...Resultado
            Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultado.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_SubClaseInstrumentoTipoGrupo, Lstr_NombreTabla)

            LDS_SubClaseInstrumentoTipoGrupo_Aux = LDS_SubClaseInstrumentoTipoGrupo

            If strColumnas.Trim = "" Then
                LDS_SubClaseInstrumentoTipoGrupo_Aux = LDS_SubClaseInstrumentoTipoGrupo
            Else
                LDS_SubClaseInstrumentoTipoGrupo_Aux = LDS_SubClaseInstrumentoTipoGrupo.Copy
                For Each Columna In LDS_SubClaseInstrumentoTipoGrupo.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_SubClaseInstrumentoTipoGrupo_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_SubClaseInstrumentoTipoGrupo_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            LDS_SubClaseInstrumentoTipoGrupo.Dispose()
            Return LDS_SubClaseInstrumentoTipoGrupo_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function SubClaseInstrumentoTipoGrupo_Mantencion(ByVal strAccion As String, _
                                                            ByVal intIdSubClaseInstrumentoGrupo As Integer, _
                                                            ByVal intIdSubClaseInstrumentoTipoGrupo As Integer, _
                                                            ByVal strCodSubClaseInstrumentoGrupo As String, _
                                                            ByVal strDscSubClaseInstrumentoGrupo As String, _
                                                            ByVal strEstSubClaseInstrumentoGrupo As String) As String


        Dim strDescError As String = "OK"
        Dim lintIdSubClaseInstrumentoTipoGrupo As Integer
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try


            SQLCommand = New SqlClient.SqlCommand("Rcp_SubClaseInstrumentoGrupo_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_SubClaseInstrumentoGrupo_Mantencion"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 20).Value = UCase(strAccion.Trim)
            'SQLCommand.Parameters.Add("pIdGrupoComisiones", SqlDbType.Int, 10).Value = IIf(intIdGrupoComisiones = 0, DBNull.Value, intIdGrupoComisiones)

            '...IdSubClaseInstrumentoGrupo
            Dim pSalIdSubClaseInstrumentoGrupo As New SqlClient.SqlParameter("pIdSubClaseInstrumentoGrupo", SqlDbType.Int, 10)
            pSalIdSubClaseInstrumentoGrupo.Direction = Data.ParameterDirection.InputOutput
            pSalIdSubClaseInstrumentoGrupo.Value = IIf(intIdSubClaseInstrumentoGrupo = 0, DBNull.Value, intIdSubClaseInstrumentoGrupo)
            SQLCommand.Parameters.Add(pSalIdSubClaseInstrumentoGrupo)


            SQLCommand.Parameters.Add("pIdSubClaseInstrumentoTipoGrupo", SqlDbType.Int, 10).Value = intIdSubClaseInstrumentoTipoGrupo
            SQLCommand.Parameters.Add("pCodSubClaseInstrumentoGrupo", SqlDbType.VarChar, 20).Value = strCodSubClaseInstrumentoGrupo
            SQLCommand.Parameters.Add("pDscSubClaseInstrumentoGrupo", SqlDbType.VarChar, 100).Value = strDscSubClaseInstrumentoGrupo
            SQLCommand.Parameters.Add("pEstSubClaseInstrumentoGrupo", SqlDbType.VarChar, 3).Value = strEstSubClaseInstrumentoGrupo


            '...Resultado
            Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultado.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                lintIdSubClaseInstrumentoTipoGrupo = CInt(SQLCommand.Parameters("pIdSubClaseInstrumentoGrupo").Value.ToString.Trim)
                If Trim(strDescError) = "OK" Then
                    MiTransaccionSQL.Commit()
                    Return ("OK")
                End If
            Else
                GoTo Salir
            End If

Salir:
            If Trim(strDescError) = "OK" Then
                MiTransaccionSQL.Commit()
                Return ("OK")
            Else
                MiTransaccionSQL.Rollback()
                Return strDescError
            End If


        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en la mantención  grupo sub clase instrumento." & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            SubClaseInstrumentoTipoGrupo_Mantencion = strDescError
        End Try
    End Function

    Public Function SubClaseInstrumentoGrupo_BuscarSubClases(ByVal intIdSubClaseInstrumentoGrupo As Integer, _
                                                             ByVal strBusca As String, _
                                                             ByVal strColumnas As String, _
                                                             ByRef strRetorno As String) As DataSet

        Dim LDS_SubClaseInstrumentoTipoGrupo As New DataSet
        Dim LDS_SubClaseInstrumentoTipoGrupo_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""


        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_SubClaseInstrumentoGrupo_BuscarSubClases"
        Lstr_NombreTabla = "SubClaseInstrumento"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdSubClaseInstrumentoGrupo", SqlDbType.Int, 10).Value = intIdSubClaseInstrumentoGrupo
            SQLCommand.Parameters.Add("pBusca", SqlDbType.VarChar, 100).Value = strBusca

            '...Resultado
            Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultado.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_SubClaseInstrumentoTipoGrupo, Lstr_NombreTabla)

            LDS_SubClaseInstrumentoTipoGrupo_Aux = LDS_SubClaseInstrumentoTipoGrupo


            If strColumnas.Trim = "" Then
                LDS_SubClaseInstrumentoTipoGrupo_Aux = LDS_SubClaseInstrumentoTipoGrupo
            Else
                LDS_SubClaseInstrumentoTipoGrupo_Aux = LDS_SubClaseInstrumentoTipoGrupo.Copy
                For Each Columna In LDS_SubClaseInstrumentoTipoGrupo.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_SubClaseInstrumentoTipoGrupo_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_SubClaseInstrumentoTipoGrupo_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            Return LDS_SubClaseInstrumentoTipoGrupo_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function SubClaseInstrumentoSCI_Mantenedor(ByVal intIdSubClaseInstrumentoGrupo As Integer, _
                                                      ByVal DtbSubClaseInstrumento As DataTable) As String
        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction
        Dim lintIdRestriccion As Integer = 0

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try

            If Not (IsNothing(DtbSubClaseInstrumento)) AndAlso DtbSubClaseInstrumento.Rows.Count > 0 Then
                For Each ldtrFila As DataRow In DtbSubClaseInstrumento.Rows
                    If ldtrFila("ACCION") <> "O" Then
                        strDescError = ""
                        SQLCommand = New SqlClient.SqlCommand("Rcp_SubClaseInstrumentoGrupo_Mantencion_SubClaseInstrumento", MiTransaccionSQL.Connection, MiTransaccionSQL)

                        SQLCommand.CommandType = CommandType.StoredProcedure
                        SQLCommand.CommandText = "Rcp_SubClaseInstrumentoGrupo_Mantencion_SubClaseInstrumento"
                        SQLCommand.Parameters.Clear()
                        SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 10).Value = IIf(ldtrFila("ACCION") = "I", "INSERTAR", "ELIMINAR")
                        SQLCommand.Parameters.Add("@pCodSubClaseInstrumentoGrupo", SqlDbType.VarChar, 10).Value = ldtrFila("COD_SUB_CLASE_INSTRUMENTO")
                        SQLCommand.Parameters.Add("@pIdSubClaseInstrumentoGrupo", SqlDbType.Int).Value = intIdSubClaseInstrumentoGrupo

                        '...Resultado
                        Dim ParametroSal As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                        ParametroSal.Direction = Data.ParameterDirection.Output
                        SQLCommand.Parameters.Add(ParametroSal)

                        SQLCommand.ExecuteNonQuery()

                        strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

                        If (strDescError <> "OK") Then
                            GoTo Salir
                        End If
                    End If
                Next
            End If

Salir:
            If Trim(strDescError) = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error al grabar sub clase a grupo. " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            SubClaseInstrumentoSCI_Mantenedor = strDescError
        End Try
    End Function
End Class
