﻿Public Class ClsGrafico

    Public Function CrearGraficoTorta(ByVal Valores() As Double, ByVal Etiquetas() As String, ByVal colors() As Integer, Optional ByVal Titulo As String = "") As System.Drawing.Image
        Dim ImgGrafico As System.Drawing.Image
        ChartDirector.Chart.setLicenseCode("DEVP-2D3G-8XUW-WY3N-3C2F-9343")


        ' Create a XYChart object of size 250 x 250 pixels
        Dim c As ChartDirector.XYChart = New ChartDirector.XYChart(600, 300)

        ' Set the plotarea at (30, 20) and of size 200 x 200 pixels
        c.setPlotArea(5, 5, 450, 125)

        ' Add a line chart layer using the given data
        c.addLineLayer(Valores)

        ' Set the labels on the x axis.
        c.xAxis().setLabels(Etiquetas)

        ' Display 1 out of 3 labels on the x-axis.
        c.xAxis().setLabelStep(3)

        ImgGrafico = c.makeImage()

        'ImageMap = c.getHTMLImageMap("Click")


        'Dim Grafico As ChartDirector.PieChart

        'Grafico = New ChartDirector.PieChart(600, 330)

        'Grafico.setBackground(&HFFFFFF)
        'Grafico.setRoundedFrame(&HFFFFFF, 20)
        'Grafico.setDropShadow(&HFFFFFF)
        'Grafico.setPieSize(180, 90, 70)
        'Grafico.setData(Valores, Etiquetas)
        'Grafico.setExplode(-1, 5)
        'Grafico.setColors2(ChartDirector.Chart.DataColor, colors)
        'Grafico.setSectorStyle(1, &HFFFFFF, 1)
        'Grafico.setLabelLayout(ChartDirector.Chart.SideLayout)

        'Grafico.setLabelFormat("<*block,halign=center*>{label}<*br*>{percent}%<*/*>")
        ''Grafico.setLabelLayout(ChartDirector.Chart.CircleLayout, 0)
        'Grafico.setLabelStyle("Arial", 7, &H0).setBackground(ChartDirector.Chart.Transparent, ChartDirector.Chart.Transparent)
        'Grafico.setSectorStyle(ChartDirector.Chart.RoundedEdgeShading, &HFFFFFF, 1)
        'Grafico.setStartAngle(135)
        ''Grafico.setLabelPos(-1, &HC50000)


        'ImgGrafico = Grafico.makeImage

        Return ImgGrafico

    End Function

    Public Function CrearGraficoTorta2(ByVal Valores() As Double, ByVal Etiquetas() As String, ByVal colors() As Integer, Optional ByVal Titulo As String = "") As System.Drawing.Image
        Dim ImgGrafico As System.Drawing.Image
        ChartDirector.Chart.setLicenseCode("DEVP-2D3G-8XUW-WY3N-3C2F-9343")

        Dim Grafico As ChartDirector.PieChart

        Grafico = New ChartDirector.PieChart(450, 400)

        Grafico.setBackground(&HFFFFFF)
        Grafico.setRoundedFrame(&HFFFFFF, 20)
        Grafico.setDropShadow(&HFFFFFF)
        Grafico.setPieSize(240, 120, 100)
        Grafico.setData(Valores, Etiquetas)
        Grafico.setExplode(-1, 9)
        Grafico.setColors2(ChartDirector.Chart.DataColor, colors)
        Grafico.setSectorStyle(1, &HFFFFFF, 1)
        'Grafico.setLabelLayout(ChartDirector.Chart.)
        Grafico.setLabelLayout(ChartDirector.Chart.SideLayout, 25)


        ' Set the label box background color the same as the sector color, with glass
        ' effect, and with 5 pixels rounded corners
        Dim t As ChartDirector.TextBox = Grafico.setLabelStyle()
        't.setBackground(ChartDirector.Chart.SameAsMainColor, ChartDirector.Chart.Transparent, _
        '    ChartDirector.Chart.glassEffect())
        't.setRoundedCorners(1)
        'Grafico.setLineColor(ChartDirector.Chart.SameAsMainColor, &H0)
        t.setBackground(&HFFFFFF, ChartDirector.Chart.Transparent)
        't.setHeight(1)

        Grafico.setLabelFormat("<*block,halign=center*>{={sector}+1}<*br*>") '{percent}%<*/*>")
        'Grafico.setLabelLayout(ChartDirector.Chart.CircleLayout, 0)
        Grafico.setLabelStyle("Impact", 10, &H0) '.setBackground(ChartDirector.Chart.Transparent, ChartDirector.Chart.Transparent)
        Grafico.setSectorStyle(ChartDirector.Chart.RoundedEdgeShading, &HFFFFFF, 1)
        Grafico.setStartAngle(135)
        'Grafico.setLabelPos(-1, &HC50000)
        Grafico.setAntiAlias(True, 2)
        Grafico.set3D(20)

        Dim b As ChartDirector.LegendBox = Grafico.addLegend(240, 385, True, "Arial Bold", 8)
        b.setAlignment(ChartDirector.Chart.Bottom)

        ' Set the legend box border to dark grey (444444), and with rounded conerns
        b.setBackground(ChartDirector.Chart.Transparent, &H444444)
        b.setRoundedCorners()

        ' Set the legend box margin to 16 pixels, and the extra line spacing between
        ' the legend entries as 5 pixels
        b.setMargin(10)
        b.setKeySpacing(0, 3)

        ' Set the legend box icon to have no border (border color same as fill color)
        b.setKeyBorder(ChartDirector.Chart.SameAsMainColor)

        ' Set the legend text to show the sector number, followed by a 120 pixels
        ' wide block showing the sector label, and a 40 pixels wide block showing the
        ' percentage
        b.setText( _
            "<*block,valign=top*>{={sector}+1}.<*advanceTo=22*>" & _
            "<*block,width=170*>{label}<*/*><*block,width=50,halign=right*>" & _
            "{percent}<*/*>%")

        ImgGrafico = Grafico.makeImage

        Return ImgGrafico

    End Function




    Public Function CrearGraficoBarrasEspecial(ByVal Valores() As Double, ByVal Etiquetas() As String, ByVal colors() As Integer, Optional ByRef ImageMap As String = "", Optional ByVal Titulo As String = "") As System.Drawing.Image
        Dim ImgGrafico As System.Drawing.Image
        ChartDirector.Chart.setLicenseCode("DEVP-2D3G-8XUW-WY3N-3C2F-9343")


        ' Create a XYChart object of size 250 x 250 pixels
        Dim c As ChartDirector.XYChart = New ChartDirector.XYChart(600, 300)

        ' Set the plotarea at (30, 20) and of size 200 x 200 pixels
        c.setPlotArea(150, 5, 415, 240)

        '' Add a bar chart layer using the given data
        'c.addBarLayer(Valores)

        ' Set the labels on the x axis.
        c.xAxis().setLabels(Etiquetas)
        c.xAxis().setLabelStyle("Arial ", 10, &H0)
        c.yAxis().setLabelStyle("Arial ", 10, ChartDirector.Chart.Transparent)
        c.yAxis().setTitle(Titulo, "Arial Bold", 12)

        Dim layer As ChartDirector.BarLayer = c.addBarLayer(Valores, colors, Etiquetas)

        layer.setAggregateLabelStyle("Arial", 10).setBackground(ChartDirector.Chart.Transparent, ChartDirector.Chart.Transparent, 0)
        'layer.setAggregateLabelFormat("{value|0.,}%")
        layer.setAggregateLabelFormat("{value} %")
        c.swapXY(True)
        'layer.setBarWidth(20)
        layer.setBarGap(0.8)

        ImgGrafico = c.makeImage()

        ImageMap = c.getHTMLImageMap("Click")

        ''''''''''''''''''''''
        'Dim grafico As ChartDirector.XYChart = New ChartDirector.XYChart(600, 300)

        '' Create a XYChart object of size 300 x 220 pixels. Use golden background color.
        '' Use a 2 pixel 3D border.
        ''Dim grafico As ChartDirector.XYChart = New ChartDirector.XYChart(300, 220, ChartDirector.Chart.Transparent, -1, 2)

        'grafico.setPlotArea(10, 0, 500, 200, &HFFFFFF, &HFFFFFF, _
        '&H0, &H0, ChartDirector.Chart.TextColor)

        'grafico.yAxis().setColors(ChartDirector.Chart.TextColor)
        'grafico.xAxis().setColors(ChartDirector.Chart.TextColor)
        'grafico.yAxis2().setColors(ChartDirector.Chart.TextColor)
        'grafico.xAxis2().setColors(ChartDirector.Chart.TextColor)
        'grafico.xAxis().setWidth(2)
        'grafico.yAxis().setWidth(2)

        'grafico.yAxis().setLabelStyle("Arial Bold", 10, &HC50000)
        'grafico.xAxis().setLabelStyle("Arial ", 10, &HC50000)

        'grafico.yAxis2().setLabelStyle("Arial Bold", 10, &HC50000)
        'grafico.xAxis2().setLabelStyle("Arial ", 10, &HC50000)


        ''grafico.xAxis().setLabels(Etiquetas)
        ''grafico.yAxis().setLabels(Valores)

        'Dim layer As ChartDirector.BarLayer = grafico.addBarLayer(Valores, colors, Etiquetas)

        'layer.setAggregateLabelStyle("Arial", 10).setBackground(ChartDirector.Chart.Transparent, ChartDirector.Chart.Transparent, 0)
        'layer.setAggregateLabelFormat("{value|0.,}%")
        'grafico.swapXY(True)
        ''layer.setBarWidth(20)
        'layer.setBarGap(0.7)


        'ImgGrafico = grafico.makeImage()

        'ImageMap = grafico.getHTMLImageMap("Click")
        Return ImgGrafico

    End Function
    Public Function CrearGraficoLinea(ByVal Valores1() As Double, ByVal Valores2() As Double, ByVal Valores3() As Double, ByVal Etiquetas() As String, ByVal colors() As Integer, Optional ByRef ImageMap As String = "") As System.Drawing.Image
        Dim ImgGrafico As System.Drawing.Image
        ChartDirector.Chart.setLicenseCode("DEVP-2D3G-8XUW-WY3N-3C2F-9343")
        Dim lpFecha_Anterior As String
        Dim X As Integer
        ' Inicio
        lpFecha_Anterior = ""
        For X = LBound(Etiquetas) To UBound(Etiquetas) - 1
            If X = LBound(Etiquetas) Then
                lpFecha_Anterior = Month(CDate(Etiquetas(X)))
                Etiquetas(X) = Fnt_Meses(CInt(lpFecha_Anterior)) & "-" & Format(CDate(Etiquetas(X)), "yy")
            Else
                If Month(CDate(Etiquetas(X))) = lpFecha_Anterior Then
                    Etiquetas(X) = ""
                Else
                    lpFecha_Anterior = Month(CDate(Etiquetas(X)))
                    Etiquetas(X) = Fnt_Meses(CInt(lpFecha_Anterior)) & "-" & Format(CDate(Etiquetas(X)), "yy")
                End If
            End If
        Next
        ' Fin

        Dim c As ChartDirector.XYChart = New ChartDirector.XYChart(600, 375)

        ' Add a title to the chart using 18 pts Times Bold Italic font
        c.addTitle("                   Rentabilidad Anual", "Arial Bold", 12)

        c.setPlotArea(110, 35, 480, 300)

        'c.addLegend(120, 350, False, "Arial Bold", 10).setBackground(ChartDirector.Chart.Transparent)
        c.addLegend(130, 350, False, "Arial Bold", 10).setBackground(ChartDirector.Chart.Transparent)

        c.xAxis().setLabels(Etiquetas).setFontAngle(45)
        c.xAxis().setLabelFormat("{value|MMM}")
        c.xAxis().setLabelStep(1)

        c.yAxis().setLabelFormat("{value|2.} %")
        c.yAxis().setLabelStep(5)
        c.xAxis().setLabelStyle("Arial Bold", 7, &H0&, 30).setPos(0, -1)

        ' Add a line layer to the chart
        Dim layer As ChartDirector.LineLayer = c.addLineLayer2

        ' Set the line width to 3 pixels
        layer.setLineWidth(3)
        layer.setDataLabelStyle("Arial Bold", 9, &H0&)
        layer.addDataSet(Valores1, &HFF0000, "En Pesos ($)")

        'layer.addDataSet(Valores2, &H8800, "Nacional ($)")
        'layer.addDataSet(Valores3, c.dashLineColor(&H3333FF), "Internacional (US$)")

        ImgGrafico = c.makeImage()

        Return ImgGrafico

    End Function
    
    Public Function Fnt_Meses(ByVal mes As Integer) As String
        Dim lstrMes As String = ""
        Select Case mes
            Case 1
                lstrMes = "ENE"
            Case 2
                lstrMes = "FEB"
            Case 3
                lstrMes = "MAR"
            Case 4
                lstrMes = "ABR"
            Case 5
                lstrMes = "MAY"
            Case 6
                lstrMes = "JUN"
            Case 7
                lstrMes = "JUL"
            Case 8
                lstrMes = "AGO"
            Case 9
                lstrMes = "SEP"
            Case 10
                lstrMes = "OCT"
            Case 11
                lstrMes = "NOV"
            Case 12
                lstrMes = "DIC"
        End Select
        Return lstrMes
    End Function
End Class
