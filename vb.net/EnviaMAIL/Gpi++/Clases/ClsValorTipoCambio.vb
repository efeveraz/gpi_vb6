﻿Public Class ClsValorTipoCambio
    Public Function BuscarTiposCambios(ByVal strColumnas As String, _
                                        ByRef strRetorno As String) As DataSet

        Dim LDS_TipoCambio As New DataSet
        Dim LDS_TipoCambio_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_TipoCambio_Buscar"
        Lstr_NombreTabla = "TipoCambio"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_TipoCambio, Lstr_NombreTabla)

            LDS_TipoCambio_Aux = LDS_TipoCambio

            If strColumnas.Trim = "" Then
                LDS_TipoCambio_Aux = LDS_TipoCambio
            Else
                LDS_TipoCambio_Aux = LDS_TipoCambio.Copy
                For Each Columna In LDS_TipoCambio.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_TipoCambio_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_TipoCambio_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next


            strRetorno = "OK"
            LDS_TipoCambio.Dispose()

            Return LDS_TipoCambio_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function BuscarValorPorFecha(ByVal strFecha As String, _
                                        ByVal strColumnas As String, _
                                        ByRef strRetorno As String) As DataSet

        Dim LDS_ValorTipoCambio As New DataSet
        Dim LDS_ValorTipoCambio_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_ValorTipoCambio_BuscarPorFecha"
        Lstr_NombreTabla = "ValorTipoCambio"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("@pFecha", SqlDbType.Char, 10).Value = strFecha

            Dim oParamSal As New SqlClient.SqlParameter("@pResultado", SqlDbType.Char, 200)
            oParamSal.Direction = ParameterDirection.Output
            SQLCommand.Parameters.Add(oParamSal)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_ValorTipoCambio, Lstr_NombreTabla)

            LDS_ValorTipoCambio_Aux = LDS_ValorTipoCambio

            If strColumnas.Trim = "" Then
                LDS_ValorTipoCambio_Aux = LDS_ValorTipoCambio
            Else
                LDS_ValorTipoCambio_Aux = LDS_ValorTipoCambio.Copy
                For Each Columna In LDS_ValorTipoCambio.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_ValorTipoCambio_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_ValorTipoCambio_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = SQLCommand.Parameters("@pResultado").Value.ToString.Trim

            LDS_ValorTipoCambio.Dispose()

            Return LDS_ValorTipoCambio_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function BuscarValorPorTipo(ByVal intIdTipoCambio As Integer, _
                                       ByVal strFechaDesde As String, _
                                       ByVal strFechaHasta As String, _
                                       ByVal strColumnas As String, _
                                       ByRef strRetorno As String) As DataSet

        Dim LDS_ValorTipoCambio As New DataSet
        Dim LDS_ValorTipoCambio_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_ValorTipoCambio_BuscarPorTipoCambio"
        Lstr_NombreTabla = "ValorTipoCambio"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("@pIdTipoCambio", SqlDbType.Int).Value = intIdTipoCambio
            SQLCommand.Parameters.Add("@pfechaDesde", SqlDbType.Char, 10).Value = strFechaDesde
            SQLCommand.Parameters.Add("@pfechaHasta", SqlDbType.Char, 10).Value = strFechaHasta

            Dim oParamSal As New SqlClient.SqlParameter("@pResultado", SqlDbType.Char, 200)
            oParamSal.Direction = ParameterDirection.Output
            SQLCommand.Parameters.Add(oParamSal)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_ValorTipoCambio, Lstr_NombreTabla)

            LDS_ValorTipoCambio_Aux = LDS_ValorTipoCambio

            If strColumnas.Trim = "" Then
                LDS_ValorTipoCambio_Aux = LDS_ValorTipoCambio
            Else
                LDS_ValorTipoCambio_Aux = LDS_ValorTipoCambio.Copy
                For Each Columna In LDS_ValorTipoCambio.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_ValorTipoCambio_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_ValorTipoCambio_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = SQLCommand.Parameters("@pResultado").Value.ToString.Trim

            LDS_ValorTipoCambio.Dispose()

            Return LDS_ValorTipoCambio_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Sub ValorTipoCambioMantenedor(ByVal DS_ValorTipoCambio As DataSet, _
                                         ByRef strRetorno As String)
        Dim DR_Fila As DataRow = Nothing
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        'Abre la conexion
        Connect.Abrir()

        Dim MiTransaccion As SqlClient.SqlTransaction


        MiTransaccion = Connect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_ValorTipoCambio_Mantencion", MiTransaccion.Connection, MiTransaccion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_ValorTipoCambio_Mantencion"

            For Each DR_Fila In DS_ValorTipoCambio.Tables(0).Select("OPERACION = 1")
                SQLCommand.Parameters.Clear()

                'INSERTA
                If IsDBNull(DR_Fila("ID_VALOR_TIPO_CAMBIO")) And (Not IsDBNull(DR_Fila("VALOR"))) Then
                    SQLCommand.Parameters.Add("@pAccion", SqlDbType.VarChar, 10).Value = "INSERTAR"

                    SQLCommand.Parameters.Add("@pIdTipoCambio", SqlDbType.Int).Value = DR_Fila("ID_TIPO_CAMBIO")
                    SQLCommand.Parameters.Add("@pfecha", SqlDbType.Char, 10).Value = DR_Fila("FECHA")
                    SQLCommand.Parameters.Add("@pValor", SqlDbType.Decimal, 18).Value = DR_Fila("VALOR")

                    Dim ParametroIdValorTipoCambio As New SqlClient.SqlParameter("@pIdValorTipoCambio", SqlDbType.Int)
                    ParametroIdValorTipoCambio.Direction = Data.ParameterDirection.Output
                    SQLCommand.Parameters.Add(ParametroIdValorTipoCambio)

                End If

                'MODIFICA
                If (Not (IsDBNull(DR_Fila("ID_VALOR_TIPO_CAMBIO")))) And (Not IsDBNull(DR_Fila("VALOR"))) Then
                    SQLCommand.Parameters.Add("@pAccion", SqlDbType.VarChar, 10).Value = "MODIFICAR"

                    SQLCommand.Parameters.Add("@pIdValorTipoCambio", SqlDbType.Int).Value = DR_Fila("ID_VALOR_TIPO_CAMBIO")
                    SQLCommand.Parameters.Add("@pIdTipoCambio", SqlDbType.Int).Value = DR_Fila("ID_TIPO_CAMBIO")
                    SQLCommand.Parameters.Add("@pfecha", SqlDbType.Char, 10).Value = DR_Fila("FECHA")
                    SQLCommand.Parameters.Add("@pValor", SqlDbType.Decimal, 18).Value = DR_Fila("VALOR")

                End If

                'ELIMINA
                If (Not (IsDBNull(DR_Fila("ID_VALOR_TIPO_CAMBIO")))) And (IsDBNull(DR_Fila("VALOR"))) Then
                    SQLCommand.Parameters.Add("@pAccion", SqlDbType.VarChar, 10).Value = "ELIMINAR"

                    SQLCommand.Parameters.Add("@pIdValorTipoCambio", SqlDbType.Int).Value = DR_Fila("ID_VALOR_TIPO_CAMBIO")

                End If

                Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                ParametroSal2.Direction = Data.ParameterDirection.Output
                SQLCommand.Parameters.Add(ParametroSal2)

                SQLCommand.ExecuteNonQuery()

                strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim

                If Trim(strRetorno) <> "OK" Then
                    Exit For
                End If
            Next
            If Trim(strRetorno) = "OK" Then
                MiTransaccion.Commit()
            Else
                MiTransaccion.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccion.Rollback()
            strRetorno = "Error al grabar Valor Tipo Cambio." & vbCr & Ex.Message
        Finally
            Connect.Cerrar()
        End Try
    End Sub
    Public Function TraerCambioMoneda(ByVal strMonedaOrigen As String, _
                                  ByVal strMonedaDestino As String, _
                                  ByVal strFechaConsulta As String, _
                                  ByRef strDescError As String) As DataSet

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion

        Dim LDS_CambioMoneda As New DataSet
        Dim LDS_CambioMoneda_Aux As New DataSet
        Dim Lstr_NombreTabla As String = ""
        Dim LstrProcedimiento As String = ""

        'Abre la conexion
        Connect.Abrir()

        LstrProcedimiento = "Rcp_Instrumento_BuscarCambio"
        Lstr_NombreTabla = "CambioMoneda"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pMonedaOrigen", SqlDbType.VarChar, 3).Value = strMonedaOrigen
            SQLCommand.Parameters.Add("pMonedaDestino", SqlDbType.VarChar, 3).Value = strMonedaDestino
            SQLCommand.Parameters.Add("pFecha", SqlDbType.VarChar, 10).Value = strFechaConsulta

            Dim ParametroSal1 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal1.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal1)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_CambioMoneda, Lstr_NombreTabla)

            LDS_CambioMoneda_Aux = LDS_CambioMoneda

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            LDS_CambioMoneda.Dispose()

            Return LDS_CambioMoneda_Aux

        Catch ex As Exception
            strDescError = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function BuscarTodosPorFecha(ByVal strFecha As String, _
                                            ByVal strColumnas As String, _
                                            ByRef strRetorno As String) As DataSet

        Dim LDS_ValorTipoCambio As New DataSet
        Dim LDS_ValorTipoCambio_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        'Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_ValorTipoCambio_BuscarTodosPorFecha"
        Lstr_NombreTabla = "ValorTipoCambio"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("@pFecha", SqlDbType.Char, 10).Value = strFecha

            Dim oParamSal As New SqlClient.SqlParameter("@pResultado", SqlDbType.Char, 200)
            oParamSal.Direction = ParameterDirection.Output
            SQLCommand.Parameters.Add(oParamSal)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_ValorTipoCambio, Lstr_NombreTabla)

            LDS_ValorTipoCambio_Aux = LDS_ValorTipoCambio

            'If strColumnas.Trim = "" Then
            '    LDS_ValorTipoCambio_Aux = LDS_ValorTipoCambio
            'Else
            '    LDS_ValorTipoCambio_Aux = LDS_ValorTipoCambio.Copy
            '    For Each Columna In LDS_ValorTipoCambio.Tables(Lstr_NombreTabla).Columns
            '        Remove = True
            '        For LInt_Col = 0 To LArr_NombreColumna.Length - 1
            '            LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
            '            If Columna.ColumnName = LInt_NomCol Then
            '                Remove = False
            '            End If
            '        Next
            '        If Remove Then
            '            LDS_ValorTipoCambio_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
            '        End If
            '    Next
            'End If

            'For LInt_Col = 0 To LArr_NombreColumna.Length - 1
            '    LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
            '    For Each Columna In LDS_ValorTipoCambio_Aux.Tables(Lstr_NombreTabla).Columns
            '        If Columna.ColumnName = LInt_NomCol Then
            '            Columna.SetOrdinal(LInt_Col)
            '            Exit For
            '        End If
            '    Next
            'Next

            strRetorno = SQLCommand.Parameters("@pResultado").Value.ToString.Trim

            LDS_ValorTipoCambio.Dispose()

            Return LDS_ValorTipoCambio_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function
End Class
