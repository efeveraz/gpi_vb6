﻿Public Class ClsSerieCupones

    Public Function SerieCupones_Buscar(ByVal dblIdInstrumento As Double, _
                                    ByVal strNemotecnico As String, _
                                    ByVal strSubClase As String, _
                                    ByRef strRetorno As String) As DataSet

        Dim LDS_SerieCupones As New DataSet
        Dim LDS_SerieCupones_Aux As New DataSet
        'Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        'Dim LInt_Col As Integer = 0
        'Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        'Dim Columna As DataColumn
        'Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_SerieCupones_Buscar"
        Lstr_NombreTabla = "SerieCupones"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("pIdInstrumento", SqlDbType.Float, 10).Value = dblIdInstrumento
            SQLCommand.Parameters.Add("pNemotecnico", SqlDbType.VarChar, 50).Value = strNemotecnico
            SQLCommand.Parameters.Add("pSubClase", SqlDbType.VarChar, 10).Value = strSubClase

            '...Resultado
            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_SerieCupones, Lstr_NombreTabla)

            LDS_SerieCupones_Aux = LDS_SerieCupones

            'If strColumnas.Trim = "" Then
            '    LDS_SerieCupones_Aux = LDS_SerieCupones
            'Else
            '    LDS_SerieCupones_Aux = LDS_SerieCupones.Copy
            '    For Each Columna In LDS_SerieCupones.Tables(Lstr_NombreTabla).Columns
            '        Remove = True
            '        For LInt_Col = 0 To LArr_NombreColumna.Length - 1
            '            LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
            '            If Columna.ColumnName = LInt_NomCol Then
            '                Remove = False
            '            End If
            '        Next
            '        If Remove Then
            '            LDS_SerieCupones_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
            '        End If
            '    Next
            'End If

            'For LInt_Col = 0 To LArr_NombreColumna.Length - 1
            '    LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
            '    For Each Columna In LDS_SerieCupones_Aux.Tables(Lstr_NombreTabla).Columns
            '        If Columna.ColumnName = LInt_NomCol Then
            '            Columna.SetOrdinal(LInt_Col)
            '            Exit For
            '        End If
            '    Next
            'Next

            strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            LDS_SerieCupones.Dispose()
            Return LDS_SerieCupones_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function SerieCupones_Mantenedor(ByVal dtbCupones As DataTable, _
                                            ByVal dtbSerie As DataTable, _
                                            ByVal dblIdInstrumento As Double, _
                                            ByVal strNemotecnico As String, _
                                            ByRef strRetorno As String) As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction
        Dim llngIdSerie As Long = 0

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion
        Try

            'SQLCommand = New SqlClient.SqlCommand("Rcp_Cs_Tb_Series_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)

            'SQLCommand.CommandType = CommandType.StoredProcedure
            'SQLCommand.CommandText = "Rcp_Cs_Tb_Series_Mantencion"
            'SQLCommand.Parameters.Clear()

            Dim dtrCupon As DataRow = dtbCupones.Rows(0)
            Dim dtrSerie As DataRow = dtbSerie.Rows(0)

            'SQLCommand.Parameters.Add("pIdSerie", SqlDbType.Float, 12).Value = IIf(IsDBNull(dtrSerie("IDSERIE")), 0, dtrSerie("IDSERIE"))
            'SQLCommand.Parameters.Add("pCodSerie", SqlDbType.VarChar, 50).Value = strNemotecnico
            'SQLCommand.Parameters.Add("pCodTipoAmortizacion", SqlDbType.VarChar, 1).Value = "0"
            'SQLCommand.Parameters.Add("pTasaEmision", SqlDbType.Float, 18).Value = 0
            'SQLCommand.Parameters.Add("pTasaEfectiva", SqlDbType.Float, 18).Value = dtrSerie("TASAEFECTIVA")
            'SQLCommand.Parameters.Add("pFechaEmision", SqlDbType.VarChar, 10).Value = gdteFechaSistema
            'SQLCommand.Parameters.Add("pFechaVcto", SqlDbType.VarChar, 10).Value = DateAdd(DateInterval.Year, dtrSerie("ANOS"), gdteFechaSistema)
            'SQLCommand.Parameters.Add("pNroCupones", SqlDbType.Int, 10).Value = dtrSerie("NROCUPONES")
            'SQLCommand.Parameters.Add("pNroAmortizaciones", SqlDbType.Int, 10).Value = dtrSerie("NROAMORTIZACIONES")
            'SQLCommand.Parameters.Add("pDiaVctoCupon", SqlDbType.Int, 10).Value = 1
            'SQLCommand.Parameters.Add("pPeriodoCupon", SqlDbType.Int, 10).Value = 0
            'SQLCommand.Parameters.Add("pAnos", SqlDbType.Float, 10).Value = dtrSerie("ANOS")
            'SQLCommand.Parameters.Add("pDecimalestd", SqlDbType.Int, 10).Value = 0
            'SQLCommand.Parameters.Add("pFechaPrimerCorte", SqlDbType.VarChar, 10).Value = dtrSerie("FECHAPRIMERCORTE")
            'SQLCommand.Parameters.Add("pCorteMinimo", SqlDbType.Float, 18).Value = 0
            'SQLCommand.Parameters.Add("pIdEmisor", SqlDbType.Float, 12).Value = 0
            'SQLCommand.Parameters.Add("pIdMoneda", SqlDbType.Float, 12).Value = 0
            'SQLCommand.Parameters.Add("pIdFamilia", SqlDbType.Float, 12).Value = 0
            'SQLCommand.Parameters.Add("pSerieBanco", SqlDbType.VarChar, 3).Value = ""
            'SQLCommand.Parameters.Add("pIdInstrumento", SqlDbType.Float, 10).Value = dblIdInstrumento

            ''...Id Operacion Detalle
            'Dim ParametroSal1 As New SqlClient.SqlParameter("pIdGenerado", SqlDbType.Float, 10)
            'ParametroSal1.Direction = Data.ParameterDirection.Output
            'SQLCommand.Parameters.Add(ParametroSal1)

            ''...Resultado
            'Dim ParametroSal As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            'ParametroSal.Direction = Data.ParameterDirection.Output
            'SQLCommand.Parameters.Add(ParametroSal)

            'SQLCommand.ExecuteNonQuery()

            'strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            '' ELIMINAMOS LO CUPONES EXISTENTES
            'If strDescError.ToUpper.Trim = "OK" Then
            '    If IsDBNull(dtrSerie("IDSERIE")) Then
            '        llngIdSerie = SQLCommand.Parameters("pIdGenerado").Value
            '    ElseIf IsDBNull(dtrCupon("IDSERIE")) Then ' = 0 Then
            '        llngIdSerie = SQLCommand.Parameters("pIdGenerado").Value
            '    ElseIf dtrCupon("CODSERIE") = 0 Then
            '        llngIdSerie = SQLCommand.Parameters("pIdGenerado").Value
            '    Else
            '        llngIdSerie = dtrSerie("IDSERIE")
            '    End If
            'End If

            SQLCommand = New SqlClient.SqlCommand("Rcp_Cs_Tb_Cupones_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Cs_Tb_Cupones_Mantencion"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pCodSerie", SqlDbType.VarChar, 50).Value = dtrCupon("CODSERIE")
            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 10).Value = "ELIMINAR"

            '...Resultado
            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If Trim(strDescError) <> "OK" Then
                GoTo Salir
            End If

            If strDescError.ToUpper.Trim = "OK" Then

                For Each DrCupon As DataRow In dtbCupones.Rows

                    SQLCommand = New SqlClient.SqlCommand("Rcp_Cs_Tb_Cupones_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)

                    SQLCommand.CommandType = CommandType.StoredProcedure
                    SQLCommand.CommandText = "Rcp_Cs_Tb_Cupones_Mantencion"
                    SQLCommand.Parameters.Clear()

                    SQLCommand.Parameters.Add("pCodSerie", SqlDbType.VarChar, 50).Value = DrCupon("CODSERIE")
                    SQLCommand.Parameters.Add("pNroCupon", SqlDbType.Decimal, 3).Value = DrCupon("NROCUPON")
                    SQLCommand.Parameters.Add("pFechaCupon", SqlDbType.VarChar, 10).Value = DrCupon("FECHA")
                    SQLCommand.Parameters.Add("pInteres", SqlDbType.Decimal, 11).Value = DrCupon("INTERES")
                    SQLCommand.Parameters.Add("pCapital", SqlDbType.Decimal, 11).Value = DrCupon("AMORTIZACION")
                    SQLCommand.Parameters.Add("pFlujo", SqlDbType.Decimal, 11).Value = DrCupon("FLUJO")
                    SQLCommand.Parameters.Add("pSaldo", SqlDbType.Decimal, 11).Value = DrCupon("SALDO")
                    '-- SQLCommand.Parameters.Add("pIdSerie", SqlDbType.Decimal, 12).Value = llngIdSerie
                    SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 10).Value = "GRABAR"

                    '...Resultado
                    Dim ParametroSal3 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                    ParametroSal3.Direction = Data.ParameterDirection.Output
                    SQLCommand.Parameters.Add(ParametroSal3)

                    SQLCommand.ExecuteNonQuery()

                    strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

                    If Trim(strDescError) <> "OK" Then
                        GoTo Salir
                    End If
                Next
            End If

Salir:
            If Trim(strDescError) = "OK" Then
                MiTransaccionSQL.Commit()
                strRetorno = "OK"
            Else
                MiTransaccionSQL.Rollback()
                strRetorno = strDescError
            End If
        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error al Grabar Operación " & vbCr & Ex.Message
            strRetorno = strDescError
        Finally
            SQLConnect.Cerrar()
        End Try

        Return strDescError
    End Function
    Public Function SerieCupones_Act_CM() As String
        Dim strDescError As String = "OK"
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction
        Dim llngIdSerie As Long = 0
        Dim lstrProc As String = "Rcp_CM_Act_SeriesCupones"

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion
        Try

            SQLCommand = New SqlClient.SqlCommand(lstrProc, MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = lstrProc
            SQLCommand.Parameters.Clear()
            SQLCommand.CommandTimeout = 0

            '...Resultado
            Dim ParametroSal As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If Trim(strDescError) = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If
        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error al actualizar Series y Cupones en la Carga Masiva " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            SerieCupones_Act_CM = strDescError
        End Try
    End Function
End Class
