﻿Public Class ClsBloqueo
    Public Function Conceptos_Consultar(ByVal intConcepto As Integer, _
                                                ByVal strDscConcepto As String, _
                                                ByVal strColumnas As String, _
                                                ByRef strRetorno As String, _
                                                Optional ByVal strDscTipoBloqueo As String = "", _
                                                Optional ByVal strEstado As String = "VIG") As DataSet

        Dim LDS_ConceptoBloq As New DataSet
        Dim LDS_ConceptoBloq_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Conceptos_Consultar"
        Lstr_NombreTabla = "Conceptos"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdConcepto", SqlDbType.Float, 10).Value = IIf(intConcepto = 0, DBNull.Value, intConcepto)
            SQLCommand.Parameters.Add("pDscConcepto", SqlDbType.VarChar, 50).Value = IIf(strDscConcepto.Trim = "", DBNull.Value, strDscConcepto.Trim)
            SQLCommand.Parameters.Add("pDscTipoConcepto", SqlDbType.VarChar, 50).Value = IIf(strDscTipoBloqueo.Trim = "", DBNull.Value, strDscTipoBloqueo.Trim)
            SQLCommand.Parameters.Add("pEstado", SqlDbType.VarChar, 50).Value = IIf(strEstado.Trim = "", DBNull.Value, strEstado.Trim)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_ConceptoBloq, Lstr_NombreTabla)

            LDS_ConceptoBloq_Aux = LDS_ConceptoBloq

            If strColumnas.Trim = "" Then
                LDS_ConceptoBloq_Aux = LDS_ConceptoBloq
            Else
                LDS_ConceptoBloq_Aux = LDS_ConceptoBloq.Copy
                For Each Columna In LDS_ConceptoBloq.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_ConceptoBloq_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_ConceptoBloq_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_ConceptoBloq.Dispose()
            Return LDS_ConceptoBloq_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function
    Public Function TipoConcepto_Ver(ByVal intIdTipoBloqueo As Integer, _
                                     ByVal strColumnas As String, _
                                     ByRef strRetorno As String, _
                                     Optional ByVal strBloqueo As String = "") As DataSet


        Dim LDS_TipoBloqueo As New DataSet
        Dim LDS_TipoBloqueo_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_TipoConceptos_Consultar"
        Lstr_NombreTabla = "TIPO_CONCEPTOS"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdTipoConcepto", SqlDbType.Float, 10).Value = IIf(intIdTipoBloqueo = 0, DBNull.Value, intIdTipoBloqueo)
            SQLCommand.Parameters.Add("pFiltroBloqueo", SqlDbType.VarChar, 10).Value = IIf(strBloqueo.Trim = "", DBNull.Value, strBloqueo.Trim)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_TipoBloqueo, Lstr_NombreTabla)

            LDS_TipoBloqueo_Aux = LDS_TipoBloqueo

            If strColumnas.Trim = "" Then
                LDS_TipoBloqueo_Aux = LDS_TipoBloqueo
            Else
                LDS_TipoBloqueo_Aux = LDS_TipoBloqueo.Copy
                For Each Columna In LDS_TipoBloqueo.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_TipoBloqueo_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_TipoBloqueo_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_TipoBloqueo.Dispose()
            Return LDS_TipoBloqueo_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function
    Public Function Conceptos_Mantenedor(ByVal strAccion As String, _
                                         ByVal intIdConcepto As Integer, _
                                         ByVal strDscConcepto As String, _
                                         ByVal intIdTipoConcepto As Integer, _
                                         ByVal strEstConcepto As String) As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_Concepto_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Concepto_Mantencion"
            SQLCommand.Parameters.Clear()


            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 10).Value = strAccion
            SQLCommand.Parameters.Add("pIdConcepto", SqlDbType.VarChar, 3).Value = intIdConcepto
            SQLCommand.Parameters.Add("pIdTipoConcepto", SqlDbType.VarChar, 4).Value = intIdTipoConcepto
            SQLCommand.Parameters.Add("pDscConcepto", SqlDbType.VarChar, 60).Value = strDscConcepto
            SQLCommand.Parameters.Add("pEstConcepto", SqlDbType.VarChar, 3).Value = strEstConcepto


            '...Resultado
            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            Conceptos_Mantenedor = strDescError
        End Try
    End Function
    Public Function Bloqueo_Mantenedor(ByVal strAccion As String, _
                                       ByVal strBloqueo As String, _
                                       ByVal dblIdConceptoBloqueo As Double, _
                                       ByVal DS_Bloqueo As DataSet, _
                                       ByRef strDescError As String, _
                                       Optional ByVal dtFechaBloqueo As String = "0") As String


        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try

            If strAccion = "BLO" Then
                If Not (IsNothing(DS_Bloqueo)) Then
                    If DS_Bloqueo.Tables.Count > 0 And DS_Bloqueo.Tables(0).Rows.Count > 0 Then
                        For Each DR_Filas In DS_Bloqueo.Tables(0).Rows

                            SQLCommand = New SqlClient.SqlCommand("Rcp_Bloqueo_Mantenedor", MiTransaccionSQL.Connection, MiTransaccionSQL)
                            SQLCommand.CommandType = CommandType.StoredProcedure
                            SQLCommand.CommandText = "Rcp_Bloqueo_Mantenedor"
                            SQLCommand.Parameters.Clear()

                            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 20).Value = "INSERTAR"
                            If strBloqueo = "POR_CUENTA" Then
                                If DR_Filas("SEL") = 1 Then
                                    SQLCommand.Parameters.Add("pIdConceptoBloqueo", SqlDbType.Int, 10).Value = dblIdConceptoBloqueo
                                    SQLCommand.Parameters.Add("pIdInstrumento", SqlDbType.Int, 10).Value = DR_Filas("ID_INSTRUMENTO")
                                    SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Int, 12).Value = DR_Filas("ID_CUENTA")
                                    SQLCommand.Parameters.Add("pIdPatrimonioCuenta", SqlDbType.Int, 12).Value = DBNull.Value
                                    SQLCommand.Parameters.Add("pSaldoBloqueo", SqlDbType.Float, 18).Value = DR_Filas("CANTIDAD")
                                    SQLCommand.Parameters.Add("pEstado", SqlDbType.VarChar, 3).Value = strAccion
                                    SQLCommand.Parameters.Add("pIdCliente", SqlDbType.Int, 10).Value = DBNull.Value
                                    SQLCommand.Parameters.Add("pIdGrupoCuenta", SqlDbType.Int, 12).Value = DBNull.Value
                                    SQLCommand.Parameters.Add("pIdCajaCuenta", SqlDbType.Int, 12).Value = DBNull.Value
                                    SQLCommand.Parameters.Add("pIdCartera", SqlDbType.Int, 12).Value = IIf(DR_Filas("ID_CARTERA") = 0, DBNull.Value, DR_Filas("ID_CARTERA"))
                                    SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Float, 10).Value = glngIdUsuario
                                    SQLCommand.Parameters.Add("pFechaBloqueo", SqlDbType.VarChar, 10).Value = IIf(dtFechaBloqueo = "0", DBNull.Value, dtFechaBloqueo)
                                    ''...Resultado
                                    Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                                    pSalResultado.Direction = Data.ParameterDirection.Output
                                    SQLCommand.Parameters.Add(pSalResultado)

                                    SQLCommand.ExecuteNonQuery()

                                    strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim


                                    If strDescError.ToUpper.Trim <> "OK" Then
                                        MiTransaccionSQL.Rollback()
                                        Exit For
                                    End If
                                End If
                            ElseIf strBloqueo = "POR_INSTRUMENTO" Then
                                If DR_Filas("SEL") = 1 Then
                                    SQLCommand.Parameters.Add("pIdConceptoBloqueo", SqlDbType.Int, 10).Value = dblIdConceptoBloqueo
                                    SQLCommand.Parameters.Add("pIdInstrumento", SqlDbType.Int, 10).Value = DR_Filas("ID_INSTRUMENTO")
                                    SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Int, 12).Value = DBNull.Value
                                    SQLCommand.Parameters.Add("pIdPatrimonioCuenta", SqlDbType.Int, 12).Value = DBNull.Value
                                    SQLCommand.Parameters.Add("pSaldoBloqueo", SqlDbType.Float, 18).Value = DBNull.Value
                                    SQLCommand.Parameters.Add("pEstado", SqlDbType.VarChar, 3).Value = strAccion
                                    SQLCommand.Parameters.Add("pIdCliente", SqlDbType.Int, 10).Value = DBNull.Value
                                    SQLCommand.Parameters.Add("pIdGrupoCuenta", SqlDbType.Int, 12).Value = DBNull.Value
                                    SQLCommand.Parameters.Add("pIdCajaCuenta", SqlDbType.Int, 12).Value = DBNull.Value
                                    SQLCommand.Parameters.Add("pIdCartera", SqlDbType.Int, 12).Value = DBNull.Value
                                    SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Float, 10).Value = glngIdUsuario
                                    ''...Resultado
                                    Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                                    pSalResultado.Direction = Data.ParameterDirection.Output
                                    SQLCommand.Parameters.Add(pSalResultado)

                                    SQLCommand.ExecuteNonQuery()

                                    strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim


                                    If strDescError.ToUpper.Trim <> "OK" Then
                                        MiTransaccionSQL.Rollback()
                                        Exit For
                                    End If
                                End If
                            ElseIf strBloqueo = "POR_CLIENTE_CUENTA" Then
                                If DR_Filas("SEL") = 1 Then
                                    SQLCommand.Parameters.Add("pIdConceptoBloqueo", SqlDbType.Int, 10).Value = dblIdConceptoBloqueo
                                    SQLCommand.Parameters.Add("pIdInstrumento", SqlDbType.Int, 10).Value = DBNull.Value
                                    SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Int, 12).Value = DR_Filas("ID_CUENTA")
                                    SQLCommand.Parameters.Add("pIdPatrimonioCuenta", SqlDbType.Int, 12).Value = DBNull.Value
                                    SQLCommand.Parameters.Add("pSaldoBloqueo", SqlDbType.Float, 18).Value = DBNull.Value
                                    SQLCommand.Parameters.Add("pEstado", SqlDbType.VarChar, 3).Value = strAccion
                                    SQLCommand.Parameters.Add("pIdCliente", SqlDbType.Int, 10).Value = DR_Filas("ID_CLIENTE")
                                    SQLCommand.Parameters.Add("pIdGrupoCuenta", SqlDbType.Int, 12).Value = DBNull.Value
                                    SQLCommand.Parameters.Add("pIdCajaCuenta", SqlDbType.Int, 12).Value = DBNull.Value
                                    SQLCommand.Parameters.Add("pIdCartera", SqlDbType.Int, 12).Value = DBNull.Value
                                    SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Float, 10).Value = glngIdUsuario
                                    ''...Resultado
                                    Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                                    pSalResultado.Direction = Data.ParameterDirection.Output
                                    SQLCommand.Parameters.Add(pSalResultado)

                                    SQLCommand.ExecuteNonQuery()

                                    strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim


                                    If strDescError.ToUpper.Trim <> "OK" Then
                                        MiTransaccionSQL.Rollback()
                                        Exit For
                                    End If
                                End If
                            ElseIf strBloqueo = "POR_CAJA" Then
                                If DR_Filas("SEL") = 1 Then
                                    SQLCommand.Parameters.Add("pIdConceptoBloqueo", SqlDbType.Int, 10).Value = dblIdConceptoBloqueo
                                    SQLCommand.Parameters.Add("pIdInstrumento", SqlDbType.Int, 10).Value = DBNull.Value
                                    SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Int, 12).Value = DR_Filas("ID_CUENTA")
                                    SQLCommand.Parameters.Add("pIdPatrimonioCuenta", SqlDbType.Int, 12).Value = DBNull.Value
                                    SQLCommand.Parameters.Add("pSaldoBloqueo", SqlDbType.Float, 18).Value = DR_Filas("SALDO_FINAL")
                                    SQLCommand.Parameters.Add("pEstado", SqlDbType.VarChar, 3).Value = strAccion
                                    SQLCommand.Parameters.Add("pIdCliente", SqlDbType.Int, 10).Value = DR_Filas("ID_CLIENTE")
                                    SQLCommand.Parameters.Add("pIdGrupoCuenta", SqlDbType.Int, 12).Value = DBNull.Value
                                    SQLCommand.Parameters.Add("pIdCajaCuenta", SqlDbType.Int, 12).Value = DR_Filas("ID_CAJA_CUENTA")
                                    SQLCommand.Parameters.Add("pIdCartera", SqlDbType.Int, 12).Value = DBNull.Value
                                    SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Float, 10).Value = glngIdUsuario
                                    SQLCommand.Parameters.Add("pCodmoneda", SqlDbType.VarChar, 10).Value = DBNull.Value 'DR_Filas("COD_MONEDA")
                                    ''...Resultado
                                    Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                                    pSalResultado.Direction = Data.ParameterDirection.Output
                                    SQLCommand.Parameters.Add(pSalResultado)

                                    SQLCommand.ExecuteNonQuery()

                                    strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim


                                    If strDescError.ToUpper.Trim <> "OK" Then
                                        MiTransaccionSQL.Rollback()
                                        Exit For
                                    End If
                                End If
                            ElseIf strBloqueo = "POR_PATRIMONIO" Then
                                If DR_Filas("SEL") = 1 Then
                                    SQLCommand.Parameters.Add("pIdConceptoBloqueo", SqlDbType.Int, 10).Value = dblIdConceptoBloqueo
                                    SQLCommand.Parameters.Add("pIdInstrumento", SqlDbType.Int, 10).Value = DBNull.Value
                                    SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Int, 12).Value = DR_Filas("ID_CUENTA")
                                    SQLCommand.Parameters.Add("pIdPatrimonioCuenta", SqlDbType.Int, 12).Value = DBNull.Value
                                    SQLCommand.Parameters.Add("pSaldoBloqueo", SqlDbType.Float, 18).Value = DR_Filas("PATRIMONIO_DISPONIBLE")
                                    SQLCommand.Parameters.Add("pEstado", SqlDbType.VarChar, 3).Value = strAccion
                                    SQLCommand.Parameters.Add("pIdCliente", SqlDbType.Int, 10).Value = DR_Filas("ID_CLIENTE")
                                    SQLCommand.Parameters.Add("pIdGrupoCuenta", SqlDbType.Int, 12).Value = DR_Filas("ID_GRUPO_CUENTA")
                                    SQLCommand.Parameters.Add("pIdCajaCuenta", SqlDbType.Int, 12).Value = DBNull.Value
                                    SQLCommand.Parameters.Add("pIdCartera", SqlDbType.Int, 12).Value = DBNull.Value
                                    SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Float, 10).Value = glngIdUsuario
                                    SQLCommand.Parameters.Add("pCodmoneda", SqlDbType.VarChar, 10).Value = DR_Filas("COD_MONEDA")


                                    ''...Resultado
                                    Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                                    pSalResultado.Direction = Data.ParameterDirection.Output
                                    SQLCommand.Parameters.Add(pSalResultado)

                                    SQLCommand.ExecuteNonQuery()

                                    strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim


                                    If strDescError.ToUpper.Trim <> "OK" Then
                                        MiTransaccionSQL.Rollback()
                                        Exit For
                                    End If
                                End If
                            End If
                        Next
                    End If
                End If
            Else
                If Not (IsNothing(DS_Bloqueo)) Then
                    If DS_Bloqueo.Tables.Count > 0 And DS_Bloqueo.Tables(0).Rows.Count > 0 Then
                        For Each DR_Filas In DS_Bloqueo.Tables(0).Rows

                            SQLCommand = New SqlClient.SqlCommand("Rcp_Bloqueo_Mantenedor", MiTransaccionSQL.Connection, MiTransaccionSQL)
                            SQLCommand.CommandType = CommandType.StoredProcedure
                            SQLCommand.CommandText = "Rcp_Bloqueo_Mantenedor"
                            SQLCommand.Parameters.Clear()

                            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 20).Value = "MODIFICAR"
                            If strBloqueo = "POR_CUENTA" Then
                                If DR_Filas("SEL") = 1 Then
                                    SQLCommand.Parameters.Add("pIdBloqueo", SqlDbType.Int, 10).Value = IIf(DR_Filas("ID_BLOQUEO") = 0, DBNull.Value, DR_Filas("ID_BLOQUEO"))
                                    SQLCommand.Parameters.Add("pSaldoBloqueo", SqlDbType.Float, 10).Value = DR_Filas("SALDO_BLOQUEO")
                                    SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = DR_Filas("ID_CUENTA")
                                    SQLCommand.Parameters.Add("pIdConceptoBloqueo", SqlDbType.Float, 10).Value = DR_Filas("ID_CONCEPTO")
                                    SQLCommand.Parameters.Add("pIdInstrumento", SqlDbType.Float, 10).Value = DR_Filas("ID_INSTRUMENTO")
                                    SQLCommand.Parameters.Add("pEstado", SqlDbType.VarChar, 3).Value = strAccion
                                    SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Float, 10).Value = glngIdUsuario

                                    ''...Resultado
                                    Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                                    pSalResultado.Direction = Data.ParameterDirection.Output
                                    SQLCommand.Parameters.Add(pSalResultado)

                                    SQLCommand.ExecuteNonQuery()

                                    strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim


                                    If strDescError.ToUpper.Trim <> "OK" Then
                                        MiTransaccionSQL.Rollback()
                                        Exit For
                                    End If
                                End If

                            ElseIf strBloqueo = "POR_INSTRUMENTO" Then
                                If DR_Filas("SEL") = 1 Then
                                    SQLCommand.Parameters.Add("pIdBloqueo", SqlDbType.Int, 10).Value = DR_Filas("ID_BLOQUEO")
                                    SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Float, 10).Value = glngIdUsuario
                                    SQLCommand.Parameters.Add("pEstado", SqlDbType.VarChar, 3).Value = strAccion

                                    '...Resultado
                                    Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                                    pSalResultado.Direction = Data.ParameterDirection.Output
                                    SQLCommand.Parameters.Add(pSalResultado)

                                    SQLCommand.ExecuteNonQuery()

                                    strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

                                    If strDescError.ToUpper.Trim <> "OK" Then
                                        MiTransaccionSQL.Rollback()
                                        Exit For
                                    End If
                                End If
                            ElseIf strBloqueo = "POR_CLIENTE_CUENTA" Then
                                If DR_Filas("SEL") = 1 Then
                                    SQLCommand.Parameters.Add("pIdBloqueo", SqlDbType.Int, 10).Value = DR_Filas("ID_BLOQUEO")
                                    SQLCommand.Parameters.Add("pEstado", SqlDbType.VarChar, 3).Value = strAccion
                                    SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Float, 10).Value = glngIdUsuario

                                    '...Resultado
                                    Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                                    pSalResultado.Direction = Data.ParameterDirection.Output
                                    SQLCommand.Parameters.Add(pSalResultado)

                                    SQLCommand.ExecuteNonQuery()

                                    strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

                                    If strDescError.ToUpper.Trim <> "OK" Then
                                        MiTransaccionSQL.Rollback()
                                        Exit For
                                    End If
                                End If
                            ElseIf strBloqueo = "POR_CAJA" Then
                                If DR_Filas("SEL") = 1 Then
                                    SQLCommand.Parameters.Add("pIdBloqueo", SqlDbType.Int, 10).Value = DR_Filas("ID_BLOQUEO")
                                    SQLCommand.Parameters.Add("pEstado", SqlDbType.VarChar, 3).Value = strAccion
                                    SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Float, 10).Value = glngIdUsuario

                                    '...Resultado
                                    Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                                    pSalResultado.Direction = Data.ParameterDirection.Output
                                    SQLCommand.Parameters.Add(pSalResultado)

                                    SQLCommand.ExecuteNonQuery()

                                    strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

                                    If strDescError.ToUpper.Trim <> "OK" Then
                                        MiTransaccionSQL.Rollback()
                                        Exit For
                                    End If
                                End If
                            ElseIf strBloqueo = "POR_PATRIMONIO" Then
                                If DR_Filas("SEL") = 1 Then
                                    SQLCommand.Parameters.Add("pIdBloqueo", SqlDbType.Int, 10).Value = DR_Filas("ID_BLOQUEO")
                                    SQLCommand.Parameters.Add("pEstado", SqlDbType.VarChar, 3).Value = strAccion
                                    SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Float, 10).Value = glngIdUsuario

                                    '...Resultado
                                    Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                                    pSalResultado.Direction = Data.ParameterDirection.Output
                                    SQLCommand.Parameters.Add(pSalResultado)

                                    SQLCommand.ExecuteNonQuery()

                                    strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

                                    If strDescError.ToUpper.Trim <> "OK" Then
                                        MiTransaccionSQL.Rollback()
                                        Exit For
                                    End If
                                End If
                            End If
                        Next

                    End If
                End If
            End If

            ' -------------
            If Trim(strDescError) = "OK" Then
                MiTransaccionSQL.Commit()
                Return ("OK")
            Else
                If Not (MiTransaccionSQL.Connection Is Nothing) Then MiTransaccionSQL.Rollback()
                Return strDescError
            End If
        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error: " & vbCr & Ex.Message

        Finally
            SQLConnect.Cerrar()
            Bloqueo_Mantenedor = strDescError
        End Try


    End Function
    Public Function BloqueosInstrumento_Consultar(ByVal StrEstado As String, _
                                                  ByVal intIdInstrumento As Integer, _
                                                  ByVal intIdCuenta As Integer, _
                                                  ByVal intIdConceptoBloqueo As Integer, _
                                                  ByVal strCodClaseInstrumento As String, _
                                                  ByVal strCodSubClaseInstrumento As String, _
                                                  ByVal intIdEmisor As Integer, _
                                                  ByVal strColumnas As String, _
                                                  ByRef strRetorno As String, _
                                                  Optional ByVal strDscTipoBloqueo As String = "") As DataSet

        Dim LDS_ConceptoBloq As New DataSet
        Dim LDS_ConceptoBloq_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_BloqueosInstrumentos_Consultar"
        Lstr_NombreTabla = "Bloqueos_Instrumento"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pEstado", SqlDbType.VarChar, 50).Value = IIf(StrEstado.Trim = "", DBNull.Value, StrEstado.Trim)
            SQLCommand.Parameters.Add("pIdInstrumento", SqlDbType.Float, 12).Value = IIf(intIdInstrumento = 0, DBNull.Value, intIdInstrumento)
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 50).Value = IIf(intIdCuenta = 0, DBNull.Value, intIdCuenta)
            SQLCommand.Parameters.Add("pIdConceptoBloqueo", SqlDbType.Float, 10).Value = IIf(intIdConceptoBloqueo = 0, DBNull.Value, intIdConceptoBloqueo)
            SQLCommand.Parameters.Add("pCodClaseInstrumento", SqlDbType.VarChar, 50).Value = IIf(strCodClaseInstrumento.Trim = "", DBNull.Value, strCodClaseInstrumento.Trim)
            SQLCommand.Parameters.Add("pCodSubClaseInstrumento", SqlDbType.VarChar, 50).Value = IIf(strCodSubClaseInstrumento.Trim = "", DBNull.Value, strCodSubClaseInstrumento.Trim)
            SQLCommand.Parameters.Add("pIdEmisor", SqlDbType.Float, 10).Value = IIf(intIdEmisor = 0, DBNull.Value, intIdEmisor)
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Float, 10).Value = IIf(glngNegocioOperacion = 0, DBNull.Value, glngNegocioOperacion)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_ConceptoBloq, Lstr_NombreTabla)

            LDS_ConceptoBloq_Aux = LDS_ConceptoBloq

            If strColumnas.Trim = "" Then
                LDS_ConceptoBloq_Aux = LDS_ConceptoBloq
            Else
                LDS_ConceptoBloq_Aux = LDS_ConceptoBloq.Copy
                For Each Columna In LDS_ConceptoBloq.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_ConceptoBloq_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_ConceptoBloq_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_ConceptoBloq.Dispose()
            Return LDS_ConceptoBloq_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function BloqueosCuentaActivo_Consultar(ByVal StrEstado As String, _
                                                  ByVal intIdInstrumento As Integer, _
                                                  ByVal strIdCuenta As String, _
                                                  ByVal intIdConceptoBloqueo As Integer, _
                                                  ByVal strCodClaseInstrumento As String, _
                                                  ByVal strCodSubClaseInstrumento As String, _
                                                  ByVal intIdEmisor As Integer, _
                                                  ByVal strColumnas As String, _
                                                  ByRef strRetorno As String, _
                                                  Optional ByVal strDscTipoBloqueo As String = "") As DataSet

        Dim LDS_ConceptoBloq As New DataSet
        Dim LDS_ConceptoBloq_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_BloqueosCuentaActivos_Consultar"
        Lstr_NombreTabla = "Bloqueos_Cuentas"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pEstado", SqlDbType.VarChar, 50).Value = IIf(StrEstado.Trim = "", DBNull.Value, StrEstado.Trim)
            SQLCommand.Parameters.Add("pIdInstrumento", SqlDbType.Float, 12).Value = IIf(intIdInstrumento = 0, DBNull.Value, intIdInstrumento)
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.VarChar, 50).Value = IIf(strIdCuenta.Trim = "", DBNull.Value, strIdCuenta.Trim)
            SQLCommand.Parameters.Add("pIdConceptoBloqueo", SqlDbType.Float, 10).Value = IIf(intIdConceptoBloqueo = 0, DBNull.Value, intIdConceptoBloqueo)
            SQLCommand.Parameters.Add("pCodClaseInstrumento", SqlDbType.VarChar, 50).Value = IIf(strCodClaseInstrumento.Trim = "", DBNull.Value, strCodClaseInstrumento.Trim)
            SQLCommand.Parameters.Add("pCodSubClaseInstrumento", SqlDbType.VarChar, 50).Value = IIf(strCodSubClaseInstrumento.Trim = "", DBNull.Value, strCodSubClaseInstrumento.Trim)
            SQLCommand.Parameters.Add("pIdEmisor", SqlDbType.Float, 10).Value = IIf(intIdEmisor = 0, DBNull.Value, intIdEmisor)
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Float, 10).Value = IIf(glngNegocioOperacion = 0, DBNull.Value, glngNegocioOperacion)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_ConceptoBloq, Lstr_NombreTabla)

            LDS_ConceptoBloq_Aux = LDS_ConceptoBloq

            If strColumnas.Trim = "" Then
                LDS_ConceptoBloq_Aux = LDS_ConceptoBloq
            Else
                LDS_ConceptoBloq_Aux = LDS_ConceptoBloq.Copy
                For Each Columna In LDS_ConceptoBloq.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_ConceptoBloq_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_ConceptoBloq_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_ConceptoBloq.Dispose()
            Return LDS_ConceptoBloq_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function BloqueosClienteCuenta_Consultar(ByVal StrEstado As String, _
                                                  ByVal intIdCuenta As Integer, _
                                                  ByVal intIdConceptoBloqueo As Integer, _
                                                  ByVal intIdCliente As Integer, _
                                                  ByVal strColumnas As String, _
                                                  ByRef strRetorno As String, _
                                                  Optional ByVal strDscTipoBloqueo As String = "") As DataSet

        Dim LDS_ConceptoBloq As New DataSet
        Dim LDS_ConceptoBloq_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_BloqueosClienteCuenta_Consultar"
        Lstr_NombreTabla = "Bloqueos_CliCta"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pEstado", SqlDbType.VarChar, 50).Value = IIf(StrEstado.Trim = "", DBNull.Value, StrEstado.Trim)
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = IIf(intIdCuenta = 0, DBNull.Value, intIdCuenta)
            SQLCommand.Parameters.Add("pIdConceptoBloqueo", SqlDbType.Float, 10).Value = IIf(intIdConceptoBloqueo = 0, DBNull.Value, intIdConceptoBloqueo)
            SQLCommand.Parameters.Add("pIdCliente ", SqlDbType.Float, 10).Value = IIf(intIdCliente = 0, DBNull.Value, intIdCliente)
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Float, 10).Value = IIf(glngNegocioOperacion = 0, DBNull.Value, glngNegocioOperacion)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_ConceptoBloq, Lstr_NombreTabla)

            LDS_ConceptoBloq_Aux = LDS_ConceptoBloq

            If strColumnas.Trim = "" Then
                LDS_ConceptoBloq_Aux = LDS_ConceptoBloq
            Else
                LDS_ConceptoBloq_Aux = LDS_ConceptoBloq.Copy
                For Each Columna In LDS_ConceptoBloq.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_ConceptoBloq_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_ConceptoBloq_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_ConceptoBloq.Dispose()
            Return LDS_ConceptoBloq_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function BloqueosCajaCuenta_Consultar(ByVal StrEstado As String, _
                                                ByVal intIdCuenta As Integer, _
                                                ByVal intIdConceptoBloqueo As Integer, _
                                                ByVal intIdCliente As Integer, _
                                                ByVal strCodMoneda As String, _
                                                ByVal strColumnas As String, _
                                                ByRef strRetorno As String, _
                                                Optional ByVal strDscTipoBloqueo As String = "") As DataSet

        Dim LDS_ConceptoBloq As New DataSet
        Dim LDS_ConceptoBloq_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_BloqueosCajaCuenta_Consultar"
        Lstr_NombreTabla = "Bloqueos_CajaCta"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pEstado", SqlDbType.VarChar, 50).Value = IIf(StrEstado.Trim = "", DBNull.Value, StrEstado.Trim)
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = IIf(intIdCuenta = 0, DBNull.Value, intIdCuenta)
            SQLCommand.Parameters.Add("pIdConceptoBloqueo", SqlDbType.Float, 10).Value = IIf(intIdConceptoBloqueo = 0, DBNull.Value, intIdConceptoBloqueo)
            SQLCommand.Parameters.Add("pIdCliente ", SqlDbType.Float, 10).Value = IIf(intIdCliente = 0, DBNull.Value, intIdCliente)
            SQLCommand.Parameters.Add("pCodMoneda ", SqlDbType.VarChar, 10).Value = IIf(strCodMoneda.Trim = "", DBNull.Value, strCodMoneda.Trim)
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Float, 10).Value = IIf(glngNegocioOperacion = 0, DBNull.Value, glngNegocioOperacion)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_ConceptoBloq, Lstr_NombreTabla)

            LDS_ConceptoBloq_Aux = LDS_ConceptoBloq

            If strColumnas.Trim = "" Then
                LDS_ConceptoBloq_Aux = LDS_ConceptoBloq
            Else
                LDS_ConceptoBloq_Aux = LDS_ConceptoBloq.Copy
                For Each Columna In LDS_ConceptoBloq.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_ConceptoBloq_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_ConceptoBloq_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_ConceptoBloq.Dispose()
            Return LDS_ConceptoBloq_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function BloqueosPatrimonio_Consultar(ByVal StrEstado As String, _
                                                 ByVal intIdCuenta As Integer, _
                                                 ByVal intIdConceptoBloqueo As Integer, _
                                                 ByVal intIdCliente As Integer, _
                                                 ByVal strCodMoneda As String, _
                                                 ByVal strColumnas As String, _
                                                 ByRef strRetorno As String, _
                                                 ByVal intdGrupoCuentas As Integer, _
                                                Optional ByVal strDscTipoBloqueo As String = "") As DataSet

        Dim LDS_ConceptoBloq As New DataSet
        Dim LDS_ConceptoBloq_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_BloqueosPatrimonio_Consultar"
        Lstr_NombreTabla = "Bloq_Patrimonio"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pEstado", SqlDbType.VarChar, 50).Value = IIf(StrEstado.Trim = "", DBNull.Value, StrEstado.Trim)
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = IIf(intIdCuenta = 0, DBNull.Value, intIdCuenta)
            SQLCommand.Parameters.Add("pIdConceptoBloqueo", SqlDbType.Float, 10).Value = IIf(intIdConceptoBloqueo = 0, DBNull.Value, intIdConceptoBloqueo)
            SQLCommand.Parameters.Add("pIdCliente ", SqlDbType.Float, 10).Value = IIf(intIdCliente = 0, DBNull.Value, intIdCliente)
            SQLCommand.Parameters.Add("pCodMoneda ", SqlDbType.VarChar, 10).Value = IIf(strCodMoneda.Trim = "", DBNull.Value, strCodMoneda.Trim)
            SQLCommand.Parameters.Add("pIdGrupoCuenta ", SqlDbType.Float, 10).Value = IIf(intdGrupoCuentas = 0, DBNull.Value, intdGrupoCuentas)
            SQLCommand.Parameters.Add("pFiltro ", SqlDbType.VarChar, 50).Value = IIf(strDscTipoBloqueo.Trim = "", DBNull.Value, strDscTipoBloqueo.Trim)
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Float, 10).Value = IIf(glngNegocioOperacion = 0, DBNull.Value, glngNegocioOperacion)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_ConceptoBloq, Lstr_NombreTabla)

            LDS_ConceptoBloq_Aux = LDS_ConceptoBloq

            If strColumnas.Trim = "" Then
                LDS_ConceptoBloq_Aux = LDS_ConceptoBloq
            Else
                LDS_ConceptoBloq_Aux = LDS_ConceptoBloq.Copy
                For Each Columna In LDS_ConceptoBloq.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_ConceptoBloq_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_ConceptoBloq_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_ConceptoBloq.Dispose()
            Return LDS_ConceptoBloq_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function
    Public Function CuentasDesbloqueadas(ByVal strNegocio As String, _
                                      ByVal strNombreCuenta As String, _
                                      ByVal strNumeroCuenta As String, _
                                      ByVal strRutCliente As String, _
                                      ByVal strNombreCliente As String, _
                                      ByVal dblIdCliente As Integer, _
                                      ByVal strColumnas As String, _
                                      ByRef strRetorno As String, _
                                      Optional ByVal dblIdAsesor As Double = 0, _
                                      Optional ByVal strcodMercado As String = "") As DataTable

        Dim ldtCuentas As New DataTable
        Dim LstrProcedimiento = "RCP_CUENTAS_DESBLOQUEDAS_BUSCARDOR"
        Dim strDescError As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Int).Value = glngNegocioOperacion
            SQLCommand.Parameters.Add("pNombreCuenta", SqlDbType.VarChar, 50).Value = IIf(strNombreCuenta = "", DBNull.Value, strNombreCuenta)
            SQLCommand.Parameters.Add("pNumeroCuenta", SqlDbType.VarChar, 15).Value = IIf(strNumeroCuenta = "", DBNull.Value, strNumeroCuenta)
            SQLCommand.Parameters.Add("pNombreCliente", SqlDbType.VarChar, 50).Value = IIf(strNombreCliente = "", DBNull.Value, strNombreCliente)
            SQLCommand.Parameters.Add("pRutCliente", SqlDbType.VarChar, 12).Value = IIf(strRutCliente = "", DBNull.Value, strRutCliente)
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Float, 6).Value = IIf(glngIdUsuario = 0, DBNull.Value, glngIdUsuario)
            SQLCommand.Parameters.Add("pIdCliente", SqlDbType.Float, 6).Value = IIf(dblIdCliente = 0, DBNull.Value, dblIdCliente)
            SQLCommand.Parameters.Add("pIdAsesor", SqlDbType.Float, 6).Value = IIf(dblIdAsesor = 0, DBNull.Value, dblIdAsesor)
            SQLCommand.Parameters.Add("@pCodMercado", SqlDbType.VarChar, 1).Value = IIf(strcodMercado = "", DBNull.Value, strcodMercado)

            '...Resultado
            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()
            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(ldtCuentas)

            If strDescError.ToUpper.Trim <> "OK" Then
                gFun_ResultadoMsg("2|" & strDescError, "Buscar Cuentas")
                ldtCuentas = Nothing
            Else
                Call gsubConfiguraDataTable(ldtCuentas, strColumnas)
                strRetorno = "OK"
            End If
        Catch ex As Exception
            strRetorno = ex.Message
            ldtCuentas = Nothing
        Finally
            Connect.Cerrar()
        End Try

        Return ldtCuentas
    End Function
    Public Function CajaCuentas_Desbloqueadas(ByVal intIdCuenta As Integer, _
                                              ByVal intIdCliente As Integer, _
                                              ByVal intIdNegocio As Integer, _
                                              ByVal strCodMoneda As String, _
                                              ByVal strColumnas As String, _
                                              ByRef strRetorno As String) As DataSet


        Dim LDS_ConceptoBloq As New DataSet
        Dim LDS_ConceptoBloq_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_CajaCuenta_DesbloqueoBuscador"
        Lstr_NombreTabla = "Bloq_CajaCta"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()


            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 50).Value = IIf(intIdCuenta = 0, DBNull.Value, intIdCuenta)
            SQLCommand.Parameters.Add("pIdCliente ", SqlDbType.Float, 10).Value = IIf(intIdCliente = 0, DBNull.Value, intIdCliente)
            SQLCommand.Parameters.Add("pCodMoneda", SqlDbType.VarChar, 10).Value = IIf(strCodMoneda.Trim = "", DBNull.Value, strCodMoneda.Trim)
            SQLCommand.Parameters.Add("@pIdNegocio", SqlDbType.Int, 10).Value = intIdNegocio
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Float, 6).Value = IIf(glngIdUsuario = 0, DBNull.Value, glngIdUsuario)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_ConceptoBloq, Lstr_NombreTabla)

            LDS_ConceptoBloq_Aux = LDS_ConceptoBloq

            If strColumnas.Trim = "" Then
                LDS_ConceptoBloq_Aux = LDS_ConceptoBloq
            Else
                LDS_ConceptoBloq_Aux = LDS_ConceptoBloq.Copy
                For Each Columna In LDS_ConceptoBloq.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_ConceptoBloq_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_ConceptoBloq_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_ConceptoBloq.Dispose()
            Return LDS_ConceptoBloq_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function Patrimonio_Desbloqueados(ByVal intIdCuenta As Integer, _
                                             ByVal intIdCliente As Integer, _
                                             ByVal intdGrupoCuentas As Integer, _
                                             ByVal strCodMoneda As String, _
                                             ByVal strVariable As String, _
                                             ByVal strColumnas As String, _
                                             ByRef strRetorno As String) As DataSet


        Dim LDS_ConceptoBloq As New DataSet
        Dim LDS_ConceptoBloq_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Patrimonio_DesbloqueoBuscador"
        Lstr_NombreTabla = "Patrimonio"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)
            SQLCommand.CommandTimeout = 0
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()


            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = IIf(intIdCuenta = 0, DBNull.Value, intIdCuenta)
            SQLCommand.Parameters.Add("pIdCliente ", SqlDbType.Float, 10).Value = IIf(intIdCliente = 0, DBNull.Value, intIdCliente)
            SQLCommand.Parameters.Add("pIdGrupoCuentas ", SqlDbType.Float, 10).Value = IIf(intdGrupoCuentas = 0, DBNull.Value, intdGrupoCuentas)
            SQLCommand.Parameters.Add("pCodMoneda", SqlDbType.VarChar, 10).Value = IIf(strCodMoneda.Trim = "", DBNull.Value, strCodMoneda.Trim)
            SQLCommand.Parameters.Add("pVariableDatos", SqlDbType.VarChar, 50).Value = IIf(strVariable.Trim = "", DBNull.Value, strVariable.Trim)
            SQLCommand.Parameters.Add("pIdNegocio ", SqlDbType.Int).Value = glngNegocioOperacion

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_ConceptoBloq, Lstr_NombreTabla)

            LDS_ConceptoBloq_Aux = LDS_ConceptoBloq

            If strColumnas.Trim = "" Then
                LDS_ConceptoBloq_Aux = LDS_ConceptoBloq
            Else
                LDS_ConceptoBloq_Aux = LDS_ConceptoBloq.Copy
                For Each Columna In LDS_ConceptoBloq.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_ConceptoBloq_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_ConceptoBloq_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_ConceptoBloq.Dispose()
            Return LDS_ConceptoBloq_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function
    Public Function TraerInstrumentosNoBloqueados(ByVal strCodClaseInstrumento As String, _
                                              ByVal strCodSubClaseInstrumento As String, _
                                              ByVal dblIdInstrumento As Double, _
                                              ByVal strCodEstado As String, _
                                              ByVal strNemotecnico As String, _
                                              ByVal strEmisor As String, _
                                              ByVal strCodMoneda As String, _
                                              ByVal lngIdPerfilRiesgo As Long, _
                                              ByVal strColumnas As String, _
                                              ByRef strRetorno As String, _
                                              Optional ByVal dblIdCuenta As Double = 0, _
                                              Optional ByVal strCodExternoInstrumento As String = "", _
                                              Optional ByVal strCodNormaInstrumento As String = "", _
                                              Optional ByVal strOpcionDividendo As String = "") As DataSet

        Dim LDS_Nemotecnico As New DataSet
        Dim LDS_Nemotecnico_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Instrumento_BuscarBloqueados"
        Lstr_NombreTabla = "NEMOS"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)
            SQLCommand.CommandTimeout = 0

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pCodClaseInstrumento", SqlDbType.VarChar, 15).Value = IIf(strCodClaseInstrumento = "", DBNull.Value, strCodClaseInstrumento)
            SQLCommand.Parameters.Add("pCodSubClaseInstrumento", SqlDbType.VarChar, 10).Value = IIf(strCodSubClaseInstrumento = "", DBNull.Value, strCodSubClaseInstrumento)
            SQLCommand.Parameters.Add("pIdInstrumento", SqlDbType.Float, 10).Value = IIf(dblIdInstrumento = 0, DBNull.Value, dblIdInstrumento)
            SQLCommand.Parameters.Add("pCodEstado", SqlDbType.VarChar, 3).Value = IIf(strCodEstado = "", DBNull.Value, strCodEstado)
            SQLCommand.Parameters.Add("pNemotecnico", SqlDbType.VarChar, 100).Value = IIf(strNemotecnico = "", DBNull.Value, strNemotecnico)
            SQLCommand.Parameters.Add("pDscEmisor", SqlDbType.VarChar, 100).Value = IIf(strEmisor = "", DBNull.Value, strEmisor)
            SQLCommand.Parameters.Add("pCodMoneda", SqlDbType.VarChar, 3).Value = IIf(strCodMoneda = "", DBNull.Value, strCodMoneda)
            SQLCommand.Parameters.Add("pIdPerfilRiesgo", SqlDbType.Float, 3).Value = IIf(lngIdPerfilRiesgo = 0, DBNull.Value, lngIdPerfilRiesgo)
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = dblIdCuenta
            SQLCommand.Parameters.Add("pCodExternoInstrumento", SqlDbType.VarChar, 15).Value = IIf(strCodExternoInstrumento = "", DBNull.Value, strCodExternoInstrumento)
            SQLCommand.Parameters.Add("pCodNormaInstrumento", SqlDbType.VarChar, 10).Value = IIf(strCodNormaInstrumento = "", DBNull.Value, strCodNormaInstrumento)
            SQLCommand.Parameters.Add("pOpcionDividendo", SqlDbType.VarChar, 10).Value = strOpcionDividendo

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Nemotecnico, Lstr_NombreTabla)

            LDS_Nemotecnico_Aux = LDS_Nemotecnico

            If strColumnas.Trim = "" Then
                LDS_Nemotecnico_Aux = LDS_Nemotecnico
            Else
                LDS_Nemotecnico_Aux = LDS_Nemotecnico.Copy
                For Each Columna In LDS_Nemotecnico.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Nemotecnico_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Nemotecnico_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            Return LDS_Nemotecnico_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function TraerCarteraBloqueoActivos(ByVal strClaseInstrumento As String, _
                                              ByVal strSubClaseInstrumento As String, _
                                              ByVal strIdCuenta As String, _
                                              ByVal strFechaConsulta As String, _
                                              ByVal lintIdEmisor As Integer, _
                                              ByVal lintIdInstrumento As Integer, _
                                              ByVal strColumnas As String, _
                                              ByRef strRetorno As String) As DataSet

        Dim LDS_Cartera As New DataSet
        Dim LDS_Cartera_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Cartera_BuscarBloqueoActivos"
        Lstr_NombreTabla = "CARTERA"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)
            SQLCommand.CommandTimeout = 0

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pCodClaseInstrumento", SqlDbType.VarChar, 15).Value = IIf(strClaseInstrumento = "", DBNull.Value, strClaseInstrumento)
            SQLCommand.Parameters.Add("pCodSubClaseInstrumento", SqlDbType.VarChar, 10).Value = IIf(strSubClaseInstrumento = "", DBNull.Value, strSubClaseInstrumento)
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = IIf(strIdCuenta = "", DBNull.Value, CLng(0 & strIdCuenta))
            SQLCommand.Parameters.Add("pFechaConsulta", SqlDbType.Char, 10).Value = IIf(strFechaConsulta = "", DBNull.Value, gstrFechaSistema)
            SQLCommand.Parameters.Add("pIdEmisor", SqlDbType.Float, 10).Value = IIf(lintIdEmisor = 0, DBNull.Value, lintIdEmisor)
            SQLCommand.Parameters.Add("pIdInstrumento", SqlDbType.Float, 10).Value = IIf(lintIdInstrumento = 0, DBNull.Value, lintIdInstrumento)
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Float, 10).Value = IIf(glngNegocioOperacion = 0, DBNull.Value, glngNegocioOperacion)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Cartera, Lstr_NombreTabla)

            LDS_Cartera_Aux = LDS_Cartera


            If strColumnas.Trim = "" Then
                LDS_Cartera_Aux = LDS_Cartera
            Else
                LDS_Cartera_Aux = LDS_Cartera.Copy
                For Each Columna In LDS_Cartera.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Cartera_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Cartera_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            Return LDS_Cartera_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function
    Public Function Carga_Grilla_Bitacora(ByVal strFechadesde As String, _
                                        ByVal strFechahasta As String, _
                                        ByVal dblConcepto As Double, _
                                        ByVal dblTipoBloqueo As Double, _
                                        ByVal strColumnas As String, _
                                        ByRef strRetorno As String) As DataSet
        'Devuelve los datos a la Grilla

        Dim LDS_ImportaD As New DataSet
        Dim LDS_ImportaD_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Reporte_Bitacora"
        Lstr_NombreTabla = "Bitacora"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)
            SQLCommand.CommandTimeout = 0

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("pFechadesde", SqlDbType.Char, 10).Value = strFechadesde
            SQLCommand.Parameters.Add("pFechahasta", SqlDbType.Char, 10).Value = strFechahasta
            SQLCommand.Parameters.Add("pIdConcepto", SqlDbType.Float, 10).Value = IIf(dblConcepto = 0, DBNull.Value, dblConcepto)
            SQLCommand.Parameters.Add("pIdbloqueo", SqlDbType.Float, 10).Value = IIf(dblTipoBloqueo = 0, DBNull.Value, dblTipoBloqueo)
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Float, 10).Value = IIf(glngNegocioOperacion = 0, DBNull.Value, glngNegocioOperacion)


            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_ImportaD, Lstr_NombreTabla)

            LDS_ImportaD_Aux = LDS_ImportaD

            If strColumnas.Trim = "" Then
                LDS_ImportaD_Aux = LDS_ImportaD
            Else
                LDS_ImportaD_Aux = LDS_ImportaD.Copy
                For Each Columna In LDS_ImportaD.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_ImportaD_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_ImportaD_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_ImportaD.Dispose()
            Return LDS_ImportaD_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function ValidarBloqueoSaldos( _
                                ByVal strTipoOperacion As String, _
                                ByVal intIdCuenta As Integer, _
                                ByVal intIdcajaCuenta As Integer, _
                                ByVal dblIdInstrumento As Double, _
                                ByVal strCodMoneda As String, _
                                ByVal strCodMercado As String, _
                                ByVal dblMonto As Double, _
                                ByVal dblNominales As Double, _
                                ByVal strFecha As String, _
                                ByVal strFormaOperarCartera As String, _
                                ByVal dblIdCartera As Double, _
                                ByRef strRetorno As String, _
                                Optional ByVal strFlgFlujoPatrimonial As String = "N", _
                                Optional ByRef strRetorno2 As String = "") As DataSet


        Dim LDS_MovimientosCaja As New DataSet
        Dim LDS_MovimientosCaja_Aux As New DataSet
        Dim Lstr_NombreTabla As String
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Bloqueos_ValidarSaldos"
        Lstr_NombreTabla = "validaIngreso"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)
            SQLCommand.CommandTimeout = 0

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pTipoOperacion", SqlDbType.VarChar, 20).Value = strTipoOperacion
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Int).Value = IIf(intIdCuenta = 0, DBNull.Value, intIdCuenta)
            SQLCommand.Parameters.Add("pIdCajaCuenta", SqlDbType.Int).Value = IIf(intIdcajaCuenta = 0, DBNull.Value, intIdcajaCuenta)
            SQLCommand.Parameters.Add("pIdInstrumento", SqlDbType.Float, 10).Value = IIf(dblIdInstrumento = 0, DBNull.Value, dblIdInstrumento)
            SQLCommand.Parameters.Add("pCodMoneda", SqlDbType.VarChar, 3).Value = IIf(strCodMoneda = "", DBNull.Value, strCodMoneda)
            SQLCommand.Parameters.Add("pCodMercado", SqlDbType.VarChar, 4).Value = IIf(strCodMercado = "", DBNull.Value, strCodMercado)
            SQLCommand.Parameters.Add("pMonto", SqlDbType.Float).Value = IIf(dblMonto = 0, DBNull.Value, dblMonto)
            SQLCommand.Parameters.Add("pNominales", SqlDbType.Float).Value = IIf(dblNominales = 0, DBNull.Value, dblNominales)
            SQLCommand.Parameters.Add("pFecha", SqlDbType.VarChar, 10).Value = IIf(strFecha = "", DBNull.Value, strFecha)
            SQLCommand.Parameters.Add("pFormaOperarCartera", SqlDbType.VarChar, 1).Value = strFormaOperarCartera
            SQLCommand.Parameters.Add("pIdCartera", SqlDbType.Int).Value = IIf(dblIdCartera = 0, DBNull.Value, dblIdCartera)
            SQLCommand.Parameters.Add("pFlgFlujoPatrimonial", SqlDbType.VarChar, 1).Value = strFlgFlujoPatrimonial
            SQLCommand.Parameters.Add("pIdNegocio ", SqlDbType.Int).Value = glngNegocioOperacion

            '...Resultado
            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            Dim ParametroSal3 As New SqlClient.SqlParameter("pTipoCustodia", SqlDbType.VarChar, 20)
            ParametroSal3.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal3)


            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_MovimientosCaja, Lstr_NombreTabla)

            strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            strRetorno2 = SQLCommand.Parameters("pTipoCustodia").Value.ToString.Trim
            Return LDS_MovimientosCaja

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

End Class
