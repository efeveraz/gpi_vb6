﻿Public Class ClsBanco

    Public Function Banco_Ver(ByVal strCodigo As String, _
                              ByVal strEstado As String, _
                              ByVal strCodPais As String, _
                              ByVal strCodMercado As String, _
                              ByVal strColumnas As String, _
                              ByRef strRetorno As String) As DataSet

        Dim LDS_Banco As New DataSet
        Dim LDS_Banco_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Banco_Consultar"
        Lstr_NombreTabla = "Banco"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdBanco", SqlDbType.VarChar, 3).Value = IIf(strCodigo.Trim = "", DBNull.Value, strCodigo.Trim)
            SQLCommand.Parameters.Add("pEstado", SqlDbType.VarChar, 3).Value = IIf(strEstado.Trim = "", DBNull.Value, strEstado.Trim)
            SQLCommand.Parameters.Add("pCodPais", SqlDbType.VarChar, 3).Value = IIf(strCodPais.Trim = "", DBNull.Value, strCodPais.Trim)
            SQLCommand.Parameters.Add("pCodMercado", SqlDbType.VarChar, 4).Value = IIf(strCodMercado.Trim = "", DBNull.Value, strCodMercado.Trim)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Banco, Lstr_NombreTabla)

            LDS_Banco_Aux = LDS_Banco

            If strColumnas.Trim = "" Then
                LDS_Banco_Aux = LDS_Banco
            Else
                LDS_Banco_Aux = LDS_Banco.Copy
                For Each Columna In LDS_Banco.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Banco_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Banco_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_Banco.Dispose()
            Return LDS_Banco_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function Banco_Mantenedor(ByVal strAccion As String, _
                              ByVal strCodPais As String, _
                              ByVal strCodMercado As String, _
                              ByVal strDescBanco As String, _
                              ByVal strEstBanco As String, _
                              ByVal intIdBanco As Integer) As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_Banco_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Banco_Mantencion"
            SQLCommand.Parameters.Clear()

            '@pAccion varchar(10),
            '@pCod_Pais varchar(3),
            '@pCod_Mercado char(4),
            '@pDsc_Banco varchar(60),
            '@pEstado char(3),
            '@pIdBanco int,
            '@pResultado varchar(200) OUTPUT)

            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 10).Value = strAccion
            SQLCommand.Parameters.Add("pCodPais", SqlDbType.VarChar, 3).Value = strCodPais
            SQLCommand.Parameters.Add("pCodMercado", SqlDbType.VarChar, 4).Value = strCodMercado
            SQLCommand.Parameters.Add("pDscBanco", SqlDbType.VarChar, 60).Value = strDescBanco
            SQLCommand.Parameters.Add("pEstado", SqlDbType.VarChar, 3).Value = strEstBanco
            SQLCommand.Parameters.Add("pIdBanco", SqlDbType.Float, 3).Value = intIdBanco

            '...Resultado
            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            Banco_Mantenedor = strDescError
        End Try
    End Function

    Public Function BuscarRelacionConBancoPorCliente(ByVal strIdCliente As String, _
                                                      ByVal strColumnas As String, _
                                                      ByRef strRetorno As String) As DataSet

        Dim LDS_Banco As New DataSet
        Dim LDS_Banco_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_RelClienteConBanco_BuscarPorCte"
        Lstr_NombreTabla = "RelacionConBanco"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdCliente", SqlDbType.VarChar, 3).Value = IIf(strIdCliente.Trim = "", DBNull.Value, strIdCliente.Trim)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Banco, Lstr_NombreTabla)

            LDS_Banco_Aux = LDS_Banco

            If strColumnas.Trim = "" Then
                LDS_Banco_Aux = LDS_Banco
            Else
                LDS_Banco_Aux = LDS_Banco.Copy
                For Each Columna In LDS_Banco.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Banco_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Banco_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_Banco.Dispose()
            Return LDS_Banco_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function RelClienteConBanco_Mantenedor(ByVal strId_Cliente As String, ByRef dstRelClienteConBanco As DataSet) As String

        Dim strDescError As String = "OK"
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try

            '       ELIMINAMOS LAS RELCLIENTECONBANCO YA EXISTENTES

            SQLCommand = New SqlClient.SqlCommand("Rcp_RelClienteConBanco_MantencionPorCliente", MiTransaccionSQL.Connection, MiTransaccionSQL)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_RelClienteConBanco_MantencionPorCliente"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = "ELIMINAR"
            SQLCommand.Parameters.Add("pIdRelClienteConBanco", SqlDbType.Float, 6).Value = DBNull.Value
            SQLCommand.Parameters.Add("pIdCliente", SqlDbType.Float, 10).Value = strId_Cliente
            SQLCommand.Parameters.Add("pObservacion", SqlDbType.VarChar, 100).Value = DBNull.Value
            '...Resultado
            Dim pSalElimIntPort As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalElimIntPort.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalElimIntPort)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            If Not strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Rollback()
                Exit Function
            End If

            '       INSERTAMOS LOS NUEVOS RELCLIENTECONBANCO X CUENTA 

            If dstRelClienteConBanco.Tables(0).Rows.Count <> Nothing Then
                For Each pRow As DataRow In dstRelClienteConBanco.Tables(0).Rows
                    If (pRow("INCLUIDO").ToString = "1") Then
                        SQLCommand = New SqlClient.SqlCommand("Rcp_RelClienteConBanco_MantencionPorCliente", MiTransaccionSQL.Connection, MiTransaccionSQL)
                        SQLCommand.CommandType = CommandType.StoredProcedure
                        SQLCommand.CommandText = "Rcp_RelClienteConBanco_MantencionPorCliente"
                        SQLCommand.Parameters.Clear()

                        SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = "INSERTAR"
                        SQLCommand.Parameters.Add("pIdRelClienteConBanco", SqlDbType.Float, 6).Value = pRow("ID_RELACION_CLIENTE_CON_BANCO").ToString
                        SQLCommand.Parameters.Add("pObservacion", SqlDbType.VarChar, 100).Value = pRow("OBS_REL_CLIENTE_BANCO").ToString.ToUpper
                        SQLCommand.Parameters.Add("pIdCliente", SqlDbType.Float, 10).Value = strId_Cliente

                        '...Resultado
                        Dim pSalInstPort As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                        pSalInstPort.Direction = Data.ParameterDirection.Output
                        SQLCommand.Parameters.Add(pSalInstPort)

                        SQLCommand.ExecuteNonQuery()

                        strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
                    End If
                    If Not strDescError.ToUpper.Trim = "OK" Then
                        Exit For
                    End If
                Next
            End If

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            RelClienteConBanco_Mantenedor = strDescError
        End Try
    End Function

End Class

