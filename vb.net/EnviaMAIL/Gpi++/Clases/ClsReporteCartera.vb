﻿Public Class ClsReporteCartera

    Public Function ConsultarGrupoCuenta(ByVal strNegocio As String, _
                                         ByRef strRetorno As String, _
                                         Optional ByRef strCodEstado As String = "") As DataSet

        Dim LDS_Cuentas As New DataSet
        Dim LDS_Cuentas_Aux As New DataSet
        Dim Lstr_NombreTabla As String
        Dim LstrProcedimiento
        Dim strDescError As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_GrupoCuenta_Consultar"
        Lstr_NombreTabla = "GRUPO_CUENTAS"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Float, 9).Value = IIf(strNegocio = "", DBNull.Value, CLng(strNegocio))
            SQLCommand.Parameters.Add("pIdGrupoCuenta", SqlDbType.Float, 6).Value = DBNull.Value
            SQLCommand.Parameters.Add("pCodEstado", SqlDbType.VarChar, 3).Value = IIf(strCodEstado.Trim = "", DBNull.Value, strCodEstado.Trim)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Cuentas, Lstr_NombreTabla)

            LDS_Cuentas_Aux = LDS_Cuentas
            strRetorno = "OK"
            Return LDS_Cuentas_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function ConsultarGrupoCuenta(ByVal strNegocio As String, _
                                         ByVal intIdGrupoCuenta As Integer, _
                                         ByRef strRetorno As String) As DataSet

        Dim LDS_Cuentas As New DataSet
        Dim LDS_Cuentas_Aux As New DataSet
        Dim Lstr_NombreTabla As String
        Dim LstrProcedimiento
        Dim strDescError As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_GrupoCuenta_Consultar"
        Lstr_NombreTabla = "GRUPO_CUENTAS"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Float, 9).Value = IIf(strNegocio = "", DBNull.Value, CLng(strNegocio))
            SQLCommand.Parameters.Add("pIdGrupoCuenta", SqlDbType.Float, 6).Value = IIf(intIdGrupoCuenta = 0, DBNull.Value, intIdGrupoCuenta)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Cuentas, Lstr_NombreTabla)

            LDS_Cuentas_Aux = LDS_Cuentas
            strRetorno = "OK"
            Return LDS_Cuentas_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function



    Public Function ConsultarDetallesCuenta(ByRef strRetorno As String, _
                                            Optional ByRef strTipoAdm As String = "", _
                                            Optional ByRef intIdGrupo As Integer = -1, _
                                            Optional ByRef strRutClie As String = "", _
                                            Optional ByRef intIdCuent As Integer = -1) As DataSet

        Dim LDS_CuentasDET As New DataSet
        Dim LDS_CuentasDET_Aux As New DataSet
        Dim Lstr_NombreTabla As String
        Dim LstrProcedimiento
        Dim strDescError As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "RCP_REPORTE_CARTERA_CUENTADETALLE_BUSCAR"
        Lstr_NombreTabla = "GRUPO_CUENTAS"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Int, 10).Value = glngNegocioOperacion
            SQLCommand.Parameters.Add("pCodAdmin", SqlDbType.Char, 5).Value = IIf(strTipoAdm = "", DBNull.Value, strTipoAdm)
            SQLCommand.Parameters.Add("pGrupoCuen", SqlDbType.Int, 10).Value = IIf(intIdGrupo = -1, DBNull.Value, intIdGrupo)
            SQLCommand.Parameters.Add("pRutClient", SqlDbType.Char, 12).Value = IIf(strRutClie = "", DBNull.Value, strRutClie)
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Int, 10).Value = IIf(intIdCuent = -1, DBNull.Value, intIdCuent)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_CuentasDET, Lstr_NombreTabla)

            LDS_CuentasDET_Aux = LDS_CuentasDET
            strRetorno = "OK"
            Return LDS_CuentasDET_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    ' ERB - BEGIN - 2011-10-05
    'Se reemplaza IdCuenta por ListaIdCuentas
    'Public Function BuscarReporteCartera(ByRef strRetorno As String, _
    '                                     ByVal strFecha As String, _
    '                                     ByVal intIdCuent As Integer, _
    '                                     Optional ByRef strSubClase As String = "", _
    '                                     Optional ByRef strClase As String = "", _
    '                                     Optional ByRef strNemo As String = "") As DataSet

    '    Dim LDS_CuentasDET As New DataSet
    '    Dim LDS_CuentasDET_Aux As New DataSet
    '    Dim Lstr_NombreTabla As String
    '    Dim LstrProcedimiento
    '    Dim strDescError As String = ""

    '    Dim SQLCommand As New SqlClient.SqlCommand
    '    Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

    '    Dim Connect As Cls_Conexion = New Cls_Conexion

    '    LstrProcedimiento = "RCP_REPORTE_CARTERA_BUSCAR"
    '    Lstr_NombreTabla = "GRUPO_CUENTAS"

    '    Connect.Abrir()

    '    Try
    '        SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

    '        SQLCommand.CommandType = CommandType.StoredProcedure
    '        SQLCommand.CommandText = LstrProcedimiento
    '        SQLCommand.Parameters.Clear()

    '        SQLCommand.Parameters.Add("pFecha", SqlDbType.Char, 10).Value = strFecha
    '        SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Int, 5).Value = intIdCuent
    '        SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Int, 10).Value = glngNegocioOperacion
    '        SQLCommand.Parameters.Add("pCodClase", SqlDbType.Char, 12).Value = IIf(strClase = "", DBNull.Value, strClase)
    '        SQLCommand.Parameters.Add("pCodSubClase", SqlDbType.Char, 10).Value = IIf(strSubClase = "", DBNull.Value, strSubClase)
    '        SQLCommand.Parameters.Add("pNemo", SqlDbType.Char, 50).Value = IIf(strNemo = "", DBNull.Value, strNemo)
    '        SQLDataAdapter.SelectCommand = SQLCommand
    '        SQLDataAdapter.Fill(LDS_CuentasDET, Lstr_NombreTabla)

    '        LDS_CuentasDET_Aux = LDS_CuentasDET
    '        strRetorno = "OK"
    '        Return LDS_CuentasDET_Aux

    '    Catch ex As Exception
    '        strRetorno = ex.Message
    '        Return Nothing
    '    Finally
    '        Connect.Cerrar()
    '    End Try

    'End Function

    Public Function BuscarReporteCarteraTodos(ByRef strRetorno As String, _
                                              ByVal strFecha As String, _
                                              ByVal strTipoLista As String, _
                                              ByVal strTiporeporte As String, _
                                              ByVal strLista As String, _
                                              ByVal strMoneda As String, _
                                              Optional ByRef strSubClase As String = "", _
                                              Optional ByRef strClase As String = "", _
                                              Optional ByRef strNemo As String = "") As DataSet

        Dim LDS_CuentasDET As New DataSet
        Dim LDS_CuentasDET_Aux As New DataSet
        Dim Lstr_NombreTabla As String
        Dim LstrProcedimiento
        Dim strDescError As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "RCP_REPORTE_CARTERA_BUSCAR"
        Lstr_NombreTabla = "GRUPO_CUENTAS"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pFecha", SqlDbType.Char, 10).Value = strFecha
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Int, 10).Value = glngNegocioOperacion
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Int, 10).Value = glngIdUsuario
            SQLCommand.Parameters.Add("pCodClase", SqlDbType.Char, 12).Value = IIf(strClase = "", DBNull.Value, strClase)
            SQLCommand.Parameters.Add("pCodSubClase", SqlDbType.Char, 10).Value = IIf(strSubClase = "", DBNull.Value, strSubClase)
            SQLCommand.Parameters.Add("pNemo", SqlDbType.Char, 50).Value = IIf(strNemo = "", DBNull.Value, strNemo)
            SQLCommand.Parameters.Add("pTipoLista", SqlDbType.Char, 1).Value = strTipoLista
            SQLCommand.Parameters.Add("pTipoReporte", SqlDbType.Char, 1).Value = strTiporeporte
            SQLCommand.Parameters.Add("pListaCuentas", SqlDbType.Text, 60000).Value = strLista
            SQLCommand.Parameters.Add("pMoneda", SqlDbType.VarChar, 3).Value = IIf(strMoneda = "", DBNull.Value, strMoneda)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_CuentasDET, Lstr_NombreTabla)

            LDS_CuentasDET_Aux = LDS_CuentasDET
            strRetorno = "OK"
            Return LDS_CuentasDET_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function
    ' ERB - END - 2011-10-05

    Public Function ConsultarTipoAdmin(ByRef strRetorno As String) As DataSet

        Dim LDS_Cuentas As New DataSet
        Dim LDS_Cuentas_Aux As New DataSet
        Dim Lstr_NombreTabla As String
        Dim LstrProcedimiento
        Dim strDescError As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_TipoAdministracion_Ver"
        Lstr_NombreTabla = "TIPO_ADMIN"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pCodTipoAdministracion", SqlDbType.Char, 9).Value = DBNull.Value
            SQLCommand.Parameters.Add("pEstado", SqlDbType.Char, 3).Value = "VIG"

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Cuentas, Lstr_NombreTabla)

            LDS_Cuentas_Aux = LDS_Cuentas
            strRetorno = "OK"
            Return LDS_Cuentas_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function BuscarCarteraTodos(ByRef strRetorno As String, _
                                              ByVal strFecha As String, _
                                              ByVal strTipoLista As String, _
                                              ByVal strTiporeporte As String, _
                                              ByVal strLista As String, _
                                              ByVal strMoneda As String, _
                                              ByVal strTipoArbol As String, _
                                              ByVal IdArbolClasificacion As String) As DataSet

        Dim LDS_CuentasDET As New DataSet
        Dim LDS_CuentasDET_Aux As New DataSet
        Dim Lstr_NombreTabla As String
        Dim LstrProcedimiento
        Dim strDescError As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Cartera_Arbol_Buscar"
        Lstr_NombreTabla = "Cartera_por_Arbol"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pFecha", SqlDbType.Char, 10).Value = strFecha
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Int, 10).Value = glngNegocioOperacion
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Int, 10).Value = glngIdUsuario
            'SQLCommand.Parameters.Add("pCodClase", SqlDbType.Char, 12).Value = IIf(strClase = "", DBNull.Value, strClase)
            'SQLCommand.Parameters.Add("pCodSubClase", SqlDbType.Char, 10).Value = IIf(strSubClase = "", DBNull.Value, strSubClase)
            'SQLCommand.Parameters.Add("pNemo", SqlDbType.Char, 50).Value = IIf(strNemo = "", DBNull.Value, strNemo)
            SQLCommand.Parameters.Add("pTipoLista", SqlDbType.Char, 1).Value = strTipoLista
            SQLCommand.Parameters.Add("pTipoReporte", SqlDbType.Char, 1).Value = strTiporeporte
            SQLCommand.Parameters.Add("pListaCuentas", SqlDbType.Text, 60000).Value = strLista
            SQLCommand.Parameters.Add("pMoneda", SqlDbType.VarChar, 10).Value = strMoneda
            SQLCommand.Parameters.Add("pTipoArbol", SqlDbType.Char, 1).Value = strTipoArbol
            SQLCommand.Parameters.Add("pIdArbolClasificacion", SqlDbType.Int, 10).Value = IdArbolClasificacion

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_CuentasDET, Lstr_NombreTabla)

            LDS_CuentasDET_Aux = LDS_CuentasDET
            strRetorno = "OK"
            Return LDS_CuentasDET_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function


    Public Function BuscarCartera_ESC(ByRef strRetorno As String, _
                                          ByVal strFecha As String, _
                                          ByVal intidcuenta As Integer, _
                                          ByVal intidcliente As Integer, _
                                          ByVal intidgrupo As Integer, _
                                          ByVal strTipoArbol As String, _
                                          ByVal IdArbolClasificacion As String, _
                                          ByVal strCodigoArbol As String) As DataSet

        Dim LDS_CuentasDET As New DataSet
        Dim LDS_CuentasDET_Aux As New DataSet
        Dim Lstr_NombreTabla As String
        Dim LstrProcedimiento
        Dim strDescError As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "RCP_Cartera_Esc_Buscar"
        Lstr_NombreTabla = "Cartera_por_Arbol"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pFecha", SqlDbType.Char, 10).Value = strFecha
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Int, 10).Value = glngNegocioOperacion
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Int, 10).Value = glngIdUsuario
            SQLCommand.Parameters.Add("pid_Cuenta", SqlDbType.Int, 10).Value = IIf(intidcuenta = 0, DBNull.Value, intidcuenta)
            SQLCommand.Parameters.Add("pid_Cliente", SqlDbType.Int, 10).Value = IIf(intidcliente = 0, DBNull.Value, intidcliente)
            SQLCommand.Parameters.Add("pid_Grupo", SqlDbType.Int, 10).Value = IIf(intidgrupo = 0, DBNull.Value, intidgrupo)
            SQLCommand.Parameters.Add("pTipoArbol", SqlDbType.Char, 1).Value = strTipoArbol
            SQLCommand.Parameters.Add("pIdArbolClasificacion", SqlDbType.Int, 10).Value = IdArbolClasificacion
            SQLCommand.Parameters.Add("pCodigoArbol", SqlDbType.VarChar, 10).Value = strCodigoArbol

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_CuentasDET, Lstr_NombreTabla)

            LDS_CuentasDET_Aux = LDS_CuentasDET
            strRetorno = "OK"
            Return LDS_CuentasDET_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function BuscarCartera_ESCNC(ByRef strRetorno As String, _
                                          ByVal strFecha As String, _
                                          ByVal intidcuenta As Integer, _
                                          ByVal intidcliente As Integer, _
                                          ByVal intidgrupo As Integer, _
                                          ByVal strTipoArbol As String, _
                                          ByVal IdArbolClasificacion As String, _
                                          ByVal strCodigoArbol As String) As DataSet

        Dim LDS_CuentasDET As New DataSet
        Dim LDS_CuentasDET_Aux As New DataSet
        Dim Lstr_NombreTabla As String
        Dim LstrProcedimiento
        Dim strDescError As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "RCP_Cartera_EscNC_Buscar"
        Lstr_NombreTabla = "Cartera_por_Arbol"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pFecha", SqlDbType.Char, 10).Value = strFecha
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Int, 10).Value = glngNegocioOperacion
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Int, 10).Value = glngIdUsuario
            SQLCommand.Parameters.Add("pid_Cuenta", SqlDbType.Int, 10).Value = IIf(intidcuenta = 0, DBNull.Value, intidcuenta)
            SQLCommand.Parameters.Add("pTipoArbol", SqlDbType.Char, 1).Value = strTipoArbol
            SQLCommand.Parameters.Add("pCodigoArbol", SqlDbType.VarChar, 10).Value = strCodigoArbol

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_CuentasDET, Lstr_NombreTabla)

            LDS_CuentasDET_Aux = LDS_CuentasDET
            strRetorno = "OK"
            Return LDS_CuentasDET_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function


End Class



