﻿Imports System.IO

Public Module gFuncionesSeguridad

    Public Sub GrabarLog(ByVal LogTipoRegistro As TipoRegistroLog, _
                         ByVal IdConceptoRegistro As Integer, _
                         ByVal strMensaje As String)
        Dim obj_LogSistema As ClsSeguridadLog = New ClsSeguridadLog
        Dim strRetorno As String = ""
        strRetorno = obj_LogSistema.Registra_Log_Basico(LogTipoRegistro, _
                                                       IdConceptoRegistro.ToString.Trim, _
                                                       glngIdUsuario, _
                                                       strMensaje.Trim)
        obj_LogSistema = Nothing
    End Sub

    Public Sub RegistraSalidaSistema()
        GrabarLog(TipoRegistroLog.LogEntradaSalida, 8, "Usuario Desconectado.")
    End Sub

    Public Sub RegistraConexionSistema()
        GrabarLog(TipoRegistroLog.LogEntradaSalida, 7, "Usuario Conectado.")
    End Sub

End Module
