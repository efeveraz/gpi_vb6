Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports Microsoft.VisualBasic

Public Class ClsTipoRegistro

    Public Function Buscar(ByRef strRetorno As String, _
                           ByVal pId_Tipo_Registro As Integer, _
                           ByVal pNombre_Tipo_Registro As String, _
                           ByVal pNombre_Corto_Tipo_Registro As String, _
                           ByVal pEstado_Tipo_Registro As String) As DataSet

        Dim LDS_TipoRegistro As New DataSet
        Dim LDS_TipoRegistro_Aux As New DataSet

        Dim Sqlcommand As New SqlClient.SqlCommand
        Dim SqlLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim Lstr_Procedimiento As String = "SGC_SEGURIDAD_LOG.SP_CONSULTA_TIPO_REGISTRO"
        Dim Lstr_NombreTabla As String = "PERFILESACCIONES"

        Connect.Abrir()

        Try
            Sqlcommand = New SqlCommand(Lstr_Procedimiento, Connect.Conexion)
            Sqlcommand.CommandType = CommandType.StoredProcedure
            Sqlcommand.CommandText = Lstr_Procedimiento
            Sqlcommand.Parameters.Clear()

            Sqlcommand.Parameters.Add("@pIdTipoRegistro", SqlDbType.Int).Value = IIf(pId_Tipo_Registro = -1, DBNull.Value, pId_Tipo_Registro)
            Sqlcommand.Parameters.Add("@pNombreTipoRegistro", SqlDbType.Char, 50).Value = IIf(pNombre_Tipo_Registro = "", DBNull.Value, pNombre_Tipo_Registro)
            Sqlcommand.Parameters.Add("@pNombreCortoTipoRegistro", SqlDbType.Char, 15).Value = IIf(pNombre_Corto_Tipo_Registro = "", DBNull.Value, pNombre_Corto_Tipo_Registro)
            Sqlcommand.Parameters.Add("@pEstadoTipoRegistro", SqlDbType.Char, 3).Value = IIf(pEstado_Tipo_Registro = "", DBNull.Value, pEstado_Tipo_Registro)

            SqlLDataAdapter.SelectCommand = Sqlcommand
            SqlLDataAdapter.Fill(LDS_TipoRegistro, Lstr_NombreTabla)

            LDS_TipoRegistro_Aux = LDS_TipoRegistro

            strRetorno = "OK"
            Return LDS_TipoRegistro_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function TipoRegistro_Ver(ByVal LTxt_pId_TipoRegistro As String,
                                     ByVal LTxt_ColumnasDetalle As String,
                                     ByRef LTxt_Resultado As String) As DataSet

        Dim LArr_NombreColumna_Detalle() As String = Split(LTxt_ColumnasDetalle, ",")
        Dim LInt_Col As Integer
        Dim LTxt_NomCol As String
        Dim Columna As DataColumn
        Dim Remove As Boolean

        Dim LDS_CuentasClientes As New DataSet
        Dim LDS_CuentasClientes_Aux As New DataSet

        Dim Sqlcommand As New SqlClient.SqlCommand
        Dim SqlLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim Lstr_Procedimiento As String = "Sis_TipoRegistro_ver"
        Dim Lstr_NombreTabla As String = "Tipo_Registro"

        Connect.Abrir()
        Try
            Sqlcommand = New SqlCommand(Lstr_Procedimiento, Connect.Conexion)

            Sqlcommand.CommandType = CommandType.StoredProcedure
            Sqlcommand.CommandText = Lstr_Procedimiento
            Sqlcommand.Parameters.Clear()

            Sqlcommand.Parameters.Add("@pIdRegistro", SqlDbType.Int).Value = CInt(LTxt_pId_TipoRegistro)

            SqlLDataAdapter.SelectCommand = Sqlcommand
            SqlLDataAdapter.Fill(LDS_CuentasClientes, Lstr_NombreTabla)

            If LTxt_ColumnasDetalle.Trim = "" Then
                LDS_CuentasClientes_Aux = LDS_CuentasClientes
            Else
                LDS_CuentasClientes_Aux = LDS_CuentasClientes.Copy
                For Each Columna In LDS_CuentasClientes.Tables(0).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna_Detalle.Length - 1
                        LTxt_NomCol = LArr_NombreColumna_Detalle(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LTxt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_CuentasClientes_Aux.Tables(0).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            LTxt_Resultado = "OK"
            Return LDS_CuentasClientes_Aux

        Catch ex As Exception
            LTxt_Resultado = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    ''' <summary>
    ''' Funci�n que recupera el Id del tipo de registros seg�n el nombre corto de este.
    ''' </summary>
    ''' <param name="nombreCorto"></param>
    ''' <returns></returns>
    Public Function GetIdTipoRegistro(ByVal nombreCorto As String) As Decimal
        Dim resultado = "OK"
        Dim dsTipoRegistro = TipoRegistro_Ver("0", "", resultado)
        If resultado = "OK" Then
            If Not IsNothing(dsTipoRegistro) AndAlso
                    dsTipoRegistro.Tables.Count > 0 AndAlso
                        dsTipoRegistro.Tables(0).Rows.Count > 0 Then
                Dim row = dsTipoRegistro.Tables(0).AsEnumerable().SingleOrDefault(
                        Function(r)
                            Return r.Field(Of String)("NOMBRE_CORTO_TIPO_REGISTRO") = nombreCorto
                        End Function)
                If Not IsNothing(row) Then
                    Return row.Field(Of Decimal)("ID_TIPO_REGISTRO")
                End If
            End If
            Throw New Exception("No se encontr� el Tipo de Registro.")
        Else
            Throw New Exception(resultado)
        End If
    End Function
End Class
