﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports Microsoft.VisualBasic
Public Class ClsGeneraMenu
    Dim lstrProcedimiento As String = ""
    Dim LstrNombreTabla As String = ""

    Dim lobjAccesos As List(Of ClsUsuario.TAccesos)

    Dim lobjUsuario As ClsUsuario = New ClsUsuario
    Dim lobjAcciones As ClsAcciones = New ClsAcciones

    'Dim DS_Acciones As New DataSet

    Public Sub ConfiguraMenu(ByVal sUsuario As String, ByRef mnuBarraDeMenu As MenuStrip, ByVal Rutina As EventHandler, ByVal int_IdPantalla As Integer)
        Dim oDSMenu As Data.DataSet
        Dim oUsuario As New ClsUsuario()
        Dim oMenu As New ClsAcciones

        Dim oMnu As New System.Windows.Forms.ToolStripMenuItem

        Dim id_menu As Integer
        Dim numero_pantalla As Integer
        Dim numero_linea As Integer
        Dim sub_pantalla As Integer
        Dim descripcion_item As String
        Dim estado_menu As String
        Dim nombre_programa As String
        Dim strRetorno As String = ""
        Dim blnHabilitado As Boolean = True

        '...Trae los accesos del Sistema para el usuario Logeado
        lobjAccesos = oUsuario.TraerAccesosUsuario(strRetorno, sUsuario)

        Dim sRetorno As String = ""
        oDSMenu = oMenu.BuscarOpcionesMenu(sRetorno, int_IdPantalla, glngIdUsuario, glngNegocioOperacion)

        mnuBarraDeMenu.Items.Clear()
        If sRetorno = "OK" Then
            For Each dr As Data.DataRow In oDSMenu.Tables(0).Rows

                oMnu = New System.Windows.Forms.ToolStripMenuItem

                id_menu = dr.Item("ID_ACCION")
                numero_pantalla = dr.Item("NUMERO_PANTALLA")
                numero_linea = dr.Item("NUMERO_LINEA")
                sub_pantalla = dr.Item("SUB_PANTALLA")
                descripcion_item = Trim(dr.Item("DESCRIPCION_ITEM")) ' & "(" & ID_MENU.TOSTRING & ")"

                If IsDBNull(dr.Item("NOMBRE_PROGRAMA")) Then
                    nombre_programa = ""
                Else
                    nombre_programa = Trim(dr.Item("NOMBRE_PROGRAMA"))
                End If

                estado_menu = dr.Item("ESTADO_ACCION")
                blnHabilitado = IIf(dr.Item("CONTROL_HABILITADO") <> 0, True, False)
                With oMnu
                    .ForeColor = System.Drawing.Color.DarkBlue
                    .Font = New System.Drawing.Font("Arial", 7, System.Drawing.FontStyle.Bold)
                    .BackColor = Drawing.Color.Transparent
                    '.BackgroundImage = Global.My.Resources.Img_Fondo_Boton_SubMenu

                    .Size = New System.Drawing.Size(106, 20)
                    .Name = nombre_programa
                    .Text = descripcion_item
                    .TextAlign = Drawing.ContentAlignment.MiddleLeft
                    .Enabled = blnHabilitado
                End With

                'If TieneAcceso(id_menu) Then
                mnuBarraDeMenu.Items.Add(oMnu)

                Dim objSeparador As New System.Windows.Forms.ToolStripSeparator
                objSeparador.AutoSize = True
                mnuBarraDeMenu.Items.Add(objSeparador)
                'LlenaSubMenu("", sub_pantalla, oMnu, sub_pantalla, Rutina, False)
                AddHandler oMnu.Click, Rutina
                'End If

                oMnu = Nothing
                ' If MostrarUnNivel Then Exit For
            Next

        End If

        oUsuario = Nothing
        oMenu = Nothing

    End Sub

    'Private Sub LlenaSubMenu(ByVal sNombreMenu As String, _
    '                   ByVal nSubPantalla As Integer, _
    '                   ByVal oNodoRaiz As System.Windows.Forms.ToolStripMenuItem, _
    '                   ByVal idUsuario As Integer, _
    '                   ByRef sRutina As EventHandler, _
    '                   Optional ByVal Navegate As Boolean = True)

    '    Dim id_menu As Integer
    '    Dim numero_pantalla As Integer
    '    Dim numero_linea As Integer
    '    Dim sub_pantalla As Integer
    '    Dim descripcion_item As String = ""
    '    Dim nombre_programa As String = ""
    '    Dim bFlagLinea As Boolean = True

    '    Dim strRetorno As String = ""
    '    Dim estado_menu As String = ""
    '    Dim oDSMenu As New DataSet
    '    Dim oMenu As New WSC_Menu

    '    'oMenu.Url = GTxt_URLServiciosSigCO & GTxt_Menu_asmx

    '    oDSMenu = oMenu.Buscar(strRetorno, idUsuario, 0)

    '    If strRetorno = "OK" Then
    '        For Each dr As DataRow In oDSMenu.Tables("C_MENU").Rows
    '            Dim oHijo As New System.Windows.Forms.ToolStripMenuItem

    '            oHijo = New System.Windows.Forms.ToolStripMenuItem

    '            id_menu = dr.Item("ID_MENU")
    '            numero_pantalla = dr.Item("NUMERO_PANTALLA")
    '            numero_linea = dr.Item("NUMERO_LINEA")
    '            sub_pantalla = dr.Item("SUB_PANTALLA")
    '            descripcion_item = dr.Item("DESCRIPCION_ITEM").ToString.Trim

    '            If IsDBNull(dr.Item("NOMBRE_PROGRAMA")) Then
    '                nombre_programa = ""
    '            Else
    '                nombre_programa = dr.Item("NOMBRE_PROGRAMA").ToString.Trim
    '            End If

    '            If IsDBNull(dr.Item("estado_menu")) Then
    '                estado_menu = "BLO"
    '            Else
    '                estado_menu = dr.Item("estado_menu").ToString.Trim
    '            End If

    '            If descripcion_item = "\-" Then

    '                'If Not bFlagLinea Then
    '                '    Dim oLinea As New System.Windows.Forms.ToolStripSeparator
    '                '    oLinea = New System.Windows.Forms.ToolStripSeparator
    '                '    oLinea.Name = id_menu.ToString
    '                '    oLinea.AutoSize = True
    '                '    oNodoRaiz.DropDownItems.Add(oLinea)
    '                '    bFlagLinea = True
    '                'End If

    '            Else
    '                oHijo = New System.Windows.Forms.ToolStripMenuItem
    '                With oHijo
    '                    .BackColor = Color.Transparent
    '                    '.BackColor = Color.Transparent
    '                    .ForeColor = Color.SteelBlue
    '                    '.BackgroundImage = Global.Siccb_Penta.My.Resources.Resources.Fondo_Panel_Generico_Mediano

    '                    .Size = New System.Drawing.Size(250, 22)
    '                    .Name = id_menu.ToString
    '                    .Text = descripcion_item
    '                    .Font = New System.Drawing.Font("Verdana", 8.25!)

    '                    '.DisplayStyle = ToolStripItemDisplayStyle.ImageAndText
    '                    ' .Image = My.Resources.Resources.Img_Mnu_Bloqueo_Aplicacion
    '                    ' .ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
    '                End With

    '                ' oHijo.ShowCheckBox = ConCheckBox

    '                If nombre_programa = "" Then
    '                    oHijo.Tag = ""
    '                Else
    '                    oHijo.Tag = nombre_programa
    '                    'If Navegate Then
    '                    'oHijo.NavigateUrl = nombre_programa
    '                    'oHijo.Target = Target
    '                    'End If
    '                End If

    '                If nombre_programa <> "accion" Then
    '                    'If ConCheckBox Then
    '                    LlenaSubMenu("", sub_pantalla, oHijo, sub_pantalla, sRutina, Navegate:=False)

    '                    ' Si no tiene Hijos es porque se debe abrir con Click 
    '                    If Not oHijo.HasDropDownItems Then
    '                        AddHandler oHijo.Click, sRutina ' AddressOf SelectedChildMenu_OnClick
    '                    End If

    '                    If TieneAcceso(id_menu) Then
    '                        oNodoRaiz.DropDownItems.Add(oHijo)
    '                        bFlagLinea = False
    '                    End If
    '                End If
    '            End If

    '        Next
    '    End If

    '    oMenu = Nothing

    'End Sub

    Public Function TieneAcceso(ByVal intIdAccion As Integer) As Boolean
        If IsNothing(lobjAccesos) Then Return False
        Dim lValRet As Boolean = False
        Dim i As Integer = 0
        Dim bHabilitado As Boolean = False

        Dim lobjAcceso As ClsUsuario.TAccesos

        For Each lobjAcceso In lobjAccesos
            If lobjAcceso.strIdAccion = intIdAccion Then
                Return True
            End If
        Next

        Return False

    End Function

    'Public Sub MenuParaAsignacion(ByVal sSubPantalla As String, _
    '                                ByVal sPerfilUsuario As String, _
    '                                ByRef mnuBarraDeMenu As MenuStrip)

    '    Dim oDSMenu As DataSet
    '    ' Dim oPerfilUsuario As New clsPerfilUsuario.WSC_Perfil_Usuario
    '    Dim oUsuario As New WSC_Usuarios
    '    Dim oMenu As New WSC_Menu

    '    'oMenu.Url = GTxt_URLServiciosSigCO & GTxt_Menu_asmx

    '    'oUsuario.Url = GTxt_URLServiciosSigCO & GTxt_Usuarios_asmx

    '    Dim oMnu As New System.Windows.Forms.ToolStripMenuItem
    '    Dim id_menu As Integer
    '    Dim numero_pantalla As Integer
    '    Dim numero_linea As Integer
    '    Dim sub_pantalla As Integer
    '    Dim descripcion_item As String
    '    Dim estado_menu As String
    '    Dim nombre_programa As String
    '    Dim strRetorno As String = ""

    '    ' Trae los accesos del Sistema para el usuario Logeado
    '    oTAccesos = oUsuario.TraerAccesosPerfil(strRetorno, sPerfilUsuario)

    '    '    Dim i As Integer = 0
    '    '    oMenu.ConCheckBox = False
    '    '    oMenu.MostrarUnNivel = True
    '    '    oMenu.MuestraLineas = False
    '    '    oMenu.MostrarMenuCompleto = False

    '    '    If Request("ifr") <> "" Then
    '    '        oMenu.Target = "principal2"
    '    '    End If

    '    Dim sRetorno As String = ""
    '    oDSMenu = oMenu.Buscar(sRetorno, sSubPantalla, 0)

    '    mnuBarraDeMenu.Items.Clear()

    '    If sRetorno = "OK" Then
    '        For Each dr As DataRow In oDSMenu.Tables("C_MENU").Rows
    '            oMnu = New System.Windows.Forms.ToolStripMenuItem

    '            id_menu = dr.Item("ID_MENU")
    '            numero_pantalla = dr.Item("NUMERO_PANTALLA")
    '            numero_linea = dr.Item("NUMERO_LINEA")
    '            sub_pantalla = dr.Item("SUB_PANTALLA")
    '            descripcion_item = dr.Item("DESCRIPCION_ITEM").ToString.Trim  ' & "(" & ID_MENU.TOSTRING & ")"

    '            If IsDBNull(dr.Item("NOMBRE_PROGRAMA")) Then
    '                nombre_programa = ""
    '            Else
    '                nombre_programa = dr.Item("NOMBRE_PROGRAMA").ToString.Trim.ToLower
    '            End If

    '            estado_menu = dr.Item("ESTADO_MENU")

    '            With oMnu
    '                .ForeColor = System.Drawing.Color.DarkBlue
    '                .Size = New System.Drawing.Size(106, 20)
    '                .Name = id_menu.ToString
    '                .Text = descripcion_item '& "(" & id_menu.ToString & ")"

    '                If nombre_programa = "accion" Then
    '                    .ForeColor = System.Drawing.Color.DarkRed
    '                End If

    '            End With

    '            oMnu.Checked = TieneAcceso(id_menu)    ' Si tiene acceso lo marca con un ticket 
    '            oMnu.CheckOnClick = True

    '            AddHandler oMnu.Click, AddressOf SelectedItem_Click

    '            ' oMnu.CheckOnClick = True
    '            mnuBarraDeMenu.Items.Add(oMnu)

    '            LlenaSubMenuParaAsignacion("", sub_pantalla, oMnu, sub_pantalla, False)

    '            oMnu = Nothing
    '            ' If MostrarUnNivel Then Exit For
    '        Next

    '    End If

    '    oUsuario = Nothing
    '    oMenu = Nothing

    'End Sub

    'Private Sub LlenaSubMenuParaAsignacion(ByVal sNombreMenu As String, _
    '                   ByVal nSubPantalla As Integer, _
    '                   ByVal oNodoRaiz As System.Windows.Forms.ToolStripMenuItem, _
    '                   ByVal idUsuario As Integer, _
    '                   Optional ByVal Navegate As Boolean = True)

    '    Dim id_menu As Integer
    '    Dim numero_pantalla As Integer
    '    Dim numero_linea As Integer
    '    Dim sub_pantalla As Integer
    '    Dim descripcion_item As String = ""
    '    Dim nombre_programa As String = ""
    '    Dim bFlagLinea As Boolean = True

    '    Dim strRetorno As String = ""
    '    Dim estado_menu As String = ""

    '    Dim oDSMenu As New DataSet
    '    Dim oMenu As New WSC_Menu

    '    'oMenu.Url = GTxt_URLServiciosSigCO & GTxt_Menu_asmx

    '    oDSMenu = oMenu.Buscar(strRetorno, idUsuario, 0)

    '    If strRetorno = "OK" Then
    '        For Each dr As DataRow In oDSMenu.Tables("C_MENU").Rows
    '            Dim oHijo As New System.Windows.Forms.ToolStripMenuItem

    '            oHijo = New System.Windows.Forms.ToolStripMenuItem

    '            id_menu = dr.Item("ID_MENU")
    '            numero_pantalla = dr.Item("NUMERO_PANTALLA")
    '            numero_linea = dr.Item("NUMERO_LINEA")
    '            sub_pantalla = dr.Item("SUB_PANTALLA")
    '            descripcion_item = dr.Item("DESCRIPCION_ITEM").ToString.Trim ' & "(" & id_menu.ToString & ")"

    '            If IsDBNull(dr.Item("NOMBRE_PROGRAMA")) Then
    '                nombre_programa = ""
    '            Else
    '                nombre_programa = dr.Item("NOMBRE_PROGRAMA").ToString.Trim
    '            End If

    '            If IsDBNull(dr.Item("estado_menu")) Then
    '                estado_menu = "BLO"
    '            Else
    '                estado_menu = dr.Item("estado_menu").ToString.Trim
    '            End If

    '            If descripcion_item = "\-" Then

    '                If Not bFlagLinea Then
    '                    Dim oLinea As New System.Windows.Forms.ToolStripSeparator
    '                    oLinea = New System.Windows.Forms.ToolStripSeparator
    '                    oLinea.Name = id_menu.ToString
    '                    oLinea.AutoSize = True
    '                    oNodoRaiz.DropDownItems.Add(oLinea)
    '                    bFlagLinea = True
    '                End If

    '            Else
    '                oHijo = New System.Windows.Forms.ToolStripMenuItem

    '                With oHijo
    '                    ' Dim oToolTips As New C1.Win.C1SuperTooltip.C1SuperTooltip

    '                    .ForeColor = System.Drawing.Color.Indigo
    '                    .Size = New System.Drawing.Size(250, 22)
    '                    .Name = id_menu.ToString
    '                    .Text = descripcion_item  '& "(" & id_menu.ToString & ")"
    '                    .Font = New System.Drawing.Font("Verdana", 8.25!)

    '                    If nombre_programa = "accion" Then
    '                        .ForeColor = System.Drawing.Color.DarkRed
    '                        oHijo.ToolTipText = id_menu.ToString
    '                    End If

    '                    ' .Image = My.Resources.Resources.Img_Mnu_Bloqueo_Aplicacion
    '                    ' .ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None

    '                End With

    '                ' oHijo.ShowCheckBox = ConCheckBox

    '                If nombre_programa = "" Then
    '                    oHijo.Tag = ""
    '                Else
    '                    oHijo.Tag = nombre_programa
    '                    'If Navegate Then
    '                    'oHijo.NavigateUrl = nombre_programa
    '                    'oHijo.Target = Target
    '                    'End If
    '                End If


    '                'If ConCheckBox Then
    '                LlenaSubMenuParaAsignacion("", sub_pantalla, oHijo, sub_pantalla, Navegate:=False)

    '                ' Si no tiene Hijos es porque se debe abrir con Click 
    '                If Not oHijo.HasDropDownItems Then
    '                End If

    '                If TieneAcceso(id_menu) Then
    '                    oHijo.Checked = True
    '                Else
    '                    oHijo.Checked = False
    '                End If

    '                ' oHijo.CheckOnClick = True

    '                AddHandler oHijo.Click, AddressOf SelectedChildMenu_Click

    '                oNodoRaiz.DropDownItems.Add(oHijo)

    '                bFlagLinea = False
    '            End If
    '        Next
    '    End If

    '    oMenu = Nothing

    'End Sub

    'Private Sub SelectedChildMenu_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Dim sPrograma As String = sender.tag.ToString.Trim.ToLower
    '    Dim oHijo As Object ' New System.Windows.Forms.ToolStripMenuItem
    '    Dim oHijo1 As New System.Windows.Forms.ToolStripMenuItem
    '    Dim bolItem As Boolean

    '    If Not sender.HasDropDownItems Then
    '        sender.checked = Not sender.checked

    '        ' sender.Owner.Select()
    '        'oHijo1 = New System.Windows.Forms.ToolStripMenuItem
    '        'oHijo1.Owner
    '        ' sender.OwnerItem.Select()

    '    Else
    '        bolItem = Not sender.checked
    '        sender.checked = bolItem

    '        For Each oHijo In sender.dropdownitems
    '            If TypeOf oHijo Is System.Windows.Forms.ToolStripMenuItem Then
    '                oHijo.Checked = bolItem
    '            ElseIf TypeOf oHijo Is System.Windows.Forms.ToolStripSeparator Then

    '            End If
    '        Next
    '    End If
    '    ''Try
    '    ''    If sPrograma <> "" Then

    '    ''        GetFormulario(sPrograma).Show(Me)

    '    ''    Else
    '    ''        MsgBox("La opción " & sender.name & ", no está implementada", MsgBoxStyle.Exclamation, " Error")
    '    ''    End If

    '    ''Catch ex As Exception
    '    ''    MsgBox("Ocurrió el siguiente error : " & vbCrLf & "- Numero de Error : " & Err.Number & vbCrLf & "- El programa o formulario """ & sPrograma & """ no existe " & vbCrLf & "Mensaje Adicional " & ex.Message & "(" & ex.Source & ")", MsgBoxStyle.Exclamation, " Error")
    '    ''End Try

    'End Sub

    Public Sub MenuParaAsignacionTreeView(ByVal lstrSubPantalla As String, ByVal strIdPerfilUsuario As String, ByRef objTreeView As TreeView)

        Dim lObjTreenode As New TreeNode ' -->Dim oMnu As New TreeNode
        Dim lintIdAccion As Integer
        Dim lintNumeroPantalla As Integer
        Dim lintNumeroLinea As Integer
        Dim lintSubPantalla As Integer
        Dim lstrDescripcionItem As String
        Dim lstrEstadoAccion As String
        Dim lstrNombrePrograma As String
        Dim lstrRetorno As String = ""
        'Dim sRetorno As String = ""


        ' Trae los accesos del Sistema para el usuario Logeado
        lobjAccesos = lobjUsuario.TraerAccesosPerfil(lstrRetorno, strIdPerfilUsuario, glngNegocioOperacion)

        ' Trae el menu de la pantalla 
        'DS_Acciones = lobjAcciones.BuscarAcciones(lstrRetorno, lstrSubPantalla, 0)

        With objTreeView
            .Nodes.Clear()
            .CheckBoxes = True
            .Scrollable = True
            AddHandler .BeforeCheck, AddressOf SelectedItemTreeview_AfterCheck
        End With

        If lstrRetorno = "OK" Then
            'For Each dr As DataRow In DS_Acciones.Tables(0).Rows
            For Each dr As DataRow In gDS_Acciones.Tables(0).Select("NUMERO_PANTALLA=" & lstrSubPantalla, "")
                lObjTreenode = New TreeNode

                lintIdAccion = dr.Item("ID_ACCION")
                lintNumeroPantalla = dr.Item("NUMERO_PANTALLA")
                lintNumeroLinea = dr.Item("NUMERO_LINEA")
                lintSubPantalla = dr.Item("SUB_PANTALLA")
                lstrDescripcionItem = dr.Item("DESCRIPCION_ITEM").ToString.Trim  ' & "(" & ID_MENU.TOSTRING & ")"

                If IsDBNull(dr.Item("NOMBRE_PROGRAMA")) Then
                    lstrNombrePrograma = ""
                Else
                    lstrNombrePrograma = dr.Item("NOMBRE_PROGRAMA").ToString.Trim.ToLower
                End If

                lstrEstadoAccion = dr.Item("ESTADO_ACCION")

                With lObjTreenode
                    .ForeColor = System.Drawing.Color.DarkBlue
                    .Name = lintIdAccion.ToString
                    .Text = lstrDescripcionItem '& "(" & id_menu.ToString & ")"

                    If lstrNombrePrograma = "accion" Then
                        .ForeColor = System.Drawing.Color.DarkRed
                    End If
                End With

                ' Si tiene acceso lo marca con un ticket 
                lObjTreenode.Checked = TieneAcceso(lintIdAccion)
                lObjTreenode.EnsureVisible()
                If objTreeView.Name = lintIdAccion And lstrSubPantalla = "1" Then
                    lObjTreenode.Expand()
                    objTreeView.Nodes.Add(lObjTreenode)
                End If
                LlenaSubMenuParaAsignacionTreeview("", lintSubPantalla, lObjTreenode, lintSubPantalla, False)

                lObjTreenode = Nothing
            Next

        End If


    End Sub

    Private Sub LlenaSubMenuParaAsignacionTreeview(ByVal strNombreMenu As String, _
                                                   ByVal intSubPantalla As Integer, _
                                                   ByVal objNodoRaiz As TreeNode, _
                                                   ByVal intIdUsuario As Integer, _
                                          Optional ByVal blnNavegate As Boolean = True)

        Dim lintIdAccion As Integer
        Dim lintNumeroPantalla As Integer
        Dim lintNumeroLinea As Integer
        Dim lntSubPantalla As Integer
        Dim lstrDescripcionItem As String = ""
        Dim lstrNombrePrograma As String = ""
        Dim lblnFlagLinea As Boolean = True
        Dim lstrRetorno As String = ""
        Dim lstrEstadoMenu As String = ""


        'DS_Acciones = lobjAcciones.BuscarAcciones(lstrRetorno, intIdUsuario, 0)

        If gDS_Acciones.Tables(0).Rows.Count > 0 Then
            'For Each dr As DataRow In DS_Acciones.Tables(0).Rows
            For Each dr As DataRow In gDS_Acciones.Tables(0).Select("NUMERO_PANTALLA=" & intSubPantalla, "")
                Dim oHijo As New TreeNode

                oHijo = New TreeNode

                lintIdAccion = dr.Item("ID_ACCION")
                lintNumeroPantalla = dr.Item("NUMERO_PANTALLA")
                lintNumeroLinea = dr.Item("NUMERO_LINEA")
                lntSubPantalla = dr.Item("SUB_PANTALLA")
                lstrDescripcionItem = dr.Item("DESCRIPCION_ITEM").ToString.Trim ' & "(" & id_menu.ToString & ")"

                If IsDBNull(dr.Item("NOMBRE_PROGRAMA")) Then
                    lstrNombrePrograma = ""
                Else
                    lstrNombrePrograma = dr.Item("NOMBRE_PROGRAMA").ToString.Trim
                End If

                If IsDBNull(dr.Item("ESTADO_ACCION")) Then
                    lstrEstadoMenu = "BLO"
                Else
                    lstrEstadoMenu = dr.Item("ESTADO_ACCION").ToString.Trim
                End If

                If lstrDescripcionItem = "\-" Then
                    If Not lblnFlagLinea Then

                        Dim oLinea As New TreeNode ' System.Windows.Forms.ToolStripSeparator
                        oLinea.Name = lintIdAccion.ToString
                        oLinea.Text = oLinea.Text.PadRight(20, "-"c)
                        oLinea.Tag = "\-"
                        oLinea.Checked = True


                        objNodoRaiz.Nodes.Add(oLinea)
                        lblnFlagLinea = True
                    End If

                Else
                    oHijo = New TreeNode

                    With oHijo

                        .ForeColor = System.Drawing.Color.Indigo
                        .Name = lintIdAccion.ToString
                        .Text = lstrDescripcionItem '& "(" & id_menu.ToString & ")"

                        If lstrNombrePrograma = "accion" Then
                            .ForeColor = System.Drawing.Color.DarkRed
                            oHijo.ToolTipText = lintIdAccion.ToString
                        End If

                    End With

                    If lstrNombrePrograma = "" Then
                        oHijo.Tag = ""
                    Else
                        oHijo.Tag = lstrNombrePrograma
                    End If


                    LlenaSubMenuParaAsignacionTreeview("", lntSubPantalla, oHijo, lntSubPantalla, blnNavegate:=False)


                    If TieneAcceso(lintIdAccion) Then
                        oHijo.Checked = True
                    Else
                        oHijo.Checked = False
                    End If



                    objNodoRaiz.Nodes.Add(oHijo)

                    lblnFlagLinea = False
                End If
            Next
        End If


    End Sub

    Private Sub SelectedItemTreeview_AfterCheck(ByVal sender As Object, ByVal e As TreeViewCancelEventArgs)
        If e.Action <> TreeViewAction.Unknown Then
            Dim bolItem As Boolean
            bolItem = Not e.Node.Checked
            If Not IsNothing(e.Node.Parent) Then
                CheckAllParentNodes(e.Node, bolItem)

            End If

            If e.Node.Nodes.Count > 0 Then
                Me.CheckAllChildNodes(e.Node, bolItem)
            ElseIf e.Node.Tag = "\-" Then
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub CheckAllChildNodes(ByVal treeNode As TreeNode, ByVal nodeChecked As Boolean)
        Dim node As TreeNode

        For Each node In treeNode.Nodes
            node.Checked = nodeChecked
            If node.Nodes.Count > 0 Then
                Me.CheckAllChildNodes(node, nodeChecked)
            End If
        Next node
    End Sub

    Private Sub CheckAllParentNodes(ByVal treeNode As TreeNode, ByVal nodeChecked As Boolean)
        Dim node As TreeNode
        If nodeChecked Then
            treeNode.Parent.Checked = nodeChecked
            node = treeNode.Parent
            If Not IsNothing(node.Parent) Then
                CheckAllParentNodes(node, nodeChecked)
            End If
            'Else
            '    Dim lBlnHijosChecked As Boolean = False
            '    For Each node In treeNode.Parent.Nodes
            '        If node.Index <> treeNode.Index Then
            '            If node.Checked Then
            '                lBlnHijosChecked = True
            '                Exit For
            '            End If
            '        End If
            '    Next
            '    If Not lBlnHijosChecked Then
            '        treeNode.Parent.Checked = nodeChecked
            '        node = treeNode.Parent
            '        If Not IsNothing(node.Parent) Then
            '            CheckAllParentNodes(node, nodeChecked)
            '        End If
            '    End If
        End If
    End Sub
End Class
