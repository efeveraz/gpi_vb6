﻿Imports System.Data
Imports System.Data.SqlClient
Public Class ClsCuentasUsuarios
    Public Function RetornaCuentasDisponibles(ByRef strRetorno As String, _
                                              ByVal intIdNegocio As Integer, _
                                              ByVal intIdUsuario As Integer, _
                                              ByVal strNombreCuenta As String, _
                                              ByVal strNombreCortoCuenta As String, _
                                              ByVal strNumeroCuenta As String, _
                                              ByVal strRutCliente As String, _
                                              ByVal strNombreCliente As String, _
                                              ByVal strApellidoPaterno As String, _
                                              ByVal strApellidoMaterno As String, _
                                              ByVal strRazonSocial As String, _
                                              ByVal intIdPerfilRiesgo As Integer, _
                                              ByVal strCodTipoAdministracion As String, _
                                              ByVal strCodSucursal As String, _
                                              ByVal strSegmentoCliente As String, _
                                              ByVal strEstadoPerfil As String, _
                                              ByVal dblAsesor As Double, _
                                              ByVal strColumnas As String) As DataSet

        Dim LDS_Cuentas As New DataSet
        Dim LDS_Cuentas_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim LstrNombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Sis_CuentasUsuario_BuscarDisponibles"
        LstrNombreTabla = "CUENTASDISPONIBLES"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("@pIdNegocio", SqlDbType.Int).Value = intIdNegocio
            SQLCommand.Parameters.Add("@pIdUsuario", SqlDbType.Int).Value = intIdUsuario
            SQLCommand.Parameters.Add("@pNombreCuenta", SqlDbType.VarChar, 50).Value = IIf(strNombreCuenta = "", DBNull.Value, strNombreCuenta)
            SQLCommand.Parameters.Add("@pNombreCortoCuenta", SqlDbType.VarChar, 20).Value = IIf(strNombreCortoCuenta = "", DBNull.Value, strNombreCortoCuenta)
            SQLCommand.Parameters.Add("@pNumeroCuenta", SqlDbType.VarChar, 15).Value = IIf(strNumeroCuenta = "", DBNull.Value, strNumeroCuenta)
            SQLCommand.Parameters.Add("@pNombreCliente", SqlDbType.VarChar, 50).Value = IIf(strNombreCliente = "", DBNull.Value, strNombreCliente)
            SQLCommand.Parameters.Add("@pApellidoPaterno", SqlDbType.VarChar, 50).Value = IIf(strApellidoPaterno = "", DBNull.Value, strApellidoPaterno)
            SQLCommand.Parameters.Add("@pApellidoMaterno", SqlDbType.VarChar, 50).Value = IIf(strApellidoMaterno = "", DBNull.Value, strApellidoMaterno)
            SQLCommand.Parameters.Add("@pRazonSocial", SqlDbType.VarChar, 50).Value = IIf(strRazonSocial = "", DBNull.Value, strRazonSocial)
            SQLCommand.Parameters.Add("@pIdPerfilRiesgo", SqlDbType.Int).Value = IIf(intIdPerfilRiesgo = "0", DBNull.Value, intIdPerfilRiesgo)
            SQLCommand.Parameters.Add("@pCodTipoAdministracion", SqlDbType.VarChar, 5).Value = IIf(strCodTipoAdministracion = "", DBNull.Value, strCodTipoAdministracion)
            SQLCommand.Parameters.Add("@pCodSucursal", SqlDbType.VarChar, 10).Value = IIf(strCodSucursal = "0", DBNull.Value, strCodSucursal)
            SQLCommand.Parameters.Add("@pSegmentoCliente", SqlDbType.Int).Value = IIf(strSegmentoCliente = "0", DBNull.Value, strSegmentoCliente)
            SQLCommand.Parameters.Add("@pAsesor", SqlDbType.Float).Value = IIf(dblAsesor = 0, DBNull.Value, dblAsesor)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Cuentas, LstrNombreTabla)

            LDS_Cuentas_Aux = LDS_Cuentas


            If strColumnas.Trim = "" Then
                LDS_Cuentas_Aux = LDS_Cuentas
            Else
                LDS_Cuentas_Aux = LDS_Cuentas.Copy
                For Each Columna In LDS_Cuentas.Tables(LstrNombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Cuentas_Aux.Tables(LstrNombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Cuentas_Aux.Tables(LstrNombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            Return LDS_Cuentas_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try


    End Function

    Public Function RetornaCuentasAsociadas(ByRef strRetorno As String, _
                                            ByVal strColumnas As String, _
                                            Optional ByVal intIdUsuario As Integer = 0, _
                                            Optional ByVal intIdCuenta As Integer = 0) As DataSet
        Dim LDS_Cuentas As New DataSet
        Dim LDS_Cuentas_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim LstrNombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Sis_CuentasUsuarios_ver"
        LstrNombreTabla = "CUENTASUSUARIO"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("@pIdUsuario", SqlDbType.Int).Value = IIf(intIdUsuario = 0, DBNull.Value, intIdUsuario)
            SQLCommand.Parameters.Add("@pIdCuenta", SqlDbType.Int).Value = IIf(intIdCuenta = 0, DBNull.Value, intIdCuenta)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Cuentas, LstrNombreTabla)

            LDS_Cuentas_Aux = LDS_Cuentas


            If strColumnas.Trim = "" Then
                LDS_Cuentas_Aux = LDS_Cuentas
            Else
                LDS_Cuentas_Aux = LDS_Cuentas.Copy
                For Each Columna In LDS_Cuentas.Tables(LstrNombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Cuentas_Aux.Tables(LstrNombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Cuentas_Aux.Tables(LstrNombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            Return LDS_Cuentas_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function ProcesarCuentasUsuario(ByVal intIdUsuario As Integer, _
                                           ByVal DS_CuentasUsuario As DataSet) As String

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        Dim lstrProcedimiento As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim lstrRetorno As String = "OK"
        Dim drFila As DataRow

        lstrProcedimiento = "Sis_CuentasUsuario_Mantencion"
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, MiTransaccionSQL.Connection, MiTransaccionSQL)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = lstrProcedimiento
            SQLCommand.Parameters.Clear()


            'Primero se elimina todas las relaciones Cuentas-Usuario
            SQLCommand.Parameters.Add("@pCodigoOperacion", SqlDbType.Char, 20).Value = "ELIMINAR"
            SQLCommand.Parameters.Add("@pIdUsuario", SqlDbType.Int).Value = intIdUsuario

            Dim oParamSal As New SqlClient.SqlParameter("@pMsjRetorno", SqlDbType.Char, 60)
            oParamSal.Direction = ParameterDirection.Output
            SQLCommand.Parameters.Add(oParamSal)

            SQLCommand.ExecuteNonQuery()

            If Trim(lstrRetorno) <> "OK" Then
                GoTo Salir
            End If


            For Each drFila In DS_CuentasUsuario.Tables(0).Rows
                SQLCommand.Parameters.Clear()

                SQLCommand.Parameters.Add("@pCodigoOperacion", SqlDbType.Char, 20).Value = "AGREGAR"
                SQLCommand.Parameters.Add("@pIdUsuario", SqlDbType.Int).Value = intIdUsuario
                SQLCommand.Parameters.Add("@pIdCuenta", SqlDbType.Int).Value = drFila("ID_CUENTA")

                Dim oParamSal2 As New SqlClient.SqlParameter("@pMsjRetorno", SqlDbType.Char, 60)
                oParamSal2.Direction = ParameterDirection.Output
                SQLCommand.Parameters.Add(oParamSal2)

                SQLCommand.ExecuteNonQuery()

                lstrRetorno = SQLCommand.Parameters("@pMsjRetorno").Value.ToString.Trim

                If Trim(lstrRetorno) <> "OK" Then
                    GoTo Salir
                End If
            Next
            If Trim(lstrRetorno) = "OK" Then
                MiTransaccionSQL.Commit()
                Return ("OK")
            End If


Salir:
            If Trim(lstrRetorno) = "OK" Then
                MiTransaccionSQL.Commit()
                Return ("OK")
            Else
                MiTransaccionSQL.Rollback()
                Return lstrRetorno
            End If

        Catch ex As Exception
            MiTransaccionSQL.Rollback()
            lstrRetorno = ex.Message
            Return lstrRetorno
        Finally
            SQLConnect.Cerrar()
        End Try

    End Function
End Class
