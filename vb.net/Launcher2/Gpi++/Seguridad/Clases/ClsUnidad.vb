﻿Public Class ClsUnidad

    Public Function RetornaUnidades(ByRef strRetorno As String, _
                                    ByVal strColumnas As String, _
                                    ByVal intIdUnidad As Integer, _
                                    ByVal strNombreUnidad As String, _
                                    ByVal strNombreCortoUnidad As String, _
                                    ByVal strCodigoExternoUnidad As String, _
                                    ByVal strEstadoUnidad As String) As DataSet

        Dim lblnRemove As Boolean = True
        Dim larr_NombreColumna() As String = Split(strColumnas, ",")
        Dim lInt_Col As Integer = 0
        Dim lInt_NomCol As String = ""

        Dim lDS_Unidad As New DataSet
        Dim lDS_Unidad_Aux As New DataSet

        Dim Sqlcommand As New SqlClient.SqlCommand
        Dim SqlDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim LstrProcedimiento As String = ""
        Dim LstrNombreTabla As String = ""

        'Lstr_Procedimiento = "SGC_SP_CONSULTA_UNIDAD"
        LstrProcedimiento = "Sis_Unidad_Consultar"
        LstrNombreTabla = "UNIDAD"
        Connect.Abrir()

        Try
            Sqlcommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            Sqlcommand.CommandType = CommandType.StoredProcedure
            Sqlcommand.CommandText = LstrProcedimiento
            Sqlcommand.Parameters.Clear()

            Sqlcommand.Parameters.Add("@pIdUnidad", SqlDbType.Int).Value = IIf(intIdUnidad = 0, DBNull.Value, intIdUnidad)
            Sqlcommand.Parameters.Add("@pNombreUnidad", SqlDbType.Int).Value = IIf(strNombreUnidad = "", DBNull.Value, strNombreUnidad)
            Sqlcommand.Parameters.Add("@pNombreCortoUnidad", SqlDbType.Int).Value = IIf(strNombreCortoUnidad = "", DBNull.Value, strNombreCortoUnidad)
            Sqlcommand.Parameters.Add("@pEstadoUnidad", SqlDbType.Int).Value = IIf(strEstadoUnidad = "", DBNull.Value, strEstadoUnidad)

            Dim oParamSal As New SqlClient.SqlParameter("@pMsjRetorno", SqlDbType.Char, 60)
            oParamSal.Direction = ParameterDirection.Output
            Sqlcommand.Parameters.Add(oParamSal)


            SqlDataAdapter.SelectCommand = Sqlcommand
            SqlDataAdapter.Fill(lDS_Unidad, LstrNombreTabla)

            If strColumnas.Trim = "" Then
                lDS_Unidad_Aux = lDS_Unidad
            Else
                lDS_Unidad_Aux = lDS_Unidad.Copy
                For Each lstrColumna As DataColumn In lDS_Unidad.Tables(LstrNombreTabla).Columns
                    lblnRemove = True
                    For lInt_Col = 0 To larr_NombreColumna.Length - 1
                        lInt_NomCol = larr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                        If lstrColumna.ColumnName = lInt_NomCol Then
                            lblnRemove = False
                        End If
                    Next
                    If lblnRemove Then
                        lDS_Unidad_Aux.Tables(LstrNombreTabla).Columns.Remove(lstrColumna.ColumnName)
                    End If
                Next
            End If

            For lInt_Col = 0 To larr_NombreColumna.Length - 1
                lInt_NomCol = larr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                For Each lstrColumna As DataColumn In lDS_Unidad_Aux.Tables(LstrNombreTabla).Columns
                    If lstrColumna.ColumnName = lInt_NomCol Then
                        lstrColumna.SetOrdinal(lInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = Sqlcommand.Parameters("@pMsjRetorno").Value.ToString.Trim

            Return lDS_Unidad_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function Insertar(ByRef intIdUnidad As Integer, _
                             ByVal strNombreUnidad As String, _
                             ByVal strNombreCortoUnidad As String, _
                             ByVal strCodigoExternoUnidad As String, _
                             ByVal strEstadoUnidad As String) As String


        Dim Sqlcommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim MiTransaccionSQL As SqlClient.SqlTransaction
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim lstrProcedimiento As String = ""
        Dim lstrRetorno As String = "OK"

        lstrProcedimiento = "Sis_Unidad_Mantencion"
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try

            Sqlcommand = New SqlClient.SqlCommand(lstrProcedimiento, MiTransaccionSQL.Connection, MiTransaccionSQL)
            Sqlcommand.CommandType = CommandType.StoredProcedure
            Sqlcommand.CommandText = lstrProcedimiento
            Sqlcommand.Parameters.Clear()

            Sqlcommand.Parameters.Add("@pCodigoOperacion", SqlDbType.Char, 10).Value = "INSERTAR"

            'IdUnidad...(Identity)
            Dim ParametroIdUsuario As New SqlClient.SqlParameter("@pIdUnidad", SqlDbType.Int)
            ParametroIdUsuario.Direction = Data.ParameterDirection.InputOutput
            Sqlcommand.Parameters.Add(ParametroIdUsuario)

            Sqlcommand.Parameters.Add("@pNombreUnidad", SqlDbType.Char, 50).Value = IIf(strNombreUnidad = "", DBNull.Value, strNombreUnidad)
            Sqlcommand.Parameters.Add("@pNombreCortoUnidad", SqlDbType.Char, 15).Value = IIf(strNombreCortoUnidad = "", DBNull.Value, strNombreCortoUnidad)
            Sqlcommand.Parameters.Add("@pCodigoExternoUnidad", SqlDbType.Char, 20).Value = IIf(strCodigoExternoUnidad = "", DBNull.Value, strCodigoExternoUnidad)
            Sqlcommand.Parameters.Add("@pEstadoUnidad", SqlDbType.Char, 200).Value = IIf(strEstadoUnidad = "", DBNull.Value, strEstadoUnidad)

            Dim oParamSal As New SqlClient.SqlParameter("@pMsjRetorno", SqlDbType.Char, 60)
            oParamSal.Direction = ParameterDirection.Output
            Sqlcommand.Parameters.Add(oParamSal)

            Sqlcommand.ExecuteNonQuery()

            lstrRetorno = Sqlcommand.Parameters("@pMsjRetorno").Value.ToString.Trim
            If lstrRetorno = "OK" Then
                intIdUnidad = Sqlcommand.Parameters("@pIdUnidad").Value

            End If

            If lstrRetorno = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If
            Return lstrRetorno

        Catch ex As Exception
            lstrRetorno = ex.Message
            Return lstrRetorno
        Finally
            SQLConnect.Cerrar()
        End Try

    End Function

    Public Function Modificar(ByVal intIdUnidad As Integer, _
                              ByVal strNombreUnidad As String, _
                              ByVal strNombreCortoUnidad As String, _
                              ByVal strCodigoExternoUnidad As String, _
                              ByVal strEstadoUnidad As String) As String


        Dim Sqlcommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim MiTransaccionSQL As SqlClient.SqlTransaction
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim lstrProcedimiento As String = ""
        Dim lstrRetorno As String = "OK"

        lstrProcedimiento = "Sis_Unidad_Mantencion"
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try

            Sqlcommand = New SqlClient.SqlCommand(lstrProcedimiento, MiTransaccionSQL.Connection, MiTransaccionSQL)
            Sqlcommand.CommandType = CommandType.StoredProcedure
            Sqlcommand.CommandText = lstrProcedimiento
            Sqlcommand.Parameters.Clear()

            Sqlcommand.Parameters.Add("@pCodigoOperacion", SqlDbType.Char, 10).Value = "MODIFICAR"
            Sqlcommand.Parameters.Add("@pIdUnidad", SqlDbType.Char, 50).Value = intIdUnidad
            Sqlcommand.Parameters.Add("@pNombreUnidad", SqlDbType.Char, 50).Value = IIf(strNombreUnidad = "", DBNull.Value, strNombreUnidad)
            Sqlcommand.Parameters.Add("@pNombreCortoUnidad", SqlDbType.Char, 15).Value = IIf(strNombreCortoUnidad = "", DBNull.Value, strNombreCortoUnidad)
            Sqlcommand.Parameters.Add("@pCodigoExternoUnidad", SqlDbType.Char, 20).Value = IIf(strCodigoExternoUnidad = "", DBNull.Value, strCodigoExternoUnidad)
            Sqlcommand.Parameters.Add("@pEstadoUnidad", SqlDbType.Char, 200).Value = IIf(strEstadoUnidad = "", DBNull.Value, strEstadoUnidad)

            Dim oParamSal As New SqlClient.SqlParameter("@pMsjRetorno", SqlDbType.Char, 60)
            oParamSal.Direction = ParameterDirection.Output
            Sqlcommand.Parameters.Add(oParamSal)

            Sqlcommand.ExecuteNonQuery()

            lstrRetorno = Sqlcommand.Parameters("@pMsjRetorno").Value.ToString.Trim
            If lstrRetorno = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If
            Return lstrRetorno

        Catch ex As Exception
            lstrRetorno = ex.Message
            Return lstrRetorno
        Finally
            SQLConnect.Cerrar()
        End Try

    End Function

    Public Function Eliminar(ByVal intIdUnidad As Integer) As String

        Dim Sqlcommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim MiTransaccionSQL As SqlClient.SqlTransaction
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim lstrProcedimiento As String = ""
        Dim lstrRetorno As String = "OK"

        lstrProcedimiento = "Sis_Unidad_Mantencion"
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try

            Sqlcommand = New SqlClient.SqlCommand(lstrProcedimiento, MiTransaccionSQL.Connection, MiTransaccionSQL)
            Sqlcommand.CommandType = CommandType.StoredProcedure
            Sqlcommand.CommandText = lstrProcedimiento
            Sqlcommand.Parameters.Clear()

            Sqlcommand.Parameters.Add("@pCodigoOperacion", SqlDbType.Char, 10).Value = "ELIMINAR"
            Sqlcommand.Parameters.Add("@pIdUnidad", SqlDbType.Char, 50).Value = intIdUnidad

            Dim oParamSal As New SqlClient.SqlParameter("@pMsjRetorno", SqlDbType.Char, 60)
            oParamSal.Direction = ParameterDirection.Output
            Sqlcommand.Parameters.Add(oParamSal)

            Sqlcommand.ExecuteNonQuery()

            lstrRetorno = Sqlcommand.Parameters("@pMsjRetorno").Value.ToString.Trim
            If lstrRetorno = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

            Return lstrRetorno



        Catch ex As Exception
            MiTransaccionSQL.Rollback()
            lstrRetorno = ex.Message
            Return lstrRetorno
        Finally
            SQLConnect.Cerrar()
        End Try

    End Function

End Class
