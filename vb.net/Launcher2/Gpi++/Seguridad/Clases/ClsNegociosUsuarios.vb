﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports Microsoft.VisualBasic
Public Class ClsNegociosUsuarios
    Dim lstrProcedimiento As String = ""
    Public Function RetornaNegocios(ByRef strRetorno As String,
                                    ByVal strColumnas As String) As DataSet

        Dim lDS_Negocios As New DataSet
        Dim lDS_Negocios_Aux As New DataSet

        Dim Sqlcommand As New SqlClient.SqlCommand
        Dim SqlDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim LstrProcedimiento As String = ""
        Dim LstrNombreTabla As String = ""
        Dim lblnRemove As Boolean = True
        Dim larr_NombreColumna() As String = Split(strColumnas, ",")
        Dim lInt_Col As Integer = 0
        Dim lInt_NomCol As String = ""

        LstrProcedimiento = "PKG_EMPRESAS$BUSCAR"
        LstrNombreTabla = "NEGOCIOS"
        Connect.Abrir()

        Try
            Sqlcommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            Sqlcommand.CommandType = CommandType.StoredProcedure
            Sqlcommand.CommandText = LstrProcedimiento
            Sqlcommand.Parameters.Clear()

            Sqlcommand.Parameters.Add("@PID_EMPRESA", SqlDbType.VarChar, 20).Value = DBNull.Value

            SqlDataAdapter.SelectCommand = Sqlcommand
            SqlDataAdapter.Fill(lDS_Negocios, LstrNombreTabla)

            'DS_Auxiliar_aux = DS_Auxiliar
            lDS_Negocios_Aux = lDS_Negocios

            If strColumnas.Trim = "" Then
                lDS_Negocios_Aux = lDS_Negocios
            Else
                lDS_Negocios_Aux = lDS_Negocios.Copy
                For Each lstrColumna As DataColumn In lDS_Negocios.Tables(LstrNombreTabla).Columns
                    lblnRemove = True
                    For lInt_Col = 0 To larr_NombreColumna.Length - 1
                        lInt_NomCol = larr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                        If lstrColumna.ColumnName = lInt_NomCol Then
                            lblnRemove = False
                        End If
                    Next
                    If lblnRemove Then
                        lDS_Negocios_Aux.Tables(LstrNombreTabla).Columns.Remove(lstrColumna.ColumnName)
                    End If
                Next
            End If

            For lInt_Col = 0 To larr_NombreColumna.Length - 1
                lInt_NomCol = larr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                For Each lstrColumna As DataColumn In lDS_Negocios_Aux.Tables(LstrNombreTabla).Columns
                    If lstrColumna.ColumnName = lInt_NomCol Then
                        lstrColumna.SetOrdinal(lInt_Col)
                        Exit For
                    End If
                Next
            Next


            strRetorno = "OK"

            Return lDS_Negocios_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function
    Public Function RetornaNegociosUsuarioConectado(ByRef strRetorno As String, _
                                                    ByVal intIdUsuarioConectado As Integer, _
                                                    ByVal strColumnas As String) As DataSet

        Dim lDS_Negocios As New DataSet
        Dim lDS_Negocios_Aux As New DataSet

        Dim Sqlcommand As New SqlClient.SqlCommand
        Dim SqlDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim LstrProcedimiento As String = ""
        Dim LstrNombreTabla As String = ""
        Dim lblnRemove As Boolean = True
        Dim larr_NombreColumna() As String = Split(strColumnas, ",")
        Dim lInt_Col As Integer = 0
        Dim lInt_NomCol As String = ""

        LstrProcedimiento = "Sis_Negocios_Consultar"
        LstrNombreTabla = "NEGOCIOS"
        Connect.Abrir()

        Try
            Sqlcommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            Sqlcommand.CommandType = CommandType.StoredProcedure
            Sqlcommand.CommandText = LstrProcedimiento
            Sqlcommand.Parameters.Clear()

            Sqlcommand.Parameters.Add("@pCodigoOperacion", SqlDbType.VarChar, 20).Value = "USUARIOCONECTADO"
            Sqlcommand.Parameters.Add("@pIdUsuarioConectado", SqlDbType.Int).Value = intIdUsuarioConectado

            Dim oParamSal As New SqlClient.SqlParameter("@pMsjRetorno", SqlDbType.Char, 60)
            oParamSal.Direction = ParameterDirection.Output
            Sqlcommand.Parameters.Add(oParamSal)


            SqlDataAdapter.SelectCommand = Sqlcommand
            SqlDataAdapter.Fill(lDS_Negocios, LstrNombreTabla)


            'DS_Auxiliar_aux = DS_Auxiliar
            lDS_Negocios_Aux = lDS_Negocios

            If strColumnas.Trim = "" Then
                lDS_Negocios_Aux = lDS_Negocios
            Else
                lDS_Negocios_Aux = lDS_Negocios.Copy
                For Each lstrColumna As DataColumn In lDS_Negocios.Tables(LstrNombreTabla).Columns
                    lblnRemove = True
                    For lInt_Col = 0 To larr_NombreColumna.Length - 1
                        lInt_NomCol = larr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                        If lstrColumna.ColumnName = lInt_NomCol Then
                            lblnRemove = False
                        End If
                    Next
                    If lblnRemove Then
                        lDS_Negocios_Aux.Tables(LstrNombreTabla).Columns.Remove(lstrColumna.ColumnName)
                    End If
                Next
            End If

            For lInt_Col = 0 To larr_NombreColumna.Length - 1
                lInt_NomCol = larr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                For Each lstrColumna As DataColumn In lDS_Negocios_Aux.Tables(LstrNombreTabla).Columns
                    If lstrColumna.ColumnName = lInt_NomCol Then
                        lstrColumna.SetOrdinal(lInt_Col)
                        Exit For
                    End If
                Next
            Next


            strRetorno = "OK"

            Return lDS_Negocios_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function
    Public Function RetornaNegociosUsuario(ByRef strRetorno As String, _
                                           ByVal intIdUsuario As Integer, _
                                           ByVal intIdUsuarioConectado As Integer, _
                                           ByVal strColumnas As String) As DataSet

        Dim lDS_Negocios As New DataSet
        Dim lDS_Negocios_Aux As New DataSet

        Dim Sqlcommand As New SqlClient.SqlCommand
        Dim SqlDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim LstrProcedimiento As String = ""
        Dim LstrNombreTabla As String = ""
        Dim lblnRemove As Boolean = True
        Dim larr_NombreColumna() As String = Split(strColumnas, ",")
        Dim lInt_Col As Integer = 0
        Dim lInt_NomCol As String = ""

        LstrProcedimiento = "Sis_Negocios_Consultar"
        LstrNombreTabla = "NEGOCIOS"
        Connect.Abrir()

        Try
            Sqlcommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            Sqlcommand.CommandType = CommandType.StoredProcedure
            Sqlcommand.CommandText = LstrProcedimiento
            Sqlcommand.Parameters.Clear()

            Sqlcommand.Parameters.Add("@pCodigoOperacion", SqlDbType.VarChar, 20).Value = "USUARIO"
            Sqlcommand.Parameters.Add("@pIdUsuario", SqlDbType.Int).Value = intIdUsuario
            Sqlcommand.Parameters.Add("@pIdUsuarioConectado", SqlDbType.Int).Value = intIdUsuarioConectado

            Dim oParamSal As New SqlClient.SqlParameter("@pMsjRetorno", SqlDbType.Char, 60)
            oParamSal.Direction = ParameterDirection.Output
            Sqlcommand.Parameters.Add(oParamSal)


            SqlDataAdapter.SelectCommand = Sqlcommand
            SqlDataAdapter.Fill(lDS_Negocios, LstrNombreTabla)


            'DS_Auxiliar_aux = DS_Auxiliar
            lDS_Negocios_Aux = lDS_Negocios

            If strColumnas.Trim = "" Then
                lDS_Negocios_Aux = lDS_Negocios
            Else
                lDS_Negocios_Aux = lDS_Negocios.Copy
                For Each lstrColumna As DataColumn In lDS_Negocios.Tables(LstrNombreTabla).Columns
                    lblnRemove = True
                    For lInt_Col = 0 To larr_NombreColumna.Length - 1
                        lInt_NomCol = larr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                        If lstrColumna.ColumnName = lInt_NomCol Then
                            lblnRemove = False
                        End If
                    Next
                    If lblnRemove Then
                        lDS_Negocios_Aux.Tables(LstrNombreTabla).Columns.Remove(lstrColumna.ColumnName)
                    End If
                Next
            End If

            For lInt_Col = 0 To larr_NombreColumna.Length - 1
                lInt_NomCol = larr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                For Each lstrColumna As DataColumn In lDS_Negocios_Aux.Tables(LstrNombreTabla).Columns
                    If lstrColumna.ColumnName = lInt_NomCol Then
                        lstrColumna.SetOrdinal(lInt_Col)
                        Exit For
                    End If
                Next
            Next


            strRetorno = "OK"

            Return lDS_Negocios_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function
    Public Function RetornaNegociosUsuarioPerfil(ByRef strRetorno As String, _
                                                 ByVal intIdUsuario As Integer, _
                                                 ByVal intIdPerfilUsuario As Integer, _
                                                 ByVal strColumnas As String) As DataSet

        Dim lDS_Negocios As New DataSet
        Dim lDS_Negocios_Aux As New DataSet

        Dim Sqlcommand As New SqlClient.SqlCommand
        Dim SqlDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim LstrProcedimiento As String = ""
        Dim LstrNombreTabla As String = ""
        Dim lblnRemove As Boolean = True
        Dim larr_NombreColumna() As String = Split(strColumnas, ",")
        Dim lInt_Col As Integer = 0
        Dim lInt_NomCol As String = ""

        LstrProcedimiento = "Sis_Negocios_Consultar"
        LstrNombreTabla = "NEGOCIOSUSUARIOS"
        Connect.Abrir()

        Try
            Sqlcommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            Sqlcommand.CommandType = CommandType.StoredProcedure
            Sqlcommand.CommandText = LstrProcedimiento
            Sqlcommand.Parameters.Clear()

            Sqlcommand.Parameters.Add("@pCodigoOperacion", SqlDbType.VarChar, 20).Value = "USUARIOPERFIL"
            Sqlcommand.Parameters.Add("@pIdUsuario", SqlDbType.Int).Value = intIdUsuario
            Sqlcommand.Parameters.Add("@pIdPerfilUsuario", SqlDbType.Int).Value = intIdPerfilUsuario

            Dim oParamSal As New SqlClient.SqlParameter("@pMsjRetorno", SqlDbType.Char, 60)
            oParamSal.Direction = ParameterDirection.Output
            Sqlcommand.Parameters.Add(oParamSal)


            SqlDataAdapter.SelectCommand = Sqlcommand
            SqlDataAdapter.Fill(lDS_Negocios, LstrNombreTabla)


            'DS_Auxiliar_aux = DS_Auxiliar
            lDS_Negocios_Aux = lDS_Negocios

            If strColumnas.Trim = "" Then
                lDS_Negocios_Aux = lDS_Negocios
            Else
                lDS_Negocios_Aux = lDS_Negocios.Copy
                For Each lstrColumna As DataColumn In lDS_Negocios.Tables(LstrNombreTabla).Columns
                    lblnRemove = True
                    For lInt_Col = 0 To larr_NombreColumna.Length - 1
                        lInt_NomCol = larr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                        If lstrColumna.ColumnName = lInt_NomCol Then
                            lblnRemove = False
                        End If
                    Next
                    If lblnRemove Then
                        lDS_Negocios_Aux.Tables(LstrNombreTabla).Columns.Remove(lstrColumna.ColumnName)
                    End If
                Next
            End If

            For lInt_Col = 0 To larr_NombreColumna.Length - 1
                lInt_NomCol = larr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                For Each lstrColumna As DataColumn In lDS_Negocios_Aux.Tables(LstrNombreTabla).Columns
                    If lstrColumna.ColumnName = lInt_NomCol Then
                        lstrColumna.SetOrdinal(lInt_Col)
                        Exit For
                    End If
                Next
            Next


            strRetorno = "OK"

            Return lDS_Negocios_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function
    Public Function NegocioValidoUsuario(ByRef strRetorno As String, ByVal IntIdNegocio As Integer, ByVal IntIdUsuario As Integer) As Boolean

        Dim lDS_Negocios As New DataSet
        Dim lDS_Negocios_Aux As New DataSet

        Dim Sqlcommand As New SqlClient.SqlCommand
        Dim SqlDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim LstrProcedimiento As String = ""
        Dim LstrNombreTabla As String = ""

        LstrProcedimiento = "Sis_Negocios_Consultar"
        LstrNombreTabla = "NEGOCIOS"
        Connect.Abrir()

        Try
            Sqlcommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            Sqlcommand.CommandType = CommandType.StoredProcedure
            Sqlcommand.CommandText = LstrProcedimiento
            Sqlcommand.Parameters.Clear()

            Sqlcommand.Parameters.Add("@pCodigoOperacion", SqlDbType.VarChar, 20).Value = "VALIDANEGOCIO"
            Sqlcommand.Parameters.Add("@pIdUsuario", SqlDbType.Int).Value = IntIdUsuario
            Sqlcommand.Parameters.Add("@pIdNegocio", SqlDbType.Int).Value = IntIdNegocio

            Dim oParamSal As New SqlClient.SqlParameter("@pMsjRetorno", SqlDbType.Char, 60)
            oParamSal.Direction = ParameterDirection.Output
            Sqlcommand.Parameters.Add(oParamSal)


            SqlDataAdapter.SelectCommand = Sqlcommand
            SqlDataAdapter.Fill(lDS_Negocios, LstrNombreTabla)

            strRetorno = Sqlcommand.Parameters("@pMsjRetorno").Value.ToString.Trim
            If strRetorno = "OK" Then
                If lDS_Negocios.Tables.Count > 0 Then
                    If lDS_Negocios.Tables("NEGOCIOS").Rows.Count > 0 Then
                        Return True
                    Else
                        Return False
                    End If
                Else
                    Return False
                End If
            Else
                Return False
            End If
            

        Catch ex As Exception
            strRetorno = ex.Message
            Return False
        Finally
            Connect.Cerrar()
        End Try
    End Function
End Class
