Public Class Frm_Login

    Dim lstrTituloFormulario As String = ""
    Dim lstrResultado As String = ""

    Public IntNumeroIntentos As Integer = 1
    Public PasswordArray() As String

    Dim lobjConexion As Cls_Conexion = New Cls_Conexion
    'Dim lobjConexion_Moneda As ClsConexionMoneda = New ClsConexionMoneda
    Dim lobjConexion_ITZ As ClsConexionITZ = New ClsConexionITZ
    'Dim lobjConexionGF As ClsConexionGF = New ClsConexionGF
    'Dim lobjConexionConc As ClsConexionConc = New ClsConexionConc
    'Dim lobjConexionCLTE As ClsConexionCLTE = New ClsConexionCLTE
    Dim lobjUsuario As ClsUsuario = New ClsUsuario
    Dim lobjNegocio As ClsNegociosUsuarios = New ClsNegociosUsuarios
    Dim lobjParametrosSistema As ClsParametrosSistema = New ClsParametrosSistema
    Dim lobjAcciones As ClsAcciones = New ClsAcciones
    Dim lobjPerfilesAcciones As ClsPerfilesAcciones = New ClsPerfilesAcciones
    Dim DS_Usuario As New DataSet
    Dim DS_Negocios As New DataSet
    Dim DS_Acciones As New DataSet
    Public lUsuarioLogueado As String = ""
    Dim lblnCreaModulos As Boolean = False

    ' Leer una clave de un fichero INI

    Private mblnPWD As Boolean = False

    Dim lBlnNegocioValido As Boolean = False

#Region "      ###   Login - Cargar Formulario    ###   "

    Private Sub Frm_Login_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Dim lstrPathArchivoIni As String = Application.StartupPath
            Dim lintIndice As Integer = lstrPathArchivoIni.IndexOf("\bin")
            If lintIndice > -1 Then lstrPathArchivoIni = lstrPathArchivoIni.Substring(0, lintIndice)
            gPathApp = lstrPathArchivoIni
            lstrPathArchivoIni = lstrPathArchivoIni & "\GPI_CONFIG.INI"
            gstrPathArchivoIni = lstrPathArchivoIni
            gLogoCliente = ObtieneValor_ArchivoIni(lstrPathArchivoIni, "EMPRESA", "Cod_Empresa", "")
            gtxtAmbienteSistema = ObtieneValor_ArchivoIni(lstrPathArchivoIni, "AMBIENTEBD", "Ambiente", "")
            gtxtPath_Excel = ObtieneValor_ArchivoIni(lstrPathArchivoIni, "REPOSITORIOS", "Path_Excel", "C:\GPIMAS\Excel")
            gtxtPath_Txt = ObtieneValor_ArchivoIni(lstrPathArchivoIni, "REPOSITORIOS", "Path_Txt", "C:\GPIMAS\Txt")
            gServer = lobjConexion.GetStringDB(lstrPathArchivoIni, "Data Source")
            gDB = lobjConexion.GetStringDB(lstrPathArchivoIni, "Initial Catalog")
            gModoAutenticacion = ObtieneValor_ArchivoIni(lstrPathArchivoIni, "MODO_AUTENTICACION", "Modo_autenticacion", "SQL SERVER")
            gstrCadenaConexion = lobjConexion.GetConnectionString(lstrPathArchivoIni)
            gstrCadenaConexion_ITZ = lobjConexion_ITZ.GetConnectionString_ITZ(lstrPathArchivoIni)
            ''gstrCadenaConexionGF = lobjConexionGF.GetConnectionString(lstrPathArchivoIni)
            'gstrCadenaConexionConc = lobjConexionConc.GetConnectionString(lstrPathArchivoIni)
            'gstrCadenaConexionCLTE = lobjConexionCLTE.GetConnectionString(lstrPathArchivoIni)
            'gstrCadenaConexionMoneda = lobjConexion_Moneda.GetConnectionString_Moneda(lstrPathArchivoIni)

            tslbl_InfoServer.Text = " SVR:" & gServer
            tslbl_InfoDB.Text = "DB:" & gtxtAmbienteSistema
            tslbl_InfoVersion.Text = "V:" & gtxtVersionSistema
            tslbl_InfoFecha.Text = Now

        Catch ex As Exception
            MsgBox("No se puede conectar a BD" & vbCrLf & ex.Message, MsgBoxStyle.Critical, gtxtNombreSistema & " Indetificación")
            End
        End Try

        Try
            'vc Dim lstrAmbienteConexion = gFunExisteAmbienteConexion()
            'Dim lstrAmbienteConexion As String
            'lstrAmbienteConexion = "OK"
            Dim lstrAmbienteConexion = gFunExisteAmbienteConexion()
            If lstrAmbienteConexion <> "OK" Then
                MsgBox(lstrAmbienteConexion, MsgBoxStyle.Critical, gtxtNombreSistema & " Identificación")
                End
            End If

            Dim bmp As Bitmap = Nothing

            'If gstrAmbienteBD = "PRODUCCION" Then
            '    'Me.BackgroundImage = My.Resources.Resources.Fondo_Login
            '    Me.BackColor = Color.SteelBlue
            'Else
            '    'Me.BackgroundImage = My.Resources.Resources.Fondo_Login_Desarrollo
            '    Me.BackColor = Color.White
            'End If

            Pnl_CambioPwd.Top = Pnl_Login.Top
            Pnl_CambioPwd.Left = Pnl_Login.Left
            Pnl_Modulos.Controls.Clear()
            If lstrTituloFormulario = "cambio" Then
                CambioPwd()
            Else
                Pnl_Login.Visible = True
                'ToolStrip1.Visible = True
                Pnl_CambioPwd.Visible = False
            End If

            Call CargaNegocios()
            Me.Txt_NombreUsuario.Focus()

        Catch ex As Exception
            MsgBox("Error : NO se  pudo establecer conexión con la Base de Datos...// " & vbCrLf & vbCrLf &
                   ex.Message, MsgBoxStyle.Critical, gtxtNombreSistema & " Indetificación")
        End Try
    End Sub

    Private Sub Btn_Salir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) 'Handles Btn_Salir.Click
        glngIdUsuario = 0
        Me.Close()
    End Sub

    Private Sub Frm_Login_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Me.Dispose()
    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

    Private Sub CargaNegocios()
        Try
            Dim lstrColumnas As String = "DSC_EMPRESA,ID_EMPRESA"
            DS_Negocios = lobjNegocio.RetornaNegocios(lstrResultado, lstrColumnas)
            If lstrResultado = "OK" Then
                If DS_Negocios.Tables.Count > 0 Then
                    If DS_Negocios.Tables(0).Rows.Count > 0 Then

                        Cmb_Negocio.DataSource = DS_Negocios.Tables(0)


                        Cmb_Negocio.Columns(0).DataField = "DSC_EMPRESA"
                        Cmb_Negocio.Columns(1).DataField = "ID_EMPRESA"
                        Cmb_Negocio.ColumnHeaders = False


                        Cmb_Negocio.Splits(0).DisplayColumns("DSC_EMPRESA").Visible = True
                        Cmb_Negocio.Splits(0).DisplayColumns("ID_EMPRESA").Visible = False
                        Cmb_Negocio.Splits(0).DisplayColumns("DSC_EMPRESA").Width = 180

                        For i = 2 To DS_Negocios.Tables(0).Columns.Count - 3
                            Cmb_Negocio.Splits(i).DisplayColumns(DS_Negocios.Tables(0).Columns.ToString).Visible = False
                        Next

                        'Cmb_Negocio.Splits(0).DisplayColumns("ABR_EMPRESA").Width = 180
                        Cmb_Negocio.SelectedIndex = -1
                        Cmb_Negocio.Text = ""
                    End If
                End If
            Else
                MsgBox("NO se puede obtener Negocios." & vbCrLf & vbCrLf &
                       lstrResultado, MsgBoxStyle.Critical, gtxtNombreSistema & " Indetificación")

            End If
        Catch ex As Exception
            MsgBox("Error en carga de Combo de Negocios u Empresas." & vbCrLf & vbCrLf &
                       lstrResultado, MsgBoxStyle.Critical, gtxtNombreSistema & " Indetificación")
        End Try

    End Sub

    'Private Sub Me_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.GotFocus, Me.Click
    '    Me.BringToFront()
    'End Sub
    Private Sub Me_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If (e.KeyCode = Keys.F4) And e.Modifiers = Keys.Alt Then
            e.Handled = True
            Call Btn_Salir_Click(sender, e)
        ElseIf e.KeyCode = Keys.Enter Then
            e.Handled = True
            SendKeys.Send("{TAB}")
            'ElseIf e.KeyCode = Keys.Escape Then
            '    e.Handled = True
            '    Call Btn_Cancelar_Click(sender, e)
        End If
    End Sub

    Private Sub Me_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.DoubleClick
        Me.Location = New Point(440, 394)
    End Sub

#End Region

#Region "      ###   Login - Autentificación    ###   "

    Private Sub Btn_Aceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_Aceptar.Click
        Dim sEstadoUsuario As String = ""
        Dim sClaveIngresada As String = ""
        Dim sClaveUsuario As String = ""
        Dim Ingresa As Boolean = False
        Dim sRetorno As String = ""
        Dim lintIdNegocio As Integer = 0
        Dim numeroIntentosFallidos = 0
        lBlnNegocioValido = False
        Try
            Cursor = Cursors.WaitCursor

            numeroIntentosFallidos = ClsSeguridad.GetNumeroIntentosFallidos()

            If (Txt_NombreUsuario.Text.Trim.Equals("")) Then
                MsgBox("Ingrese Usuario", , gtxtNombreSistema & " Indetificación")
                Txt_NombreUsuario.Focus()
                Exit Sub
            End If

            If (Txt_Contraseña.Text.Trim.Equals("")) Then
                MsgBox("Ingrese Contraseña", , gtxtNombreSistema & " Indetificación")
                Txt_Contraseña.Focus()
                Exit Sub
            End If

            If Cmb_Negocio.SelectedIndex = -1 Then
                MsgBox("Debe Seleccionar un Negocio", , gtxtNombreSistema & " Indetificación")
                IntNumeroIntentos = 0
                Cmb_Negocio.Focus()
                Exit Sub
            Else
                lintIdNegocio = Cmb_Negocio.Columns(1).CellValue(Cmb_Negocio.SelectedIndex)
            End If


            Try
                DS_Usuario = lobjUsuario.TraerUsuario(sRetorno, Txt_NombreUsuario.Text.ToUpper, Txt_Contraseña.Text.Trim, lintIdNegocio)
                Ingresa = (sRetorno = "OK")
            Catch ex As Exception
                MsgBox("Error no Contemplado " & ex.Message, , gtxtNombreSistema & " Indetificación")
                Exit Sub
            End Try

            If Ingresa Then
                If DS_Usuario.Tables("USUARIO").Rows.Count > 0 Then
                    With DS_Usuario.Tables("USUARIO").Rows(0)
                        glngIdUsuario = .Item("ID_USUARIO")
                        'glngIdUsuario = gintIdUsuarioConectado  '...vcl
                        gtxtNombreUsuario = .Item("DSC_USUARIO").ToString.Trim   '...vcl
                        sEstadoUsuario = .Item("COD_ESTADO").ToString.Trim.ToUpper
                        sClaveUsuario = .Item("PASSWORD").ToString.Trim
                    End With

                    sClaveUsuario = ClsEncriptacion.DesEncriptar(sClaveUsuario)
                    sClaveIngresada = Txt_Contraseña.Text.Trim

                    '...VIG-vigente
                    '...PWD-cambio de clave
                    '...ELI-eliminado
                    '...BLO-Bloqueado
                    '...SIS-????
                    If sEstadoUsuario <> "V" Then
                        If sEstadoUsuario <> "SIS" Then
                            If sEstadoUsuario = "ELI" Then
                                MsgBox("El Usuario No se encuentra en el sistema, favor comuníquese con el administrador.", MsgBoxStyle.Exclamation, gtxtNombreSistema & " Indetificación")
                                glngIdUsuario = 0
                                Exit Sub
                            ElseIf sEstadoUsuario = "BLO" Then
                                MsgBox("El Usuario se encuentra Bloqueado, favor comuníquese con el administrador.", MsgBoxStyle.Exclamation, gtxtNombreSistema & " Indetificación")
                                Exit Sub
                            ElseIf sEstadoUsuario <> "PWD" Then
                                MsgBox("El Usuario No se encuentra Vigente, favor comuníquese con el administrador.", MsgBoxStyle.Exclamation, gtxtNombreSistema & " Indetificación")
                                Exit Sub
                            End If
                        End If
                    End If

                    PasswordArray = sClaveUsuario.Split(" ")
                    If PasswordArray(0).Trim <> sClaveIngresada Then
                        If IntNumeroIntentos >= numeroIntentosFallidos Then
                            BloqueaUsuario()
                            glngIdUsuario = 0
                            Exit Sub
                        Else
                            MsgBox("El nombre de usuario o la contraseña es incorrecta", , gtxtNombreSistema & " Indetificación")
                            Txt_Contraseña.Focus()
                            If (lUsuarioLogueado = "" Or lUsuarioLogueado <> Txt_NombreUsuario.Text.Trim.ToUpper) Then
                                lUsuarioLogueado = Txt_NombreUsuario.Text.Trim.ToUpper
                                IntNumeroIntentos = 2
                            Else
                                IntNumeroIntentos = IntNumeroIntentos + 1
                            End If
                            Exit Sub
                        End If
                    End If

                    lUsuarioLogueado = Txt_NombreUsuario.Text.Trim.ToUpper



                    'lBlnNegocioValido = lobjNegocio.NegocioValidoUsuario(lstrResultado, lintIdNegocio, glngIdUsuario)
                    'If lstrResultado = "OK" Then
                    '    If lBlnNegocioValido Then
                    '        glngNegocioOperacion = lintIdNegocio
                    '        'glngNegocioOperacion = lintIdNegocio '...vcl
                    '        gtxtNombreNegocio = Cmb_Negocio.Columns(0).CellValue(Cmb_Negocio.SelectedIndex).ToString    '...vcl
                    '        gStrAbrNegocio = gtxtNombreNegocio
                    '        'gStrDscNegocio = Cmb_Negocio.Columns(0).CellValue(Cmb_Negocio.SelectedIndex).ToString
                    '    Else
                    '        MsgBox("Usuario No tiene Permisos para el Negocio(" & Cmb_Negocio.SelectedText.Trim & ")", , gtxtNombreSistema & " Indetificación")
                    '    End If
                    'Else
                    '    MsgBox("No se puede validar Negocio", , gtxtNombreSistema & " Indetificación")
                    '    Exit Sub
                    'End If

                    IntNumeroIntentos = 1

                    If sEstadoUsuario = "PWD" Then
                        mblnPWD = True
                        CambioPwd()
                        Exit Sub
                    End If

                    '...Valida Vigencia de la Clave
                    If Not IsDBNull(DS_Usuario.Tables("USUARIO").Rows(0).Item("FECHA_VIGENCIA")) Then
                        If CDate(DS_Usuario.Tables("USUARIO").Rows(0).Item("FECHA_VIGENCIA")) <= CDate(Now) Then
                            MsgBox("Su clave de acceso ha caducado, favor de ingresar una nueva.", MsgBoxStyle.Information, gtxtNombreSistema & " Indetificación")
                            CambioPwd()
                            Exit Sub
                        End If
                    End If

                    ' Obtiene fecha de Sistema
                    'Dim LTxt_ResultadoFecha As String = ""
                    'For LInt_Intentos As Integer = 0 To 4
                    '    LTxt_ResultadoFecha = gFun_BuscarFechaHoraSistema(gstrFechaSistema, gstrHoraSistema)
                    '    If UCase(Trim(LTxt_ResultadoFecha)) = "OK" Then
                    '        Exit For
                    '    End If
                    'Next

                    'If UCase(Trim(LTxt_ResultadoFecha)) = "OK" Then
                    '    Try
                    '        gdteFechaSistema = Format(CDate(gstrFechaSistema), "dd/MM/yyyy")
                    '    Catch ex As Exception
                    '        Dim strFechaAuxiliar As String = ""
                    '        strFechaAuxiliar = gstrFechaSistema.Substring(3, 2) & "/" & gstrFechaSistema.Substring(0, 2) & "/" & gstrFechaSistema.Substring(6, 4)
                    '        gdteFechaSistema = Format(CDate(strFechaAuxiliar), "MM/dd/yyyy")
                    '    End Try
                    'Else
                    '    If LTxt_ResultadoFecha = "SISTEMA DESACTUALIZADO, FAVOR COMUNIQUESE CON SU PROVEEDOR" Then
                    '        MsgBox(LTxt_ResultadoFecha, MsgBoxStyle.Critical, gtxtNombreSistema & " Indetificación")
                    '    Else
                    '        MsgBox("No se pudo leer Fecha y Hora de Sistema, Reingrese al sistema" & vbCrLf & LTxt_ResultadoFecha, MsgBoxStyle.Critical, gtxtNombreSistema & " Indetificación")
                    '    End If
                    '    Exit Sub
                    'End If

                    lUsuarioLogueado = ""

                    '...Registra el Evento "Usuario Conectado"
                    'RegistraConexionSistema()

                    '...Verifica Modulos a los que tiene acceso el usuario, segun el negocio
                    'If lBlnNegocioValido Then
                    '    CrearModulos()
                    'End If
                    'MsgBox("inicio vb6")
                    Dim appPath As String = Application.StartupPath()




                    Try
                        Process.Start(appPath & "\CSBPI.exe", gtxtNombreUsuario & "|" & lintIdNegocio)
                        Me.Close()
                    Catch ex As Exception
                        MsgBox("Error en la ruta de origen: " & ex.Message & "!" & appPath & "\CSBPI.exe", , gtxtNombreSistema & " Identificación")
                        Exit Sub
                    End Try

                Else
                    If IntNumeroIntentos >= numeroIntentosFallidos Then
                        BloqueaUsuario()
                        glngIdUsuario = 0
                        Exit Sub
                    End If
                    IntNumeroIntentos = 0
                    MsgBox("El nombre de usuario es incorrecto ", , gtxtNombreSistema & " Identificación")
                End If
            Else
                If IntNumeroIntentos >= numeroIntentosFallidos Then
                    BloqueaUsuario()
                    glngIdUsuario = 0
                    Exit Sub
                End If
                MsgBox("El nombre de usuario o la contraseña es incorrecta", , gtxtNombreSistema & " Identificación")
            End If

        Catch ex As Exception
            MsgBox("Error no Contemplado " & ex.Message, , gtxtNombreSistema & " Identificación")
            Exit Sub
        Finally
            Me.Cursor = Cursors.Default
        End Try

    End Sub

    Private Sub BloqueaUsuario()
        Dim oUsuario As ClsUsuario = New ClsUsuario
        Dim strRetorno As String = ""

        oUsuario.Bloquear(strRetorno,
                           DS_Usuario.Tables("USUARIO").Rows(0).Item("DSC_USUARIO"))
        MsgBox("Por motivo de seguridad su clave ha sido bloqueda, tras superar" & vbCr & "los intentos de ingreso." & vbCr & "Favor comuníquese con el administrador del sistema.", MsgBoxStyle.Exclamation, gtxtNombreSistema & " Indetificación")

        Txt_Contraseña.Text = String.Empty
    End Sub

    Private Sub Btn_CambiarUsuario_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_CambiarUsuario.Click
        Btn_CambiarUsuario.Visible = False
        Btn_CambiarPasswd.Visible = False

        lblnCreaModulos = False

        '... Registra salida de usuario
        RegistraSalidaSistema()
        Txt_NombreUsuario.Height = 18

        '... Cambia apariencia de Formulario
        Txt_NombreUsuario.Visible = True
        Txt_Contraseña.Visible = True
        Txt_NombreUsuario.Text = ""
        Txt_Contraseña.Text = ""
        Lbl_Login.Visible = False
        Lbl_NombreUsuario.Visible = False
        Lbl_LoginUser.Text = "Usuario"
        Lbl_ClaveUser.Text = "Clave"
        Txt_NombreUsuario.Focus()

        Btn_Aceptar.Visible = True
        Cmb_Negocio.ReadOnly = False
        Cmb_Negocio.TabStop = True

        Btn_CambiarPasswd.Enabled = False
        Btn_CambiarUsuario.Enabled = False

        Pnl_Modulos.Controls.Clear()
        PnlUsuarioLogeado.Visible = False

        glngIdUsuario = 0
        glngNegocioOperacion = 0
        gstrLista = ""
        gStrListaC = ""
        gStrListaR = ""
        gStrListaG = ""
        gstrTipoLista = ""
        gBolReporte = False
        ESC_COM_SEL = False

    End Sub

    Private Sub Btn_CambiarPasswd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_CambiarPasswd.Click

        CambioPwd()

    End Sub

    Function EsConActiveDirectory() As Boolean
        Dim lobjWebConfig As New ClsWebConfig
        Dim sKey As String = ""
        Dim sValue As String = ""
        Dim lValRet As Boolean = True

        If Not lobjWebConfig.GetKeyValue("ldap", sValue) Then
            lValRet = False
        Else
            sValue = sValue.ToLower.Trim
            lValRet = (sValue = "1" Or sValue = "true")
        End If

        lobjWebConfig = Nothing

        Return lValRet

    End Function

    Function EstaAutenticadoLDAP(ByRef sMensaje As String) As Boolean
        ' LDAP://host1/OU=Ventas, DC=Arcadia,DC=COM"

        Dim adAuth As ClsSeguridad
        Dim oWebConfig As New ClsWebConfig
        Dim adPath As String = "" ' "LDAP://CREASYSW2K3" 'Path to your LDAP directory server
        Dim sDominio As String = ""
        Dim sUserName As String = ""
        Dim sPassWord As String = ""

        'sDominio = "Creasys.local"

        sUserName = Txt_NombreUsuario.Text.Trim
        sPassWord = Txt_Contraseña.Text.Trim

        If Not oWebConfig.GetKeyValue("ldaphost", adPath) Then
            EstaAutenticadoLDAP = False
            sMensaje = "Valor de host inexistente, Verifique web.config"
            Return False
        Else
            adPath = "LDAP://" & adPath.Trim
        End If

        If Not oWebConfig.GetKeyValue("ldapdc", sDominio) Then
            EstaAutenticadoLDAP = False
            sMensaje = "Valor de Dominio inexistente, Verifique web.config"
            Return False
        End If

        adAuth = New ClsSeguridad(adPath)

        Try
            If (True = adAuth.IsAuthenticated(sDominio, sUserName, sPassWord)) Then
                EstaAutenticadoLDAP = True
                sMensaje = ""
            Else
                EstaAutenticadoLDAP = False
                sMensaje = "El nombre de usuario o la contraseña es incorrecta."
            End If

        Catch ex As Exception
            sMensaje = ex.Message
            EstaAutenticadoLDAP = False

        End Try

    End Function

    'Private Sub txtContraseña_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Txt_Contraseña.KeyDown
    '    '// Gatilla el Buscar Clientes Cuando pisa enter
    '    If e.KeyCode = Keys.Enter Then
    '        Dim evento As EventArgs = Nothing
    '        Btn_Aceptar_Click(Btn_Aceptar, evento)
    '    End If
    'End Sub

#End Region

#Region "      ###   Login - Cambio de Contraseña   ###      "

    Sub CambioPwd()
        Pnl_Login.Visible = False
        'ToolStrip1.Visible = False
        Pnl_CambioPwd.Height = 176
        Pnl_CambioPwd.Visible = True
        LimpiarPwdNueva()
        txtPwdAnterior.Focus()
    End Sub

    Sub LimpiarPwdNueva()
        txtPwdAnterior.Text = String.Empty
        txtPwdNueva.Text = String.Empty
        txtPwdRetipeada.Text = String.Empty
    End Sub

    Sub LimpiarLogin()
        Txt_Contraseña.Text = ""
        Txt_NombreUsuario.Text = ""
    End Sub

    Private Sub Btn_Volver_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_Volver.Click
        Pnl_Login.Visible = True
        ' If mblnPWD Then
        ' ToolStrip1.Visible = True
        ' End If
        Pnl_CambioPwd.Visible = False
    End Sub

    Private Sub Btn_CambiarClave_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles Btn_CambiarClave.Click
        Me.Cursor = Cursors.WaitCursor
        If GrabaNuevaPwd() Then
            mblnPWD = False
            IntNumeroIntentos = 1
            If lstrTituloFormulario = "cambio" Then
                GrabarLog(TipoRegistroLog.LogAplicacion, 11, "Cambio de contraseña")
                Pnl_Login.Visible = True
                'ToolStrip1.Visible = True
                Pnl_CambioPwd.Visible = False
                Btn_CambiarUsuario_Click(sender, e)
            Else
                Pnl_Login.Visible = True
                'ToolStrip1.Visible = True
                Pnl_CambioPwd.Visible = False
                Txt_NombreUsuario.Focus()
            End If
        End If
        Me.Cursor = Cursors.Default
    End Sub

    Function GrabaNuevaPwd() As Boolean
        Dim oUsuario As ClsUsuario = New ClsUsuario
        Dim nRetorno As Integer = -1
        Dim strRetorno As String = ""
        Dim lValRet As Boolean = False
        Dim titulo = "CAMBIO DE CONTRASEÑA"

        If Campos_Validos() Then
            If Not txtPwdAnterior.Text.Trim.Equals(PasswordArray(0)) Then
                MessageBox.Show("La clave anterior NO coincide, favor ingrésela e inténtelo nuevamente.", titulo, MessageBoxButtons.OK)
                txtPwdAnterior.Text = String.Empty
                Exit Function
            End If

            If txtPwdNueva.Text.Trim <> txtPwdRetipeada.Text.Trim Then
                MessageBox.Show("La clave nueva y la clave reescrita deben ser iguales.", titulo, MessageBoxButtons.OK)
                Exit Function
            End If

            Try
                Dim password = txtPwdNueva.Text.Trim
                Dim resultadoValidacion = ClsSeguridad.ValidarPassword(glngIdUsuario, password)
                If resultadoValidacion = String.Empty Then
                    oUsuario.CambiarPassword(strRetorno, glngIdUsuario, ClsEncriptacion.Encriptar(password))
                    If strRetorno = "OK" Then
                        ' ClsSeguridad.RegistraClaveNueva(glngIdUsuario, password)
                        MsgBox("Contraseña cambiada con exito", MsgBoxStyle.OkOnly, gtxtNombreSistema & " Indetificación")
                        lValRet = True
                    End If
                Else
                    Mostrar_Mensaje("Siga las siguientes recomendaciones para la creación de su nueva contraseña:" &
                                        resultadoValidacion,
                                        "CAMBIO DE CONTRASEÑA",
                                        "Aceptar",
                                        "",
                                        "ALERTA",
                                        False,
                                        False,
                                        "")

                    'txtPwdAnterior.Text = ""
                    txtPwdNueva.Text = ""
                    txtPwdRetipeada.Text = ""
                End If
            Catch ex As Exception
                Mostrar_Mensaje(ex.Message, "CAMBIO DE CONTRASEÑA", "Aceptar", "", "ALERTA", False, False, "")
            Finally
            End Try
            'oUsuario.Dispose()
            oUsuario = Nothing
        Else
            Mostrar_Mensaje("Todos los Campos son Obligatorios", "CAMBIO DE CONTRASEÑA", "Aceptar", "", "ALERTA", False, False, "")
        End If
        Return lValRet
    End Function

    Function Campos_Validos() As Boolean
        Dim Ingreso_valido As Boolean = True
        If txtPwdAnterior.Text.Trim = "" Then
            Ingreso_valido = False
        End If
        If txtPwdNueva.Text.Trim = "" Then
            Ingreso_valido = False
        End If
        If txtPwdRetipeada.Text.Trim = "" Then
            Ingreso_valido = False
        End If
        Return Ingreso_valido
    End Function

    Sub Mostrar_Mensaje(ByVal Mensaje As String, ByVal Titulo As String, ByVal Text_BotonAceptar As String, ByVal Text_BotonCancelar As String, ByVal Tipo_Mensaje As String, ByVal Btn_Cancelar_Visible As Boolean, ByVal Pnl_Detalle_Visible As Boolean, ByVal Detalle_Mensaje As String)
        MsgBox(Mensaje, MsgBoxStyle.Critical, Titulo)
    End Sub

#End Region

#Region "      ###   Login - Carga de MDI     ###   "

    Private Sub Btn_EscComercial_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        'Dim lobjFormulario As New Frm_MDIPrincipal_EscCom
        'Me.Hide()
        'gblnBackOffice = False
        'gblnLimites = False
        'gblnEscritorioRemoto = True
        'gblnConta = False
        'ESC_COM_SEL = True
        'Try
        '    lobjFormulario.Show() '    CargarFormulario()
        '    'lobjFormulario.WindowState = FormWindowState.Maximized
        'Catch ex As Exception
        '    MsgBox("No se puede Abrir Módulo" & vbCrLf & ex.Message, MsgBoxStyle.Critical, gtxtNombreSistema & " Indentificación")
        '    Me.Close()
        'End Try

    End Sub

    Private Sub Btn_BackOffice_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim lobjFormulario As New Frm_Principal
        Dim lintIdNegocio As Integer
        Me.Hide()
        gblnBackOffice = True
        gblnLimites = False
        gblnEscritorioRemoto = False
        gblnConta = False
        ESC_COM_SEL = False
        lintIdNegocio = Cmb_Negocio.Columns(1).CellValue(Cmb_Negocio.SelectedIndex)
        glngNegocioOperacion = lintIdNegocio
        gtxtNombreNegocio = Cmb_Negocio.Columns(0).CellValue(Cmb_Negocio.SelectedIndex).ToString
        Try
            lobjFormulario.Show()
        Catch ex As Exception
            MsgBox("No se puede Abrir Módulo" & vbCrLf & ex.Message, MsgBoxStyle.Critical, gtxtNombreSistema & " Indentificación")
            Me.Close()
        End Try
    End Sub

    Private Sub Btn_Limites_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If Not lBlnNegocioValido Then
            MsgBox("Usuario No tiene Permisos para el Negocio ( " & Cmb_Negocio.SelectedText.Trim & " ).", , gtxtNombreSistema & " Indetificación")
            Exit Sub
        End If
        'Dim lobjFormulario As New Frm_MDIPrincipal_Lim
        'Me.Hide()
        'gblnBackOffice = False
        'gblnEscritorioRemoto = False
        'gblnLimites = True
        'gblnConta = False
        'ESC_COM_SEL = False
        Try
            'lobjFormulario.Show()
            MsgBox("SE ABRE EL VB6")
        Catch ex As Exception
            MsgBox("No se puede Abrir Módulo" & vbCrLf & ex.Message, MsgBoxStyle.Critical, gtxtNombreSistema & " Indentificación")
            Me.Close()
        End Try
    End Sub
#End Region

    'Public Function InitCap(ByVal strCadena As String) As String
    '    Dim lstrNuevaCadena As String = ""
    '    If strCadena <> "" Then
    '        Dim lArrCadena() As String = Split(strCadena, " ")
    '        Dim lstrPalabra As String = ""

    '        For lintPalabra As Integer = 0 To lArrCadena.Length - 1
    '            lstrPalabra = lArrCadena(lintPalabra)
    '            lstrPalabra = UCase(lstrPalabra.Substring(0, 1)) & LCase(lstrPalabra.Substring(1))
    '            lstrNuevaCadena = lstrNuevaCadena & "" & lstrPalabra
    '        Next
    '        Return lstrNuevaCadena.Trim
    '    Else
    '        Return strCadena
    '    End If

    'End Function

    Private Sub Cmb_Negocio_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cmb_Negocio.TextChanged
        Dim lintIdNegocio As Integer
        lBlnNegocioValido = False
        If Cmb_Negocio.SelectedIndex = -1 Then
            Pnl_Modulos.Controls.Clear()
            Exit Sub
        Else
            lintIdNegocio = Cmb_Negocio.Columns(1).CellValue(Cmb_Negocio.SelectedIndex)
            gtxtNombreNegocio = Cmb_Negocio.Columns(0).CellValue(Cmb_Negocio.SelectedIndex)
            gStrAbrNegocio = gtxtNombreNegocio
        End If
        lBlnNegocioValido = lobjNegocio.NegocioValidoUsuario(lstrResultado, lintIdNegocio, glngIdUsuario)
        If Not lblnCreaModulos Then Exit Sub

        If lstrResultado = "OK" Then
            If lBlnNegocioValido Then
                glngNegocioOperacion = lintIdNegocio
            Else
                MsgBox("Usuario No tiene Permisos para el Negocio ( " & Cmb_Negocio.SelectedText.Trim & " ).", , gtxtNombreSistema & " Indetificación")
            End If
        Else
            MsgBox("No se puede validar Negocio", , gtxtNombreSistema & " Indetificación")
            Exit Sub
        End If

        If Not lBlnNegocioValido Then Exit Sub

        Pnl_Modulos.Controls.Clear()
        Call CrearModulos()
    End Sub

    Private Sub CrearModulos()
        Dim lintLeft As Integer = 10
        Dim lintAncho As Integer = 90
        Dim lintAlto As Integer = 21
        Dim lintTop As Integer = 0
        lblnCreaModulos = False

        DS_Acciones = lobjPerfilesAcciones.TraeModulos(lstrResultado, glngIdUsuario, glngNegocioOperacion)
        If lstrResultado <> "OK" Then
            MsgBox("No se  pudo obtener los Módulos Habilitados para el Usuario." & vbCrLf & lstrResultado, MsgBoxStyle.Critical, gtxtNombreSistema & " Indetificación")
        Else
            If DS_Acciones.Tables(0).Rows.Count = 0 Then
                MsgBox("Usuario No tiene Acceso a Modulos en el Negocio [" & Cmb_Negocio.SelectedText & "]" & vbCrLf & lstrResultado, MsgBoxStyle.Critical, gtxtNombreSistema & " Indetificación")
                IntNumeroIntentos = IntNumeroIntentos - 1
                Exit Sub
            Else
                '.....  lintPosicion = (397 / 2) - (Math.Pow((lintAncho / 2), DS_Acciones.Tables(0).Rows.Count) + 1)   ' vcl...

                'lintLeft = (Pnl_Modulos.Width / (DS_Acciones.Tables(0).Rows.Count + 2)) - (lintAncho / 3)   ' vcl...
                lintLeft = Math.Truncate((Pnl_Modulos.Width - (2 * (DS_Acciones.Tables(0).Rows.Count - 1) + (DS_Acciones.Tables(0).Rows.Count * lintAncho))) / 2)

            End If
        End If
        'ToolStrip1.Visible = False
        Pnl_CambioPwd.Height = 176
        Pnl_Login.Height = Pnl_CambioPwd.Height

        '...Cambia apariencia de Formulario según datos de usuario
        PnlUsuarioLogeado.Visible = True
        PnlUsuarioLogeado.BringToFront()
        Txt_NombreUsuario.Visible = False
        Txt_Contraseña.Visible = False

        Lbl_Login.Visible = True
        Lbl_NombreUsuario.Visible = True
        Lbl_Login.Text = DS_Usuario.Tables("USUARIO").Rows(0).Item("LOGIN_USUARIO").ToString.Trim.ToUpper
        Lbl_NombreUsuario.Text = DS_Usuario.Tables("USUARIO").Rows(0).Item("NOMBRE_USUARIO").ToString.Trim
        Lbl_LoginUser.Text = "Usuario Conectado"
        Lbl_ClaveUser.Text = "Nombre Usuario"

        Btn_CambiarUsuario.Enabled = True
        Btn_CambiarPasswd.Enabled = True
        Btn_Aceptar.Visible = False
        Cmb_Negocio.ReadOnly = False
        Cmb_Negocio.TabStop = False

        Btn_CambiarUsuario.Visible = True
        Btn_CambiarPasswd.Visible = True

        Pnl_Modulos.Controls.Clear()
        Pnl_Modulos.TabStop = True

        For Each DR_FILA As DataRow In DS_Acciones.Tables(0).Rows
            Dim Btn_Modulo As New C1.Win.C1Input.C1Button With {
                .Name = DR_FILA("NOMBRE_CONTROL").ToString.Trim,
                .Text = "&" & DR_FILA("DESCRIPCION_ITEM").ToString.Trim,
                .VisualStyle = C1.Win.C1Input.VisualStyle.Office2007Silver,
                .Top = lintTop,
                .Left = lintLeft,
                .Height = lintAlto,
                .Width = lintAncho,
                .TabStop = True,
                .Parent = Pnl_Modulos,
                .TabIndex = Pnl_Modulos.TabIndex + 100,
                .Tag = DR_FILA("SUB_PANTALLA").ToString.Trim
            }

            Select Case DR_FILA("NUMERO_LINEA")
                Case 1
                    AddHandler Btn_Modulo.Click, AddressOf Btn_EscComercial_Click
                    ' Pnl_Modulos.Controls(Pnl_Modulos.Controls.Count - 1).Focus()
                    'Pnl_Modulos.Controls(Pnl_Modulos.Controls.Count - 1).Enabled = False                    '  SE DESHABILITA BOTON ESCRITORIO en GPI++PLUSMASPLUSPLUS MAS !!!
                Case 2
                    AddHandler Btn_Modulo.Click, AddressOf Btn_BackOffice_Click
                    ' Pnl_Modulos.Controls(Pnl_Modulos.Controls.Count - 1).Focus()
                Case 3
                    AddHandler Btn_Modulo.Click, AddressOf Btn_Limites_Click
                    Pnl_Modulos.Controls(Pnl_Modulos.Controls.Count - 1).Focus()
            End Select

            Pnl_Modulos.Controls.Add(Btn_Modulo)
            lintLeft = lintLeft + lintAncho + 2
        Next
        lblnCreaModulos = True
    End Sub

End Class