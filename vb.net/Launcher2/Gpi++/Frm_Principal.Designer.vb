﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Frm_Principal
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Frm_Principal))
        Me.OperacionesToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem
        Me.ConsultaOperacionesToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem
        Me.SistemaToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem
        Me.MonedasToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem
        Me.InstrumentosToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem2 = New System.Windows.Forms.ToolStripSeparator
        Me.EmpresasToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem
        Me.ClientesToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem
        Me.BanquerosToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem
        Me.TipoDeCambioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ArchivoToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem
        Me.CambiarDeUsuarioToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem
        Me.SalirToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem
        Me.AyudaToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem
        Me.AcercaDeFINAMToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.TemasDeAyudaToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem
        Me.ProcesosToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem
        Me.CarteraToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.RentabilidadToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem
        Me.btnOperacionesMenuProcesos = New System.Windows.Forms.ToolStripMenuItem
        Me.ReporteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.PlanDeCuentasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.PlantillasContablesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.CentralizaciónToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.AuxiliaresToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.LibroDiarioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ValorizaciónToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.RentaFijaSeriadosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.IntermediaciónToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripStatusLabel = New System.Windows.Forms.ToolStripStatusLabel
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.Label1 = New System.Windows.Forms.Label
        Me.C1PictureBox1 = New C1.Win.C1Input.C1PictureBox
        Me.C1PictureBox2 = New C1.Win.C1Input.C1PictureBox
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.lblArgumentos = New System.Windows.Forms.Label
        Me.btnMainClientes = New System.Windows.Forms.ToolStripButton
        Me.btnMainCuentas = New System.Windows.Forms.ToolStripButton
        Me.btnMainAportes = New System.Windows.Forms.ToolStripButton
        Me.btnCalculadora = New System.Windows.Forms.ToolStripButton
        Me.btnCargaExcel = New System.Windows.Forms.ToolStripButton
        Me.btnCargaWord = New System.Windows.Forms.ToolStripButton
        Me.objMenuPrincipal = New System.Windows.Forms.MenuStrip
        Me.tmr_FechaSrv = New System.Windows.Forms.Timer(Me.components)
        Me.Panel2.SuspendLayout()
        CType(Me.C1PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C1PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'OperacionesToolStripMenuItem1
        '
        Me.OperacionesToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ConsultaOperacionesToolStripMenuItem1, Me.SistemaToolStripMenuItem1, Me.ToolStripMenuItem2, Me.EmpresasToolStripMenuItem1, Me.ClientesToolStripMenuItem1, Me.BanquerosToolStripMenuItem1, Me.TipoDeCambioToolStripMenuItem})
        Me.OperacionesToolStripMenuItem1.ForeColor = System.Drawing.Color.White
        Me.OperacionesToolStripMenuItem1.Name = "OperacionesToolStripMenuItem1"
        Me.OperacionesToolStripMenuItem1.Size = New System.Drawing.Size(79, 20)
        Me.OperacionesToolStripMenuItem1.Text = "&Operaciones"
        '
        'ConsultaOperacionesToolStripMenuItem1
        '
        Me.ConsultaOperacionesToolStripMenuItem1.Name = "ConsultaOperacionesToolStripMenuItem1"
        Me.ConsultaOperacionesToolStripMenuItem1.Size = New System.Drawing.Size(204, 22)
        Me.ConsultaOperacionesToolStripMenuItem1.Text = "Consulta de Movimientos"
        '
        'SistemaToolStripMenuItem1
        '
        Me.SistemaToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MonedasToolStripMenuItem1, Me.InstrumentosToolStripMenuItem1})
        Me.SistemaToolStripMenuItem1.Name = "SistemaToolStripMenuItem1"
        Me.SistemaToolStripMenuItem1.Size = New System.Drawing.Size(204, 22)
        Me.SistemaToolStripMenuItem1.Text = "Sistema"
        '
        'MonedasToolStripMenuItem1
        '
        Me.MonedasToolStripMenuItem1.Name = "MonedasToolStripMenuItem1"
        Me.MonedasToolStripMenuItem1.Size = New System.Drawing.Size(149, 22)
        Me.MonedasToolStripMenuItem1.Text = "Monedas"
        '
        'InstrumentosToolStripMenuItem1
        '
        Me.InstrumentosToolStripMenuItem1.Name = "InstrumentosToolStripMenuItem1"
        Me.InstrumentosToolStripMenuItem1.Size = New System.Drawing.Size(149, 22)
        Me.InstrumentosToolStripMenuItem1.Text = "Instrumentos"
        '
        'ToolStripMenuItem2
        '
        Me.ToolStripMenuItem2.Name = "ToolStripMenuItem2"
        Me.ToolStripMenuItem2.Size = New System.Drawing.Size(201, 6)
        '
        'EmpresasToolStripMenuItem1
        '
        Me.EmpresasToolStripMenuItem1.Name = "EmpresasToolStripMenuItem1"
        Me.EmpresasToolStripMenuItem1.Size = New System.Drawing.Size(204, 22)
        Me.EmpresasToolStripMenuItem1.Text = "Empresas"
        '
        'ClientesToolStripMenuItem1
        '
        Me.ClientesToolStripMenuItem1.Name = "ClientesToolStripMenuItem1"
        Me.ClientesToolStripMenuItem1.Size = New System.Drawing.Size(204, 22)
        Me.ClientesToolStripMenuItem1.Text = "Clientes"
        '
        'BanquerosToolStripMenuItem1
        '
        Me.BanquerosToolStripMenuItem1.Name = "BanquerosToolStripMenuItem1"
        Me.BanquerosToolStripMenuItem1.Size = New System.Drawing.Size(204, 22)
        Me.BanquerosToolStripMenuItem1.Text = "Banqueros"
        '
        'TipoDeCambioToolStripMenuItem
        '
        Me.TipoDeCambioToolStripMenuItem.Name = "TipoDeCambioToolStripMenuItem"
        Me.TipoDeCambioToolStripMenuItem.Size = New System.Drawing.Size(204, 22)
        Me.TipoDeCambioToolStripMenuItem.Text = "Tipo de Cambio"
        '
        'ArchivoToolStripMenuItem1
        '
        Me.ArchivoToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CambiarDeUsuarioToolStripMenuItem1, Me.SalirToolStripMenuItem1})
        Me.ArchivoToolStripMenuItem1.ForeColor = System.Drawing.Color.White
        Me.ArchivoToolStripMenuItem1.Name = "ArchivoToolStripMenuItem1"
        Me.ArchivoToolStripMenuItem1.Size = New System.Drawing.Size(74, 20)
        Me.ArchivoToolStripMenuItem1.Text = "&Mantención"
        '
        'CambiarDeUsuarioToolStripMenuItem1
        '
        Me.CambiarDeUsuarioToolStripMenuItem1.Name = "CambiarDeUsuarioToolStripMenuItem1"
        Me.CambiarDeUsuarioToolStripMenuItem1.Size = New System.Drawing.Size(177, 22)
        Me.CambiarDeUsuarioToolStripMenuItem1.Text = "Cambiar de usuario"
        '
        'SalirToolStripMenuItem1
        '
        Me.SalirToolStripMenuItem1.Name = "SalirToolStripMenuItem1"
        Me.SalirToolStripMenuItem1.Size = New System.Drawing.Size(177, 22)
        Me.SalirToolStripMenuItem1.Text = "Salir"
        '
        'AyudaToolStripMenuItem1
        '
        Me.AyudaToolStripMenuItem1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.AyudaToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AcercaDeFINAMToolStripMenuItem, Me.TemasDeAyudaToolStripMenuItem1})
        Me.AyudaToolStripMenuItem1.ForeColor = System.Drawing.Color.White
        Me.AyudaToolStripMenuItem1.Name = "AyudaToolStripMenuItem1"
        Me.AyudaToolStripMenuItem1.Size = New System.Drawing.Size(50, 20)
        Me.AyudaToolStripMenuItem1.Text = "Ayuda"
        '
        'AcercaDeFINAMToolStripMenuItem
        '
        Me.AcercaDeFINAMToolStripMenuItem.Name = "AcercaDeFINAMToolStripMenuItem"
        Me.AcercaDeFINAMToolStripMenuItem.Size = New System.Drawing.Size(167, 22)
        Me.AcercaDeFINAMToolStripMenuItem.Text = "Acerca de Gpi++"
        '
        'TemasDeAyudaToolStripMenuItem1
        '
        Me.TemasDeAyudaToolStripMenuItem1.Name = "TemasDeAyudaToolStripMenuItem1"
        Me.TemasDeAyudaToolStripMenuItem1.Size = New System.Drawing.Size(167, 22)
        Me.TemasDeAyudaToolStripMenuItem1.Text = "Temas de ayuda"
        '
        'ProcesosToolStripMenuItem1
        '
        Me.ProcesosToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CarteraToolStripMenuItem, Me.RentabilidadToolStripMenuItem1, Me.btnOperacionesMenuProcesos})
        Me.ProcesosToolStripMenuItem1.ForeColor = System.Drawing.Color.White
        Me.ProcesosToolStripMenuItem1.Name = "ProcesosToolStripMenuItem1"
        Me.ProcesosToolStripMenuItem1.Size = New System.Drawing.Size(62, 20)
        Me.ProcesosToolStripMenuItem1.Text = "&Procesos"
        '
        'CarteraToolStripMenuItem
        '
        Me.CarteraToolStripMenuItem.Name = "CarteraToolStripMenuItem"
        Me.CarteraToolStripMenuItem.Size = New System.Drawing.Size(145, 22)
        Me.CarteraToolStripMenuItem.Text = "Cartera"
        '
        'RentabilidadToolStripMenuItem1
        '
        Me.RentabilidadToolStripMenuItem1.Name = "RentabilidadToolStripMenuItem1"
        Me.RentabilidadToolStripMenuItem1.Size = New System.Drawing.Size(145, 22)
        Me.RentabilidadToolStripMenuItem1.Text = "Rentabilidad"
        '
        'btnOperacionesMenuProcesos
        '
        Me.btnOperacionesMenuProcesos.Name = "btnOperacionesMenuProcesos"
        Me.btnOperacionesMenuProcesos.Size = New System.Drawing.Size(145, 22)
        Me.btnOperacionesMenuProcesos.Text = "Operaciones"
        '
        'ReporteToolStripMenuItem
        '
        Me.ReporteToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PlanDeCuentasToolStripMenuItem, Me.PlantillasContablesToolStripMenuItem, Me.CentralizaciónToolStripMenuItem, Me.AuxiliaresToolStripMenuItem})
        Me.ReporteToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me.ReporteToolStripMenuItem.Name = "ReporteToolStripMenuItem"
        Me.ReporteToolStripMenuItem.Size = New System.Drawing.Size(63, 20)
        Me.ReporteToolStripMenuItem.Text = "&Reportes"
        '
        'PlanDeCuentasToolStripMenuItem
        '
        Me.PlanDeCuentasToolStripMenuItem.Name = "PlanDeCuentasToolStripMenuItem"
        Me.PlanDeCuentasToolStripMenuItem.Size = New System.Drawing.Size(177, 22)
        Me.PlanDeCuentasToolStripMenuItem.Text = "Plan de Cuentas"
        '
        'PlantillasContablesToolStripMenuItem
        '
        Me.PlantillasContablesToolStripMenuItem.Name = "PlantillasContablesToolStripMenuItem"
        Me.PlantillasContablesToolStripMenuItem.Size = New System.Drawing.Size(177, 22)
        Me.PlantillasContablesToolStripMenuItem.Text = "Plantillas Contables"
        '
        'CentralizaciónToolStripMenuItem
        '
        Me.CentralizaciónToolStripMenuItem.Name = "CentralizaciónToolStripMenuItem"
        Me.CentralizaciónToolStripMenuItem.Size = New System.Drawing.Size(177, 22)
        Me.CentralizaciónToolStripMenuItem.Text = "Centralización"
        '
        'AuxiliaresToolStripMenuItem
        '
        Me.AuxiliaresToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.LibroDiarioToolStripMenuItem})
        Me.AuxiliaresToolStripMenuItem.Name = "AuxiliaresToolStripMenuItem"
        Me.AuxiliaresToolStripMenuItem.Size = New System.Drawing.Size(177, 22)
        Me.AuxiliaresToolStripMenuItem.Text = "Auxiliares"
        '
        'LibroDiarioToolStripMenuItem
        '
        Me.LibroDiarioToolStripMenuItem.Name = "LibroDiarioToolStripMenuItem"
        Me.LibroDiarioToolStripMenuItem.Size = New System.Drawing.Size(138, 22)
        Me.LibroDiarioToolStripMenuItem.Text = "Libro Diario"
        '
        'ValorizaciónToolStripMenuItem
        '
        Me.ValorizaciónToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.RentaFijaSeriadosToolStripMenuItem, Me.IntermediaciónToolStripMenuItem})
        Me.ValorizaciónToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me.ValorizaciónToolStripMenuItem.Name = "ValorizaciónToolStripMenuItem"
        Me.ValorizaciónToolStripMenuItem.Size = New System.Drawing.Size(75, 20)
        Me.ValorizaciónToolStripMenuItem.Text = "&Valorización"
        '
        'RentaFijaSeriadosToolStripMenuItem
        '
        Me.RentaFijaSeriadosToolStripMenuItem.Name = "RentaFijaSeriadosToolStripMenuItem"
        Me.RentaFijaSeriadosToolStripMenuItem.Size = New System.Drawing.Size(178, 22)
        Me.RentaFijaSeriadosToolStripMenuItem.Text = "Renta Fija Seriados"
        '
        'IntermediaciónToolStripMenuItem
        '
        Me.IntermediaciónToolStripMenuItem.Name = "IntermediaciónToolStripMenuItem"
        Me.IntermediaciónToolStripMenuItem.Size = New System.Drawing.Size(178, 22)
        Me.IntermediaciónToolStripMenuItem.Text = "Intermediación"
        '
        'ToolStripStatusLabel
        '
        Me.ToolStripStatusLabel.Name = "ToolStripStatusLabel"
        Me.ToolStripStatusLabel.Size = New System.Drawing.Size(40, 17)
        Me.ToolStripStatusLabel.Text = "Estado"
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.Silver
        Me.Panel2.Controls.Add(Me.Label1)
        Me.Panel2.Controls.Add(Me.C1PictureBox1)
        Me.Panel2.Controls.Add(Me.C1PictureBox2)
        Me.Panel2.Location = New System.Drawing.Point(0, 1)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(1019, 51)
        Me.Panel2.TabIndex = 8
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.Desktop
        Me.Label1.Location = New System.Drawing.Point(341, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(336, 19)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "GESTIÓN DE PORTAFOLIO DE INVERSIÓN"
        '
        'C1PictureBox1
        '
        Me.C1PictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.C1PictureBox1.Location = New System.Drawing.Point(0, 0)
        Me.C1PictureBox1.Name = "C1PictureBox1"
        Me.C1PictureBox1.Size = New System.Drawing.Size(222, 49)
        Me.C1PictureBox1.TabIndex = 0
        Me.C1PictureBox1.TabStop = False
        '
        'C1PictureBox2
        '
        Me.C1PictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.C1PictureBox2.Image = CType(resources.GetObject("C1PictureBox2.Image"), System.Drawing.Image)
        Me.C1PictureBox2.Location = New System.Drawing.Point(797, 0)
        Me.C1PictureBox2.Name = "C1PictureBox2"
        Me.C1PictureBox2.Size = New System.Drawing.Size(222, 49)
        Me.C1PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.C1PictureBox2.TabIndex = 1
        Me.C1PictureBox2.TabStop = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.White
        Me.Panel1.BackgroundImage = CType(resources.GetObject("Panel1.BackgroundImage"), System.Drawing.Image)
        Me.Panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.Panel1.Controls.Add(Me.lblArgumentos)
        Me.Panel1.Location = New System.Drawing.Point(0, 50)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1018, 692)
        Me.Panel1.TabIndex = 6
        '
        'lblArgumentos
        '
        Me.lblArgumentos.AutoSize = True
        Me.lblArgumentos.Location = New System.Drawing.Point(181, 81)
        Me.lblArgumentos.Name = "lblArgumentos"
        Me.lblArgumentos.Size = New System.Drawing.Size(0, 13)
        Me.lblArgumentos.TabIndex = 0
        Me.lblArgumentos.Visible = False
        '
        'btnMainClientes
        '
        Me.btnMainClientes.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnMainClientes.Image = CType(resources.GetObject("btnMainClientes.Image"), System.Drawing.Image)
        Me.btnMainClientes.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMainClientes.Name = "btnMainClientes"
        Me.btnMainClientes.Size = New System.Drawing.Size(23, 22)
        Me.btnMainClientes.Text = "Clientes"
        '
        'btnMainCuentas
        '
        Me.btnMainCuentas.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnMainCuentas.Image = CType(resources.GetObject("btnMainCuentas.Image"), System.Drawing.Image)
        Me.btnMainCuentas.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMainCuentas.Name = "btnMainCuentas"
        Me.btnMainCuentas.Size = New System.Drawing.Size(23, 22)
        Me.btnMainCuentas.Text = "Cuentas de Inversión"
        '
        'btnMainAportes
        '
        Me.btnMainAportes.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnMainAportes.Image = CType(resources.GetObject("btnMainAportes.Image"), System.Drawing.Image)
        Me.btnMainAportes.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMainAportes.Name = "btnMainAportes"
        Me.btnMainAportes.Size = New System.Drawing.Size(23, 22)
        Me.btnMainAportes.Text = "Aportes a Cuentas"
        '
        'btnCalculadora
        '
        Me.btnCalculadora.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.btnCalculadora.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnCalculadora.Image = CType(resources.GetObject("btnCalculadora.Image"), System.Drawing.Image)
        Me.btnCalculadora.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnCalculadora.Name = "btnCalculadora"
        Me.btnCalculadora.Size = New System.Drawing.Size(23, 22)
        Me.btnCalculadora.Text = "Calculadora"
        '
        'btnCargaExcel
        '
        Me.btnCargaExcel.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.btnCargaExcel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnCargaExcel.Image = CType(resources.GetObject("btnCargaExcel.Image"), System.Drawing.Image)
        Me.btnCargaExcel.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnCargaExcel.Name = "btnCargaExcel"
        Me.btnCargaExcel.Size = New System.Drawing.Size(23, 22)
        Me.btnCargaExcel.Text = "MS Excel"
        '
        'btnCargaWord
        '
        Me.btnCargaWord.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.btnCargaWord.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnCargaWord.Image = CType(resources.GetObject("btnCargaWord.Image"), System.Drawing.Image)
        Me.btnCargaWord.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnCargaWord.Name = "btnCargaWord"
        Me.btnCargaWord.Size = New System.Drawing.Size(23, 22)
        Me.btnCargaWord.Text = "MS Word"
        '
        'objMenuPrincipal
        '
        Me.objMenuPrincipal.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.objMenuPrincipal.Location = New System.Drawing.Point(0, 0)
        Me.objMenuPrincipal.Name = "objMenuPrincipal"
        Me.objMenuPrincipal.Size = New System.Drawing.Size(1017, 24)
        Me.objMenuPrincipal.TabIndex = 2
        Me.objMenuPrincipal.Text = "Menu principal"
        '
        'tmr_FechaSrv
        '
        Me.tmr_FechaSrv.Enabled = True
        Me.tmr_FechaSrv.Interval = 300000
        '
        'Frm_Principal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1017, 744)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.objMenuPrincipal)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.IsMdiContainer = True
        Me.MinimumSize = New System.Drawing.Size(1023, 732)
        Me.Name = "Frm_Principal"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Gpi+ 2.0  (Back Office)"
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.C1PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C1PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents OperacionesToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConsultaOperacionesToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SistemaToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MonedasToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InstrumentosToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents EmpresasToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ClientesToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BanquerosToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TipoDeCambioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ArchivoToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CambiarDeUsuarioToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SalirToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AyudaToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AcercaDeFINAMToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TemasDeAyudaToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ProcesosToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CarteraToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RentabilidadToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnOperacionesMenuProcesos As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReporteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PlanDeCuentasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PlantillasContablesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CentralizaciónToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AuxiliaresToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LibroDiarioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ValorizaciónToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RentaFijaSeriadosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents IntermediaciónToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnMainClientes As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMainCuentas As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMainAportes As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnCalculadora As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnCargaExcel As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnCargaWord As System.Windows.Forms.ToolStripButton
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents ToolStripStatusLabel As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents lblArgumentos As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents C1PictureBox2 As C1.Win.C1Input.C1PictureBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents objMenuPrincipal As System.Windows.Forms.MenuStrip
    Friend WithEvents C1PictureBox1 As C1.Win.C1Input.C1PictureBox
    Friend WithEvents tmr_FechaSrv As System.Windows.Forms.Timer
End Class
