<body style="background-color: #efefef; padding:30px">
	<table width="631px" align="center" style="background-color: #fff;">
		<tr><td><img src="cid:creasys_logo" width="150" height="45" /></td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td><img src="cid:paswword_titulo" width="631" height="56"/></td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr>
			<td>
				<table 
					align="center"
					style="width: 500px;
						font-family: Arial;
						font-size: 12px;
						font-weight: bold;
						color: #7F7F7F;">
					<tr>
						<td style="width: 70px;">
							Estimado(a)
						</td>
						<td style="font-weight: bold; color: #2D0363;">
							[NOMBRE]
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<table 
					align="center"
					style="width: 500px;
						font-family: Arial;
						font-size: 12px;
						font-weight: bold;
						color: #7F7F7F;
						text-align: center;
						text-align: justify;">
					<tr>
						<td>
							&nbsp;&nbsp;&nbsp;&nbsp;Se ha generado una contrase&ntilde;a temporal para acceder al m&oacute;dulo de
							l&iacute;mites.&nbsp;&nbsp;&nbsp;&nbsp;A continuaci&oacute;n se indica el usuario y contrase&ntilde;a, &uacute;sela en el
							ingreso del sistema para acceder, pero luego se le requerir&aacute; cambiarla por
							una personal.
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td style="text-align: center; color: #2D0363; font-weight: bold;">[USUARIO]</td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr>
			<td>
				<table
					align="center"
					style="background-color: #FD082B;
						border-color: #FD082B;
						border-width: 5px 10px;
						border-style: solid;
						color: #fff;font-weight: bold;
						border-radius: 5px;
						font-size: 20px;">
					<tr>
						<td>
							[PASSWORD]
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td align="center"><img src="cid:cliente_logo" width="260" height="40"/></td></tr>
		<tr><td>&nbsp;</td></tr>
	</table>
</body>