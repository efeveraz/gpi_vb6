﻿Public Class ClsRegion
    Public Function Region_Ver(ByVal lngIdRegion As Long, _
                               ByVal strCodPais As String, _
                               ByVal strColumnas As String, _
                               ByRef strRetorno As String) As DataSet

        Dim LDS_Region As New DataSet
        Dim LDS_Region_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Region_Consultar"
        Lstr_NombreTabla = "Region"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("pIdRegion", SqlDbType.Float, 10).Value = IIf(lngIdRegion = 0, DBNull.Value, lngIdRegion)
            SQLCommand.Parameters.Add("pCodPais", SqlDbType.VarChar, 3).Value = IIf(strCodPais.Trim = "", DBNull.Value, strCodPais.Trim)
            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Region, Lstr_NombreTabla)

            LDS_Region_Aux = LDS_Region

            If strColumnas.Trim = "" Then
                LDS_Region_Aux = LDS_Region
            Else
                LDS_Region_Aux = LDS_Region.Copy
                For Each Columna In LDS_Region.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Region_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Region_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_Region.Dispose()
            Return LDS_Region_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function
    Public Function Region_Mantenedor(ByVal strAccion As String, _
                                      ByVal lngIdRegion As Long, _
                                      ByVal strDescRegion As String, _
                                      ByVal strCodPais As String) As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_Region_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Region_Mantencion"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 10).Value = strAccion
            SQLCommand.Parameters.Add("pIdRegion", SqlDbType.Float, 10).Value = IIf(lngIdRegion = 0, DBNull.Value, lngIdRegion)
            SQLCommand.Parameters.Add("pDscRegion", SqlDbType.VarChar, 100).Value = strDescRegion
            SQLCommand.Parameters.Add("pCodPais", SqlDbType.VarChar, 3).Value = strCodPais

            '...Resultado
            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            Region_Mantenedor = strDescError
        End Try
    End Function
End Class
