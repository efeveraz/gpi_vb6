﻿Public Class ClsProcedenciaFondo
    Public Function BuscarProcedenciaFondoPorCliente(ByVal strCuenta As String, _
                                                        ByVal strColumnas As String, _
                                                        ByRef strRetorno As String) As DataSet

        Dim LDS_ProcedenciaFondo As New DataSet
        Dim LDS_ProcedenciaFondo_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_ProcedenciaFondos_BuscarPorCte"
        Lstr_NombreTabla = "ProcedenciaFondo"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdCliente", SqlDbType.Float, 10).Value = IIf(strCuenta = "", DBNull.Value, strCuenta)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_ProcedenciaFondo, Lstr_NombreTabla)

            LDS_ProcedenciaFondo_Aux = LDS_ProcedenciaFondo

            If strColumnas.Trim = "" Then
                LDS_ProcedenciaFondo_Aux = LDS_ProcedenciaFondo
            Else
                LDS_ProcedenciaFondo_Aux = LDS_ProcedenciaFondo.Copy
                For Each Columna In LDS_ProcedenciaFondo.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_ProcedenciaFondo_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_ProcedenciaFondo_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_ProcedenciaFondo.Dispose()
            Return LDS_ProcedenciaFondo_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function ProcedenciaFondos_Mantenedor(ByVal strId_Cliente As String, ByRef dstProcedenciaFondos As DataSet) As String

        Dim strDescError As String = "OK"
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try

            '       ELIMINAMOS LAS PROCEDENCIAFONDOS YA EXISTENTES

            SQLCommand = New SqlClient.SqlCommand("Rcp_ProcedenciaFondos_MantencionPorCliente", MiTransaccionSQL.Connection, MiTransaccionSQL)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_ProcedenciaFondos_MantencionPorCliente"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = "ELIMINAR"
            SQLCommand.Parameters.Add("pIdProcedenciaFondos", SqlDbType.Float, 9).Value = DBNull.Value
            SQLCommand.Parameters.Add("pIdCliente", SqlDbType.Float, 10).Value = strId_Cliente
            SQLCommand.Parameters.Add("pObservacion", SqlDbType.VarChar, 100).Value = DBNull.Value
            '...Resultado  @pObservacion varchar(100),
            Dim pSalElimIntPort As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalElimIntPort.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalElimIntPort)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            If Not strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Rollback()
                Exit Function
            End If

            '       INSERTAMOS LOS NUEVOS PROCEDENCIAFONDOS X CUENTA 

            If dstProcedenciaFondos.Tables(0).Rows.Count <> Nothing Then
                For Each pRow As DataRow In dstProcedenciaFondos.Tables(0).Rows
                    If (pRow("INCLUIDO").ToString = "1") Then
                        SQLCommand = New SqlClient.SqlCommand("Rcp_ProcedenciaFondos_MantencionPorCliente", MiTransaccionSQL.Connection, MiTransaccionSQL)
                        SQLCommand.CommandType = CommandType.StoredProcedure
                        SQLCommand.CommandText = "Rcp_ProcedenciaFondos_MantencionPorCliente"
                        SQLCommand.Parameters.Clear()

                        SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = "INSERTAR"
                        SQLCommand.Parameters.Add("pIdProcedenciaFondos", SqlDbType.Float, 9).Value = pRow("ID_PROCEDENCIA_FONDO").ToString
                        SQLCommand.Parameters.Add("pObservacion", SqlDbType.VarChar, 100).Value = pRow("OBS_REL_PROCEDENCIA_FONDO").ToString.ToUpper
                        SQLCommand.Parameters.Add("pIdCliente", SqlDbType.Float, 10).Value = strId_Cliente

                        '...Resultado
                        Dim pSalInstPort As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                        pSalInstPort.Direction = Data.ParameterDirection.Output
                        SQLCommand.Parameters.Add(pSalInstPort)

                        SQLCommand.ExecuteNonQuery()

                        strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
                    End If
                    If Not strDescError.ToUpper.Trim = "OK" Then
                        Exit For
                    End If
                Next
            End If

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            ProcedenciaFondos_Mantenedor = strDescError
        End Try
    End Function

End Class
