﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports Microsoft.VisualBasic

Public Class ClsVencimientos
    Public Function Vencimientos_Consultar(ByRef strRetorno As String, _
                                           ByVal strFechaInicio As String, _
                                           ByVal strFechaTermino As String, _
                                           ByVal strColumnas As String) As DataSet

        Dim lDS_Vencimiento As New DataSet
        Dim lDS_Vencimiento_Aux As New DataSet

        Dim Sqlcommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim Lstr_Procedimiento As String = ""
        Dim Lstr_NombreTabla As String = ""

        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True


        Lstr_Procedimiento = "Rcp_VencimientoInstrumento_Consultar"
        Lstr_NombreTabla = "VENCIMIENTO_INSTRUMENTO"
        Connect.Abrir()

        Try
            Sqlcommand = New SqlCommand(Lstr_Procedimiento, Connect.Conexion)

            Sqlcommand.CommandType = CommandType.StoredProcedure
            Sqlcommand.CommandText = Lstr_Procedimiento
            Sqlcommand.Parameters.Clear()
            Sqlcommand.Parameters.Add("@pFechaInicio", SqlDbType.VarChar, 10).Value = strFechaInicio
            Sqlcommand.Parameters.Add("@pFechaTermino", SqlDbType.VarChar, 10).Value = strFechaTermino
            Sqlcommand.Parameters.Add("@pIdNegocio", SqlDbType.Int, 10).Value = glngNegocioOperacion

            Sqlcommand.CommandTimeout = 0
            Dim oParamSal As New SqlClient.SqlParameter("@pResultado", SqlDbType.VarChar, 200)
            oParamSal.Direction = ParameterDirection.Output
            Sqlcommand.Parameters.Add(oParamSal)

            SQLDataAdapter.SelectCommand = Sqlcommand
            SQLDataAdapter.Fill(lDS_Vencimiento, Lstr_NombreTabla)

            lDS_Vencimiento_Aux = lDS_Vencimiento

            If strColumnas.Trim = "" Then
                lDS_Vencimiento_Aux = lDS_Vencimiento
            Else
                lDS_Vencimiento_Aux = lDS_Vencimiento.Copy
                For Each Columna In lDS_Vencimiento.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        lDS_Vencimiento_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In lDS_Vencimiento_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"

            Return lDS_Vencimiento_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function Vencimientos_CorteCupon_Consultar(ByRef strRetorno As String, _
                                                      ByVal strFechaInicio As String, _
                                                      ByVal strFechaTermino As String, _
                                                      ByVal strColumnas As String) As DataSet

        Dim lDS_Vencimiento As New DataSet
        Dim lDS_Vencimiento_Aux As New DataSet

        Dim Sqlcommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim Lstr_Procedimiento As String = ""
        Dim Lstr_NombreTabla As String = ""

        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True


        Lstr_Procedimiento = "Rcp_VencimientoCorteCupon_Consultar"
        Lstr_NombreTabla = "VENCIMIENTO_CORTECUPON"
        Connect.Abrir()

        Try
            Sqlcommand = New SqlCommand(Lstr_Procedimiento, Connect.Conexion)

            Sqlcommand.CommandType = CommandType.StoredProcedure
            Sqlcommand.CommandText = Lstr_Procedimiento
            Sqlcommand.CommandTimeout = 0
            Sqlcommand.Parameters.Clear()
            Sqlcommand.Parameters.Add("@pFechaInicio", SqlDbType.VarChar, 10).Value = strFechaInicio
            Sqlcommand.Parameters.Add("@pFechaTermino", SqlDbType.VarChar, 10).Value = strFechaTermino
            Sqlcommand.Parameters.Add("@pIdNegocio", SqlDbType.Int, 10).Value = glngNegocioOperacion

            Dim oParamSal As New SqlClient.SqlParameter("@pResultado", SqlDbType.VarChar, 200)
            oParamSal.Direction = ParameterDirection.Output
            Sqlcommand.Parameters.Add(oParamSal)

            SQLDataAdapter.SelectCommand = Sqlcommand
            SQLDataAdapter.Fill(lDS_Vencimiento, Lstr_NombreTabla)

            lDS_Vencimiento_Aux = lDS_Vencimiento

            If strColumnas.Trim = "" Then
                lDS_Vencimiento_Aux = lDS_Vencimiento
            Else
                lDS_Vencimiento_Aux = lDS_Vencimiento.Copy
                For Each Columna In lDS_Vencimiento.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        lDS_Vencimiento_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In lDS_Vencimiento_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"

            Return lDS_Vencimiento_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Sub Vencimientos_CorteCupon_Procesar(ByRef strRetorno As String, _
                                      ByVal strVcto As String)

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_Vecimiento_CorteCupon", MiTransaccionSQL.Connection, MiTransaccionSQL)
            SQLCommand.CommandTimeout = 0
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Vecimiento_CorteCupon"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("@pListaVecto", SqlDbType.Text).Value = strVcto
            SQLCommand.Parameters.Add("@pNegocio", SqlDbType.Decimal, 10).Value = glngNegocioOperacion

            '...Resultado
            Dim pSalInstPort As New SqlClient.SqlParameter("@pResultado", SqlDbType.VarChar, 200)
            pSalInstPort.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalInstPort)

            SQLCommand.ExecuteNonQuery()
            strDescError = SQLCommand.Parameters("@pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If
            strRetorno = strDescError

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strRetorno = Ex.Message
        Finally
            SQLConnect.Cerrar()
        End Try
    End Sub

    Public Sub Vencimientos_RFNacional(ByRef strRetorno As String, _
                                       ByVal strVcto As String)

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_Vecimiento_CorteCupon", MiTransaccionSQL.Connection, MiTransaccionSQL)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Vecimiento_CorteCupon"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("@pNegocio", SqlDbType.Decimal, 10).Value = glngNegocioOperacion
            SQLCommand.Parameters.Add("@pVcto", SqlDbType.Text).Value = strVcto

            '...Resultado
            Dim pSalInstPort As New SqlClient.SqlParameter("@pResultado", SqlDbType.VarChar, 200)
            pSalInstPort.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalInstPort)

            SQLCommand.ExecuteNonQuery()
            strDescError = SQLCommand.Parameters("@pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If
            strRetorno = "OK"

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strRetorno = Ex.Message
        Finally
            SQLConnect.Cerrar()
        End Try
    End Sub


    'Public Function Vencimientos_INSTRUMENTOS(ByVal strDesde As String, _
    '                                     ByVal strHasta As String, _
    '                                     ByVal strListaInstrumentos As String, _
    '                                     ByRef strRetorno As String) As DataSet

    '    Dim strDescError As String = ""
    '    Dim SQLCommand As New SqlClient.SqlCommand
    '    Dim SQLConnect As Cls_Conexion = New Cls_Conexion
    '    Dim MiTransaccionSQL As SqlClient.SqlTransaction

    '    '...Abre la conexion
    '    SQLConnect.Abrir()
    '    MiTransaccionSQL = SQLConnect.Transaccion

    '    Try
    '        SQLCommand = New SqlClient.SqlCommand("Rcp_Procesa_Vencimiento_Instrumentos", MiTransaccionSQL.Connection, MiTransaccionSQL)
    '        SQLCommand.CommandType = CommandType.StoredProcedure
    '        SQLCommand.CommandText = "Rcp_Procesa_Vencimiento_Instrumentos"
    '        SQLCommand.Parameters.Clear()

    '        SQLCommand.Parameters.Add("@pFechaDesde", SqlDbType.VarChar, 10).Value = strDesde
    '        SQLCommand.Parameters.Add("@pFechaHasta", SqlDbType.VarChar, 10).Value = strHasta
    '        SQLCommand.Parameters.Add("@pListaInstrumentos", SqlDbType.Text).Value = strListaInstrumentos

    '        '...Resultado
    '        Dim pSalInstPort As New SqlClient.SqlParameter("@pResultado", SqlDbType.VarChar, 200)
    '        pSalInstPort.Direction = Data.ParameterDirection.Output
    '        SQLCommand.Parameters.Add(pSalInstPort)

    '        SQLCommand.ExecuteNonQuery()
    '        strDescError = SQLCommand.Parameters("@pResultado").Value.ToString.Trim

    '        If strDescError.ToUpper.Trim = "OK" Then
    '            MiTransaccionSQL.Commit()
    '        Else
    '            MiTransaccionSQL.Rollback()
    '        End If
    '        strRetorno = "OK"

    '    Catch Ex As Exception
    '        MiTransaccionSQL.Rollback()
    '        strRetorno = Ex.Message
    '    Finally
    '        SQLConnect.Cerrar()
    '    End Try
    'End Function
    Public Function Vencimientos_INST(ByVal strDesde As String, _
                                         ByVal strHasta As String, _
                                         ByVal strListaInstrumentos As String, _
                                         ByVal strColumnas As String, _
                                         ByRef strRetorno As String) As DataSet

        Dim lDS_Vencimiento As New DataSet
        Dim lDS_Vencimiento_Aux As New DataSet

        Dim Sqlcommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim Lstr_Procedimiento As String = ""
        Dim Lstr_NombreTabla As String = ""

        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim strDescError As String


        Lstr_Procedimiento = "Rcp_Procesa_Vencimiento_Instrumentos"
        Lstr_NombreTabla = "VENCIMIENTO_CORTECUPON"
        Connect.Abrir()

        Try
            Sqlcommand = New SqlCommand(Lstr_Procedimiento, Connect.Conexion)

            Sqlcommand.CommandType = CommandType.StoredProcedure
            Sqlcommand.CommandText = Lstr_Procedimiento
            Sqlcommand.Parameters.Clear()

            Sqlcommand.Parameters.Add("@pFechaDesde", SqlDbType.VarChar, 10).Value = strDesde
            Sqlcommand.Parameters.Add("@pFechaHasta", SqlDbType.VarChar, 10).Value = strHasta
            Sqlcommand.Parameters.Add("@pListaInstrumentos", SqlDbType.Text).Value = strListaInstrumentos
            Sqlcommand.Parameters.Add("@pId_Negocio", SqlDbType.Decimal, 10).Value = glngNegocioOperacion

            Sqlcommand.CommandTimeout = 0

            Dim oParamSal As New SqlClient.SqlParameter("@pResultado", SqlDbType.VarChar, 200)
            oParamSal.Direction = ParameterDirection.Output
            Sqlcommand.Parameters.Add(oParamSal)

            SQLDataAdapter.SelectCommand = Sqlcommand
            SQLDataAdapter.Fill(lDS_Vencimiento, Lstr_NombreTabla)

            lDS_Vencimiento_Aux = lDS_Vencimiento

            strDescError = Sqlcommand.Parameters("@pResultado").Value.ToString.Trim

            If strDescError <> "OK" Then
                strRetorno = strDescError
                Return lDS_Vencimiento_Aux
            End If

            If strColumnas.Trim = "" Then
                lDS_Vencimiento_Aux = lDS_Vencimiento
            Else
                lDS_Vencimiento_Aux = lDS_Vencimiento.Copy
                For Each Columna In lDS_Vencimiento.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        lDS_Vencimiento_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In lDS_Vencimiento_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next



            strRetorno = "OK"

            Return lDS_Vencimiento_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function Vencimientos_Reportes(ByVal strAccion As String, _
                                          ByVal strDesde As String, _
                                          ByVal strHasta As String, _
                                          ByVal strIdCuenta As String, _
                                          ByVal strIdInstrumento As String, _
                                          ByVal strCodMoneda As String, _
                                          ByVal strColumnas As String, _
                                          ByRef strRetorno As String) As DataSet

        Dim lDS_Vencimiento As New DataSet
        Dim lDS_Vencimiento_Aux As New DataSet

        Dim Sqlcommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim Lstr_Procedimiento As String = ""
        Dim Lstr_NombreTabla As String = ""

        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True


        Lstr_Procedimiento = "Rcp_Vencimientos_Reporte"
        Lstr_NombreTabla = "VENCIMIENTOS"
        Connect.Abrir()

        Try
            Sqlcommand = New SqlCommand(Lstr_Procedimiento, Connect.Conexion)

            Sqlcommand.CommandType = CommandType.StoredProcedure
            Sqlcommand.CommandText = Lstr_Procedimiento
            Sqlcommand.CommandTimeout = 0
            Sqlcommand.Parameters.Clear()

            Sqlcommand.Parameters.Add("@pAccion", SqlDbType.VarChar, 10).Value = IIf(IsNothing(strAccion) Or strAccion = "", DBNull.Value, strAccion)
            Sqlcommand.Parameters.Add("@pFechaDesde", SqlDbType.VarChar, 10).Value = strDesde
            Sqlcommand.Parameters.Add("@pFechaHasta", SqlDbType.VarChar, 10).Value = IIf(strHasta = "", DBNull.Value, strHasta)
            Sqlcommand.Parameters.Add("@pIdCuenta", SqlDbType.Decimal, 10).Value = IIf(strIdCuenta = "", DBNull.Value, strIdCuenta)
            Sqlcommand.Parameters.Add("@pIdInstrumento", SqlDbType.Decimal, 10).Value = IIf(strIdInstrumento = "", DBNull.Value, strIdInstrumento)
            Sqlcommand.Parameters.Add("@pCodMoneda", SqlDbType.VarChar, 3).Value = IIf(strCodMoneda = "", DBNull.Value, strCodMoneda)
            Sqlcommand.Parameters.Add("pIdNegocio", SqlDbType.Int, 10).Value = glngNegocioOperacion

            Dim oParamSal As New SqlClient.SqlParameter("@pResultado", SqlDbType.VarChar, 200)
            oParamSal.Direction = ParameterDirection.Output
            Sqlcommand.Parameters.Add(oParamSal)

            SQLDataAdapter.SelectCommand = Sqlcommand
            SQLDataAdapter.Fill(lDS_Vencimiento, Lstr_NombreTabla)

            lDS_Vencimiento_Aux = lDS_Vencimiento

            If strColumnas.Trim = "" Then
                lDS_Vencimiento_Aux = lDS_Vencimiento
            Else
                lDS_Vencimiento_Aux = lDS_Vencimiento.Copy
                For Each Columna In lDS_Vencimiento.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        lDS_Vencimiento_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In lDS_Vencimiento_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"

            Return lDS_Vencimiento_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function Vencimientos_Fordward_Consultar(ByRef strRetorno As String, _
                                                    ByVal strFechaInicio As String, _
                                                    ByVal strFechaTermino As String, _
                                                    ByVal strColumnas As String, _
                                                          Optional ByVal IntIdOperacion As Integer = 0, _
                                                          Optional ByVal lTomaFechaHoy As Char = "N") As DataSet

        Dim lDS_Vencimiento As New DataSet
        Dim lDS_Vencimiento_Aux As New DataSet

        Dim Sqlcommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim Lstr_Procedimiento As String = ""
        Dim Lstr_NombreTabla As String = ""

        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        'Dim Columna As DataColumn
        Dim Remove As Boolean = True


        Lstr_Procedimiento = "Rcp_VencimientoForward_Consultar"
        Lstr_NombreTabla = "VENCIMIENTO_FORWARD"
        Connect.Abrir()

        Try
            Sqlcommand = New SqlCommand(Lstr_Procedimiento, Connect.Conexion)

            Sqlcommand.CommandType = CommandType.StoredProcedure
            Sqlcommand.CommandText = Lstr_Procedimiento
            Sqlcommand.CommandTimeout = 0
            Sqlcommand.Parameters.Clear()
            Sqlcommand.Parameters.Add("@pFechaInicio", SqlDbType.VarChar, 10).Value = IIf(strFechaInicio = "", DBNull.Value, strFechaInicio)
            Sqlcommand.Parameters.Add("@pFechaTermino", SqlDbType.VarChar, 10).Value = IIf(strFechaTermino = "", DBNull.Value, strFechaTermino)
            Sqlcommand.Parameters.Add("@pIdNegocio", SqlDbType.Int, 10).Value = glngNegocioOperacion
            Sqlcommand.Parameters.Add("@pIdOPeracion", SqlDbType.Int, 10).Value = IIf(IntIdOperacion = 0, DBNull.Value, IntIdOperacion)
            Sqlcommand.Parameters.Add("@pTomaFechaHoy", SqlDbType.VarChar, 1).Value = lTomaFechaHoy

            Dim oParamSal As New SqlClient.SqlParameter("@pResultado", SqlDbType.VarChar, 200)
            oParamSal.Direction = ParameterDirection.Output
            Sqlcommand.Parameters.Add(oParamSal)

            SQLDataAdapter.SelectCommand = Sqlcommand
            SQLDataAdapter.Fill(lDS_Vencimiento, Lstr_NombreTabla)

            lDS_Vencimiento_Aux = lDS_Vencimiento

            'If strColumnas.Trim = "" Then
            '    lDS_Vencimiento_Aux = lDS_Vencimiento
            'Else
            '    lDS_Vencimiento_Aux = lDS_Vencimiento.Copy
            '    For Each Columna In lDS_Vencimiento.Tables(Lstr_NombreTabla).Columns
            '        Remove = True
            '        For LInt_Col = 0 To LArr_NombreColumna.Length - 1
            '            LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
            '            If Columna.ColumnName = LInt_NomCol Then
            '                Remove = False
            '            End If
            '        Next
            '        If Remove Then
            '            lDS_Vencimiento_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
            '        End If
            '    Next
            'End If

            'For LInt_Col = 0 To LArr_NombreColumna.Length - 1
            '    LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
            '    For Each Columna In lDS_Vencimiento_Aux.Tables(Lstr_NombreTabla).Columns
            '        If Columna.ColumnName = LInt_NomCol Then
            '            Columna.SetOrdinal(LInt_Col)
            '            Exit For
            '        End If
            '    Next
            'Next

            strRetorno = "OK"

            Return lDS_Vencimiento_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Sub Vencimientos_Forward_Procesar(ByRef strRetorno As String, _
                                             ByVal strVcto As String)

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction
        Dim strProcedimiento As String

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion
        strProcedimiento = "Rcp_Vencimiento_Forward"

        Try
            SQLCommand = New SqlClient.SqlCommand(strProcedimiento, MiTransaccionSQL.Connection, MiTransaccionSQL)
            SQLCommand.CommandTimeout = 0
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = strProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("@pListaVcto", SqlDbType.Text).Value = strVcto
            SQLCommand.Parameters.Add("@pNegocio", SqlDbType.Decimal, 10).Value = glngNegocioOperacion

            '...Resultado
            Dim pSalInstPort As New SqlClient.SqlParameter("@pResultado", SqlDbType.VarChar, 200)
            pSalInstPort.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalInstPort)

            SQLCommand.ExecuteNonQuery()
            strDescError = SQLCommand.Parameters("@pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If
            strRetorno = strDescError

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strRetorno = Ex.Message
        Finally
            SQLConnect.Cerrar()
        End Try
    End Sub

    Public Sub Vencimientos_Forward_Anticipado_Procesar(ByRef strRetorno As String, _
                                                        ByVal strVcto As String, _
                                                        ByVal lMonto As Integer, _
                                                        ByVal lFecha_Operacion As String, _
                                                        ByVal lTipoAboCarFwrd As String, _
                                                        ByVal lPrecio As Integer, _
                                                              Optional ByVal lTipoTCCompensacion As Integer = 0)

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction
        Dim strProcedimiento As String

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion
        strProcedimiento = "Rcp_Vencimiento_Anticipado_Forward"

        Try
            SQLCommand = New SqlClient.SqlCommand(strProcedimiento, MiTransaccionSQL.Connection, MiTransaccionSQL)
            SQLCommand.CommandTimeout = 0
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = strProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("@pListaVcto", SqlDbType.Text).Value = strVcto
            SQLCommand.Parameters.Add("@pNegocio", SqlDbType.Decimal, 10).Value = glngNegocioOperacion
            SQLCommand.Parameters.Add("@pMonto", SqlDbType.Float).Value = lMonto
            SQLCommand.Parameters.Add("@pFechaOperacion", SqlDbType.VarChar).Value = lFecha_Operacion
            SQLCommand.Parameters.Add("@pTipoAboCarFwr", SqlDbType.Char).Value = lTipoAboCarFwrd
            SQLCommand.Parameters.Add("@pPrecio", SqlDbType.Float).Value = lPrecio
            SQLCommand.Parameters.Add("@pTipoTCCompensacion", SqlDbType.Float).Value = IIf(lTipoTCCompensacion = 0, DBNull.Value, lTipoTCCompensacion)

            '...Resultado
            Dim pSalInstPort As New SqlClient.SqlParameter("@pResultado", SqlDbType.VarChar, 200)
            pSalInstPort.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalInstPort)

            SQLCommand.ExecuteNonQuery()
            strDescError = SQLCommand.Parameters("@pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If
            strRetorno = strDescError

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strRetorno = Ex.Message
        Finally
            SQLConnect.Cerrar()
        End Try
    End Sub

End Class