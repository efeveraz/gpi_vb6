﻿Public Class ClsChecklist
    Public Function Checklist_Ver(ByVal strCodigoNegocio As String, _
                                  ByVal strCodigoCliente As String, _
                                  ByVal strCodigoTipo As String, _
                                  ByVal strColumnas As String, _
                                  ByRef strRetorno As String, _
                                  ByRef strEstado As String) As DataSet

        Dim LDS_Checklist As New DataSet
        Dim LDS_Checklist_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Checklist_ConsultarPorCliente"
        Lstr_NombreTabla = "Checklist"

        Connect.Abrir()

        '@pIdTipo Define si es el data set del checklist de equicitos del cliente (1) o Si son los de Aporte Inicial (2)

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Float, 9).Value = IIf(strCodigoNegocio.Trim = "", DBNull.Value, strCodigoNegocio.Trim)
            SQLCommand.Parameters.Add("pIdCliente", SqlDbType.Float, 9).Value = IIf(strCodigoCliente.Trim = "", DBNull.Value, strCodigoCliente.Trim)
            SQLCommand.Parameters.Add("pIdTipo", SqlDbType.Float, 43).Value = IIf(strCodigoTipo.Trim = "", DBNull.Value, strCodigoTipo.Trim)
            SQLCommand.Parameters.Add("pEstado", SqlDbType.VarChar, 3).Value = IIf(strEstado.Trim = "", DBNull.Value, strEstado.Trim)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Checklist, Lstr_NombreTabla)

            LDS_Checklist_Aux = LDS_Checklist

            If strColumnas.Trim = "" Then
                LDS_Checklist_Aux = LDS_Checklist
            Else
                LDS_Checklist_Aux = LDS_Checklist.Copy
                For Each Columna In LDS_Checklist.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Checklist_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Checklist_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_Checklist.Dispose()
            Return LDS_Checklist_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function Checklist_Mantenedor(ByVal strId_Cliente As String, _
                                         ByRef dstChecklist As DataSet, _
                                         ByRef dstAporteInicial As DataSet) As String

        Dim strDescError As String = "OK"
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            '       ELIMINAMOS LOS CHECKLIST YA EXISTENTES

            SQLCommand = New SqlClient.SqlCommand("Rcp_Checklist_MantencionPorCliente", MiTransaccionSQL.Connection, MiTransaccionSQL)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Checklist_MantencionPorCliente"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = "ELIMINAR"
            SQLCommand.Parameters.Add("pIdChecklistCliente", SqlDbType.Float, 10).Value = DBNull.Value
            SQLCommand.Parameters.Add("pIdcliente", SqlDbType.Float, 10).Value = strId_Cliente
            SQLCommand.Parameters.Add("pIdChecklistEstado", SqlDbType.Float, 5).Value = DBNull.Value
            '...Resultado
            Dim pSalElimIntPort As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalElimIntPort.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalElimIntPort)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            If Not strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Rollback()
                Exit Function
            End If

            '       INSERTAMOS CHECKLIST DE DOCUEMNTOS PARA EL CLIENTE

            If dstChecklist.Tables(0).Rows.Count <> Nothing Then
                For Each pRow As DataRow In dstChecklist.Tables(0).Rows
                    SQLCommand = New SqlClient.SqlCommand("Rcp_Checklist_MantencionPorCliente", MiTransaccionSQL.Connection, MiTransaccionSQL)
                    SQLCommand.CommandType = CommandType.StoredProcedure
                    SQLCommand.CommandText = "Rcp_Checklist_MantencionPorCliente"
                    SQLCommand.Parameters.Clear()

                    '[Rcp_Checklist_MantencionPorCliente]()
                    '(@pAccion varchar(10), 
                    '@pIdChecklistCliente numeric(10, 0),
                    '@pIdCliente numeric(10, 0),
                    '@pIdChecklistEstado numeric(5, 0),
                    '@pResultado varchar(200) OUTPUT) TIPO,IDCHECKLIST,IDESTADO,CHECKLIST,ESTADO

                    SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = "INSERTAR"
                    SQLCommand.Parameters.Add("pIdChecklistCliente", SqlDbType.Float, 10).Value = pRow("IDCHECKLIST").ToString
                    SQLCommand.Parameters.Add("pIdcliente", SqlDbType.Float, 10).Value = strId_Cliente
                    SQLCommand.Parameters.Add("pIdChecklistEstado", SqlDbType.Float, 5).Value = pRow("IDESTADO").ToString
                    SQLCommand.Parameters.Add("pObservacion", SqlDbType.VarChar, 100).Value = pRow("OBSERVACION").ToString

                    '...Resultado
                    Dim pSalInstPort As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                    pSalInstPort.Direction = Data.ParameterDirection.Output
                    SQLCommand.Parameters.Add(pSalInstPort)

                    SQLCommand.ExecuteNonQuery()

                    strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
                    If Not strDescError.ToUpper.Trim = "OK" Then
                        Exit For
                    End If
                Next
            End If
            '       INSERTAMOS  CHECKLIST DE APORTE INICIAL PARA EL CLIENTE
            If dstAporteInicial.Tables(0).Rows.Count <> Nothing Then
                For Each pRow As DataRow In dstAporteInicial.Tables(0).Rows
                    SQLCommand = New SqlClient.SqlCommand("Rcp_Checklist_MantencionPorCliente", MiTransaccionSQL.Connection, MiTransaccionSQL)
                    SQLCommand.CommandType = CommandType.StoredProcedure
                    SQLCommand.CommandText = "Rcp_Checklist_MantencionPorCliente"
                    SQLCommand.Parameters.Clear()

                    '[Rcp_Checklist_MantencionPorCliente]()
                    '(@pAccion varchar(10), 
                    '@pIdChecklistCliente numeric(10, 0),
                    '@pIdCliente numeric(10, 0),
                    '@pIdChecklistEstado numeric(5, 0),
                    '@pResultado varchar(200) OUTPUT) TIPO,IDCHECKLIST,IDESTADO,CHECKLIST,ESTADO

                    SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = "INSERTAR"
                    SQLCommand.Parameters.Add("pIdChecklistCliente", SqlDbType.Float, 10).Value = pRow("IDCHECKLIST").ToString
                    SQLCommand.Parameters.Add("pIdcliente", SqlDbType.Float, 10).Value = strId_Cliente
                    SQLCommand.Parameters.Add("pIdChecklistEstado", SqlDbType.Float, 5).Value = pRow("IDESTADO").ToString
                    SQLCommand.Parameters.Add("pObservacion", SqlDbType.VarChar, 100).Value = pRow("OBSERVACION").ToString

                    '...Resultado
                    Dim pSalInstPort As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                    pSalInstPort.Direction = Data.ParameterDirection.Output
                    SQLCommand.Parameters.Add(pSalInstPort)

                    SQLCommand.ExecuteNonQuery()

                    strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
                    If Not strDescError.ToUpper.Trim = "OK" Then
                        Exit For
                    End If
                Next
            End If
            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            Checklist_Mantenedor = strDescError
        End Try
    End Function

    Public Function Checklist_Cliente_Ver(ByVal strIdChkList As String, _
                                  ByVal Negocio As String, _
                                  ByVal Estado As String, _
                                  ByVal strColumnas As String, _
                                  ByRef strRetorno As String) As DataSet

        Dim LDS_Checklist As New DataSet
        Dim LDS_Checklist_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Checklist_Cliente_Consultar"
        Lstr_NombreTabla = "ChkLClientes"

        Connect.Abrir()

        '@pIdTipo Define si es el data set del checklist de equicitos del cliente (1) o Si son los de Aporte Inicial (2)

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdChkList", SqlDbType.Float, 9).Value = IIf(strIdChkList.Trim = "", DBNull.Value, strIdChkList.Trim)
            SQLCommand.Parameters.Add("pNegocio", SqlDbType.VarChar, 50).Value = Negocio.Trim
            SQLCommand.Parameters.Add("pEstado", SqlDbType.VarChar, 3).Value = Estado.Trim

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Checklist, Lstr_NombreTabla)

            LDS_Checklist_Aux = LDS_Checklist

            If strColumnas.Trim = "" Then
                LDS_Checklist_Aux = LDS_Checklist
            Else
                LDS_Checklist_Aux = LDS_Checklist.Copy
                For Each Columna In LDS_Checklist.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Checklist_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Checklist_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_Checklist.Dispose()
            Return LDS_Checklist_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function Checklist_Buscar_Negocio(ByVal strNegocio As String, _
                                  ByVal strColumnas As String, _
                                  ByRef strRetorno As String) As DataSet

        Dim LDS_Checklist As New DataSet
        Dim LDS_Checklist_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Checklist_Cliente_Consultar"
        Lstr_NombreTabla = "Checklist"

        Connect.Abrir()

        '@pIdTipo Define si es el data set del checklist de equicitos del cliente (1) o Si son los de Aporte Inicial (2)

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdChkList", SqlDbType.Float, 9).Value = DBNull.Value
            SQLCommand.Parameters.Add("pNegocio", SqlDbType.VarChar, 50).Value = strNegocio.Trim
            SQLCommand.Parameters.Add("pEstado", SqlDbType.VarChar, 3).Value = "TOD"

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Checklist, Lstr_NombreTabla)

            LDS_Checklist_Aux = LDS_Checklist

            If strColumnas.Trim = "" Then
                LDS_Checklist_Aux = LDS_Checklist
            Else
                LDS_Checklist_Aux = LDS_Checklist.Copy
                For Each Columna In LDS_Checklist.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Checklist_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            strRetorno = "OK"
            LDS_Checklist.Dispose()
            Return LDS_Checklist_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function


    Public Function ChkList_Cliente_Mantenedor(ByVal strAccion As String, _
                              ByVal strIdChkListCliente As String, _
                              ByVal strDescripcion As String, _
                              ByVal strTipo As String, _
                              ByVal strOrden As String, _
                              ByVal strObservacion As String, _
                              ByVal strIdNegocio As String, _
                              ByVal strEstado As String, _
                              ByVal strOrden_ini As String, _
                              ByVal strOrden_fin As String) As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_Checklist_Cliente_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Checklist_Cliente_Mantencion"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 10).Value = strAccion
            SQLCommand.Parameters.Add("pIdChkList", SqlDbType.Int, 10).Value = IIf(strIdChkListCliente.Trim = "0", DBNull.Value, strIdChkListCliente.Trim)
            SQLCommand.Parameters.Add("pDescripcion", SqlDbType.VarChar, 100).Value = strDescripcion.Trim
            SQLCommand.Parameters.Add("pTipo", SqlDbType.Int, 2).Value = CInt(strTipo.Trim)
            SQLCommand.Parameters.Add("pOrden", SqlDbType.Int, 3).Value = CInt(strOrden.Trim)
            SQLCommand.Parameters.Add("pObservacion", SqlDbType.VarChar, 2).Value = strObservacion.Trim
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Int, 10).Value = CInt(strIdNegocio.Trim)
            SQLCommand.Parameters.Add("pEstado", SqlDbType.VarChar, 3).Value = strEstado
            SQLCommand.Parameters.Add("pOrden1", SqlDbType.Int, 3).Value = CInt(strOrden_ini.Trim)
            SQLCommand.Parameters.Add("pOrden2", SqlDbType.Int, 3).Value = CInt(strOrden_fin.Trim)
            '...Resultado
            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            ChkList_Cliente_Mantenedor = strDescError
        End Try
    End Function



    Public Function Checklist_Cliente_Buscar(ByVal lngNegocio As Long, _
                                             ByVal lngIdCliente As Long, _
                                             ByVal lngEstado As Long, _
                                             ByVal strColumnas As String, _
                                             ByRef strRetorno As String) As DataSet

        Dim LDS_Checklist As New DataSet
        Dim LDS_Checklist_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Checklist_Cliente_Buscar"
        Lstr_NombreTabla = "CHECK_LIST_CLIENTES"

        Connect.Abrir()

        '@pIdTipo Define si es el data set del checklist de equicitos del cliente (1) o Si son los de Aporte Inicial (2)

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Float, 10).Value = lngNegocio
            SQLCommand.Parameters.Add("pIdCliente", SqlDbType.Float, 10).Value = lngIdCliente
            SQLCommand.Parameters.Add("pEstado", SqlDbType.Float, 5).Value = lngEstado

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Checklist, Lstr_NombreTabla)

            LDS_Checklist_Aux = LDS_Checklist

            If strColumnas.Trim = "" Then
                LDS_Checklist_Aux = LDS_Checklist
            Else
                LDS_Checklist_Aux = LDS_Checklist.Copy
                For Each Columna In LDS_Checklist.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Checklist_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Checklist_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_Checklist.Dispose()
            Return LDS_Checklist_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function
    Public Function Consultar_Estados_Check(ByRef strRetorno As String, _
                                            ByVal strColumnas As String) As DataSet

        Dim lDS_Vencimiento As New DataSet
        Dim lDS_Vencimiento_Aux As New DataSet

        Dim Sqlcommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim Lstr_Procedimiento As String = ""
        Dim Lstr_NombreTabla As String = ""

        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True


        Lstr_Procedimiento = "Rcp_Consulta_Check_Cliente"
        Lstr_NombreTabla = "CHECK_CLIENTES"
        Connect.Abrir()

        Try
            Sqlcommand = New SqlClient.SqlCommand(Lstr_Procedimiento, Connect.Conexion)

            Sqlcommand.CommandType = CommandType.StoredProcedure
            Sqlcommand.CommandText = Lstr_Procedimiento
            Sqlcommand.Parameters.Clear()

            Dim oParamSal As New SqlClient.SqlParameter("@pResultado", SqlDbType.VarChar, 200)
            oParamSal.Direction = ParameterDirection.Output
            Sqlcommand.Parameters.Add(oParamSal)

            SQLDataAdapter.SelectCommand = Sqlcommand
            SQLDataAdapter.Fill(lDS_Vencimiento, Lstr_NombreTabla)

            lDS_Vencimiento_Aux = lDS_Vencimiento

            If strColumnas.Trim = "" Then
                lDS_Vencimiento_Aux = lDS_Vencimiento
            Else
                lDS_Vencimiento_Aux = lDS_Vencimiento.Copy
                For Each Columna In lDS_Vencimiento.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        lDS_Vencimiento_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In lDS_Vencimiento_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"

            Return lDS_Vencimiento_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function


End Class
