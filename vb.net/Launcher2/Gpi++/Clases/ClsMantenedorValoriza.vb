﻿Imports System.Data.SqlClient
Imports System.Data
Public Class ClsMantenedorValoriza

    Public Function TraeDatosValorizacion(ByRef strRetorno As String, _
                                          ByVal strFecha As String, _
                                          ByVal strNemo As String) As DataSet

        Dim LDS_CuentasDET As New DataSet
        Dim LDS_CuentasDET_Aux As New DataSet
        Dim Lstr_NombreTabla As String
        Dim LstrProcedimiento
        Dim strDescError As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_BuscarValorizacion"
        Lstr_NombreTabla = "VALORES"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("FechaConsulta", SqlDbType.VarChar, 10).Value = strFecha
            SQLCommand.Parameters.Add("Nemotecnico", SqlDbType.Char, 25).Value = strNemo

            SQLCommand.CommandTimeout = 0
            Dim oParamSal As New SqlClient.SqlParameter("MensajeSalida", SqlDbType.VarChar, 200)
            oParamSal.Direction = ParameterDirection.Output
            SQLCommand.Parameters.Add(oParamSal)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_CuentasDET, Lstr_NombreTabla)

            LDS_CuentasDET_Aux = LDS_CuentasDET
            strRetorno = "OK"
            Return LDS_CuentasDET_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function



    Public Function Valorizacion_Mantenedor(ByVal strAccion As String, _
                                            ByVal Fecha As String, _
                                            ByVal Nemotecnico As String, _
                                            ByVal Tasa As Double, _
                                            ByVal ValorPar As Double, _
                                            ByVal SaldoCapital As Double, _
                                            ByVal PorcValorPar As Double, _
                                            ByVal FechaProximoCupon As String, _
                                            ByVal FechaActualCupon As String, _
                                            ByVal CuponProximo As Integer, _
                                            ByVal CuponActual As Integer, _
                                            ByVal ValorunitarioUM As Double, _
                                            ByVal Duration As Double, _
                                            ByVal Convexidad As Double, _
                                            ByVal CapitalCuponActual As Double, _
                                            ByVal InteresCuponActual As Double, _
                                            ByVal CapitalCuponProximo As Double, _
                                            ByVal InteresCuponProximo As Double, _
                                            ByVal FactorRedondeo As Integer) As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_Valorizacion_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Valorizacion_Mantencion"
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 10).Value = strAccion
            SQLCommand.Parameters.Add("pFecha", SqlDbType.VarChar, 10).Value = Fecha
            SQLCommand.Parameters.Add("pNemotecnico", SqlDbType.Char, 25).Value = Nemotecnico
            SQLCommand.Parameters.Add("pTasa", SqlDbType.Float).Value = Tasa
            SQLCommand.Parameters.Add("pValor_Par", SqlDbType.Float).Value = ValorPar
            SQLCommand.Parameters.Add("pSaldo_Capital", SqlDbType.Float).Value = SaldoCapital
            SQLCommand.Parameters.Add("pPorc_Valor_Par", SqlDbType.Float).Value = PorcValorPar
            SQLCommand.Parameters.Add("pFecha_Prx_Cupon", SqlDbType.VarChar, 10).Value = FechaProximoCupon
            SQLCommand.Parameters.Add("pFecha_Act_Cupon", SqlDbType.VarChar, 10).Value = FechaActualCupon
            SQLCommand.Parameters.Add("pCupon_Prx", SqlDbType.Int).Value = CuponProximo
            SQLCommand.Parameters.Add("pCupon_Act", SqlDbType.Int).Value = CuponActual
            SQLCommand.Parameters.Add("pValor_Unitario_Um", SqlDbType.Float).Value = ValorunitarioUM
            SQLCommand.Parameters.Add("pDuration", SqlDbType.Float).Value = Duration
            SQLCommand.Parameters.Add("pConvexidad", SqlDbType.Float).Value = Convexidad
            SQLCommand.Parameters.Add("pCapital_Cupon_Actual", SqlDbType.Float).Value = CapitalCuponActual
            SQLCommand.Parameters.Add("pInteres_Cupon_Actual", SqlDbType.Float).Value = InteresCuponActual
            SQLCommand.Parameters.Add("pCapital_Cupon_Proximo", SqlDbType.Float).Value = CapitalCuponProximo
            SQLCommand.Parameters.Add("pINTERES_Cupon_Proximo", SqlDbType.Float).Value = InteresCuponProximo
            SQLCommand.Parameters.Add("pFactor_Redondeo", SqlDbType.Int).Value = FactorRedondeo

            '...Resultado
            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()
            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim



            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            Valorizacion_Mantenedor = strDescError
        End Try
    End Function

End Class
