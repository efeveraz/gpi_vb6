﻿Public Class ClasTipoControl
    Public Function TipoControl_Ver(ByVal strIdTipo As String, _
                              ByVal strEstado As String, _
                              ByVal strDscTip As String, _
                              ByVal strDscCtTip As String, _
                              ByVal strColumnas As String, _
                              ByRef strRetorno As String) As DataSet

        Dim LDS_Tipo As New DataSet
        Dim LDS_Tipo_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Tipo_Control_Consultar"
        Lstr_NombreTabla = "Tipo_Control"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdTipo", SqlDbType.Float, 3).Value = IIf(strIdTipo.Trim = "", DBNull.Value, strIdTipo.Trim)
            SQLCommand.Parameters.Add("pEstado", SqlDbType.VarChar, 3).Value = IIf(strEstado.Trim = "", DBNull.Value, strEstado.Trim)
            SQLCommand.Parameters.Add("pDscTip", SqlDbType.VarChar, 50).Value = IIf(strDscTip.Trim = "", DBNull.Value, strDscTip.Trim)
            SQLCommand.Parameters.Add("pDscCtTip", SqlDbType.VarChar, 15).Value = IIf(strDscCtTip.Trim = "", DBNull.Value, strDscCtTip.Trim)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Tipo, Lstr_NombreTabla)

            LDS_Tipo_Aux = LDS_Tipo

            If strColumnas.Trim = "" Then
                LDS_Tipo_Aux = LDS_Tipo
            Else
                LDS_Tipo_Aux = LDS_Tipo.Copy
                For Each Columna In LDS_Tipo.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Tipo_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Tipo_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_Tipo.Dispose()
            Return LDS_Tipo_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function TipoControl_Mantenedor(ByVal strAccion As String, _
                              ByVal strDscTip As String, _
                              ByVal strDscCtTip As String, _
                              ByVal strEstado As String, _
                              ByVal intIdTipo As Integer) As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_Tipo_Control_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Tipo_Control_Mantencion"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 10).Value = strAccion
            SQLCommand.Parameters.Add("pDscTip", SqlDbType.VarChar, 50).Value = strDscTip
            SQLCommand.Parameters.Add("pDscCtTip", SqlDbType.VarChar, 15).Value = strDscCtTip
            SQLCommand.Parameters.Add("pEstado", SqlDbType.VarChar, 3).Value = strEstado
            SQLCommand.Parameters.Add("pIdTipo", SqlDbType.Float, 3).Value = intIdTipo

            '...Resultado
            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            TipoControl_Mantenedor = strDescError
        End Try
    End Function
End Class
