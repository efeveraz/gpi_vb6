﻿Public Class ClsMoneda

    Public Function Moneda_Ver(ByVal strCodigo As String, _
                               ByVal strColumnas As String, _
                               ByVal strEstado As String, _
                               ByRef strRetorno As String, _
                               Optional ByVal strEsMonedaPago As String = "", _
                               Optional ByVal strEsDual As String = "D", _
                               Optional ByVal strMonedaIndice As String = "M", _
                               Optional ByVal strSoloMonedaMX As String = "NO", _
                               Optional ByVal strEsMonedaPagoConUf As String = "NO") As DataSet

        Dim LDS_Moneda As New DataSet
        Dim LDS_Moneda_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Moneda_Consultar"
        Lstr_NombreTabla = "MONEDA"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("pCodMoneda", SqlDbType.VarChar, 100).Value = IIf(strCodigo.Trim = "", DBNull.Value, strCodigo.Trim)
            SQLCommand.Parameters.Add("pEstado", SqlDbType.VarChar, 3).Value = IIf(strEstado.Trim = "", DBNull.Value, strEstado.Trim)
            SQLCommand.Parameters.Add("pFlgEsMonedaPago", SqlDbType.VarChar, 1).Value = IIf(strEsMonedaPago.Trim = "", DBNull.Value, strEsMonedaPago.Trim)
            SQLCommand.Parameters.Add("pEsDual ", SqlDbType.VarChar, 1).Value = strEsDual
            SQLCommand.Parameters.Add("pMonedaIndice", SqlDbType.VarChar, 1).Value = strMonedaIndice
            SQLCommand.Parameters.Add("pSoloMonedaMX", SqlDbType.VarChar, 2).Value = strSoloMonedaMX
            SQLCommand.Parameters.Add("pFlgEsMonedaPagoConUF", SqlDbType.VarChar, 2).Value = strEsMonedaPagoConUf
            SQLCommand.Parameters.Add("@pIdNegocio", SqlDbType.Int).Value = glngNegocioOperacion

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Moneda, Lstr_NombreTabla)

            LDS_Moneda_Aux = LDS_Moneda

            If strColumnas.Trim = "" Then
                LDS_Moneda_Aux = LDS_Moneda
            Else
                LDS_Moneda_Aux = LDS_Moneda.Copy
                For Each Columna In LDS_Moneda.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Moneda_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Moneda_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_Moneda.Dispose()
            Return LDS_Moneda_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function Moneda_Mantenedor(ByVal strAccion As String, _
                                      ByVal strCodigo As String, _
                                      ByVal strDescripcion As String, _
                                      ByVal strEsMonedaPago As String, _
                                      ByVal intDecimalesMostrar As Integer, _
                                      ByVal strSimbolo As String, _
                                      ByRef strEstado As String, _
                                      Optional ByVal strClasificacion As String = "M") As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_Moneda_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Moneda_Mantencion"
            SQLCommand.Parameters.Clear()

            '@pAccion varchar(10)
            '@pCod_Moneda varchar(3), 
            '@pDsc_Moneda varchar(20), 
            '@pFlg_Es_Moneda_Pago varchar(1), 
            '@pDecimales_mostrar numeric(5, 0), 
            '@pSimbolo varchar(3), 
            '@pEstado char(3), 
            '@pIdMoneda int=NULL OUTPUT, 
            '@pResultado varchar(200) OUTPUT)

            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 10).Value = strAccion
            SQLCommand.Parameters.Add("pCod_Moneda", SqlDbType.VarChar, 3).Value = strCodigo
            SQLCommand.Parameters.Add("pDsc_Moneda", SqlDbType.VarChar, 50).Value = strDescripcion
            SQLCommand.Parameters.Add("pFlg_Es_Moneda_Pago", SqlDbType.VarChar, 1).Value = strEsMonedaPago
            SQLCommand.Parameters.Add("pDecimales_mostrar", SqlDbType.Float, 10).Value = intDecimalesMostrar
            SQLCommand.Parameters.Add("pSimbolo", SqlDbType.VarChar, 3).Value = strSimbolo
            SQLCommand.Parameters.Add("pEstado", SqlDbType.VarChar, 3).Value = strEstado
            SQLCommand.Parameters.Add("pClasificacion", SqlDbType.VarChar, 1).Value = strClasificacion

            ''...(Identity)
            'Dim ParametroSal1 As New SqlClient.SqlParameter("pIdMoneda", SqlDbType.Float, 10)
            'ParametroSal1.Direction = Data.ParameterDirection.Output
            'SQLCommand.Parameters.Add(ParametroSal1)

            '...Resultado
            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            Moneda_Mantenedor = strDescError
        End Try
    End Function



End Class
