﻿Public Class ClsGrupoCuentas

    Public Function GrupoCuenta_Ver(ByVal dblIdGrupoCuenta As Double, _
                                    ByVal strCodEstado As String, _
                                    ByVal strColumnas As String, _
                                    ByRef strRetorno As String) As DataSet

        Dim LDS_GrupoCuentas As New DataSet
        Dim LDS_GrupoCuentas_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_GrupoCuenta_Consultar"
        Lstr_NombreTabla = "GRUPO_CUENTA"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()


            SQLCommand.Parameters.Add("pIdGrupoCuenta", SqlDbType.Float, 10).Value = IIf(dblIdGrupoCuenta = 0, DBNull.Value, dblIdGrupoCuenta)
            SQLCommand.Parameters.Add("pCodEstado", SqlDbType.VarChar, 3).Value = IIf(strCodEstado.Trim = "", DBNull.Value, strCodEstado.Trim)
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Float, 10).Value = IIf(glngNegocioOperacion = 0, DBNull.Value, glngNegocioOperacion)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_GrupoCuentas, Lstr_NombreTabla)

            LDS_GrupoCuentas_Aux = LDS_GrupoCuentas

            If strColumnas.Trim = "" Then
                LDS_GrupoCuentas_Aux = LDS_GrupoCuentas
            Else
                LDS_GrupoCuentas_Aux = LDS_GrupoCuentas.Copy
                For Each Columna In LDS_GrupoCuentas.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_GrupoCuentas_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_GrupoCuentas_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_GrupoCuentas.Dispose()
            Return LDS_GrupoCuentas_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function



    Public Function GrupoCuenta_Ver2(ByVal strCodEstado As String, _
                                    ByVal strColumnas As String, _
                                    ByVal strLista As String, _
                                    ByRef strRetorno As String, _
                                    Optional ByVal StrTipoEntidad As String = "") As DataSet

        Dim LDS_GrupoCuentas As New DataSet
        Dim LDS_GrupoCuentas_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_GrupoCuenta_Consultar2"
        Lstr_NombreTabla = "GRUPO_CUENTA"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Float, 10).Value = IIf(glngNegocioOperacion = 0, DBNull.Value, glngNegocioOperacion)
            SQLCommand.Parameters.Add("pCodEstado", SqlDbType.VarChar, 3).Value = IIf(strCodEstado.Trim = "", DBNull.Value, strCodEstado.Trim)
            SQLCommand.Parameters.Add("pTipoPersona", SqlDbType.Char, 1).Value = IIf(StrTipoEntidad = "", DBNull.Value, StrTipoEntidad)
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Float, 6).Value = IIf(glngIdUsuario = 0, DBNull.Value, glngIdUsuario)
            'SQLCommand.Parameters.Add("pIdGrupo", SqlDbType.Float, 9).Value = IIf(StrIdGrupo = "0", DBNull.Value, CLng(StrIdGrupo))
            SQLCommand.Parameters.Add("pListaCuentas", SqlDbType.Text, 60000).Value = strLista

            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)


            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_GrupoCuentas, Lstr_NombreTabla)

            LDS_GrupoCuentas_Aux = LDS_GrupoCuentas

            If strColumnas.Trim = "" Then
                LDS_GrupoCuentas_Aux = LDS_GrupoCuentas
            Else
                LDS_GrupoCuentas_Aux = LDS_GrupoCuentas.Copy
                For Each Columna In LDS_GrupoCuentas.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_GrupoCuentas_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_GrupoCuentas_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_GrupoCuentas.Dispose()
            Return LDS_GrupoCuentas_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function


    Public Function TraeCuentasSegunGrupoCuentas(ByVal dblIdNegocio As Double, _
                                                 ByVal dblIdGrupoCuenta As Double, _
                                                 ByVal strColumnas As String, _
                                                 ByRef strRetorno As String) As DataSet

        Dim LDS_Publicador As New DataSet
        Dim LDS_Publicador_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_GrupoCuentas_XCuentas"
        Lstr_NombreTabla = "REL_CUENTA_GRUPO_CUENTA"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()


            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Float, 10).Value = IIf(dblIdNegocio = 0, DBNull.Value, dblIdNegocio)
            SQLCommand.Parameters.Add("pIdGrupoCuenta", SqlDbType.Float, 10).Value = IIf(dblIdGrupoCuenta = 0, DBNull.Value, dblIdGrupoCuenta)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Publicador, Lstr_NombreTabla)

            LDS_Publicador_Aux = LDS_Publicador

            If strColumnas.Trim = "" Then
                LDS_Publicador_Aux = LDS_Publicador
            Else
                LDS_Publicador_Aux = LDS_Publicador.Copy
                For Each Columna In LDS_Publicador.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Publicador_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Publicador_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_Publicador.Dispose()
            Return LDS_Publicador_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function GrupoCuentas_Mantenedor(ByVal strAccion As String, _
                                            ByVal strCodEstado As String, _
                                            ByVal strDescGrupoCuenta As String, _
                                            ByVal intIdNegocio As Integer, _
                                            ByVal intIdGrupoCuenta As Integer, _
                                            ByRef dblIdgenerado As Double) As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_GrupoCuentas_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_GrupoCuentas_Mantencion"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 10).Value = strAccion
            SQLCommand.Parameters.Add("pCodEstado", SqlDbType.VarChar, 3).Value = strCodEstado
            SQLCommand.Parameters.Add("pDescGrupoCuenta", SqlDbType.VarChar, 60).Value = strDescGrupoCuenta
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Float, 10).Value = intIdNegocio
            SQLCommand.Parameters.Add("pIdGrupoCuentas", SqlDbType.Float, 10).Value = intIdGrupoCuenta

            '...(Identity)
            Dim pSalInstr As New SqlClient.SqlParameter("pGenerado", SqlDbType.Float, 10)
            pSalInstr.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalInstr)

            '...Resultado
            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            dblIdgenerado = SQLCommand.Parameters("pGenerado").Value

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            GrupoCuentas_Mantenedor = strDescError
        End Try
    End Function


    Public Function RelCuentasGrupo_Cuentas_Mantenedor(ByVal strAccion As String, _
                                                       ByVal intOrdenCuenta As Integer, _
                                                       ByVal intIdGrupoCuentas As Integer, _
                                                       ByVal DS_GrupoCuentas As DataSet, _
                                                       ByRef strRetorno As String) As String

        Dim strDescError As String = ""
        Dim strDescError1 As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction
        Dim LDS_Publicador As New DataSet
        Dim LDS_Publicador_Aux As New DataSet
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        'Dim dtrRelPublicador As DataRow

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            '+ ELIMINAMOS LOS QUE ESTAN EN LA REL
            SQLCommand = New SqlClient.SqlCommand("Rcp_RelGrupoCuentas_XCuentas_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_RelGrupoCuentas_XCuentas_Mantencion"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 10).Value = "MODIFICAR"
            SQLCommand.Parameters.Add("pOrdenCuenta", SqlDbType.Float, 10).Value = intOrdenCuenta
            SQLCommand.Parameters.Add("pIdGrupoCuentas ", SqlDbType.Float, 10).Value = intIdGrupoCuentas
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = 0

            '...Resultado
            Dim pSalElimIntPort1 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalElimIntPort1.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalElimIntPort1)

            SQLCommand.ExecuteNonQuery()

            strDescError1 = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            If strDescError1.ToUpper.Trim <> "OK" Then
                MiTransaccionSQL.Rollback()
                Exit Function
            End If


            '+ INSERTAR
            If Not (IsNothing(DS_GrupoCuentas)) Then
                If DS_GrupoCuentas.Tables(0).Rows.Count <> Nothing Then
                    For Each dtrRegristro As DataRow In DS_GrupoCuentas.Tables(0).Rows

                        SQLCommand = New SqlClient.SqlCommand("Rcp_RelGrupoCuentas_XCuentas_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)
                        SQLCommand.CommandType = CommandType.StoredProcedure
                        SQLCommand.CommandText = "Rcp_RelGrupoCuentas_XCuentas_Mantencion"
                        SQLCommand.Parameters.Clear()

                        SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = "INSERTAR"
                        SQLCommand.Parameters.Add("pOrdenCuenta", SqlDbType.Float, 10).Value = intOrdenCuenta
                        SQLCommand.Parameters.Add("pIdGrupoCuentas ", SqlDbType.Float, 10).Value = intIdGrupoCuentas
                        SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = dtrRegristro("IdCuenta").ToString



                        '...Resultado
                        Dim pSalInstPort As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                        pSalInstPort.Direction = Data.ParameterDirection.Output
                        SQLCommand.Parameters.Add(pSalInstPort)

                        SQLCommand.ExecuteNonQuery()

                        strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
                        If strDescError.ToUpper.Trim <> "OK" Then
                            Exit For
                        End If
                    Next
                Else
                    strDescError = "OK"
                End If
            Else
                strDescError = "OK"
            End If
            If strDescError.ToUpper.Trim = "OK" And strDescError1.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            RelCuentasGrupo_Cuentas_Mantenedor = strDescError
        End Try
    End Function
End Class
