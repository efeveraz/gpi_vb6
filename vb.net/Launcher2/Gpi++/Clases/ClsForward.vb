﻿Public Class ClsForward

    Public Function TipoCambio_Buscar(ByVal strCodMonedaFuente As String, _
                                      ByVal strCodMonedaDestino As String, _
                                      ByVal strColumnas As String, _
                                      ByRef strRetorno As String) As DataSet

        Dim LDS_TipoCambio As New DataSet
        Dim LDS_TipoCambio_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Forward_Buscar_TipoCambio"
        Lstr_NombreTabla = "TIPOCAMBIO"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pCodMonedaFuente", SqlDbType.VarChar, 3).Value = strCodMonedaFuente
            SQLCommand.Parameters.Add("pCodMonedaDestino", SqlDbType.VarChar, 3).Value = strCodMonedaDestino

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_TipoCambio, Lstr_NombreTabla)

            LDS_TipoCambio_Aux = LDS_TipoCambio

            If strColumnas.Trim = "" Then
                LDS_TipoCambio_Aux = LDS_TipoCambio
            Else
                LDS_TipoCambio_Aux = LDS_TipoCambio.Copy
                For Each Columna In LDS_TipoCambio.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_TipoCambio_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_TipoCambio_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            Return LDS_TipoCambio_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function ValorTipoCambio_Buscar(ByVal dblIdTipoCambio As Double, _
                                           ByVal strCodMonedaFuente As String, _
                                           ByVal strCodMonedaDestino As String, _
                                           ByVal strFecha As String, _
                                           ByVal strColumnas As String, _
                                           ByRef strRetorno As String) As DataSet

        Dim LDS_TipoCambio As New DataSet
        Dim LDS_TipoCambio_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Forward_ValorTipoCambio_Buscar"
        Lstr_NombreTabla = "VALORTIPOCAMBIO"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdTipoCambio", SqlDbType.Float, 6).Value = dblIdTipoCambio
            SQLCommand.Parameters.Add("pCodMonedaFuente", SqlDbType.VarChar, 3).Value = strCodMonedaFuente
            SQLCommand.Parameters.Add("pCodMonedaDestino", SqlDbType.VarChar, 3).Value = strCodMonedaDestino
            SQLCommand.Parameters.Add("pFecha", SqlDbType.Char, 10).Value = strFecha

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_TipoCambio, Lstr_NombreTabla)

            LDS_TipoCambio_Aux = LDS_TipoCambio

            If strColumnas.Trim = "" Then
                LDS_TipoCambio_Aux = LDS_TipoCambio
            Else
                LDS_TipoCambio_Aux = LDS_TipoCambio.Copy
                For Each Columna In LDS_TipoCambio.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_TipoCambio_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_TipoCambio_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            Return LDS_TipoCambio_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function Forward_Buscar(ByVal strColumnas As String, _
                                           ByRef strRetorno As String, _
                                           Optional ByVal dblIdOpPlazo As Double = 0, _
                                           Optional ByVal dblIdCuenta As Double = 0, _
                                           Optional ByVal strEstadoOpPlazo As String = "") As DataSet

        Dim LDS_Forward As New DataSet
        Dim LDS_Forward_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Forward_Buscar"
        Lstr_NombreTabla = "FORWARD"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdOpPlazo", SqlDbType.Float, 6).Value = IIf(dblIdOpPlazo = 0, DBNull.Value, dblIdOpPlazo)
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = IIf(dblIdCuenta = 0, DBNull.Value, dblIdCuenta)
            SQLCommand.Parameters.Add("pEstadoOpPlazo", SqlDbType.Char, 3).Value = IIf(strEstadoOpPlazo = "", DBNull.Value, strEstadoOpPlazo)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Forward, Lstr_NombreTabla)

            LDS_Forward_Aux = LDS_Forward

            If strColumnas.Trim = "" Then
                LDS_Forward_Aux = LDS_Forward
            Else
                LDS_Forward_Aux = LDS_Forward.Copy
                For Each Columna In LDS_Forward.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Forward_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Forward_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            Return LDS_Forward_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function Forward_Mantenedor(ByVal strAccion As String, ByVal dblIdOpPLazo As Double, ByVal strNumeroOpPlazo As String, _
                                          ByVal strGlosa As String, ByVal dblFolioDocAdj As Double, ByVal strCodEjecutivo As String, _
                                          ByVal strEstadoOpPlazo As String, ByVal strFechaOpPlazo As String, ByVal strFechaVctoOpPlazo As String, _
                                          ByVal strFechaLiqOpPlazo As String, ByVal dblMontoCapital As Double, ByVal dblMontoPlazo As Double, ByVal dblMontoCapitalAjustado As Double, _
                                          ByVal dblPlazoDias As Double, ByVal dblSpread As Double, ByVal dblBaseDiasTasaOpPlazo As Double, _
                                          ByVal dblValorPresenteOpPlazo As Double, ByVal dblPrecioContOpPlazo As Double, ByVal dblPrecioPlazOpPlazo As Double, _
                                          ByVal dblPorcentajeGtiaOpPlazo As Double, ByVal dblTipoOpContado As Double, ByVal strCodTipoOperacion As String, _
                                          ByVal dblIdTipoCambio As Double, ByVal dblIdTipoCambioCompensacion As Double, ByVal strCodMonedaOrigen As String, ByVal strCodMonedaDestino As String, _
                                          ByVal strTipoLiquidacion As String, ByVal dblTasa As Double, ByVal dblIdContraparte As Double, ByVal dblIdCuenta As Double, ByVal strSubClaseInstrumento As String, _
                                          ByVal gdteFechaSistema As DateTime, ByVal glngIdUsuario As Double, ByVal dblCantidad As Double, ByRef strDescError As String) As String

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_Cartera_Op_Plazo_Mantenedor", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Cartera_Op_Plazo_Mantenedor"
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = strAccion
            SQLCommand.Parameters.Add("pIdOpPlazo", SqlDbType.Float, 9).Value = dblIdOpPLazo
            SQLCommand.Parameters.Add("pNumeroOpPlazo", SqlDbType.VarChar, 20).Value = IIf(strNumeroOpPlazo = "", DBNull.Value, strNumeroOpPlazo)
            SQLCommand.Parameters.Add("pGlosa", SqlDbType.VarChar, 50).Value = IIf(strGlosa = "", DBNull.Value, strGlosa)
            SQLCommand.Parameters.Add("pFolioDocAdj", SqlDbType.Float, 9).Value = IIf(dblFolioDocAdj = 0, DBNull.Value, dblFolioDocAdj)
            SQLCommand.Parameters.Add("pCodEjecutivo", SqlDbType.VarChar, 10).Value = IIf(strCodEjecutivo = "", DBNull.Value, strCodEjecutivo)
            SQLCommand.Parameters.Add("pEstadoOpPlazo", SqlDbType.VarChar, 3).Value = IIf(strEstadoOpPlazo = "", DBNull.Value, strEstadoOpPlazo)
            SQLCommand.Parameters.Add("pFechaOpPlazo", SqlDbType.VarChar, 10).Value = IIf(strFechaOpPlazo = "", DBNull.Value, strFechaOpPlazo)
            SQLCommand.Parameters.Add("pFechaVctoOpPlazo", SqlDbType.VarChar, 10).Value = IIf(strFechaVctoOpPlazo = "", DBNull.Value, strFechaVctoOpPlazo)
            SQLCommand.Parameters.Add("pFechaLiqOpPlazo", SqlDbType.VarChar, 10).Value = IIf(strFechaLiqOpPlazo = "", DBNull.Value, strFechaLiqOpPlazo)
            SQLCommand.Parameters.Add("pMontoCapital", SqlDbType.Float, 13).Value = IIf(dblMontoCapital = 0, DBNull.Value, dblMontoCapital)
            SQLCommand.Parameters.Add("pMontoPlazo", SqlDbType.Float, 13).Value = IIf(dblMontoPlazo = 0, DBNull.Value, dblMontoPlazo)
            SQLCommand.Parameters.Add("pMontoCapitalAjustado", SqlDbType.Float, 13).Value = IIf(dblMontoCapitalAjustado = 0, DBNull.Value, dblMontoCapitalAjustado)
            SQLCommand.Parameters.Add("pPlazoDias", SqlDbType.Float, 9).Value = dblPlazoDias
            SQLCommand.Parameters.Add("pSpread", SqlDbType.Float, 13).Value = IIf(dblSpread = 0, DBNull.Value, dblSpread)
            SQLCommand.Parameters.Add("pBaseDiasTasaOpPlazo", SqlDbType.Float, 9).Value = IIf(dblBaseDiasTasaOpPlazo = 0, DBNull.Value, dblBaseDiasTasaOpPlazo)
            SQLCommand.Parameters.Add("pValorPresenteOpPlazo", SqlDbType.Float, 13).Value = IIf(dblValorPresenteOpPlazo = 0, DBNull.Value, dblValorPresenteOpPlazo)
            SQLCommand.Parameters.Add("pPrecioContOpPlazo", SqlDbType.Float, 13).Value = IIf(dblPrecioContOpPlazo = 0, DBNull.Value, dblPrecioContOpPlazo)
            SQLCommand.Parameters.Add("pPrecioPlazOpPlazo", SqlDbType.Float, 13).Value = IIf(dblPrecioPlazOpPlazo = 0, DBNull.Value, dblPrecioPlazOpPlazo)
            SQLCommand.Parameters.Add("pPorcentajeGtiaOpPlazo", SqlDbType.Float, 13).Value = IIf(dblPorcentajeGtiaOpPlazo = 0, DBNull.Value, dblPorcentajeGtiaOpPlazo)
            SQLCommand.Parameters.Add("pTipoOpContado", SqlDbType.Float, 9).Value = dblTipoOpContado
            SQLCommand.Parameters.Add("pCodTipoOperacion", SqlDbType.VarChar, 10).Value = IIf(strCodTipoOperacion = "", DBNull.Value, strCodTipoOperacion)
            SQLCommand.Parameters.Add("pIdTipoCambio", SqlDbType.Float, 9).Value = IIf(dblIdTipoCambio = 0, DBNull.Value, dblIdTipoCambio)
            SQLCommand.Parameters.Add("pIdTipoCambioCompensacion", SqlDbType.Float, 9).Value = IIf(dblIdTipoCambioCompensacion = 0, DBNull.Value, dblIdTipoCambioCompensacion)
            SQLCommand.Parameters.Add("pCodMonedaOrigen", SqlDbType.VarChar, 3).Value = IIf(strCodMonedaOrigen = "", DBNull.Value, strCodMonedaOrigen)
            SQLCommand.Parameters.Add("pCodMonedaDestino", SqlDbType.VarChar, 3).Value = IIf(strCodMonedaDestino = "", DBNull.Value, strCodMonedaDestino)
            SQLCommand.Parameters.Add("pTipoLiquidacion", SqlDbType.Char, 3).Value = IIf(strTipoLiquidacion = "", DBNull.Value, strTipoLiquidacion)
            SQLCommand.Parameters.Add("pTasa", SqlDbType.Float, 13).Value = IIf(dblTasa = 0, DBNull.Value, dblTasa)
            SQLCommand.Parameters.Add("pIdContraparte", SqlDbType.Float, 10).Value = IIf(dblIdContraparte = 0, DBNull.Value, dblIdContraparte)
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = IIf(dblIdCuenta = 0, DBNull.Value, dblIdCuenta)
            SQLCommand.Parameters.Add("pInsertFch", SqlDbType.DateTime).Value = gdteFechaSistema
            SQLCommand.Parameters.Add("pInsertUsuario", SqlDbType.Float, 6).Value = IIf(glngIdUsuario = 0, DBNull.Value, glngIdUsuario)
            SQLCommand.Parameters.Add("pSubClaseInstrumento", SqlDbType.VarChar, 10).Value = strSubClaseInstrumento
            SQLCommand.Parameters.Add("pCantidad", SqlDbType.Float, 13).Value = IIf(dblCantidad = 0, DBNull.Value, dblCantidad)



            '...(Identity)
            Dim pSalInstr As New SqlClient.SqlParameter("pGenerado", SqlDbType.Float, 10)
            pSalInstr.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalInstr)

            '...Resultado
            Dim pSalResultInstr As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultInstr.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultInstr)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If
            '___________________________________________________________________________________________________________

            If strDescError = "OK" Then
                If gobjParametro.Tesoreria_Envio.ValorParametro = "S" Then
                    Dim lcInterfaceTesoreria As New clsInterfaceTesoreria
                    lcInterfaceTesoreria.InformarIngresoOpPlazo(SQLCommand.Parameters("pGenerado").Value.ToString.Trim, "FWDCONT")
                End If
            End If
        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error: " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            Forward_Mantenedor = strDescError
        End Try
    End Function

    Public Function OperacionPlazo_Guardar(ByVal strFamilia As String, ByVal strCodTipoOperacion As String, _
                                           ByVal lngIdOperacionConcepto As Long, ByVal strClaseInstrumento As String, _
                                           ByVal strSubClaseInstrumento As String, ByVal strNumeroOpPlazo As String, _
                                           ByVal strGlosa As String, ByVal strEstadoOpPlazo As String, _
                                           ByVal strFechaOpPlazo As String, ByVal strFechaVctoOpPlazo As String, _
                                           ByVal strFechaLiqOpPlazo As String, ByVal dblMontoCapital As Double, _
                                           ByVal dblMontoPlazo As Double, ByVal dblPlazoDias As Double, _
                                           ByVal dblParidad As Double, ByVal dblBaseDiasTasaOpPlazo As Double, _
                                           ByVal dblIdTipoCambio As Double, ByVal dblIdTipoCambioCompensacion As Double, _
                                           ByVal strCodMonedaOrigen As String, ByVal strCodMonedaDestino As String, _
                                           ByVal strTipoLiquidacion As String, ByVal dblTasa As Double, _
                                           ByVal dblIdContraparte As Double, ByVal dblIdCuenta As Double, _
                                           ByVal dblIdCustodio As Double, ByVal strTipoCustodio As String, _
                                           ByVal strNemotecnico As String, ByVal dblIdEmisor As Double, _
                                           ByVal dblIdTrader As Double) As String

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        Dim strDescError As String = ""
        Dim strProcedure As String = "Rcp_OperacionPlazo_Guardar"

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand(strProcedure, MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = strProcedure
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()


            SQLCommand.Parameters.Add("pFamilia", SqlDbType.VarChar, 10).Value = IIf(strFamilia = "", DBNull.Value, strFamilia)
            SQLCommand.Parameters.Add("pCodTipoOperacion", SqlDbType.VarChar, 10).Value = IIf(strCodTipoOperacion = "", DBNull.Value, strCodTipoOperacion)
            SQLCommand.Parameters.Add("pIdOperacionConcepto", SqlDbType.Int, 3).Value = IIf(lngIdOperacionConcepto = 0, DBNull.Value, lngIdOperacionConcepto)
            SQLCommand.Parameters.Add("pClaseInstrumento ", SqlDbType.VarChar, 15).Value = IIf(strClaseInstrumento = "", DBNull.Value, strClaseInstrumento)
            SQLCommand.Parameters.Add("pSubClaseInstrumento", SqlDbType.VarChar, 15).Value = strSubClaseInstrumento
            SQLCommand.Parameters.Add("pNumeroOpPlazo", SqlDbType.VarChar, 20).Value = IIf(strNumeroOpPlazo = "", DBNull.Value, strNumeroOpPlazo)
            SQLCommand.Parameters.Add("pGlosa", SqlDbType.VarChar, 50).Value = IIf(strGlosa = "", DBNull.Value, strGlosa)
            SQLCommand.Parameters.Add("pEstadoOpPlazo", SqlDbType.VarChar, 3).Value = IIf(strEstadoOpPlazo = "", DBNull.Value, strEstadoOpPlazo)
            SQLCommand.Parameters.Add("pFechaOpPlazo", SqlDbType.VarChar, 10).Value = IIf(strFechaOpPlazo = "", DBNull.Value, strFechaOpPlazo)
            SQLCommand.Parameters.Add("pFechaVctoOpPlazo", SqlDbType.VarChar, 10).Value = IIf(strFechaVctoOpPlazo = "", DBNull.Value, strFechaVctoOpPlazo)
            SQLCommand.Parameters.Add("pFechaLiqOpPlazo", SqlDbType.VarChar, 10).Value = IIf(strFechaLiqOpPlazo = "", DBNull.Value, strFechaLiqOpPlazo)
            SQLCommand.Parameters.Add("pMontoCapital", SqlDbType.Float, 13).Value = IIf(dblMontoCapital = 0, DBNull.Value, dblMontoCapital)
            SQLCommand.Parameters.Add("pMontoPlazo", SqlDbType.Float, 13).Value = IIf(dblMontoPlazo = 0, DBNull.Value, dblMontoPlazo)
            SQLCommand.Parameters.Add("pPlazoDias", SqlDbType.Float, 9).Value = IIf(dblPlazoDias = 0, DBNull.Value, dblPlazoDias)
            SQLCommand.Parameters.Add("pParidad", SqlDbType.Float, 13).Value = IIf(dblParidad = 0, DBNull.Value, dblParidad)
            SQLCommand.Parameters.Add("pBaseDiasTasaOpPlazo", SqlDbType.Float, 9).Value = IIf(dblBaseDiasTasaOpPlazo = 0, DBNull.Value, dblBaseDiasTasaOpPlazo)
            SQLCommand.Parameters.Add("pIdTipoCambio", SqlDbType.Float, 9).Value = IIf(dblIdTipoCambio = 0, DBNull.Value, dblIdTipoCambio)
            SQLCommand.Parameters.Add("pIdTipoCambioCompensacion", SqlDbType.Float, 9).Value = IIf(dblIdTipoCambioCompensacion = 0, DBNull.Value, dblIdTipoCambioCompensacion)
            SQLCommand.Parameters.Add("pCodMonedaOrigen", SqlDbType.VarChar, 3).Value = IIf(strCodMonedaOrigen = "", DBNull.Value, strCodMonedaOrigen)
            SQLCommand.Parameters.Add("pCodMonedaDestino", SqlDbType.VarChar, 3).Value = IIf(strCodMonedaDestino = "", DBNull.Value, strCodMonedaDestino)
            SQLCommand.Parameters.Add("pTipoLiquidacion", SqlDbType.Char, 3).Value = IIf(strTipoLiquidacion = "", DBNull.Value, strTipoLiquidacion)
            SQLCommand.Parameters.Add("pTasa", SqlDbType.Float, 13).Value = dblTasa
            SQLCommand.Parameters.Add("pIdContraparte", SqlDbType.Float, 10).Value = IIf(dblIdContraparte = 0, DBNull.Value, dblIdContraparte)
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = IIf(dblIdCuenta = 0, DBNull.Value, dblIdCuenta)
            SQLCommand.Parameters.Add("pIdCustodio", SqlDbType.Float, 10).Value = IIf(dblIdCustodio = 0, DBNull.Value, dblIdCustodio)
            SQLCommand.Parameters.Add("pTipoCustodio", SqlDbType.VarChar, 10).Value = IIf(strTipoCustodio = "", DBNull.Value, strTipoCustodio)
            SQLCommand.Parameters.Add("pNemotecnico", SqlDbType.VarChar, 50).Value = IIf(strNemotecnico = "", DBNull.Value, strNemotecnico)
            SQLCommand.Parameters.Add("pIdEmisor", SqlDbType.Float, 10).Value = IIf(dblIdEmisor = 0, DBNull.Value, dblIdEmisor)
            SQLCommand.Parameters.Add("pIdTrader", SqlDbType.Float, 18).Value = IIf(dblIdTrader = 0, DBNull.Value, dblIdTrader)
            SQLCommand.Parameters.Add("pInsertUsuario", SqlDbType.Float, 6).Value = IIf(glngIdUsuario = 0, DBNull.Value, glngIdUsuario)

            '...Resultado
            Dim pSalResultInstr As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultInstr.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultInstr)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()

            End If
            '___________________________________________________________________________________________________________

            'If strDescError = "OK" Then
            '  If gobjParametro.Tesoreria_Envio.ValorParametro = "S" Then
            '    Dim lcInterfaceTesoreria As New clsInterfaceTesoreria
            '    lcInterfaceTesoreria.InformarIngresoOpPlazo(SQLCommand.Parameters("pGenerado").Value.ToString.Trim, "FWDCONT")
            '  End If
            'End If
        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error: " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            OperacionPlazo_Guardar = strDescError
        End Try
    End Function

    Public Function CarteraForward_Buscar(ByVal strColumnas As String, _
                                          ByRef strRetorno As String, _
                                          ByVal strTipoLista As String, _
                                          ByVal strLista As String, _
                                          ByVal strFecha As String) As DataSet

        Dim lDS_Forward As New DataSet
        Dim lDS_Forward_Aux As New DataSet

        Dim lArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim lInt_Col As Integer
        Dim lInt_NomCol As String
        Dim lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_ForwardCartera_Buscar"
        lstr_NombreTabla = "FORWARD"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Float, 10).Value = glngNegocioOperacion
            SQLCommand.Parameters.Add("pTipoLista", SqlDbType.VarChar, 1).Value = IIf(strTipoLista = "", "C", strTipoLista)
            SQLCommand.Parameters.Add("pLista", SqlDbType.Text, 60000).Value = IIf(strLista = "", DBNull.Value, strLista)
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Float, 6).Value = glngIdUsuario
            SQLCommand.Parameters.Add("pFecha", SqlDbType.VarChar, 10).Value = IIf(strFecha = "", DBNull.Value, strFecha)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(lDS_Forward, lstr_NombreTabla)

            lDS_Forward_Aux = lDS_Forward

            If strColumnas.Trim = "" Then
                lDS_Forward_Aux = lDS_Forward
            Else
                lDS_Forward_Aux = lDS_Forward.Copy
                For Each Columna In lDS_Forward.Tables(lstr_NombreTabla).Columns
                    Remove = True
                    For lInt_Col = 0 To lArr_NombreColumna.Length - 1
                        lInt_NomCol = lArr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = lInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        lDS_Forward_Aux.Tables(lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For lInt_Col = 0 To lArr_NombreColumna.Length - 1
                lInt_NomCol = lArr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                For Each Columna In lDS_Forward_Aux.Tables(lstr_NombreTabla).Columns
                    If Columna.ColumnName = lInt_NomCol Then
                        Columna.SetOrdinal(lInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            Return lDS_Forward_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function ForwardOperaciones_Buscar(ByVal dblpIdOpeacion As Double, _
                                              ByVal strpFechaDesde As String, _
                                              ByVal strpFechaHasta As String, _
                                              ByVal strpColumnas As String, _
                                              ByRef strpRetorno As String, _
                                     Optional ByRef strCodEstado As String = "", _
                                     Optional ByVal StrListaCuentas As String = "", _
                                     Optional ByRef StrTipoReporte As String = "", _
                                     Optional ByVal StrNemotecnico As String = "") As DataSet

        Dim lDS_Operaciones As New DataSet
        Dim lDS_Operaciones_Aux As New DataSet
        Dim larr_NombreColumna() As String = Split(strpColumnas, ",")
        Dim lInt_Col As Integer = 0
        Dim lInt_NomCol As String = ""
        Dim lstr_NombreTabla As String = ""
        Dim lstrColumna As DataColumn
        Dim lblnRemove As Boolean = True
        Dim lstrProcedimiento As String = ""
        Dim strDescError As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        lstrProcedimiento = "Rcp_ForwardOperacion_Buscar"
        lstr_NombreTabla = "OPERACIONES"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, Connect.Conexion)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandTimeout = 0
            SQLCommand.CommandText = lstrProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("pIdOpeacion", SqlDbType.Float, 10).Value = IIf(dblpIdOpeacion = 0, DBNull.Value, dblpIdOpeacion)
            SQLCommand.Parameters.Add("pFechaDesde", SqlDbType.Char, 10).Value = strpFechaDesde
            SQLCommand.Parameters.Add("pFechaHasta", SqlDbType.Char, 10).Value = strpFechaHasta
            SQLCommand.Parameters.Add("pCodEstado", SqlDbType.Char, 1).Value = IIf(strCodEstado = "", DBNull.Value, strCodEstado)
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Float, 10).Value = glngNegocioOperacion
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Float, 6).Value = IIf(glngIdUsuario = 0, DBNull.Value, glngIdUsuario)
            SQLCommand.Parameters.Add("pListaCuentas", SqlDbType.Text, 60000).Value = StrListaCuentas
            SQLCommand.Parameters.Add("pTipoReporte", SqlDbType.Char, 1).Value = IIf(StrTipoReporte = "", DBNull.Value, StrTipoReporte)
            SQLCommand.Parameters.Add("pNemotecnico", SqlDbType.VarChar, 25).Value = IIf(StrNemotecnico = "", DBNull.Value, StrNemotecnico)

            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)



            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(lDS_Operaciones, lstr_NombreTabla)
            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            lDS_Operaciones_Aux = lDS_Operaciones

            If strDescError.ToUpper.Trim <> "OK" Then
                gFun_ResultadoMsg("2|" & strDescError, "Buscar Cuentas")
                strpRetorno = "OK"
                lDS_Operaciones_Aux = Nothing
            Else
                If strpColumnas.Trim = "" Then
                    lDS_Operaciones_Aux = lDS_Operaciones
                Else
                    lDS_Operaciones_Aux = lDS_Operaciones.Copy
                    For Each lstrColumna In lDS_Operaciones.Tables(lstr_NombreTabla).Columns
                        lblnRemove = True
                        For lInt_Col = 0 To larr_NombreColumna.Length - 1
                            lInt_NomCol = larr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                            If lstrColumna.ColumnName = lInt_NomCol Then
                                lblnRemove = False
                            End If
                        Next
                        If lblnRemove Then
                            lDS_Operaciones_Aux.Tables(lstr_NombreTabla).Columns.Remove(lstrColumna.ColumnName)
                        End If
                    Next
                End If

                For lInt_Col = 0 To larr_NombreColumna.Length - 1
                    lInt_NomCol = larr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                    For Each lstrColumna In lDS_Operaciones_Aux.Tables(lstr_NombreTabla).Columns
                        If lstrColumna.ColumnName = lInt_NomCol Then
                            lstrColumna.SetOrdinal(lInt_Col)
                            Exit For
                        End If
                    Next
                Next

                strpRetorno = "OK"
            End If
            lDS_Operaciones.Dispose()
            Return lDS_Operaciones_Aux

        Catch ex As Exception
            strpRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function


    Public Function BuscarPuntoCurva(ByVal strColumnas As String, _
                                     ByVal strTipoPC As String, _
                                     ByRef strRetorno As String) As DataSet

        Dim LDS_TipoCambio As New DataSet
        Dim LDS_TipoCambio_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_PuntoCurva_Buscar"
        Lstr_NombreTabla = "PuntoCurva"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("@pTipoPC", SqlDbType.Char, 3).Value = strTipoPC

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_TipoCambio, Lstr_NombreTabla)

            LDS_TipoCambio_Aux = LDS_TipoCambio

            If strColumnas.Trim = "" Then
                LDS_TipoCambio_Aux = LDS_TipoCambio
            Else
                LDS_TipoCambio_Aux = LDS_TipoCambio.Copy
                For Each Columna In LDS_TipoCambio.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_TipoCambio_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_TipoCambio_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_TipoCambio.Dispose()

            Return LDS_TipoCambio_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function PuntoCurva_Mantenedor(ByVal strAccion As String, _
                                          ByRef dblId_Val_PuntoCurvas As Double, _
                                          ByVal strTipo_Valoriza As String, _
                                          ByVal lngId_Tipo_Cambio As Long, _
                                          ByVal strCod_Moneda As String, _
                                          ByVal strDsc_Val_PuntoCurvas As String, _
                                          ByVal strTipo_Operacion As String, _
                                          ByVal strBase As String) As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction
        Dim strProcedimiento As String

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion
        strProcedimiento = "Rcp_PuntoCurvas_Mantencion"

        Try
            SQLCommand = New SqlClient.SqlCommand(strProcedimiento, MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = strProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 10).Value = strAccion
            'SQLCommand.Parameters.Add("pId_Tipo_Cambio", SqlDbType.Float, 6).Value = dblId_TipoCambio
            '...Id_Tipo_Cambio
            Dim ParametroSal0 As New SqlClient.SqlParameter("pId_Val_PuntoCurvas", SqlDbType.Float, 18)
            ParametroSal0.Direction = Data.ParameterDirection.InputOutput
            ParametroSal0.Value = dblId_Val_PuntoCurvas  'IIf(dblId_Val_PuntoCurvas = "", DBNull.Value, dblId_Val_PuntoCurvas)
            SQLCommand.Parameters.Add(ParametroSal0)

            SQLCommand.Parameters.Add("pTipo_Valoriza", SqlDbType.VarChar, 3).Value = strTipo_Valoriza
            SQLCommand.Parameters.Add("pId_Tipo_Cambio", SqlDbType.Float, 6).Value = IIf(lngId_Tipo_Cambio = 0, DBNull.Value, lngId_Tipo_Cambio)
            SQLCommand.Parameters.Add("pCod_Moneda", SqlDbType.VarChar, 3).Value = IIf(strCod_Moneda = "", DBNull.Value, strCod_Moneda)
            SQLCommand.Parameters.Add("pDsc_Val_PuntoCurvas", SqlDbType.VarChar, 100).Value = strDsc_Val_PuntoCurvas
            SQLCommand.Parameters.Add("pTipo_Operacion", SqlDbType.VarChar, 1).Value = strTipo_Operacion
            SQLCommand.Parameters.Add("pBase", SqlDbType.Float, 10).Value = IIf(strBase = 0, DBNull.Value, CInt(strBase))
            SQLCommand.Parameters.Add("pId_usuario", SqlDbType.Float, 10).Value = glngIdUsuario
            '...Resultado
            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            dblId_Val_PuntoCurvas = SQLCommand.Parameters("pId_Val_PuntoCurvas").Value

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error al actualizar " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            PuntoCurva_Mantenedor = strDescError
        End Try
    End Function

    Public Sub ValorPuntoCargas_Mantenedor(ByVal DS_ValorPuntoCargas As DataSet, _
                                           ByRef strRetorno As String)
        Dim DR_Fila As DataRow = Nothing
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Entra As Boolean = True

        Dim Connect As Cls_Conexion = New Cls_Conexion

        'Abre la conexion
        Connect.Abrir()

        Dim MiTransaccion As SqlClient.SqlTransaction


        MiTransaccion = Connect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_ValorPuntoCurvas_Mantencion", MiTransaccion.Connection, MiTransaccion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_ValorPuntoCurvas_Mantencion"

            For Each DR_Fila In DS_ValorPuntoCargas.Tables(0).Select("OPERACION = 1")
                SQLCommand.Parameters.Clear()
                Entra = True
                'INSERTA
                If IsDBNull(DR_Fila("ID_VAL_PUNTOCURVAS_DET")) And (Not IsDBNull(DR_Fila("VALOR"))) Then
                    SQLCommand.Parameters.Add("@pAccion", SqlDbType.VarChar, 10).Value = "INSERTAR"
                    SQLCommand.Parameters.Add("@pIdValPuntoCurvas", SqlDbType.Int).Value = DR_Fila("ID_VAL_PUNTOCURVAS")
                    SQLCommand.Parameters.Add("@pfecha", SqlDbType.Char, 10).Value = DR_Fila("FECHA")
                    SQLCommand.Parameters.Add("@pPlazo", SqlDbType.Decimal, 18).Value = DR_Fila("PLAZO")
                    SQLCommand.Parameters.Add("@pValor", SqlDbType.Decimal, 18).Value = DR_Fila("VALOR")
                    SQLCommand.Parameters.Add("@pIdUsuario", SqlDbType.Decimal, 18).Value = glngIdUsuario

                    Dim ParametroIdValorTipoCambio As New SqlClient.SqlParameter("@pIdValPuntoCurvas_Det", SqlDbType.Int)
                    ParametroIdValorTipoCambio.Direction = Data.ParameterDirection.Output
                    SQLCommand.Parameters.Add(ParametroIdValorTipoCambio)
                ElseIf (Not (IsDBNull(DR_Fila("ID_VAL_PUNTOCURVAS_DET")))) And (Not IsDBNull(DR_Fila("VALOR"))) Then 'MODIFICA
                    SQLCommand.Parameters.Add("@pAccion", SqlDbType.VarChar, 10).Value = "MODIFICAR"

                    SQLCommand.Parameters.Add("@pIdValPuntoCurvas_Det", SqlDbType.Int).Value = DR_Fila("ID_VAL_PUNTOCURVAS_DET")
                    SQLCommand.Parameters.Add("@pIdValPuntoCurvas", SqlDbType.Int).Value = DR_Fila("ID_VAL_PUNTOCURVAS")
                    SQLCommand.Parameters.Add("@pfecha", SqlDbType.Char, 10).Value = DR_Fila("FECHA")
                    SQLCommand.Parameters.Add("@pPlazo", SqlDbType.Decimal, 18).Value = DR_Fila("PLAZO")
                    SQLCommand.Parameters.Add("@pValor", SqlDbType.Decimal, 18).Value = DR_Fila("VALOR")
                    SQLCommand.Parameters.Add("@pIdUsuario", SqlDbType.Decimal, 18).Value = glngIdUsuario

                ElseIf (Not (IsDBNull(DR_Fila("ID_VAL_PUNTOCURVAS_DET")))) And (IsDBNull(DR_Fila("VALOR"))) Then 'ELIMINA
                    SQLCommand.Parameters.Add("@pAccion", SqlDbType.VarChar, 10).Value = "ELIMINAR"
                    SQLCommand.Parameters.Add("@pIdValorTipoCambio", SqlDbType.Int).Value = DR_Fila("ID_VALOR_TIPO_CAMBIO")
                    SQLCommand.Parameters.Add("@pIdUsuario", SqlDbType.Decimal, 18).Value = glngIdUsuario
                Else
                    strRetorno = "OK"
                    Entra = False
                    'MiTransaccion.Commit()
                    'Exit Sub
                End If

                If Entra Then
                    Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                    ParametroSal2.Direction = Data.ParameterDirection.Output
                    SQLCommand.Parameters.Add(ParametroSal2)
                    SQLCommand.ExecuteNonQuery()
                    strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim
                    If Trim(strRetorno) <> "OK" Then
                        Exit For
                    End If
                End If
            Next
            If Trim(strRetorno) = "OK" Then
                MiTransaccion.Commit()
            Else
                MiTransaccion.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccion.Rollback()
            strRetorno = "Error al grabar Valor Tipo Cambio." & vbCr & Ex.Message
        Finally
            Connect.Cerrar()
        End Try


    End Sub

    Public Sub ValorPuntoCurvasCargaValores(ByVal idTipoCambio As Integer, _
                                           ByVal DS_Cargas As DataSet, _
                                           ByRef strRetorno As String)
        Dim DR_Fila As DataRow = Nothing
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        'Abre la conexion
        Connect.Abrir()

        Dim MiTransaccion As SqlClient.SqlTransaction


        MiTransaccion = Connect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_ValorPuntoCurvas_CargaValor", MiTransaccion.Connection, MiTransaccion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_ValorPuntoCurvas_CargaValor"

            For Each DR_Fila In DS_Cargas.Tables(0).Rows
                SQLCommand.Parameters.Clear()

                'INSERTA
                If IsDBNull(DR_Fila("Fecha")) And IsDBNull(DR_Fila("VALOR")) Then
                    strRetorno = "OK"
                    Exit For
                End If
                SQLCommand.Parameters.Add("@pId_Val_PuntoCurvas", SqlDbType.Int).Value = idTipoCambio
                SQLCommand.Parameters.Add("@pfecha", SqlDbType.DateTime).Value = DR_Fila("FECHA")
                SQLCommand.Parameters.Add("@pPlazo", SqlDbType.Decimal, 18).Value = DR_Fila("PLAZO")
                SQLCommand.Parameters.Add("@pValor", SqlDbType.Decimal, 18).Value = DR_Fila("VALOR")
                SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Float, 6).Value = IIf(glngIdUsuario = 0, DBNull.Value, glngIdUsuario)

                Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                ParametroSal2.Direction = Data.ParameterDirection.Output
                SQLCommand.Parameters.Add(ParametroSal2)

                SQLCommand.ExecuteNonQuery()

                strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim

                If Trim(strRetorno) <> "OK" Then
                    Exit For
                End If
            Next
            If Trim(strRetorno) = "OK" Then
                MiTransaccion.Commit()
            Else
                MiTransaccion.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccion.Rollback()
            strRetorno = "Error al grabar Valor Tipo Cambio." & vbCr & Ex.Message
        Finally
            Connect.Cerrar()
        End Try


    End Sub

    Public Function BuscarValorPuntoCurvasFecha(ByVal strTipoVal As String, _
                                            ByVal strFecha As String, _
                                            ByVal strColumnas As String, _
                                            ByRef strRetorno As String) As DataSet

        Dim LDS_ValorTipoCambio As New DataSet
        Dim LDS_ValorTipoCambio_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        'Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        'LstrProcedimiento = "Rcp_ValorTipoCambio_BuscarTodosPorFecha"
        LstrProcedimiento = "Rcp_ValorPuntoCurvas_BuscarPorFecha"
        Lstr_NombreTabla = "ValorTipoCambio"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()


            SQLCommand.Parameters.Add("@pTipoVal", SqlDbType.Char, 10).Value = strTipoVal
            SQLCommand.Parameters.Add("@pFecha", SqlDbType.Char, 10).Value = strFecha

            Dim oParamSal As New SqlClient.SqlParameter("@pResultado", SqlDbType.Char, 200)
            oParamSal.Direction = ParameterDirection.Output
            SQLCommand.Parameters.Add(oParamSal)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_ValorTipoCambio, Lstr_NombreTabla)

            LDS_ValorTipoCambio_Aux = LDS_ValorTipoCambio

            'If strColumnas.Trim = "" Then
            '    LDS_ValorTipoCambio_Aux = LDS_ValorTipoCambio
            'Else
            '    LDS_ValorTipoCambio_Aux = LDS_ValorTipoCambio.Copy
            '    For Each Columna In LDS_ValorTipoCambio.Tables(Lstr_NombreTabla).Columns
            '        Remove = True
            '        For LInt_Col = 0 To LArr_NombreColumna.Length - 1
            '            LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
            '            If Columna.ColumnName = LInt_NomCol Then
            '                Remove = False
            '            End If
            '        Next
            '        If Remove Then
            '            LDS_ValorTipoCambio_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
            '        End If
            '    Next
            'End If

            'For LInt_Col = 0 To LArr_NombreColumna.Length - 1
            '    LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
            '    For Each Columna In LDS_ValorTipoCambio_Aux.Tables(Lstr_NombreTabla).Columns
            '        If Columna.ColumnName = LInt_NomCol Then
            '            Columna.SetOrdinal(LInt_Col)
            '            Exit For
            '        End If
            '    Next
            'Next

            strRetorno = SQLCommand.Parameters("@pResultado").Value.ToString.Trim

            LDS_ValorTipoCambio.Dispose()

            Return LDS_ValorTipoCambio_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function BuscarValorPuntoCurvasPorTipo(ByVal lngIdValPuntoCurvas As Long, _
                                                  ByVal strFechaDesde As String, _
                                                  ByVal strFechaHasta As String, _
                                                  ByVal strColumnas As String, _
                                                  ByRef strRetorno As String) As DataSet

        Dim LDS_ValorPuntoCurvas As New DataSet
        Dim LDS_ValorPuntoCurvas_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_ValorPuntoCurvas_BuscarPorTipo"
        Lstr_NombreTabla = "ValorTipoCambio"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("@pId_Val_PuntoCurvas", SqlDbType.Float).Value = lngIdValPuntoCurvas
            SQLCommand.Parameters.Add("@pfechaDesde", SqlDbType.Char, 10).Value = strFechaDesde
            SQLCommand.Parameters.Add("@pfechaHasta", SqlDbType.Char, 10).Value = strFechaHasta

            Dim oParamSal As New SqlClient.SqlParameter("@pResultado", SqlDbType.Char, 200)
            oParamSal.Direction = ParameterDirection.Output
            SQLCommand.Parameters.Add(oParamSal)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_ValorPuntoCurvas, Lstr_NombreTabla)

            LDS_ValorPuntoCurvas_Aux = LDS_ValorPuntoCurvas

            If strColumnas.Trim = "" Then
                LDS_ValorPuntoCurvas_Aux = LDS_ValorPuntoCurvas
            Else
                LDS_ValorPuntoCurvas_Aux = LDS_ValorPuntoCurvas.Copy
                For Each Columna In LDS_ValorPuntoCurvas.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_ValorPuntoCurvas_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_ValorPuntoCurvas_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = SQLCommand.Parameters("@pResultado").Value.ToString.Trim

            LDS_ValorPuntoCurvas.Dispose()

            Return LDS_ValorPuntoCurvas_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

End Class
