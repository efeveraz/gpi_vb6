﻿Public Class ClsNegocio
    Public Function Negocio_Ver(ByVal intIdNegocio As Integer, _
                                 ByVal strIdMoneda As String, _
                                 ByVal strIdBanco As String, _
                                 ByVal strColumnas As String, _
                                 ByRef strRetorno As String) As DataSet

        Dim LDS_Negocios As New DataSet
        Dim LDS_Negocios_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Negocios_Consultar"
        Lstr_NombreTabla = "Negocios"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Float, 9).Value = IIf(intIdNegocio = 0, DBNull.Value, intIdNegocio)
            SQLCommand.Parameters.Add("pIdMoneda", SqlDbType.VarChar, 3).Value = IIf(strIdMoneda.Trim = "", DBNull.Value, strIdMoneda.Trim)
            SQLCommand.Parameters.Add("pIdBanco", SqlDbType.VarChar, 3).Value = IIf(strIdBanco.Trim = "", DBNull.Value, strIdBanco.Trim)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Negocios, Lstr_NombreTabla)

            LDS_Negocios_Aux = LDS_Negocios

            If strColumnas.Trim = "" Then
                LDS_Negocios_Aux = LDS_Negocios
            Else
                LDS_Negocios_Aux = LDS_Negocios.Copy
                For Each Columna In LDS_Negocios.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Negocios_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Negocios_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_Negocios.Dispose()
            Return LDS_Negocios_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function
End Class
