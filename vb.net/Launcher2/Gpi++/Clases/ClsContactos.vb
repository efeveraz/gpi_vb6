﻿Public Class ClsContactos

    Public Function Contacto_Ver(ByVal lngIdContacto As Long, _
                                 ByVal lngIdTipoContacto As Long, _
                                 ByVal lngIdPesona As Long, _
                                 ByVal lngIdCliente As Long, _
                                 ByVal strColumnas As String, _
                                 ByRef strRetorno As String) As DataSet

        Dim LDS_Contacto As New DataSet
        Dim LDS_Contacto_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = "Contacto"
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = "Rcp_Contacto_Consultar"

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("@pIdContacto", SqlDbType.Float, 10).Value = IIf(lngIdContacto = 0, DBNull.Value, lngIdContacto)
            SQLCommand.Parameters.Add("@pIdTipoContacto", SqlDbType.Float, 10).Value = IIf(lngIdTipoContacto = 0, DBNull.Value, lngIdTipoContacto)
            SQLCommand.Parameters.Add("@pIdPersona", SqlDbType.Float, 10).Value = IIf(lngIdPesona = 0, DBNull.Value, lngIdPesona)
            SQLCommand.Parameters.Add("@pIdCliente", SqlDbType.Float, 10).Value = IIf(lngIdCliente = 0, DBNull.Value, lngIdCliente)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Contacto, Lstr_NombreTabla)


            LDS_Contacto_Aux = LDS_Contacto

            If strColumnas.Trim = "" Then
                LDS_Contacto_Aux = LDS_Contacto
            Else
                LDS_Contacto_Aux = LDS_Contacto.Copy
                For Each Columna In LDS_Contacto.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Contacto_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Contacto_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_Contacto.Dispose()
            Return LDS_Contacto_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

End Class
