﻿Public Class ClsDividendos

    Public Function TraeTipoVariacion() As DataSet

        Dim DS_TipoVariacion As New DataSet
        Dim dtbTipoVariacion As New DataTable
        Dim drNewRow As DataRow

        With dtbTipoVariacion
            .Columns.Add("DESCRIPCION", GetType(String))
            .Columns.Add("CODIGO", GetType(String))

            ' DIVIDENDO
            drNewRow = dtbTipoVariacion.NewRow
            drNewRow("DESCRIPCION") = "DIVIDENDO"
            drNewRow("CODIGO") = "DIVIDENDO"
            Call dtbTipoVariacion.Rows.Add(drNewRow)

            ' OPCION
            drNewRow = dtbTipoVariacion.NewRow
            drNewRow("DESCRIPCION") = "OPCION"
            drNewRow("CODIGO") = "OPCION"
            Call dtbTipoVariacion.Rows.Add(drNewRow)

            ' CRIAS
            drNewRow = dtbTipoVariacion.NewRow
            drNewRow("DESCRIPCION") = "ACCIÓN LIBERADA"
            drNewRow("CODIGO") = "CRIA"
            Call dtbTipoVariacion.Rows.Add(drNewRow)

            ' CANJE
            drNewRow = dtbTipoVariacion.NewRow
            drNewRow("DESCRIPCION") = "CANJE"
            drNewRow("CODIGO") = "CANJE"
            Call dtbTipoVariacion.Rows.Add(drNewRow)

            ' DIVICIÓN
            drNewRow = dtbTipoVariacion.NewRow
            drNewRow("DESCRIPCION") = "DIVISIÓN"
            drNewRow("CODIGO") = "DIVISION"
            Call dtbTipoVariacion.Rows.Add(drNewRow)

        End With

        DS_TipoVariacion.Tables.Add(dtbTipoVariacion)
        Return DS_TipoVariacion

    End Function

    Public Function TraeDividendos(ByVal dblIdDividendo As Double, _
                          ByVal dblIdInstrumento As Double, _
                          ByVal dblIdInstrumentoOpcion As Double, _
                          ByVal strColumnas As String, _
                          ByVal strTipoVariacion As String, _
                          ByRef strRetorno As String) As DataSet

        Dim LDS_Dividendos As New DataSet
        Dim LDS_Dividendo_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Dividendo_Buscar"
        Lstr_NombreTabla = "DIVIDENDOS"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdDividendo", SqlDbType.Float, 9).Value = IIf(dblIdDividendo = 0, DBNull.Value, dblIdDividendo)
            SQLCommand.Parameters.Add("pIdInstrumento", SqlDbType.Float, 9).Value = IIf(dblIdInstrumento = 0, DBNull.Value, dblIdInstrumento)
            SQLCommand.Parameters.Add("pIdInstrumentoOpcion", SqlDbType.Float, 9).Value = IIf(dblIdInstrumentoOpcion = 0, DBNull.Value, dblIdInstrumentoOpcion)
            SQLCommand.Parameters.Add("pTipoVariacion", SqlDbType.VarChar, 15).Value = IIf(strTipoVariacion.Trim = "", DBNull.Value, strTipoVariacion.Trim)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Dividendos, Lstr_NombreTabla)

            LDS_Dividendo_Aux = LDS_Dividendos

            If strColumnas.Trim = "" Then
                LDS_Dividendo_Aux = LDS_Dividendos
            Else
                LDS_Dividendo_Aux = LDS_Dividendos.Copy
                For Each Columna In LDS_Dividendos.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Dividendo_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Dividendo_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            Return LDS_Dividendo_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function
    Public Function TraeDetalleDividendos(ByVal dblIdDividendo As Double, _
                          ByVal strColumnas As String, _
                          ByRef strRetorno As String) As DataSet

        Dim LDS_Dividendos As New DataSet
        Dim LDS_Dividendo_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_DetalleDividendo_Buscar"
        Lstr_NombreTabla = "DETALLE_DIVIDENDOS"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdDividendo", SqlDbType.Float, 9).Value = IIf(dblIdDividendo = 0, DBNull.Value, dblIdDividendo)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Dividendos, Lstr_NombreTabla)

            LDS_Dividendo_Aux = LDS_Dividendos

            If strColumnas.Trim = "" Then
                LDS_Dividendo_Aux = LDS_Dividendos
            Else
                LDS_Dividendo_Aux = LDS_Dividendos.Copy
                For Each Columna In LDS_Dividendos.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Dividendo_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Dividendo_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            Return LDS_Dividendo_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function TraeDividendosPorFecha(ByVal strFechadesde As String, _
                      ByVal strFechaHasta As String, _
                      ByVal strColumnas As String, _
                      ByVal strTipoVariacion As String, _
                      ByRef strRetorno As String) As DataSet

        Dim LDS_Dividendos As New DataSet
        Dim LDS_Dividendo_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Dividendo_Buscar_por_Fechas"
        Lstr_NombreTabla = "DIVIDENDOS"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pFechaDesde", SqlDbType.Char, 10).Value = strFechadesde
            SQLCommand.Parameters.Add("pFechaHasta", SqlDbType.Char, 10).Value = strFechaHasta
            SQLCommand.Parameters.Add("pTipoVariacion", SqlDbType.VarChar, 15).Value = IIf(strTipoVariacion.Trim = "", DBNull.Value, strTipoVariacion.Trim)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Dividendos, Lstr_NombreTabla)

            LDS_Dividendo_Aux = LDS_Dividendos

            If strColumnas.Trim = "" Then
                LDS_Dividendo_Aux = LDS_Dividendos
            Else
                LDS_Dividendo_Aux = LDS_Dividendos.Copy
                For Each Columna In LDS_Dividendos.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Dividendo_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Dividendo_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            Return LDS_Dividendo_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function Dividendo_Mantenedor(ByVal strAccion As String, _
                                         ByVal dblIdDividendo As Double, _
                                         ByVal dblIdInstrumento As Double, _
                                          ByVal strFecha As String, _
                                          ByVal dblMonto As Double, _
                                          ByVal strFechaCorte As String, _
                                          ByVal strTipoVariacion As String, _
                                          ByVal dblIdInstrumento_OPC As Double, _
                                          ByVal dblCadaxCantidad As Double, _
                                          ByVal dblEntregarxCantidad As Double, _
                                          ByVal strFechaDivisa As String, _
                                          ByVal dblMontoDivisa As Double, _
                                          ByVal strMonedaDivisa As String, _
                                          ByVal DS_Detalle As DataSet, _
                                          ByRef strDescError As String, _
                                           Optional ByRef lngIdDividendo As Long = 0) As String

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_Dividendo_Mantenedor", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Dividendo_Mantenedor"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = strAccion
            SQLCommand.Parameters.Add("pIdDividendo", SqlDbType.Float, 10).Value = dblIdDividendo
            SQLCommand.Parameters.Add("pIdInstrumento", SqlDbType.Float, 10).Value = IIf(dblIdInstrumento = 0, DBNull.Value, dblIdInstrumento)
            SQLCommand.Parameters.Add("pFecha", SqlDbType.Char, 10).Value = IIf(strFecha = "", DBNull.Value, strFecha)
            SQLCommand.Parameters.Add("pMonto", SqlDbType.Float, 18).Value = dblMonto
            SQLCommand.Parameters.Add("pFechaCorte", SqlDbType.Char, 10).Value = IIf(strFechaCorte = "", DBNull.Value, strFechaCorte)
            SQLCommand.Parameters.Add("pTipoVariacion", SqlDbType.VarChar, 15).Value = IIf(strTipoVariacion = "", DBNull.Value, strTipoVariacion)
            SQLCommand.Parameters.Add("pIdInstrumentoOpcion", SqlDbType.Float, 18).Value = IIf(dblIdInstrumento_OPC = 0, DBNull.Value, dblIdInstrumento_OPC)
            SQLCommand.Parameters.Add("pCadaxCantidad", SqlDbType.Float, 18).Value = dblCadaxCantidad
            SQLCommand.Parameters.Add("pEntregarxCantidad", SqlDbType.Float, 18).Value = dblEntregarxCantidad
            SQLCommand.Parameters.Add("pFechaDivisa", SqlDbType.Char, 10).Value = strFechaDivisa
            SQLCommand.Parameters.Add("pMontoDivisa", SqlDbType.Float, 18).Value = dblMontoDivisa
            SQLCommand.Parameters.Add("pMonedaDivisa", SqlDbType.Char, 3).Value = IIf(strMonedaDivisa = "", "CLP", strMonedaDivisa)

            '...(Identity)
            Dim pSalInstr As New SqlClient.SqlParameter("pGenerado", SqlDbType.Float, 10)
            pSalInstr.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalInstr)

            '...Resultado
            Dim pSalResultInstr As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultInstr.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultInstr)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            lngIdDividendo = IIf(SQLCommand.Parameters("pGenerado").Value Is DBNull.Value, 0, SQLCommand.Parameters("pGenerado").Value)
            If strDescError.ToUpper.Trim = "OK" Then
                If (strTipoVariacion <> "DIVIDENDO" And strTipoVariacion <> "OPCION") Then
                    Dim lstrDetalle1 As String = ""
                    Dim lstrDetalle2 As String = ""
                    Dim lstrDetalle3 As String = ""
                    Dim lstrColumnasDetalle As String = "ID_DIVIDENDO © " & _
                                                        "ID_INSTRUMENTO_ORIGEN © " & _
                                                        "ID_INSTRUMENTO_DESTINO © " & _
                                                        "CADA_X_CANTIDAD © " & _
                                                        "ENTREGAR_X_CANTIDAD "

                    Dim lstrResultadoCadena As String = ""

                    ' GENERA CADENA(S) DE DETALLE DE DIVIDENDOS
                    lstrResultadoCadena = GeneraCadena(DS_Detalle.Tables(0), lstrColumnasDetalle, "©", lstrDetalle1, lstrDetalle2, lstrDetalle3)
                    If lstrResultadoCadena <> "OK" Then
                        Return lstrResultadoCadena
                    End If

                    lstrResultadoCadena = ""

                    SQLCommand = New SqlClient.SqlCommand("Rcp_DetalleDividendo_Guardar", MiTransaccionSQL.Connection, MiTransaccionSQL)

                    SQLCommand.CommandType = CommandType.StoredProcedure
                    SQLCommand.CommandText = "Rcp_DetalleDividendo_Guardar"
                    SQLCommand.Parameters.Clear()

                    SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = strAccion.ToString.Trim
                    SQLCommand.Parameters.Add("pIdDividendo", SqlDbType.Float, 10).Value = IIf(dblIdDividendo = 0, lngIdDividendo, dblIdDividendo)
                    SQLCommand.Parameters.Add("pDetalle1", SqlDbType.VarChar, 8000).Value = lstrDetalle1
                    SQLCommand.Parameters.Add("pDetalle2", SqlDbType.VarChar, 8000).Value = IIf(lstrDetalle2 = "", DBNull.Value, lstrDetalle2)
                    SQLCommand.Parameters.Add("pDetalle3", SqlDbType.VarChar, 8000).Value = IIf(lstrDetalle3 = "", DBNull.Value, lstrDetalle3)
                    SQLCommand.Parameters.Add("pLineas", SqlDbType.Float, 5).Value = DS_Detalle.Tables(0).Rows.Count

                    '...Resultado
                    Dim ParametroSal As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                    ParametroSal.Direction = Data.ParameterDirection.Output
                    SQLCommand.Parameters.Add(ParametroSal)

                    SQLCommand.ExecuteNonQuery()

                    strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
                End If
                If strDescError.ToUpper.Trim = "OK" Then
                    MiTransaccionSQL.Commit()
                Else
                    MiTransaccionSQL.Rollback()
                End If
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error: " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            Dividendo_Mantenedor = strDescError
        End Try
    End Function

    Public Function TraerDividendosCustodio(ByVal strFechadesde As String, _
                                            ByVal strFechaHasta As String, _
                                            ByVal strColumnas As String, _
                                            ByRef strRetorno As String) As DataSet

        Dim LDS_DividendosCustodia As New DataSet
        Dim LDS_DividendosCustodia_Aux As New DataSet
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim LstrProcedimiento As String
        Dim Lstr_NombreTabla As String

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Reporte_DividendosCustodio_Buscar"
        Lstr_NombreTabla = "DivCustodio"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pFechaDesde", SqlDbType.Char, 10).Value = strFechadesde
            SQLCommand.Parameters.Add("pFechaHasta", SqlDbType.Char, 10).Value = strFechaHasta


            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_DividendosCustodia, Lstr_NombreTabla)

            LDS_DividendosCustodia_Aux = LDS_DividendosCustodia

            strRetorno = "OK"
            Return LDS_DividendosCustodia_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function TraerDividendosProc(ByVal dblIdDividendo As Double, _
                                        ByRef strRetorno As String) As DataSet

        Dim LDS_DividendosProc As New DataSet
        Dim LDS_DividendosProc_Aux As New DataSet
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim LstrProcedimiento As String
        Dim Lstr_NombreTabla As String

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_DividendosProc"
        Lstr_NombreTabla = "DivCustodioProc"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdDividendo", SqlDbType.Float, 10).Value = IIf(dblIdDividendo = 0, dblIdDividendo, dblIdDividendo)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_DividendosProc, Lstr_NombreTabla)

            LDS_DividendosProc_Aux = LDS_DividendosProc

            strRetorno = "OK"
            Return LDS_DividendosProc_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function Opciones_Cartera_Buscar(ByVal strFechadesde As String, _
                                            ByVal dblIdInstrumentoOSA As Double, _
                                            ByRef strRetorno As String) As DataSet

        Dim LDS_Opciones_Cartera As New DataSet
        Dim LDS_Opciones_Cartera_Aux As New DataSet
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim LstrProcedimiento As String
        Dim Lstr_NombreTabla As String

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Dividendos_Opciones_Cartera_Buscar"
        Lstr_NombreTabla = "OPCIONES_CARTERA"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pFechaConsulta", SqlDbType.Char, 10).Value = strFechadesde
            SQLCommand.Parameters.Add("@pIdNegocio", SqlDbType.Int, 10).Value = glngNegocioOperacion
            SQLCommand.Parameters.Add("pIdNemotecnicoOSA", SqlDbType.Float, 10).Value = IIf(dblIdInstrumentoOSA = 0, DBNull.Value, dblIdInstrumentoOSA)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Opciones_Cartera, Lstr_NombreTabla)

            LDS_Opciones_Cartera_Aux = LDS_Opciones_Cartera

            strRetorno = "OK"
            Return LDS_Opciones_Cartera_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

End Class
