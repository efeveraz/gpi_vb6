﻿Imports System.Data.SqlClient

Public Class ClsRelacionContraparteGPI

    Public Function Entidad_Relacion_Contraparte_GPI_Buscar( _
                    ByVal strEntidadGPI As String, _
                    ByVal strEntidadContraparte As String, _
                    ByVal strCodigoContrapate As String) As String

        Dim strDescError As String = ""
        Dim DS_Paso As New DataSet
        Dim lstrProcedimiento As String = ""
        Dim LstrNombreTabla As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SqlLDataAdapter As New SqlClient.SqlDataAdapter
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion

        '...Abre la conexion
        lstrProcedimiento = "Rcp_RelContraparteGPI_Buscar"
        LstrNombreTabla = "Paso"
        SQLConnect.Abrir()

        Try

            SQLCommand = New SqlCommand(lstrProcedimiento, SQLConnect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = lstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pEntidadGPI", SqlDbType.VarChar, 40).Value = strEntidadGPI
            SQLCommand.Parameters.Add("pEntidadContraparte", SqlDbType.VarChar, 50).Value = strEntidadContraparte
            SQLCommand.Parameters.Add("pCodigoContrapate", SqlDbType.VarChar, 50).Value = strCodigoContrapate
            '...(Id_Entidad en GPI)
            Dim ParametroSal As New SqlClient.SqlParameter("pIdGPI", SqlDbType.VarChar, 50)
            ParametroSal.Direction = ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal)

            '...Resultado
            Dim ParametroSal1 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal1.Direction = ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal1)

            SqlLDataAdapter.SelectCommand = SQLCommand
            SqlLDataAdapter.Fill(DS_Paso, LstrNombreTabla)

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            If strDescError <> "OK" Then
                Return ""
            Else
                strDescError = SQLCommand.Parameters("pIdGPI").Value.ToString.Trim
                Return strDescError
            End If

        Catch Ex As Exception
            Return ""
        Finally
            SQLConnect.Cerrar()

        End Try

    End Function

    Public Function Entidad_Relacion_Contraparte_GPI_Mantenedor(ByVal dblIdContraparte As Double, ByVal dblIdEntidadGpi As Double, _
                                                                ByVal strIdEntidad As String, ByVal strValor As String, ByRef strDescError As String, _
                                                                Optional ByVal pTabla As String = "", Optional ByVal pAccion As String = "") As String

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_RelContraparteGPI_Mantenedor", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_RelContraparteGPI_Mantenedor"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdContraparte", SqlDbType.Float, 10).Value = dblIdContraparte
            SQLCommand.Parameters.Add("pIdEntidadGPI", SqlDbType.Float, 10).Value = dblIdEntidadGpi
            SQLCommand.Parameters.Add("pTablaGPI", SqlDbType.VarChar, 40).Value = IIf(pTabla = "", DBNull.Value, pTabla)
            SQLCommand.Parameters.Add("pIdEntidad", SqlDbType.VarChar, 50).Value = strIdEntidad
            SQLCommand.Parameters.Add("pValor", SqlDbType.VarChar, 50).Value = strValor
            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 50).Value = IIf(pAccion = "", DBNull.Value, pAccion)

            '...Resultado
            Dim pSalResultInstr As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultInstr.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultInstr)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If
            '___________________________________________________________________________________________________________

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error: " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            Entidad_Relacion_Contraparte_GPI_Mantenedor = strDescError
        End Try
    End Function
    Public Function GrabarRelContraparteGPI_Id_Cartera(ByVal lngIdContraparteProc As Long, _
                                                       ByVal strIdCarteraCli As String, _
                                                       ByVal lngIdOperacion As Long) As String

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction
        Dim strDescError As String = ""

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_RelContraparte_Grabar_IdCartera", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_RelContraparte_Grabar_IdCartera"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("@pIdContraparteProc", SqlDbType.Float, 10).Value = lngIdContraparteProc
            SQLCommand.Parameters.Add("pArrIdCarteraCli", SqlDbType.VarChar, 1000).Value = strIdCarteraCli
            SQLCommand.Parameters.Add("pIdOperacion", SqlDbType.Float, 10).Value = lngIdOperacion

            '...Resultado
            Dim pSalResultInstr As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultInstr.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultInstr)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If
            '___________________________________________________________________________________________________________

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error: " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            GrabarRelContraparteGPI_Id_Cartera = strDescError
        End Try
    End Function

    Public Function Entidad_Relacion_Contraparte_GPI_Buscar_Valor( _
                    ByVal strIdEntidadGPI As Double, _
                    ByVal strIdContraparte As Double, _
                    ByRef strValor As String) As String

        Dim strDescError As String = ""
        Dim DS_Paso As New DataSet
        Dim lstrProcedimiento As String = ""
        Dim LstrNombreTabla As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SqlLDataAdapter As New SqlClient.SqlDataAdapter
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion

        '...Abre la conexion
        lstrProcedimiento = "Rcp_RelContraparteGPI_BuscarValor"
        LstrNombreTabla = "PASO"
        SQLConnect.Abrir()

        Try

            SQLCommand = New SqlCommand(lstrProcedimiento, SQLConnect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = lstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("@pIdEntidadGPI", SqlDbType.Decimal, 10).Value = strIdEntidadGPI
            SQLCommand.Parameters.Add("@pIdContraparte", SqlDbType.Decimal, 10).Value = strIdContraparte
            SQLCommand.Parameters.Add("@pMiValor", SqlDbType.VarChar, 50).Value = strValor
            '...(Valor)
            Dim ParametroSal As New SqlClient.SqlParameter("@pValor", SqlDbType.VarChar, 50)
            ParametroSal.Direction = ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal)


            '...Resultado
            Dim ParametroSal1 As New SqlClient.SqlParameter("@pResultado", SqlDbType.VarChar, 200)
            ParametroSal1.Direction = ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal1)

            SqlLDataAdapter.SelectCommand = SQLCommand
            SqlLDataAdapter.Fill(DS_Paso, LstrNombreTabla)

            strDescError = SQLCommand.Parameters("@pResultado").Value.ToString.Trim
            If strDescError <> "OK" Then
                strValor = ""
                Return ""
            Else
                strValor = SQLCommand.Parameters("@pValor").Value.ToString.Trim
                Return "OK"
            End If

        Catch Ex As Exception
            strValor = ""
            Return "Error: " & Ex.Message
        Finally
            SQLConnect.Cerrar()

        End Try


    End Function
    'Public Function BuscarEntidadesGPI( _
    '                ByVal strIdEntidadGPI As String, _
    '                ByVal strIdContraparte As String, _
    '                ByRef strValor As String) As String

    '    Dim strDescError As String = ""
    '    Dim SQLCommand As New SqlClient.SqlCommand
    '    Dim SQLConnect As Cls_Conexion = New Cls_Conexion
    '    Dim MiTransaccionSQL As SqlClient.SqlTransaction

    '    '...Abre la conexion
    '    SQLConnect.Abrir()
    '    MiTransaccionSQL = SQLConnect.Transaccion

    '    Try

    '        SQLCommand = New SqlClient.SqlCommand("Rcp_RelContraparteGPI_BuscarValor", MiTransaccionSQL.Connection, MiTransaccionSQL)

    '        SQLCommand.CommandType = CommandType.StoredProcedure
    '        SQLCommand.CommandText = "Rcp_RelContraparteGPI_BuscarValor"
    '        SQLCommand.Parameters.Clear()

    '        SQLCommand.Parameters.Add("@pIdEntidadGPI", SqlDbType.Decimal, 10).Value = strIdEntidadGPI
    '        SQLCommand.Parameters.Add("@pIdContraparte", SqlDbType.Decimal, 10).Value = strIdContraparte
    '        SQLCommand.Parameters.Add("@pMiValor", SqlDbType.VarChar, 50).Value = strValor
    '        '...(Valor)
    '        Dim ParametroSal As New SqlClient.SqlParameter("@pValor", SqlDbType.VarChar, 50)
    '        ParametroSal.Direction = Data.ParameterDirection.Output
    '        SQLCommand.Parameters.Add(ParametroSal)


    '        '...Resultado
    '        Dim ParametroSal1 As New SqlClient.SqlParameter("@pResultado", SqlDbType.VarChar, 200)
    '        ParametroSal1.Direction = Data.ParameterDirection.Output
    '        SQLCommand.Parameters.Add(ParametroSal1)

    '        SQLCommand.ExecuteNonQuery()

    '        strDescError = SQLCommand.Parameters("@pResultado").Value.ToString.Trim
    '        If strDescError <> "OK" Then
    '            strValor = ""
    '            Return ""
    '        Else
    '            strValor = SQLCommand.Parameters("@pValor").Value.ToString.Trim
    '            Return "OK"
    '        End If

    '    Catch Ex As Exception
    '        strValor = ""
    '        Return "Error: " & Ex.Message
    '    Finally
    '        SQLConnect.Cerrar()

    '    End Try

    'End Function

    Public Function BuscarEntidadesGPI(ByVal strCod_Tabla As String, _
                                       ByVal strItem As String, _
                                       ByVal strValor As String, _
                                       ByRef strRetorno As String) As String

        ' Objetivo 
        ' Busca REL_CONTRAPARTE_GPI por nombre tabla, cod_externo contraparte y valor todos string 
        ' rescatando el campo ID_ENTIDAD de tabla REL_CONTRAPARTE_GPI

        Dim LDS_AliasNemo As New DataSet
        Dim LDS_Conciliador_Aux As New DataSet

        Dim lValor As String = ""
        Dim LstrProcedimiento As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim strResultado As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_BuscarEntidadesGPI"
        Lstr_NombreTabla = ""

        Connect.Abrir()

        Try

            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("PCOD_EXTERNO_CONTRAPARTE", SqlDbType.VarChar, 100).Value = strCod_Tabla
            SQLCommand.Parameters.Add("PTABLA", SqlDbType.VarChar, 100).Value = strItem
            SQLCommand.Parameters.Add("PVALOR", SqlDbType.VarChar, 50).Value = strValor

            Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultado.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)

            Dim pSalValor As New SqlClient.SqlParameter("pSalResul", SqlDbType.VarChar, 200)
            pSalValor.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalValor)

            SQLCommand.ExecuteNonQuery()

            strRetorno = SQLCommand.Parameters("pSalResul").Value.ToString.Trim

            Return strRetorno

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()

        End Try
    End Function

    Public Function Busca_Rel_Contraparte_xEntidad(ByVal strCodExterno As String, _
                                                   ByVal strTabla As String, _
                                                   ByVal strEntidad As String) As String

        ' Objetivo 
        ' Busca REL_CONTRAPARTE_GPI por nombre tabla, cod_externo contraparte y Entidada todos string 
        ' rescatando el campo VALOR de tabla REL_CONTRAPARTE_GPI

        Dim LDS_AliasNemo As New DataSet
        Dim LDS_Conciliador_Aux As New DataSet

        Dim lValor As String = ""
        Dim LstrProcedimiento As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim strResultado As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion

        Dim strValor As String
        LstrProcedimiento = "Rcp_RelContraparteGPI_BuscaXEntidad"
        Lstr_NombreTabla = ""

        Connect.Abrir()

        Try

            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("PCOD_EXTERNO_CONTRAPARTE", SqlDbType.VarChar, 100).Value = strCodExterno
            SQLCommand.Parameters.Add("PTABLA", SqlDbType.VarChar, 100).Value = strTabla
            SQLCommand.Parameters.Add("PENTIDAD", SqlDbType.VarChar, 50).Value = strEntidad

            Dim pValor As New SqlClient.SqlParameter("PVALOR", SqlDbType.VarChar, 50)
            pValor.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pValor)

            SQLCommand.ExecuteNonQuery()

            strValor = SQLCommand.Parameters("PVALOR").Value.ToString.Trim

            Return strValor

        Catch ex As Exception
            strValor = ex.Message
            Return strValor
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function BuscarEntidadesGPI_x_Cod_Ext(ByVal strCod_Externo As String, ByRef strRetorno2 As String) As String

        Dim LDS_AliasNemo As New DataSet
        Dim LDS_Conciliador_Aux As New DataSet
        'Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        'Dim LInt_Col As Integer = 0
        'Dim LInt_NomCol As String = ""
        'Dim Columna As DataColumn
        'Dim Remove As Boolean = True
        Dim lValor As String = ""
        Dim LstrProcedimiento As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim strResultado As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim strRetorno As String

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_ContraParte_Buscar_x_Cod_Ext"
        Lstr_NombreTabla = ""

        Connect.Abrir()

        Try

            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pCod_Contraparte_ext", SqlDbType.VarChar, 100).Value = strCod_Externo

            Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultado.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)


            Dim pSalResultado2 As New SqlClient.SqlParameter("pResultado2", SqlDbType.VarChar, 200)
            pSalResultado2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado2)



            SQLCommand.ExecuteNonQuery()

            strRetorno2 = SQLCommand.Parameters("pResultado2").Value.ToString.Trim
            strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            Return strRetorno

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()

        End Try
    End Function

End Class
