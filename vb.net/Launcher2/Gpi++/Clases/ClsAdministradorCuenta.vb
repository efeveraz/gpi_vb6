﻿Public Class ClsAdministradorCuenta

    Public Function AdministradorCuenta_Ver(ByVal lngIdAdministradorCuenta As Long, _
                             ByVal strEstadoAdministradorCuenta As String, _
                             ByVal strColumnas As String, _
                             ByRef strRetorno As String) As DataSet

        Dim LDS_AdministradorCuenta As New DataSet
        Dim LDS_AdministradorCuenta_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Administrador_Cuenta_Consultar"
        Lstr_NombreTabla = "AdministradorCuenta"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("@pIdAdministrador_Cuenta", SqlDbType.Float, 10).Value = IIf(lngIdAdministradorCuenta = 0, DBNull.Value, lngIdAdministradorCuenta)
            SQLCommand.Parameters.Add("@pEstAdministrador_Cuenta", SqlDbType.VarChar, 15).Value = IIf(strEstadoAdministradorCuenta.Trim = "", DBNull.Value, strEstadoAdministradorCuenta.Trim)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_AdministradorCuenta, Lstr_NombreTabla)

            LDS_AdministradorCuenta_Aux = LDS_AdministradorCuenta

            If strColumnas.Trim = "" Then
                LDS_AdministradorCuenta_Aux = LDS_AdministradorCuenta
            Else
                LDS_AdministradorCuenta_Aux = LDS_AdministradorCuenta.Copy
                For Each Columna In LDS_AdministradorCuenta.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_AdministradorCuenta_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_AdministradorCuenta_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            Return LDS_AdministradorCuenta_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function Obtiene_IdAdministradorCuenta_ABR(ByVal strAbrAdministradorCuenta As String, _
                                           Optional ByVal strEstadoAdministradorCuenta As String = "") As Long

        Dim LDS_AdministradorCuenta As New DataSet
        Dim LDS_AdministradorCuenta_Aux As New DataSet
        Dim Lstr_NombreTabla As String
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_AdministradorCuenta_ID_ABR"
        Lstr_NombreTabla = "AdministradorCuenta"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAbrAdministradorCuenta", SqlDbType.VarChar, 30).Value = IIf(strAbrAdministradorCuenta = "", DBNull.Value, strAbrAdministradorCuenta)
            SQLCommand.Parameters.Add("pEstAdministradorCuenta", SqlDbType.VarChar, 3).Value = IIf(strEstadoAdministradorCuenta.Trim = "", DBNull.Value, strEstadoAdministradorCuenta.Trim)

            Dim ParametroSal1 As New SqlClient.SqlParameter("pIdAdministradorCuenta", SqlDbType.Float, 10)
            ParametroSal1.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal1)

            SQLCommand.ExecuteNonQuery()

            Obtiene_IdAdministradorCuenta_ABR = SQLCommand.Parameters("pIdAdministradorCuenta").Value.ToString.Trim

        Catch ex As Exception
            Obtiene_IdAdministradorCuenta_ABR = -1

        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function CuentaAdministradorCuenta_Mantenedor(ByVal lngIdCuenta As Long, ByVal lngIdAdministradorCuenta As Long) As String
        Dim strDescError As String = "OK"
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try

            SQLCommand = New SqlClient.SqlCommand("Rcp_CuentaAdministradorCuenta_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_CuentaAdministradorCuenta_Mantencion"

            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("@pIdCuenta", SqlDbType.Float, 10).Value = lngIdCuenta
            SQLCommand.Parameters.Add("@pIdAdministradorCuenta", SqlDbType.Float, 10).Value = lngIdAdministradorCuenta

            SQLCommand.ExecuteNonQuery()

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en la actualización del AdministradorCuenta por cuenta [" & vbCr & Ex.Message & "]"
        Finally
            If strDescError = "OK" Then
                MiTransaccionSQL.Commit()
            End If
            SQLConnect.Cerrar()
            CuentaAdministradorCuenta_Mantenedor = strDescError
        End Try
    End Function

    Public Function CuentaAdministradorCuenta_Ver(ByVal lngIdCuenta As Long) As Long

        Dim LDS_AdministradorCuenta As New DataSet
        Dim LDS_AdministradorCuenta_Aux As New DataSet
        Dim Lstr_NombreTabla As String
        Dim LstrProcedimiento
        Dim llngValor As Long = 0

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_CuentaAdministradorCuenta_Consultar"
        Lstr_NombreTabla = "RelCuentaAdministradorCuenta"

        Connect.Abrir()
        If lngIdCuenta > 0 Then
            Try
                SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

                SQLCommand.CommandType = CommandType.StoredProcedure
                SQLCommand.CommandText = LstrProcedimiento
                SQLCommand.Parameters.Clear()

                SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = IIf(lngIdCuenta = 0, DBNull.Value, lngIdCuenta)

                SQLDataAdapter.SelectCommand = SQLCommand
                SQLDataAdapter.Fill(LDS_AdministradorCuenta, Lstr_NombreTabla)

                If LDS_AdministradorCuenta.Tables("RelCuentaAdministradorCuenta").Rows.Count > 0 Then
                    llngValor = CLng("0" & LDS_AdministradorCuenta.Tables("RelCuentaAdministradorCuenta").Rows(0).Item("Id_Administrador"))
                End If

            Catch ex As Exception
                gFun_ResultadoMsg("1|" & ex.Message, "CuentaAdministradorCuenta_Ver")

            Finally
                Connect.Cerrar()
                CuentaAdministradorCuenta_Ver = llngValor
            End Try
        End If
    End Function

    Public Function AdministradorCuenta_Mantenedor(ByVal strAccion As String, _
                              ByVal IntIdAdministradorCuenta As String, _
                              ByVal strAbr_AdministradorCuenta As String, _
                              ByVal strDsc_AdministradorCuenta As String, _
                              ByVal strEstado_AdministradorCuenta As String) As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_AdministradorCuenta_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_AdministradorCuenta_Mantencion"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 10).Value = strAccion
            SQLCommand.Parameters.Add("pIdAdministradorCuenta", SqlDbType.VarChar, 10).Value = IIf(IntIdAdministradorCuenta.Trim = "0", DBNull.Value, IntIdAdministradorCuenta.Trim)
            SQLCommand.Parameters.Add("pAbr_AdministradorCuenta", SqlDbType.VarChar, 30).Value = IIf(strAbr_AdministradorCuenta.Trim = "", DBNull.Value, strAbr_AdministradorCuenta.Trim)
            SQLCommand.Parameters.Add("pDsc_AdministradorCuenta", SqlDbType.VarChar, 50).Value = IIf(strDsc_AdministradorCuenta.Trim = "", DBNull.Value, strDsc_AdministradorCuenta.Trim)
            SQLCommand.Parameters.Add("pEstado_AdministradorCuenta", SqlDbType.VarChar, 3).Value = strEstado_AdministradorCuenta

            '...Resultado
            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            AdministradorCuenta_Mantenedor = strDescError
        End Try
    End Function

    Public Function Adm_Cta_Mantenedor(ByVal strAccion As String, _
                              ByVal IntId_AdmCta As String, _
                              ByVal strAbr_AdmCta As String, _
                              ByVal strDsc_AdmCta As String, _
                              ByVal strEst_AdmCta As String) As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_Adm_Cta_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Adm_Cta_Mantencion"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 10).Value = strAccion
            SQLCommand.Parameters.Add("pIdAdmCta", SqlDbType.VarChar, 10).Value = IIf(IntId_AdmCta.Trim = "0", DBNull.Value, IntId_AdmCta.Trim)
            SQLCommand.Parameters.Add("pAbrAdmCta", SqlDbType.VarChar, 30).Value = IIf(strAbr_AdmCta.Trim = "", DBNull.Value, strAbr_AdmCta.Trim)
            SQLCommand.Parameters.Add("pDscAdmCta", SqlDbType.VarChar, 30).Value = IIf(strDsc_AdmCta.Trim = "", DBNull.Value, strDsc_AdmCta.Trim)
            SQLCommand.Parameters.Add("pEstAdmCta", SqlDbType.VarChar, 3).Value = strEst_AdmCta

            '...Resultado
            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            Adm_Cta_Mantenedor = strDescError
        End Try
    End Function
End Class
