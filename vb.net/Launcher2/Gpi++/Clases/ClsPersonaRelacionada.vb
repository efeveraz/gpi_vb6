﻿Public Class ClsPersonaRelacionada
    Public Function TraerPersonaAutRestirarDoc(ByVal strIdCliente As String, _
                                      ByVal strColumnas As String, _
                                      ByRef strRetorno As String) As DataSet

        Dim LDS_PersonaRelacionada As New DataSet
        Dim LDS_PersonaRelacionada_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Pers_Aut_RetirarDoc_Consultar"
        Lstr_NombreTabla = "Personas"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdCliente", SqlDbType.VarChar, 9).Value = IIf(strIdCliente = "", DBNull.Value, CLng(0 & strIdCliente))

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_PersonaRelacionada, Lstr_NombreTabla)

            LDS_PersonaRelacionada_Aux = LDS_PersonaRelacionada

            If strColumnas.Trim = "" Then
                LDS_PersonaRelacionada_Aux = LDS_PersonaRelacionada
            Else
                LDS_PersonaRelacionada_Aux = LDS_PersonaRelacionada.Copy
                For Each Columna In LDS_PersonaRelacionada.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_PersonaRelacionada_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_PersonaRelacionada_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            Return LDS_PersonaRelacionada_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function
    Public Function TraerPersonaAutDarOrden(ByVal strIdCliente As String, _
                                      ByVal strColumnas As String, _
                                      ByRef strRetorno As String) As DataSet

        Dim LDS_PersonaRelacionada As New DataSet
        Dim LDS_PersonaRelacionada_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Pers_Aut_Orden_Consultar"
        Lstr_NombreTabla = "Personas"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdCliente", SqlDbType.VarChar, 9).Value = IIf(strIdCliente = "", DBNull.Value, strIdCliente)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_PersonaRelacionada, Lstr_NombreTabla)

            LDS_PersonaRelacionada_Aux = LDS_PersonaRelacionada

            If strColumnas.Trim = "" Then
                LDS_PersonaRelacionada_Aux = LDS_PersonaRelacionada
            Else
                LDS_PersonaRelacionada_Aux = LDS_PersonaRelacionada.Copy
                For Each Columna In LDS_PersonaRelacionada.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_PersonaRelacionada_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_PersonaRelacionada_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            Return LDS_PersonaRelacionada_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function PersonaAutDarOrden_Mantenedor(ByVal strId_Cliente As String, ByRef dstPersonaAutDarOrden As DataSet) As String

        Dim strDescError As String = "OK"
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try

            '       ELIMINAMOS LAS PersonaAutDarOrden YA EXISTENTES

            SQLCommand = New SqlClient.SqlCommand("Rcp_PersonaAutorizadaOrden_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_PersonaAutorizadaOrden_Mantencion"
            SQLCommand.Parameters.Clear()
            '(@pAccion varchar(10), @pRut varchar(10)=NULL, @pDsc varchar(100)=NULL, @pIdCliente numeric(9),
            '@pCargo varchar(50)=NULL, @pIdTipoOrden numeric(9),@pResultado varchar(200) OUTPUT)
            SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = "ELIMINAR"
            SQLCommand.Parameters.Add("pIdTipoOrden", SqlDbType.Float, 9).Value = DBNull.Value
            SQLCommand.Parameters.Add("pIdCliente", SqlDbType.Float, 10).Value = strId_Cliente
            SQLCommand.Parameters.Add("pDsc", SqlDbType.VarChar, 100).Value = DBNull.Value
            SQLCommand.Parameters.Add("pCargo", SqlDbType.VarChar, 50).Value = DBNull.Value
            SQLCommand.Parameters.Add("pRut", SqlDbType.VarChar, 10).Value = DBNull.Value
            '...Resultado  @pObservacion varchar(100),
            Dim pSalElimIntPort As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalElimIntPort.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalElimIntPort)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            If Not strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Rollback()
                Exit Function
            End If

            '       INSERTAMOS LOS NUEVOS PersonaAutDarOrden X cliente 

            If dstPersonaAutDarOrden.Tables(0).Rows.Count <> Nothing Then
                For Each pRow As DataRow In dstPersonaAutDarOrden.Tables(0).Rows

                    SQLCommand = New SqlClient.SqlCommand("Rcp_PersonaAutorizadaOrden_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)
                    SQLCommand.CommandType = CommandType.StoredProcedure
                    SQLCommand.CommandText = "Rcp_PersonaAutorizadaOrden_Mantencion"
                    SQLCommand.Parameters.Clear()
                    '(DSC_PERSONA_AUTORIZADA, ID_CLIENTE, RUT_PERSONA_AUTORIZADA,ID_TIPO_ORDEN,CARGO)
                    SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = "INSERTAR"
                    SQLCommand.Parameters.Add("pIdTipoOrden", SqlDbType.Float, 9).Value = pRow("ID_FORMA_OPERACION").ToString
                    SQLCommand.Parameters.Add("pDsc", SqlDbType.VarChar, 100).Value = pRow("DSC_PERSONA_AUTORIZADA").ToString.ToUpper
                    SQLCommand.Parameters.Add("pCargo", SqlDbType.VarChar, 50).Value = pRow("CARGO").ToString.ToUpper
                    SQLCommand.Parameters.Add("pRut", SqlDbType.VarChar, 10).Value = pRow("RUT_PERSONA_AUTORIZADA").ToString.ToUpper
                    SQLCommand.Parameters.Add("pIdCliente", SqlDbType.Float, 10).Value = strId_Cliente

                    '...Resultado
                    Dim pSalInstPort As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                    pSalInstPort.Direction = Data.ParameterDirection.Output
                    SQLCommand.Parameters.Add(pSalInstPort)

                    SQLCommand.ExecuteNonQuery()

                    strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

                    If Not strDescError.ToUpper.Trim = "OK" Then
                        Exit For
                    End If
                Next
            End If

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            PersonaAutDarOrden_Mantenedor = strDescError
        End Try
    End Function
    Public Function PersonaAutRestirarDoc_Mantenedor(ByVal strId_Cliente As String, ByRef dstPersonaAutRestirarDoc As DataSet) As String

        Dim strDescError As String = "OK"
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try

            '       ELIMINAMOS LAS PersonaAutRestirarDoc YA EXISTENTES

            SQLCommand = New SqlClient.SqlCommand("Rcp_PersonaAutRestirarDoc_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_PersonaAutRestirarDoc_Mantencion"
            SQLCommand.Parameters.Clear()
            '(@pAccion varchar(10), @pRut varchar(10)=NULL, @pDsc varchar(100)=NULL, @pIdCliente numeric(9)
            ',@pResultado varchar(200) OUTPUT)
            SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = "ELIMINAR"
            SQLCommand.Parameters.Add("pIdCliente", SqlDbType.Float, 10).Value = strId_Cliente
            SQLCommand.Parameters.Add("pDsc", SqlDbType.VarChar, 100).Value = DBNull.Value
            SQLCommand.Parameters.Add("pRut", SqlDbType.VarChar, 10).Value = DBNull.Value
            '...Resultado  @pObservacion varchar(100),
            Dim pSalElimIntPort As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalElimIntPort.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalElimIntPort)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            If Not strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Rollback()
                Exit Function
            End If

            '       INSERTAMOS LOS NUEVOS PersonaAutRestirarDoc X cliente 

            If dstPersonaAutRestirarDoc.Tables(0).Rows.Count <> Nothing Then
                For Each pRow As DataRow In dstPersonaAutRestirarDoc.Tables(0).Rows

                    SQLCommand = New SqlClient.SqlCommand("Rcp_PersonaAutRestirarDoc_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)
                    SQLCommand.CommandType = CommandType.StoredProcedure
                    SQLCommand.CommandText = "Rcp_PersonaAutRestirarDoc_Mantencion"
                    SQLCommand.Parameters.Clear()

                    SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = "INSERTAR"
                    SQLCommand.Parameters.Add("pDsc", SqlDbType.VarChar, 100).Value = pRow("DSC_PERSONA_RETIRA").ToString.ToUpper
                    SQLCommand.Parameters.Add("pRut", SqlDbType.VarChar, 10).Value = pRow("RUT_PERSONA_RETIRA").ToString.ToUpper
                    SQLCommand.Parameters.Add("pIdCliente", SqlDbType.Float, 10).Value = strId_Cliente

                    '...Resultado
                    Dim pSalInstPort As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                    pSalInstPort.Direction = Data.ParameterDirection.Output
                    SQLCommand.Parameters.Add(pSalInstPort)

                    SQLCommand.ExecuteNonQuery()

                    strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

                    If Not strDescError.ToUpper.Trim = "OK" Then
                        Exit For
                    End If
                Next
            End If

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            PersonaAutRestirarDoc_Mantenedor = strDescError
        End Try
    End Function
End Class
