﻿Imports System.Data.SqlClient
Imports System.Data


Public Class ClsPortafolioActivos
#Region "Declaraciones de variables"
    Dim sProcedimiento As String = ""

#End Region

#Region "Metodos de la Clase"

    Public Function TraeDatosPortafolio(ByRef strRetorno As String, Optional ByVal dblIdPortafolio As Double = 0) As DataSet

        Dim LDS_Portafolio As New DataSet
        Dim LDS_Portafolio_Aux As New DataSet

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        sProcedimiento = "Rcp_PortafoliosActivos_Buscar"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(sProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = sProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("@pIdPortafolio", SqlDbType.Float, 10).Value = IIf(dblIdPortafolio = 0, DBNull.Value, dblIdPortafolio)
            
            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Portafolio)

            LDS_Portafolio_Aux = LDS_Portafolio
            strRetorno = "OK"
            Return LDS_Portafolio_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function TraePortaFolioPerfil(ByVal lngIdPerfilRiesgo As Long, _
                                 ByVal strColumnas As String, _
                                 ByRef strRetorno As String, _
                                 Optional ByVal strCodMoneda As String = "") As DataSet


        Dim LDS_PortfolioPerfil As New DataSet
        Dim LDS_PortfolioPerfil_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_PortafoliosActivos_BuscarPorPerfil"
        Lstr_NombreTabla = "PORTFOLIO_PERFIL"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdPerfilRiesgo", SqlDbType.Float, 3).Value = lngIdPerfilRiesgo
            SQLCommand.Parameters.Add("pCodMoneda", SqlDbType.VarChar, 3).Value = IIf(strCodMoneda = "", DBNull.Value, strCodMoneda)


            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_PortfolioPerfil, Lstr_NombreTabla)

            LDS_PortfolioPerfil_Aux = LDS_PortfolioPerfil


            If strColumnas.Trim = "" Then
                LDS_PortfolioPerfil_Aux = LDS_PortfolioPerfil
            Else
                LDS_PortfolioPerfil_Aux = LDS_PortfolioPerfil.Copy
                For Each Columna In LDS_PortfolioPerfil.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_PortfolioPerfil_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_PortfolioPerfil_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            Return LDS_PortfolioPerfil_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function TraePortaFolioCuenta(ByVal lngIdPerfilRiesgo As Long, _
                                         ByVal lngIdCuenta As Long, _
                                         ByVal strFechaConsulta As String, _
                                         ByVal strCodClaseInstrumento As String, _
                                         ByVal strColumnas As String, _
                                         ByRef strRetorno As String) As DataSet

        Dim LDS_PortfolioPerfil As New DataSet
        Dim LDS_PortfolioPerfil_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_PortafoliosActivos_BuscarPorCuenta"
        Lstr_NombreTabla = "PORTFOLIO_CUENTA"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdPerfilRiesgo", SqlDbType.Float, 3).Value = IIf(lngIdPerfilRiesgo = 0, DBNull.Value, lngIdPerfilRiesgo)
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = IIf(lngIdCuenta = 0, DBNull.Value, lngIdCuenta)
            SQLCommand.Parameters.Add("pFechaConsulta", SqlDbType.Char, 10).Value = IIf(strFechaConsulta = "", DBNull.Value, strFechaConsulta)
            SQLCommand.Parameters.Add("pCodClaseInstrumento", SqlDbType.VarChar, 10).Value = IIf(strCodClaseInstrumento = "", DBNull.Value, strCodClaseInstrumento)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_PortfolioPerfil, Lstr_NombreTabla)

            LDS_PortfolioPerfil_Aux = LDS_PortfolioPerfil


            If strColumnas.Trim = "" Then
                LDS_PortfolioPerfil_Aux = LDS_PortfolioPerfil
            Else
                LDS_PortfolioPerfil_Aux = LDS_PortfolioPerfil.Copy
                For Each Columna In LDS_PortfolioPerfil.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_PortfolioPerfil_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_PortfolioPerfil_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            Return LDS_PortfolioPerfil_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function TraeComposicionPortfolio(ByVal strIdNegocio As String, _
                                 ByVal strIdPortafolio As String, _
                                 ByVal strCodClaseInstrumento As String, _
                                 ByVal strCodSubClaseInstrumento As String, _
                                 ByVal lngIdCuenta As Long, _
                                 ByVal strFechaConsulta As String, _
                                 ByVal strConsideraUltimoPrecioConocido As String, _
                                 ByVal strColumnas As String, _
                                 ByRef strRetorno As String) As DataSet

        Dim LDS_PortfolioComposicion As New DataSet
        Dim LDS_PortfolioComposicion_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_PortafoliosActivos_BuscarComposicion"
        Lstr_NombreTabla = "PORTFOLIO_COMPOSICION"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Float, 10).Value = IIf(strIdNegocio = "", DBNull.Value, CLng(0 & strIdNegocio))
            SQLCommand.Parameters.Add("pIdPortafolio", SqlDbType.Float, 18).Value = IIf(strIdPortafolio = "", DBNull.Value, CLng(0 & strIdPortafolio))
            SQLCommand.Parameters.Add("pCodClaseInstrumento", SqlDbType.VarChar, 15).Value = IIf(strCodClaseInstrumento.Trim = "", DBNull.Value, strCodClaseInstrumento)
            SQLCommand.Parameters.Add("pCodSubClaseInstrumento", SqlDbType.VarChar, 10).Value = IIf(strCodSubClaseInstrumento = "", DBNull.Value, strCodSubClaseInstrumento)
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = IIf(lngIdCuenta = 0, DBNull.Value, lngIdCuenta)
            SQLCommand.Parameters.Add("pFechaConsulta", SqlDbType.Char, 10).Value = IIf(strFechaConsulta = "", DBNull.Value, strFechaConsulta)
            SQLCommand.Parameters.Add("pConsideraUltimoPrecioConocido", SqlDbType.Char, 1).Value = IIf(strConsideraUltimoPrecioConocido = "", DBNull.Value, strConsideraUltimoPrecioConocido)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_PortfolioComposicion, Lstr_NombreTabla)

            LDS_PortfolioComposicion_Aux = LDS_PortfolioComposicion

            If strColumnas.Trim = "" Then
                LDS_PortfolioComposicion_Aux = LDS_PortfolioComposicion
            Else
                LDS_PortfolioComposicion_Aux = LDS_PortfolioComposicion.Copy
                For Each Columna In LDS_PortfolioComposicion.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_PortfolioComposicion_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_PortfolioComposicion_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            Return LDS_PortfolioComposicion_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function TraeEstado() As DataSet

        Dim DS_Estados As New DataSet
        Dim dtbEstados As New DataTable
        Dim drNewRow As DataRow

        With dtbEstados
            .Columns.Add("DESCRIPCION", GetType(String))
            .Columns.Add("CODIGO", GetType(String))

            ' VIGENTE
            drNewRow = dtbEstados.NewRow
            drNewRow("DESCRIPCION") = "VIGENTE"
            drNewRow("CODIGO") = "VIG"
            Call dtbEstados.Rows.Add(drNewRow)

            ' ANULADO
            drNewRow = dtbEstados.NewRow
            drNewRow("DESCRIPCION") = "ANULADO"
            drNewRow("CODIGO") = "ANU"
            Call dtbEstados.Rows.Add(drNewRow)

        End With

        DS_Estados.Tables.Add(dtbEstados)
        Return DS_Estados

    End Function

    Public Function Portafolio_Mantenedor(ByVal strAccion As String, _
                              ByVal dblIdPortafolio As Double, _
                              ByVal strNombreCortoPortafolio As String, _
                              ByVal strDescripcionPortafolio As String, _
                              ByVal strCodMoneda As String, _
                              ByRef strEstado As String, _
                              ByVal dblMinimoInversion As String, _
                              ByRef dtbInstrumentos As DataTable, _
                              ByRef dtbPerfilesRiesgo As DataTable) As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            ' SECCION PORTAFOLIO ACTIVOS
            '___________________________________________________________________________________________________________
            ' INSERTAMOS EL NUEVO PORTAFOLIO
            SQLCommand = New SqlClient.SqlCommand("Rcp_PortafoliosActivos_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_PortafoliosActivos_Mantencion"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = strAccion
            SQLCommand.Parameters.Add("pIdPortafolio", SqlDbType.Char, 10).Value = dblIdPortafolio
            SQLCommand.Parameters.Add("pNombreCortoPortafolio", SqlDbType.Char, 10).Value = strNombreCortoPortafolio
            SQLCommand.Parameters.Add("pDescPortafolio", SqlDbType.VarChar, 50).Value = strDescripcionPortafolio
            SQLCommand.Parameters.Add("pCodMoneda", SqlDbType.VarChar, 3).Value = strCodMoneda
            SQLCommand.Parameters.Add("pMinimoInversion", SqlDbType.Float, 20).Value = dblMinimoInversion
            SQLCommand.Parameters.Add("pEstPortafolio", SqlDbType.VarChar, 3).Value = strEstado

            '...(Identity)
            Dim pSalPortFol As New SqlClient.SqlParameter("pGenerado", SqlDbType.Float, 10)
            pSalPortFol.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalPortFol).Value = dblIdPortafolio

            '...Resultado
            Dim pSalResultPorFol As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultPorFol.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultPorFol)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If Not strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Rollback()
                Exit Function
            End If

            ' SECCION INSTRUMENTOS PORTAFOLIO
            '___________________________________________________________________________________________________________
            ' ELIMINAMOS LOS INSTRUMENTOS YA EXISTENTES
            SQLCommand = New SqlClient.SqlCommand("Rcp_InstrumentosPortafolio_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_InstrumentosPortafolio_Mantencion"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = "ELIMINAR"
            SQLCommand.Parameters.Add("pIdPortafolio", SqlDbType.Float, 18).Value = pSalPortFol.Value

            '...Resultado
            Dim pSalElimIntPort As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalElimIntPort.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalElimIntPort)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            If Not strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Rollback()
                Exit Function
            End If

            ' INSERTAMOS LOS NUEVOS INSTRUMENTOS PARA EL PORTAFOLIO
            For Each pRow As DataRow In dtbInstrumentos.Rows
                SQLCommand = New SqlClient.SqlCommand("Rcp_InstrumentosPortafolio_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)
                SQLCommand.CommandType = CommandType.StoredProcedure
                SQLCommand.CommandText = "Rcp_InstrumentosPortafolio_Mantencion"
                SQLCommand.Parameters.Clear()

                SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = "GUARDAR"
                SQLCommand.Parameters.Add("pIdPortafolio", SqlDbType.Float, 18).Value = pSalPortFol.Value
                SQLCommand.Parameters.Add("pIdInstrumento", SqlDbType.Float, 14).Value = pRow("IDINSTRUMENTO").ToString
                SQLCommand.Parameters.Add("pPorcInstrumento", SqlDbType.Decimal, 10.4).Value = pRow("PORC").ToString

                '...Resultado
                Dim pSalInstPort As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                pSalInstPort.Direction = Data.ParameterDirection.Output
                SQLCommand.Parameters.Add(pSalInstPort)

                SQLCommand.ExecuteNonQuery()

                strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
                If Not strDescError.ToUpper.Trim = "OK" Then
                    Exit For
                End If
            Next

            If Not strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Rollback()
                Exit Function
            End If
            '___________________________________________________________________________________________________________


            ' SECCION PERFILES PORTAFOLIO
            '___________________________________________________________________________________________________________
            ' ELIMINAMOS LOS PERFILES ASOCIADOS AL PORTAFOLIO
            SQLCommand = New SqlClient.SqlCommand("Rcp_PerfilPortafolio_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_PerfilPortafolio_Mantencion"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = "ELIMINAR"
            SQLCommand.Parameters.Add("pIdPortafolio", SqlDbType.Float, 18).Value = pSalPortFol.Value

            '...Resultado
            Dim pSalElimPerfPort As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalElimPerfPort.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalElimPerfPort)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            If Not strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Rollback()
                Exit Function
            End If


            ' INSERTAMOS LOS NUEVOS PERFILES ASOCIADOS AL PORTAFOLIO
            For Each pRow As DataRow In dtbPerfilesRiesgo.Rows
                SQLCommand = New SqlClient.SqlCommand("Rcp_PerfilPortafolio_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)
                SQLCommand.CommandType = CommandType.StoredProcedure
                SQLCommand.CommandText = "Rcp_PerfilPortafolio_Mantencion"
                SQLCommand.Parameters.Clear()

                SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = "GUARDAR"
                SQLCommand.Parameters.Add("pIdPortafolio", SqlDbType.Float, 18).Value = pSalPortFol.Value
                SQLCommand.Parameters.Add("pIdPerfilRiesgo", SqlDbType.Float, 3).Value = pRow("IDPERFIL")

                '...Resultado
                Dim pSalPerfPort As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                pSalPerfPort.Direction = Data.ParameterDirection.Output
                SQLCommand.Parameters.Add(pSalPerfPort)

                SQLCommand.ExecuteNonQuery()

                strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
                If Not strDescError.ToUpper.Trim = "OK" Then
                    Exit For
                End If
            Next

            ' AQUI SE REALIZA EL COMMIT POR QUE ES LA ULTIMA OPERACION 
            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If
            '___________________________________________________________________________________________________________


        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            Portafolio_Mantenedor = strDescError
        End Try
    End Function

    Public Function TraePortaFolioInstrumento(ByVal lngIdInstrumento As Long, _
                                    ByVal strColumnas As String, _
                                    ByRef strRetorno As String, _
                                    Optional ByVal strEstado As String = "") As DataSet


        Dim LDS_Portfolio As New DataSet
        Dim LDS_Portfolio_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_PortafoliosActivos_BuscarPorInstrumento"
        Lstr_NombreTabla = "PORTFOLIO"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdInstrumento", SqlDbType.Float, 10).Value = lngIdInstrumento
            SQLCommand.Parameters.Add("pEstado", SqlDbType.Char, 3).Value = IIf(strEstado = "", DBNull.Value, strEstado)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Portfolio, Lstr_NombreTabla)

            LDS_Portfolio_Aux = LDS_Portfolio


            If strColumnas.Trim = "" Then
                LDS_Portfolio_Aux = LDS_Portfolio
            Else
                LDS_Portfolio_Aux = LDS_Portfolio.Copy
                For Each Columna In LDS_Portfolio.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Portfolio_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Portfolio_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            Return LDS_Portfolio_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

#End Region

End Class
