﻿Public Class ClsFiltro

    Public Function ListaPorFiltro(ByVal strTipoReporte As String, _
                                   ByVal strFiltro As String, _
                                   ByRef strRetorno As String, _
                                   Optional ByVal pIDAsesor As Long = 0, _
                                   Optional ByVal pCod_Tipo_Admin As String = "", _
                                   Optional ByVal pId_Cliente As Long = 0, _
                                   Optional ByVal pId_Cuenta As Long = 0, _
                                   Optional ByVal pId_GrupoCuenta As Long = 0, _
                                   Optional ByVal pId_Negocio As Long = 0, _
                                   Optional ByVal pId_Administrador As Long = 0, _
                                   Optional ByVal pTodasLasCuentas As String = "") As DataSet
        Dim LDS_Clientes As New DataSet
        Dim LDS_Clientes_Aux As New DataSet
        Dim Lstr_NombreTabla As String
        Dim LstrProcedimiento
        Dim strDescError As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Lista_Por_Filtro"
        Lstr_NombreTabla = "CLIENTES"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            '@pIdNegocio	numeric(10),
            '@pIdUsuario	numeric(10),
            '@pTipoReporte	varchar(01), -- (C)uenta, (R)ut o (G)rupo
            '@pFiltro	varchar(8000)

            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Float, 9).Value = IIf(pId_Negocio = 0, DBNull.Value, pId_Negocio)
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Float, 6).Value = IIf(glngIdUsuario = 0, DBNull.Value, glngIdUsuario)
            SQLCommand.Parameters.Add("@pTipoReporte", SqlDbType.VarChar, 1).Value = strTipoReporte
            SQLCommand.Parameters.Add("@pFiltro", SqlDbType.Text, 60000).Value = strFiltro
            SQLCommand.Parameters.Add("@pAsesor", SqlDbType.Float, 6).Value = IIf(pIDAsesor = 0, DBNull.Value, pIDAsesor)
            SQLCommand.Parameters.Add("@pTipo_Admin", SqlDbType.VarChar, 10).Value = IIf(pCod_Tipo_Admin = "" Or pCod_Tipo_Admin = "0", DBNull.Value, pCod_Tipo_Admin)
            SQLCommand.Parameters.Add("@pId_Cliente", SqlDbType.Float, 6).Value = IIf(pId_Cliente = 0, DBNull.Value, pId_Cliente)
            SQLCommand.Parameters.Add("@pId_Cuenta", SqlDbType.Float, 6).Value = IIf(pId_Cuenta = 0, DBNull.Value, pId_Cuenta)
            SQLCommand.Parameters.Add("@pId_GrupoCuenta", SqlDbType.Float, 6).Value = IIf(pId_GrupoCuenta = 0, DBNull.Value, pId_GrupoCuenta)
            SQLCommand.Parameters.Add("pId_Administrador", SqlDbType.Float, 6).Value = IIf(pId_Administrador = 0, DBNull.Value, pId_Administrador)
            If pTodasLasCuentas.ToUpper = "S" Then
                SQLCommand.Parameters.Add("@pTodas_Las_Cuentas", SqlDbType.VarChar, 10).Value = IIf(pTodasLasCuentas = "", DBNull.Value, pTodasLasCuentas)
            End If


            'Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            'ParametroSal2.Direction = Data.ParameterDirection.Output
            'SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()
            'strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Clientes, Lstr_NombreTabla)

            LDS_Clientes_Aux = LDS_Clientes

            strRetorno = "OK"
            Return LDS_Clientes_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function



End Class
