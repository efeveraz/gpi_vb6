﻿Public Class ClsEntidadGPI

    Public Function Entidad_GPI_Consultar(ByVal dblIdEntidadGPI As Double, _
                                   ByVal strDscEntidadGPI As String, _
                                   ByVal strTabla As String, _
                                   ByRef strRetorno As String) As DataSet

        Dim LDS_EntidadGPI As New DataSet
        Dim LDS_EntidadGPI_Aux As New DataSet
        Dim Lstr_NombreTabla As String
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_EntidadGPI_Consultar"
        Lstr_NombreTabla = "ENTIDADGPI"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdEntidadGPI", SqlDbType.VarChar, 40).Value = IIf(dblIdEntidadGPI = 0, DBNull.Value, dblIdEntidadGPI)
            SQLCommand.Parameters.Add("pDscEntidadGPI", SqlDbType.VarChar, 50).Value = IIf(strDscEntidadGPI = "", DBNull.Value, strDscEntidadGPI)
            SQLCommand.Parameters.Add("pTabla", SqlDbType.VarChar, 50).Value = IIf(strTabla = "", DBNull.Value, strTabla)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_EntidadGPI, Lstr_NombreTabla)

            LDS_EntidadGPI_Aux = LDS_EntidadGPI

            strRetorno = "OK"
            Return LDS_EntidadGPI_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function Interfaz_GPI_Consultar(ByVal strColumnas As String, _
                                           ByRef strRetorno As String) As DataSet

        Dim LDS_Conceptos_GPI As New DataSet
        Dim LDS_Conceptos_GPI_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "ITZ_GPI_Consultar"
        Lstr_NombreTabla = "Conceptos"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            '...Resultado
            Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultado.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Conceptos_GPI, Lstr_NombreTabla)

            LDS_Conceptos_GPI_Aux = LDS_Conceptos_GPI

            If strColumnas.Trim = "" Then
                LDS_Conceptos_GPI_Aux = LDS_Conceptos_GPI
            Else
                LDS_Conceptos_GPI_Aux = LDS_Conceptos_GPI.Copy
                For Each Columna In LDS_Conceptos_GPI.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Conceptos_GPI_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Conceptos_GPI_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            LDS_Conceptos_GPI.Dispose()
            Return LDS_Conceptos_GPI_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function Interfaz_GPI_Mantenedor(ByVal strAccion As String, _
                                            ByVal dtsDatosTabla As DataSet) As String
        Dim strDescError As String = "OK"
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction
        Dim LstrProcedimiento As String = ""

        LstrProcedimiento = "ITZ_GPI_Cargar"


        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion
        
        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, MiTransaccionSQL.Connection, MiTransaccionSQL)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 20).Value = UCase(strAccion.Trim)

            '...Resultado
            Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultado.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If Trim(strDescError) = "OK" Then
                MiTransaccionSQL.Commit()
                Return ("OK")
            Else
                MiTransaccionSQL.Rollback()
                Return strDescError
            End If


        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en la Carga de Interfaz GPI" & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            Interfaz_GPI_Mantenedor = strDescError
        End Try
    End Function

  Public Function RelContraparte_Cuenta_Mantenedor(ByVal strAccion As String, _
                                                   ByVal strCodExContraparte As String, _
                                                   ByVal strIdEntidad As String, _
                                                   ByVal strValor As String, _
                                                   ByRef strDescError As String) As String

    Dim SQLCommand As New SqlClient.SqlCommand
    Dim SQLConnect As Cls_Conexion = New Cls_Conexion
    Dim MiTransaccionSQL As SqlClient.SqlTransaction

    '...Abre la conexion
    SQLConnect.Abrir()
    MiTransaccionSQL = SQLConnect.Transaccion

    Try
      SQLCommand = New SqlClient.SqlCommand("Rcp_Rel_ContraparteGpi_Cuenta_Mantenedor", MiTransaccionSQL.Connection, MiTransaccionSQL)

      SQLCommand.CommandType = CommandType.StoredProcedure
      SQLCommand.CommandText = "Rcp_Rel_ContraparteGpi_Cuenta_Mantenedor"
      SQLCommand.Parameters.Clear()

      SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 10).Value = strAccion
      SQLCommand.Parameters.Add("pCodExContraparte", SqlDbType.VarChar, 50).Value = strCodExContraparte
      SQLCommand.Parameters.Add("pIdEntidad", SqlDbType.VarChar, 50).Value = IIf(strIdEntidad = "", DBNull.Value, strIdEntidad)
      SQLCommand.Parameters.Add("pValor", SqlDbType.VarChar, 50).Value = strValor

      '...Resultado
      Dim pSalResultInstr As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
      pSalResultInstr.Direction = Data.ParameterDirection.Output
      SQLCommand.Parameters.Add(pSalResultInstr)

      SQLCommand.ExecuteNonQuery()

      strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

      If strDescError.ToUpper.Trim = "OK" Then
        MiTransaccionSQL.Commit()
      Else
        MiTransaccionSQL.Rollback()
      End If
      '___________________________________________________________________________________________________________

    Catch Ex As Exception
      MiTransaccionSQL.Rollback()
      strDescError = "Error: " & vbCr & Ex.Message
    Finally
      SQLConnect.Cerrar()
      RelContraparte_Cuenta_Mantenedor = strDescError
    End Try
  End Function
  Public Function VerificaExisteRel_Entidad_GPI(ByVal plngEntidad1 As Long, _
                                                ByVal plngEntidad2 As Long, _
                                                ByVal plngIdEntidadGPI As Long, _
                                                ByVal pstrValor As String) As Integer
    Dim LDS_EntidadGPI As New DataSet
    Dim LDS_EntidadGPI_Aux As New DataSet
    Dim Lstr_NombreTabla As String
    Dim LstrProcedimiento

    Dim SQLCommand As New SqlClient.SqlCommand
    Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

    Dim Connect As Cls_Conexion = New Cls_Conexion

    LstrProcedimiento = "Rcp_Rel_Entidad_GPI_Existe"
    Lstr_NombreTabla = "ENTIDADGPI_Exis"

    Connect.Abrir()

    Try
      SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

      SQLCommand.CommandType = CommandType.StoredProcedure
      SQLCommand.CommandText = LstrProcedimiento
      SQLCommand.Parameters.Clear()

      SQLCommand.Parameters.Add("pIdEntidad1", SqlDbType.Float, 10).Value = IIf(plngEntidad1 = 0, DBNull.Value, plngEntidad1)
      SQLCommand.Parameters.Add("pIdEntidad2", SqlDbType.Float, 10).Value = IIf(plngEntidad2 = 0, DBNull.Value, plngEntidad2)
      SQLCommand.Parameters.Add("pIdEntidadGPI", SqlDbType.Float, 10).Value = IIf(plngIdEntidadGPI = 0, DBNull.Value, plngIdEntidadGPI)
      SQLCommand.Parameters.Add("pValor", SqlDbType.VarChar, 50).Value = IIf(pstrValor = "", DBNull.Value, pstrValor)

      '...Resultado
      Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.Int, 1)
      pSalResultado.Direction = Data.ParameterDirection.Output
      SQLCommand.Parameters.Add(pSalResultado)

      SQLDataAdapter.SelectCommand = SQLCommand
      SQLDataAdapter.Fill(LDS_EntidadGPI, Lstr_NombreTabla)

      LDS_EntidadGPI_Aux = LDS_EntidadGPI

      VerificaExisteRel_Entidad_GPI = SQLCommand.Parameters("pResultado").Value

    Catch ex As Exception
      gFun_ResultadoMsg("1|" & ex.Message, "VerificaExisteRel_Entidad_GPI")
      VerificaExisteRel_Entidad_GPI = 0
    Finally
      Connect.Cerrar()
    End Try
  End Function
  Public Function EntregaIdEntidadGPI(ByVal pDsc_EntidadGPI As String, ByVal pTabla As String) As Long
    Dim lstrMensaje As String = "OK"
    Dim lstrRetorno As String = ""
    Dim DS_EntidadGPI As DataSet
    Dim lobjEntidadGPI As ClsEntidadGPI = New ClsEntidadGPI

    Try
      DS_EntidadGPI = lobjEntidadGPI.Entidad_GPI_Consultar(0, pDsc_EntidadGPI, pTabla, lstrRetorno)

      If DS_EntidadGPI.Tables.Count > 0 Then
        If DS_EntidadGPI.Tables(0).Rows.Count > 0 Then
          EntregaIdEntidadGPI = DS_EntidadGPI.Tables(0).Rows(0).Item("ID_ENTIDAD_GPI")
        Else
          lstrMensaje = "1|No se encontró la EntidadGPI: " & pDsc_EntidadGPI
          EntregaIdEntidadGPI = 0
        End If
      Else
        lstrMensaje = "1|No hay EntidadGPI definida para: " & pDsc_EntidadGPI
        EntregaIdEntidadGPI = 0
      End If

    Catch ex As Exception
      lstrMensaje = "1|Error al buscar la EntidadGPI: " & pDsc_EntidadGPI & " -> " & ex.Message
      EntregaIdEntidadGPI = 0
    End Try
    If lstrMensaje <> "OK" Then
      gFun_ResultadoMsg(lstrMensaje, "EntregaIdEntidadGPI")
    End If
  End Function


  Public Function Rel_Entidad_Contraparte_cuenta_Ver(ByVal strCodContraparte As String, _
                                                     ByVal strColumnas As String, _
                                                     ByRef strRetorno As String) As DataSet

    Dim LDS_Rel_Entidad As New DataSet
    Dim LDS_Rel_Entidad_Aux As New DataSet
    Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
    Dim LInt_Col As Integer
    Dim LInt_NomCol As String
    Dim Lstr_NombreTabla As String
    Dim Columna As DataColumn
    Dim Remove As Boolean
    Dim LstrProcedimiento

    Dim SQLCommand As New SqlClient.SqlCommand
    Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

    Dim Connect As Cls_Conexion = New Cls_Conexion

    LstrProcedimiento = "Rcp_Rel_ContraparteGpi_Ver_Cuenta"
    Lstr_NombreTabla = "ENTIDADGPI"

    Connect.Abrir()

    Try
      SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

      SQLCommand.CommandType = CommandType.StoredProcedure
      SQLCommand.CommandText = LstrProcedimiento
      SQLCommand.Parameters.Clear()



      SQLCommand.Parameters.Add("pCod_Contraparte", SqlDbType.VarChar, 50).Value = IIf(strCodContraparte = "", DBNull.Value, strCodContraparte)
      'SQLCommand.Parameters.Add("pCuentaFoh", SqlDbType.VarChar, 50).Value = IIf(strCuentaFoh = "", DBNull.Value, strCuentaFoh)


      SQLDataAdapter.SelectCommand = SQLCommand
      SQLDataAdapter.Fill(LDS_Rel_Entidad, Lstr_NombreTabla)

      LDS_Rel_Entidad_Aux = LDS_Rel_Entidad

      If strColumnas.Trim = "" Then
        LDS_Rel_Entidad_Aux = LDS_Rel_Entidad
      Else
        LDS_Rel_Entidad_Aux = LDS_Rel_Entidad.Copy
        For Each Columna In LDS_Rel_Entidad.Tables(Lstr_NombreTabla).Columns
          Remove = True
          For LInt_Col = 0 To LArr_NombreColumna.Length - 1
            LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
            If Columna.ColumnName = LInt_NomCol Then
              Remove = False
            End If
          Next
          If Remove Then
            LDS_Rel_Entidad_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
          End If
        Next
      End If

      For LInt_Col = 0 To LArr_NombreColumna.Length - 1
        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
        For Each Columna In LDS_Rel_Entidad.Tables(Lstr_NombreTabla).Columns
          If Columna.ColumnName = LInt_NomCol Then
            Columna.SetOrdinal(LInt_Col)
            Exit For
          End If
        Next
      Next

      strRetorno = "OK"
      Return LDS_Rel_Entidad_Aux

    Catch ex As Exception
      strRetorno = ex.Message
      Return Nothing
    Finally
      Connect.Cerrar()
    End Try

  End Function

  Public Function Rel_Contraparte_Usuarios_Ver(ByVal strCodContraparte As String, _
                                               ByVal strColumnas As String, _
                                               ByRef strRetorno As String) As DataSet

    Dim LDS_Rel_EntUsuario As New DataSet
    Dim LDS_Rel_EntUsuario_Aux As New DataSet
    Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
    Dim LInt_Col As Integer
    Dim LInt_NomCol As String
    Dim Lstr_NombreTabla As String
    Dim Columna As DataColumn
    Dim Remove As Boolean
    Dim LstrProcedimiento

    Dim SQLCommand As New SqlClient.SqlCommand
    Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

    Dim Connect As Cls_Conexion = New Cls_Conexion

    LstrProcedimiento = "Rcp_Rel_ContraparteGpi_Ver_Usuario"
    Lstr_NombreTabla = "USUARIO"

    Connect.Abrir()

    Try
      SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

      SQLCommand.CommandType = CommandType.StoredProcedure
      SQLCommand.CommandText = LstrProcedimiento
      SQLCommand.Parameters.Clear()



      SQLCommand.Parameters.Add("pCod_Contraparte", SqlDbType.VarChar, 50).Value = IIf(strCodContraparte = "", DBNull.Value, strCodContraparte)
      'SQLCommand.Parameters.Add("pCuentaFoh", SqlDbType.VarChar, 50).Value = IIf(strCuentaFoh = "", DBNull.Value, strCuentaFoh)


      SQLDataAdapter.SelectCommand = SQLCommand
      SQLDataAdapter.Fill(LDS_Rel_EntUsuario, Lstr_NombreTabla)

      LDS_Rel_EntUsuario_Aux = LDS_Rel_EntUsuario

      If strColumnas.Trim = "" Then
        LDS_Rel_EntUsuario_Aux = LDS_Rel_EntUsuario
      Else
        LDS_Rel_EntUsuario_Aux = LDS_Rel_EntUsuario.Copy
        For Each Columna In LDS_Rel_EntUsuario.Tables(Lstr_NombreTabla).Columns
          Remove = True
          For LInt_Col = 0 To LArr_NombreColumna.Length - 1
            LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
            If Columna.ColumnName = LInt_NomCol Then
              Remove = False
            End If
          Next
          If Remove Then
            LDS_Rel_EntUsuario_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
          End If
        Next
      End If

      For LInt_Col = 0 To LArr_NombreColumna.Length - 1
        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
        For Each Columna In LDS_Rel_EntUsuario.Tables(Lstr_NombreTabla).Columns
          If Columna.ColumnName = LInt_NomCol Then
            Columna.SetOrdinal(LInt_Col)
            Exit For
          End If
        Next
      Next

      strRetorno = "OK"
      Return LDS_Rel_EntUsuario_Aux

    Catch ex As Exception
      strRetorno = ex.Message
      Return Nothing
    Finally
      Connect.Cerrar()
    End Try

  End Function


  Public Function RelContraparte_Usuario_Mantenedor(ByVal strAccion As String, _
                                                    ByVal strCodExContraparte As String, _
                                                    ByVal strIdEntidad As String, _
                                                    ByVal strValor As String, _
                                                    ByRef strDescError As String) As String

    Dim SQLCommand As New SqlClient.SqlCommand
    Dim SQLConnect As Cls_Conexion = New Cls_Conexion
    Dim MiTransaccionSQL As SqlClient.SqlTransaction

    '...Abre la conexion
    SQLConnect.Abrir()
    MiTransaccionSQL = SQLConnect.Transaccion

    Try
      SQLCommand = New SqlClient.SqlCommand("Rcp_Rel_ContraparteGpi_Usuario_Mantenedor", MiTransaccionSQL.Connection, MiTransaccionSQL)

      SQLCommand.CommandType = CommandType.StoredProcedure
      SQLCommand.CommandText = "Rcp_Rel_ContraparteGpi_Usuario_Mantenedor"
      SQLCommand.Parameters.Clear()

      SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 10).Value = strAccion
      SQLCommand.Parameters.Add("pCodExContraparte", SqlDbType.VarChar, 50).Value = strCodExContraparte
      SQLCommand.Parameters.Add("pIdEntidad", SqlDbType.VarChar, 50).Value = IIf(strIdEntidad = "", DBNull.Value, strIdEntidad)
      SQLCommand.Parameters.Add("pValor", SqlDbType.VarChar, 50).Value = strValor

      '...Resultado
      Dim pSalResultInstr As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
      pSalResultInstr.Direction = Data.ParameterDirection.Output
      SQLCommand.Parameters.Add(pSalResultInstr)

      SQLCommand.ExecuteNonQuery()

      strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

      If strDescError.ToUpper.Trim = "OK" Then
        MiTransaccionSQL.Commit()
      Else
        MiTransaccionSQL.Rollback()
      End If
      '___________________________________________________________________________________________________________

    Catch Ex As Exception
      MiTransaccionSQL.Rollback()
      strDescError = "Error: " & vbCr & Ex.Message
    Finally
      SQLConnect.Cerrar()
      RelContraparte_Usuario_Mantenedor = strDescError
    End Try
  End Function

  Public Function PublicadorXAlias_Mantenedor(ByVal strId_Entidad As String, _
                                            ByVal intTipoEndtidad As Integer, _
                                            ByRef dstAlias As DataSet) As String
    Dim strDescError As String = "OK"
    Dim SQLCommand As New SqlClient.SqlCommand
    Dim SQLConnect As Cls_Conexion = New Cls_Conexion
    Dim MiTransaccionSQL As SqlClient.SqlTransaction

    '...Abre la conexion
    SQLConnect.Abrir()
    MiTransaccionSQL = SQLConnect.Transaccion

    Try
      '+ ELIMINAMOS LOS ALIAS YA EXISTENTES
      SQLCommand = New SqlClient.SqlCommand("Rcp_AliasPublicador_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)
      SQLCommand.CommandType = CommandType.StoredProcedure
      SQLCommand.CommandText = "Rcp_AliasPublicador_Mantencion"
      SQLCommand.Parameters.Clear()

      SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = "ELIMINAR"
      SQLCommand.Parameters.Add("pIdEntidad", SqlDbType.Float, 10).Value = IIf(strId_Entidad.Trim = "", DBNull.Value, strId_Entidad.Trim)
      SQLCommand.Parameters.Add("pIdEntidadGPI", SqlDbType.Float, 10).Value = intTipoEndtidad
      SQLCommand.Parameters.Add("pIdPublicador", SqlDbType.VarChar, 9).Value = DBNull.Value
      SQLCommand.Parameters.Add("pValor", SqlDbType.VarChar, 50).Value = DBNull.Value
      SQLCommand.Parameters.Add("pDscPublicador", SqlDbType.VarChar, 50).Value = DBNull.Value

      '...Resultado
      Dim pSalElimIntPort1 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
      pSalElimIntPort1.Direction = Data.ParameterDirection.Output
      SQLCommand.Parameters.Add(pSalElimIntPort1)

      SQLCommand.ExecuteNonQuery()

      strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
      If strDescError.ToUpper.Trim <> "OK" Then
        MiTransaccionSQL.Rollback()
        Exit Function
      End If

      '+ INSERTAMOS LOS NUEVOS ALIAS PUBLICADORES PARA LA ENTIDAD GPI 
      If Not (IsNothing(dstAlias)) Then
        If dstAlias.Tables(0).Rows.Count <> Nothing Then
          For Each dtrRegristro As DataRow In dstAlias.Tables(0).Rows
            SQLCommand = New SqlClient.SqlCommand("Rcp_AliasPublicador_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_AliasPublicador_Mantencion"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = "INSERTAR"
            SQLCommand.Parameters.Add("pIdEntidad", SqlDbType.VarChar, 50).Value = strId_Entidad
            SQLCommand.Parameters.Add("pIdEntidadGPI", SqlDbType.VarChar, 90).Value = intTipoEndtidad
            SQLCommand.Parameters.Add("pIdPublicador", SqlDbType.VarChar, 9).Value = dtrRegristro("ID_PUBLICADOR").ToString
            SQLCommand.Parameters.Add("pValor", SqlDbType.VarChar, 50).Value = dtrRegristro("VALOR").ToString
            SQLCommand.Parameters.Add("pDscPublicador", SqlDbType.VarChar, 50).Value = dtrRegristro("DSC_PUBLICADOR").ToString

            '...Resultado
            Dim pSalInstPort As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalInstPort.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalInstPort)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            If strDescError.ToUpper.Trim <> "OK" Then
              Exit For
            End If
          Next
        End If
        If strDescError.ToUpper.Trim = "OK" Then
          MiTransaccionSQL.Commit()
        Else
          MiTransaccionSQL.Rollback()
        End If
      End If
    Catch Ex As Exception
      MiTransaccionSQL.Rollback()
      strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
    Finally
      SQLConnect.Cerrar()
      PublicadorXAlias_Mantenedor = strDescError
    End Try
  End Function

  Public Function RelContraparte_Emisor_Mantenedor(ByVal intId_Emisor As Integer, _
                                                   ByVal strTipoEndtidad As String, _
                                                   ByRef DS_AliasEmisor As DataSet) As String
    Dim strDescError As String = "OK"
    Dim SQLCommand As New SqlClient.SqlCommand
    Dim SQLConnect As Cls_Conexion = New Cls_Conexion
    Dim MiTransaccionSQL As SqlClient.SqlTransaction

    ''...Abre la conexion
    SQLConnect.Abrir()
    MiTransaccionSQL = SQLConnect.Transaccion

    Try
      '  '+ ELIMINAMOS LOS ALIAS YA EXISTENTES
      SQLCommand = New SqlClient.SqlCommand("Rcp_Rel_ContraparteGpi_Emisor_Mantenedor", MiTransaccionSQL.Connection, MiTransaccionSQL)
      SQLCommand.CommandType = CommandType.StoredProcedure
      SQLCommand.CommandText = "Rcp_Rel_ContraparteGpi_Emisor_Mantenedor"
      SQLCommand.Parameters.Clear()

      SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = "ELIMINAR"
      SQLCommand.Parameters.Add("pIdEntidad", SqlDbType.Float, 10).Value = IIf(intId_Emisor = 0, DBNull.Value, intId_Emisor)
      SQLCommand.Parameters.Add("pValor", SqlDbType.VarChar, 50).Value = DBNull.Value
      SQLCommand.Parameters.Add("pIdTipoEntidadGPI", SqlDbType.VarChar, 50).Value = strTipoEndtidad
      SQLCommand.Parameters.Add("pIdContraparte", SqlDbType.VarChar, 50).Value = DBNull.Value
      SQLCommand.Parameters.Add("pDscContraparte", SqlDbType.VarChar, 50).Value = DBNull.Value




      '...Resultado
      Dim pSalElimIntPort1 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
      pSalElimIntPort1.Direction = Data.ParameterDirection.Output
      SQLCommand.Parameters.Add(pSalElimIntPort1)

      SQLCommand.ExecuteNonQuery()

      strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
      If strDescError.ToUpper.Trim <> "OK" Then
        MiTransaccionSQL.Rollback()
        Exit Function
      End If

      '+ INSERTAMOS LOS NUEVOS ALIAS PUBLICADORES PARA LA ENTIDAD GPI 
      If Not (IsNothing(DS_AliasEmisor)) Then
        If DS_AliasEmisor.Tables(0).Rows.Count <> Nothing Then
          For Each dtrRegristro As DataRow In DS_AliasEmisor.Tables(0).Rows
            SQLCommand = New SqlClient.SqlCommand("Rcp_Rel_ContraparteGpi_Emisor_Mantenedor", MiTransaccionSQL.Connection, MiTransaccionSQL)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Rel_ContraparteGpi_Emisor_Mantenedor"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = "INSERTAR"
            SQLCommand.Parameters.Add("pIdEntidad", SqlDbType.VarChar, 50).Value = intId_Emisor
            SQLCommand.Parameters.Add("pValor", SqlDbType.VarChar, 50).Value = dtrRegristro("VALOR").ToString
            SQLCommand.Parameters.Add("pIdContraparte", SqlDbType.VarChar, 50).Value = dtrRegristro("ID_ENTIDAD_CONTRAPARTE").ToString
            SQLCommand.Parameters.Add("pIdTipoEntidadGPI", SqlDbType.VarChar, 50).Value = strTipoEndtidad
            SQLCommand.Parameters.Add("pDscContraparte", SqlDbType.VarChar, 50).Value = dtrRegristro("DSC_CONTRAPARTE").ToString


            '...Resultado
            Dim pSalInstPort As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalInstPort.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalInstPort)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            If strDescError.ToUpper.Trim <> "OK" Then
              Exit For
            End If
          Next
        End If
        If strDescError.ToUpper.Trim = "OK" Then
          MiTransaccionSQL.Commit()
        Else
          MiTransaccionSQL.Rollback()
        End If
      End If
    Catch Ex As Exception
      MiTransaccionSQL.Rollback()
      strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
    Finally
      SQLConnect.Cerrar()
      RelContraparte_Emisor_Mantenedor = strDescError
    End Try
  End Function


  Public Function Rel_Entidad_Contraparte_Emisor_Ver(ByVal intIdEmisor As Integer, _
                                                     ByVal strColumnas As String, _
                                                     ByRef strRetorno As String) As DataSet

    Dim LDS_Rel_Entidad As New DataSet
    Dim LDS_Rel_Entidad_Aux As New DataSet
    Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
    Dim LInt_Col As Integer
    Dim LInt_NomCol As String
    Dim Lstr_NombreTabla As String
    Dim Columna As DataColumn
    Dim Remove As Boolean
    Dim LstrProcedimiento

    Dim SQLCommand As New SqlClient.SqlCommand
    Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

    Dim Connect As Cls_Conexion = New Cls_Conexion

    LstrProcedimiento = "Rcp_Rel_ContraparteGpi_Ver_Emisor"
    Lstr_NombreTabla = "ENTIDADGPI"

    Connect.Abrir()

    Try
      SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

      SQLCommand.CommandType = CommandType.StoredProcedure
      SQLCommand.CommandText = LstrProcedimiento
      SQLCommand.Parameters.Clear()



      'SQLCommand.Parameters.Add("pCod_Contraparte", SqlDbType.VarChar, 50).Value = IIf(strCodContraparte = "", DBNull.Value, strCodContraparte)
      SQLCommand.Parameters.Add("pIdEmisor", SqlDbType.VarChar, 50).Value = IIf(intIdEmisor = 0, DBNull.Value, intIdEmisor)

      SQLDataAdapter.SelectCommand = SQLCommand
      SQLDataAdapter.Fill(LDS_Rel_Entidad, Lstr_NombreTabla)

      LDS_Rel_Entidad_Aux = LDS_Rel_Entidad

      If strColumnas.Trim = "" Then
        LDS_Rel_Entidad_Aux = LDS_Rel_Entidad
      Else
        LDS_Rel_Entidad_Aux = LDS_Rel_Entidad.Copy
        For Each Columna In LDS_Rel_Entidad.Tables(Lstr_NombreTabla).Columns
          Remove = True
          For LInt_Col = 0 To LArr_NombreColumna.Length - 1
            LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
            If Columna.ColumnName = LInt_NomCol Then
              Remove = False
            End If
          Next
          If Remove Then
            LDS_Rel_Entidad_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
          End If
        Next
      End If

      For LInt_Col = 0 To LArr_NombreColumna.Length - 1
        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
        For Each Columna In LDS_Rel_Entidad.Tables(Lstr_NombreTabla).Columns
          If Columna.ColumnName = LInt_NomCol Then
            Columna.SetOrdinal(LInt_Col)
            Exit For
          End If
        Next
      Next

      strRetorno = "OK"
      Return LDS_Rel_Entidad_Aux

    Catch ex As Exception
      strRetorno = ex.Message
      Return Nothing
    Finally
      Connect.Cerrar()
    End Try

  End Function
  Public Function RelContraparte_Moneda_Mantenedor(ByVal strCodMoneda As String, _
                                                   ByVal intTipoEndtidad As String, _
                                                   ByRef DS_AliasMoneda As DataSet) As String
    Dim strDescError As String = "OK"
    Dim SQLCommand As New SqlClient.SqlCommand
    Dim SQLConnect As Cls_Conexion = New Cls_Conexion
    Dim MiTransaccionSQL As SqlClient.SqlTransaction

    ''...Abre la conexion
    SQLConnect.Abrir()
    MiTransaccionSQL = SQLConnect.Transaccion

    Try
      '  '+ ELIMINAMOS LOS ALIAS YA EXISTENTES
      SQLCommand = New SqlClient.SqlCommand("Rcp_Rel_ContraparteGpi_Moneda_Mantenedor", MiTransaccionSQL.Connection, MiTransaccionSQL)
      SQLCommand.CommandType = CommandType.StoredProcedure
      SQLCommand.CommandText = "Rcp_Rel_ContraparteGpi_Moneda_Mantenedor"
      SQLCommand.Parameters.Clear()

      SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = "ELIMINAR"
      SQLCommand.Parameters.Add("pIdEntidad", SqlDbType.VarChar, 3).Value = IIf(strCodMoneda = "", DBNull.Value, strCodMoneda)
      SQLCommand.Parameters.Add("pValor", SqlDbType.VarChar, 50).Value = DBNull.Value
      SQLCommand.Parameters.Add("pIdTipoEntidadGPI", SqlDbType.VarChar, 50).Value = intTipoEndtidad
      SQLCommand.Parameters.Add("pIdContraparte", SqlDbType.VarChar, 50).Value = DBNull.Value
      SQLCommand.Parameters.Add("pDscContraparte", SqlDbType.VarChar, 50).Value = DBNull.Value




      '...Resultado
      Dim pSalElimIntPort1 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
      pSalElimIntPort1.Direction = Data.ParameterDirection.Output
      SQLCommand.Parameters.Add(pSalElimIntPort1)

      SQLCommand.ExecuteNonQuery()

      strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
      If strDescError.ToUpper.Trim <> "OK" Then
        MiTransaccionSQL.Rollback()
        Exit Function
      End If

      '+ INSERTAMOS LOS NUEVOS ALIAS PUBLICADORES PARA LA ENTIDAD GPI 
      If Not (IsNothing(DS_AliasMoneda)) Then
        If DS_AliasMoneda.Tables(0).Rows.Count <> Nothing Then
          For Each dtrRegristro As DataRow In DS_AliasMoneda.Tables(0).Rows
            SQLCommand = New SqlClient.SqlCommand("Rcp_Rel_ContraparteGpi_Moneda_Mantenedor", MiTransaccionSQL.Connection, MiTransaccionSQL)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Rel_ContraparteGpi_Moneda_Mantenedor"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = "INSERTAR"
            SQLCommand.Parameters.Add("pIdEntidad", SqlDbType.VarChar, 3).Value = strCodMoneda
            SQLCommand.Parameters.Add("pValor", SqlDbType.VarChar, 50).Value = dtrRegristro("VALOR").ToString
            SQLCommand.Parameters.Add("pIdContraparte", SqlDbType.VarChar, 50).Value = dtrRegristro("ID_ENTIDAD_CONTRAPARTE").ToString
            SQLCommand.Parameters.Add("pIdTipoEntidadGPI", SqlDbType.VarChar, 50).Value = intTipoEndtidad
            SQLCommand.Parameters.Add("pDscContraparte", SqlDbType.VarChar, 50).Value = dtrRegristro("DSC_CONTRAPARTE").ToString


            '...Resultado
            Dim pSalInstPort As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalInstPort.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalInstPort)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            If strDescError.ToUpper.Trim <> "OK" Then
              Exit For
            End If
          Next
        End If
        If strDescError.ToUpper.Trim = "OK" Then
          MiTransaccionSQL.Commit()
        Else
          MiTransaccionSQL.Rollback()
        End If
      End If
    Catch Ex As Exception
      MiTransaccionSQL.Rollback()
      strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
    Finally
      SQLConnect.Cerrar()
      RelContraparte_Moneda_Mantenedor = strDescError
    End Try
  End Function



  Public Function Rel_Entidad_Contraparte_Moneda_Ver(ByVal srtCodMoneda As String, _
                                                     ByVal strColumnas As String, _
                                                     ByRef strRetorno As String) As DataSet

    Dim LDS_Rel_Entidad As New DataSet
    Dim LDS_Rel_Entidad_Aux As New DataSet
    Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
    Dim LInt_Col As Integer
    Dim LInt_NomCol As String
    Dim Lstr_NombreTabla As String
    Dim Columna As DataColumn
    Dim Remove As Boolean
    Dim LstrProcedimiento

    Dim SQLCommand As New SqlClient.SqlCommand
    Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

    Dim Connect As Cls_Conexion = New Cls_Conexion

    LstrProcedimiento = "Rcp_Rel_ContraparteGpi_Ver_Moneda"
    Lstr_NombreTabla = "ENTIDADGPI"

    Connect.Abrir()

    Try
      SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

      SQLCommand.CommandType = CommandType.StoredProcedure
      SQLCommand.CommandText = LstrProcedimiento
      SQLCommand.Parameters.Clear()



      SQLCommand.Parameters.Add("pCod_Moneda", SqlDbType.VarChar, 3).Value = IIf(srtCodMoneda = "", DBNull.Value, srtCodMoneda)


      SQLDataAdapter.SelectCommand = SQLCommand
      SQLDataAdapter.Fill(LDS_Rel_Entidad, Lstr_NombreTabla)

      LDS_Rel_Entidad_Aux = LDS_Rel_Entidad

      If strColumnas.Trim = "" Then
        LDS_Rel_Entidad_Aux = LDS_Rel_Entidad
      Else
        LDS_Rel_Entidad_Aux = LDS_Rel_Entidad.Copy
        For Each Columna In LDS_Rel_Entidad.Tables(Lstr_NombreTabla).Columns
          Remove = True
          For LInt_Col = 0 To LArr_NombreColumna.Length - 1
            LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
            If Columna.ColumnName = LInt_NomCol Then
              Remove = False
            End If
          Next
          If Remove Then
            LDS_Rel_Entidad_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
          End If
        Next
      End If

      For LInt_Col = 0 To LArr_NombreColumna.Length - 1
        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
        For Each Columna In LDS_Rel_Entidad.Tables(Lstr_NombreTabla).Columns
          If Columna.ColumnName = LInt_NomCol Then
            Columna.SetOrdinal(LInt_Col)
            Exit For
          End If
        Next
      Next

      strRetorno = "OK"
      Return LDS_Rel_Entidad_Aux

    Catch ex As Exception
      strRetorno = ex.Message
      Return Nothing
    Finally
      Connect.Cerrar()
    End Try

  End Function

    Public Function Rel_Entidad_Contraparte_Asesor_Ver(ByVal IdAsesor As String, _
                                                     ByVal strColumnas As String, _
                                                     ByRef strRetorno As String) As DataSet
        Dim IidAsesor As Integer = CInt(IdAsesor)
        Dim LDS_Rel_Entidad As New DataSet
        Dim LDS_Rel_Entidad_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Rel_ContraparteGpi_Ver_Asesor"
        Lstr_NombreTabla = "ENTIDADGPI"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()



            SQLCommand.Parameters.Add("pIdAsesor", SqlDbType.Int, 10).Value = IIf(IdAsesor = 0, DBNull.Value, IdAsesor)


            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Rel_Entidad, Lstr_NombreTabla)

            LDS_Rel_Entidad_Aux = LDS_Rel_Entidad

            If strColumnas.Trim = "" Then
                LDS_Rel_Entidad_Aux = LDS_Rel_Entidad
            Else
                LDS_Rel_Entidad_Aux = LDS_Rel_Entidad.Copy
                For Each Columna In LDS_Rel_Entidad.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Rel_Entidad_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Rel_Entidad.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            Return LDS_Rel_Entidad_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function RelContraparte_Asesor_Mantenedor(ByVal intId_Asesor As Integer, _
                                                  ByVal strTipoEndtidad As String, _
                                                  ByRef DS_AliasAsesor As DataSet) As String
        Dim strDescError As String = "OK"
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        ''...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            '  '+ ELIMINAMOS LOS ALIAS YA EXISTENTES
            SQLCommand = New SqlClient.SqlCommand("Rcp_Rel_ContraparteGpi_Asesor_Mantenedor", MiTransaccionSQL.Connection, MiTransaccionSQL)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Rel_ContraparteGpi_Asesor_Mantenedor"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = "ELIMINAR"
            SQLCommand.Parameters.Add("pIdEntidad", SqlDbType.Float, 10).Value = IIf(intId_Asesor = 0, DBNull.Value, intId_Asesor)
            SQLCommand.Parameters.Add("pValor", SqlDbType.VarChar, 50).Value = DBNull.Value
            SQLCommand.Parameters.Add("pIdTipoEntidadGPI", SqlDbType.VarChar, 50).Value = strTipoEndtidad
            SQLCommand.Parameters.Add("pIdContraparte", SqlDbType.VarChar, 50).Value = DBNull.Value
            SQLCommand.Parameters.Add("pDscContraparte", SqlDbType.VarChar, 50).Value = DBNull.Value




            '...Resultado
            Dim pSalElimIntPort1 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalElimIntPort1.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalElimIntPort1)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            If strDescError.ToUpper.Trim <> "OK" Then
                MiTransaccionSQL.Rollback()
                Exit Function
            End If

            '+ INSERTAMOS LOS NUEVOS ALIAS PUBLICADORES PARA LA ENTIDAD GPI 
            If Not (IsNothing(DS_AliasAsesor)) Then
                If DS_AliasAsesor.Tables(0).Rows.Count <> Nothing Then
                    For Each dtrRegristro As DataRow In DS_AliasAsesor.Tables(0).Rows
                        SQLCommand = New SqlClient.SqlCommand("Rcp_Rel_ContraparteGpi_Asesor_Mantenedor", MiTransaccionSQL.Connection, MiTransaccionSQL)
                        SQLCommand.CommandType = CommandType.StoredProcedure
                        SQLCommand.CommandText = "Rcp_Rel_ContraparteGpi_Asesor_Mantenedor"
                        SQLCommand.Parameters.Clear()

                        SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = "INSERTAR"
                        SQLCommand.Parameters.Add("pIdEntidad", SqlDbType.VarChar, 50).Value = intId_Asesor
                        SQLCommand.Parameters.Add("pValor", SqlDbType.VarChar, 50).Value = dtrRegristro("VALOR").ToString
                        SQLCommand.Parameters.Add("pIdContraparte", SqlDbType.VarChar, 50).Value = dtrRegristro("ID_ENTIDAD_CONTRAPARTE").ToString
                        SQLCommand.Parameters.Add("pIdTipoEntidadGPI", SqlDbType.VarChar, 50).Value = strTipoEndtidad
                        SQLCommand.Parameters.Add("pDscContraparte", SqlDbType.VarChar, 50).Value = dtrRegristro("DSC_CONTRAPARTE").ToString


                        '...Resultado
                        Dim pSalInstPort As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                        pSalInstPort.Direction = Data.ParameterDirection.Output
                        SQLCommand.Parameters.Add(pSalInstPort)

                        SQLCommand.ExecuteNonQuery()

                        strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
                        If strDescError.ToUpper.Trim <> "OK" Then
                            Exit For
                        End If
                    Next
                End If
                If strDescError.ToUpper.Trim = "OK" Then
                    MiTransaccionSQL.Commit()
                Else
                    MiTransaccionSQL.Rollback()
                End If
            End If
        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            RelContraparte_Asesor_Mantenedor = strDescError
        End Try
    End Function

    Public Function Rel_Entidad_Contraparte_Operacion_Ver(ByVal IdOperacion As String, _
                                                 ByVal strColumnas As String, _
                                                 ByRef strRetorno As String) As DataSet
        Dim IidOperacion As Integer = CInt(IdOperacion)
        Dim LDS_Rel_Entidad As New DataSet
        Dim LDS_Rel_Entidad_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Rel_ContraparteGpi_Ver_Operacion"
        Lstr_NombreTabla = "ENTIDADGPI"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdOperacion", SqlDbType.Int, 10).Value = IIf(IdOperacion = 0, DBNull.Value, IdOperacion)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Rel_Entidad, Lstr_NombreTabla)

            LDS_Rel_Entidad_Aux = LDS_Rel_Entidad

            If strColumnas.Trim = "" Then
                LDS_Rel_Entidad_Aux = LDS_Rel_Entidad
            Else
                LDS_Rel_Entidad_Aux = LDS_Rel_Entidad.Copy
                For Each Columna In LDS_Rel_Entidad.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Rel_Entidad_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Rel_Entidad.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            Return LDS_Rel_Entidad_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function RelContraparte_Operacion_Mantenedor(ByVal intId_Operacion As Integer, _
                                                  ByVal strTipoEndtidad As String, _
                                                  ByRef DS_AliasOperacion As DataSet) As String
        Dim strDescError As String = "OK"
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        ''...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            '  '+ ELIMINAMOS LOS ALIAS YA EXISTENTES
            SQLCommand = New SqlClient.SqlCommand("Rcp_Rel_ContraparteGpi_Operacion_Mantenedor", MiTransaccionSQL.Connection, MiTransaccionSQL)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Rel_ContraparteGpi_Operacion_Mantenedor"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = "ELIMINAR"
            SQLCommand.Parameters.Add("pIdEntidad", SqlDbType.Float, 10).Value = IIf(intId_Operacion = 0, DBNull.Value, intId_Operacion)
            SQLCommand.Parameters.Add("pValor", SqlDbType.VarChar, 50).Value = DBNull.Value
            SQLCommand.Parameters.Add("pIdTipoEntidadGPI", SqlDbType.VarChar, 50).Value = strTipoEndtidad
            SQLCommand.Parameters.Add("pIdContraparte", SqlDbType.VarChar, 50).Value = DBNull.Value
            SQLCommand.Parameters.Add("pDscContraparte", SqlDbType.VarChar, 50).Value = DBNull.Value


            '...Resultado
            Dim pSalElimIntPort1 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalElimIntPort1.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalElimIntPort1)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            If strDescError.ToUpper.Trim <> "OK" Then
                MiTransaccionSQL.Rollback()
                Exit Function
            End If

            '+ INSERTAMOS LOS NUEVOS ALIAS PUBLICADORES PARA LA ENTIDAD GPI 
            If Not (IsNothing(DS_AliasOperacion)) Then
                If DS_AliasOperacion.Tables(0).Rows.Count <> Nothing Then
                    For Each dtrRegristro As DataRow In DS_AliasOperacion.Tables(0).Rows
                        SQLCommand = New SqlClient.SqlCommand("Rcp_Rel_ContraparteGpi_Operacion_Mantenedor", MiTransaccionSQL.Connection, MiTransaccionSQL)
                        SQLCommand.CommandType = CommandType.StoredProcedure
                        SQLCommand.CommandText = "Rcp_Rel_ContraparteGpi_Operacion_Mantenedor"
                        SQLCommand.Parameters.Clear()

                        SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = "INSERTAR"
                        SQLCommand.Parameters.Add("pIdEntidad", SqlDbType.VarChar, 50).Value = intId_Operacion
                        SQLCommand.Parameters.Add("pValor", SqlDbType.VarChar, 50).Value = dtrRegristro("VALOR").ToString
                        SQLCommand.Parameters.Add("pIdContraparte", SqlDbType.VarChar, 50).Value = dtrRegristro("ID_ENTIDAD_CONTRAPARTE").ToString
                        SQLCommand.Parameters.Add("pIdTipoEntidadGPI", SqlDbType.VarChar, 50).Value = strTipoEndtidad
                        SQLCommand.Parameters.Add("pDscContraparte", SqlDbType.VarChar, 50).Value = dtrRegristro("DSC_CONTRAPARTE").ToString


                        '...Resultado
                        Dim pSalInstPort As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                        pSalInstPort.Direction = Data.ParameterDirection.Output
                        SQLCommand.Parameters.Add(pSalInstPort)

                        SQLCommand.ExecuteNonQuery()

                        strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
                        If strDescError.ToUpper.Trim <> "OK" Then
                            Exit For
                        End If
                    Next
                End If
                If strDescError.ToUpper.Trim = "OK" Then
                    MiTransaccionSQL.Commit()
                Else
                    MiTransaccionSQL.Rollback()
                End If
            End If
        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            RelContraparte_Operacion_Mantenedor = strDescError
        End Try
    End Function
End Class
