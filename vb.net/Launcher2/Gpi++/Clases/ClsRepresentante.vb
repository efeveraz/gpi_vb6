﻿Public Class ClsRepresentante
    Public Function Representante_Ver(ByVal lngIdRepresentante As Long, _
                                      ByVal lngIdCliente As Long, _
                                      ByVal strColumnas As String, _
                                      ByRef strRetorno As String) As DataSet

        Dim LDS_Representante As New DataSet
        Dim LDS_Representante_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = "Representante"
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = "Rcp_Representante_Consultar"

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdRepresentante", SqlDbType.Float, 10).Value = IIf(lngIdRepresentante = 0, DBNull.Value, lngIdRepresentante)
            SQLCommand.Parameters.Add("pIdCliente", SqlDbType.Float, 10).Value = IIf(lngIdCliente = 0, DBNull.Value, lngIdCliente)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Representante, Lstr_NombreTabla)


            LDS_Representante_Aux = LDS_Representante

            If strColumnas.Trim = "" Then
                LDS_Representante_Aux = LDS_Representante
            Else
                LDS_Representante_Aux = LDS_Representante.Copy
                For Each Columna In LDS_Representante.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Representante_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Representante_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_Representante.Dispose()
            Return LDS_Representante_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function
    Public Function Representante_Mantenedor(ByVal strId_Cliente As String, ByVal strTipo_Entidad As String, ByRef dstRepresentante As DataSet) As String
        Dim strDescError As String = "OK"
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try

            '       REALIZA LOS CAMBIOS DEL DATASET EN LA TABLA DE RepresentanteES

            If (dstRepresentante.Tables(0).Rows.Count > 0) Or (dstRepresentante.Tables(1).Rows.Count > 0) Then
                For Each ptabla As DataTable In dstRepresentante.Tables
                    If ptabla.Rows.Count > 0 Then
                        For Each pRow As DataRow In ptabla.Rows
                            If pRow("ESTADO").ToString <> "CONSERVAR" Then
                                SQLCommand = New SqlClient.SqlCommand("Rcp_Representante_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)
                                SQLCommand.CommandType = CommandType.StoredProcedure
                                SQLCommand.CommandText = "Rcp_Representante_Mantencion"
                                SQLCommand.Parameters.Clear()

                                SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = pRow("ESTADO").ToString
                                SQLCommand.Parameters.Add("pId_Cliente", SqlDbType.Float, 10).Value = CLng("0" & strId_Cliente)
                                SQLCommand.Parameters.Add("Ptipo_entidad", SqlDbType.VarChar, 1).Value = strTipo_Entidad
                                SQLCommand.Parameters.Add("pId_Representante", SqlDbType.Float, 10).Value = CLng("0" & pRow("ID_REPRESENTANTE").ToString)
                                SQLCommand.Parameters.Add("pRut_Representante", SqlDbType.VarChar, 10).Value = pRow("RUT_REPRESENTANTE").ToString
                                SQLCommand.Parameters.Add("pNombre", SqlDbType.VarChar, 150).Value = pRow("NOMBRE").ToString
                                SQLCommand.Parameters.Add("pFecha_Nacimiento", SqlDbType.VarChar, 10).Value = IIf(pRow("FECHA_NACIMIENTO").ToString = "", DBNull.Value, pRow("FECHA_NACIMIENTO").ToString)
                                SQLCommand.Parameters.Add("pCargo", SqlDbType.VarChar, 50).Value = pRow("CARGO").ToString
                                SQLCommand.Parameters.Add("pCelular", SqlDbType.VarChar, 50).Value = pRow("CELULAR").ToString
                                SQLCommand.Parameters.Add("pFono", SqlDbType.VarChar, 50).Value = pRow("FONO").ToString
                                SQLCommand.Parameters.Add("pEmail", SqlDbType.VarChar, 50).Value = pRow("EMAIL").ToString
                                SQLCommand.Parameters.Add("pRelacion_Cliente", SqlDbType.VarChar, 50).Value = pRow("RELACION_CLIENTE").ToString
                                SQLCommand.Parameters.Add("pObservaciones", SqlDbType.VarChar, 150).Value = pRow("OBSERVACIONES").ToString

                                '...Resultado
                                Dim pSalInstPort As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                                pSalInstPort.Direction = Data.ParameterDirection.Output
                                SQLCommand.Parameters.Add(pSalInstPort)

                                SQLCommand.ExecuteNonQuery()

                                strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
                                If Not strDescError.ToUpper.Trim = "OK" Then
                                    Exit For
                                End If
                            End If
                        Next
                    End If
                Next
            End If

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error al actualizar: " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            Representante_Mantenedor = strDescError
        End Try
    End Function
End Class
