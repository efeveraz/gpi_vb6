﻿Public Class ClsPropuestas
    Public Function Propuesta_Apo_Ret_Buscar( _
                        ByVal dblIdPropuestaApoRet As Double, _
                        ByVal lngIdCuenta As Double, _
                        ByVal dblIdMovCaja As Double, _
                        ByVal lngIdTipoEstado As Long, _
                        ByVal strCodEstado As String, _
                        ByVal strFechaDesde As String, _
                        ByVal strFechaHasta As String, _
                        ByVal dblIdUsuario As Double, _
                        ByVal strColumnas As String, _
                        ByRef strRetorno As String) As DataSet

        Dim LDS_PropuestaApoRet As New DataSet
        Dim LDS_PropuestaApoRet_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Propuesta_Apo_Ret_Consultar"
        Lstr_NombreTabla = "PROPUESTAAPORET"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdPropuestaApoRet", SqlDbType.Int).Value = IIf(dblIdPropuestaApoRet = 0, DBNull.Value, dblIdPropuestaApoRet)
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Int).Value = IIf(lngIdCuenta = 0, DBNull.Value, lngIdCuenta)
            SQLCommand.Parameters.Add("pIdMovCaja", SqlDbType.Int).Value = IIf(dblIdMovCaja = 0, DBNull.Value, dblIdMovCaja)
            SQLCommand.Parameters.Add("pIdTipoEstado", SqlDbType.Float, 10).Value = IIf(lngIdTipoEstado = 0, DBNull.Value, lngIdTipoEstado)
            SQLCommand.Parameters.Add("pCodEstado", SqlDbType.VarChar, 3).Value = IIf(strCodEstado = "", DBNull.Value, strCodEstado)
            SQLCommand.Parameters.Add("pFechaDesde", SqlDbType.Char, 10).Value = IIf(strFechaDesde = "", DBNull.Value, strFechaDesde)
            SQLCommand.Parameters.Add("pFechaHasta", SqlDbType.Char, 10).Value = IIf(strFechaHasta = "", DBNull.Value, strFechaHasta)
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Int, 10).Value = IIf(dblIdUsuario = 0, DBNull.Value, dblIdUsuario)
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Int, 10).Value = glngNegocioOperacion

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_PropuestaApoRet, Lstr_NombreTabla)

            LDS_PropuestaApoRet_Aux = LDS_PropuestaApoRet

            If strColumnas.Trim = "" Then
                LDS_PropuestaApoRet_Aux = LDS_PropuestaApoRet
            Else
                LDS_PropuestaApoRet_Aux = LDS_PropuestaApoRet.Copy
                For Each Columna In LDS_PropuestaApoRet.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_PropuestaApoRet_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_PropuestaApoRet_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_PropuestaApoRet.Dispose()
            Return LDS_PropuestaApoRet_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function Propuesta_Apo_Ret_Mantenedor(ByVal strAccion As String, _
                                                 ByVal strOperacion As String, _
                                                 ByVal dblIdPropuestaApoRet As Double, _
                                                 ByVal dblIdDetallePropuesta As Double, _
                                                 ByVal dblIdMovcaja As Double, _
                                                 ByVal dblIdCuenta As Double, _
                                                 ByVal dblIdMedioPagoCobro As Double, _
                                                 ByVal strCodMedioPagoCobro As String, _
                                                 ByVal dblIdTipoEstado As Double, _
                                                 ByVal strCodEstado As String, _
                                                 ByVal dblIdUsuario As Double, _
                                        Optional ByVal strFechaLiquidacion As String = vbNullString, _
                                        Optional ByVal strFechaOperacion As String = vbNullString, _
                                        Optional ByVal strFechaAprobacion As String = vbNullString, _
                                        Optional ByVal strIdTareaControl As String = vbNullString, _
                                        Optional ByVal strIdExcepcion As String = vbNullString, _
                                        Optional ByVal strObsDetalle As String = vbNullString, _
                                        Optional ByVal strIdRechazo As String = vbNullString) As String

        Dim strDescError As String = "OK"
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction
        Dim LstrProcedimiento As String = ""

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        LstrProcedimiento = "Rcp_Propuesta_Apo_Ret_Act"

        Try

            '       ACTUALIZA DATOS 
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, MiTransaccionSQL.Connection, MiTransaccionSQL)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("@pAccion", SqlDbType.Char, 10).Value = strAccion
            SQLCommand.Parameters.Add("@pOperacion", SqlDbType.Char, 10).Value = strOperacion
            SQLCommand.Parameters.Add("@pIdPropuestaApoRet", SqlDbType.Float, 10).Value = dblIdPropuestaApoRet
            SQLCommand.Parameters.Add("@pIdDetallePropuesta", SqlDbType.Float, 10).Value = dblIdDetallePropuesta
            SQLCommand.Parameters.Add("@pIdMovcaja", SqlDbType.Float, 10).Value = dblIdMovcaja
            SQLCommand.Parameters.Add("@pIdCuenta", SqlDbType.Float, 10).Value = dblIdCuenta
            SQLCommand.Parameters.Add("@pIdMedioPagoCobro", SqlDbType.Float, 10).Value = dblIdMedioPagoCobro
            SQLCommand.Parameters.Add("@pCodMedioPagoCobro", SqlDbType.Char, 20).Value = IIf(strCodMedioPagoCobro = "", DBNull.Value, strCodMedioPagoCobro)
            SQLCommand.Parameters.Add("@pIdTipoEstado", SqlDbType.Float, 10).Value = dblIdTipoEstado
            SQLCommand.Parameters.Add("@pCodEstado", SqlDbType.Char, 3).Value = strCodEstado
            SQLCommand.Parameters.Add("@pFechaLiquidacion", SqlDbType.Char, 10).Value = strFechaLiquidacion
            SQLCommand.Parameters.Add("@pFechaOperacion", SqlDbType.Char, 10).Value = strFechaOperacion
            SQLCommand.Parameters.Add("@pFechaAprobacion", SqlDbType.Char, 10).Value = strFechaAprobacion
            SQLCommand.Parameters.Add("@pIdUsuario", SqlDbType.Float, 10).Value = dblIdUsuario
            SQLCommand.Parameters.Add("@pIdTareaControl", SqlDbType.Char, 10).Value = IIf(strIdTareaControl = "", DBNull.Value, strIdTareaControl)
            SQLCommand.Parameters.Add("@pIdExcepcion", SqlDbType.Char, 10).Value = IIf(strIdExcepcion = "", DBNull.Value, strIdExcepcion)
            SQLCommand.Parameters.Add("@pObsDetalle", SqlDbType.Char, 500).Value = IIf(strObsDetalle = "", DBNull.Value, strObsDetalle)
            SQLCommand.Parameters.Add("@pIdRechazo", SqlDbType.Char, 10).Value = IIf(strIdRechazo = "", DBNull.Value, strIdRechazo)

            '...Resultado
            Dim ParametroSal1 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal1.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal1)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error al Grabar Propuesta " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
        End Try

        Propuesta_Apo_Ret_Mantenedor = strDescError

    End Function

    Function ActPropuestaSel(ByVal strAccion As String, _
                             ByVal strOperacion As String, _
                             ByVal DS_PropuestaSel As DataSet, _
                             ByVal dblIdMedioPagoCobro As Double, _
                             ByVal strCodMedioPagoCobro As String, _
                             ByVal dblIdUsuario As Double, _
                             ByVal strFechaAprobacion As String, _
                             ByVal strIdExcepcion As String, _
                             ByVal strObsDetalle As String, _
                             ByVal strIdRechazo As String, _
                             ByRef strResultado As String) As String

        Dim lstrRegistro As String = ""
        Dim lstrstringAux As String = ""
        Dim lstrDetalle As String = ""
        'Dim strDescError As String = ""
        'Dim lstrstring As String = ""
        'Dim strResultado As String = ""
        'Dim lstrColumnasDetalle As String = "ID_PROPUESTA_APO_RET"
        'Dim lstrResultadoCadena As String = ""

        For Each dtrPropuestaSel As DataRow In DS_PropuestaSel.Tables(0).Rows
            lstrRegistro = dtrPropuestaSel("ID_PROPUESTA_APO_RET").ToString + "©"
            lstrstringAux = lstrstringAux + lstrRegistro
            If lstrstringAux.Length < 8000 Then
                lstrDetalle = lstrstringAux
            Else
                strResultado = ("Error: La Seleccion es demasiado amplia")
                If strResultado = "OK" Then
                    lstrstringAux = lstrRegistro
                Else
                    strResultado = ("Error: La Seleccion es demasiado amplia")
                    ActPropuestaSel = strResultado
                    Exit Function
                End If

            End If
        Next
        If Len(lstrDetalle) > 0 Then
            ActPropuestaSel = ActPropuestaSel_Mant(strAccion, strOperacion, lstrDetalle, _
                                                   dblIdMedioPagoCobro, strCodMedioPagoCobro, _
                                                   dblIdUsuario, strFechaAprobacion, _
                                                   strIdExcepcion, strObsDetalle, _
                                                   strIdRechazo, strResultado)
            If ActPropuestaSel <> "OK" Then
                strResultado = "Error al Actualizar Propuestas:" + ActPropuestaSel
                'MsgBox(strResultado)
                Exit Function
            Else
                ActPropuestaSel = ActPropuestaSel
            End If
        Else
            ActPropuestaSel = vbNullString
        End If

    End Function

    Public Function ActPropuestaSel_Mant(ByVal strAccion As String, _
                                         ByVal strOperacion As String, _
                                         ByVal lstrRegistro As String, _
                                         ByVal dblIdMedioPagoCobro As Double, _
                                         ByVal strCodMedioPagoCobro As String, _
                                         ByVal dblIdUsuario As Double, _
                                         ByVal strFechaAprobacion As String, _
                                         ByVal strIdExcepcion As String, _
                                         ByVal strObsDetalle As String, _
                                         ByVal strIdRechazo As String, _
                                         ByRef strResultado As String) As String

        Dim strDescError As String = "OK"
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction
        Dim LstrProcedimiento As String = ""

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        LstrProcedimiento = "Rcp_Propuesta_Apo_Ret_ActSel"

        Try

            '       ACTUALIZA DATOS 
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, MiTransaccionSQL.Connection, MiTransaccionSQL)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = strAccion
            SQLCommand.Parameters.Add("pOperacion", SqlDbType.Char, 10).Value = strOperacion
            SQLCommand.Parameters.Add("pstrRegistro", SqlDbType.VarChar, 8000).Value = lstrRegistro
            SQLCommand.Parameters.Add("pIdMedioPagoCobro", SqlDbType.Float, 10).Value = dblIdMedioPagoCobro
            SQLCommand.Parameters.Add("pCodMedioPagoCobro", SqlDbType.Char, 20).Value = IIf(strCodMedioPagoCobro = "", DBNull.Value, strCodMedioPagoCobro)
            SQLCommand.Parameters.Add("pFechaAprobacion", SqlDbType.Char, 10).Value = strFechaAprobacion
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Float, 10).Value = dblIdUsuario
            SQLCommand.Parameters.Add("pIdExcepcion", SqlDbType.Char, 10).Value = IIf(strIdExcepcion = "", DBNull.Value, strIdExcepcion)
            SQLCommand.Parameters.Add("pObsDetalle", SqlDbType.VarChar, 500).Value = IIf(strObsDetalle = "", DBNull.Value, strObsDetalle)
            SQLCommand.Parameters.Add("pIdRechazo", SqlDbType.Char, 10).Value = IIf(strIdRechazo = "", DBNull.Value, strIdRechazo)

            '...Resultado
            Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultado.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error al Actualizar Propuesta " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
        End Try

        ActPropuestaSel_Mant = strDescError

    End Function

    Public Function Detalle_Propuesta_Buscar( _
                        ByVal dblIdPropuestaApoRet As Double, _
                        ByVal dblIdDetallePropuesta As Double, _
                        ByVal dblIdUsuario As Double, _
                        ByVal strColumnas As String, _
                        ByRef strRetorno As String) As DataSet

        Dim LDS_DetallePropuesta As New DataSet
        Dim LDS_DetallePropuesta_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Detalle_Propuesta_Consultar"
        Lstr_NombreTabla = "DetallePropuesta"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdPropuestaApoRet", SqlDbType.Int).Value = dblIdPropuestaApoRet
            SQLCommand.Parameters.Add("pIdDetallePropuesta", SqlDbType.Int).Value = IIf(dblIdDetallePropuesta = 0, DBNull.Value, dblIdDetallePropuesta)
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Int).Value = IIf(dblIdUsuario = 0, DBNull.Value, dblIdUsuario)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_DetallePropuesta, Lstr_NombreTabla)

            LDS_DetallePropuesta_Aux = LDS_DetallePropuesta

            If strColumnas.Trim = "" Then
                LDS_DetallePropuesta_Aux = LDS_DetallePropuesta
            Else
                LDS_DetallePropuesta_Aux = LDS_DetallePropuesta.Copy
                For Each Columna In LDS_DetallePropuesta.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_DetallePropuesta_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_DetallePropuesta_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_DetallePropuesta.Dispose()
            Return LDS_DetallePropuesta_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    'Public Function BuscaExcepciones(ByRef strRetorno As String, _
    '                                 ByVal strColumnas As String, _
    '                                 ByVal dblIdExcepcion As Double, _
    '                                 ByVal strDscExcepcion As String) As DataSet

    '    Dim lblnRemove As Boolean = True
    '    Dim larr_NombreColumna() As String = Split(strColumnas, ",")
    '    Dim lInt_Col As Integer = 0
    '    Dim lInt_NomCol As String = ""

    '    Dim DS_Excepcion As New DataSet
    '    Dim DS_Excepcion_Aux As New DataSet

    '    Dim Sqlcommand As New SqlClient.SqlCommand
    '    Dim SqlDataAdapter As New SqlClient.SqlDataAdapter
    '    Dim Connect As Cls_Conexion = New Cls_Conexion
    '    Dim LstrProcedimiento As String = ""
    '    Dim LstrNombreTabla As String = ""

    '    LstrProcedimiento = "Rcp_Excepciones_Busca"
    '    LstrNombreTabla = "EXCEPCIONES"

    '    'Abre la conexion 
    '    Connect.Abrir()

    '    Try
    '        Sqlcommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

    '        Sqlcommand.CommandType = CommandType.StoredProcedure
    '        Sqlcommand.CommandText = LstrProcedimiento
    '        Sqlcommand.Parameters.Clear()

    '        'If dblIdExcepcion <> 0 Then
    '        Sqlcommand.Parameters.Add("@pIdExcepcion", SqlDbType.Int).Value = IIf(dblIdExcepcion = 0, DBNull.Value, dblIdExcepcion)
    '        'End If

    '        'If strDscExcepcion <> "" Then
    '        Sqlcommand.Parameters.Add("@pDscExcepcion", SqlDbType.VarChar, 100).Value = IIf(strDscExcepcion = "", DBNull.Value, strDscExcepcion.Trim)
    '        'End If

    '        Dim oParamSal As New SqlClient.SqlParameter("@pMsjRetorno", SqlDbType.Char, 60)
    '        oParamSal.Direction = ParameterDirection.Output
    '        Sqlcommand.Parameters.Add(oParamSal)

    '        SqlDataAdapter.SelectCommand = Sqlcommand
    '        SqlDataAdapter.Fill(DS_Excepcion, LstrNombreTabla)

    '        If strColumnas.Trim = "" Then
    '            DS_Excepcion_Aux = DS_Excepcion
    '        Else
    '            DS_Excepcion_Aux = DS_Excepcion.Copy
    '            For Each lstrColumna As DataColumn In DS_Excepcion.Tables(LstrNombreTabla).Columns
    '                lblnRemove = True
    '                For lInt_Col = 0 To larr_NombreColumna.Length - 1
    '                    lInt_NomCol = larr_NombreColumna(lInt_Col).TrimEnd.TrimStart
    '                    If lstrColumna.ColumnName = lInt_NomCol Then
    '                        lblnRemove = False
    '                    End If
    '                Next
    '                If lblnRemove Then
    '                    DS_Excepcion_Aux.Tables(LstrNombreTabla).Columns.Remove(lstrColumna.ColumnName)
    '                End If
    '            Next
    '        End If

    '        For lInt_Col = 0 To larr_NombreColumna.Length - 1
    '            lInt_NomCol = larr_NombreColumna(lInt_Col).TrimEnd.TrimStart
    '            For Each lstrColumna As DataColumn In DS_Excepcion_Aux.Tables(LstrNombreTabla).Columns
    '                If lstrColumna.ColumnName = lInt_NomCol Then
    '                    lstrColumna.SetOrdinal(lInt_Col)
    '                    Exit For
    '                End If
    '            Next
    '        Next

    '        strRetorno = Sqlcommand.Parameters("@pMsjRetorno").Value.ToString.Trim

    '        Return DS_Excepcion_Aux
    '    Catch ex As Exception
    '        strRetorno = ex.Message
    '        Return Nothing
    '    Finally
    '        Connect.Cerrar()
    '    End Try
    'End Function

    'Public Function BuscaRechazo(ByRef strRetorno As String, _
    '                             ByVal strColumnas As String, _
    '                             ByVal dblIdRechazo As Double, _
    '                             ByVal strDscRechazo As String) As DataSet

    '    Dim lblnRemove As Boolean = True
    '    Dim larr_NombreColumna() As String = Split(strColumnas, ",")
    '    Dim lInt_Col As Integer = 0
    '    Dim lInt_NomCol As String = ""

    '    Dim DS_Rechazo As New DataSet
    '    Dim DS_Rechazo_Aux As New DataSet

    '    Dim Sqlcommand As New SqlClient.SqlCommand
    '    Dim SqlDataAdapter As New SqlClient.SqlDataAdapter
    '    Dim Connect As Cls_Conexion = New Cls_Conexion
    '    Dim LstrProcedimiento As String = ""
    '    Dim LstrNombreTabla As String = ""

    '    LstrProcedimiento = "Rcp_Rechazo_Busca"
    '    LstrNombreTabla = "RECHAZO"

    '    'Abre la conexion 
    '    Connect.Abrir()

    '    Try
    '        Sqlcommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

    '        Sqlcommand.CommandType = CommandType.StoredProcedure
    '        Sqlcommand.CommandText = LstrProcedimiento
    '        Sqlcommand.Parameters.Clear()

    '        'If dblIdExcepcion <> 0 Then
    '        Sqlcommand.Parameters.Add("@pIdRechazo", SqlDbType.Int).Value = IIf(dblIdRechazo = 0, DBNull.Value, dblIdRechazo)
    '        'End If

    '        'If strDscExcepcion <> "" Then
    '        Sqlcommand.Parameters.Add("@pDscRechazo", SqlDbType.VarChar, 100).Value = IIf(strDscRechazo = "", DBNull.Value, strDscRechazo.Trim)
    '        'End If

    '        Dim oParamSal As New SqlClient.SqlParameter("@pMsjRetorno", SqlDbType.Char, 60)
    '        oParamSal.Direction = ParameterDirection.Output
    '        Sqlcommand.Parameters.Add(oParamSal)

    '        SqlDataAdapter.SelectCommand = Sqlcommand
    '        SqlDataAdapter.Fill(DS_Rechazo, LstrNombreTabla)

    '        If strColumnas.Trim = "" Then
    '            DS_Rechazo_Aux = DS_Rechazo
    '        Else
    '            DS_Rechazo_Aux = DS_Rechazo.Copy
    '            For Each lstrColumna As DataColumn In DS_Rechazo.Tables(LstrNombreTabla).Columns
    '                lblnRemove = True
    '                For lInt_Col = 0 To larr_NombreColumna.Length - 1
    '                    lInt_NomCol = larr_NombreColumna(lInt_Col).TrimEnd.TrimStart
    '                    If lstrColumna.ColumnName = lInt_NomCol Then
    '                        lblnRemove = False
    '                    End If
    '                Next
    '                If lblnRemove Then
    '                    DS_Rechazo_Aux.Tables(LstrNombreTabla).Columns.Remove(lstrColumna.ColumnName)
    '                End If
    '            Next
    '        End If

    '        For lInt_Col = 0 To larr_NombreColumna.Length - 1
    '            lInt_NomCol = larr_NombreColumna(lInt_Col).TrimEnd.TrimStart
    '            For Each lstrColumna As DataColumn In DS_Rechazo_Aux.Tables(LstrNombreTabla).Columns
    '                If lstrColumna.ColumnName = lInt_NomCol Then
    '                    lstrColumna.SetOrdinal(lInt_Col)
    '                    Exit For
    '                End If
    '            Next
    '        Next

    '        strRetorno = Sqlcommand.Parameters("@pMsjRetorno").Value.ToString.Trim

    '        Return DS_Rechazo_Aux
    '    Catch ex As Exception
    '        strRetorno = ex.Message
    '        Return Nothing
    '    Finally
    '        Connect.Cerrar()
    '    End Try
    'End Function
End Class
