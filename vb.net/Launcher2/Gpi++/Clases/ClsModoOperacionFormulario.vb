Public Class ClsModoOperacionFormulario

    Dim Edicion As Boolean = False
    Dim Consulta As Boolean = False

    Public Property _EDICION() As Boolean
        Get
            Return Edicion
        End Get

        Set(ByVal value As Boolean)
            Edicion = value
        End Set
    End Property

    Public Property _CONSULTA() As Boolean
        Get
            Return Consulta
        End Get

        Set(ByVal value As Boolean)
            Consulta = value
        End Set
    End Property

End Class
