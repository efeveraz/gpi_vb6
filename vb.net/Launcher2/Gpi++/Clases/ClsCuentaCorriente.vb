﻿Public Class ClsCuentaCorriente
    Public Function CuentaCorrienteCliente_Ver(ByVal strIdCuentaCorriente As String, _
                                        ByVal strIdCliente As String, _
                                        ByVal strColumnas As String, _
                                        ByRef strRetorno As String) As DataSet

        Dim LDS_CuentaCorrienteCliente As New DataSet
        Dim LDS_CuentaCorrienteCliente_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_CuentaCorrienteCliente_Consultar"
        Lstr_NombreTabla = "Originales"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdCuentaCorriente", SqlDbType.VarChar, 9).Value = IIf(strIdCuentaCorriente.Trim = "", DBNull.Value, strIdCuentaCorriente.Trim)
            SQLCommand.Parameters.Add("pIdCliente", SqlDbType.VarChar, 9).Value = IIf(strIdCliente.Trim = "", DBNull.Value, strIdCliente.Trim)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_CuentaCorrienteCliente, Lstr_NombreTabla)

            LDS_CuentaCorrienteCliente_Aux = LDS_CuentaCorrienteCliente

            If strColumnas.Trim = "" Then
                LDS_CuentaCorrienteCliente_Aux = LDS_CuentaCorrienteCliente
            Else
                LDS_CuentaCorrienteCliente_Aux = LDS_CuentaCorrienteCliente.Copy
                For Each Columna In LDS_CuentaCorrienteCliente.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_CuentaCorrienteCliente_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_CuentaCorrienteCliente_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_CuentaCorrienteCliente.Dispose()
            Return LDS_CuentaCorrienteCliente_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function CuentaCorrienteCliente_Mantenedor(ByVal strId_Cliente As String, ByRef dstCuentasCorrientes As DataSet) As String

        Dim strDescError As String = "OK"
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try


            '       REALIZA LOS CAMBIOS DEL DATASET EN LA TABLA DE CUENTAS-CORRIENTES

            If (dstCuentasCorrientes.Tables(0).Rows.Count > 0) Or (dstCuentasCorrientes.Tables(1).Rows.Count > 0) Then
                For Each ptabla As DataTable In dstCuentasCorrientes.Tables
                    If ptabla.Rows.Count > 0 Then
                        For Each pRow As DataRow In ptabla.Rows
                            If pRow("ESTADO_MANTENCION").ToString <> "CONSERVAR" Then
                                SQLCommand = New SqlClient.SqlCommand("Rcp_CuentaCorriente_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)
                                SQLCommand.CommandType = CommandType.StoredProcedure
                                SQLCommand.CommandText = "Rcp_CuentaCorriente_Mantencion"
                                SQLCommand.Parameters.Clear()

                                '@pAccion varchar(10), 
                                '@pIdCtaCteCliente numeric(10, 0), 
                                '@pIdCliente numeric(10, 0), 
                                '@pNumeroCtaCte varchar(20), 
                                '@pIdBanco numeric (5, 0), 
                                '@pCodMoneda varchar (3), 
                                '@pSucursal varchar(120), 
                                '@pEjecutivo varchar(120), 
                                '@pFono varchar(50),
                                '@pFecha_Apertura varchar(10),
                                '@pFecha_Cierre varchar(10), 
                                '@pCodEstado varchar (3), 
                                '@pResultado varchar(200) OUTPUT)  

                                'ID_CTA_CTE,ID_CLIENTE,NUM_CTA_CTE, ID_BANCO, DSC_BANCO, COD_MONEDA, DSC_MONEDA, SUCURSAL, EJECUTIVO, FONO, 
                                'FECHA_APERTURA, FECHA_CIERRE, COD_ESTADO, ESTADO_CTACTE, ESTADO_MANTENCION

                                SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = pRow("ESTADO_MANTENCION").ToString
                                SQLCommand.Parameters.Add("pIdCtaCteCliente", SqlDbType.Float, 10).Value = pRow("ID_CTA_CTE").ToString
                                SQLCommand.Parameters.Add("pIdCliente", SqlDbType.Float, 10).Value = CLng("0" & strId_Cliente)
                                SQLCommand.Parameters.Add("pNumeroCtaCte", SqlDbType.VarChar, 20).Value = pRow("NUM_CTA_CTE").ToString
                                SQLCommand.Parameters.Add("pIdBanco", SqlDbType.Float, 5).Value = pRow("ID_BANCO").ToString
                                SQLCommand.Parameters.Add("pCodMoneda", SqlDbType.VarChar, 3).Value = pRow("COD_MONEDA").ToString
                                SQLCommand.Parameters.Add("pSucursal", SqlDbType.VarChar, 120).Value = pRow("SUCURSAL").ToString
                                SQLCommand.Parameters.Add("pEjecutivo", SqlDbType.VarChar, 120).Value = pRow("EJECUTIVO").ToString
                                SQLCommand.Parameters.Add("pFono", SqlDbType.VarChar, 50).Value = pRow("FONO").ToString
                                SQLCommand.Parameters.Add("pFecha_Apertura", SqlDbType.VarChar, 10).Value = pRow("FECHA_APERTURA").ToString
                                SQLCommand.Parameters.Add("pFecha_Cierre", SqlDbType.VarChar, 10).Value = pRow("FECHA_CIERRE").ToString
                                SQLCommand.Parameters.Add("pCodEstado", SqlDbType.VarChar, 3).Value = pRow("COD_ESTADO").ToString

                                '...Resultado
                                Dim pSalInstPort As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                                pSalInstPort.Direction = Data.ParameterDirection.Output
                                SQLCommand.Parameters.Add(pSalInstPort)

                                SQLCommand.ExecuteNonQuery()

                                strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
                                If Not strDescError.ToUpper.Trim = "OK" Then
                                    Exit For
                                End If

                            End If
                        Next
                    End If
                Next
            End If

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            CuentaCorrienteCliente_Mantenedor = strDescError
        End Try
    End Function

End Class
