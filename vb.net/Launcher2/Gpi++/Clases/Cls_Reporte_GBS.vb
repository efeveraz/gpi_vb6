﻿Public Class Cls_Reporte_GBS
    Public Function Reporte_RPT013(ByVal strFecha As String, _
                                   ByVal strUMS As String, _
                                   ByVal strLista As String, _
                                   ByVal strTipoLista As String, _
                                   ByVal intIdEjecutivo As Integer, _
                                   ByVal intIdNegocio As Integer, _
                                   ByRef strRetorno As String, _
                                   Optional ByVal strTipoCuentaDescCorta As String = "") As DataSet



        Dim lds_Reportes As New DataSet
        Dim lds_Reportes_Aux As New DataSet
        Dim Lstr_NombreTabla As String
        Dim LstrProcedimiento As String
        'Dim Columna As DataColumn

        Dim strDescError As String = ""
        'Dim SQLCommand As New SqlClient.SqlCommand
        ''Dim MiTransaccionSQL As SqlClient.SqlTransaction
        'Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        'Dim SQLConnect As ClsConexionConc = New ClsConexionConc
        ''Dim SQLConnect As Cls_Conexion = New Cls_Conexion


        Dim Sqlcommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Informe_Cartera_Inversiones_Resumido"
        Lstr_NombreTabla = "A"

        Connect.Abrir()

        Try
            Sqlcommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            Sqlcommand.CommandType = CommandType.StoredProcedure
            Sqlcommand.CommandText = LstrProcedimiento
            Sqlcommand.CommandTimeout = 0

            Sqlcommand.Parameters.Clear()

            Sqlcommand.Parameters.Add("@pTipoLista", SqlDbType.VarChar, 10).Value = strTipoLista
            Sqlcommand.Parameters.Add("@pLista", SqlDbType.Text).Value = strLista
            Sqlcommand.Parameters.Add("@pCod_Moneda_UMS", SqlDbType.VarChar, 50).Value = IIf(strUMS = "", DBNull.Value, strUMS)
            Sqlcommand.Parameters.Add("@pFechaProceso", SqlDbType.VarChar, 10).Value = Replace(strFecha, "-", "/")
            Sqlcommand.Parameters.Add("pIdNegocio", SqlDbType.Decimal, 10).Value = IIf(intIdNegocio = 0, DBNull.Value, intIdNegocio)
            Sqlcommand.Parameters.Add("pIdEjecutivo", SqlDbType.Decimal, 10).Value = IIf(intIdEjecutivo = 0, DBNull.Value, intIdEjecutivo)
            Sqlcommand.Parameters.Add("@pIdUsuario", SqlDbType.Decimal, 10).Value = IIf(glngIdUsuario = 0, DBNull.Value, glngIdUsuario)
            Sqlcommand.Parameters.Add("@pTipoCuentaDescCorta", SqlDbType.VarChar, 10).Value = strTipoCuentaDescCorta

            Dim ParametroSal As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal.Direction = Data.ParameterDirection.Output
            Sqlcommand.Parameters.Add(ParametroSal)

            SQLDataAdapter.SelectCommand = Sqlcommand
            SQLDataAdapter.Fill(lds_Reportes, Lstr_NombreTabla)

            strDescError = Sqlcommand.Parameters("pResultado").Value.ToString.Trim

            lds_Reportes_Aux = lds_Reportes


            strRetorno = strDescError
            Return lds_Reportes_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function
    Public Function Reporte_RPT046(ByVal strFecha As String, _
                                   ByVal strLista As String, _
                                   ByVal strTipoLista As String, _
                                   ByVal intIdEjecutivo As Integer, _
                                   ByVal intIdNegocio As Integer, _
                                   ByRef strRetorno As String) As DataSet

        Dim lds_Reportes As New DataSet
        Dim lds_Reportes_Aux As New DataSet
        Dim Lstr_NombreTabla As String = "CUOTAS"
        Dim LstrProcedimiento As String
        Dim strDescError As String = ""
        Dim Sqlcommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Informe_Consulta_Valores_Cuotas"

        Connect.Abrir()

        Try
            Sqlcommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            Sqlcommand.CommandType = CommandType.StoredProcedure
            Sqlcommand.CommandText = LstrProcedimiento
            Sqlcommand.CommandTimeout = 0

            Sqlcommand.Parameters.Clear()

            Sqlcommand.Parameters.Add("@pTipoLista", SqlDbType.VarChar, 10).Value = strTipoLista
            Sqlcommand.Parameters.Add("@pLista", SqlDbType.Text).Value = strLista
            Sqlcommand.Parameters.Add("@pFechaProceso", SqlDbType.VarChar, 10).Value = Replace(strFecha, "-", "/")
            Sqlcommand.Parameters.Add("pIdNegocio", SqlDbType.Decimal, 10).Value = IIf(intIdNegocio = 0, DBNull.Value, intIdNegocio)
            Sqlcommand.Parameters.Add("pIdEjecutivo", SqlDbType.Decimal, 10).Value = IIf(intIdEjecutivo = 0, DBNull.Value, intIdEjecutivo)
            Sqlcommand.Parameters.Add("@pIdUsuario", SqlDbType.Decimal, 10).Value = IIf(glngIdUsuario = 0, DBNull.Value, glngIdUsuario)

            Dim ParametroSal As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal.Direction = Data.ParameterDirection.Output
            Sqlcommand.Parameters.Add(ParametroSal)

            SQLDataAdapter.SelectCommand = Sqlcommand
            SQLDataAdapter.Fill(lds_Reportes, Lstr_NombreTabla)

            strDescError = Sqlcommand.Parameters("pResultado").Value.ToString.Trim

            lds_Reportes_Aux = lds_Reportes


            strRetorno = strDescError
            Return lds_Reportes_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function


End Class
