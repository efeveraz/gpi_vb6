﻿Public Class ClsGrupoEconomico

    Public Function GrupoEconomico_Ver(ByVal strCodigo As String, _
                                       ByVal strColumnas As String, _
                                       ByVal strEstado As String, _
                                       ByRef strRetorno As String) As DataSet

        Dim LDS_GrupoEconomico As New DataSet
        Dim LDS_GrupoEconomico_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_GrupoEconomico_Consultar"
        Lstr_NombreTabla = "GRUPO_ECONOMICO"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("pIdGrupoEconomico", SqlDbType.VarChar, 5).Value = IIf(strCodigo.Trim = "", DBNull.Value, strCodigo.Trim)
            SQLCommand.Parameters.Add("pEstado", SqlDbType.VarChar, 3).Value = IIf(strEstado.Trim = "", DBNull.Value, strEstado.Trim)
            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_GrupoEconomico, Lstr_NombreTabla)

            LDS_GrupoEconomico_Aux = LDS_GrupoEconomico

            If strColumnas.Trim = "" Then
                LDS_GrupoEconomico_Aux = LDS_GrupoEconomico
            Else
                LDS_GrupoEconomico_Aux = LDS_GrupoEconomico.Copy
                For Each Columna In LDS_GrupoEconomico.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_GrupoEconomico_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_GrupoEconomico_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_GrupoEconomico.Dispose()
            Return LDS_GrupoEconomico_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function GrupoEconomico_Mantenedor(ByVal strAccion As String, _
                                              ByVal strDescripcion As String, _
                                              ByVal intIdGrupoEconomico As Integer, _
                                              ByRef strEstado As String) As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_GrupoEconomico_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_GrupoEconomico_Mantencion"
            SQLCommand.Parameters.Clear()

            ' @pAccion varchar(10),
            ' @pDscGrupoEconomico varchar(60),
            ' @pIdGrupoEconomico inT output , 
            ' @pResultado varchar(200) OUTPUT)

            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 10).Value = strAccion
            SQLCommand.Parameters.Add("pDscGrupoEconomico", SqlDbType.VarChar, 60).Value = strDescripcion
            SQLCommand.Parameters.Add("pIdGrupoEconomico", SqlDbType.Float, 3).Value = intIdGrupoEconomico
            SQLCommand.Parameters.Add("pEstado", SqlDbType.VarChar, 3).Value = strEstado

            '...Resultado
            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            GrupoEconomico_Mantenedor = strDescError
        End Try
    End Function
End Class

