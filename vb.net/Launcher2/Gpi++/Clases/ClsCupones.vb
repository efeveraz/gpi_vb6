﻿Public Class ClsCupones
    '************************************
    '   Metodo          :   Grb_CM_Cupones
    '   Fecha Creacion  :   27-08-2012
    '   Por             :   JLP.
    '************************************
    Enum accion
        INSERTAR = 1
        MODIFICAR = 2
        ELIMINAR = 3
    End Enum
#Region "Propiedades cupones"

    Private _idcupones As Integer
    Private _codserie As String
    Private _nrocupon As Integer
    Private _fechacupon As String
    Private _interes As Decimal
    Private _capital As Decimal
    Private _flujo As Decimal
    Private _saldo As Decimal
    Private _idserie As Long

    Public Property idCupones() As Integer
        Get
            Return _idcupones
        End Get
        Set(ByVal value As Integer)
            _idcupones = value
        End Set
    End Property

    Public Property CodSerie() As String
        Get
            Return _codserie
        End Get
        Set(ByVal value As String)
            _codserie = value
        End Set
    End Property

    Public Property Nrocupon() As String
        Get
            Return _nrocupon
        End Get
        Set(ByVal value As String)
            _nrocupon = value
        End Set
    End Property

    Public Property FechaCupon() As String
        Get
            Return _fechacupon
        End Get
        Set(ByVal value As String)
            _fechacupon = value
        End Set
    End Property

    Public Property Interes() As Decimal
        Get
            Return _interes
        End Get
        Set(ByVal value As Decimal)
            _interes = value
        End Set
    End Property

    Public Property Capital() As Decimal
        Get
            Return _capital
        End Get
        Set(ByVal value As Decimal)
            _capital = value
        End Set
    End Property

    Public Property Flujo() As Decimal
        Get
            Return _flujo
        End Get
        Set(ByVal value As Decimal)
            _flujo = value
        End Set
    End Property

    Public Property Saldo() As Decimal
        Get
            Return _saldo
        End Get
        Set(ByVal value As Decimal)
            _saldo = value
        End Set
    End Property

    Public Property idSerie() As Integer
        Get
            Return _idserie
        End Get
        Set(ByVal value As Integer)
            _idserie = value
        End Set
    End Property

#End Region

#Region "Metodos"

    Public Function GuardarSerieCupones(ByVal paccion As accion, ByVal oclas As ClsCupones, ByRef strDescError As String) As Boolean

        Dim lresult As Boolean = False
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction
        Dim NombreSP As String = "Rcp_CM_grb_cupones"

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try

            SQLCommand = New SqlClient.SqlCommand(NombreSP, MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = NombreSP.ToString
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 9).Value = [Enum].GetName(GetType(accion), paccion.GetHashCode)
            'SQLCommand.Parameters.Add("idcupones", SqlDbType.Float, 9).Value = oclas.idCupones
            SQLCommand.Parameters.Add("codserie", SqlDbType.Char, 50).Value = oclas.CodSerie
            SQLCommand.Parameters.Add("nrocupon", SqlDbType.Float, 5).Value = oclas.Nrocupon
            SQLCommand.Parameters.Add("fechacupon", SqlDbType.VarChar, 10).Value = oclas.FechaCupon
            SQLCommand.Parameters.Add("interes", SqlDbType.Float, 11).Value = oclas.Interes
            SQLCommand.Parameters.Add("capital", SqlDbType.Float, 11).Value = oclas.Capital
            SQLCommand.Parameters.Add("flujo", SqlDbType.Float, 11).Value = oclas.Flujo
            SQLCommand.Parameters.Add("saldo", SqlDbType.Float, 11).Value = oclas.Saldo
            SQLCommand.Parameters.Add("idserie", SqlDbType.Decimal, 9).Value = oclas.idSerie

            ''...Resultado
            Dim ParametroSal As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError = "OK" Then
                lresult = True
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
        Finally
            SQLConnect.Cerrar()
        End Try
        Return lresult

    End Function

#End Region
End Class
