﻿Public Class ClsAlias
    Public Function Alias_Ver(ByVal strIdEntidad As String, _
                              ByVal strIdEntidadGPI As String, _
                              ByVal strIdEntidadContraparte As String, _
                              ByVal strColumnas As String, _
                              ByRef strRetorno As String) As DataSet

        Dim LDS_Alias As New DataSet
        Dim LDS_Alias_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Alias_Consultar"
        Lstr_NombreTabla = "Alias"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdEntidad", SqlDbType.VarChar, 50).Value = IIf(strIdEntidad.Trim = "", DBNull.Value, strIdEntidad.Trim)
            SQLCommand.Parameters.Add("pIdEntidadGPI", SqlDbType.VarChar, 9).Value = IIf(strIdEntidadGPI.Trim = "", DBNull.Value, strIdEntidadGPI.Trim)
            SQLCommand.Parameters.Add("pIdEntidadContraparte", SqlDbType.Float, 9).Value = IIf(strIdEntidadContraparte.Trim = "", DBNull.Value, strIdEntidadContraparte.Trim)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Alias, Lstr_NombreTabla)

            LDS_Alias_Aux = LDS_Alias

            If strColumnas.Trim = "" Then
                LDS_Alias_Aux = LDS_Alias
            Else
                LDS_Alias_Aux = LDS_Alias.Copy
                For Each Columna In LDS_Alias.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Alias_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Alias_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_Alias.Dispose()
            Return LDS_Alias_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function Alias_Mantenedor(ByVal strId_Entidad As String, _
                                     ByVal intTipoEndtidad As Integer, _
                                     ByRef dstAlias As DataSet) As String

        Dim strDescError As String = "OK"
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try

            '       ELIMINAMOS LOS ALIAS YA EXISTENTES

            SQLCommand = New SqlClient.SqlCommand("Rcp_Alias_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Alias_Mantencion"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = "ELIMINAR"
            SQLCommand.Parameters.Add("pIdEntidad", SqlDbType.VarChar, 50).Value = IIf(strId_Entidad.Trim = "", DBNull.Value, strId_Entidad.Trim)
            SQLCommand.Parameters.Add("pIdEntidadGPI", SqlDbType.VarChar, 50).Value = intTipoEndtidad
            SQLCommand.Parameters.Add("pIdEntidadContraparte", SqlDbType.VarChar, 9).Value = DBNull.Value
            SQLCommand.Parameters.Add("pValor", SqlDbType.VarChar, 50).Value = DBNull.Value
            SQLCommand.Parameters.Add("pCodEntidadContraparte", SqlDbType.VarChar, 50).Value = DBNull.Value

            '...Resultado
            Dim pSalElimIntPort As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalElimIntPort.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalElimIntPort)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            If Not strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Rollback()
                Exit Function
            End If

            '       INSERTAMOS LOS NUEVOS ALIAS PARA LA ENTIDAD GPI 
            If Not (IsNothing(dstAlias)) Then
                If dstAlias.Tables(0).Rows.Count <> Nothing Then
                    For Each pRow As DataRow In dstAlias.Tables(0).Rows
                        SQLCommand = New SqlClient.SqlCommand("Rcp_Alias_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)
                        SQLCommand.CommandType = CommandType.StoredProcedure
                        SQLCommand.CommandText = "Rcp_Alias_Mantencion"
                        SQLCommand.Parameters.Clear()

                        '[Rcp_Alias_Mantencion]
                        '(@pAccion varchar(10), 
                        '@pIdEntidad varchar(50),  Numero de la Entidad id_Cliente IdCuenta etc
                        '@pIdEntidadGPI varchar(9), TIPO DE ENTIDAD GPI Cliente Cuentas Cajas Etc
                        '@pIdEntidadContraparte varchar(9),
                        '@pValor varchar(50), 
                        '@pCodEntidadContraparte varchar(50),
                        '@pResultado varchar(200) OUTPUT) ID_ENTIDAD_CONTRAPARTE,DSC_ENTIDAD_CONTRAPARTE,COD_ENTIDAD_CONTRAPARTE,VALOR"

                        SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = "INSERTAR"
                        SQLCommand.Parameters.Add("pIdEntidad", SqlDbType.VarChar, 50).Value = strId_Entidad
                        SQLCommand.Parameters.Add("pIdEntidadGPI", SqlDbType.VarChar, 90).Value = intTipoEndtidad
                        SQLCommand.Parameters.Add("pIdEntidadContraparte", SqlDbType.VarChar, 9).Value = pRow("ID_ENTIDAD_CONTRAPARTE").ToString
                        SQLCommand.Parameters.Add("pValor", SqlDbType.VarChar, 50).Value = pRow("VALOR").ToString
                        SQLCommand.Parameters.Add("pCodEntidadContraparte", SqlDbType.VarChar, 50).Value = pRow("COD_ENTIDAD_CONTRAPARTE").ToString

                        '...Resultado
                        Dim pSalInstPort As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                        pSalInstPort.Direction = Data.ParameterDirection.Output
                        SQLCommand.Parameters.Add(pSalInstPort)

                        SQLCommand.ExecuteNonQuery()

                        strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
                        If Not strDescError.ToUpper.Trim = "OK" Then
                            Exit For
                        End If
                    Next
                End If

                If strDescError.ToUpper.Trim = "OK" Then
                    MiTransaccionSQL.Commit()
                Else
                    MiTransaccionSQL.Rollback()
                End If
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            Alias_Mantenedor = strDescError
        End Try
    End Function
    ' Zona Publicador
    Public Function AliasPublicador_Ver(ByVal strIdEntidad As String, _
                              ByVal strIdEntidadGPI As String, _
                              ByVal strIdEntidadPublicador As String, _
                              ByVal strColumnas As String, _
                              ByRef strRetorno As String) As DataSet

        Dim LDS_Alias As New DataSet
        Dim LDS_Alias_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_AliasPublicador_Consultar"
        Lstr_NombreTabla = "AliasPublicador"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdEntidad", SqlDbType.Float, 50).Value = IIf(strIdEntidad.Trim = "", DBNull.Value, strIdEntidad.Trim)
            SQLCommand.Parameters.Add("pIdEntidadGPI", SqlDbType.Float, 9).Value = IIf(strIdEntidadGPI.Trim = "", DBNull.Value, strIdEntidadGPI.Trim)
            SQLCommand.Parameters.Add("pIdEntidadPublicador", SqlDbType.Float, 9).Value = IIf(strIdEntidadPublicador.Trim = "", DBNull.Value, strIdEntidadPublicador.Trim)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Alias, Lstr_NombreTabla)

            LDS_Alias_Aux = LDS_Alias

            If strColumnas.Trim = "" Then
                LDS_Alias_Aux = LDS_Alias
            Else
                LDS_Alias_Aux = LDS_Alias.Copy
                For Each Columna In LDS_Alias.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Alias_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Alias_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_Alias.Dispose()
            Return LDS_Alias_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function AliasPublicador_Mantenedor(ByVal strId_Entidad As String, _
                                     ByVal intTipoEndtidad As Integer, _
                                     ByRef dstAlias As DataSet) As String

        Dim strDescError As String = "OK"
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try

            '       ELIMINAMOS LOS ALIAS YA EXISTENTES

            SQLCommand = New SqlClient.SqlCommand("Rcp_Alias_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Alias_Mantencion"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = "ELIMINAR"
            SQLCommand.Parameters.Add("pIdEntidad", SqlDbType.Float, 10).Value = IIf(strId_Entidad.Trim = "", DBNull.Value, strId_Entidad.Trim)
            SQLCommand.Parameters.Add("pIdEntidadGPI", SqlDbType.Float, 10).Value = intTipoEndtidad
            SQLCommand.Parameters.Add("pIdEntidadContraparte", SqlDbType.VarChar, 9).Value = DBNull.Value
            SQLCommand.Parameters.Add("pValor", SqlDbType.VarChar, 50).Value = DBNull.Value
            SQLCommand.Parameters.Add("pCodEntidadContraparte", SqlDbType.VarChar, 50).Value = DBNull.Value

            '...Resultado
            Dim pSalElimIntPort As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalElimIntPort.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalElimIntPort)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            If Not strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Rollback()
                Exit Function
            End If

            '       INSERTAMOS LOS NUEVOS ALIAS PARA LA ENTIDAD GPI 
            If Not (IsNothing(dstAlias)) Then
                If dstAlias.Tables(0).Rows.Count <> Nothing Then
                    For Each pRow As DataRow In dstAlias.Tables(0).Rows
                        SQLCommand = New SqlClient.SqlCommand("Rcp_Alias_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)
                        SQLCommand.CommandType = CommandType.StoredProcedure
                        SQLCommand.CommandText = "Rcp_Alias_Mantencion"
                        SQLCommand.Parameters.Clear()

                        '[Rcp_Alias_Mantencion]
                        '(@pAccion varchar(10), 
                        '@pIdEntidad varchar(50),  Numero de la Entidad id_Cliente IdCuenta etc
                        '@pIdEntidadGPI varchar(9), TIPO DE ENTIDAD GPI Cliente Cuentas Cajas Etc
                        '@pIdEntidadContraparte varchar(9),
                        '@pValor varchar(50), 
                        '@pCodEntidadContraparte varchar(50),
                        '@pResultado varchar(200) OUTPUT) ID_ENTIDAD_CONTRAPARTE,DSC_ENTIDAD_CONTRAPARTE,COD_ENTIDAD_CONTRAPARTE,VALOR"

                        SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = "INSERTAR"
                        SQLCommand.Parameters.Add("pIdEntidad", SqlDbType.VarChar, 50).Value = strId_Entidad
                        SQLCommand.Parameters.Add("pIdEntidadGPI", SqlDbType.VarChar, 90).Value = intTipoEndtidad
                        SQLCommand.Parameters.Add("pIdEntidadContraparte", SqlDbType.VarChar, 9).Value = pRow("ID_ENTIDAD_CONTRAPARTE").ToString
                        SQLCommand.Parameters.Add("pValor", SqlDbType.VarChar, 50).Value = pRow("VALOR").ToString
                        SQLCommand.Parameters.Add("pCodEntidadContraparte", SqlDbType.VarChar, 50).Value = pRow("COD_ENTIDAD_CONTRAPARTE").ToString

                        '...Resultado
                        Dim pSalInstPort As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                        pSalInstPort.Direction = Data.ParameterDirection.Output
                        SQLCommand.Parameters.Add(pSalInstPort)

                        SQLCommand.ExecuteNonQuery()

                        strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
                        If Not strDescError.ToUpper.Trim = "OK" Then
                            Exit For
                        End If
                    Next
                End If

                If strDescError.ToUpper.Trim = "OK" Then
                    MiTransaccionSQL.Commit()
                Else
                    MiTransaccionSQL.Rollback()
                End If
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            AliasPublicador_Mantenedor = strDescError
        End Try
    End Function

    Public Function AliasContraparte_Buscar(ByVal strCod_Contraparte As String, ByVal strTabla As String _
                                                , ByVal strValor As String, ByRef strRetorno As String) As String

        Dim LDS_AliasNemo As New DataSet
        Dim LDS_Conciliador_Aux As New DataSet
        Dim lValor As String = ""
        Dim LstrProcedimiento As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim strResultado As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "CBA_CSIMPTASARISK_BuscarContraparte"
        Lstr_NombreTabla = ""

        Connect.Abrir()

        Try

            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("PCOD_EXTERNO_CONTRAPARTE", SqlDbType.VarChar, 100).Value = strCod_Contraparte
            SQLCommand.Parameters.Add("PTABLA", SqlDbType.VarChar, 100).Value = strTabla
            SQLCommand.Parameters.Add("PVALOR", SqlDbType.VarChar, 10).Value = strValor

            Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultado.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)

            Dim pSalValor As New SqlClient.SqlParameter("pSalResul", SqlDbType.VarChar, 200)
            pSalValor.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalValor)

            SQLCommand.ExecuteNonQuery()

            strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            strResultado = SQLCommand.Parameters("pSalResul").Value.ToString.Trim

            Return strResultado

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()

        End Try
    End Function


End Class
