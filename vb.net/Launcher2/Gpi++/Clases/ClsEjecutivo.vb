﻿Public Class ClsEjecutivo
    Public Function Ejecutivo_Ver(ByVal strCodigoEjecutivo As String, _
                                  ByVal strCodigoSucursal As String, _
                                  ByVal strEstadoEjecutivo As String, _
                                  ByVal strEstadoSucursal As String, _
                                  ByVal strCodigoExterno As String, _
                                  ByVal strColumnas As String, _
                                  ByRef strRetorno As String) As DataSet

        Dim LDS_Ejecutivo As New DataSet
        Dim LDS_Ejecutivo_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Ejecutivo_Consultar"
        Lstr_NombreTabla = "Ejecutivo_Sucursal"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdEjecutivo", SqlDbType.VarChar, 10).Value = IIf(strCodigoEjecutivo.Trim = "", DBNull.Value, strCodigoEjecutivo.Trim)
            SQLCommand.Parameters.Add("pIdSucursal", SqlDbType.VarChar, 10).Value = IIf(strCodigoSucursal.Trim = "", DBNull.Value, strCodigoSucursal.Trim)
            SQLCommand.Parameters.Add("pEstadoEjecutivo", SqlDbType.Char, 3).Value = IIf(strEstadoEjecutivo.Trim = "", DBNull.Value, strEstadoEjecutivo.Trim)
            SQLCommand.Parameters.Add("pEstadoSucursal", SqlDbType.Char, 3).Value = IIf(strEstadoSucursal.Trim = "", DBNull.Value, strEstadoSucursal.Trim)
            SQLCommand.Parameters.Add("pCodExterno", SqlDbType.VarChar, 30).Value = IIf(strCodigoExterno.Trim = "", DBNull.Value, strCodigoExterno.Trim)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Ejecutivo, Lstr_NombreTabla)

            LDS_Ejecutivo_Aux = LDS_Ejecutivo

            If strColumnas.Trim = "" Then
                LDS_Ejecutivo_Aux = LDS_Ejecutivo
            Else
                LDS_Ejecutivo_Aux = LDS_Ejecutivo.Copy
                For Each Columna In LDS_Ejecutivo.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Ejecutivo_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Ejecutivo_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_Ejecutivo.Dispose()
            Return LDS_Ejecutivo_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function Ejecutivo_Mantenedor(ByVal strAccion As String, _
                                         ByVal strCodEjecutivo As String, _
                                         ByVal strDscEjecutivo As String, _
                                         ByVal strEstEjecutivo As String, _
                                         ByVal strCodSucursal As String, _
                                         Optional ByVal strCodExterno As String = "") As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_Ejecutivo_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Ejecutivo_Mantencion"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 10).Value = strAccion
            SQLCommand.Parameters.Add("pCodEjecutivo", SqlDbType.VarChar, 10).Value = strCodEjecutivo
            SQLCommand.Parameters.Add("pDscEjecutivo", SqlDbType.VarChar, 100).Value = strDscEjecutivo
            SQLCommand.Parameters.Add("pEstEjecutivo", SqlDbType.VarChar, 3).Value = strEstEjecutivo
            SQLCommand.Parameters.Add("pCodSucursal", SqlDbType.VarChar, 10).Value = strCodSucursal
            SQLCommand.Parameters.Add("pCodExterno", SqlDbType.VarChar, 30).Value = strCodExterno

            '...Resultado
            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            Ejecutivo_Mantenedor = strDescError
        End Try
    End Function

    Public Function Ejecutivo_Buscar(ByVal strCodigoEjecutivo As String, _
                                  ByVal strNombreEjecutivo As String, _
                                  ByVal strEstadoEjecutivo As String, _
                                  ByVal strNombreSucursal As String, _
                                  ByVal strCodigoExterno As String, _
                                  ByRef strRetorno As String) As DataSet

        Dim LDS_Ejecutivo As New DataSet

        Dim Lstr_NombreTabla As String = ""

        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Buscar_Ejecutivo"
        Lstr_NombreTabla = "Ejecutivo_Sucursal"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pCodEjecutivo", SqlDbType.VarChar, 10).Value = IIf(strCodigoEjecutivo.Trim = "", DBNull.Value, strCodigoEjecutivo.Trim)
            SQLCommand.Parameters.Add("pNombreEjecutivo", SqlDbType.VarChar, 100).Value = IIf(strNombreEjecutivo.Trim = "", DBNull.Value, strNombreEjecutivo.Trim)
            SQLCommand.Parameters.Add("@pDscSucursal", SqlDbType.Char, 100).Value = IIf(strNombreSucursal.Trim = "", DBNull.Value, strNombreSucursal.Trim)
            SQLCommand.Parameters.Add("@pEstadoEjecutivo", SqlDbType.Char, 3).Value = IIf(strEstadoEjecutivo.Trim = "", DBNull.Value, strEstadoEjecutivo.Trim)
            SQLCommand.Parameters.Add("pCodExterno", SqlDbType.VarChar, 30).Value = IIf(strCodigoExterno.Trim = "", DBNull.Value, strCodigoExterno.Trim)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Ejecutivo, Lstr_NombreTabla)

            strRetorno = "OK"
            LDS_Ejecutivo.Dispose()
            Return LDS_Ejecutivo

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

End Class
