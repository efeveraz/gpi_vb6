﻿Public Class ClsPlazo

    Public Function Plazo_Consultar(ByVal strCodPlazo As String, _
                                    ByVal strColumnas As String, _
                                    ByRef strRetorno As String) As DataSet

        Dim LDS_Plazo As New DataSet
        Dim LDS_Plazo_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Plazo_Consultar"
        Lstr_NombreTabla = "Plazos"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pCodPlazo", SqlDbType.Int, 10).Value = IIf(strCodPlazo = "", DBNull.Value, strCodPlazo)

            '...Resultado
            Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultado.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Plazo, Lstr_NombreTabla)

            LDS_Plazo_Aux = LDS_Plazo

            If strColumnas.Trim = "" Then
                LDS_Plazo_Aux = LDS_Plazo
            Else
                LDS_Plazo_Aux = LDS_Plazo.Copy
                For Each Columna In LDS_Plazo.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Plazo_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Plazo_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            LDS_Plazo.Dispose()
            Return LDS_Plazo_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function Plazo_Buscar(ByVal strCodPlazo As String, _
                                 ByVal strDscPlazo As String, _
                                 ByVal dblValorInicial As Double, _
                                 ByVal dblValorFinal As Double, _
                                 ByVal strEstPlazo As String, _
                                 ByVal strColumnas As String, _
                                 ByRef strRetorno As String) As DataSet

        Dim LDS_Plazo As New DataSet
        Dim LDS_Plazo_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Plazo_Buscar"
        Lstr_NombreTabla = "Plazos"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pCodPlazo", SqlDbType.VarChar, 5).Value = IIf(strCodPlazo = "", DBNull.Value, strCodPlazo)
            SQLCommand.Parameters.Add("pDscPlazo", SqlDbType.VarChar, 100).Value = IIf(strDscPlazo = "", DBNull.Value, strDscPlazo)
            SQLCommand.Parameters.Add("pValorInicial", SqlDbType.Float).Value = IIf(dblValorInicial = 0, DBNull.Value, dblValorInicial)
            SQLCommand.Parameters.Add("pValorFinal", SqlDbType.Float).Value = IIf(dblValorFinal = 0, DBNull.Value, dblValorFinal)
            SQLCommand.Parameters.Add("pEstPlazo", SqlDbType.VarChar, 3).Value = IIf(strEstPlazo = "", DBNull.Value, strEstPlazo)



            '...Resultado
            Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultado.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Plazo, Lstr_NombreTabla)

            LDS_Plazo_Aux = LDS_Plazo

            If strColumnas.Trim = "" Then
                LDS_Plazo_Aux = LDS_Plazo
            Else
                LDS_Plazo_Aux = LDS_Plazo.Copy
                For Each Columna In LDS_Plazo.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Plazo_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Plazo_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            LDS_Plazo.Dispose()
            Return LDS_Plazo_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function Plazo_Mantencion(ByVal strAccion As String, _
                                     ByVal strCodPlazo As String, _
                                     ByVal strDscPlazo As String, _
                                     ByVal dblValorInicial As Double, _
                                     ByVal dblValorFinal As Double, _
                                     ByVal strEstPlazo As String) As String


        Dim strDescError As String = "OK"
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try


            SQLCommand = New SqlClient.SqlCommand("Rcp_Plazo_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Plazo_Mantencion"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 20).Value = UCase(strAccion.Trim)
            SQLCommand.Parameters.Add("pCodPlazo", SqlDbType.VarChar, 5).Value = strCodPlazo
            SQLCommand.Parameters.Add("pDscPlazo", SqlDbType.VarChar, 100).Value = strDscPlazo
            SQLCommand.Parameters.Add("pValorInicial", SqlDbType.Float).Value = dblValorInicial
            SQLCommand.Parameters.Add("pValorFinal", SqlDbType.Float).Value = dblValorFinal
            SQLCommand.Parameters.Add("pEstPlazo", SqlDbType.VarChar, 3).Value = strEstPlazo


            '...Resultado
            Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultado.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                If Trim(strDescError) = "OK" Then
                    MiTransaccionSQL.Commit()
                    Return ("OK")
                End If
            Else
                GoTo Salir
            End If

Salir:
            If Trim(strDescError) = "OK" Then
                MiTransaccionSQL.Commit()
                Return ("OK")
            Else
                MiTransaccionSQL.Rollback()
                Return strDescError
            End If


        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en la mantención de plazos." & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            Plazo_Mantencion = strDescError
        End Try
    End Function

End Class
