﻿Public Class ClsNormativo

    Public Function Normativo_Ver(ByVal lngIdNormativo As Long, _
                                  ByVal strAbrNormativo As String, _
                                  ByVal strEstNormativo As String, _
                                  ByVal strColumnas As String, _
                                  ByRef strRetorno As String) As DataSet

        Dim LDS_Normativo As New DataSet
        Dim LDS_Normativo_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Normativo_Consultar"
        Lstr_NombreTabla = "NORMATIVO"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdNormativo", SqlDbType.Float, 10).Value = IIf(lngIdNormativo = 0, DBNull.Value, lngIdNormativo)
            SQLCommand.Parameters.Add("pArbNormativo", SqlDbType.VarChar, 15).Value = IIf(strAbrNormativo.Trim = "", DBNull.Value, strAbrNormativo.Trim)
            SQLCommand.Parameters.Add("pEstNormativo", SqlDbType.VarChar, 15).Value = IIf(strEstNormativo.Trim = "", DBNull.Value, strEstNormativo.Trim)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Normativo, Lstr_NombreTabla)

            LDS_Normativo_Aux = LDS_Normativo

            If strColumnas.Trim = "" Then
                LDS_Normativo_Aux = LDS_Normativo
            Else
                LDS_Normativo_Aux = LDS_Normativo.Copy
                For Each Columna In LDS_Normativo.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Normativo_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Normativo_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            Return LDS_Normativo_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function


End Class
