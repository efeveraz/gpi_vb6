﻿Public Class ClsReporteMasivoComprobante
    Public sPlantilla As String
    Private nTotalDetalles As Integer
    Private nTotalVariables As Integer

    Dim VariableNombre As New ArrayList
    Dim VariableValor As New ArrayList
    Dim DetalleNemo As New ArrayList
    Dim DetalleCant As New ArrayList
    Dim DetallePrec As New ArrayList
    Dim DetalleTota As New ArrayList

    Dim sProcedimiento As String
    Public Function Carga_Grilla_APORTE_RETIRO(ByVal strCuentas As String, _
                                               ByVal StrFechaDesde As String, _
                                               ByVal StrFechaHasta As String, _
                                               ByVal strColumnas As String, _
                                               ByRef strRetorno As String) As DataSet
        'Devuelve los datos a la Grilla

        Dim LDS_ImportaD As New DataSet
        Dim LDS_ImportaD_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "RCP_REPORTE_APORTERETIRO_PATRIMONIAL"
        Lstr_NombreTabla = "APORTE_RETIRO"

        Connect.Abrir()
        '@LID_NEGOCIO  NUMERIC,
        '@pCuentas     TEXT,
        '@PFECHADESD	  CHAR(10),
        '@PFECHAHAST	  CHAR(10),
        '@pError		  VARCHAR(100)  OUTPUT

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("pLID_NEGOCIO", SqlDbType.Int, 10).Value = glngNegocioOperacion
            SQLCommand.Parameters.Add("pCuentas", SqlDbType.Text).Value = IIf(strCuentas = "©", DBNull.Value, strCuentas)
            SQLCommand.Parameters.Add("PFECHADESD", SqlDbType.VarChar, 10).Value = StrFechaDesde
            SQLCommand.Parameters.Add("PFECHAHAST", SqlDbType.VarChar, 10).Value = StrFechaHasta
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Int).Value = glngIdUsuario

            '...Resultado
            Dim ParametroSal1 As New SqlClient.SqlParameter("pError", SqlDbType.VarChar, 200)
            ParametroSal1.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal1)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_ImportaD, Lstr_NombreTabla)

            LDS_ImportaD_Aux = LDS_ImportaD

            If strColumnas.Trim = "" Then
                LDS_ImportaD_Aux = LDS_ImportaD
            Else
                LDS_ImportaD_Aux = LDS_ImportaD.Copy
                For Each Columna In LDS_ImportaD.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_ImportaD_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_ImportaD_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_ImportaD.Dispose()
            Return LDS_ImportaD_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function ReporteComprobante_BUSCAR(ByVal strFechaDesde As String, _
                                              ByVal strFechaHasta As String, _
                                              ByRef strRetorno As String) As DataSet

        Dim LDS_ReporteComprobante_BUSCAR As New DataSet
        Dim LDS_ReporteComprobante_BUSCAR_Aux As New DataSet

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        sProcedimiento = "RCP_REPORTECOMPROBANTE_BUSCAR"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(sProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = sProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("@PIDNEGOCIO", SqlDbType.Int, 10).Value = glngNegocioOperacion
            SQLCommand.Parameters.Add("@PFECHADESD", SqlDbType.Char, 10).Value = strFechaDesde
            SQLCommand.Parameters.Add("@PFECHAHAST", SqlDbType.Char, 10).Value = strFechaHasta

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_ReporteComprobante_BUSCAR)

            LDS_ReporteComprobante_BUSCAR_Aux = LDS_ReporteComprobante_BUSCAR
            strRetorno = "OK"
            Return LDS_ReporteComprobante_BUSCAR_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function ReporteComprobanteOperacion_BUSCAR(ByVal strFechaDesde As String, _
                                              ByVal strFechaHasta As String, _
                                              ByRef strRetorno As String) As DataSet

        Dim LDS_ReporteComprobante_BUSCAR As New DataSet
        Dim LDS_ReporteComprobante_BUSCAR_Aux As New DataSet

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        sProcedimiento = "RCP_COMPROBANTEOPERACION_BUSCAR"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(sProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = sProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("@PIDNEGOCIO", SqlDbType.Int, 10).Value = glngNegocioOperacion
            SQLCommand.Parameters.Add("@PFECHADESD", SqlDbType.Char, 10).Value = strFechaDesde
            SQLCommand.Parameters.Add("@PFECHAHAST", SqlDbType.Char, 10).Value = strFechaHasta

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_ReporteComprobante_BUSCAR)

            LDS_ReporteComprobante_BUSCAR_Aux = LDS_ReporteComprobante_BUSCAR
            strRetorno = "OK"
            Return LDS_ReporteComprobante_BUSCAR_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function DetalleOperacion_Buscar(ByVal intIdOperacion As Integer, _
                                            ByRef strRetorno As String) As DataSet

        Dim LDS_Circular0012 As New DataSet
        Dim LDS_Circular0012_Aux As New DataSet

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        sProcedimiento = "RCP_REPORTECOMPROBANTE_DETALLEOPERACION_BUSCAR"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(sProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = sProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("@pIdOperacion", SqlDbType.Int, 10).Value = intIdOperacion

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Circular0012)

            LDS_Circular0012_Aux = LDS_Circular0012
            strRetorno = "OK"
            Return LDS_Circular0012_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function DetalleComproOperacion_Buscar(ByVal intIdOperacion As Integer, _
                                            ByRef strRetorno As String) As DataSet

        Dim LDS_Circular0012 As New DataSet
        Dim LDS_Circular0012_Aux As New DataSet

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        sProcedimiento = "RCP_REPORTECOMPROBANTE_DETALLEOPERACION_BUSCAR"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(sProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = sProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("@pIdOperacion", SqlDbType.Int, 10).Value = intIdOperacion

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Circular0012)

            LDS_Circular0012_Aux = LDS_Circular0012
            strRetorno = "OK"
            Return LDS_Circular0012_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function GeneraComprobante(ByVal Cant As Integer, _
                                            ByRef strRetorno As String) As Boolean
        Dim DocWord As Object
        Dim oTabla As Object
        '-----------------------------------
        Dim nX As Integer
        Dim nFila As Integer

        ' Dim Existe As Long
        '-----------------------------------

        DocWord = CreateObject("word.application")

        With DocWord
            .Application.Visible = False
            Try
                .Documents.Open(sPlantilla)
            Catch ex As Exception
                strRetorno = "Problema al generar reporte. " & ex.Message
                .Quit(0)
                Return Nothing
            End Try

            For nX = 0 To nTotalVariables - 1
                .ActiveDocument.Content.Find.Execute(FindText:=Me.VariableNombre(nX), _
                                                    replacewith:=Me.VariableValor(nX), _
                                                    Replace:=2)
            Next

            nFila = 1
            oTabla = DocWord.ActiveDocument.Tables(3)

            For nY = 0 To nTotalDetalles - 1
                If nY >= oTabla.Rows.Count - 1 Then
                    oTabla.Rows.Add()
                End If

                nFila = nY + 2

                oTabla.Cell(nFila, 1).Range.Text = Me.DetalleNemo(nY) 'oDetalleFichas(nX).Nemotecnico
                oTabla.Cell(nFila, 2).Range.Text = Me.DetalleCant(nY) 'oDetalleFichas(nX).Cantidad
                oTabla.Cell(nFila, 3).Range.Text = Me.DetallePrec(nY) 'oDetalleFichas(nX).Precio
                oTabla.Cell(nFila, 4).Range.Text = Me.DetalleTota(nY) 'oDetalleFichas(nX).Total
            Next
            If Cant > 1 Then
                Try
                    .ActiveDocument.PrintOut(False)
                Catch ex As Exception
                    strRetorno = ex.Message
                    .Quit(0)
                    Return Nothing
                End Try
                strRetorno = "OK"
                Do While .BackgroundPrintingStatus >= 1
                Loop
                .Quit(0)
            Else
                .Visible = True
            End If

            CleanDocumento()

        End With

        DocWord = Nothing

    End Function

    Public Function GeneraComprobanteOperacion(ByVal Cant As Integer, _
                                            ByRef strRetorno As String) As Boolean
        Dim DocWord As Object
        '-----------------------------------
        Dim nX As Integer

        ' Dim Existe As Long
        '-----------------------------------

        DocWord = CreateObject("word.application")

        With DocWord
            .Application.Visible = False
            Try
                .Documents.Open(sPlantilla)
            Catch ex As Exception
                strRetorno = "Problema al generar reporte. " & ex.Message
                .Quit(0)
                Return Nothing
            End Try

            For nX = 0 To nTotalVariables - 1
                .ActiveDocument.Content.Find.Execute(FindText:=Me.VariableNombre(nX), _
                                                    replacewith:=Me.VariableValor(nX), _
                                                    Replace:=2)
            Next

            If Cant > 1 Then
                Try
                    .ActiveDocument.PrintOut(False)
                Catch ex As Exception
                    strRetorno = ex.Message
                    .Quit(0)
                    Return Nothing
                End Try
                strRetorno = "OK"
                Do While .BackgroundPrintingStatus >= 1
                Loop
                .Quit(0)
            Else
                .Visible = True
            End If

            CleanDocumento()

        End With

        DocWord = Nothing

    End Function

    Public Sub AddVariable(ByVal Nombre As String, ByVal valor As String)
        Me.VariableNombre.Add(Nombre)
        Me.VariableValor.Add(valor)
        nTotalVariables = Me.VariableNombre.Count

    End Sub

    Public Sub AddDetalle(ByVal Nemotecnico As String, ByVal Cantidad As String, ByVal Precio As String, ByVal Total As String)
        Me.DetalleNemo.Add(Nemotecnico)
        Me.DetalleCant.Add(Cantidad)
        Me.DetallePrec.Add(Precio)
        Me.DetalleTota.Add(Total)
        nTotalDetalles = Me.DetalleNemo.Count

    End Sub

    Public Sub CleanDocumento()
        CleanVariables()

        CleanDetalle()
    End Sub

    Public Sub CleanDetalle()
        nTotalDetalles = 0
        Me.DetalleCant.Clear()
        Me.DetalleNemo.Clear()
        Me.DetallePrec.Clear()
        Me.DetalleTota.Clear()
    End Sub

    Public Sub CleanVariables()
        nTotalVariables = 0
        Me.VariableNombre.Clear()
        Me.VariableValor.Clear()
    End Sub

End Class
