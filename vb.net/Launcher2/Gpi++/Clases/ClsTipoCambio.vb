﻿Public Class ClsTipoCambio
    Public Function BuscarTiposCambios(ByVal strColumnas As String, _
                                       ByVal strParidadTipoCambio As String, _
                                       ByRef strRetorno As String) As DataSet

        Dim LDS_TipoCambio As New DataSet
        Dim LDS_TipoCambio_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_TipoCambio_Buscar"
        Lstr_NombreTabla = "TipoCambio"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("@pPARIDAD_TIPOCAMBIO", SqlDbType.Char, 3).Value = strParidadTipoCambio

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_TipoCambio, Lstr_NombreTabla)

            LDS_TipoCambio_Aux = LDS_TipoCambio

            If strColumnas.Trim = "" Then
                LDS_TipoCambio_Aux = LDS_TipoCambio
            Else
                LDS_TipoCambio_Aux = LDS_TipoCambio.Copy
                For Each Columna In LDS_TipoCambio.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_TipoCambio_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_TipoCambio_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_TipoCambio.Dispose()

            Return LDS_TipoCambio_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function BuscarIndices(ByVal strColumnas As String, _
                                  ByRef strRetorno As String) As DataSet

        Dim LDS_TipoCambio As New DataSet
        Dim LDS_TipoCambio_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_TipoCambioIndice_Buscar"
        Lstr_NombreTabla = "TipoIndice"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_TipoCambio, Lstr_NombreTabla)

            LDS_TipoCambio_Aux = LDS_TipoCambio

            If strColumnas.Trim = "" Then
                LDS_TipoCambio_Aux = LDS_TipoCambio
            Else
                LDS_TipoCambio_Aux = LDS_TipoCambio.Copy
                For Each Columna In LDS_TipoCambio.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_TipoCambio_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_TipoCambio_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next


            strRetorno = "OK"
            LDS_TipoCambio.Dispose()

            Return LDS_TipoCambio_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function
    Public Function BuscarValorPorFecha(ByVal strFecha As String, _
                                        ByVal strColumnas As String, _
                                        ByRef strRetorno As String) As DataSet

        Dim LDS_ValorTipoCambio As New DataSet
        Dim LDS_ValorTipoCambio_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_ValorTipoCambio_BuscarPorFecha"
        Lstr_NombreTabla = "ValorTipoCambio"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("@pFecha", SqlDbType.Char, 10).Value = strFecha

            Dim oParamSal As New SqlClient.SqlParameter("@pResultado", SqlDbType.Char, 200)
            oParamSal.Direction = ParameterDirection.Output
            SQLCommand.Parameters.Add(oParamSal)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_ValorTipoCambio, Lstr_NombreTabla)

            LDS_ValorTipoCambio_Aux = LDS_ValorTipoCambio

            If strColumnas.Trim = "" Then
                LDS_ValorTipoCambio_Aux = LDS_ValorTipoCambio
            Else
                LDS_ValorTipoCambio_Aux = LDS_ValorTipoCambio.Copy
                For Each Columna In LDS_ValorTipoCambio.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_ValorTipoCambio_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_ValorTipoCambio_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = SQLCommand.Parameters("@pResultado").Value.ToString.Trim

            LDS_ValorTipoCambio.Dispose()

            Return LDS_ValorTipoCambio_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function ValorIndicePorFecha(ByVal strFecha As String, _
                                        ByVal strColumnas As String, _
                                        ByRef strRetorno As String) As DataSet

        Dim LDS_ValorTipoCambio As New DataSet
        Dim LDS_ValorTipoCambio_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_ValorIndice_BuscarPorFecha"
        Lstr_NombreTabla = "ValorIndice"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("@pFecha", SqlDbType.Char, 10).Value = strFecha

            Dim oParamSal As New SqlClient.SqlParameter("@pResultado", SqlDbType.Char, 200)
            oParamSal.Direction = ParameterDirection.Output
            SQLCommand.Parameters.Add(oParamSal)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_ValorTipoCambio, Lstr_NombreTabla)

            LDS_ValorTipoCambio_Aux = LDS_ValorTipoCambio

            If strColumnas.Trim = "" Then
                LDS_ValorTipoCambio_Aux = LDS_ValorTipoCambio
            Else
                LDS_ValorTipoCambio_Aux = LDS_ValorTipoCambio.Copy
                For Each Columna In LDS_ValorTipoCambio.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_ValorTipoCambio_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_ValorTipoCambio_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = SQLCommand.Parameters("@pResultado").Value.ToString.Trim

            LDS_ValorTipoCambio.Dispose()

            Return LDS_ValorTipoCambio_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function


    Public Function BuscarValorPorTipo(ByVal intIdTipoCambio As Integer, _
                                       ByVal strFechaDesde As String, _
                                       ByVal strFechaHasta As String, _
                                       ByVal strColumnas As String, _
                                       ByRef strRetorno As String) As DataSet

        Dim LDS_ValorTipoCambio As New DataSet
        Dim LDS_ValorTipoCambio_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_ValorTipoCambio_BuscarPorTipoCambio"
        Lstr_NombreTabla = "ValorTipoCambio"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("@pIdTipoCambio", SqlDbType.Int).Value = intIdTipoCambio
            SQLCommand.Parameters.Add("@pfechaDesde", SqlDbType.Char, 10).Value = strFechaDesde
            SQLCommand.Parameters.Add("@pfechaHasta", SqlDbType.Char, 10).Value = strFechaHasta

            Dim oParamSal As New SqlClient.SqlParameter("@pResultado", SqlDbType.Char, 200)
            oParamSal.Direction = ParameterDirection.Output
            SQLCommand.Parameters.Add(oParamSal)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_ValorTipoCambio, Lstr_NombreTabla)

            LDS_ValorTipoCambio_Aux = LDS_ValorTipoCambio

            If strColumnas.Trim = "" Then
                LDS_ValorTipoCambio_Aux = LDS_ValorTipoCambio
            Else
                LDS_ValorTipoCambio_Aux = LDS_ValorTipoCambio.Copy
                For Each Columna In LDS_ValorTipoCambio.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_ValorTipoCambio_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_ValorTipoCambio_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = SQLCommand.Parameters("@pResultado").Value.ToString.Trim

            LDS_ValorTipoCambio.Dispose()

            Return LDS_ValorTipoCambio_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Sub ValorTipoCambioMantenedor(ByVal DS_ValorTipoCambio As DataSet, _
                                         ByRef strRetorno As String)
        Dim DR_Fila As DataRow = Nothing
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Entra As Boolean = True

        Dim Connect As Cls_Conexion = New Cls_Conexion

        'Abre la conexion
        Connect.Abrir()

        Dim MiTransaccion As SqlClient.SqlTransaction


        MiTransaccion = Connect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_ValorTipoCambio_Mantencion", MiTransaccion.Connection, MiTransaccion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_ValorTipoCambio_Mantencion"

            For Each DR_Fila In DS_ValorTipoCambio.Tables(0).Select("OPERACION = 1")
                SQLCommand.Parameters.Clear()
                Entra = True
                'INSERTA
                If IsDBNull(DR_Fila("ID_VALOR_TIPO_CAMBIO")) And (Not IsDBNull(DR_Fila("VALOR"))) Then
                    SQLCommand.Parameters.Add("@pAccion", SqlDbType.VarChar, 10).Value = "INSERTAR"

                    SQLCommand.Parameters.Add("@pIdTipoCambio", SqlDbType.Int).Value = DR_Fila("ID_TIPO_CAMBIO")
                    SQLCommand.Parameters.Add("@pfecha", SqlDbType.Char, 10).Value = DR_Fila("FECHA")
                    SQLCommand.Parameters.Add("@pValor", SqlDbType.Decimal, 18).Value = DR_Fila("VALOR")

                    Dim ParametroIdValorTipoCambio As New SqlClient.SqlParameter("@pIdValorTipoCambio", SqlDbType.Int)
                    ParametroIdValorTipoCambio.Direction = Data.ParameterDirection.Output
                    SQLCommand.Parameters.Add(ParametroIdValorTipoCambio)
                ElseIf (Not (IsDBNull(DR_Fila("ID_VALOR_TIPO_CAMBIO")))) And (Not IsDBNull(DR_Fila("VALOR"))) Then 'MODIFICA
                    SQLCommand.Parameters.Add("@pAccion", SqlDbType.VarChar, 10).Value = "MODIFICAR"

                    SQLCommand.Parameters.Add("@pIdValorTipoCambio", SqlDbType.Int).Value = DR_Fila("ID_VALOR_TIPO_CAMBIO")
                    SQLCommand.Parameters.Add("@pIdTipoCambio", SqlDbType.Int).Value = DR_Fila("ID_TIPO_CAMBIO")
                    SQLCommand.Parameters.Add("@pfecha", SqlDbType.Char, 10).Value = DR_Fila("FECHA")
                    SQLCommand.Parameters.Add("@pValor", SqlDbType.Decimal, 18).Value = DR_Fila("VALOR")
                ElseIf (Not (IsDBNull(DR_Fila("ID_VALOR_TIPO_CAMBIO")))) And (IsDBNull(DR_Fila("VALOR"))) Then 'ELIMINA
                    SQLCommand.Parameters.Add("@pAccion", SqlDbType.VarChar, 10).Value = "ELIMINAR"
                    SQLCommand.Parameters.Add("@pIdValorTipoCambio", SqlDbType.Int).Value = DR_Fila("ID_VALOR_TIPO_CAMBIO")
                Else
                    strRetorno = "OK"
                    Entra = False
                    'MiTransaccion.Commit()
                    'Exit Sub
                End If

                If Entra Then
                    Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                    ParametroSal2.Direction = Data.ParameterDirection.Output
                    SQLCommand.Parameters.Add(ParametroSal2)
                    SQLCommand.ExecuteNonQuery()
                    strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim
                    If Trim(strRetorno) <> "OK" Then
                        Exit For
                    End If
                End If
            Next
            If Trim(strRetorno) = "OK" Then
                MiTransaccion.Commit()
            Else
                MiTransaccion.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccion.Rollback()
            strRetorno = "Error al grabar Valor Tipo Cambio." & vbCr & Ex.Message
        Finally
            Connect.Cerrar()
        End Try


    End Sub

    Public Sub ValorTipoCambioCargaValores(ByVal idTipoCambio As Integer, _
                                           ByVal DS_Cargas As DataSet, _
                                           ByRef strRetorno As String)
        Dim DR_Fila As DataRow = Nothing
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        'Abre la conexion
        Connect.Abrir()

        Dim MiTransaccion As SqlClient.SqlTransaction


        MiTransaccion = Connect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_ValorTipoCambio_CargaValor", MiTransaccion.Connection, MiTransaccion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_ValorTipoCambio_CargaValor"

            For Each DR_Fila In DS_Cargas.Tables(0).Rows
                SQLCommand.Parameters.Clear()

                'INSERTA
                If IsDBNull(DR_Fila("Fecha")) And IsDBNull(DR_Fila("Monto")) Then
                    strRetorno = "OK"
                    Exit For
                End If
                SQLCommand.Parameters.Add("@pIdTipoCambio", SqlDbType.Int).Value = idTipoCambio
                SQLCommand.Parameters.Add("@pfecha", SqlDbType.DateTime).Value = DR_Fila("Fecha")
                SQLCommand.Parameters.Add("@pValor", SqlDbType.Decimal, 18).Value = DR_Fila("Monto")

                Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                ParametroSal2.Direction = Data.ParameterDirection.Output
                SQLCommand.Parameters.Add(ParametroSal2)

                SQLCommand.ExecuteNonQuery()

                strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim

                If Trim(strRetorno) <> "OK" Then
                    Exit For
                End If
            Next
            If Trim(strRetorno) = "OK" Then
                MiTransaccion.Commit()
            Else
                MiTransaccion.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccion.Rollback()
            strRetorno = "Error al grabar Valor Tipo Cambio." & vbCr & Ex.Message
        Finally
            Connect.Cerrar()
        End Try


    End Sub

    Public Function BuscarTipoCambioporCuentaNegocio(ByVal lngNegocio As Long, _
                                                    ByVal strCuenta As String, _
                                                    ByVal strColumnas As String, _
                                                    ByRef strRetorno As String) As DataSet

        Dim LDS_TipoCambio As New DataSet
        Dim LDS_TipoCambio_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_TipoCambio_BuscarPorCtaNegocio"
        Lstr_NombreTabla = "TipoCambio"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Float, 10).Value = IIf(lngNegocio = 0, DBNull.Value, lngNegocio)
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = IIf(strCuenta = "", DBNull.Value, strCuenta)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_TipoCambio, Lstr_NombreTabla)

            LDS_TipoCambio_Aux = LDS_TipoCambio

            If strColumnas.Trim = "" Then
                LDS_TipoCambio_Aux = LDS_TipoCambio
            Else
                LDS_TipoCambio_Aux = LDS_TipoCambio.Copy
                For Each Columna In LDS_TipoCambio.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_TipoCambio_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_TipoCambio_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_TipoCambio.Dispose()
            Return LDS_TipoCambio_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function TiposDeCambioXCuenta_Mantenedor(ByVal strId_Cuenta As String, ByRef dstTiposDeCambioXCuenta As DataSet) As String

        Dim strDescError As String = "OK"
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try

            '       ELIMINAMOS LOS TIPOS DE CAMBIO X CUENTA YA EXISTENTES

            SQLCommand = New SqlClient.SqlCommand("Rcp_TipoCambio_MantencionPorCliente", MiTransaccionSQL.Connection, MiTransaccionSQL)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_TipoCambio_MantencionPorCliente"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = "ELIMINAR"
            SQLCommand.Parameters.Add("pIdTipoCambio", SqlDbType.Float, 6).Value = DBNull.Value
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = strId_Cuenta
            '...Resultado
            Dim pSalElimIntPort As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalElimIntPort.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalElimIntPort)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            If Not strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Rollback()
                Exit Function
            End If

            '       INSERTAMOS LOS NUEVOS TIPOS DE CAMBIO X CUENTA 

            If dstTiposDeCambioXCuenta.Tables(0).Rows.Count <> Nothing Then
                For Each pRow As DataRow In dstTiposDeCambioXCuenta.Tables(0).Rows
                    If (pRow("ID_NEGOCIO").ToString = 0) And (pRow("INCLUIDO").ToString = "1") Then
                        SQLCommand = New SqlClient.SqlCommand("Rcp_TipoCambio_MantencionPorCliente", MiTransaccionSQL.Connection, MiTransaccionSQL)
                        SQLCommand.CommandType = CommandType.StoredProcedure
                        SQLCommand.CommandText = "Rcp_TipoCambio_MantencionPorCliente"
                        SQLCommand.Parameters.Clear()

                        '[Rcp_TipoCambio_MantencionPorCliente]()
                        '(@pAccion varchar(10), 
                        '@pIdTipoCambio numeric(6, 0),
                        '@pIdCuenta numeric(10, 0), 
                        '@pResultado varchar(200) OUTPUT)
                        '"INCLUIDO, ID_NEGOCIO, ID_CUENTA, ID_TIPO_CAMBIO, DSC_TIPO_CAMBIO, FLG_TIPO_CAMBIO, ABR_TIPO_CAMBIO, COD_MONEDA_FUENTE, DSC_MONEDA_FUENTE, COD_MONEDA_DESTINO, DSC_MONEDA_DESTINO"

                        SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = "INSERTAR"
                        SQLCommand.Parameters.Add("pIdTipoCambio", SqlDbType.Float, 6).Value = pRow("ID_TIPO_CAMBIO").ToString
                        SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = strId_Cuenta

                        '...Resultado
                        Dim pSalInstPort As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                        pSalInstPort.Direction = Data.ParameterDirection.Output
                        SQLCommand.Parameters.Add(pSalInstPort)

                        SQLCommand.ExecuteNonQuery()

                        strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
                    End If
                    If Not strDescError.ToUpper.Trim = "OK" Then
                        Exit For
                    End If
                Next
            End If

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            TiposDeCambioXCuenta_Mantenedor = strDescError
        End Try
    End Function
    Public Function Obtiene_FLGTipoCambio(ByVal strCodMonedaFuente As String, ByVal strCodMonedaDestino As String) As String
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim lstrValor As String = ""
        Dim lstrError As String = ""

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_Tipo_Cambio_Tipo_Cambio", Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Tipo_Cambio_Tipo_Cambio"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pCodMonedaFuente", SqlDbType.VarChar, 3).Value = IIf(strCodMonedaFuente = "", DBNull.Value, strCodMonedaFuente)
            SQLCommand.Parameters.Add("pCodMonedaDestino", SqlDbType.VarChar, 3).Value = IIf(strCodMonedaDestino = "", DBNull.Value, strCodMonedaDestino)

            Dim oParamSal As New SqlClient.SqlParameter("pTipoCambio", SqlDbType.Char, 1)
            oParamSal.Direction = ParameterDirection.Output
            SQLCommand.Parameters.Add(oParamSal)

            SQLCommand.ExecuteNonQuery()

            lstrValor = SQLCommand.Parameters("pTipoCambio").Value.ToString.Trim

        Catch ex As Exception
            lstrError = ex.Message
        Finally
            Connect.Cerrar()
            Obtiene_FLGTipoCambio = lstrValor
            If lstrError <> "" Then
                Call gFun_ResultadoMsg("1|Error al obtener el flag del tipo de cambio " & vbCr & "[" & lstrError & "].", Obtiene_FLGTipoCambio)
            End If
        End Try

    End Function
    Public Function TipoCambio_Mantenedor(ByVal strAccion As String, _
                                          ByRef dblId_TipoCambio As String, _
                                          ByVal strDescripcion As String, _
                                          ByVal strFlg_TipoCambio As String, _
                                          ByVal strAbreviatura As String, _
                                          ByVal strMoneda_Fuente As String, _
                                          Optional ByVal strMoneda_Destino As String = "") As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_TipoCambio_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_TipoCambio_Mantencion"
            SQLCommand.Parameters.Clear()


            '@pAccion varchar(10)
            '@pId_Tipo_Cambio numeric(6,0)
            '@pDsc_Tipo_Cambio varchar(120),
            '@pFlg_Tipo_Cambio varchar(1),
            '@pAbr_Tipo_Cambio varchar(10),
            '@pCod_Moneda_Fuente varchar(3),
            '@pCod_Moneda_Destino varchar(3)
            '@pResultado varchar(200) OUTPUT)



            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 10).Value = strAccion
            'SQLCommand.Parameters.Add("pId_Tipo_Cambio", SqlDbType.Float, 6).Value = dblId_TipoCambio

            '...Resultado
            Dim ParametroSal0 As New SqlClient.SqlParameter("pId_Tipo_Cambio", SqlDbType.Float, 10)
            ParametroSal0.Direction = Data.ParameterDirection.InputOutput
            ParametroSal0.Value = IIf(dblId_TipoCambio = "", DBNull.Value, dblId_TipoCambio)
            SQLCommand.Parameters.Add(ParametroSal0)

            SQLCommand.Parameters.Add("pDsc_Tipo_Cambio", SqlDbType.VarChar, 120).Value = strDescripcion
            SQLCommand.Parameters.Add("pFlg_Tipo_Cambio", SqlDbType.VarChar, 1).Value = strFlg_TipoCambio
            SQLCommand.Parameters.Add("pAbr_Tipo_Cambio", SqlDbType.VarChar, 10).Value = strAbreviatura
            SQLCommand.Parameters.Add("pCod_Moneda_Fuente", SqlDbType.VarChar, 3).Value = strMoneda_Fuente
            SQLCommand.Parameters.Add("pCod_Moneda_Destino", SqlDbType.VarChar, 3).Value = strMoneda_Destino

            ''...(Identity)
            'Dim ParametroSal1 As New SqlClient.SqlParameter("pIdMoneda", SqlDbType.Float, 10)
            'ParametroSal1.Direction = Data.ParameterDirection.Output
            'SQLCommand.Parameters.Add(ParametroSal1)

            '...Resultado
            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            dblId_TipoCambio = "" & SQLCommand.Parameters("pId_Tipo_Cambio").Value

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            TipoCambio_Mantenedor = strDescError
        End Try
    End Function

    Public Function Buscar_X_Moneda(ByVal strCodMoneda As String _
                                  , ByVal dtFecha As Date _
                                  , ByRef strRetorno As String _
                                  , Optional ByVal strColumnas As String = "" _
                                  , Optional ByVal strCodMoneDest As String = "") As DataTable

        Dim ldsTabla As New DataSet
        Dim ldsTabla_Aux As New DataSet
        Dim lArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim lInt_Col As Integer = 0
        Dim lInt_NomCol As String = ""
        Dim lStr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "RCP_TIPOCAMBIO_BUSCAR_X_MONEDA"
        lStr_NombreTabla = "TABLA"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            '( @PCOD_MONEDA  VARCHAR(10)
            ', @PFECHA       VARCHAR(10)

            SQLCommand.Parameters.Add("@PCOD_MONEDA", SqlDbType.VarChar, 10).Value = strCodMoneda
            SQLCommand.Parameters.Add("@PFECHA", SqlDbType.VarChar, 10).Value = Format(dtFecha, "yyyyMMdd")
            SQLCommand.Parameters.Add("@PCODMONEDEST", SqlDbType.VarChar, 10).Value = IIf(strCodMoneDest = "", DBNull.Value, strCodMoneDest)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(ldsTabla, lStr_NombreTabla)

            ldsTabla_Aux = ldsTabla

            If strColumnas.Trim = "" Then
                ldsTabla_Aux = ldsTabla
            Else
                ldsTabla_Aux = ldsTabla.Copy
                For Each Columna In ldsTabla.Tables(lStr_NombreTabla).Columns
                    Remove = True
                    For lInt_Col = 0 To lArr_NombreColumna.Length - 1
                        lInt_NomCol = lArr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = lInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        ldsTabla_Aux.Tables(lStr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For lInt_Col = 0 To lArr_NombreColumna.Length - 1
                lInt_NomCol = lArr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                For Each Columna In ldsTabla_Aux.Tables(lStr_NombreTabla).Columns
                    If Columna.ColumnName = lInt_NomCol Then
                        Columna.SetOrdinal(lInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"

            ldsTabla.Dispose()

            Return ldsTabla_Aux.Tables(lStr_NombreTabla)

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function BuscarSinIndice(ByRef strRetorno As String, _
                                    Optional ByVal strColumnas As String = "" _
                                    ) As DataSet

        Dim ldsTipoCambio As New DataSet
        Dim ldsTipoCambio_Aux As New DataSet
        Dim lStr_NombreTabla As String = ""
        Dim LstrProcedimiento As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim lArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim lInt_NomCol As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_TipoCuenta_BuscaSinIndice"
        lStr_NombreTabla = "TipoCambio"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultado.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(ldsTipoCambio, lStr_NombreTabla)

            If strColumnas.Trim = "" Then
                ldsTipoCambio_Aux = ldsTipoCambio
            Else
                ldsTipoCambio_Aux = ldsTipoCambio.Copy
                For Each Columna In ldsTipoCambio.Tables(lStr_NombreTabla).Columns
                    Remove = True
                    For lInt_Col = 0 To lArr_NombreColumna.Length - 1
                        lInt_NomCol = lArr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = lInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        ldsTipoCambio_Aux.Tables(lStr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For lInt_Col = 0 To lArr_NombreColumna.Length - 1
                lInt_NomCol = lArr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                For Each Columna In ldsTipoCambio_Aux.Tables(lStr_NombreTabla).Columns
                    If Columna.ColumnName = lInt_NomCol Then
                        Columna.SetOrdinal(lInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim


            ldsTipoCambio.Dispose()
            Return ldsTipoCambio_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()

        End Try
    End Function

    Public Function AgregaNuevoTipoCambio(ByVal codMoneda As String, ByRef strRetorno As String) As String

        Dim lStr_NombreTabla As String = ""
        Dim LstrProcedimiento As String = ""
        Dim lstrValor As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim MiTransaccionSQL As SqlClient.SqlTransaction
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion

        Dim strDescError As String = ""

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_TipoCambio_Guardar", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_TipoCambio_Guardar"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pCodMoneda", SqlDbType.VarChar, 10).Value = codMoneda

            Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultado.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
                strRetorno = "OK"
            Else
                MiTransaccionSQL.Rollback()
                strRetorno = strDescError
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
            strRetorno = strDescError
        Finally
            SQLConnect.Cerrar()
            AgregaNuevoTipoCambio = strDescError
        End Try
    End Function

End Class
