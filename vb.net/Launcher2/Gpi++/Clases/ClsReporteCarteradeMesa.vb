﻿Public Class ClsReporteCarteradeMesa

    Public Function CarteradeMesa_Buscar(ByVal strFecha As String, _
                                         ByVal dblIdCuenta As Double, _
                                         ByVal lngIdCliente As Long, _
                                         ByVal lngIdGrupo As Long, _
                                         ByVal strCodClase As String, _
                                         ByVal strCodSubClase As String, _
                                         ByVal dblIdInstrumento As Double, _
                                        ByVal strColumnas As String, _
                              ByRef strRetorno As String) As DataSet

        Dim LDS_Resumen As New DataSet
        Dim LDS_Resumen_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Reporte_CarteradeMesa"
        Lstr_NombreTabla = "RESUMEN"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)
            SQLCommand.CommandTimeout = 0
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pFecha", SqlDbType.Char, 10).Value = strFecha
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = IIf(dblIdCuenta = 0, DBNull.Value, dblIdCuenta)
            SQLCommand.Parameters.Add("pIdCliente", SqlDbType.Int).Value = IIf(lngIdCliente = 0, DBNull.Value, lngIdCliente)
            SQLCommand.Parameters.Add("pIdGrupo", SqlDbType.Int).Value = IIf(lngIdGrupo = 0, DBNull.Value, lngIdGrupo)
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Float, 6).Value = glngIdUsuario
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Float, 10).Value = glngNegocioOperacion
            SQLCommand.Parameters.Add("pCodClase", SqlDbType.Char, 15).Value = IIf(strCodClase = "", DBNull.Value, strCodClase)
            SQLCommand.Parameters.Add("pCodSubClase", SqlDbType.Char, 15).Value = IIf(strCodSubClase = "", DBNull.Value, strCodSubClase)
            SQLCommand.Parameters.Add("pIdInstrumento", SqlDbType.Char, 50).Value = IIf(dblIdInstrumento = 0, DBNull.Value, dblIdInstrumento)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Resumen, Lstr_NombreTabla)

            LDS_Resumen_Aux = LDS_Resumen

            If strColumnas.Trim = "" Then
                LDS_Resumen_Aux = LDS_Resumen
            Else
                LDS_Resumen_Aux = LDS_Resumen.Copy
                For Each Columna In LDS_Resumen.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Resumen_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Resumen_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            Return LDS_Resumen_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function
End Class
