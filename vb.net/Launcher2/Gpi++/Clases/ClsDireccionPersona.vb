﻿Public Class ClsDireccionPersona
    Private ID_DIRECCION As Long
    Private ID_PERSONA As Long
    Private COD_TIPO_DIRECCION As String
    Private DIRECCION As String
    Private FONO As String
    Private FAX As String
    Private ID_COMUNA_CIUDAD As Long


    Public Function Direccion_Ver(ByVal strIdPersona As String, _
                                 ByVal strColumnas As String, _
                                 ByRef strRetorno As String) As DataSet

        Dim LDS_DireccionPersona As New DataSet
        Dim LDS_DireccionPersona_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento
        Dim strDescError As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "[Rcp_DireccionPersona_Buscar]"
        Lstr_NombreTabla = "DIRECCION_PERSONA"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdPersona", SqlDbType.VarChar, 12).Value = IIf(strIdPersona = "", DBNull.Value, strIdPersona)

            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()
            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_DireccionPersona, Lstr_NombreTabla)

            LDS_DireccionPersona_Aux = LDS_DireccionPersona

            If strDescError.ToUpper.Trim <> "OK" Then
                gFun_ResultadoMsg("2|" & strDescError, "Buscar Personas")
                strRetorno = "OK"
                LDS_DireccionPersona_Aux = Nothing
            Else
                If strColumnas.Trim = "" Then
                    LDS_DireccionPersona_Aux = LDS_DireccionPersona
                Else
                    LDS_DireccionPersona_Aux = LDS_DireccionPersona.Copy
                    For Each Columna In LDS_DireccionPersona.Tables(Lstr_NombreTabla).Columns
                        Remove = True
                        For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                            LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                            If Columna.ColumnName = LInt_NomCol Then
                                Remove = False
                            End If
                        Next
                        If Remove Then
                            LDS_DireccionPersona_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                        End If
                    Next
                End If

                For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                    LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                    For Each Columna In LDS_DireccionPersona_Aux.Tables(Lstr_NombreTabla).Columns
                        If Columna.ColumnName = LInt_NomCol Then
                            Columna.SetOrdinal(LInt_Col)
                            Exit For
                        End If
                    Next
                Next

                strRetorno = "OK"
            End If
            Return LDS_DireccionPersona_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function DireccionPersona_Mantenedor(ByVal strAccion As String, ByVal strIdDireccion As String, ByVal strIdPersona As String, ByVal strCodTipoDireccion As String, ByVal strDireccion As String, ByVal strFono As String, ByVal strFax As String, ByVal strIdComunaCiudad As String) As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_Direccion_Mantencion2", MiTransaccionSQL.Connection, MiTransaccionSQL)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Direccion_Mantencion2"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 10).Value = strAccion
            SQLCommand.Parameters.Add("PId_Direccion", SqlDbType.Float, 10).Value = IIf(strIdDireccion = "", DBNull.Value, IIf(strIdDireccion <> "-1", CLng("0" & strIdDireccion), -1))
            SQLCommand.Parameters.Add("PId_Persona", SqlDbType.Float, 10).Value = IIf(strIdPersona = "", DBNull.Value, IIf(strIdPersona <> "-1", CLng("0" & strIdPersona), -1))
            SQLCommand.Parameters.Add("PCod_Tipo_Direccion", SqlDbType.VarChar, 30).Value = strCodTipoDireccion
            SQLCommand.Parameters.Add("PDireccion", SqlDbType.VarChar, 150).Value = strDireccion
            SQLCommand.Parameters.Add("PFono", SqlDbType.VarChar, 50).Value = strFono
            SQLCommand.Parameters.Add("PFax", SqlDbType.VarChar, 50).Value = strFax
            SQLCommand.Parameters.Add("PId_Comuna_Ciudad", SqlDbType.Float, 18).Value = IIf(strIdComunaCiudad = "", DBNull.Value, IIf(strIdComunaCiudad <> "-1", CLng("0" & strIdComunaCiudad), -1))


            'MessageBox.Show(strAccion & "::" & strIdDireccion & "::" & strIdPersona & "::" & strCodTipoDireccion & "::" & strDireccion & "::" & strFono & "::" & strFax & "::" & strIdComunaCiudad)

            '...Resultado
            Dim pSalInstPort As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalInstPort.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalInstPort)

            SQLCommand.ExecuteNonQuery()
            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            DireccionPersona_Mantenedor = strDescError
        End Try
    End Function

    Public Function DireccionPersonas_Mantenedor(ByVal strAccion As String, ByVal DS_DireccionPersona As DataSet) As String

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        Dim strResultado As String = "OK"

        Dim strIdDireccion As String = ""
        Dim strIdPersona As String = ""
        Dim strCodTipoDireccion As String = ""
        Dim strDireccion As String = ""
        Dim strFono As String = ""
        Dim strFax As String = ""
        Dim strIdComunaCiudad As String = ""

        Dim fila As DataRow

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            For Each fila In DS_DireccionPersona.Tables(0).Rows
                strIdDireccion = IIf(IsDBNull(fila.Item("ID_DIRECCION")), fila.Item("ID_DIRECCION"), "")
                strIdPersona = IIf(IsDBNull(fila.Item("ID_PERSONA")), fila.Item("ID_PERSONA"), "")
                strCodTipoDireccion = IIf(IsDBNull(fila.Item("COD_TIPO_DIRECCION")), fila.Item(""), "")
                strDireccion = IIf(IsDBNull(fila.Item("DIRECCION")), fila.Item(""), "")
                strFono = IIf(IsDBNull(fila.Item("FONO")), fila.Item(""), "")
                strFax = IIf(IsDBNull(fila.Item("FAX")), fila.Item(""), "")
                strIdComunaCiudad = IIf(IsDBNull(fila.Item("ID_COMUNA_CIUDAD")), fila.Item(""), "")
                strResultado = DireccionPersona_Mantenedor(strAccion, strIdDireccion, strIdPersona, strCodTipoDireccion, strDireccion, strFono, strFax, strIdComunaCiudad)
                If strResultado <> "OK" Then
                    Exit For
                End If
            Next

            If strResultado.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If
        Catch Ex As Exception

            MiTransaccionSQL.Rollback()
            strResultado = "Error " & vbCr & Ex.Message

        Finally
            SQLConnect.Cerrar()
        End Try

        Return strResultado

    End Function

    Public Function DireccionIncluidas_Ver(ByVal strIdDireccion As String, _
                                     ByVal strIdPersona As String, _
                                     ByVal strIdCuenta As String, _
                                     ByVal strColumnas As String, _
                                     ByRef strRetorno As String) As DataSet

        Dim LDS_DireccionCliente As New DataSet
        Dim LDS_DireccionCliente_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = "Incluidas"
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = "Rcp_DireccionIncluidaACuenta_Consultar"

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("pIdDireccion", SqlDbType.VarChar, 9).Value = IIf(strIdDireccion.Trim = "", DBNull.Value, strIdDireccion.Trim)
            SQLCommand.Parameters.Add("pIdPersona", SqlDbType.VarChar, 9).Value = IIf(strIdPersona.Trim = "", DBNull.Value, strIdPersona.Trim)
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.VarChar, 9).Value = IIf(strIdCuenta.Trim = "", DBNull.Value, strIdCuenta.Trim)


            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_DireccionCliente, Lstr_NombreTabla)


            LDS_DireccionCliente_Aux = LDS_DireccionCliente

            If strColumnas.Trim = "" Then
                LDS_DireccionCliente_Aux = LDS_DireccionCliente
            Else
                LDS_DireccionCliente_Aux = LDS_DireccionCliente.Copy
                For Each Columna In LDS_DireccionCliente.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_DireccionCliente_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_DireccionCliente_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_DireccionCliente.Dispose()
            Return LDS_DireccionCliente_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function DireccionIncluidaXCuenta_Mantenedor(ByVal strId_Cuenta As String, ByRef dstDireccionIncluidaXCuenta As DataSet) As String

        Dim strDescError As String = "OK"
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try

            '       ELIMINAMOS LAS DIRECCION  X CUENTA YA EXISTENTES

            SQLCommand = New SqlClient.SqlCommand("Rcp_DireccionCuenta_MantencionPorCuenta", MiTransaccionSQL.Connection, MiTransaccionSQL)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_DireccionCuenta_MantencionPorCuenta"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = "ELIMINAR"
            SQLCommand.Parameters.Add("pIdDireccion", SqlDbType.Float, 6).Value = DBNull.Value
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = strId_Cuenta
            SQLCommand.Parameters.Add("pObservacion", SqlDbType.VarChar, 180).Value = DBNull.Value
            '...Resultado
            Dim pSalElimIntPort As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalElimIntPort.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalElimIntPort)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            If Not strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Rollback()
                Exit Function
            End If

            '       INSERTAMOS LAS NUEVAS DIRECCION X CUENTA 

            If dstDireccionIncluidaXCuenta.Tables(0).Rows.Count <> Nothing Then
                For Each pRow As DataRow In dstDireccionIncluidaXCuenta.Tables(0).Rows
                    If (pRow("ASOCIADA").ToString = "1") Then
                        SQLCommand = New SqlClient.SqlCommand("Rcp_DireccionCuenta_MantencionPorCuenta", MiTransaccionSQL.Connection, MiTransaccionSQL)
                        SQLCommand.CommandType = CommandType.StoredProcedure
                        SQLCommand.CommandText = "Rcp_DireccionCuenta_MantencionPorCuenta"
                        SQLCommand.Parameters.Clear()

                        SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = "INSERTAR"
                        SQLCommand.Parameters.Add("pIdDireccion", SqlDbType.Float, 6).Value = pRow("ID_DIRECCION").ToString
                        SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = strId_Cuenta
                        SQLCommand.Parameters.Add("pObservacion", SqlDbType.VarChar, 180).Value = pRow("OBSERVACION").ToString.ToUpper

                        '...Resultado
                        Dim pSalInstPort As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                        pSalInstPort.Direction = Data.ParameterDirection.Output
                        SQLCommand.Parameters.Add(pSalInstPort)

                        SQLCommand.ExecuteNonQuery()

                        strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
                    End If
                    If Not strDescError.ToUpper.Trim = "OK" Then
                        Exit For
                    End If
                Next
            End If

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            DireccionIncluidaXCuenta_Mantenedor = strDescError
        End Try
    End Function

End Class
