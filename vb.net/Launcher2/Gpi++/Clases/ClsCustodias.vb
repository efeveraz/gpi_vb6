﻿Imports System.Data.SqlClient
Imports System.Data

Public Class ClsCustodias

    Public Function TraerMovimientosCustodias(ByVal strClaseInstrumento As String, _
                                                ByVal strSubClaseInstrumento As String, _
                                                ByVal lngIdCuenta As Long, _
                                                ByVal strFechadesde As String, _
                                                ByVal strFechaHasta As String, _
                                                ByVal lngNemotecnico As Long, _
                                                ByVal StrNemo As String, _
                                                ByRef strRetorno As String) As DataSet

        Dim LDS_MovimientosCustodia As New DataSet
        Dim LDS_Cartera_Aux As New DataSet
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim LstrProcedimiento As String
        Dim Lstr_NombreTabla As String

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Movimiento_Custodia"
        Lstr_NombreTabla = "Movimientos_Custodia"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.CommandTimeout = 0

            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pCodClaseInstrumento", SqlDbType.VarChar, 15).Value = IIf(strClaseInstrumento = "", DBNull.Value, strClaseInstrumento)
            SQLCommand.Parameters.Add("pCodSubClaseInstrumento", SqlDbType.VarChar, 10).Value = IIf(strSubClaseInstrumento = "", DBNull.Value, strSubClaseInstrumento)
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = IIf(lngIdCuenta = 0, DBNull.Value, lngIdCuenta)
            SQLCommand.Parameters.Add("pFechaDesde", SqlDbType.Char, 10).Value = strFechadesde
            SQLCommand.Parameters.Add("pFechaHasta", SqlDbType.Char, 10).Value = strFechaHasta
            SQLCommand.Parameters.Add("pIdInstrumento", SqlDbType.Float, 10).Value = IIf(lngNemotecnico = 0, DBNull.Value, lngNemotecnico)
            SQLCommand.Parameters.Add("pNemo", SqlDbType.Char, 50).Value = IIf(StrNemo = "", DBNull.Value, StrNemo)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_MovimientosCustodia, Lstr_NombreTabla)

            LDS_Cartera_Aux = LDS_MovimientosCustodia

            strRetorno = "OK"
            Return LDS_Cartera_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function
    Public Function TraerMovimientosCustodiasValorizadas(ByVal strClaseInstrumento As String, _
                                                ByVal lngIdCuenta As Long, _
                                                ByVal strFechadesde As String, _
                                                ByVal StrNemo As String, _
                                                ByRef strRetorno As String) As DataSet

        Dim LDS_MovimientosCustodia As New DataSet
        Dim LDS_Cartera_Aux As New DataSet
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim LstrProcedimiento As String
        Dim Lstr_NombreTabla As String

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Cartera_Buscar_al_Cierre"
        Lstr_NombreTabla = "Movimientos_Custodia_Valorizada"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pCodClaseInstrumento", SqlDbType.VarChar, 15).Value = IIf(strClaseInstrumento = "", DBNull.Value, strClaseInstrumento)
            'SQLCommand.Parameters.Add("pCodSubClaseInstrumento", SqlDbType.VarChar, 10).Value = IIf(strSubClaseInstrumento = "", DBNull.Value, strSubClaseInstrumento)
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = IIf(lngIdCuenta = 0, DBNull.Value, lngIdCuenta)
            SQLCommand.Parameters.Add("pFechaConsulta", SqlDbType.Char, 10).Value = strFechadesde
            'SQLCommand.Parameters.Add("pFechaHasta", SqlDbType.Char, 10).Value = strFechaHasta
            'SQLCommand.Parameters.Add("pIdInstrumento", SqlDbType.Float, 10).Value = IIf(lngNemotecnico = 0, DBNull.Value, lngNemotecnico)
            SQLCommand.Parameters.Add("pNemo", SqlDbType.Char, 50).Value = IIf(StrNemo = "", DBNull.Value, StrNemo)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_MovimientosCustodia, Lstr_NombreTabla)

            LDS_Cartera_Aux = LDS_MovimientosCustodia

            strRetorno = "OK"
            Return LDS_Cartera_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function
    Public Function CartolaMovimientosSaldos(ByVal strFechadesde As String, _
                                                ByVal strFechaHasta As String, _
                                                ByVal dblIdCuenta As Double, _
                                                ByVal dblIdInstrumento As Double, _
                                                ByVal strClaseInstrumento As String, _
                                                ByVal strSubClaseInstrumento As String, _
                                                ByVal StrNemo As String, _
                                                ByRef strRetorno As String) As DataSet

        Dim LDS_CartolaMS As New DataSet
        Dim LDS_CartolaMS_Aux As New DataSet
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim LstrProcedimiento As String
        Dim Lstr_NombreTabla As String

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Cartola_MovtosSaldosCustodia"
        Lstr_NombreTabla = "CARTOLAMOVIMIENTOSSALDOS"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pFechaDesde", SqlDbType.Char, 10).Value = strFechadesde
            SQLCommand.Parameters.Add("pFechaHasta", SqlDbType.Char, 10).Value = strFechaHasta
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = dblIdCuenta
            SQLCommand.Parameters.Add("pIdInstrumento", SqlDbType.Float, 10).Value = IIf(dblIdInstrumento = 0, DBNull.Value, dblIdInstrumento)
            SQLCommand.Parameters.Add("pCodClaseInstrumento", SqlDbType.VarChar, 15).Value = IIf(strClaseInstrumento = "", DBNull.Value, strClaseInstrumento)
            SQLCommand.Parameters.Add("pCodSubClaseInstrumento", SqlDbType.VarChar, 10).Value = IIf(strSubClaseInstrumento = "", DBNull.Value, strSubClaseInstrumento)
            SQLCommand.Parameters.Add("pNemo", SqlDbType.Char, 50).Value = IIf(StrNemo = "", DBNull.Value, StrNemo)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_CartolaMS, Lstr_NombreTabla)

            LDS_CartolaMS_Aux = LDS_CartolaMS

            strRetorno = "OK"
            Return LDS_CartolaMS_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function
    Public Function TraerMovimientosActivos(ByVal strFecha As String, _
                                            ByVal lngIdCuenta As Long, _
                                            ByVal strClaseInstrumento As String, _
                                            ByVal strSubClaseInstrumento As String, _
                                            ByVal lngIntrumento As Long, _
                                            ByVal lngAsesor As Long, _
                                            ByRef strRetorno As String) As DataSet

        Dim LDS_MovimientosCustodia As New DataSet
        Dim LDS_Cartera_Aux As New DataSet
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim LstrProcedimiento As String
        Dim Lstr_NombreTabla As String

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Cartera_Activos"
        Lstr_NombreTabla = "Cartera"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pFechaConsulta", SqlDbType.Char, 10).Value = strFecha
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = IIf(lngIdCuenta = 0, DBNull.Value, lngIdCuenta)
            SQLCommand.Parameters.Add("pCodClaseInstrumento", SqlDbType.VarChar, 15).Value = IIf(strClaseInstrumento = "", DBNull.Value, strClaseInstrumento)
            SQLCommand.Parameters.Add("pCodSubClaseInstrumento", SqlDbType.VarChar, 10).Value = IIf(strSubClaseInstrumento = "", DBNull.Value, strSubClaseInstrumento)
            SQLCommand.Parameters.Add("pCodInstrumento", SqlDbType.Float, 10).Value = IIf(lngIntrumento = 0, DBNull.Value, lngIntrumento)
            SQLCommand.Parameters.Add("pCodAsesor", SqlDbType.Float, 4).Value = IIf(lngAsesor = 0, DBNull.Value, lngAsesor)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_MovimientosCustodia, Lstr_NombreTabla)

            LDS_Cartera_Aux = LDS_MovimientosCustodia

            strRetorno = "OK"
            Return LDS_Cartera_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function


    Public Function TraerMovimientosCustodiasHistorica(ByVal lngIdCuenta As Long, _
                                                       ByVal lngNemotecnico As Long, _
                                                       ByVal StrTipoConsulta As String, _
                                                       ByVal StrFechaCartera As String, _
                                                       ByRef strRetorno As String) As DataSet

        Dim LDS_MovimientosCustodia As New DataSet
        Dim LDS_Cartera_Aux As New DataSet
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim LstrProcedimiento As String
        Dim Lstr_NombreTabla As String

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Custodia_Historica_Buscar"
        Lstr_NombreTabla = "Movimientos_Custodia_Historica"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = IIf(lngIdCuenta = 0, DBNull.Value, lngIdCuenta)
            SQLCommand.Parameters.Add("pIdInstrumento", SqlDbType.Float, 10).Value = IIf(lngNemotecnico = 0, DBNull.Value, lngNemotecnico)
            SQLCommand.Parameters.Add("pTipoSalida", SqlDbType.Char, 1).Value = IIf(StrTipoConsulta = "", DBNull.Value, StrTipoConsulta)
            SQLCommand.Parameters.Add("pFecha", SqlDbType.Char, 10).Value = StrFechaCartera

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_MovimientosCustodia, Lstr_NombreTabla)

            LDS_Cartera_Aux = LDS_MovimientosCustodia

            strRetorno = "OK"
            Return LDS_Cartera_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function


    Public Sub Graba_Movimientos_Custodia_Historica(ByVal strMovCustodia As String, _
                                                    ByVal Linstrumento As Double, _
                                                    ByRef Reg_Leidos As Integer, _
                                                    ByRef Reg_Grabados As Integer, _
                                                    ByRef strRetorno As String)
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim lstrProc As String = "Rcp_MovCustodia_Historica_Mantencion"

        'Abre la conexion
        Connect.Abrir()

        Dim MiTransaccion As SqlClient.SqlTransaction

        Reg_Leidos = 0
        Reg_Grabados = 0

        MiTransaccion = Connect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand(lstrProc, MiTransaccion.Connection, MiTransaccion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = lstrProc

            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pMovCustodia ", SqlDbType.Text).Value = strMovCustodia
            SQLCommand.Parameters.Add("pIdInstrumento", SqlDbType.Float, 10).Value = IIf(Linstrumento = 0, DBNull.Value, Linstrumento)

            Dim pReg_Leidos As New SqlClient.SqlParameter("pReg_Leidos", SqlDbType.Int, 10)
            pReg_Leidos.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pReg_Leidos).Value = Reg_Leidos

            Dim pReg_Grabados As New SqlClient.SqlParameter("pReg_Grabados", SqlDbType.Int, 10)
            pReg_Grabados.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pReg_Grabados).Value = Reg_Grabados

            Dim ParametroSal2 As New SqlClient.SqlParameter("pError", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()

            Reg_Leidos = SQLCommand.Parameters("pReg_Leidos").Value
            Reg_Grabados = SQLCommand.Parameters("pReg_Grabados").Value
            strRetorno = SQLCommand.Parameters("pError").Value.ToString.Trim


            If Trim(strRetorno) = "OK" Then
                MiTransaccion.Commit()
            Else
                MiTransaccion.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccion.Rollback()
            strRetorno = "Error al grabar Aportes y Retiros Programados." & vbCr & Ex.Message
        Finally
            Connect.Cerrar()
        End Try
    End Sub


End Class
