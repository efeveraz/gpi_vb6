﻿'/*-------------------------------------------------------------------------------------------------
'SOURCESAFE Information:
'    $Workfile: ClsReportedeFlujos.vb $
'    $Author: Raguirre $
'    $Date: 16-02-12 9:45 $
'    $Revision: 5 $
'-------------------------------------------------------------------------------------------------*/
Public Class ClsReportedeFlujos

    Public Function PeriodosBuscar(ByVal strColumnas As String, _
                               ByRef strRetorno As String) As DataSet

        Dim LDS_Periodos As New DataSet
        Dim LDS_Periodos_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Reporte_Flujo_Cajas_Periodos_Buscar"
        Lstr_NombreTabla = "PERIODOS"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            'SQLCommand.Parameters.Add("", SqlDbType.Float, 10).Value = 0

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Periodos, Lstr_NombreTabla)

            LDS_Periodos_Aux = LDS_Periodos

            If strColumnas.Trim = "" Then
                LDS_Periodos_Aux = LDS_Periodos
            Else
                LDS_Periodos_Aux = LDS_Periodos.Copy
                For Each Columna In LDS_Periodos.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Periodos_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Periodos_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            Return LDS_Periodos_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function ResumenBuscar(ByVal strFechaDesde As String, _
                                  ByVal strFechaHasta As String, _
                                  ByVal strMoneda As String, _
                                  ByVal dblIdUsuario As Double, _
                                  ByVal dblIdCuenta As Double, _
                                  ByVal dblIdCliente As Double, _
                                  ByVal dblIdGrupo As Double, _
                                  ByVal dblIdAsesor As Double, _
                                  ByVal strMercado As String, _
                                  ByVal dblIdPerfil As Double, _
                                  ByVal strTipoAdmin As String, _
                                  ByVal strColumnas As String, _
                                  ByRef strRetorno As String) As DataSet

        Dim LDS_Resumen As New DataSet
        Dim LDS_Resumen_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Reporte_Flujo_Cajas_Resumen_Buscar"
        Lstr_NombreTabla = "RESUMEN"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pFechaDesde", SqlDbType.Char, 10).Value = strFechaDesde
            SQLCommand.Parameters.Add("pFechaHasta", SqlDbType.Char, 10).Value = strFechaHasta
            SQLCommand.Parameters.Add("pCodMoneda", SqlDbType.Char, 3).Value = IIf(strMoneda = "TOD", DBNull.Value, strMoneda)
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Float, 10).Value = dblIdUsuario
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Int).Value = glngNegocioOperacion
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Int).Value = dblIdCuenta
            SQLCommand.Parameters.Add("pIdCliente", SqlDbType.Int).Value = dblIdCliente
            SQLCommand.Parameters.Add("pIdGrupo", SqlDbType.Int).Value = dblIdGrupo
            'SQLCommand.Parameters.Add("pIdAsesor", SqlDbType.Int).Value = IIf(dblIdAsesor = 0, 0, dblIdAsesor)
            'SQLCommand.Parameters.Add("pCodMercado", SqlDbType.Char, 3).Value = IIf(strMercado = "TOD", "", strMercado)
            'SQLCommand.Parameters.Add("pIdPerfilRiesgo", SqlDbType.Int).Value = IIf(dblIdPerfil = 0, 0, dblIdPerfil)
            'SQLCommand.Parameters.Add("pCodTipoAdmin", SqlDbType.Char, 3).Value = IIf(strTipoAdmin = "TOD", "", strTipoAdmin)
            SQLCommand.Parameters.Add("pIdAsesor", SqlDbType.Int).Value = IIf(dblIdAsesor = 0, DBNull.Value, dblIdAsesor)
            SQLCommand.Parameters.Add("pCodMercado", SqlDbType.Char, 3).Value = IIf(strMercado = "TOD", DBNull.Value, strMercado)
            SQLCommand.Parameters.Add("pIdPerfilRiesgo", SqlDbType.Int).Value = IIf(dblIdPerfil = 0, DBNull.Value, dblIdPerfil)
            SQLCommand.Parameters.Add("pCodTipoAdmin", SqlDbType.Char, 3).Value = IIf(strTipoAdmin = "TOD", DBNull.Value, strTipoAdmin)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Resumen, Lstr_NombreTabla)

            LDS_Resumen_Aux = LDS_Resumen

            If strColumnas.Trim = "" Then
                LDS_Resumen_Aux = LDS_Resumen
            Else
                LDS_Resumen_Aux = LDS_Resumen.Copy
                For Each Columna In LDS_Resumen.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Resumen_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Resumen_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            Return LDS_Resumen_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function DetalleBuscar(ByVal strFechaDesde As String, _
                                  ByVal strFechaHasta As String, _
                                  ByVal strMoneda As String, _
                                  ByVal dblIdCuenta As Double, _
                                  ByVal strColumnas As String, _
                                  ByRef strRetorno As String) As DataSet

        Dim LDS_Detalle As New DataSet
        Dim LDS_Detalle_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Reporte_Flujo_Cajas_Detalle_Buscar"
        Lstr_NombreTabla = "DETALLE"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pFechaDesde", SqlDbType.Char, 10).Value = strFechaDesde
            SQLCommand.Parameters.Add("pFechaHasta", SqlDbType.Char, 10).Value = strFechaHasta
            SQLCommand.Parameters.Add("pCodMoneda", SqlDbType.Char, 3).Value = strMoneda
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = dblIdCuenta
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Int, 1).Value = glngNegocioOperacion

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Detalle, Lstr_NombreTabla)

            LDS_Detalle_Aux = LDS_Detalle

            If strColumnas.Trim = "" Then
                LDS_Detalle_Aux = LDS_Detalle
            Else
                LDS_Detalle_Aux = LDS_Detalle.Copy
                For Each Columna In LDS_Detalle.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Detalle_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Detalle_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            Return LDS_Detalle_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function Grupo_Cuentas_Buscar(ByVal strFechaDesde As String, _
                                 ByVal strFechaHasta As String, _
                                 ByVal strMoneda As String, _
                                 ByVal dblIdUsuario As Double, _
                                 ByVal strColumnas As String, _
                                 ByRef strRetorno As String) As DataSet

        Dim LDS_Resumen As New DataSet
        Dim LDS_Resumen_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Reporte_Flujo_Cajas_GrupoXCuentas_Buscar"
        Lstr_NombreTabla = "GrupoCuenta"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pFechaDesde", SqlDbType.Char, 10).Value = strFechaDesde
            SQLCommand.Parameters.Add("pFechaHasta", SqlDbType.Char, 10).Value = strFechaHasta
            SQLCommand.Parameters.Add("pCodMoneda", SqlDbType.Char, 3).Value = strMoneda
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Float, 10).Value = dblIdUsuario

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Resumen, Lstr_NombreTabla)

            LDS_Resumen_Aux = LDS_Resumen

            If strColumnas.Trim = "" Then
                LDS_Resumen_Aux = LDS_Resumen
            Else
                LDS_Resumen_Aux = LDS_Resumen.Copy
                For Each Columna In LDS_Resumen.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Resumen_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Resumen_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            Return LDS_Resumen_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function
End Class
