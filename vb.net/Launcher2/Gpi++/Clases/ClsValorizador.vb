﻿Imports System.Data.SqlClient

Public Class ClsValorizador
    Public Sub ValorizarInstrumento(ByVal strSubClaseInstrumento As String, _
                                    ByVal strNemotecnico As String, _
                                    ByVal strFechaValoriza As String, _
                                    ByVal strQueryNemo As String, _
                                    ByVal strQueryValo As String, _
                                    ByVal strCalcula As String, _
                                    ByVal dblNominales As Double, _
                                    ByRef dblPrecioTasa As Double, _
                                    Optional ByRef dblValorPar As Double = 0, _
                                    Optional ByRef dblValorPresente As Double = 0, _
                                    Optional ByRef dblValorUm As Double = 0, _
                                    Optional ByRef dblValorMonedaLocal As Double = 0, _
                                    Optional ByRef dblValorMonedaTrans As Double = 0, _
                                    Optional ByRef dblNumeroActualCupon As Double = 0, _
                                    Optional ByRef dblNumeroProximoCupon As Double = 0, _
                                    Optional ByRef strFechaActualCupon As String = "", _
                                    Optional ByRef strFechaProximoCupon As String = "", _
                                    Optional ByRef dblCapitalActualCupon As Double = 0, _
                                    Optional ByRef dblInteresActualCupon As Double = 0, _
                                    Optional ByRef dblCapitalProximoCupon As Double = 0, _
                                    Optional ByRef dblInteresProximoCupon As Double = 0, _
                                    Optional ByRef dblDuracion As Double = 0, _
                                    Optional ByRef strDescError As String = "", _
                                    Optional ByRef strFchVencimiento As String = "", _
                                    Optional ByRef strCodMonedaUM As Object = Nothing, _
                                    Optional ByRef dblAcrow As Double = 0)


        Dim DS_Paso As New DataSet
        Dim lstrProcedimiento As String = ""
        Dim LstrNombreTabla As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion

        'Abre la conexion
        lstrProcedimiento = "Rcp_Valorizar"
        LstrNombreTabla = "PASO"
        Connect.Abrir()

        Try

            SQLCommand = New SqlCommand(lstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = lstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pSubClaseInstrumento", SqlDbType.Char, 10).Value = strSubClaseInstrumento
            SQLCommand.Parameters.Add("pNemotecnico", SqlDbType.VarChar, 50).Value = strNemotecnico
            SQLCommand.Parameters.Add("pFechaValoriza", SqlDbType.Char, 10).Value = strFechaValoriza
            SQLCommand.Parameters.Add("pQuery_Nemo", SqlDbType.Char, 1).Value = strQueryNemo
            SQLCommand.Parameters.Add("pQuery_Valo", SqlDbType.Char, 1).Value = strQueryValo
            SQLCommand.Parameters.Add("pCalcula", SqlDbType.Char, 1).Value = strCalcula
            SQLCommand.Parameters.Add("pNominales", SqlDbType.Float, 18).Value = dblNominales
            SQLCommand.Parameters.Add("pAcrow", SqlDbType.Float, 18).Value = dblAcrow

            Dim ParametroSal1 As New SqlClient.SqlParameter("pPrecioTasa", SqlDbType.Float, 18)
            ParametroSal1.Value = dblPrecioTasa
            ParametroSal1.Direction = ParameterDirection.InputOutput
            SQLCommand.Parameters.Add(ParametroSal1)
            SQLCommand.Parameters("pPrecioTasa").Value = dblPrecioTasa

            Dim ParametroSal2 As New SqlClient.SqlParameter("pPvp", SqlDbType.Float, 15)
            ParametroSal2.Value = dblValorPar
            ParametroSal2.Direction = ParameterDirection.InputOutput
            SQLCommand.Parameters.Add(ParametroSal2)

            Dim ParametroSal3 As New SqlClient.SqlParameter("pValor_Unitario", SqlDbType.Float, 18)
            ParametroSal3.Direction = ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal3)

            Dim ParametroSal4 As New SqlClient.SqlParameter("pValor_Nemo_Um", SqlDbType.Float, 18)
            ParametroSal4.Direction = ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal4)

            Dim ParametroSal5 As New SqlClient.SqlParameter("pValor_Moneda_Local", SqlDbType.Float, 18)
            ParametroSal5.Direction = ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal5)

            Dim ParametroSal6 As New SqlClient.SqlParameter("pValor_Moneda_Trans", SqlDbType.Float, 18)
            ParametroSal6.Direction = ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal6)

            Dim ParametroSal7 As New SqlClient.SqlParameter("pNnumcupact", SqlDbType.Int, 10)
            ParametroSal7.Direction = ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal7)

            Dim ParametroSal8 As New SqlClient.SqlParameter("pNnumcupprx", SqlDbType.Int, 10)
            ParametroSal8.Direction = ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal8)

            Dim ParametroSal9 As New SqlClient.SqlParameter("pFecactcupon", SqlDbType.Char, 10)
            ParametroSal9.Direction = ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal9)

            Dim ParametroSal10 As New SqlClient.SqlParameter("pFecprxcupon", SqlDbType.Char, 10)
            ParametroSal10.Direction = ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal10)

            Dim ParametroSal11 As New SqlClient.SqlParameter("pAmortiza_Act", SqlDbType.Float, 11)
            ParametroSal11.Direction = ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal11)

            Dim ParametroSal12 As New SqlClient.SqlParameter("pInteres_Act", SqlDbType.Float, 11)
            ParametroSal12.Direction = ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal12)

            Dim ParametroSal13 As New SqlClient.SqlParameter("pAmortiza_Prx", SqlDbType.Float, 11)
            ParametroSal13.Direction = ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal13)

            Dim ParametroSal14 As New SqlClient.SqlParameter("pInteres_Prx", SqlDbType.Float, 11)
            ParametroSal14.Direction = ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal14)

            Dim ParametroSal15 As New SqlClient.SqlParameter("pDuration", SqlDbType.Float, 11)
            ParametroSal15.Direction = ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal15)

            Dim ParametroSal16 As New SqlClient.SqlParameter("pMsjRetorno", SqlDbType.VarChar, 255)
            ParametroSal16.Direction = ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal16)

            If Not IsNothing(strFchVencimiento) Then
                SQLCommand.Parameters.Add("PFCH_VENCIMIENTO", SqlDbType.Char, 10).Value = strFchVencimiento
            End If

            If Not IsNothing(strCodMonedaUM) Then
                SQLCommand.Parameters.Add("PCOD_MONEDA_UM", SqlDbType.Char, 18).Value = strCodMonedaUM
            End If

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(DS_Paso, LstrNombreTabla)

            strDescError = SQLCommand.Parameters("pMsjRetorno").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "" Or strDescError.ToUpper.Trim = "OK" Then
                dblPrecioTasa = NVL(SQLCommand.Parameters("pPrecioTasa").Value, 0)
                dblValorPar = NVL(SQLCommand.Parameters("pPvp").Value, 0)
                dblValorPresente = NVL(SQLCommand.Parameters("pValor_Unitario").Value, 0)
                dblValorUm = NVL(SQLCommand.Parameters("pValor_Nemo_Um").Value, 0)
                dblValorMonedaLocal = NVL(SQLCommand.Parameters("pValor_Moneda_Local").Value, 0)
                dblValorMonedaTrans = NVL(SQLCommand.Parameters("pValor_Moneda_Trans").Value, 0)
                dblNumeroActualCupon = NVL(SQLCommand.Parameters("pNnumcupact").Value, 0)
                dblNumeroProximoCupon = NVL(SQLCommand.Parameters("pNnumcupprx").Value, 0)
                strFechaActualCupon = NVL(SQLCommand.Parameters("pFecactcupon").Value, "")
                strFechaProximoCupon = NVL(SQLCommand.Parameters("pFecprxcupon").Value, "")
                dblCapitalActualCupon = NVL(SQLCommand.Parameters("pAmortiza_Act").Value, 0)
                dblInteresActualCupon = NVL(SQLCommand.Parameters("pInteres_Act").Value, 0)
                dblCapitalProximoCupon = NVL(SQLCommand.Parameters("pAmortiza_Prx").Value, 0)
                dblInteresProximoCupon = NVL(SQLCommand.Parameters("pInteres_Prx").Value, 0)
                dblDuracion = NVL(SQLCommand.Parameters("pDuration").Value, 0)
            Else
                dblValorPresente = 0
                dblValorUm = 0
                dblNumeroActualCupon = 0
                dblNumeroProximoCupon = 0
                strFechaActualCupon = gstrFechaSistema
                strFechaProximoCupon = gstrFechaSistema
                dblCapitalActualCupon = 0
                dblInteresActualCupon = 0
                dblCapitalProximoCupon = 0
                dblInteresProximoCupon = 0
                dblValorMonedaLocal = 0
                dblValorMonedaTrans = 0
                dblDuracion = 0
            End If

        Catch Ex As Exception
            strDescError = "Error al Valorizar." & vbCr & Ex.Message
        Finally
            Connect.Cerrar()
        End Try
    End Sub

    Public Function TraerEstados(ByRef strRetorno As String) As DataSet

        Dim LDS_Estados As New DataSet
        Dim LDS_Estados_Aux As New DataSet
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Estado_Buscar"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdTipoEstado", SqlDbType.Float, 10).Value = 3 ' id de tipo estado para nemotecnicos

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Estados)

            LDS_Estados_Aux = LDS_Estados

            strRetorno = "OK"
            Return LDS_Estados_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function


    Public Sub ValorizarDepositos(ByVal strEsPrimeraEmision As String, _
                                ByRef dblCapitalInicial As Double, _
                                ByRef dblTasaEmision As Double, _
                                ByVal intBaseTasa As Integer, _
                                ByVal strFechaEmision As String, _
                                ByVal strFechaVencimiento As String, _
                                ByVal lngPlazo As Long, _
                                ByRef dblValorRescate As Double, _
                                ByRef dblMontoOperacion As Double, _
                                ByRef dblTasaOperacion As Double, _
                                ByVal strFechaOperacion As String, _
                                ByVal lngDias As Long, _
                                ByRef dblValorFinal As Double, _
                                ByRef strDescError As String, _
                        Optional ByVal dblIdInstrumento As Double = 0)

        Dim DS_Paso As New DataSet
        Dim lstrProcedimiento As String = ""
        Dim LstrNombreTabla As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion

        'Abre la conexion
        lstrProcedimiento = "Rcp_ValorizaDepositos"
        LstrNombreTabla = "PASO"
        Connect.Abrir()

        Try

            ' @pEsPrimeraEmision      CHAR(1)        = NULL, 
            ' @pCapitalInicial        NUMERIC(18,4)  = NULL OUTPUT, 
            ' @pTasaEmision           NUMERIC(18,4)  = NULL OUTPUT, 
            ' @pBaseTasa              NUMERIC(10),
            ' @pFechaEmision          CHAR(10), 
            ' @pFechaVencimiento      CHAR(10), 
            ' @pPlazo                 NUMERIC(10)    = NULL, 
            ' @pValorRescate          NUMERIC(18,4)  = NULL OUTPUT, 
            ' @pMontoOperacion        NUMERIC(18,4)  = NULL OUTPUT, 
            ' @pTasaOperacion         NUMERIC(18,4)  = NULL OUTPUT, 
            ' @pFechaOperacion        CHAR(10)       = NULL, 
            ' @pDias                  NUMERIC(10)    = NULL, 
            ' @pValorFinal            NUMERIC(18,4)  = NULL OUTPUT, 
            ' @pMsjRetorno            VARCHAR(255)
            ' @pIdInstrumento         NUMERIC(10)    = NULL, 


            SQLCommand = New SqlCommand(lstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = lstrProcedimiento
            SQLCommand.Parameters.Clear()

            'SQLCommand.Parameters.Add("pEsPrimeraEmision", SqlDbType.Char, 1).Value = strEsPrimeraEmision

            Dim ParametroSal1 As New SqlClient.SqlParameter("pCapitalInicial", SqlDbType.Float, 18)
            ParametroSal1.Value = IIf(dblCapitalInicial = 0, DBNull.Value, dblCapitalInicial)
            ParametroSal1.Direction = ParameterDirection.InputOutput
            SQLCommand.Parameters.Add(ParametroSal1)

            Dim ParametroSal2 As New SqlClient.SqlParameter("pTasaEmision", SqlDbType.Float, 18)
            'ParametroSal2.Value = IIf(dblTasaEmision = 0, DBNull.Value, dblTasaEmision)
            ParametroSal2.Value = dblTasaEmision
            ParametroSal2.Direction = ParameterDirection.InputOutput
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.Parameters.Add("pBaseTasa", SqlDbType.Float, 10).Value = intBaseTasa
            SQLCommand.Parameters.Add("pFechaEmision", SqlDbType.Char, 10).Value = strFechaEmision
            SQLCommand.Parameters.Add("pFechaVencimiento", SqlDbType.Char, 10).Value = strFechaVencimiento
            SQLCommand.Parameters.Add("pPlazo", SqlDbType.Float, 10).Value = IIf(lngPlazo = 0, DBNull.Value, lngPlazo)

            Dim ParametroSal3 As New SqlClient.SqlParameter("pValorRescate", SqlDbType.Float, 18)
            ParametroSal3.Value = IIf(dblValorRescate = 0, DBNull.Value, dblValorRescate)
            ParametroSal3.Direction = ParameterDirection.InputOutput
            SQLCommand.Parameters.Add(ParametroSal3)

            Dim ParametroSal4 As New SqlClient.SqlParameter("pMontoOperacion", SqlDbType.Float, 18)
            ParametroSal4.Value = IIf(dblMontoOperacion = 0, DBNull.Value, dblMontoOperacion)
            ParametroSal4.Direction = ParameterDirection.InputOutput
            SQLCommand.Parameters.Add(ParametroSal4)

            Dim ParametroSal5 As New SqlClient.SqlParameter("pTasaOperacion", SqlDbType.Float, 18)
            ParametroSal5.Value = IIf(dblTasaOperacion = 0, DBNull.Value, dblTasaOperacion)
            ParametroSal5.Direction = ParameterDirection.InputOutput
            SQLCommand.Parameters.Add(ParametroSal5)

            SQLCommand.Parameters.Add("pFechaOperacion", SqlDbType.Char, 10).Value = strFechaOperacion
            SQLCommand.Parameters.Add("pDias", SqlDbType.Float, 10).Value = IIf(lngDias = 0, DBNull.Value, lngDias)

            Dim ParametroSal6 As New SqlClient.SqlParameter("pValorFinal", SqlDbType.Float, 18)
            ParametroSal6.Value = IIf(dblValorFinal = 0, DBNull.Value, dblValorFinal)
            ParametroSal6.Direction = ParameterDirection.InputOutput
            SQLCommand.Parameters.Add(ParametroSal6)

            Dim ParametroSal7 As New SqlClient.SqlParameter("pMsjRetorno", SqlDbType.VarChar, 255)
            ParametroSal7.Direction = ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal7)

            SQLCommand.Parameters.Add("pIdInstrumento", SqlDbType.Float, 10).Value = IIf(dblIdInstrumento = 0, DBNull.Value, dblIdInstrumento)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(DS_Paso, LstrNombreTabla)

            strDescError = SQLCommand.Parameters("pMsjRetorno").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                dblCapitalInicial = IIf(IsDBNull(SQLCommand.Parameters("pCapitalInicial").Value), 0, SQLCommand.Parameters("pCapitalInicial").Value)
                dblTasaEmision = IIf(IsDBNull(SQLCommand.Parameters("pTasaEmision").Value), 0, SQLCommand.Parameters("pTasaEmision").Value)
                dblValorRescate = IIf(IsDBNull(SQLCommand.Parameters("pValorRescate").Value), 0, SQLCommand.Parameters("pValorRescate").Value)

                dblMontoOperacion = IIf(IsDBNull(SQLCommand.Parameters("pMontoOperacion").Value), 0, SQLCommand.Parameters("pMontoOperacion").Value)
                dblTasaOperacion = IIf(IsDBNull(SQLCommand.Parameters("pTasaOperacion").Value), 0, SQLCommand.Parameters("pTasaOperacion").Value)
                dblValorFinal = IIf(IsDBNull(SQLCommand.Parameters("pValorFinal").Value), 0, SQLCommand.Parameters("pValorFinal").Value)

            Else
                dblCapitalInicial = 0
                dblTasaEmision = 0
                dblValorRescate = 0

                dblMontoOperacion = 0
                dblTasaOperacion = 0
                dblValorFinal = 0
            End If

        Catch Ex As Exception
            strDescError = "Error al Valorizar." & vbCr & Ex.Message
        Finally
            Connect.Cerrar()
        End Try
    End Sub
    Public Function ValorizarInstrumentoRF(ByVal strNemotecnico As String, _
                                    ByVal strFechaValoriza As String, _
                                    ByVal dblTir As Double, _
                                    ByVal dblNominales As Double, _
                                    ByVal strFlujoCupones As String, _
                                    ByVal strSalidaQuery As String, _
                                    ByRef strResultado As String) As DataSet

        Dim LDS_Valoriza As New DataSet
        Dim LDS_Valoriza_Aux As New DataSet
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_RutinaValorizacionRF"

        Connect.Abrir()
        strResultado = "OK"
        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pNemotecnico", SqlDbType.VarChar, 25).Value = strNemotecnico
            SQLCommand.Parameters.Add("pFechaValorizacion", SqlDbType.Char, 10).Value = strFechaValoriza
            SQLCommand.Parameters.Add("pTir", SqlDbType.Float, 20).Value = dblTir
            SQLCommand.Parameters.Add("pNominal", SqlDbType.Float, 20).Value = dblNominales
            SQLCommand.Parameters.Add("pFlujoCupones", SqlDbType.Char, 1).Value = IIf(strFlujoCupones = "", "S", strFlujoCupones)
            SQLCommand.Parameters.Add("pSalidaQuery", SqlDbType.Char, 1).Value = IIf(strSalidaQuery = "", "N", strSalidaQuery)

            Dim pSalResultInstr As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultInstr.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultInstr)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Valoriza)

            LDS_Valoriza_Aux = LDS_Valoriza

            strResultado = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            Return LDS_Valoriza_Aux
        Catch Ex As Exception
            strResultado = "Error al Valorizar." & vbCr & Ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function
End Class
