﻿Public Class ClsCustodio


    Public Function Custodio_Ver(ByVal lngIdCustodio As Long, _
                                 ByVal strEstadoCustodio As String, _
                                 ByVal strColumnas As String, _
                                 ByRef strRetorno As String, _
                                 Optional ByVal strCodMercado As String = "", _
                                 Optional ByVal strConDCV As String = "") As DataSet

        Dim LDS_Custodio As New DataSet
        Dim LDS_Custodio_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Custodio_Consultar"
        Lstr_NombreTabla = "CUSTODIO"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdCustodio", SqlDbType.Float, 10).Value = IIf(lngIdCustodio = 0, DBNull.Value, lngIdCustodio)
            SQLCommand.Parameters.Add("pEstCustodio", SqlDbType.VarChar, 15).Value = IIf(strEstadoCustodio.Trim = "", DBNull.Value, strEstadoCustodio.Trim)
            SQLCommand.Parameters.Add("pCodMercado", SqlDbType.Char, 1).Value = IIf(strCodMercado.Trim = "", DBNull.Value, strCodMercado.Trim)
            SQLCommand.Parameters.Add("@pFlgConDCV", SqlDbType.Char, 2).Value = IIf(strConDCV.Trim = "", DBNull.Value, strConDCV.Trim)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Custodio, Lstr_NombreTabla)

            LDS_Custodio_Aux = LDS_Custodio

            If strColumnas.Trim = "" Then
                LDS_Custodio_Aux = LDS_Custodio
            Else
                LDS_Custodio_Aux = LDS_Custodio.Copy
                For Each Columna In LDS_Custodio.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Custodio_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Custodio_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            Return LDS_Custodio_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function
    Public Function Obtiene_IdCustodio_ABR(ByVal strAbrCustodio As String, _
                                           Optional ByVal strEstadoCustodio As String = "") As Long

        Dim LDS_Custodio As New DataSet
        Dim LDS_Custodio_Aux As New DataSet
        Dim Lstr_NombreTabla As String
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Custodio_ID_ABR"
        Lstr_NombreTabla = "CUSTODIO"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAbrCustodio", SqlDbType.VarChar, 30).Value = IIf(strAbrCustodio = "", DBNull.Value, strAbrCustodio)
            SQLCommand.Parameters.Add("pEstCustodio", SqlDbType.VarChar, 3).Value = IIf(strEstadoCustodio.Trim = "", DBNull.Value, strEstadoCustodio.Trim)

            Dim ParametroSal1 As New SqlClient.SqlParameter("pIdCustodio", SqlDbType.Float, 10)
            ParametroSal1.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal1)

            SQLCommand.ExecuteNonQuery()

            Obtiene_IdCustodio_ABR = SQLCommand.Parameters("pIdCustodio").Value.ToString.Trim

        Catch ex As Exception
            Obtiene_IdCustodio_ABR = -1

        Finally
            Connect.Cerrar()
        End Try
    End Function
    Public Function CuentaCustodio_Mantenedor(ByVal lngIdCuenta As Long, ByVal lngIdCustodio As Long) As String
        Dim strDescError As String = "OK"
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try

            SQLCommand = New SqlClient.SqlCommand("Rcp_CuentaCustodio_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_CuentaCustodio_Mantencion"

            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = lngIdCuenta
            SQLCommand.Parameters.Add("pIdCustodio", SqlDbType.Float, 10).Value = lngIdCustodio

            SQLCommand.ExecuteNonQuery()

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en la actualización del custodio por cuenta [" & vbCr & Ex.Message & "]"
        Finally
            If strDescError = "OK" Then
                MiTransaccionSQL.Commit()
            End If
            SQLConnect.Cerrar()
            CuentaCustodio_Mantenedor = strDescError
        End Try
    End Function
    Public Function CuentaCustodio_Ver(ByVal lngIdCuenta As Long) As Long

        Dim LDS_Custodio As New DataSet
        Dim LDS_Custodio_Aux As New DataSet
        Dim Lstr_NombreTabla As String
        Dim LstrProcedimiento
        Dim llngValor As Long = 0

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_CuentaCustodio_Consultar"
        Lstr_NombreTabla = "RELCUENTACUSTODIO"

        Connect.Abrir()
        If lngIdCuenta > 0 Then
            Try
                SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

                SQLCommand.CommandType = CommandType.StoredProcedure
                SQLCommand.CommandText = LstrProcedimiento
                SQLCommand.Parameters.Clear()

                SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = IIf(lngIdCuenta = 0, DBNull.Value, lngIdCuenta)

                SQLDataAdapter.SelectCommand = SQLCommand
                SQLDataAdapter.Fill(LDS_Custodio, Lstr_NombreTabla)

                If LDS_Custodio.Tables("RELCUENTACUSTODIO").Rows.Count > 0 Then
                    llngValor = CLng("0" & LDS_Custodio.Tables("RELCUENTACUSTODIO").Rows(0).Item("ID_CUSTODIO"))
                End If

            Catch ex As Exception
                gFun_ResultadoMsg("1|" & ex.Message, "CuentaCustodio_Ver")

            Finally
                Connect.Cerrar()
                CuentaCustodio_Ver = llngValor
            End Try
        End If
    End Function
    Public Function Custodio_Mantenedor(ByVal strAccion As String, _
                              ByVal IntIdCustodio As String, _
                              ByVal strAbr_Custodio As String, _
                              ByVal strDsc_Custodio As String, _
                              ByVal strCod_Mercado As String, _
                              ByVal strEstado_Custodio As String) As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_Custodio_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Custodio_Mantencion"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 10).Value = strAccion
            SQLCommand.Parameters.Add("pIdCustodio", SqlDbType.VarChar, 10).Value = IIf(IntIdCustodio.Trim = "0", DBNull.Value, IntIdCustodio.Trim)
            SQLCommand.Parameters.Add("pAbr_Custodio", SqlDbType.VarChar, 30).Value = IIf(strAbr_Custodio.Trim = "", DBNull.Value, strAbr_Custodio.Trim)
            SQLCommand.Parameters.Add("pDsc_Custodio", SqlDbType.VarChar, 50).Value = IIf(strDsc_Custodio.Trim = "", DBNull.Value, strDsc_Custodio.Trim)
            SQLCommand.Parameters.Add("pCod_Mercado", SqlDbType.VarChar, 4).Value = IIf(strCod_Mercado.Trim = "", DBNull.Value, strCod_Mercado.Trim)
            SQLCommand.Parameters.Add("pEstado_Custodio", SqlDbType.VarChar, 3).Value = strEstado_Custodio

            '...Resultado
            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            Custodio_Mantenedor = strDescError
        End Try
    End Function


End Class
