﻿Imports System.Data.SqlClient
Imports System.Data

Public Class ClsCartera

#Region "EValenzuela (Provisional)"
    Public Function Generar_Reportes_Cartera(ByVal strFechaBusqueda As String, ByVal strFechaHasta As String, _
                                             ByVal strTipoLista As String, _
                                             ByVal strColumnas As String, _
                                             ByRef strRetorno As String, _
                                            Optional ByVal strInstruccion As String = "") As DataSet

        Dim LDS_Cartera As New DataSet
        Dim LDS_Cartera_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_GestionCarteras_Obtener"
        Lstr_NombreTabla = "CARTERA"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Char, 10).Value = glngIdUsuario
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Char, 10).Value = glngNegocioOperacion
            SQLCommand.Parameters.Add("pFechaBusqueda", SqlDbType.Char, 10).Value = IIf(strFechaBusqueda = "", DBNull.Value, strFechaBusqueda)
            SQLCommand.Parameters.Add("pFechaHasta", SqlDbType.Char, 10).Value = IIf(strFechaHasta = "", DBNull.Value, strFechaHasta)
            SQLCommand.Parameters.Add("pTipoLista", SqlDbType.Char, 10).Value = IIf(strTipoLista = "", DBNull.Value, strTipoLista)
            SQLCommand.Parameters.Add("pInstruccion", SqlDbType.Char, 10).Value = IIf(strInstruccion = "", DBNull.Value, strInstruccion)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Cartera, Lstr_NombreTabla)

            LDS_Cartera_Aux = LDS_Cartera


            If strColumnas.Trim = "" Then
                LDS_Cartera_Aux = LDS_Cartera
            Else
                LDS_Cartera_Aux = LDS_Cartera.Copy
                For Each Columna In LDS_Cartera.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Cartera_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Cartera_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            Return LDS_Cartera_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try


    End Function

#End Region

    Public Function TraerCarteraRentabilidad(ByVal strClaseInstrumento As String, _
                             ByVal strSubClaseInstrumento As String, _
                             ByVal strIdCuenta As String, _
                             ByVal strFechaConsulta As String, _
                             ByVal strColumnas As String, _
                             ByRef strRetorno As String, _
                             Optional ByVal strNemo As String = "", _
                             Optional ByVal strCodMoneda As String = "", _
                             Optional ByVal strCodFamilia As String = "" _
                             ) As DataSet

        Dim LDS_Cartera As New DataSet
        Dim LDS_Cartera_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Cartera_Buscar_Rentabilidad"
        Lstr_NombreTabla = "CARTERA"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pCodClaseInstrumento", SqlDbType.VarChar, 15).Value = IIf(strClaseInstrumento = "", DBNull.Value, strClaseInstrumento)
            SQLCommand.Parameters.Add("pCodSubClaseInstrumento", SqlDbType.VarChar, 10).Value = IIf(strSubClaseInstrumento = "", DBNull.Value, strSubClaseInstrumento)
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = IIf(strIdCuenta = "", DBNull.Value, CLng(0 & strIdCuenta))
            SQLCommand.Parameters.Add("pFechaConsulta", SqlDbType.Char, 10).Value = IIf(strFechaConsulta = "", DBNull.Value, strFechaConsulta)
            SQLCommand.Parameters.Add("pNemo", SqlDbType.VarChar, 50).Value = IIf(strNemo = "", DBNull.Value, strNemo)
            SQLCommand.Parameters.Add("pCodMoneda", SqlDbType.VarChar, 3).Value = IIf(strCodMoneda = "", DBNull.Value, strCodMoneda)
            SQLCommand.Parameters.Add("pCodFamilia", SqlDbType.VarChar, 10).Value = IIf(strCodFamilia = "", DBNull.Value, strCodFamilia)
            SQLCommand.Parameters.Add("pFechaOperacion", SqlDbType.Char, 10).Value = IIf(strFechaConsulta = "", DBNull.Value, strFechaConsulta)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Cartera, Lstr_NombreTabla)

            LDS_Cartera_Aux = LDS_Cartera


            If strColumnas.Trim = "" Then
                LDS_Cartera_Aux = LDS_Cartera
            Else
                LDS_Cartera_Aux = LDS_Cartera.Copy
                For Each Columna In LDS_Cartera.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Cartera_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Cartera_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            Return LDS_Cartera_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function TraerCartera(ByVal strClaseInstrumento As String, _
                                 ByVal strSubClaseInstrumento As String, _
                                 ByVal strIdCuenta As String, _
                                 ByVal strFechaConsulta As String, _
                                 ByVal strColumnas As String, _
                                 ByRef strRetorno As String, _
                                 Optional ByVal strNemo As String = "", _
                                 Optional ByVal strCodMoneda As String = "", _
                                 Optional ByVal strFormaOperarCartera As String = "", _
                                 Optional ByVal strCodFamilia As String = "", _
                                 Optional ByVal strFechaOperacion As String = "", _
                                 Optional ByVal dblIdOperacionDetalle As Double = 0 _
                                 ) As DataSet

        Dim LDS_Cartera As New DataSet
        Dim LDS_Cartera_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Cartera_Buscar"
        Lstr_NombreTabla = "CARTERA"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pCodClaseInstrumento", SqlDbType.VarChar, 15).Value = IIf(strClaseInstrumento = "", DBNull.Value, strClaseInstrumento)
            SQLCommand.Parameters.Add("pCodSubClaseInstrumento", SqlDbType.VarChar, 10).Value = IIf(strSubClaseInstrumento = "", DBNull.Value, strSubClaseInstrumento)
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = IIf(strIdCuenta = "", DBNull.Value, CLng(0 & strIdCuenta))
            SQLCommand.Parameters.Add("pFechaConsulta", SqlDbType.Char, 10).Value = IIf(strFechaConsulta = "", DBNull.Value, strFechaConsulta)
            SQLCommand.Parameters.Add("pNemo", SqlDbType.VarChar, 50).Value = IIf(strNemo = "", DBNull.Value, strNemo)
            SQLCommand.Parameters.Add("pCodMoneda", SqlDbType.VarChar, 3).Value = IIf(strCodMoneda = "", DBNull.Value, strCodMoneda)
            SQLCommand.Parameters.Add("pFormaOperarCartera", SqlDbType.VarChar, 1).Value = IIf(strFormaOperarCartera = "", DBNull.Value, strFormaOperarCartera)
            SQLCommand.Parameters.Add("pCodFamilia", SqlDbType.VarChar, 10).Value = IIf(strCodFamilia = "", DBNull.Value, strCodFamilia)
            SQLCommand.Parameters.Add("pFechaOperacion", SqlDbType.VarChar, 10).Value = IIf(strFechaOperacion = "", DBNull.Value, strFechaOperacion)
            SQLCommand.Parameters.Add("pIdOperacionDetalle", SqlDbType.Float, 10).Value = IIf(dblIdOperacionDetalle = 0, DBNull.Value, dblIdOperacionDetalle)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Cartera, Lstr_NombreTabla)

            LDS_Cartera_Aux = LDS_Cartera


            If strColumnas.Trim = "" Then
                LDS_Cartera_Aux = LDS_Cartera
            Else
                LDS_Cartera_Aux = LDS_Cartera.Copy
                For Each Columna In LDS_Cartera.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Cartera_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Cartera_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            Return LDS_Cartera_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function


    Public Function TraerCartera_RF_Nac(ByVal strClaseInstrumento As String, _
                                 ByVal strSubClaseInstrumento As String, _
                                 ByVal strIdCuenta As String, _
                                 ByVal strIdCliente As String, _
                                 ByVal strEmisor As String, _
                                 ByVal strIdInstrumento As String, _
                                 ByVal strCodMoneda As String, _
                                 ByVal strCodFamilia As String, _
                                 ByVal strPlazo As String, _
                                 ByVal strFecha As String, _
                                 ByVal strColumnas As String, _
                                 ByRef strRetorno As String, _
                                 Optional ByVal strNemo As String = "", _
                                 Optional ByVal strFormaOperarCartera As String = "" _
                                 ) As DataSet

        Dim LDS_Cartera As New DataSet
        Dim LDS_Cartera_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Cartera_RF_Nac"
        Lstr_NombreTabla = "CARTERA"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("@pIdCuenta", SqlDbType.Float, 10).Value = IIf(strIdCuenta = "", DBNull.Value, CLng(0 & strIdCuenta))
            SQLCommand.Parameters.Add("@pIdCliente", SqlDbType.Float, 10).Value = IIf(strIdCliente = "", DBNull.Value, CLng(0 & strIdCliente))
            SQLCommand.Parameters.Add("@pIdEmisor", SqlDbType.Float, 10).Value = IIf(strEmisor = "", DBNull.Value, CLng(0 & strEmisor))
            SQLCommand.Parameters.Add("@pIdInstrumento", SqlDbType.Float, 10).Value = IIf(strIdInstrumento = "", DBNull.Value, CLng(0 & strIdInstrumento))
            SQLCommand.Parameters.Add("@pCodMoneda", SqlDbType.VarChar, 3).Value = IIf(strCodMoneda = "", DBNull.Value, strCodMoneda)
            SQLCommand.Parameters.Add("@pCodFamilia", SqlDbType.VarChar, 10).Value = IIf(strCodFamilia = "", DBNull.Value, strCodFamilia)
            SQLCommand.Parameters.Add("@pCodSubClaseInstrumento", SqlDbType.VarChar, 10).Value = IIf(strSubClaseInstrumento = "", DBNull.Value, strSubClaseInstrumento)
            SQLCommand.Parameters.Add("@pPlazo", SqlDbType.Decimal, 4).Value = IIf(strPlazo = "", DBNull.Value, strPlazo)
            SQLCommand.Parameters.Add("@pFechaConsulta", SqlDbType.VarChar, 10).Value = strFecha

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Cartera, Lstr_NombreTabla)

            LDS_Cartera_Aux = LDS_Cartera

            If strColumnas.Trim = "" Then
                LDS_Cartera_Aux = LDS_Cartera
            Else
                LDS_Cartera_Aux = LDS_Cartera.Copy
                For Each Columna In LDS_Cartera.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Cartera_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Cartera_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            Return LDS_Cartera_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function TraerMovimientosCartera(ByVal lngIdCuenta As Long, _
                                 ByVal lngIdInstrumento As Long, _
                                 ByVal strClaseInstrumento As String, _
                                 ByVal strSubClaseInstrumento As String, _
                                 ByVal strCodMoneda As String, _
                                 ByVal strFechaIngresoDesde As String, _
                                 ByVal strFechaIngresoHasta As String, _
                                 ByVal strFechaEgresoDesde As String, _
                                 ByVal strFechaEgresoHasta As String, _
                                 ByVal strColumnas As String, _
                                 ByRef strRetorno As String) As DataSet

        Dim LDS_MovCartera As New DataSet
        Dim LDS_MovCartera_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Cartera_Consultar"
        Lstr_NombreTabla = "MOV_CARTERA"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = IIf(lngIdCuenta = 0, DBNull.Value, lngIdCuenta)
            SQLCommand.Parameters.Add("pIdInstrumento", SqlDbType.Float, 10).Value = IIf(lngIdInstrumento = 0, DBNull.Value, lngIdInstrumento)
            SQLCommand.Parameters.Add("pCodClaseInstrumento", SqlDbType.VarChar, 15).Value = IIf(strClaseInstrumento = "", DBNull.Value, strClaseInstrumento)
            SQLCommand.Parameters.Add("pCodSubClaseInstrumento", SqlDbType.VarChar, 10).Value = IIf(strSubClaseInstrumento = "", DBNull.Value, strSubClaseInstrumento)
            SQLCommand.Parameters.Add("pCodMoneda", SqlDbType.VarChar, 10).Value = IIf(strCodMoneda = "", DBNull.Value, strCodMoneda)
            SQLCommand.Parameters.Add("pFechaIngresoDesde", SqlDbType.Char, 10).Value = IIf(strFechaIngresoDesde = "", DBNull.Value, strFechaIngresoDesde)
            SQLCommand.Parameters.Add("pFechaIngresoHasta", SqlDbType.Char, 10).Value = IIf(strFechaIngresoHasta = "", DBNull.Value, strFechaIngresoHasta)
            SQLCommand.Parameters.Add("pFechaEgresoDesde", SqlDbType.Char, 10).Value = IIf(strFechaEgresoDesde = "", DBNull.Value, strFechaEgresoDesde)
            SQLCommand.Parameters.Add("pFechaEgresoHasta", SqlDbType.Char, 10).Value = IIf(strFechaEgresoHasta = "", DBNull.Value, strFechaEgresoHasta)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_MovCartera, Lstr_NombreTabla)

            LDS_MovCartera_Aux = LDS_MovCartera

            If strColumnas.Trim = "" Then
                LDS_MovCartera_Aux = LDS_MovCartera
            Else
                LDS_MovCartera_Aux = LDS_MovCartera.Copy
                For Each Columna In LDS_MovCartera.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_MovCartera_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_MovCartera_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            Return LDS_MovCartera_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function TraerSaldoCartera(ByVal lngIdCuenta As Long, _
                                      ByVal lngIdInstrumento As Long, _
                                      ByVal strFechaConsulta As String, _
                                      ByVal lngIdPortafolio As Long, _
                                      ByVal lngIdCartera As Long) As Double

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim ldblCantidad As Double = 0

        'Abre la conexion
        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_Cartera_BuscaSaldo", Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Cartera_BuscaSaldo"
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = lngIdCuenta
            SQLCommand.Parameters.Add("pIdInstrumento", SqlDbType.Float, 10).Value = lngIdInstrumento
            SQLCommand.Parameters.Add("pFechaConsulta", SqlDbType.Char, 10).Value = IIf(strFechaConsulta = "", DBNull.Value, strFechaConsulta)
            SQLCommand.Parameters.Add("pIdPortafolio", SqlDbType.Float, 10).Value = IIf(lngIdPortafolio = 0, DBNull.Value, lngIdPortafolio)
            SQLCommand.Parameters.Add("pIdCartera", SqlDbType.Float, 10).Value = IIf(lngIdCartera = 0, DBNull.Value, lngIdCartera)

            Dim ParametroSal As New SqlClient.SqlParameter("pCantidad", SqlDbType.Float, 20)
            ParametroSal.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal)

            SQLCommand.ExecuteNonQuery()

            ldblCantidad = SQLCommand.Parameters("pCantidad").Value

        Catch Ex As Exception
            Call gFun_ResultadoMsg("2|Error al obtener el saldo de la cartera" & vbCr & "[" & Ex.Message & "]", "TraerSaldoCartera")
        Finally
            Connect.Cerrar()
            TraerSaldoCartera = ldblCantidad
        End Try
    End Function

    Public Function TraerCarteraVigente(ByVal pIdCuenta As Integer, _
                                        ByVal pIdCustodio As Integer, _
                                        ByVal strCodMoneda As String, _
                                        ByVal strFecha As String, _
                                        ByVal strClaseInstrumento As String, _
                                        ByVal strSubClaseInstrumento As String, _
                                        ByVal strColumnas As String, _
                                        ByRef strRetorno As String) As DataSet

        Dim LDS_Cartera As New DataSet
        Dim LDS_Cartera_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Cartera_Vigente"
        Lstr_NombreTabla = "CARTERAVIG"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Int).Value = pIdCuenta
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Int, 10).Value = glngNegocioOperacion
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Int, 10).Value = glngIdUsuario
            SQLCommand.Parameters.Add("pIdCustodio", SqlDbType.Int).Value = pIdCustodio
            SQLCommand.Parameters.Add("pCodMoneda", SqlDbType.VarChar, 10).Value = strCodMoneda
            SQLCommand.Parameters.Add("pFecha", SqlDbType.Char, 10).Value = IIf(strFecha = "", DBNull.Value, strFecha)
            SQLCommand.Parameters.Add("pCodClaseInstrumento", SqlDbType.VarChar, 15).Value = IIf(strClaseInstrumento = "", DBNull.Value, strClaseInstrumento)
            SQLCommand.Parameters.Add("pCodSubClaseInstrumento", SqlDbType.VarChar, 10).Value = IIf(strSubClaseInstrumento = "", DBNull.Value, strSubClaseInstrumento)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Cartera, Lstr_NombreTabla)

            LDS_Cartera_Aux = LDS_Cartera


            If strColumnas.Trim = "" Then
                LDS_Cartera_Aux = LDS_Cartera
            Else
                LDS_Cartera_Aux = LDS_Cartera.Copy
                For Each Columna In LDS_Cartera.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Cartera_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Cartera_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            Return LDS_Cartera_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function Cartera_AjustarMonto(ByVal pIdCartera As Integer, _
                                         ByVal pIdOperacionDetalle As Integer, _
                                         ByVal pSecCartera As Integer, _
                                         ByVal pIngCantidad As Double, _
                                         ByVal pIngMonto As Double) As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion
        Try

            SQLCommand = New SqlClient.SqlCommand("Rcp_Cartera_Ajustar", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Cartera_Ajustar"
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdCartera", SqlDbType.Int).Value = pIdCartera
            SQLCommand.Parameters.Add("pIdOperacionDetalle", SqlDbType.Int).Value = pIdOperacionDetalle
            SQLCommand.Parameters.Add("pSecCartera", SqlDbType.Int).Value = pSecCartera
            SQLCommand.Parameters.Add("pIngCantidad", SqlDbType.Float).Value = pIngCantidad
            SQLCommand.Parameters.Add("pIngMonto", SqlDbType.Float).Value = pIngMonto

            '...Resultado
            Dim ParametroSal As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If Trim(strDescError) = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If
        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error al Ajustar Cartera " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
        End Try

        Return strDescError
    End Function

    Public Function TraerCarteraReporte(ByVal pIdCuenta As Integer, _
                                        ByVal strFechaDesde As String, _
                                        ByVal strFechaHasta As String, _
                                        ByVal strClaseInstrumento As String, _
                                        ByVal strSubClaseInstrumento As String, _
                                        ByVal strColumnas As String, _
                                        ByRef strRetorno As String) As DataTable

        Dim ldsDataTable As New DataTable
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Reporte_Cartera_Contable"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Int).Value = pIdCuenta
            SQLCommand.Parameters.Add("pFechaDesde", SqlDbType.VarChar, (10)).Value = IIf(strFechaDesde = "", DBNull.Value, strFechaDesde)
            SQLCommand.Parameters.Add("pFechaHasta", SqlDbType.VarChar, (10)).Value = IIf(strFechaHasta = "", DBNull.Value, strFechaHasta)
            SQLCommand.Parameters.Add("pCodClaseInstrumento", SqlDbType.VarChar, 15).Value = IIf(strClaseInstrumento = "", DBNull.Value, strClaseInstrumento)
            SQLCommand.Parameters.Add("pCodSubClaseInstrumento", SqlDbType.VarChar, 10).Value = IIf(strSubClaseInstrumento = "", DBNull.Value, strSubClaseInstrumento)

            '...Resultado
            Dim ParametroSal As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(ldsDataTable)

            gsubConfiguraDataTable(ldsDataTable, strColumnas)

            strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim

        Catch ex As Exception
            strRetorno = ex.Message
            ldsDataTable = Nothing
        Finally
            Connect.Cerrar()
        End Try

        Return ldsDataTable
    End Function

    Public Function TraerCarteraSorteoLetraGroup(ByVal strNemotecnico As String, _
                                                 ByVal strColumnas As String, _
                                                 ByRef strRetorno As String) As DataTable

        Dim ldtTabla As New DataTable
        Dim LstrProcedimiento As String

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Cartera_BuscarSorteoLetraGroup"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("PNEMOTECNICO", SqlDbType.VarChar, 50).Value = strNemotecnico

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(ldtTabla)

            'HAF 20101124: Configura el datatable segun lo informado en strColumnas
            Call gsubConfiguraDataTable(dtbTabla:=ldtTabla, strColumnas:=strColumnas)

            strRetorno = "OK"

        Catch ex As Exception
            strRetorno = ex.Message
            ldtTabla = Nothing
        Finally
            Connect.Cerrar()
        End Try

        Return ldtTabla
    End Function

    Public Function TraerCarteraPrepagoRFGroup(ByVal strNemotecnico As String, _
                                                 ByVal strColumnas As String, _
                                                 ByRef strRetorno As String) As DataTable

        Dim ldtTabla As New DataTable
        Dim LstrProcedimiento As String

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Cartera_BuscarPrepagoRFGroup"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("PNEMOTECNICO", SqlDbType.VarChar, 50).Value = strNemotecnico

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(ldtTabla)

            'HAF 20101124: Configura el datatable segun lo informado en strColumnas
            Call gsubConfiguraDataTable(dtbTabla:=ldtTabla, strColumnas:=strColumnas)

            strRetorno = "OK"

        Catch ex As Exception
            strRetorno = ex.Message
            ldtTabla = Nothing
        Finally
            Connect.Cerrar()
        End Try

        Return ldtTabla
    End Function

    Public Function TRAE_PREPAGORF_POR_INSTRUMENTO(ByVal strNemotecnico As String, _
                                                   ByVal dteFecha As Date, _
                                                   ByVal strColumnas As String, _
                                                   ByRef strRetorno As String) As DataTable
        Dim ldsDataTable As New DataTable
        Dim LstrProcedimiento As String

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "RCP_CARTERA_PREPAGORF_POR_INSTRUMENTO"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pNemotecnico", SqlDbType.VarChar, 50).Value = strNemotecnico
            SQLCommand.Parameters.Add("PFECHA", SqlDbType.VarChar, 10).Value = dteFecha.ToString("yyyyMMdd")

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(ldsDataTable)

            'HAF 20101124: Configura el datatable segun lo informado en strColumnas
            Call gsubConfiguraDataTable(dtbTabla:=ldsDataTable, strColumnas:=strColumnas)

            strRetorno = "OK"
            Return ldsDataTable

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function TraerSaldoCarteraDisp(ByVal lngIdCuenta As Long, _
                                          ByVal lngIdInstrumento As Long, _
                                          ByVal strFechaConsulta As Date, _
                                          ByVal lngIdCartera As Long) As Double

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim dblCantidad As Double = 0

        'Abre la conexion
        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand("DBO.RCP_CARTERA_SALDO_DISPONIBLE", Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "DBO.RCP_CARTERA_SALDO_DISPONIBLE"
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("PID_CUENTA", SqlDbType.Float, 10).Value = lngIdCuenta
            SQLCommand.Parameters.Add("PID_INSTRUMENTO", SqlDbType.Float, 10).Value = lngIdInstrumento
            SQLCommand.Parameters.Add("PFECHA", SqlDbType.Char, 10).Value = strFechaConsulta.ToString(gstrFormatoFecha112)
            SQLCommand.Parameters.Add("PID_CARTERA", SqlDbType.Float, 10).Value = IIf(lngIdCartera = 0, DBNull.Value, lngIdCartera)


            Dim ParametroSal As New SqlClient.SqlParameter("PSALDO", SqlDbType.Float, 20)
            ParametroSal.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal)

            SQLCommand.ExecuteNonQuery()

            dblCantidad = SQLCommand.Parameters("PSALDO").Value

        Catch Ex As Exception
            Call gFun_ResultadoMsg("2|Error al obtener el saldo de la cartera" & vbCr & "[" & Ex.Message & "]", "TraerSaldoCartera")
        Finally
            Connect.Cerrar()
            TraerSaldoCarteraDisp = dblCantidad
        End Try
    End Function

End Class
