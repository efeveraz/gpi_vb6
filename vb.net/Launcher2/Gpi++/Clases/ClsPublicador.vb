﻿Public Class ClsPublicador
    Public Function Publicador_Ver(ByVal dblIdPublicador As Double, _
                              ByVal strCodEstado As String, _
                              ByVal strColumnas As String, _
                              ByRef strRetorno As String, _
                              Optional ByVal strDscPublicador As String = "") As DataSet

        Dim LDS_Publicador As New DataSet
        Dim LDS_Publicador_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Publicador_Consultar"
        Lstr_NombreTabla = "Publicador"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdPublicador", SqlDbType.Float, 4).Value = IIf(dblIdPublicador = 0, DBNull.Value, dblIdPublicador)
            SQLCommand.Parameters.Add("pCodEstado", SqlDbType.VarChar, 4).Value = IIf(strCodEstado.Trim = "", DBNull.Value, strCodEstado.Trim)
            SQLCommand.Parameters.Add("pDscPublicador", SqlDbType.VarChar, 100).Value = IIf(strDscPublicador.Trim = "", DBNull.Value, strDscPublicador.Trim)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Publicador, Lstr_NombreTabla)

            LDS_Publicador_Aux = LDS_Publicador

            If strColumnas.Trim = "" Then
                LDS_Publicador_Aux = LDS_Publicador
            Else
                LDS_Publicador_Aux = LDS_Publicador.Copy
                For Each Columna In LDS_Publicador.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Publicador_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Publicador_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_Publicador.Dispose()
            Return LDS_Publicador_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function Publicador_Mantenedor(ByVal strAccion As String, _
                                          ByVal strCodEstado As String, _
                                          ByVal strDescPublicador As String, _
                                          ByVal intIdPublicador As Integer, _
                                          ByRef dblIdgenerado As Double) As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_Publicador_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Publicador_Mantencion"
            SQLCommand.Parameters.Clear()

            '@pAccion varchar(10),
            '@pCod_Moneda char(4),
            '@pDsc_Publicador varchar(100),
            '@pIdPublicador int,
            '@pResultado varchar(200) OUTPUT)

            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 10).Value = strAccion
            SQLCommand.Parameters.Add("pCodEstado", SqlDbType.VarChar, 3).Value = strCodEstado
            SQLCommand.Parameters.Add("pDscPublicador", SqlDbType.VarChar, 100).Value = strDescPublicador
            SQLCommand.Parameters.Add("pIdPublicador", SqlDbType.Float, 5).Value = intIdPublicador

            '...(Identity)
            Dim pSalInstr As New SqlClient.SqlParameter("pGenerado", SqlDbType.Float, 10)
            pSalInstr.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalInstr)

            '...Resultado
            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            dblIdgenerado = SQLCommand.Parameters("pGenerado").Value

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            Publicador_Mantenedor = strDescError
        End Try
    End Function

    Public Function BuscarPublicadorporCuentaNegocio(ByVal lngNegocio As Long, _
                                                     ByVal strCuenta As String, _
                                                     ByVal strColumnas As String, _
                                                     ByRef strRetorno As String) As DataSet

        Dim LDS_Publicador As New DataSet
        Dim LDS_Publicador_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Publicador_BuscarPorCtaNegocio"
        Lstr_NombreTabla = "PUBLICADOR"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Float, 10).Value = IIf(lngNegocio = 0, DBNull.Value, lngNegocio)
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = IIf(strCuenta = "", DBNull.Value, strCuenta)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Publicador, Lstr_NombreTabla)

            LDS_Publicador_Aux = LDS_Publicador

            If strColumnas.Trim = "" Then
                LDS_Publicador_Aux = LDS_Publicador
            Else
                LDS_Publicador_Aux = LDS_Publicador.Copy
                For Each Columna In LDS_Publicador.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Publicador_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Publicador_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_Publicador.Dispose()
            Return LDS_Publicador_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function PublicadorXCuenta_Mantenedor(ByVal strId_Cuenta As String, ByRef dstPublicadorXCuenta As DataSet) As String

        Dim strDescError As String = "OK"
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try

            '       ELIMINAMOS LOS PUBLICADORES X CUENTA YA EXISTENTES

            SQLCommand = New SqlClient.SqlCommand("Rcp_Publicador_MantencionPorCliente", MiTransaccionSQL.Connection, MiTransaccionSQL)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Publicador_MantencionPorCliente"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = "ELIMINAR"
            SQLCommand.Parameters.Add("pIdPublicador", SqlDbType.Float, 4).Value = DBNull.Value
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = strId_Cuenta
            SQLCommand.Parameters.Add("pDscSubClaseInstrumento", SqlDbType.VarChar, 100).Value = DBNull.Value
            SQLCommand.Parameters.Add("pCodSubClaseInstrumento", SqlDbType.VarChar, 10).Value = DBNull.Value
            SQLCommand.Parameters.Add("pDscPublicador", SqlDbType.VarChar, 100).Value = DBNull.Value

            '...Resultado
            Dim pSalElimIntPort As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalElimIntPort.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalElimIntPort)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            If Not strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Rollback()
                Exit Function
            End If

            '       INSERTAMOS LOS NUEVOS PUBLICADORES X CUENTA 

            If dstPublicadorXCuenta.Tables(0).Rows.Count <> Nothing Then
                For Each pRow As DataRow In dstPublicadorXCuenta.Tables(0).Rows
                    If (pRow("ID_NEGOCIO").ToString = 0) And (pRow("ID_CUENTA").ToString <> 0) And (pRow("ID_PUBLICADOR").ToString <> 0) Then
                        SQLCommand = New SqlClient.SqlCommand("Rcp_Publicador_MantencionPorCliente", MiTransaccionSQL.Connection, MiTransaccionSQL)
                        SQLCommand.CommandType = CommandType.StoredProcedure
                        SQLCommand.CommandText = "Rcp_Publicador_MantencionPorCliente"
                        SQLCommand.Parameters.Clear()

                        '[Rcp_Publicador_MantencionPorCliente]()
                        '(@pAccion varchar(10), 
                        '@pIdPublicador numeric(4, 0), 
                        '@pDscSubClaseInstrumento varchar(100),
                        '@pIdCuenta numeric(10, 0), 
                        '@pCodSubClaseInstrumento varchar(10), 
                        '@pDscPublicador varchar(100),
                        '@pResultado varchar(200) OUTPUT) 
                        '"ID_NEGOCIO, ID_CUENTA, ID_PUBLICADOR, DSC_PUBLICADOR, COD_SUB_CLASE_INSTRUMENTO, DSC_SUB_CLASE_INSTRUMENTO"

                        SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = "INSERTAR"
                        SQLCommand.Parameters.Add("pIdPublicador", SqlDbType.Float, 4).Value = pRow("ID_PUBLICADOR").ToString
                        SQLCommand.Parameters.Add("pDscSubClaseInstrumento", SqlDbType.VarChar, 100).Value = pRow("DSC_PUBLICADOR").ToString
                        SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = strId_Cuenta
                        SQLCommand.Parameters.Add("pCodSubClaseInstrumento", SqlDbType.VarChar, 10).Value = pRow("COD_SUB_CLASE_INSTRUMENTO").ToString
                        SQLCommand.Parameters.Add("pDscPublicador", SqlDbType.VarChar, 100).Value = pRow("DSC_SUB_CLASE_INSTRUMENTO").ToString

                        '...Resultado
                        Dim pSalInstPort As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                        pSalInstPort.Direction = Data.ParameterDirection.Output
                        SQLCommand.Parameters.Add(pSalInstPort)

                        SQLCommand.ExecuteNonQuery()

                        strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
                    End If
                    If Not strDescError.ToUpper.Trim = "OK" Then
                        Exit For
                    End If
                Next
            End If

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            PublicadorXCuenta_Mantenedor = strDescError
        End Try
    End Function
    Public Function PublicadorXAlias_Mantenedor(ByVal strId_Entidad As String, _
                                                ByVal intTipoEndtidad As Integer, _
                                                ByRef dstAlias As DataSet) As String
        Dim strDescError As String = "OK"
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            '+ ELIMINAMOS LOS ALIAS YA EXISTENTES
            SQLCommand = New SqlClient.SqlCommand("Rcp_AliasPublicador_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_AliasPublicador_Mantencion"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = "ELIMINAR"
            SQLCommand.Parameters.Add("pIdEntidad", SqlDbType.Float, 10).Value = IIf(strId_Entidad.Trim = "", DBNull.Value, strId_Entidad.Trim)
            SQLCommand.Parameters.Add("pIdEntidadGPI", SqlDbType.Float, 10).Value = intTipoEndtidad
            SQLCommand.Parameters.Add("pIdPublicador", SqlDbType.VarChar, 9).Value = DBNull.Value
            SQLCommand.Parameters.Add("pValor", SqlDbType.VarChar, 50).Value = DBNull.Value
            SQLCommand.Parameters.Add("pDscPublicador", SqlDbType.VarChar, 50).Value = DBNull.Value

            '...Resultado
            Dim pSalElimIntPort1 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalElimIntPort1.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalElimIntPort1)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            If strDescError.ToUpper.Trim <> "OK" Then
                MiTransaccionSQL.Rollback()
                Exit Function
            End If

            '+ INSERTAMOS LOS NUEVOS ALIAS PUBLICADORES PARA LA ENTIDAD GPI 
            If Not (IsNothing(dstAlias)) Then
                If dstAlias.Tables(0).Rows.Count <> Nothing Then
                    For Each dtrRegristro As DataRow In dstAlias.Tables(0).Rows
                        SQLCommand = New SqlClient.SqlCommand("Rcp_AliasPublicador_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)
                        SQLCommand.CommandType = CommandType.StoredProcedure
                        SQLCommand.CommandText = "Rcp_AliasPublicador_Mantencion"
                        SQLCommand.Parameters.Clear()

                        SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = "INSERTAR"
                        SQLCommand.Parameters.Add("pIdEntidad", SqlDbType.VarChar, 50).Value = strId_Entidad
                        SQLCommand.Parameters.Add("pIdEntidadGPI", SqlDbType.VarChar, 90).Value = intTipoEndtidad
                        SQLCommand.Parameters.Add("pIdPublicador", SqlDbType.VarChar, 9).Value = dtrRegristro("ID_PUBLICADOR").ToString
                        SQLCommand.Parameters.Add("pValor", SqlDbType.VarChar, 50).Value = dtrRegristro("VALOR").ToString
                        SQLCommand.Parameters.Add("pDscPublicador", SqlDbType.VarChar, 50).Value = dtrRegristro("DSC_PUBLICADOR").ToString

                        '...Resultado
                        Dim pSalInstPort As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                        pSalInstPort.Direction = Data.ParameterDirection.Output
                        SQLCommand.Parameters.Add(pSalInstPort)

                        SQLCommand.ExecuteNonQuery()

                        strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
                        If strDescError.ToUpper.Trim <> "OK" Then
                            Exit For
                        End If
                    Next
                End If
                If strDescError.ToUpper.Trim = "OK" Then
                    MiTransaccionSQL.Commit()
                Else
                    MiTransaccionSQL.Rollback()
                End If
            End If
        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            PublicadorXAlias_Mantenedor = strDescError
        End Try
    End Function
    Public Function TraeDatosSubClaseInstrumento(ByVal strIdNegocio As String, _
                                                 ByVal strIdPublicador As String, _
                                                 ByVal strColumnas As String, _
                                                 ByRef strRetorno As String) As DataSet

        Dim LDS_Publicador As New DataSet
        Dim LDS_Publicador_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Publicador_SubClaseInstrumentoVer"
        Lstr_NombreTabla = "RelPublicadorSubInst"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.VarChar, 3).Value = IIf(strIdNegocio.Trim = "", DBNull.Value, strIdNegocio.Trim)
            SQLCommand.Parameters.Add("pIdPublicador", SqlDbType.VarChar, 3).Value = IIf(strIdPublicador.Trim = "", DBNull.Value, strIdPublicador.Trim)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Publicador, Lstr_NombreTabla)

            LDS_Publicador_Aux = LDS_Publicador

            If strColumnas.Trim = "" Then
                LDS_Publicador_Aux = LDS_Publicador
            Else
                LDS_Publicador_Aux = LDS_Publicador.Copy
                For Each Columna In LDS_Publicador.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Publicador_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Publicador_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_Publicador.Dispose()
            Return LDS_Publicador_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function RelInstrumentoPublicador_Mantenedor(ByVal strAccion As String, _
                                                        ByVal intIdNegocio As Integer, _
                                                        ByVal intIdPublicador As Integer, _
                                                        ByVal DS_RelPublicador As DataSet, _
                                                        ByRef strRetorno As String) As String

        Dim strDescError As String = ""

        Dim strDescError1 As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction
        Dim LDS_Publicador As New DataSet
        Dim LDS_Publicador_Aux As New DataSet
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        'Dim dtrRelPublicador As DataRow

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            '+ ELIMINAMOS LOS QUE ESTAN EN LA RELPUBLICADOR
            SQLCommand = New SqlClient.SqlCommand("Rcp_Rel_InstPublicadorMantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Rel_InstPublicadorMantencion"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 10).Value = "MODIFICAR"
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Float, 10).Value = intIdNegocio
            SQLCommand.Parameters.Add("pIdPublicador", SqlDbType.Float, 4).Value = intIdPublicador
            SQLCommand.Parameters.Add("pCodSubInstrumento", SqlDbType.VarChar, 10).Value = ""


            ' ''...(Identity)
            'Dim pSalInst As New SqlClient.SqlParameter("pGenerado", SqlDbType.Float, 10)
            'pSalInst.Direction = Data.ParameterDirection.Output
            'SQLCommand.Parameters.Add(pSalInst)

            '...Resultado
            Dim pSalElimIntPort1 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalElimIntPort1.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalElimIntPort1)

            SQLCommand.ExecuteNonQuery()

            strDescError1 = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            If strDescError1.ToUpper.Trim <> "OK" Then
                MiTransaccionSQL.Rollback()
                Exit Function
            End If


            '+ INSERTAR
            If Not (IsNothing(DS_RelPublicador)) Then
                If DS_RelPublicador.Tables(0).Rows.Count <> Nothing Then
                    For Each dtrRegristro As DataRow In DS_RelPublicador.Tables(0).Rows

                        SQLCommand = New SqlClient.SqlCommand("Rcp_Rel_InstPublicadorMantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)
                        SQLCommand.CommandType = CommandType.StoredProcedure
                        SQLCommand.CommandText = "Rcp_Rel_InstPublicadorMantencion"
                        SQLCommand.Parameters.Clear()

                        SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = "INSERTAR"
                        SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Float, 10).Value = intIdNegocio
                        SQLCommand.Parameters.Add("pIdPublicador", SqlDbType.Float, 4).Value = intIdPublicador
                        SQLCommand.Parameters.Add("pCodSubInstrumento", SqlDbType.VarChar, 10).Value = dtrRegristro("CodSubInstrumento").ToString


                        ''...(Identity)
                        'Dim pSalInstr As New SqlClient.SqlParameter("pGenerado", SqlDbType.Float, 10)
                        'pSalInstr.Direction = Data.ParameterDirection.Output
                        'SQLCommand.Parameters.Add(pSalInstr)


                        '...Resultado
                        Dim pSalInstPort As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                        pSalInstPort.Direction = Data.ParameterDirection.Output
                        SQLCommand.Parameters.Add(pSalInstPort)

                        SQLCommand.ExecuteNonQuery()

                        strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
                        If strDescError.ToUpper.Trim <> "OK" Then
                            Exit For
                        End If
                    Next
                Else
                    strDescError = "OK"
                End If
            Else
                strDescError = "OK"
            End If
            If strDescError.ToUpper.Trim = "OK" And strDescError1.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            RelInstrumentoPublicador_Mantenedor = strDescError
        End Try
    End Function
    Public Function RelInstrumentoPublicador_Mantenedor_Sub_Clase(ByVal strAccion As String, _
                                                        ByVal intIdNegocio As Integer, _
                                                        ByVal intIdPublicador As Integer, _
                                                        ByVal strCod_Sub_Clase As String, _
                                                        ByRef strRetorno As String) As String

        Dim strDescError As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction
        Dim LDS_Publicador As New DataSet
        Dim LDS_Publicador_Aux As New DataSet
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        'Dim dtrRelPublicador As DataRow

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            '+ ELIMINAMOS LOS QUE ESTAN EN LA RELPUBLICADOR
            SQLCommand = New SqlClient.SqlCommand("Rcp_Rel_InstPublicadorMantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Rel_InstPublicadorMantencion"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 10).Value = strAccion
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Float, 10).Value = intIdNegocio
            SQLCommand.Parameters.Add("pIdPublicador", SqlDbType.Float, 4).Value = intIdPublicador
            SQLCommand.Parameters.Add("pCodSubInstrumento", SqlDbType.VarChar, 10).Value = strCod_Sub_Clase

            '...Resultado
            Dim pSalElimIntPort1 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalElimIntPort1.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalElimIntPort1)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            If strDescError.ToUpper.Trim <> "OK" Then
                MiTransaccionSQL.Rollback()
            Else
                MiTransaccionSQL.Commit()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en la actualización de RelInstrumentoPublicador_Mantenedor_Sub_Clase: " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            RelInstrumentoPublicador_Mantenedor_Sub_Clase = strDescError
        End Try
    End Function

End Class


