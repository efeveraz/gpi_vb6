﻿Public Class ClsSolicitudCambioCliente

    Public Function AgregarSolicitudCambio(ByVal strAccion As String, ByVal intIdUsuarioSolicitud As Integer, _
                                            ByVal strFechaSolicitud As String, ByVal intIdCliente As Integer, _
                                            ByVal strEstadoSolicitud As String, ByVal intIdUsuarioAprueba As Integer, _
                                            ByVal strFechaAprueba As String, ByRef intIdSolicitud As Integer, _
                                            ByVal dtbDatos As DataTable, ByVal dtbDetalleDatos As DataTable) As String

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction
        Dim strDescError As String = "OK"

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try

            SQLCommand = New SqlClient.SqlCommand("Rcp_SolicitudCambiosCliente_Mantenedor", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_SolicitudCambiosCliente_Mantenedor"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = strAccion
            SQLCommand.Parameters.Add("pIdUsuarioSolicitud", SqlDbType.Int).Value = intIdUsuarioSolicitud
            SQLCommand.Parameters.Add("pFechaSolicitud", SqlDbType.VarChar, 10).Value = strFechaSolicitud
            SQLCommand.Parameters.Add("pIdCliente", SqlDbType.Int).Value = intIdCliente

            Dim ParametroID As New SqlClient.SqlParameter("pIdSolicitud", SqlDbType.Int)
            ParametroID.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroID)

            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            Dim ParametroIDDato As New SqlClient.SqlParameter("pIdDato", SqlDbType.Int)
            Dim ParametroResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            Dim intIdDato As Integer = 0

            If Trim(strDescError) = "OK" Then
                intIdSolicitud = CInt(SQLCommand.Parameters("pIdSolicitud").Value.ToString.Trim)


                If dtbDatos.Rows.Count > 0 Then
                    For Each dtrDato As DataRow In dtbDatos.Rows

                        SQLCommand = New SqlClient.SqlCommand("Rcp_SolicitudCambiosClienteDatos_Mantenedor", MiTransaccionSQL.Connection, MiTransaccionSQL)

                        SQLCommand.CommandType = CommandType.StoredProcedure
                        SQLCommand.CommandText = "Rcp_SolicitudCambiosClienteDatos_Mantenedor"
                        SQLCommand.Parameters.Clear()

                        SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = strAccion
                        SQLCommand.Parameters.Add("pIdSolicitud", SqlDbType.Int).Value = intIdSolicitud
                        SQLCommand.Parameters.Add("pDescDato", SqlDbType.VarChar, 50).Value = dtrDato("DATO")
                        SQLCommand.Parameters.Add("pTabla", SqlDbType.VarChar, 50).Value = dtrDato("TABLA")

                        ParametroIDDato = New SqlClient.SqlParameter("pIdDato", SqlDbType.Int)
                        ParametroIDDato.Direction = Data.ParameterDirection.Output
                        SQLCommand.Parameters.Add(ParametroIDDato)

                        ParametroResultado = New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                        ParametroResultado.Direction = Data.ParameterDirection.Output
                        SQLCommand.Parameters.Add(ParametroResultado)

                        SQLCommand.ExecuteNonQuery()

                        strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

                        If Trim(strDescError) = "OK" Then
                            intIdDato = CInt(SQLCommand.Parameters("pIdDato").Value.ToString.Trim)
                            Dim ParametroIDDetalleDato As New SqlClient.SqlParameter("pIdDetalleDato", SqlDbType.Int)
                            Dim ParametroResultadoDet As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)


                            If dtbDetalleDatos.Rows.Count > 0 Then

                                For Each dtr_DetalleDato In dtbDetalleDatos.Select("DATO = '" & dtrDato("DATO") & "'")

                                    SQLCommand = New SqlClient.SqlCommand("Rcp_SolicitudCambiosClienteDetalleDatos_Mantenedor", MiTransaccionSQL.Connection, MiTransaccionSQL)

                                    SQLCommand.CommandType = CommandType.StoredProcedure
                                    SQLCommand.CommandText = "Rcp_SolicitudCambiosClienteDetalleDatos_Mantenedor"
                                    SQLCommand.Parameters.Clear()

                                    SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = strAccion
                                    SQLCommand.Parameters.Add("pIdDato", SqlDbType.Int).Value = intIdDato
                                    SQLCommand.Parameters.Add("pDescDetalleDato", SqlDbType.VarChar, 50).Value = dtr_DetalleDato("DETALLEDATO")


                                    SQLCommand.Parameters.Add("pValorActual", SqlDbType.VarChar, 200).Value = IIf(dtr_DetalleDato("VALORACTUAL") = "", DBNull.Value, dtr_DetalleDato("VALORACTUAL"))
                                    SQLCommand.Parameters.Add("pValorCambio", SqlDbType.VarChar, 200).Value = IIf(dtr_DetalleDato("VALORCAMBIO") = "", DBNull.Value, dtr_DetalleDato("VALORCAMBIO"))
                                    SQLCommand.Parameters.Add("pIdValorActual", SqlDbType.VarChar, 10).Value = IIf(dtr_DetalleDato("IDVALORACTUAL") = "", DBNull.Value, dtr_DetalleDato("IDVALORACTUAL"))
                                    SQLCommand.Parameters.Add("pIdValorCambio", SqlDbType.VarChar, 10).Value = IIf(dtr_DetalleDato("IDVALORCAMBIO") = "", DBNull.Value, dtr_DetalleDato("IDVALORCAMBIO"))
                                    SQLCommand.Parameters.Add("pIdRegistro", SqlDbType.VarChar, 10).Value = IIf(dtr_DetalleDato("ID") = "", DBNull.Value, dtr_DetalleDato("ID"))
                                    SQLCommand.Parameters.Add("pObservaciones", SqlDbType.VarChar, 300).Value = IIf(dtr_DetalleDato("OBSERVACIONES") = "", DBNull.Value, dtr_DetalleDato("OBSERVACIONES"))
                                    SQLCommand.Parameters.Add("pEstadoSolicitud", SqlDbType.Char, 3).Value = strEstadoSolicitud
                                    SQLCommand.Parameters.Add("pIdUsuarioAprueba", SqlDbType.Int).Value = IIf(intIdUsuarioAprueba = -1, DBNull.Value, intIdUsuarioAprueba)
                                    SQLCommand.Parameters.Add("pFechaAprueba", SqlDbType.VarChar, 10).Value = IIf(strFechaAprueba = "", DBNull.Value, strFechaAprueba)
                                    SQLCommand.Parameters.Add("pCampo", SqlDbType.VarChar, 50).Value = IIf(dtr_DetalleDato("CAMPO") = "", DBNull.Value, dtr_DetalleDato("CAMPO"))

                                    ParametroIDDetalleDato = New SqlClient.SqlParameter("pIdDetalleDato", SqlDbType.Int)
                                    ParametroIDDetalleDato.Direction = Data.ParameterDirection.Output
                                    SQLCommand.Parameters.Add(ParametroIDDetalleDato)

                                    ParametroResultadoDet = New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                                    ParametroResultadoDet.Direction = Data.ParameterDirection.Output
                                    SQLCommand.Parameters.Add(ParametroResultadoDet)

                                    SQLCommand.ExecuteNonQuery()

                                    strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

                                    If Trim(strDescError) <> "OK" Then
                                        GoTo Salir
                                    End If

                                Next
                            End If

                        Else
                            GoTo Salir
                        End If

                    Next

                End If

            Else
                GoTo Salir
            End If

            If Trim(strDescError) = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

            Return strDescError


Salir:
            If Trim(strDescError) = "OK" Then
                MiTransaccionSQL.Commit()
                Return ("OK")
            Else
                MiTransaccionSQL.Rollback()
                Return strDescError
            End If


        Catch ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error al Grabar Solicitud de Cambio de Cliente " & vbCr & ex.Message
            Return strDescError
        Finally
            SQLConnect.Cerrar()
        End Try

    End Function

    Public Function Solicitudes_Ver(ByVal strIdUsuario As String, _
                                    ByVal strRutCliente As String, _
                                    ByVal strFechaDesde As String, _
                                    ByVal strFechaHasta As String, _
                                    ByVal strColumnas As String, _
                                    ByVal strEstado As String, _
                                    ByRef strRetorno As String) As DataSet

        Dim LDS_Solicitudes As New DataSet
        Dim LDS_Solicitudes_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_SolicitudCambioCliente_Consultar"
        Lstr_NombreTabla = "SOLICITUDES"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("pRutCliente", SqlDbType.VarChar, 10).Value = IIf(strRutCliente.Trim = "", DBNull.Value, strRutCliente.Trim)
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.VarChar, 10).Value = IIf(strIdUsuario.Trim = "", DBNull.Value, strIdUsuario.Trim)
            SQLCommand.Parameters.Add("pEstado", SqlDbType.VarChar, 3).Value = IIf(strEstado.Trim = "", DBNull.Value, strEstado.Trim)
            SQLCommand.Parameters.Add("pFechaDesde", SqlDbType.VarChar, 10).Value = IIf(strFechaDesde.Trim = "", DBNull.Value, strFechaDesde.Trim)
            SQLCommand.Parameters.Add("pFechaHasta", SqlDbType.VarChar, 10).Value = IIf(strFechaHasta.Trim = "", DBNull.Value, strFechaHasta.Trim)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Solicitudes, Lstr_NombreTabla)

            LDS_Solicitudes_Aux = LDS_Solicitudes

            If strColumnas.Trim = "" Then
                LDS_Solicitudes_Aux = LDS_Solicitudes
            Else
                LDS_Solicitudes_Aux = LDS_Solicitudes.Copy
                For Each Columna In LDS_Solicitudes.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Solicitudes_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Solicitudes_Aux.Tables(Lstr_NombreTabla).Columns
                    'MsgBox(Columna.ColumnName & "   " & LInt_NomCol & "   " & CStr(LInt_Col))
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_Solicitudes.Dispose()
            Return LDS_Solicitudes_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function Solicitudes_Mantenedor(ByVal strAccion As String, _
                                            ByRef strValorActual As String, _
                                            ByRef strValorCambio As String, _
                                            ByRef intIdValorActual As Integer, _
                                            ByRef intIdValorCambio As Integer, _
                                            ByRef strIdRegistro As String, _
                                            ByRef intIdDetalleDato As Integer, _
                                            ByRef strIdCliente As String, _
                                            ByRef strEstado As String, _
                                            ByVal IntIdUsuarioAprueba As Integer, _
                                            ByVal strFechaAprueba As String, _
                                            ByVal strObservacion As String, _
                                            ByVal strCampo As String, _
                                            ByVal strTabla As String) As String


        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction
        Dim strDescError As String = "OK"

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            If strAccion = "MODIFICAR" Then
                SQLCommand = New SqlClient.SqlCommand("Rcp_SolicitudCambioDatos_Mantenedor", MiTransaccionSQL.Connection, MiTransaccionSQL)
                SQLCommand.CommandType = CommandType.StoredProcedure
                SQLCommand.CommandText = "Rcp_SolicitudCambioDatos_Mantenedor"
                SQLCommand.Parameters.Clear()
                SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 10).Value = strAccion.Trim
                SQLCommand.Parameters.Add("pTabla", SqlDbType.VarChar, 50).Value = strTabla.Trim
                SQLCommand.Parameters.Add("pCampo", SqlDbType.VarChar, 50).Value = strCampo.Trim
                SQLCommand.Parameters.Add("pNuevoValor", SqlDbType.VarChar, 100).Value = strValorCambio.Trim
                SQLCommand.Parameters.Add("pIdRegistro", SqlDbType.VarChar, 10).Value = strIdRegistro.Trim
                SQLCommand.Parameters.Add("pIdValorActual", SqlDbType.Int).Value = intIdValorActual
                SQLCommand.Parameters.Add("pIdValorCambio", SqlDbType.Int).Value = intIdValorCambio
                SQLCommand.Parameters.Add("pIdCliente", SqlDbType.VarChar, 10).Value = strIdCliente.Trim
                Dim ParametroSal As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                ParametroSal.Direction = Data.ParameterDirection.Output
                SQLCommand.Parameters.Add(ParametroSal)

                SQLCommand.ExecuteNonQuery()
                strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            End If
            If strAccion = "RECHAZAR" Then
                strDescError = "OK"
                strAccion = "MODIFICAR"
            End If
            If Trim(strDescError) = "OK" Then
                SQLCommand = New SqlClient.SqlCommand("Rcp_SolicitudCambiosClienteDetalleDatos_Mantenedor", MiTransaccionSQL.Connection, MiTransaccionSQL)
                SQLCommand.CommandType = CommandType.StoredProcedure
                SQLCommand.CommandText = "Rcp_SolicitudCambiosClienteDetalleDatos_Mantenedor"
                SQLCommand.Parameters.Clear()
                SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = strAccion.Trim
                SQLCommand.Parameters.Add("pIdDato", SqlDbType.Int).Value = DBNull.Value
                SQLCommand.Parameters.Add("pDescDetalleDato", SqlDbType.VarChar, 50).Value = DBNull.Value
                SQLCommand.Parameters.Add("pValorActual", SqlDbType.VarChar, 200).Value = DBNull.Value
                SQLCommand.Parameters.Add("pValorCambio", SqlDbType.VarChar, 200).Value = DBNull.Value
                SQLCommand.Parameters.Add("pIdValorActual", SqlDbType.VarChar, 10).Value = DBNull.Value
                SQLCommand.Parameters.Add("pIdValorCambio", SqlDbType.VarChar, 10).Value = DBNull.Value
                SQLCommand.Parameters.Add("pObservaciones", SqlDbType.VarChar, 300).Value = IIf((strObservacion).Trim = "", DBNull.Value, (strObservacion).Trim)
                SQLCommand.Parameters.Add("pIdDetalleDato", SqlDbType.Int).Value = IIf(intIdDetalleDato = 0, DBNull.Value, intIdDetalleDato)
                SQLCommand.Parameters.Add("pEstadoSolicitud", SqlDbType.VarChar, 3).Value = IIf((strEstado).Trim = "", DBNull.Value, (strEstado).Trim)
                SQLCommand.Parameters.Add("pIdUsuarioAprueba", SqlDbType.Int).Value = IIf(IntIdUsuarioAprueba = 0, DBNull.Value, IntIdUsuarioAprueba)
                SQLCommand.Parameters.Add("pFechaAprueba", SqlDbType.VarChar, 10).Value = IIf((strFechaAprueba).Trim = "", DBNull.Value, (strFechaAprueba).Trim)
                SQLCommand.Parameters.Add("pIdRegistro", SqlDbType.Int).Value = DBNull.Value
                SQLCommand.Parameters.Add("pCampo", SqlDbType.VarChar, 50).Value = DBNull.Value
                Dim ParametroSal1 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                ParametroSal1.Direction = Data.ParameterDirection.Output
                SQLCommand.Parameters.Add(ParametroSal1)

                SQLCommand.ExecuteNonQuery()
                strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
                If Trim(strDescError) <> "OK" Then
                    Solicitudes_Mantenedor = strDescError
                    MiTransaccionSQL.Rollback()
                Else
                    Solicitudes_Mantenedor = "OK"
                    MiTransaccionSQL.Commit()
                End If
            Else
                Solicitudes_Mantenedor = strDescError
                MiTransaccionSQL.Rollback()
            End If
        Catch ex As Exception
            Solicitudes_Mantenedor = ex.Message
        Finally
            SQLConnect.Cerrar()
        End Try
    End Function

    Public Function Clientes_Buscar(ByVal strFechaDesde As String, _
                                    ByVal strFechaHasta As String, _
                                    ByRef strRetorno As String) As DataSet

        Dim LDS_Solicitudes As New DataSet
        Dim LDS_Solicitudes_Aux As New DataSet
        Dim Lstr_NombreTabla As String = "MODIFICADOS"
        Dim LstrProcedimiento As String = "SP_GPI_LISTA_CLIENTE"

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("pFechaDesde", SqlDbType.VarChar, 10).Value = IIf(strFechaDesde.Trim = "", DBNull.Value, strFechaDesde.Trim)
            SQLCommand.Parameters.Add("pFechaHasta", SqlDbType.VarChar, 10).Value = IIf(strFechaHasta.Trim = "", DBNull.Value, strFechaHasta.Trim)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Solicitudes, Lstr_NombreTabla)

            LDS_Solicitudes_Aux = LDS_Solicitudes

            strRetorno = "OK"
            LDS_Solicitudes.Dispose()
            Return LDS_Solicitudes_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function SolicitudClientes_Buscar(ByVal strFechaDesde As String, _
                                    ByVal strFechaHasta As String, _
                                    ByRef strRetorno As String) As DataSet

        Dim LDS_Solicitudes As New DataSet
        Dim LDS_Solicitudes_Aux As New DataSet
        Dim Lstr_NombreTabla As String = "SOLICITUD_CLI"
        Dim LstrProcedimiento As String = "RCP_SOLICITUDCLIENTE_BUSCAR"

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("pFechaDesde", SqlDbType.VarChar, 10).Value = IIf(strFechaDesde.Trim = "", DBNull.Value, strFechaDesde.Trim)
            SQLCommand.Parameters.Add("pFechaHasta", SqlDbType.VarChar, 10).Value = IIf(strFechaHasta.Trim = "", DBNull.Value, strFechaHasta.Trim)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Solicitudes, Lstr_NombreTabla)

            LDS_Solicitudes_Aux = LDS_Solicitudes

            strRetorno = "OK"
            LDS_Solicitudes.Dispose()
            Return LDS_Solicitudes_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function Solicitud_VerificaNuevos(ByVal strSolicitudes As String) As String

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction
        Dim strDescError As String = "OK"

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_SolicitudCambioCliente_Verifica", MiTransaccionSQL.Connection, MiTransaccionSQL)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_SolicitudCambioCliente_Verifica"
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("@pSolicitud", SqlDbType.Text).Value = strSolicitudes

            Dim ParametroSal As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal)

            SQLCommand.ExecuteNonQuery()
            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If Trim(strDescError) <> "OK" Then
                Solicitud_VerificaNuevos = strDescError
                MiTransaccionSQL.Rollback()
            Else
                Solicitud_VerificaNuevos = "OK"
                MiTransaccionSQL.Commit()
            End If

        Catch ex As Exception
            Solicitud_VerificaNuevos = ex.Message
        Finally
            SQLConnect.Cerrar()
        End Try
    End Function

    Public Function Clientes_DeterminaCambios(ByVal strUsuario As String, _
                                              ByVal strFecha As String, _
                                              ByVal lngIdSolicitud As Long, _
                                              ByVal strPersona As String, _
                                              ByRef strCliente As String, _
                                              ByRef strTelefono As String, _
                                              ByRef strDireccion As String, _
                                              ByRef strMail As String, _
                                              ByRef strContacto As String) As String


        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction
        Dim strDescError As String = "OK"

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_SolicitudCambioCliente_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_SolicitudCambioCliente_Mantencion"
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("pUsuario", SqlDbType.Text).Value = strUsuario.Trim
            SQLCommand.Parameters.Add("pFecha", SqlDbType.Text).Value = strFecha.Trim
            SQLCommand.Parameters.Add("pIdSolicitud", SqlDbType.Float, 16).Value = lngIdSolicitud
            SQLCommand.Parameters.Add("pPersona", SqlDbType.Text).Value = strPersona.Trim
            SQLCommand.Parameters.Add("pCliente", SqlDbType.Text).Value = strCliente.Trim
            SQLCommand.Parameters.Add("pTelefonos", SqlDbType.Text).Value = strTelefono.Trim
            SQLCommand.Parameters.Add("pDirecciones", SqlDbType.Text).Value = strDireccion.Trim
            SQLCommand.Parameters.Add("pEMails", SqlDbType.Text).Value = strMail.Trim
            SQLCommand.Parameters.Add("pRelacionados", SqlDbType.Text).Value = strContacto.Trim

            Dim ParametroSal As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal)

            SQLCommand.ExecuteNonQuery()
            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If Trim(strDescError) <> "OK" Then
                Clientes_DeterminaCambios = strDescError
                MiTransaccionSQL.Rollback()
            Else
                Clientes_DeterminaCambios = "OK"
                MiTransaccionSQL.Commit()
            End If

        Catch ex As Exception
            Clientes_DeterminaCambios = ex.Message
        Finally
            SQLConnect.Cerrar()
        End Try
    End Function

    Public Function Solicitudes_Mantencion(ByVal strAccion As String, _
                                            ByRef strValorCambio As String, _
                                            ByRef lngIdPersona As Long, _
                                            ByVal strCampo As String, _
                                            ByVal strTabla As String, _
                                            ByVal lngIdSolicitudGpi As Long, _
                                            ByVal strObservacion As String) As String


        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction
        Dim strDescError As String = "OK"

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_SolicitudCambio_Mantenedor", MiTransaccionSQL.Connection, MiTransaccionSQL)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_SolicitudCambio_Mantenedor"
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 10).Value = strAccion.Trim
            SQLCommand.Parameters.Add("pTabla", SqlDbType.VarChar, 50).Value = strTabla.Trim
            SQLCommand.Parameters.Add("pCampo", SqlDbType.VarChar, 50).Value = strCampo.Trim
            SQLCommand.Parameters.Add("pNuevoValor", SqlDbType.VarChar, 100).Value = strValorCambio.Trim
            SQLCommand.Parameters.Add("pIdPersona", SqlDbType.Float, 10).Value = lngIdPersona
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Float, 10).Value = glngIdUsuario
            SQLCommand.Parameters.Add("pIdSolicitud", SqlDbType.Float, 10).Value = lngIdSolicitudGpi
            SQLCommand.Parameters.Add("pObservacion", SqlDbType.VarChar, 100).Value = strObservacion

            Dim ParametroSal As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal)

            SQLCommand.ExecuteNonQuery()
            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strAccion = "RECHAZAR" Then
                strDescError = "OK"
                strAccion = "MODIFICAR"
            End If
            'If Trim(strDescError) = "OK" Then
            '    SQLCommand = New SqlClient.SqlCommand("Rcp_SolicitudCambiosClienteDetalleDatos_Mantenedor", MiTransaccionSQL.Connection, MiTransaccionSQL)
            '    SQLCommand.CommandType = CommandType.StoredProcedure
            '    SQLCommand.CommandText = "Rcp_SolicitudCambiosClienteDetalleDatos_Mantenedor"
            '    SQLCommand.Parameters.Clear()
            '    SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = strAccion.Trim
            '    SQLCommand.Parameters.Add("pIdDato", SqlDbType.Int).Value = DBNull.Value
            '    SQLCommand.Parameters.Add("pDescDetalleDato", SqlDbType.VarChar, 50).Value = DBNull.Value
            '    SQLCommand.Parameters.Add("pValorActual", SqlDbType.VarChar, 200).Value = DBNull.Value
            '    SQLCommand.Parameters.Add("pValorCambio", SqlDbType.VarChar, 200).Value = DBNull.Value
            '    SQLCommand.Parameters.Add("pIdValorActual", SqlDbType.VarChar, 10).Value = DBNull.Value
            '    SQLCommand.Parameters.Add("pIdValorCambio", SqlDbType.VarChar, 10).Value = DBNull.Value
            '    SQLCommand.Parameters.Add("pObservaciones", SqlDbType.VarChar, 300).Value = IIf((strObservacion).Trim = "", DBNull.Value, (strObservacion).Trim)
            '    SQLCommand.Parameters.Add("pIdDetalleDato", SqlDbType.Int).Value = IIf(intIdDetalleDato = 0, DBNull.Value, intIdDetalleDato)
            '    SQLCommand.Parameters.Add("pEstadoSolicitud", SqlDbType.VarChar, 3).Value = IIf((strEstado).Trim = "", DBNull.Value, (strEstado).Trim)
            '    SQLCommand.Parameters.Add("pIdUsuarioAprueba", SqlDbType.Int).Value = IIf(IntIdUsuarioAprueba = 0, DBNull.Value, IntIdUsuarioAprueba)
            '    SQLCommand.Parameters.Add("pFechaAprueba", SqlDbType.VarChar, 10).Value = IIf((strFechaAprueba).Trim = "", DBNull.Value, (strFechaAprueba).Trim)
            '    SQLCommand.Parameters.Add("pIdRegistro", SqlDbType.Int).Value = DBNull.Value
            '    SQLCommand.Parameters.Add("pCampo", SqlDbType.VarChar, 50).Value = DBNull.Value
            '    Dim ParametroSal1 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            '    ParametroSal1.Direction = Data.ParameterDirection.Output
            '    SQLCommand.Parameters.Add(ParametroSal1)

            '    SQLCommand.ExecuteNonQuery()
            '    strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            If Trim(strDescError) <> "OK" Then
                Solicitudes_Mantencion = strDescError
                MiTransaccionSQL.Rollback()
            Else
                Solicitudes_Mantencion = "OK"
                MiTransaccionSQL.Commit()
            End If
            'Else
            'Solicitudes_Mantenedor = strDescError
            'MiTransaccionSQL.Rollback()
            'End If
        Catch ex As Exception
            Solicitudes_Mantencion = ex.Message
        Finally
            SQLConnect.Cerrar()
        End Try
    End Function
End Class
