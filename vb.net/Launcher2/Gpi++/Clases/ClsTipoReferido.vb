﻿Public Class ClsTipoReferido
    Public Function TipoReferido_Ver(ByVal strCodigoReferido As String, _
                                ByVal strEstado As String, _
                                ByVal strColumnas As String, _
                                ByRef strRetorno As String) As DataSet

        Dim LDS_TipoReferido As New DataSet
        Dim LDS_TipoReferido_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_TipoReferido_Consultar"
        Lstr_NombreTabla = "TipoReferido"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdTipoReferido", SqlDbType.VarChar, 5).Value = IIf(strCodigoReferido.Trim = "", DBNull.Value, strCodigoReferido.Trim)
            SQLCommand.Parameters.Add("pEstado", SqlDbType.VarChar, 3).Value = IIf(strEstado.Trim = "", DBNull.Value, strEstado.Trim)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_TipoReferido, Lstr_NombreTabla)

            LDS_TipoReferido_Aux = LDS_TipoReferido

            If strColumnas.Trim = "" Then
                LDS_TipoReferido_Aux = LDS_TipoReferido
            Else
                LDS_TipoReferido_Aux = LDS_TipoReferido.Copy
                For Each Columna In LDS_TipoReferido.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_TipoReferido_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_TipoReferido_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_TipoReferido.Dispose()
            Return LDS_TipoReferido_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    'Public Function TipoReferido_Mantenedor(ByVal strAccion As String, _
    '                          ByVal strCodPais As String, _
    '                          ByVal strCodMercado As String, _
    '                          ByVal strDescTipoReferido As String, _
    '                          ByVal strEstTipoReferido As String, _
    '                          ByVal intIdTipoReferido As Integer) As String

    '    Dim strDescError As String = ""
    '    Dim SQLCommand As New SqlClient.SqlCommand
    '    Dim SQLConnect As Cls_Conexion = New Cls_Conexion
    '    Dim MiTransaccionSQL As SqlClient.SqlTransaction

    '    '...Abre la conexion
    '    SQLConnect.Abrir()
    '    MiTransaccionSQL = SQLConnect.Transaccion

    '    Try
    '        SQLCommand = New SqlClient.SqlCommand("Rcp_TipoReferido_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)

    '        SQLCommand.CommandType = CommandType.StoredProcedure
    '        SQLCommand.CommandText = "Rcp_TipoReferido_Mantencion"
    '        SQLCommand.Parameters.Clear()

    '        '@pAccion varchar(10),
    '        '@pCod_Pais varchar(3),
    '        '@pCod_Mercado char(4),
    '        '@pDsc_TipoReferido varchar(60),
    '        '@pEstado char(3),
    '        '@pIdTipoReferido int,
    '        '@pResultado varchar(200) OUTPUT)

    '        SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 10).Value = strAccion
    '        SQLCommand.Parameters.Add("pCodPais", SqlDbType.VarChar, 3).Value = strCodPais
    '        SQLCommand.Parameters.Add("pCodMercado", SqlDbType.VarChar, 4).Value = strCodMercado
    '        SQLCommand.Parameters.Add("pDscTipoReferido", SqlDbType.VarChar, 60).Value = strDescTipoReferido
    '        SQLCommand.Parameters.Add("pEstado", SqlDbType.VarChar, 3).Value = strEstTipoReferido
    '        SQLCommand.Parameters.Add("pIdTipoReferido", SqlDbType.Float, 3).Value = intIdTipoReferido

    '        '...Resultado
    '        Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
    '        ParametroSal2.Direction = Data.ParameterDirection.Output
    '        SQLCommand.Parameters.Add(ParametroSal2)

    '        SQLCommand.ExecuteNonQuery()

    '        strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

    '        If strDescError.ToUpper.Trim = "OK" Then
    '            MiTransaccionSQL.Commit()
    '        Else
    '            MiTransaccionSQL.Rollback()
    '        End If

    '    Catch Ex As Exception
    '        MiTransaccionSQL.Rollback()
    '        strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
    '    Finally
    '        SQLConnect.Cerrar()
    '        TipoReferido_Mantenedor = strDescError
    '    End Try
    'End Function
    Public Function TipoReferido_Mantenedor(ByVal strAccion As String, _
                              ByVal IntIdTipoReferido As String, _
                              ByVal strDescripcion As String, _
                              ByVal strEstado As String) As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_TipoReferido_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_TipoReferido_Mantencion"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 10).Value = strAccion
            SQLCommand.Parameters.Add("pId_Referido", SqlDbType.VarChar, 2).Value = IIf(IntIdTipoReferido.Trim = "0", DBNull.Value, IntIdTipoReferido.Trim)
            SQLCommand.Parameters.Add("pDescripcion", SqlDbType.VarChar, 50).Value = IIf(strDescripcion.Trim = "", DBNull.Value, strDescripcion.Trim)
            SQLCommand.Parameters.Add("pEstado", SqlDbType.VarChar, 3).Value = strEstado

            '...Resultado
            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            TipoReferido_Mantenedor = strDescError
        End Try
    End Function
End Class
