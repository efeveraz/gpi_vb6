﻿Imports System.Data.SqlClient
Public Class ClsRelacionPublicadorGPI

    Public Function Entidad_Relacion_Publicador_GPI_Buscar( _
                    ByVal strEntidadGPI As String, _
                    ByVal strEntidadPublicador As String, _
                    ByVal strCodigoPublicador As String) As String

        Dim strDescError As String = ""
        Dim DS_Paso As New DataSet
        Dim lstrProcedimiento As String = ""
        Dim LstrNombreTabla As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SqlLDataAdapter As New SqlClient.SqlDataAdapter
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion

        '...Abre la conexion
        lstrProcedimiento = "Rcp_RelPublicadorGPI_Buscar"
        LstrNombreTabla = "PASO"
        SQLConnect.Abrir()

        Try

            SQLCommand = New SqlCommand(lstrProcedimiento, SQLConnect.Conexion)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = lstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pEntidadGPI", SqlDbType.VarChar, 50).Value = strEntidadGPI
            SQLCommand.Parameters.Add("pIdPublicador", SqlDbType.Float, 4).Value = CLng(0 & strEntidadPublicador)
            SQLCommand.Parameters.Add("pValor", SqlDbType.VarChar, 50).Value = strCodigoPublicador

            '...(Id_Entidad en GPI)
            Dim ParametroSal As New SqlClient.SqlParameter("pIdEntidad", SqlDbType.VarChar, 50)
            ParametroSal.Direction = ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal)

            '...Resultado
            Dim ParametroSal1 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal1.Direction = ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal1)

            SqlLDataAdapter.SelectCommand = SQLCommand
            SqlLDataAdapter.Fill(DS_Paso, LstrNombreTabla)

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            If strDescError <> "OK" Then
                Return ""
            Else
                strDescError = SQLCommand.Parameters("pIdEntidad").Value.ToString.Trim
                Return strDescError
            End If

        Catch Ex As Exception
            Return ""
        Finally
            SQLConnect.Cerrar()

        End Try

    End Function


End Class
