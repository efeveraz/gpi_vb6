﻿Public Class ClsRentabilidad
    Public Function ConsultaRentabilidadConsolidada(ByVal strFechaDesde As String, _
                                                    ByVal strFechaHasta As String, _
                                                    ByVal strCodMoneda As String, _
                                                    ByVal strTipoConsulta As String, _
                                                    ByVal strColumnas As String, _
                                                    ByRef strRetorno As String) As DataSet

        Dim LDS_Rentabilidad As New DataSet
        Dim LDS_Rentabilidad_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion


        LstrProcedimiento = "RCP_REPORTE_RENTABILIDAD_CONSOLIDADA"
        Lstr_NombreTabla = "Rentabilidad"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pFecha_Desde", SqlDbType.VarChar, 10).Value = strFechaDesde
            SQLCommand.Parameters.Add("pFecha_Hasta", SqlDbType.VarChar, 10).Value = strFechaHasta
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Int, 1).Value = glngNegocioOperacion
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Int, 10).Value = glngIdUsuario
            SQLCommand.Parameters.Add("pCodMoneda", SqlDbType.VarChar, 3).Value = strCodMoneda
            SQLCommand.Parameters.Add("pTipoConsulta", SqlDbType.VarChar, 10).Value = strTipoConsulta

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Rentabilidad, Lstr_NombreTabla)

            LDS_Rentabilidad_Aux = LDS_Rentabilidad

            If strColumnas.Trim = "" Then
                LDS_Rentabilidad_Aux = LDS_Rentabilidad
            Else
                LDS_Rentabilidad_Aux = LDS_Rentabilidad.Copy
                For Each Columna In LDS_Rentabilidad.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Rentabilidad_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Rentabilidad_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            Return LDS_Rentabilidad_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function
    Public Function ConsultaRentConsolidadaSerie(ByVal strFechaDesde As String, _
                                                    ByVal strFechaHasta As String, _
                                                    ByVal strCodMoneda As String, _
                                                    ByVal lngIdCuenta As Long, _
                                                    ByVal lngIdCliente As Long, _
                                                    ByVal lngIdGrupo As Long, _
                                                    ByVal strPeriodo As String, _
                                                    ByVal strColumnas As String, _
                                                    ByRef strRetorno As String) As DataSet

        Dim LDS_Serie As New DataSet
        Dim LDS_Serie_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "RCP_REPORTE_RENTABILIDAD_CONSOLIDADA_SERIE"
        Lstr_NombreTabla = "Serie"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pFecha_Desde", SqlDbType.VarChar, 10).Value = strFechaDesde
            SQLCommand.Parameters.Add("pFecha_Hasta", SqlDbType.VarChar, 10).Value = strFechaHasta
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Int, 10).Value = IIf(lngIdCuenta = 0, DBNull.Value, lngIdCuenta)
            SQLCommand.Parameters.Add("pIdCliente", SqlDbType.Int, 10).Value = IIf(lngIdCliente = 0, DBNull.Value, lngIdCliente)
            SQLCommand.Parameters.Add("pIdGrupo", SqlDbType.Int, 10).Value = IIf(lngIdGrupo = 0, DBNull.Value, lngIdGrupo)
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Int, 1).Value = glngNegocioOperacion
            SQLCommand.Parameters.Add("pCodMoneda", SqlDbType.VarChar, 3).Value = strCodMoneda
            SQLCommand.Parameters.Add("pPeriodo", SqlDbType.VarChar, 10).Value = strPeriodo

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Serie, Lstr_NombreTabla)

            LDS_Serie_Aux = LDS_Serie

            If strColumnas.Trim = "" Then
                LDS_Serie_Aux = LDS_Serie
            Else
                LDS_Serie_Aux = LDS_Serie.Copy
                For Each Columna In LDS_Serie.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Serie_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Serie_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            Return LDS_Serie_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

End Class
