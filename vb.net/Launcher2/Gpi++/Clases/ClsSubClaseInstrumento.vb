﻿Imports System.Data.SqlClient
Imports System.Data

Public Class ClsSubClaseInstrumento

#Region "Declaraciones de variables"
    Dim sProcedimiento As String = ""

#End Region

#Region "Metodos de la Clase"
    Public Function TraeDatosSubClaseInstrumento(ByVal strCodClaseInstrumento As String, _
                                                 ByVal strCodSubClaseInstrumento As String, _
                                                 ByVal strColumnas As String, _
                                                 ByRef strRetorno As String) As DataSet

        Dim LDS_SubClaseInstrumento As New DataSet
        Dim LDS_SubClaseInstrumento_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_SubClaseInstrumento_Buscar"
        Lstr_NombreTabla = "SubClaseInstrumento"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("@pCodClaseInstrumento", SqlDbType.VarChar, 10).Value = IIf(strCodClaseInstrumento = "", DBNull.Value, strCodClaseInstrumento)
            SQLCommand.Parameters.Add("pCodSubClaseInstrumento", SqlDbType.VarChar, 10).Value = IIf(strCodSubClaseInstrumento = "", DBNull.Value, strCodSubClaseInstrumento)
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Int).Value = glngNegocioOperacion
            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_SubClaseInstrumento, Lstr_NombreTabla)

            LDS_SubClaseInstrumento_Aux = LDS_SubClaseInstrumento

            If strColumnas.Trim = "" Then
                LDS_SubClaseInstrumento_Aux = LDS_SubClaseInstrumento
            Else
                LDS_SubClaseInstrumento_Aux = LDS_SubClaseInstrumento.Copy
                For Each Columna In LDS_SubClaseInstrumento.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_SubClaseInstrumento_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_SubClaseInstrumento_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_SubClaseInstrumento.Dispose()
            Return LDS_SubClaseInstrumento_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function


    Public Function SubClasesDeContraParte_Ver(ByVal strIdContraparte As String, _
                                               ByVal strColumnas As String, _
                                               ByRef strRetorno As String) As DataSet

        Dim LDS_SubClaContraparte As New DataSet
        Dim LDS_SubClaContraparte_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim Lstr_NombreTablaIncluidas As String
        Dim Lstr_NombreTablaNOIncluidas As String
        Dim LstrProcedimiento
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_SubClaseInstrumentoContraParte_Consultar"
        Lstr_NombreTablaIncluidas = "Incluidas"
        Lstr_NombreTablaNOIncluidas = "NO_Incluidas"
        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("pIdContraparte", SqlDbType.Float, 9).Value = IIf(strIdContraparte.Trim = "", DBNull.Value, CLng("0" & strIdContraparte.Trim))
            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_SubClaContraparte, Lstr_NombreTablaIncluidas)

            LDS_SubClaContraparte.Tables(1).TableName = Lstr_NombreTablaNOIncluidas

            LDS_SubClaContraparte_Aux = LDS_SubClaContraparte

            If strColumnas.Trim = "" Then
                LDS_SubClaContraparte_Aux = LDS_SubClaContraparte
            Else
                LDS_SubClaContraparte_Aux = LDS_SubClaContraparte.Copy
                For Each Columna In LDS_SubClaContraparte.Tables(Lstr_NombreTablaIncluidas).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_SubClaContraparte_Aux.Tables(Lstr_NombreTablaIncluidas).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If
            If strColumnas.Trim = "" Then
                LDS_SubClaContraparte_Aux = LDS_SubClaContraparte
            Else
                LDS_SubClaContraparte_Aux = LDS_SubClaContraparte.Copy
                For Each Columna In LDS_SubClaContraparte.Tables(Lstr_NombreTablaNOIncluidas).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_SubClaContraparte_Aux.Tables(Lstr_NombreTablaNOIncluidas).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_SubClaContraparte_Aux.Tables(Lstr_NombreTablaIncluidas).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_SubClaContraparte_Aux.Tables(Lstr_NombreTablaNOIncluidas).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            Return LDS_SubClaContraparte_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function Sub_ClaseInstrumento_Mantenedor(ByVal strAccion As String, _
                              ByVal strpCod_Sub_ClaseInstrumento As String, _
                              ByVal strpCod_ClaseInstrumento As String, _
                              ByVal strDsc_Sub_ClaseInstrumento As String, _
                              ByVal strpFlg_Seriado As String, _
                              ByVal strpFlg_TablaDesarrollo As String, _
                              ByVal strpCod_Mercado As String, _
                              ByVal strpFlg_AporteCapital As String, _
                              ByVal IntPosicion_Fecha As String, _
                              ByVal strpFmt_Fecha As String, _
                              ByVal strCodigo_Familia As String, _
                              ByVal strCod_Moneda As String, _
                              ByVal strCod_Tipo_Emisor As String) As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_SubClaseInstrumento_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_SubClaseInstrumento_Mantencion"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 10).Value = strAccion
            SQLCommand.Parameters.Add("pCod_Sub_ClaseInstrumento ", SqlDbType.VarChar, 10).Value = IIf(strpCod_Sub_ClaseInstrumento.Trim = "", DBNull.Value, strpCod_Sub_ClaseInstrumento.Trim)
            SQLCommand.Parameters.Add("pCod_ClaseInstrumento", SqlDbType.VarChar, 15).Value = IIf(strpCod_ClaseInstrumento.Trim = "", DBNull.Value, strpCod_ClaseInstrumento.Trim)
            SQLCommand.Parameters.Add("pDsc_Sub_ClaseInstrumento", SqlDbType.VarChar, 120).Value = IIf(strDsc_Sub_ClaseInstrumento.Trim = "", DBNull.Value, strDsc_Sub_ClaseInstrumento.Trim)
            SQLCommand.Parameters.Add("pFlg_Seriado", SqlDbType.VarChar, 1).Value = IIf(strpFlg_Seriado.Trim = "", "N", strpFlg_Seriado.Trim)
            SQLCommand.Parameters.Add("pFlg_TablaDesarrollo", SqlDbType.VarChar, 1).Value = IIf(strpFlg_TablaDesarrollo.Trim = "", "N", strpFlg_TablaDesarrollo.Trim)
            SQLCommand.Parameters.Add("pCod_Mercado", SqlDbType.VarChar, 4).Value = IIf(strpCod_Mercado.Trim = "", DBNull.Value, strpCod_Mercado.Trim)
            SQLCommand.Parameters.Add("pFlg_AporteCapital", SqlDbType.VarChar, 1).Value = IIf(strpFlg_AporteCapital.Trim = "", DBNull.Value, strpFlg_AporteCapital.Trim)
            SQLCommand.Parameters.Add("pPosicion_Fecha", SqlDbType.VarChar, 2).Value = IIf(IntPosicion_Fecha.Trim = "0", DBNull.Value, IntPosicion_Fecha.Trim)
            SQLCommand.Parameters.Add("pFmt_Fecha", SqlDbType.VarChar, 15).Value = IIf(strpFmt_Fecha.Trim = "", DBNull.Value, strpFmt_Fecha.Trim)
            SQLCommand.Parameters.Add("pCodigo_Familia", SqlDbType.VarChar, 10).Value = IIf(strCodigo_Familia.Trim = "", DBNull.Value, strCodigo_Familia.Trim)
            SQLCommand.Parameters.Add("pCod_Moneda", SqlDbType.VarChar, 50).Value = IIf(strCod_Moneda.Trim = "", DBNull.Value, strCod_Moneda.Trim)
            SQLCommand.Parameters.Add("pCod_Tipo_Emisor", SqlDbType.VarChar, 1).Value = IIf(strCod_Tipo_Emisor.Trim = "", DBNull.Value, strCod_Tipo_Emisor.Trim)

            '...Resultado
            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            Sub_ClaseInstrumento_Mantenedor = strDescError
        End Try
    End Function

#End Region
    Public Function Familia_Ver(ByVal strValorizacion As String, _
                                ByVal strCodFamilia As String, _
                                ByVal strColumnas As String, _
                                ByRef strRetorno As String) As DataSet

        Dim LDS_Moneda As New DataSet
        Dim LDS_Moneda_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Familia_Consultar"
        Lstr_NombreTabla = "FAMILIA"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("pValorizacion", SqlDbType.VarChar, 50).Value = IIf(strValorizacion.Trim = "", DBNull.Value, strValorizacion.Trim)
            SQLCommand.Parameters.Add("pCodFamilia", SqlDbType.VarChar, 10).Value = IIf(strCodFamilia.Trim = "", DBNull.Value, strCodFamilia.Trim)
           
            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Moneda, Lstr_NombreTabla)

            LDS_Moneda_Aux = LDS_Moneda

            If strColumnas.Trim = "" Then
                LDS_Moneda_Aux = LDS_Moneda
            Else
                LDS_Moneda_Aux = LDS_Moneda.Copy
                For Each Columna In LDS_Moneda.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Moneda_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Moneda_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_Moneda.Dispose()
            Return LDS_Moneda_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function
    Public Function TraeDatosSubClaseInstrumentoF(ByVal strCodClaseInstrumento As String, _
                                                ByVal strCodFamilia As String, _
                                                ByVal strCodMoneda As String, _
                                                ByVal strCodTipoEmisor As String, _
                                                ByVal strColumnas As String, _
                                                ByRef strRetorno As String _
                                                ) As DataSet

        Dim LDS_SubClaseInstrumento As New DataSet
        Dim LDS_SubClaseInstrumento_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_SubClaseInstrumento_BuscarxFamilia"
        Lstr_NombreTabla = "SubClaseInstrumento"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("pCodClaseInstrumento", SqlDbType.VarChar, 10).Value = IIf(strCodClaseInstrumento = "", DBNull.Value, strCodClaseInstrumento)
            SQLCommand.Parameters.Add("pCodFamilia", SqlDbType.VarChar, 10).Value = IIf(strCodFamilia = "", DBNull.Value, strCodFamilia)
            SQLCommand.Parameters.Add("pCodMoneda", SqlDbType.VarChar, 10).Value = IIf(strCodMoneda = "", DBNull.Value, strCodMoneda)
            SQLCommand.Parameters.Add("pCodTipoEmisor", SqlDbType.VarChar, 10).Value = IIf(strCodTipoEmisor = "", DBNull.Value, strCodTipoEmisor)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_SubClaseInstrumento, Lstr_NombreTabla)

            LDS_SubClaseInstrumento_Aux = LDS_SubClaseInstrumento

            If strColumnas.Trim = "" Then
                LDS_SubClaseInstrumento_Aux = LDS_SubClaseInstrumento
            Else
                LDS_SubClaseInstrumento_Aux = LDS_SubClaseInstrumento.Copy
                For Each Columna In LDS_SubClaseInstrumento.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_SubClaseInstrumento_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_SubClaseInstrumento_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_SubClaseInstrumento.Dispose()
            Return LDS_SubClaseInstrumento_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function Familia_Trae_Por_Clase(ByVal strCodClase As String, _
                            ByVal strColumnas As String, _
                            ByRef strRetorno As String) As DataSet

        Dim LDS_Familia As New DataSet
        Dim LDS_Familia_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Familia_Trae_X_Clase"
        Lstr_NombreTabla = "FAMILIA"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("@pClase", SqlDbType.VarChar, 10).Value = strCodClase

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Familia, Lstr_NombreTabla)

            LDS_Familia_Aux = LDS_Familia

            If strColumnas.Trim = "" Then
                LDS_Familia_Aux = LDS_Familia
            Else
                LDS_Familia_Aux = LDS_Familia.Copy
                For Each Columna In LDS_Familia.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Familia_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Familia_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_Familia.Dispose()
            Return LDS_Familia_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function



End Class


