﻿Public Class ClsCuenta_Pasivo
    Public Function Pasivos_Ver(ByVal IntIdCuenta As Integer, _
                                ByVal strColumnas As String, _
                                ByRef strRetorno As String) As DataSet

        Dim LDS_Alias As New DataSet
        Dim LDS_Alias_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "CMA_Pasivos_Consultar"
        Lstr_NombreTabla = "Pasivos"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = IIf(IntIdCuenta = 0, DBNull.Value, IntIdCuenta)
            ' SQLCommand.Parameters.Add("pIdEntidadGPI", SqlDbType.VarChar, 9).Value = IIf(strIdEntidadGPI.Trim = "", DBNull.Value, strIdEntidadGPI.Trim)
            'SQLCommand.Parameters.Add("pIdEntidadContraparte", SqlDbType.Float, 9).Value = IIf(strIdEntidadContraparte.Trim = "", DBNull.Value, strIdEntidadContraparte.Trim)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Alias, Lstr_NombreTabla)

            LDS_Alias_Aux = LDS_Alias

            If strColumnas.Trim = "" Then
                LDS_Alias_Aux = LDS_Alias
            Else
                LDS_Alias_Aux = LDS_Alias.Copy
                For Each Columna In LDS_Alias.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Alias_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Alias_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_Alias.Dispose()
            Return LDS_Alias_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function BuscarTiposPasivo(ByVal strColumnas As String, _
                                             ByRef strRetorno As String) As DataSet

        Dim LDS_TipoIdentificacion As New DataSet
        Dim LDS_TipoIdentificacion_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "CMA_TipoPasivo_Consultar"
        Lstr_NombreTabla = "TiposPasivos"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()
            '            SQLCommand.Parameters.Add("pEstado", SqlDbType.VarChar, 3).Value = IIf(strEstadoIdentificacion.Trim = "", DBNull.Value, strEstadoIdentificacion.Trim)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_TipoIdentificacion, Lstr_NombreTabla)

            LDS_TipoIdentificacion_Aux = LDS_TipoIdentificacion

            If strColumnas.Trim = "" Then
                LDS_TipoIdentificacion_Aux = LDS_TipoIdentificacion
            Else
                LDS_TipoIdentificacion_Aux = LDS_TipoIdentificacion.Copy
                For Each Columna In LDS_TipoIdentificacion.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_TipoIdentificacion_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_TipoIdentificacion_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next


            strRetorno = "OK"
            LDS_TipoIdentificacion.Dispose()

            Return LDS_TipoIdentificacion_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function Pasivos_Mantenedor(ByVal intIdCuenta As Integer, _
                                   ByRef dstAlias As DataSet) As String

        Dim strDescError As String = "OK"
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try

            '       ELIMINAMOS LOS ALIAS YA EXISTENTES

            'SQLCommand = New SqlClient.SqlCommand("CMA_Pasivos_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)
            'SQLCommand.CommandType = CommandType.StoredProcedure
            'SQLCommand.CommandText = "CMA_Pasivos_Mantencion"
            'SQLCommand.Parameters.Clear()

            'SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = "ELIMINAR"
            'SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = IIf(intIdCuenta = 0, DBNull.Value, intIdCuenta)
            ''IIf(pRow("MONTO").ToString.Length <= 0, DBNull.Value, pRow("MONTO").ToString)
            'SQLCommand.Parameters.Add("@COD_UNICO_SUB_MARGEN", SqlDbType.VarChar, 45).Value = DBNull.Value

            'SQLCommand.Parameters.Add("@CORR_SUB_MARGEN", SqlDbType.Int, 2).Value = DBNull.Value
            'SQLCommand.Parameters.Add("@COD_SUB_MARGEN", SqlDbType.VarChar, 20).Value = DBNull.Value
            'SQLCommand.Parameters.Add("@GLOSA_SUB_MARGEN", SqlDbType.VarChar, 80).Value = DBNull.Value

            'SQLCommand.Parameters.Add("@Cod_Tipo_Pasivo", SqlDbType.Char, 15).Value = DBNull.Value
            'SQLCommand.Parameters.Add("@MONTO_APROBADO", SqlDbType.Float, 12).Value = DBNull.Value
            'SQLCommand.Parameters.Add("@COD_MONEDA", SqlDbType.VarChar, 20).Value = DBNull.Value

            'SQLCommand.Parameters.Add("@PRODUCTO_CREDITO", SqlDbType.VarChar, 5).Value = DBNull.Value
            'SQLCommand.Parameters.Add("@NUMERO_OPERACION", SqlDbType.Float, 7).Value = DBNull.Value
            'SQLCommand.Parameters.Add("@MONEDA_CREDITO", SqlDbType.VarChar, 3).Value = DBNull.Value
            'SQLCommand.Parameters.Add("@MONTO_MONEDA_ORIGEN", SqlDbType.Float, 14).Value = DBNull.Value
            'SQLCommand.Parameters.Add("@MONTO_MONEDA_PESOS", SqlDbType.Float, 14).Value = DBNull.Value
            'SQLCommand.Parameters.Add("@NUM_TCR", SqlDbType.VarChar, 19).Value = DBNull.Value
            'SQLCommand.Parameters.Add("@MTO_CUPO_PES", SqlDbType.Float, 14).Value = DBNull.Value
            'SQLCommand.Parameters.Add("@MTO_CUPO_DOLAR", SqlDbType.Float, 14).Value = DBNull.Value

            ''...Resultado
            'Dim pSalElimIntPort As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            'pSalElimIntPort.Direction = Data.ParameterDirection.Output
            'SQLCommand.Parameters.Add(pSalElimIntPort)

            'SQLCommand.ExecuteNonQuery()

            'strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            'If Not strDescError.ToUpper.Trim = "OK" Then
            '    MiTransaccionSQL.Rollback()
            '    Exit Function
            'End If

            '       INSERTAMOS LOS NUEVOS ALIAS PARA LA ENTIDAD GPI 
            If Not (IsNothing(dstAlias)) Then
                If dstAlias.Tables(0).Rows.Count <> Nothing Then
                    For Each pRow As DataRow In dstAlias.Tables(0).Rows
                        SQLCommand = New SqlClient.SqlCommand("CMA_Pasivos_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)
                        SQLCommand.CommandType = CommandType.StoredProcedure
                        SQLCommand.CommandText = "CMA_Pasivos_Mantencion"
                        SQLCommand.Parameters.Clear()
                        SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = IIf(pRow("OPERACION").ToString.Trim.ToUpper = "", DBNull.Value, pRow("OPERACION").ToString.ToUpper)
                        SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = IIf(intIdCuenta = 0, DBNull.Value, intIdCuenta)
                        SQLCommand.Parameters.Add("@COD_UNICO_SUB_MARGEN", SqlDbType.VarChar, 45).Value = IIf(pRow("COD_UNICO_SUB_MARGEN").ToString.Trim = "", DBNull.Value, pRow("COD_UNICO_SUB_MARGEN").ToString)
                        SQLCommand.Parameters.Add("@CORR_SUB_MARGEN", SqlDbType.Int, 2).Value = IIf(pRow("CORR_SUB_MARGEN").ToString.Length <= 0, DBNull.Value, pRow("CORR_SUB_MARGEN").ToString)
                        SQLCommand.Parameters.Add("@COD_SUB_MARGEN", SqlDbType.VarChar, 20).Value = IIf(pRow("COD_SUB_MARGEN").ToString.Trim = "", DBNull.Value, pRow("COD_SUB_MARGEN").ToString)
                        SQLCommand.Parameters.Add("@GLOSA_SUB_MARGEN", SqlDbType.VarChar, 80).Value = IIf(pRow("GLOSA_SUB_MARGEN").ToString.Trim = "", DBNull.Value, pRow("GLOSA_SUB_MARGEN").ToString)


                        SQLCommand.Parameters.Add("@Cod_Tipo_Pasivo", SqlDbType.Char, 15).Value = IIf(pRow("COD_TIPO_PASIVO").ToString.Trim = "", DBNull.Value, pRow("COD_TIPO_PASIVO").ToString)
                        SQLCommand.Parameters.Add("@MONTO_APROBADO", SqlDbType.Float, 12).Value = IIf(pRow("MONTO_APROBADO").ToString.Length <= 0, DBNull.Value, pRow("MONTO_APROBADO").ToString)
                        SQLCommand.Parameters.Add("@COD_MONEDA", SqlDbType.VarChar, 20).Value = IIf(pRow("COD_MONEDA").ToString.Trim = "", DBNull.Value, pRow("COD_MONEDA").ToString)


                        SQLCommand.Parameters.Add("@PRODUCTO_CREDITO", SqlDbType.VarChar, 5).Value = IIf(pRow("PRODUCTO_CREDITO").ToString.Trim = "", DBNull.Value, pRow("PRODUCTO_CREDITO").ToString)
                        SQLCommand.Parameters.Add("@NUMERO_OPERACION", SqlDbType.Float, 7).Value = IIf(pRow("NUMERO_OPERACION").ToString.Length <= 0, DBNull.Value, pRow("NUMERO_OPERACION").ToString)
                        SQLCommand.Parameters.Add("@MONEDA_CREDITO", SqlDbType.VarChar, 3).Value = IIf(pRow("COD_MONEDA").ToString.Trim = "", DBNull.Value, pRow("COD_MONEDA").ToString)
                        SQLCommand.Parameters.Add("@MONTO_MONEDA_ORIGEN", SqlDbType.Float, 14).Value = IIf(pRow("MONTO_MONEDA_ORIGEN").ToString.Length <= 0, DBNull.Value, pRow("MONTO_MONEDA_ORIGEN").ToString)
                        SQLCommand.Parameters.Add("@MONTO_MONEDA_PESOS", SqlDbType.Float, 14).Value = IIf(pRow("MONTO_MONEDA_PESOS").ToString.Length <= 0, DBNull.Value, pRow("MONTO_MONEDA_PESOS").ToString)
                        SQLCommand.Parameters.Add("@NUM_TCR", SqlDbType.VarChar, 19).Value = IIf(pRow("NUM_TCR").ToString.Trim = "", DBNull.Value, pRow("NUM_TCR").ToString)
                        SQLCommand.Parameters.Add("@MTO_CUPO_PES", SqlDbType.Float, 14).Value = IIf(pRow("MTO_CUPO_PES").ToString.Length <= 0, DBNull.Value, pRow("MTO_CUPO_PES").ToString)
                        SQLCommand.Parameters.Add("@MTO_CUPO_DOLAR", SqlDbType.Float, 14).Value = IIf(pRow("MTO_CUPO_DOLAR").ToString.Length <= 0, DBNull.Value, pRow("MTO_CUPO_DOLAR").ToString)

                        'DblMonto_Aprobado = 0, DBNull.Value, DblMonto_Aprobado
                        '...Resultado
                        Dim pSalInstPort As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                        pSalInstPort.Direction = Data.ParameterDirection.Output
                        SQLCommand.Parameters.Add(pSalInstPort)

                        SQLCommand.ExecuteNonQuery()

                        strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
                        If Not strDescError.ToUpper.Trim = "OK" Then
                            Exit For
                        End If
                    Next
                End If

                If strDescError.ToUpper.Trim = "OK" Then
                    MiTransaccionSQL.Commit()
                Else
                    MiTransaccionSQL.Rollback()
                End If
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            Pasivos_Mantenedor = strDescError
        End Try
    End Function
End Class
