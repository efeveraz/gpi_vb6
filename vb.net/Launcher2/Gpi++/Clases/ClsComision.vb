﻿Imports System.Data.SqlClient
Public Class ClsComision
    Public Function Comision_Tipo_Rango_Ver(ByVal strColumnas As String, _
                                       ByRef strRetorno As String) As DataSet

        Dim LDS_Comision As New DataSet
        Dim LDS_Comision_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "RCP_TIPO_RANGO_VER"
        Lstr_NombreTabla = "Comision"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            'SQLCommand.Parameters.Add("@PDSC_ARBOL", SqlDbType.VarChar, 30).Value = strDSC_ARBOL
            'SQLCommand.Parameters.Add("pIdTipoComision", SqlDbType.Int, 10).Value = IIf(intIdTipoComision = 0, DBNull.Value, intIdTipoComision)
            'SQLCommand.Parameters.Add("pIdConceptoComision", SqlDbType.Int, 10).Value = IIf(intIdConceptoComision = 0, DBNull.Value, intIdConceptoComision)
            'SQLCommand.Parameters.Add("pCodEstado", SqlDbType.VarChar, 3).Value = IIf(strCodEstado = "", DBNull.Value, strCodEstado)
            '...Resultado
            Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultado.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Comision, Lstr_NombreTabla)

            LDS_Comision_Aux = LDS_Comision

            If strColumnas.Trim = "" Then
                LDS_Comision_Aux = LDS_Comision
            Else
                LDS_Comision_Aux = LDS_Comision.Copy
                For Each Columna In LDS_Comision.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Comision_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Comision_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            LDS_Comision.Dispose()
            Return LDS_Comision_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function
    Public Function Comision_Buscar_Excluidos(ByRef CodIns As Integer, ByRef Id_Grupo_Comision As Integer) As String
        Dim lstrRetorno As String = ""
        Dim LstrProcedimiento As String = ""
        Dim strDescError As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Comision_Buscar_Excluidos"
        Connect.Abrir()
        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("@PId_Instrumento", SqlDbType.Int, 20).Value = CodIns
            SQLCommand.Parameters.Add("@PId_Grupo_Comision", SqlDbType.Int, 20).Value = Id_Grupo_Comision
            Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultado.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLCommand.ExecuteNonQuery()
            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            Return strDescError
        Catch ex As Exception
            strDescError = ex.Message
            Return strDescError
        Finally
            Connect.Cerrar()
        End Try
    End Function
    Public Function Comision_Arbol_Consultar(ByVal strDSC_ARBOL As String, _
                                       ByVal strColumnas As String, _
                                       ByRef strRetorno As String) As DataSet

        Dim LDS_Comision As New DataSet
        Dim LDS_Comision_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "RCP_ARBOL_INS_BUSCAR_NIVEL1"
        Lstr_NombreTabla = "Comision"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("@PDSC_ARBOL", SqlDbType.VarChar, 30).Value = strDSC_ARBOL
            'SQLCommand.Parameters.Add("pIdTipoComision", SqlDbType.Int, 10).Value = IIf(intIdTipoComision = 0, DBNull.Value, intIdTipoComision)
            'SQLCommand.Parameters.Add("pIdConceptoComision", SqlDbType.Int, 10).Value = IIf(intIdConceptoComision = 0, DBNull.Value, intIdConceptoComision)
            'SQLCommand.Parameters.Add("pCodEstado", SqlDbType.VarChar, 3).Value = IIf(strCodEstado = "", DBNull.Value, strCodEstado)
            '...Resultado
            Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultado.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Comision, Lstr_NombreTabla)

            LDS_Comision_Aux = LDS_Comision

            If strColumnas.Trim = "" Then
                LDS_Comision_Aux = LDS_Comision
            Else
                LDS_Comision_Aux = LDS_Comision.Copy
                For Each Columna In LDS_Comision.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Comision_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Comision_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            LDS_Comision.Dispose()
            Return LDS_Comision_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function Comision_Consultar(ByVal intIdComision As Integer, _
                                       ByVal intIdGrupoComisiones As Integer, _
                                       ByVal intIdTipoComision As Integer, _
                                       ByVal intIdConceptoComision As Integer, _
                                       ByVal strCodEstado As String, _
                                       ByVal strColumnas As String, _
                                       ByRef strRetorno As String) As DataSet

        Dim LDS_Comision As New DataSet
        Dim LDS_Comision_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Comision_Consultar"
        Lstr_NombreTabla = "Comision"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdComision", SqlDbType.Int, 10).Value = IIf(intIdComision = 0, DBNull.Value, intIdComision)
            SQLCommand.Parameters.Add("pIdGrupoComisiones", SqlDbType.Int, 10).Value = IIf(intIdGrupoComisiones = 0, DBNull.Value, intIdGrupoComisiones)
            SQLCommand.Parameters.Add("pIdTipoComision", SqlDbType.Int, 10).Value = IIf(intIdTipoComision = 0, DBNull.Value, intIdTipoComision)
            SQLCommand.Parameters.Add("pIdConceptoComision", SqlDbType.Int, 10).Value = IIf(intIdConceptoComision = 0, DBNull.Value, intIdConceptoComision)
            SQLCommand.Parameters.Add("pCodEstado", SqlDbType.VarChar, 3).Value = IIf(strCodEstado = "", DBNull.Value, strCodEstado)
            '...Resultado
            Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultado.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Comision, Lstr_NombreTabla)

            LDS_Comision_Aux = LDS_Comision

            If strColumnas.Trim = "" Then
                LDS_Comision_Aux = LDS_Comision
            Else
                LDS_Comision_Aux = LDS_Comision.Copy
                For Each Columna In LDS_Comision.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Comision_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Comision_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            LDS_Comision.Dispose()
            Return LDS_Comision_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function Comision_BuscarEsquema(ByVal intIdCuenta As Integer, _
                                           ByVal intIdGrupoComisiones As Integer, _
                                           ByVal strCodTipoComision As String, _
                                           ByVal intIdConceptoComision As Integer, _
                                           ByVal strTipoOperacion As String, _
                                           ByVal strCodClaseInstrumento As String, _
                                           ByVal strCodSubClaseInstrumento As String, _
                                           ByVal intIdContraparte As Integer, _
                                           ByVal strMonedaMonto As String, _
                                           ByVal dblMonto As Double, _
                                           ByVal strCodEstado As String, _
                                           ByVal strColumnas As String, _
                                           ByRef strRetorno As String, _
                                           Optional ByVal strCodigo_Familia As String = "") As DataSet

        Dim LDS_Comision As New DataSet
        Dim LDS_Comision_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Comision_Buscar"
        Lstr_NombreTabla = "Comision"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            If strTipoOperacion = "APORTE" Or strTipoOperacion = "RESCATE" Then
                strTipoOperacion = IIf(strTipoOperacion = "APORTE", "COMPRA", "VENTA")
            End If

            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Int, 10).Value = IIf(glngNegocioOperacion = 0, DBNull.Value, glngNegocioOperacion)
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Int, 10).Value = IIf(intIdCuenta = 0, DBNull.Value, intIdCuenta)
            SQLCommand.Parameters.Add("pCodTipoComision", SqlDbType.Char, 5).Value = IIf(strCodTipoComision = "", DBNull.Value, strCodTipoComision)
            SQLCommand.Parameters.Add("pIdConceptoComision", SqlDbType.Char, 10).Value = IIf(intIdConceptoComision = 0, DBNull.Value, intIdConceptoComision)
            SQLCommand.Parameters.Add("pTipoOperacion", SqlDbType.Char, 10).Value = IIf(strTipoOperacion = "", DBNull.Value, Trim(strTipoOperacion))
            SQLCommand.Parameters.Add("pCodClaseInstrumento", SqlDbType.VarChar, 15).Value = IIf(strCodClaseInstrumento = "", DBNull.Value, strCodClaseInstrumento)
            SQLCommand.Parameters.Add("pCodSubClaseInstrumento", SqlDbType.VarChar, 10).Value = IIf(strCodSubClaseInstrumento = "", DBNull.Value, strCodSubClaseInstrumento)
            SQLCommand.Parameters.Add("pIdContraparte", SqlDbType.Int, 10).Value = IIf(intIdContraparte = 0, DBNull.Value, intIdContraparte)
            SQLCommand.Parameters.Add("pMonedaMontoOperacion", SqlDbType.VarChar, 3).Value = IIf(strMonedaMonto = "", DBNull.Value, strMonedaMonto)
            SQLCommand.Parameters.Add("pMontoOperacion", SqlDbType.Float, 24).Value = IIf(dblMonto = 0, DBNull.Value, dblMonto)
            SQLCommand.Parameters.Add("pCodEstado", SqlDbType.VarChar, 3).Value = IIf(strCodEstado = "", DBNull.Value, strCodEstado)
            SQLCommand.Parameters.Add("pCodFamilia", SqlDbType.VarChar, 10).Value = IIf(strCodigo_Familia = "", DBNull.Value, strCodigo_Familia)

            '...Resultado
            Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultado.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Comision, Lstr_NombreTabla)

            LDS_Comision_Aux = LDS_Comision

            If strColumnas.Trim = "" Then
                LDS_Comision_Aux = LDS_Comision
            Else
                LDS_Comision_Aux = LDS_Comision.Copy
                For Each Columna In LDS_Comision.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Comision_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Comision_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            LDS_Comision.Dispose()
            Return LDS_Comision_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    'Public Function Comision_Mantencion(ByVal strAccion As String, _
    '                                    ByVal intIdComision As Integer, _
    '                                    ByVal intIdConceptoComision As Integer, _
    '                                    ByVal intIdGrupoComisiones As Integer, _
    '                                    ByVal strCodEstado As String, _
    '                                    ByVal strFechaVigencia As String, _
    '                                    ByVal strTipoOperacion As String, _
    '                                    ByVal intIdContraparte As Integer, _
    '                                    ByVal strCodClaseInstrumento As String, _
    '                                    ByVal strCodSubClaseInstrumento As String, _
    '                                    ByVal strCodMoneda As String, _
    '                                    ByVal blnAfectoImpuesto As Boolean, _
    '                                    ByVal strRangoDesde As String, _
    '                                    ByVal strRangoHasta As String, _
    '                                    ByVal strCobroMinimo As String, _
    '                                    ByVal strPorcentajePropio As String, _
    '                                    ByVal intIdInstrumento As Integer, _
    '                                    ByVal strCodFormaCalculo As String, _
    '                                    ByVal blnIncluyeCaja As Boolean, _
    '                                    ByVal blnSobrepatrimonio As Boolean, _
    '                                    ByVal blnPorMonto As Boolean, _
    '                                    ByVal blnAplicaRango As Boolean, _
    '                                    ByVal strMontoComision As String, _
    '                                    ByVal strPorcentajeComision As String, _
    '                                    ByVal intIdBenchmark As Integer) As String

    Public Function Comision_Mantencion(ByVal strAccion As String, _
                                        ByVal intIdComision As Integer, _
                                        ByVal intIdGrupoComisiones As Integer, _
                                        ByVal intIdConceptoComision As Integer, _
                                        ByVal strCodEstado As String, _
                                        ByVal blnAfectoImpuesto As Boolean, _
                                        ByVal strPorcentajePropio As String, _
                                        ByVal intCobro_Minimo As Double, _
                                        ByVal strMoneda_Cobro_Minimo As String, _
                                        ByVal intSpreadCobro_Min As Integer, _
                                        ByVal strFechaVigencia As String, _
                                        ByVal strFechaAnulacion As String, _
                                        Optional ByRef lintID_Comision As Integer = 0) As String
        'ByVal intIdBenchmark As Integer

        Dim strDescError As String = "OK"
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try

            SQLCommand = New SqlClient.SqlCommand("Rcp_Comision_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Comision_Mantencion"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 20).Value = UCase(strAccion.Trim)
            Dim pSalIdComision As New SqlClient.SqlParameter("pIdComision", SqlDbType.Int, 10)
            pSalIdComision.Direction = Data.ParameterDirection.InputOutput
            pSalIdComision.Value = IIf(intIdComision = 0, DBNull.Value, intIdComision)
            SQLCommand.Parameters.Add(pSalIdComision)

            SQLCommand.Parameters.Add("pIdGrupoComisiones", SqlDbType.Int, 10).Value = intIdGrupoComisiones
            SQLCommand.Parameters.Add("pIdConceptoComision", SqlDbType.Int, 10).Value = intIdConceptoComision
            SQLCommand.Parameters.Add("pCodEstado", SqlDbType.VarChar, 3).Value = strCodEstado
            SQLCommand.Parameters.Add("pAfectoImpuesto", SqlDbType.Bit).Value = blnAfectoImpuesto
            'If strPorcentajePropio = "" Then
            SQLCommand.Parameters.Add("pPorcentajePropio", SqlDbType.Decimal).Value = IIf(strPorcentajePropio = "", DBNull.Value, CDbl(strPorcentajePropio) / 100)
            'Else
            'SQLCommand.Parameters.Add("pPorcentajePropio", SqlDbType.Decimal).Value = CDbl(strPorcentajePropio) / 100
            'End If
            SQLCommand.Parameters.Add("pCobroMinimo", SqlDbType.Decimal).Value = IIf(intCobro_Minimo = 0, DBNull.Value, intCobro_Minimo)
            SQLCommand.Parameters.Add("pCodMonedaCobroMinimo", SqlDbType.Decimal).Value = IIf(intCobro_Minimo = 0, DBNull.Value, intCobro_Minimo)
            SQLCommand.Parameters.Add("pSpreadCobroMin", SqlDbType.Decimal).Value = IIf(intSpreadCobro_Min = 0, DBNull.Value, intSpreadCobro_Min)
            SQLCommand.Parameters.Add("pFechaVigencia", SqlDbType.VarChar, 10).Value = IIf(strFechaVigencia = "", DBNull.Value, strFechaVigencia)
            SQLCommand.Parameters.Add("pFechaAnulacion", SqlDbType.VarChar, 10).Value = IIf(strFechaAnulacion = "", DBNull.Value, strFechaAnulacion)

            Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultado.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            lintID_Comision = SQLCommand.Parameters("pIdComision").Value

            If Trim(strDescError) = "OK" Then
                MiTransaccionSQL.Commit()
                Return ("OK")
            Else
                MiTransaccionSQL.Rollback()
                Return strDescError
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en la mantención Comisión" & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            Comision_Mantencion = strDescError
        End Try
    End Function

    Public Function Comision_Valida(ByVal intIdComision As Integer, _
                                    ByVal intIdTipoComision As Integer, _
                                    ByVal intIdConceptoComision As Integer, _
                                    ByVal intIdGrupoComisiones As Integer, _
                                    ByVal strCodEstado As String, _
                                    ByVal strTipoOperacion As String, _
                                    ByVal intIdContraparte As Integer, _
                                    ByVal strCodClaseInstrumento As String, _
                                    ByVal strCodSubClaseInstrumento As String, _
                                    ByVal strCodMoneda As String, _
                                    ByVal strRangoDesde As String, _
                                    ByVal strRangoHasta As String, _
                                    ByVal intIdInstrumento As Integer, _
                                    ByVal strCodFormaCalculo As String, _
                                    ByVal blnAplicaRango As Boolean, _
                                    ByVal intIdBenchmark As Integer, _
                                    ByRef strResultado As String) As Boolean


        Dim DS_Paso As New DataSet
        Dim lstrProcedimiento As String
        Dim LstrNombreTabla As String
        Dim strDescError As String = "OK"

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim SqlLDataAdapter As New SqlClient.SqlDataAdapter
        Dim lblnExisteComision As Boolean

        '...Abre la conexion
        lstrProcedimiento = "Rcp_Comision_Validar"
        LstrNombreTabla = "PASO"
        SQLConnect.Abrir()

        Try

            SQLCommand = New SqlCommand(lstrProcedimiento, SQLConnect.Conexion)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = lstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdComision", SqlDbType.Int, 10).Value = IIf(intIdComision = 0, DBNull.Value, intIdComision)
            SQLCommand.Parameters.Add("pIdTipoComision", SqlDbType.Int, 10).Value = intIdTipoComision
            SQLCommand.Parameters.Add("pIdConceptoComision", SqlDbType.Int, 10).Value = intIdConceptoComision
            SQLCommand.Parameters.Add("pIdGrupoComisiones", SqlDbType.Int, 10).Value = intIdGrupoComisiones
            SQLCommand.Parameters.Add("pTipoOperacion", SqlDbType.Char, 10).Value = IIf(strTipoOperacion = "", DBNull.Value, strTipoOperacion)
            SQLCommand.Parameters.Add("pCodClaseInstrumento", SqlDbType.VarChar, 15).Value = IIf(strCodClaseInstrumento = "", DBNull.Value, strCodClaseInstrumento)
            SQLCommand.Parameters.Add("pCodSubClaseInstrumento", SqlDbType.VarChar, 10).Value = IIf(strCodSubClaseInstrumento = "", DBNull.Value, strCodSubClaseInstrumento)
            SQLCommand.Parameters.Add("pIdContraparte", SqlDbType.Int, 10).Value = IIf(intIdContraparte = 0, DBNull.Value, intIdContraparte)
            SQLCommand.Parameters.Add("pIdInstrumento", SqlDbType.Int, 10).Value = IIf(intIdInstrumento = 0, DBNull.Value, intIdInstrumento)
            SQLCommand.Parameters.Add("pCodFormaCalculo", SqlDbType.Char, 5).Value = IIf(strCodFormaCalculo = "", DBNull.Value, strCodFormaCalculo)
            SQLCommand.Parameters.Add("pMoneda", SqlDbType.VarChar, 3).Value = strCodMoneda
            SQLCommand.Parameters.Add("pCodEstado", SqlDbType.VarChar, 3).Value = strCodEstado
            SQLCommand.Parameters.Add("pEsRango", SqlDbType.Bit).Value = blnAplicaRango
            If strRangoDesde = "" Then
                SQLCommand.Parameters.Add("pMontoDesde", SqlDbType.Decimal).Value = DBNull.Value
            Else
                SQLCommand.Parameters.Add("pMontoDesde", SqlDbType.Decimal).Value = CDbl(strRangoDesde)
            End If
            If strRangoHasta = "" Then
                SQLCommand.Parameters.Add("pMontoHasta", SqlDbType.Decimal).Value = DBNull.Value
            Else
                SQLCommand.Parameters.Add("pMontoHasta", SqlDbType.Decimal).Value = CDbl(strRangoHasta)
            End If

            SQLCommand.Parameters.Add("pIdBenchmark", SqlDbType.Int, 10).Value = IIf(intIdBenchmark = 0, DBNull.Value, intIdBenchmark)

            '...Resultado
            Dim pSalExisteComm As New SqlClient.SqlParameter("pExisteComision", SqlDbType.Bit)
            pSalExisteComm.Direction = ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalExisteComm)


            '...Resultado
            Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultado.Direction = ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)

            SqlLDataAdapter.SelectCommand = SQLCommand
            SqlLDataAdapter.Fill(DS_Paso, LstrNombreTabla)

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If Trim(strDescError) = "OK" Then
                lblnExisteComision = False
                strResultado = strDescError.Trim
            Else
                lblnExisteComision = True
                strResultado = strDescError.Trim
            End If


        Catch Ex As Exception
            strDescError = "Error en la mantención Comisión" & vbCr & Ex.Message
            Comision_Valida = False
        Finally
            SQLConnect.Cerrar()
            Comision_Valida = Not lblnExisteComision
        End Try
    End Function

    Public Function Comision_BuscarMonedas(ByVal strCodTipoComision As String, _
                                           ByVal intIdConceptoComision As Integer, _
                                           ByVal strColumnas As String, _
                                           ByRef strRetorno As String) As DataSet

        Dim LDS_Comision As New DataSet
        Dim LDS_Comision_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Comision_BuscarMonedas"
        Lstr_NombreTabla = "MonedasComision"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pCodTipoComision", SqlDbType.Char, 5).Value = IIf(strCodTipoComision = "", DBNull.Value, strCodTipoComision)
            SQLCommand.Parameters.Add("pIdConceptoComision", SqlDbType.Int).Value = IIf(intIdConceptoComision = 0, DBNull.Value, intIdConceptoComision)
            '...Resultado
            Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultado.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Comision, Lstr_NombreTabla)

            LDS_Comision_Aux = LDS_Comision

            If strColumnas.Trim = "" Then
                LDS_Comision_Aux = LDS_Comision
            Else
                LDS_Comision_Aux = LDS_Comision.Copy
                For Each Columna In LDS_Comision.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Comision_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Comision_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            LDS_Comision.Dispose()
            Return LDS_Comision_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function ComisionPorCobrar_Buscar(ByVal strFechaDesde As String, _
                                             ByVal strFechaHasta As String, _
                                             ByVal intIdConceptoComision As Integer, _
                                             ByVal strCodMonedaComision As String, _
                                             ByVal strCodMonedaPago As String, _
                                             ByVal strTipoBusqueda As String, _
                                             ByVal strColumnas As String, _
                                             ByRef strRetorno As String) As DataSet

        Dim LDS_Comision As New DataSet
        Dim LDS_Comision_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_ComisionesPorCobrar_Buscar"
        Lstr_NombreTabla = "ComisionesPorCobrar"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Int).Value = glngIdUsuario
            SQLCommand.Parameters.Add("pFechaDesde", SqlDbType.Char, 10).Value = strFechaDesde
            SQLCommand.Parameters.Add("pFechaHasta", SqlDbType.Char, 10).Value = strFechaHasta
            SQLCommand.Parameters.Add("pIdConceptoComision", SqlDbType.Int).Value = intIdConceptoComision
            SQLCommand.Parameters.Add("pCodMoneda", SqlDbType.VarChar, 3).Value = strCodMonedaComision
            SQLCommand.Parameters.Add("pCodMonedaPago", SqlDbType.VarChar, 3).Value = strCodMonedaPago
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Int).Value = glngNegocioOperacion
            SQLCommand.Parameters.Add("pTipoBusqueda", SqlDbType.VarChar, 3).Value = strTipoBusqueda


            '...Resultado
            Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultado.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Comision, Lstr_NombreTabla)

            LDS_Comision_Aux = LDS_Comision

            If strColumnas.Trim = "" Then
                LDS_Comision_Aux = LDS_Comision
            Else
                LDS_Comision_Aux = LDS_Comision.Copy
                For Each Columna In LDS_Comision.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Comision_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Comision_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            LDS_Comision.Dispose()
            Return LDS_Comision_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function Comision_Cobrar(ByVal strCodMonedaPago As String, _
                                    ByVal strDscConceptoComision As String, _
                                    ByVal strFechaMovimiento As String, _
                                    ByVal strFechaLiquidacion As String, _
                                    ByVal dtCommPorCobrar As DataTable, _
                                    ByVal dtDetCommPorCobrar As DataTable, _
                                    ByVal strPeriodoCobrado As String) As String


        Dim strDescError As String = "OK"
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        Dim lstrColumnasDetalle As String = "ID_CUENTA © ID_COMISION_DETALLE © FECHA_CALCULO © CONCEPTO_COMISION"
        'Dim lstrColumnasComision As String = "ID_CUENTA © TOTAL_COBRO"
        Dim lstrColumnasComision As String = "CONCEPTO_COMISION  © ID_CUENTA © TOTAL_COBRO © CODORIGENMOVCAJA © ID_COMISION_DETALLE "

        Dim lstrComision1 As String = ""
        Dim strRetorno As String = ""
        Dim lstrDetalle1 As String = ""
        Dim lstrDetalle2 As String = ""

        Dim lstrResultadoCadena As String = ""

        ' GENERA CADENA(S) DE DETALLE DE COMISIONES COBRADAS
        lstrResultadoCadena = GeneraCadenaText(dtCommPorCobrar, lstrColumnasComision, "©", lstrComision1)
        If lstrResultadoCadena <> "OK" Then
            strRetorno = lstrResultadoCadena & vbCrLf & "En Comisiones a Cobrar"
            Return strRetorno
        End If

        'lstrResultadoCadena = ""
        ''GENERA CADENA DE COMISIONES
        'lstrResultadoCadena = GeneraCadenaText(dtDetCommPorCobrar, lstrColumnasDetalle, "©", lstrDetalle1)
        'If lstrResultadoCadena <> "OK" Then
        '    Return lstrResultadoCadena & vbCrLf & "En Detalle de Comisiones"
        'End If


        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try


            SQLCommand = New SqlClient.SqlCommand("Rcp_Comision_CobrarPorAdministracion", MiTransaccionSQL.Connection, MiTransaccionSQL)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandTimeout = 0
            SQLCommand.CommandText = "Rcp_Comision_CobrarPorAdministracion"

            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pCodMonedaPago", SqlDbType.VarChar, 3).Value = strCodMonedaPago
            SQLCommand.Parameters.Add("pDscConceptoComision", SqlDbType.VarChar, 50).Value = strDscConceptoComision
            SQLCommand.Parameters.Add("pFechaMovimiento", SqlDbType.Char, 10).Value = strFechaMovimiento
            SQLCommand.Parameters.Add("pFechaLiquidacion", SqlDbType.Char, 10).Value = strFechaLiquidacion
            SQLCommand.Parameters.Add("pComisionesPorCobrar", SqlDbType.Text).Value = lstrComision1
            SQLCommand.Parameters.Add("pDetalleComisiones", SqlDbType.Text).Value = ""
            SQLCommand.Parameters.Add("@pPeriodoCobrado", SqlDbType.VarChar, 100).Value = strPeriodoCobrado

            '...Resultado
            Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultado.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If Trim(strDescError) = "OK" Then
                MiTransaccionSQL.Commit()
                Return ("OK")
            Else
                MiTransaccionSQL.Rollback()
                Return strDescError
            End If


        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en el cobro de Comisión por Administración" & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            Comision_Cobrar = strDescError
        End Try
    End Function

    Public Function ComisionExitoPorCobrar_Buscar(ByVal strFechaDesde As String, _
                                                  ByVal strFechaHasta As String, _
                                                  ByVal intIdConceptoComision As Integer, _
                                                  ByVal strCodMonedaComision As String, _
                                                  ByVal strCodMonedaPago As String, _
                                                  ByVal strColumnas As String, _
                                                  ByRef strRetorno As String) As DataSet

        Dim LDS_Comision As New DataSet
        Dim LDS_Comision_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_ComisionesExitoPorCobrar_Buscar"
        Lstr_NombreTabla = "ComisionesExitoPorCobrar"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Int).Value = glngIdUsuario
            SQLCommand.Parameters.Add("pFechaDesde", SqlDbType.Char, 10).Value = strFechaDesde
            SQLCommand.Parameters.Add("pFechaHasta", SqlDbType.Char, 10).Value = strFechaHasta
            SQLCommand.Parameters.Add("pIdConceptoComision", SqlDbType.Int).Value = intIdConceptoComision
            SQLCommand.Parameters.Add("pCodMoneda", SqlDbType.VarChar, 3).Value = strCodMonedaComision
            SQLCommand.Parameters.Add("pCodMonedaPago", SqlDbType.VarChar, 3).Value = strCodMonedaPago

            '...Resultado
            Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultado.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Comision, Lstr_NombreTabla)

            LDS_Comision_Aux = LDS_Comision

            If strColumnas.Trim = "" Then
                LDS_Comision_Aux = LDS_Comision
            Else
                LDS_Comision_Aux = LDS_Comision.Copy
                For Each Columna In LDS_Comision.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Comision_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Comision_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            LDS_Comision.Dispose()
            Return LDS_Comision_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function ComisionExito_Cobrar(ByVal strCodMonedaPago As String, _
                                         ByVal strDscConceptoComision As String, _
                                         ByVal strFechaMovimiento As String, _
                                         ByVal strFechaLiquidacion As String, _
                                         ByVal strCodTipoMovCaja As String, _
                                         ByVal strCodTipoConcepto As String, _
                                         ByVal intCantidadComm As Integer, _
                                         ByVal dtCommPorCobrar As DataTable, _
                                         ByVal intCantidadDetalle As Integer, _
                                         ByVal dtDetCommPorCobrar As DataTable) As String


        Dim strDescError As String = "OK"
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        Dim lstrColumnasDetalle As String = "ID_CUENTA © ID_COMISION_DETALLE © FECHA_CALCULO"
        Dim lstrColumnasComision As String = "ID_CUENTA © TOTAL_COBRO"
        Dim lstrComision1 As String = ""
        Dim lstrComision2 As String = ""
        Dim lstrDetalle1 As String = ""
        Dim lstrDetalle2 As String = ""


        Dim lstrResultadoCadena As String = ""

        ' GENERA CADENA(S) DE DETALLE DE COMISIONES COBRADAS
        lstrResultadoCadena = GeneraCadena(dtCommPorCobrar, lstrColumnasComision, "©", lstrComision1, lstrComision2)
        If lstrResultadoCadena <> "OK" Then
            Return lstrResultadoCadena & vbCrLf & "En Comisiones a Cobrar"
        End If

        lstrResultadoCadena = ""
        'GENERA CADENA DE COMISIONES
        lstrResultadoCadena = GeneraCadena(dtDetCommPorCobrar, lstrColumnasDetalle, "©", lstrDetalle1, lstrDetalle2)
        If lstrResultadoCadena <> "OK" Then
            Return lstrResultadoCadena & vbCrLf & "En Detalle de Comisiones"
        End If


        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try


            SQLCommand = New SqlClient.SqlCommand("Cba_Comision_CobrarComisionExito", MiTransaccionSQL.Connection, MiTransaccionSQL)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Cba_Comision_CobrarComisionExito"
            SQLCommand.Parameters.Clear()



            SQLCommand.Parameters.Add("pCodMonedaPago", SqlDbType.VarChar, 3).Value = strCodMonedaPago
            SQLCommand.Parameters.Add("pDscConceptoComision", SqlDbType.VarChar, 50).Value = strDscConceptoComision
            SQLCommand.Parameters.Add("pFechaMovimiento", SqlDbType.Char, 10).Value = strFechaMovimiento
            SQLCommand.Parameters.Add("pFechaLiquidacion", SqlDbType.Char, 10).Value = strFechaLiquidacion
            SQLCommand.Parameters.Add("pCodTipoMovCaja", SqlDbType.VarChar, 10).Value = strCodTipoMovCaja
            SQLCommand.Parameters.Add("pCodTipoComision", SqlDbType.Char, 3).Value = strCodTipoConcepto
            SQLCommand.Parameters.Add("pLineasComisiones", SqlDbType.Int).Value = intCantidadComm
            SQLCommand.Parameters.Add("pComisionesPorCobrar1", SqlDbType.VarChar, 8000).Value = lstrComision1
            SQLCommand.Parameters.Add("pComisionesPorCobrar2", SqlDbType.VarChar, 8000).Value = lstrComision2
            SQLCommand.Parameters.Add("pLineasDetalle", SqlDbType.Int).Value = intCantidadDetalle
            SQLCommand.Parameters.Add("pDetalleComisiones1", SqlDbType.VarChar, 8000).Value = lstrDetalle1
            SQLCommand.Parameters.Add("pDetalleComisiones2", SqlDbType.VarChar, 8000).Value = lstrDetalle2

            '...Resultado
            Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultado.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If Trim(strDescError) = "OK" Then
                MiTransaccionSQL.Commit()
                Return ("OK")
            Else
                MiTransaccionSQL.Rollback()
                Return strDescError
            End If


        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en el cobro de Comisión Éxito" & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            ComisionExito_Cobrar = strDescError
        End Try
    End Function

    Public Function ComisionExitoRecalculo_Buscar(ByVal strFechaDesde As String, _
                                                  ByVal strFechaHasta As String, _
                                                  ByVal intIdConceptoComision As Integer, _
                                                  ByVal strColumnas As String, _
                                                  ByRef strRetorno As String) As DataSet

        Dim LDS_Comision As New DataSet
        Dim LDS_Comision_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_ComisionesExitoPorCobrar_Buscar"
        Lstr_NombreTabla = "ComisionesExitoPorCobrar"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Int).Value = glngIdUsuario
            SQLCommand.Parameters.Add("pFechaDesde", SqlDbType.Char, 10).Value = strFechaDesde
            SQLCommand.Parameters.Add("pFechaHasta", SqlDbType.Char, 10).Value = strFechaHasta
            SQLCommand.Parameters.Add("pIdConceptoComision", SqlDbType.Int).Value = intIdConceptoComision

            '...Resultado
            Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultado.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Comision, Lstr_NombreTabla)

            LDS_Comision_Aux = LDS_Comision

            If strColumnas.Trim = "" Then
                LDS_Comision_Aux = LDS_Comision
            Else
                LDS_Comision_Aux = LDS_Comision.Copy
                For Each Columna In LDS_Comision.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Comision_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Comision_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            LDS_Comision.Dispose()
            Return LDS_Comision_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function Plantilla_Comision_Consultar(ByVal strColumnas As String, _
                                   ByVal intIdCuenta As Integer, _
                                   ByVal strFecha As String, _
                                   ByVal strTipoComision As String, _
                                   ByVal strTipoOperacion As String, _
                                   ByVal strClaseInstrumento As String, _
                                   ByVal strCodigoFamilia As String, _
                                   ByVal intIdContraparte As Integer, _
                                   ByVal strConceptoComision As String, _
                                   ByRef strRetorno As String, _
                                   Optional ByVal dblValor_Buscar_Rango As Double = 0, _
                                   Optional ByVal strCod_Moneda_Ope As String = "", _
                                   Optional ByVal strDscGrupoComisiones As String = "", _
                                   Optional ByVal intIdComision As Integer = 0 _
                                   ) As DataSet

        Dim LDS_Comision As New DataSet
        Dim LDS_Comision_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Plantilla_Comision_Buscar"
        Lstr_NombreTabla = "Comision"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Int, 10).Value = IIf(intIdCuenta = 0, DBNull.Value, intIdCuenta)
            SQLCommand.Parameters.Add("pFecha", SqlDbType.VarChar, 10).Value = IIf(strFecha = "", DBNull.Value, strFecha)
            SQLCommand.Parameters.Add("pTipoComision", SqlDbType.VarChar, 15).Value = IIf(strTipoComision = "", DBNull.Value, strTipoComision)
            SQLCommand.Parameters.Add("pTipoOperacion", SqlDbType.VarChar, 15).Value = IIf(strTipoOperacion = "", DBNull.Value, strTipoOperacion)
            SQLCommand.Parameters.Add("pClaseInstrumento", SqlDbType.VarChar, 15).Value = IIf(strClaseInstrumento = "", DBNull.Value, strClaseInstrumento)
            SQLCommand.Parameters.Add("pCodigoFamilia", SqlDbType.VarChar, 15).Value = IIf(strCodigoFamilia = "", DBNull.Value, strCodigoFamilia)
            SQLCommand.Parameters.Add("pIdContraparte", SqlDbType.Int, 10).Value = IIf(intIdContraparte = 0, DBNull.Value, intIdContraparte)
            SQLCommand.Parameters.Add("pConceptoComision", SqlDbType.VarChar, 50).Value = IIf(strConceptoComision = "", DBNull.Value, strConceptoComision)
            SQLCommand.Parameters.Add("@pDSC_GRUPO_COMISIONES", SqlDbType.VarChar, 15).Value = IIf(strDscGrupoComisiones = "", DBNull.Value, strDscGrupoComisiones)
            SQLCommand.Parameters.Add("@pIdComision", SqlDbType.Int, 10).Value = IIf(intIdComision = 0, DBNull.Value, intIdComision)

            SQLCommand.Parameters.Add("@pValor_Buscar_Rango", SqlDbType.Float).Value = dblValor_Buscar_Rango
            SQLCommand.Parameters.Add("@pCod_Moneda_Valor", SqlDbType.VarChar, 15).Value = strCod_Moneda_Ope


            '...Resultado
            Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultado.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Comision, Lstr_NombreTabla)

            If strColumnas.Trim = "" Then
                LDS_Comision_Aux = LDS_Comision
            Else
                LDS_Comision_Aux = LDS_Comision.Copy
                For Each Columna In LDS_Comision.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Comision_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Comision_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            If strRetorno = "" Then
                strRetorno = "OK"
            End If
            LDS_Comision.Dispose()
            Return LDS_Comision_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function Comision_Tipo_Periodo_Consultar(ByVal strColumnas As String, _
                                                    ByVal strCodTipoPeriodo As String, _
                                                    ByVal strDscTipoPeriodo As String, _
                                                    ByRef strRetorno As String) As DataSet

        Dim LDS_TipoPeriodo_Comision As New DataSet
        Dim LDS_TipoPeriodo_Comision_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_TipoPeriodo_Comision_Consultar"
        Lstr_NombreTabla = "TipoPeriodo_Comision"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pCodTipoPeriodo", SqlDbType.Int, 10).Value = IIf(strCodTipoPeriodo = "", DBNull.Value, strCodTipoPeriodo)
            SQLCommand.Parameters.Add("pDscTipoPeriodo", SqlDbType.Int, 10).Value = IIf(strDscTipoPeriodo = "", DBNull.Value, strDscTipoPeriodo)
            '...Resultado
            Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultado.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_TipoPeriodo_Comision, Lstr_NombreTabla)

            LDS_TipoPeriodo_Comision_Aux = LDS_TipoPeriodo_Comision

            If strColumnas.Trim = "" Then
                LDS_TipoPeriodo_Comision_Aux = LDS_TipoPeriodo_Comision
            Else
                LDS_TipoPeriodo_Comision_Aux = LDS_TipoPeriodo_Comision.Copy
                For Each Columna In LDS_TipoPeriodo_Comision.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_TipoPeriodo_Comision_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_TipoPeriodo_Comision_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            LDS_TipoPeriodo_Comision.Dispose()
            Return LDS_TipoPeriodo_Comision_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function Comision_Tipo_Medio_Pago_Cobro_Consultar(ByVal strColumnas As String, _
                                                             ByVal strCodTipoMedioPagoCobro As String, _
                                                             ByVal strDscTipoMedioPagoCobro As String, _
                                                             ByVal strTipoPagoCobro As String, _
                                                             ByRef strRetorno As String) As DataSet

        Dim LDS_TipoPeriodo_Comision As New DataSet
        Dim LDS_TipoPeriodo_Comision_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_TipoMedioPagoCobro_Comision_Consultar"
        Lstr_NombreTabla = "TipoMedioPagoCobro_Comision"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pCodTipoMedioPagoCobro", SqlDbType.VarChar, 20).Value = IIf(strCodTipoMedioPagoCobro = "", DBNull.Value, strCodTipoMedioPagoCobro)
            SQLCommand.Parameters.Add("pDscTipoMedioPagoCobro", SqlDbType.VarChar, 50).Value = IIf(strDscTipoMedioPagoCobro = "", DBNull.Value, strDscTipoMedioPagoCobro)
            SQLCommand.Parameters.Add("@pTipoCobroPago", SqlDbType.Char, 1).Value = strTipoPagoCobro
            '...Resultado
            Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultado.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_TipoPeriodo_Comision, Lstr_NombreTabla)

            LDS_TipoPeriodo_Comision_Aux = LDS_TipoPeriodo_Comision

            If strColumnas.Trim = "" Then
                LDS_TipoPeriodo_Comision_Aux = LDS_TipoPeriodo_Comision
            Else
                LDS_TipoPeriodo_Comision_Aux = LDS_TipoPeriodo_Comision.Copy
                For Each Columna In LDS_TipoPeriodo_Comision.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_TipoPeriodo_Comision_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_TipoPeriodo_Comision_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            LDS_TipoPeriodo_Comision.Dispose()
            Return LDS_TipoPeriodo_Comision_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function Comision_Modalidad_Consultar(ByVal strColumnas As String, _
                                                 ByVal intIdModalidad As Integer, _
                                                 ByVal strCodModalidad As String, _
                                                 ByRef strRetorno As String) As DataSet

        Dim LDS_Modalidad_Comision As New DataSet
        Dim LDS_Modalidad_Comision_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Modalidad_Comision_Consultar"
        Lstr_NombreTabla = "Modalidad_Comision"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdModalidad", SqlDbType.Decimal, 10).Value = IIf(intIdModalidad = 0, DBNull.Value, intIdModalidad)
            SQLCommand.Parameters.Add("pCodModalidad", SqlDbType.VarChar, 10).Value = IIf(strCodModalidad = "", DBNull.Value, strCodModalidad)
            '...Resultado
            Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultado.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Modalidad_Comision, Lstr_NombreTabla)

            LDS_Modalidad_Comision_Aux = LDS_Modalidad_Comision

            If strColumnas.Trim = "" Then
                LDS_Modalidad_Comision_Aux = LDS_Modalidad_Comision
            Else
                LDS_Modalidad_Comision_Aux = LDS_Modalidad_Comision.Copy
                For Each Columna In LDS_Modalidad_Comision.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Modalidad_Comision_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Modalidad_Comision_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            LDS_Modalidad_Comision.Dispose()
            Return LDS_Modalidad_Comision_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function Comision_Calcular(ByVal intIdCuenta As Integer, _
                                      ByVal intIdGrupoComisiones As Integer, _
                                      ByVal strCodTipoComision As String, _
                                      ByVal intIdConceptoComision As Integer, _
                                      ByVal strTipoOperacion As String, _
                                      ByVal strFechaDesde As String, _
                                      ByVal strFechaHasta As String, _
                                      ByVal strColumnas As String, _
                                      ByRef strRetorno As String) As DataSet

        Dim LDS_Comision As New DataSet
        Dim LDS_Comision_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Comision_Calcular"
        Lstr_NombreTabla = "Comision"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Int, 10).Value = IIf(glngNegocioOperacion = 0, DBNull.Value, glngNegocioOperacion)
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Int, 10).Value = IIf(intIdCuenta = 0, DBNull.Value, intIdCuenta)
            SQLCommand.Parameters.Add("pIdGrupoComisiones", SqlDbType.Int, 10).Value = IIf(intIdGrupoComisiones = 0, DBNull.Value, intIdGrupoComisiones)
            SQLCommand.Parameters.Add("pCodTipoComision", SqlDbType.VarChar, 5).Value = IIf(strCodTipoComision = "", DBNull.Value, strCodTipoComision)
            SQLCommand.Parameters.Add("pIdConceptoComision", SqlDbType.VarChar, 10).Value = IIf(intIdConceptoComision = 0, DBNull.Value, intIdConceptoComision)
            SQLCommand.Parameters.Add("pTipoOperacion", SqlDbType.VarChar, 10).Value = IIf(strTipoOperacion = "", DBNull.Value, Trim(strTipoOperacion))
            SQLCommand.Parameters.Add("pFechaDesde", SqlDbType.VarChar, 10).Value = IIf(strFechaDesde = "", DBNull.Value, Trim(strFechaDesde))
            SQLCommand.Parameters.Add("pFechaHasta", SqlDbType.VarChar, 10).Value = IIf(strFechaHasta = "", DBNull.Value, Trim(strFechaHasta))

            '...Resultado
            Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultado.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Comision, Lstr_NombreTabla)

            LDS_Comision_Aux = LDS_Comision

            If strColumnas.Trim = "" Then
                LDS_Comision_Aux = LDS_Comision
            Else
                LDS_Comision_Aux = LDS_Comision.Copy
                For Each Columna In LDS_Comision.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Comision_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Comision_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            LDS_Comision.Dispose()
            Return LDS_Comision_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function Comision_Cierre(ByVal strFechaGeneracion As String, _
                                    ByVal strFechaIni As String, _
                                    ByVal strFechaFin As String, _
                                    ByVal strListaCuentas As String, _
                                    ByVal strTipoProceso As String, _
                                    ByVal strColumnas As String, _
                                    ByRef strRetorno As String, _
                           Optional ByVal strGrabaComisionDetalle As String = "S", _
                           Optional ByVal strCierreCuenta As String = "N", _
                           Optional ByVal lngId_Grupo_Comisiones As Long = 0, _
                           Optional ByVal dblId_Comision As Double = 0, _
                           Optional ByVal dblPorcentajeEspecial As Double = 0, _
                           Optional ByVal dblTotcar_IN As Double = 0) As DataSet

        Dim LDS_Comision As New DataSet
        Dim LDS_Comision_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Proceso_General_Comisiones"
        Lstr_NombreTabla = "Comision"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.CommandTimeout = 0

            SQLCommand.Parameters.Add("pFechaGen", SqlDbType.VarChar, 10).Value = IIf(strFechaGeneracion = "", DBNull.Value, strFechaGeneracion)
            SQLCommand.Parameters.Add("pFechaIni", SqlDbType.VarChar, 10).Value = IIf(strFechaIni = "", DBNull.Value, strFechaIni)
            SQLCommand.Parameters.Add("pFechaFin", SqlDbType.VarChar, 10).Value = IIf(strFechaFin = "", DBNull.Value, strFechaFin)
            SQLCommand.Parameters.Add("pListaCuentas", SqlDbType.Text).Value = strListaCuentas
            SQLCommand.Parameters.Add("pGrabaComisionDetalle", SqlDbType.Char, 1).Value = strGrabaComisionDetalle
            SQLCommand.Parameters.Add("pTipoProceso", SqlDbType.Char, 1).Value = IIf(strTipoProceso = "", DBNull.Value, strTipoProceso)
            SQLCommand.Parameters.Add("pCierreCuenta", SqlDbType.Char, 1).Value = strCierreCuenta
            SQLCommand.Parameters.Add("pId_Grupo_Comisiones", SqlDbType.Float, 10).Value = lngId_Grupo_Comisiones
            SQLCommand.Parameters.Add("PId_Comision", SqlDbType.Float, 10).Value = dblId_Comision
            SQLCommand.Parameters.Add("pPorcentajeEspecial", SqlDbType.Float, 20).Value = dblPorcentajeEspecial
            SQLCommand.Parameters.Add("pTotcar_IN", SqlDbType.Float, 20).Value = dblTotcar_IN
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Int).Value = glngIdUsuario
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Int, 10).Value = IIf(glngNegocioOperacion = 0, DBNull.Value, glngNegocioOperacion)

            '@pTotcar_OUT
            Dim pSalResultado As New SqlClient.SqlParameter("pTotcar_OUT", SqlDbType.Float, 50)
            pSalResultado.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)




            '...Resultado
            Dim pSalResultado1 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultado1.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado1)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Comision, Lstr_NombreTabla)

            LDS_Comision_Aux = LDS_Comision
            strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strColumnas.Trim = "" Then
                LDS_Comision_Aux = LDS_Comision
            Else
                LDS_Comision_Aux = LDS_Comision.Copy
                For Each Columna In LDS_Comision.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Comision_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Comision_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            LDS_Comision.Dispose()
            Return LDS_Comision_Aux

        Catch ex As Exception
            If strRetorno = "OK" Or strRetorno = "" Then
                strRetorno = ex.Message
            End If
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function
    Public Function Comision_Mantencion_Full(ByVal strAccion As String, _
                                             ByVal intIdComision As Integer, _
                                             ByVal Ds_Comision As DataSet, _
                                            ByVal Ds_Comision_calculo As DataSet, _
                                            ByVal Ds_Rango_Comision As DataSet) As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction
        Dim lstrComisiones As String = ""
        Dim lstrComisionCalculos As String = ""
        Dim lstrComisionRangos As String = ""
        Dim llngIdOperacion As Integer = 0
        Dim lstrComisionCol As String = "ID_COMISION © " & _
                                        "ID_GRUPO_COMISIONES © " & _
                                        "ID_CONCEPTO_COMISION ©" & _
                                        "COD_ESTADO © " & _
                                        "AFECTO_IMPUESTO © " & _
                                        "PORCENTAJE_PROPIO © " & _
                                        "COBRO_MINIMO © " & _
                                        "COD_MONEDA_COBRO_MINIMO © " & _
                                        "SPREAD_COBRO_MIN © " & _
                                        "FECHA_VIGENCIA © " & _
                                        "FECHA_ANULACION"

        Dim lstrCalculoComisionCol As String = "ID_COMISION_CALCULO © " & _
                                               "ID_COMISION © " & _
                                               "COD_FORMA_CALCULO © " & _
                                               "APLICA_PORCENTAJE © " & _
                                               "APLICA_MONTO © " & _
                                               "APLICA_POR_RANGO © " & _
                                               "PORCENTAJE © " & _
                                               "MONTO © " & _
                                               "COD_MONEDA_MONTO © " & _
                                               "ID_ARBOL_CLASIFICACION_INSTRUMENTO"

        Dim lstrComisionRangoCol As String = "ID_RANGO_COMISION © " & _
                                             "ID_COMISION_CALCULO © " & _
                                             "COD_MONEDA © " & _
                                             "DESDE © " & _
                                             "HASTA © " & _
                                             "VALOR"

        Dim lstrResultadoCadena As String = ""

        ' GENERA CADENA(S) DE COMISIONES
        If IsNothing(Ds_Comision.Tables(0)) OrElse Ds_Comision.Tables(0).Rows.Count = 0 Then
            lstrComisiones = ""
        Else
            lstrResultadoCadena = GeneraCadenaText(Ds_Comision.Tables(0), lstrComisionCol, "©", lstrComisiones)
            If lstrResultadoCadena <> "OK" Then
                Return lstrResultadoCadena
            End If
        End If

        lstrResultadoCadena = ""
        'GENERA CADENA DE COMISIONES CALCULOS
        If IsNothing(Ds_Comision_calculo.Tables(0)) OrElse Ds_Comision_calculo.Tables(0).Rows.Count = 0 Then
            lstrComisionCalculos = ""
        Else
            lstrResultadoCadena = GeneraCadenaText(Ds_Comision_calculo.Tables(0), lstrCalculoComisionCol, "©", lstrComisionCalculos)
            If lstrResultadoCadena <> "OK" Then
                Return lstrResultadoCadena & vbCrLf & "En Comisiones calculos"
            End If
        End If

        'GENERA CADENA DE RANGOS
        lstrResultadoCadena = ""
        If IsNothing(Ds_Rango_Comision.Tables(0)) Then
            lstrComisionRangos = ""
        ElseIf Ds_Rango_Comision.Tables(0).Rows.Count = 0 Then
            lstrComisionRangos = ""
        Else
            lstrResultadoCadena = GeneraCadenaText(Ds_Rango_Comision.Tables(0), lstrComisionRangoCol, "©", lstrComisionRangos)
            If lstrResultadoCadena <> "OK" Then
                Return lstrResultadoCadena & vbCrLf & "En comisión rangos"
            End If
        End If


        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion
        Try


            SQLCommand = New SqlClient.SqlCommand("Rcp_Comision_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Comision_Mantencion"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 20).Value = UCase(strAccion.Trim)
            'SQLCommand.Parameters.Add("pIdComision", SqlDbType.Int, 10).Value = IIf(intIdComision = 0, DBNull.Value, intIdComision)
            SQLCommand.Parameters.Add("pComisiones", SqlDbType.Text).Value = lstrComisiones
            SQLCommand.Parameters.Add("pComision_Calculo", SqlDbType.Text).Value = lstrComisionCalculos
            SQLCommand.Parameters.Add("pRangos", SqlDbType.Text).Value = lstrComisionRangos

            '...Resultado
            Dim ParametroSal As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error al Grabar comisión " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
        End Try

        If strDescError.Trim.ToUpper = "OK" Then
            If gobjParametro.Tesoreria_Envio.ValorParametro = "S" Then
                Dim lcInterfaceTesoreria As New clsInterfaceTesoreria
                lcInterfaceTesoreria.InformarIngresoOperacion(llngIdOperacion)
            End If
        End If

        Return strDescError
    End Function

  
End Class
