﻿Imports System.Data.SqlClient

Public Class ClsCaja

    Public Function TraerMovimientosCaja(ByVal lngIdCuenta As Long, _
                                 ByVal lngIdCajacuenta As Long, _
                                 ByVal strCodMoneda As String, _
                                 ByVal strCodMercado As String, _
                                 ByVal strFechaDesde As String, _
                                 ByVal strFechaHasta As String, _
                                 ByVal strColumnas As String, _
                                 ByRef strRetorno As String, _
                                 ByVal strCodEstado As String, _
                                 ByVal strTipoLista As String, _
                                 ByVal strLista As String) As DataSet

        Dim LDS_MovimientosCaja As New DataSet
        Dim LDS_MovimientosCaja_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_MovCaja_Consultar"
        Lstr_NombreTabla = "MOV_CAJA"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = IIf(lngIdCuenta = 0, DBNull.Value, lngIdCuenta)
            SQLCommand.Parameters.Add("pIdCajaCuenta", SqlDbType.Float, 15).Value = IIf(lngIdCajacuenta = 0, DBNull.Value, lngIdCajacuenta)
            SQLCommand.Parameters.Add("pCodMoneda", SqlDbType.VarChar, 3).Value = IIf(strCodMoneda = "", DBNull.Value, strCodMoneda)
            SQLCommand.Parameters.Add("pCodMercado", SqlDbType.Char, 1).Value = IIf(strCodMercado = "", DBNull.Value, strCodMercado)
            SQLCommand.Parameters.Add("pFechaDesde", SqlDbType.Char, 10).Value = IIf(strFechaDesde = "", DBNull.Value, strFechaDesde)
            SQLCommand.Parameters.Add("pFechaHasta", SqlDbType.Char, 10).Value = IIf(strFechaHasta = "", DBNull.Value, strFechaHasta)
            SQLCommand.Parameters.Add("pCodEstado", SqlDbType.VarChar, 3).Value = IIf(strCodEstado = "", "L", strCodEstado)
            SQLCommand.Parameters.Add("pTipoLista", SqlDbType.Char, 1).Value = strTipoLista
            SQLCommand.Parameters.Add("pLista", SqlDbType.Text).Value = strLista
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Int).Value = glngNegocioOperacion
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Int).Value = glngIdUsuario

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_MovimientosCaja, Lstr_NombreTabla)

            LDS_MovimientosCaja.Tables(0).TableName = "SALDO_CAJA"
            LDS_MovimientosCaja.Tables(1).TableName = "MOV_CAJA"

            If strColumnas.Trim = "" Then
                LDS_MovimientosCaja_Aux = LDS_MovimientosCaja
            Else
                LDS_MovimientosCaja_Aux = LDS_MovimientosCaja.Copy
                For Each Columna In LDS_MovimientosCaja.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_MovimientosCaja_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_MovimientosCaja_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            Return LDS_MovimientosCaja_Aux
        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function TraerCajasxCuenta(ByVal lngNegocio As Long, _
                                      ByVal lngIdCajaCuenta As Long, _
                                      ByVal strIdCuenta As String, _
                                      ByVal strCodMercado As String, _
                                      ByVal strCodMoneda As String, _
                                      ByVal strColumnas As String, _
                                      ByRef strRetorno As String, _
                                      Optional ByVal strSubClase As String = "", _
                                      Optional ByVal strAccion As String = "", _
                                      Optional ByVal intFlgOperativa As Integer = 0, _
                                      Optional ByVal strEstadoCaja As String = "") As DataSet

        Dim LDS_Cajas As New DataSet
        Dim LDS_Cajas_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Caja_Consultar"
        Lstr_NombreTabla = "CAJAS"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Float, 10).Value = IIf(lngNegocio = 0, DBNull.Value, lngNegocio)
            SQLCommand.Parameters.Add("pIdCajaCuenta", SqlDbType.Float, 10).Value = IIf(lngIdCajaCuenta = 0, DBNull.Value, lngIdCajaCuenta)
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = IIf(strIdCuenta = "", DBNull.Value, strIdCuenta)
            SQLCommand.Parameters.Add("pCodMercado", SqlDbType.VarChar, 4).Value = IIf(strCodMercado = "", DBNull.Value, strCodMercado)
            SQLCommand.Parameters.Add("pCodMoneda", SqlDbType.VarChar, 3).Value = IIf(strCodMoneda = "", DBNull.Value, strCodMoneda)
            SQLCommand.Parameters.Add("pSubClase", SqlDbType.VarChar, 10).Value = IIf(strSubClase = "", DBNull.Value, strSubClase)
            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 10).Value = IIf(strAccion = "", DBNull.Value, strAccion)
            SQLCommand.Parameters.Add("pflgOperativa", SqlDbType.Char, 1).Value = IIf(intFlgOperativa = 0, DBNull.Value, intFlgOperativa)
            SQLCommand.Parameters.Add("pEstadoCaja", SqlDbType.VarChar, 3).Value = IIf(strEstadoCaja = "", DBNull.Value, strEstadoCaja)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Cajas, Lstr_NombreTabla)

            LDS_Cajas_Aux = LDS_Cajas

            If strColumnas.Trim = "" Then
                LDS_Cajas_Aux = LDS_Cajas
            Else
                LDS_Cajas_Aux = LDS_Cajas.Copy
                For Each Columna In LDS_Cajas.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Cajas_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Cajas_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            Return LDS_Cajas_Aux
        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function TraerTipoMovmientosCaja(ByVal strCodOrigenMovCaja As String, _
                                            ByVal strCodtipoCarAboCaja As String, _
                                            ByVal strFlgAportePatrimonial As String, _
                                            ByVal strColumnas As String, _
                                            ByRef strResultado As String, _
                                            Optional ByVal strSinRelOperacion As String = "N") As DataSet

        Dim LDS_TipoMovCaja As New DataSet
        Dim LDS_TipoMovCaja_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_TipoMovCaja_Consultar"
        Lstr_NombreTabla = "TIPO_MOV_CAJA"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pCodOrigenMovCaja", SqlDbType.VarChar, 10).Value = IIf(strCodOrigenMovCaja = "", DBNull.Value, strCodOrigenMovCaja)
            SQLCommand.Parameters.Add("pCodtipoCarAboCaja", SqlDbType.Char, 1).Value = IIf(strCodtipoCarAboCaja = "", DBNull.Value, strCodtipoCarAboCaja)
            SQLCommand.Parameters.Add("pFlgFlujoPatrimonial", SqlDbType.Char, 1).Value = IIf(strFlgAportePatrimonial = "", DBNull.Value, strFlgAportePatrimonial)
            SQLCommand.Parameters.Add("pSinRelOperacion", SqlDbType.Char, 1).Value = strSinRelOperacion


            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_TipoMovCaja, Lstr_NombreTabla)

            LDS_TipoMovCaja_Aux = LDS_TipoMovCaja


            If strColumnas.Trim = "" Then
                LDS_TipoMovCaja_Aux = LDS_TipoMovCaja
            Else
                LDS_TipoMovCaja_Aux = LDS_TipoMovCaja.Copy
                For Each Columna In LDS_TipoMovCaja.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_TipoMovCaja_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_TipoMovCaja_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strResultado = "OK"
            Return LDS_TipoMovCaja_Aux

        Catch ex As Exception
            strResultado = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function TraerTipoMovmientosCajaAdministrados(ByVal strFechaLiquidacion As String, _
                                            ByVal strCodOrigMovCaja As String, _
                                            ByVal intIdCuenta As Integer, _
                                            ByVal strCodEstado As String, _
                                            ByVal strColumnas As String, _
                                            ByRef strResultado As String) As DataSet

        Dim LDS_TipoMovCaja As New DataSet
        Dim LDS_TipoMovCaja_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Liquidacion_Caja_Buscar"
        Lstr_NombreTabla = "TIPO_MOV_CAJA"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pFechaLiquidacion", SqlDbType.Char, 10).Value = strFechaLiquidacion
            SQLCommand.Parameters.Add("pCodOrigMovCaja", SqlDbType.Char, 10).Value = IIf(strCodOrigMovCaja = "", DBNull.Value, strCodOrigMovCaja)
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Decimal, 10).Value = IIf(intIdCuenta = 0, DBNull.Value, intIdCuenta)
            SQLCommand.Parameters.Add("pCodEstado", SqlDbType.Char, 10).Value = strCodEstado

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_TipoMovCaja, Lstr_NombreTabla)

            LDS_TipoMovCaja_Aux = LDS_TipoMovCaja


            If strColumnas.Trim = "" Then
                LDS_TipoMovCaja_Aux = LDS_TipoMovCaja
            Else
                LDS_TipoMovCaja_Aux = LDS_TipoMovCaja.Copy
                For Each Columna In LDS_TipoMovCaja.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_TipoMovCaja_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_TipoMovCaja_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strResultado = "OK"
            Return LDS_TipoMovCaja_Aux

        Catch ex As Exception
            strResultado = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function TraerMovCajaxFechaLiquidacion(ByVal strFechaLiquidacion As String, _
                              ByVal strCodOrigMovCaja As String, _
                              ByVal lngIdCuenta As Long, _
                              ByVal lngIdInstrumento As Long, _
                              ByVal strColumnas As String, _
                              ByRef strRetorno As String) As DataSet


        Dim LDS_Cajas As New DataSet
        Dim LDS_Cajas_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Liquidacion_Caja_Buscar"
        Lstr_NombreTabla = "CAJAS"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pFechaLiquidacion", SqlDbType.Char, 10).Value = IIf(strFechaLiquidacion = "", DBNull.Value, strFechaLiquidacion)
            SQLCommand.Parameters.Add("pCodOrigMovCaja", SqlDbType.Char, 10).Value = IIf(strCodOrigMovCaja = "", DBNull.Value, strCodOrigMovCaja)
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Decimal, 10).Value = IIf(lngIdCuenta = 0, DBNull.Value, lngIdCuenta)
            SQLCommand.Parameters.Add("IdInstrumento", SqlDbType.Decimal, 10).Value = IIf(lngIdCuenta = 0, DBNull.Value, lngIdCuenta)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Cajas, Lstr_NombreTabla)

            LDS_Cajas_Aux = LDS_Cajas


            If strColumnas.Trim = "" Then
                LDS_Cajas_Aux = LDS_Cajas
            Else
                LDS_Cajas_Aux = LDS_Cajas.Copy
                For Each Columna In LDS_Cajas.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Cajas_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Cajas_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            Return LDS_Cajas_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function ModificaMovimientosCaja(ByVal intIdMovCaja As Integer, _
                                        ByVal strFechaLiquidacion As String, _
                                        ByVal dblMonto As Double, _
                                        ByVal strCodEstado As String) As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction
        Dim llngIdOperacion As Long = 0
        Dim llngIdMovCaja As Long = 0

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_Liquidacion_Caja_Mantener", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Liquidacion_Caja_Mantener"
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdMovCaja", SqlDbType.Decimal, 10).Value = intIdMovCaja
            SQLCommand.Parameters.Add("pFechaLiquidacion", SqlDbType.Char, 10).Value = strFechaLiquidacion
            SQLCommand.Parameters.Add("pMonto", SqlDbType.Decimal, 20, 6).Value = dblMonto
            SQLCommand.Parameters.Add("pCodEstado", SqlDbType.VarChar, 3).Value = strCodEstado

            SQLCommand.ExecuteNonQuery()

            MiTransaccionSQL.Commit()

            strDescError = "OK"

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error al Grabar Movimientos Caja " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            ModificaMovimientosCaja = strDescError
        End Try
    End Function

    Public Sub TraerSaldoCaja(ByVal lngIdCajaCuenta As Long, _
                              ByVal lngIdCuenta As Long, _
                              ByVal strFechaConsulta As String, _
                              ByVal strCodMoneda As String, _
                              ByVal strCodMercado As String, _
                              ByVal strConsideraOrden As String, _
                              ByRef dblSaldoCaja As Double)


        Dim Sqlcommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim lstrProcedimiento As String
        Dim LstrNombreTabla As String
        Dim DS_Paso As New DataSet

        lstrProcedimiento = "Rcp_Caja_ConsultarSaldo"
        LstrNombreTabla = "Caja"
        'Abre la conexion
        Connect.Abrir()

        Try
            Sqlcommand = New SqlCommand(lstrProcedimiento, Connect.Conexion)

            Sqlcommand.CommandType = CommandType.StoredProcedure
            Sqlcommand.CommandText = lstrProcedimiento
            Sqlcommand.CommandTimeout = 0
            Sqlcommand.Parameters.Clear()

            Sqlcommand.Parameters.Add("pIdCajaCuenta", SqlDbType.Float, 10).Value = lngIdCajaCuenta
            Sqlcommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = lngIdCuenta
            Sqlcommand.Parameters.Add("pFecha", SqlDbType.Char, 10).Value = strFechaConsulta
            Sqlcommand.Parameters.Add("pCodMoneda", SqlDbType.VarChar, 3).Value = strCodMoneda
            Sqlcommand.Parameters.Add("pCodMercado", SqlDbType.VarChar, 4).Value = strCodMercado
            Sqlcommand.Parameters.Add("pConsideraOrden", SqlDbType.Char, 1).Value = strConsideraOrden

            Dim ParametroSal As New SqlClient.SqlParameter("pSaldoCajaCuenta", SqlDbType.Float, 10)
            ParametroSal.Direction = ParameterDirection.Output
            Sqlcommand.Parameters.Add(ParametroSal)

            SQLDataAdapter.SelectCommand = Sqlcommand
            SQLDataAdapter.Fill(DS_Paso, LstrNombreTabla)

            dblSaldoCaja = Sqlcommand.Parameters("pSaldoCajaCuenta").Value

        Catch Ex As Exception
            dblSaldoCaja = 0
        Finally
            Connect.Cerrar()
        End Try
    End Sub

    Public Function GuardarMovimientosCaja(ByVal strCodOrigenMovCaja As String, _
                                        ByVal lngIdCierre As Long, _
                                        ByVal lngIdTipoEstado As Long, _
                                        ByVal strCodEstado As String, _
                                        ByVal lngIdCajacuenta As Long, _
                                        ByVal strCodTipoCarAboCaja As String, _
                                        ByVal strFechaMovimiento As String, _
                                        ByVal strFechaLiquidacion As String, _
                                        ByVal dblMonto As Double, _
                                        ByVal strDescMovCaja As String, _
                                        ByVal strMovAutomatico As String, _
                                        ByVal strConsideraOrden As String, _
                                        ByVal strOperaConCredito As String, _
                                        ByVal strObsMovCaja As String, _
                                        Optional ByRef lngIdMovCaja As Long = 0, _
                                        Optional ByVal StrMedioPagoCobro As String = "", _
                                        Optional ByVal StrConsideraWorkFlow As String = "S", _
                                        Optional ByVal lngFolioOperacion As Long = 0, _
                                        Optional ByVal lngLineaOperacion As Long = 0, _
                                        Optional ByVal lngNominales As Double = 0, _
                                        Optional ByVal lngInteres As Double = 0) As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction
        Dim llngIdOperacion As Long = 0
        Dim llngIdMovCaja As Long = 0

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_MovCaja_Guardar", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_MovCaja_Guardar"
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pCodOrigenMovCaja", SqlDbType.VarChar, 10).Value = strCodOrigenMovCaja
            SQLCommand.Parameters.Add("pIdCierre", SqlDbType.Float, 10).Value = lngIdCierre
            SQLCommand.Parameters.Add("pIdTipoEstado", SqlDbType.Float, 10).Value = lngIdTipoEstado
            SQLCommand.Parameters.Add("pCodEstado", SqlDbType.VarChar, 3).Value = strCodEstado
            SQLCommand.Parameters.Add("pIdCajaCuenta", SqlDbType.Float, 10).Value = lngIdCajacuenta
            SQLCommand.Parameters.Add("pCodTipoCarAboCaja", SqlDbType.Char, 1).Value = strCodTipoCarAboCaja
            SQLCommand.Parameters.Add("pFechaMovimiento", SqlDbType.Char, 10).Value = strFechaMovimiento
            SQLCommand.Parameters.Add("pFechaLiquidacion", SqlDbType.Char, 10).Value = strFechaLiquidacion
            SQLCommand.Parameters.Add("pMonto", SqlDbType.Float, 20).Value = dblMonto
            SQLCommand.Parameters.Add("pDscMovCaja", SqlDbType.VarChar, 120).Value = strDescMovCaja
            SQLCommand.Parameters.Add("pMovAutomatico", SqlDbType.VarChar, 1).Value = strMovAutomatico
            SQLCommand.Parameters.Add("pConsideraOrden", SqlDbType.Char, 1).Value = strConsideraOrden
            SQLCommand.Parameters.Add("pSobregiro", SqlDbType.Char, 1).Value = strOperaConCredito
            SQLCommand.Parameters.Add("pObsMovCaja", SqlDbType.VarChar, 200).Value = strObsMovCaja
            SQLCommand.Parameters.Add("pIdCartera ", SqlDbType.Float, 18).Value = DBNull.Value
            SQLCommand.Parameters.Add("pSecCartera ", SqlDbType.Float, 18).Value = DBNull.Value
            SQLCommand.Parameters.Add("pNominales ", SqlDbType.Float, 26).Value = lngNominales
            SQLCommand.Parameters.Add("pCapital ", SqlDbType.Float, 26).Value = DBNull.Value
            SQLCommand.Parameters.Add("pInteres ", SqlDbType.Float, 26).Value = lngInteres
            SQLCommand.Parameters.Add("pCodMedioPagoCobro", SqlDbType.VarChar, 20).Value = StrMedioPagoCobro
            SQLCommand.Parameters.Add("pConsideraWorkFlow", SqlDbType.VarChar, 10).Value = StrConsideraWorkFlow
            SQLCommand.Parameters.Add("pFolioOperacion ", SqlDbType.Float, 10).Value = lngFolioOperacion
            SQLCommand.Parameters.Add("pLineaOperacion ", SqlDbType.Float, 10).Value = lngLineaOperacion

            '...Id Solicitud de Inversion
            Dim ParametroSal2 As New SqlClient.SqlParameter("pIdMovCaja", SqlDbType.Float, 10)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            '...Resultado
            Dim ParametroSal As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            llngIdMovCaja = SQLCommand.Parameters("pIdMovCaja").Value
            lngIdMovCaja = llngIdMovCaja

            If strDescError = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error al Grabar Movimientos Caja " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            GuardarMovimientosCaja = strDescError
        End Try

        If strDescError = "OK" Then
            If gobjParametro.Tesoreria_Envio.ValorParametro = "S" Then
                Dim lcInterfaceTesoreria As New clsInterfaceTesoreria
                lcInterfaceTesoreria.InformarIngresoCaja(llngIdMovCaja)
            End If
        End If
    End Function

    Public Function GuardarMovimientosCajaAsig(ByVal strCodOrigenMovCaja As String, _
                                               ByVal lngIdCierre As Long, _
                                               ByVal lngIdTipoEstado As Long, _
                                               ByVal strCodEstado As String, _
                                               ByVal lngIdCajacuenta As Long, _
                                               ByVal strCodTipoCarAboCaja As String, _
                                               ByVal strFechaMovimiento As String, _
                                               ByVal strFechaLiquidacion As String, _
                                               ByVal dblMonto As Double, _
                                               ByVal strDescMovCaja As String, _
                                               ByVal strMovAutomatico As String, _
                                               ByVal strConsideraOrden As String, _
                                               ByVal strOperaConCredito As String, _
                                               ByVal dblIdOperacion As Double, _
                                               ByRef lngIdMovCaja As Long) As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction
        Dim llngIdOperacion As Long = 0
        Dim llngIdMovCaja As Long = 0

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_MovCajaAsig_Guardar", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_MovCajaAsig_Guardar"
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pCodOrigenMovCaja", SqlDbType.VarChar, 10).Value = strCodOrigenMovCaja
            SQLCommand.Parameters.Add("pIdCierre", SqlDbType.Float, 10).Value = lngIdCierre
            SQLCommand.Parameters.Add("pIdTipoEstado", SqlDbType.Float, 10).Value = lngIdTipoEstado
            SQLCommand.Parameters.Add("pCodEstado", SqlDbType.VarChar, 3).Value = strCodEstado
            SQLCommand.Parameters.Add("pIdCajaCuenta", SqlDbType.Float, 10).Value = lngIdCajacuenta
            SQLCommand.Parameters.Add("pCodTipoCarAboCaja", SqlDbType.Char, 1).Value = strCodTipoCarAboCaja
            SQLCommand.Parameters.Add("pFechaMovimiento", SqlDbType.Char, 10).Value = strFechaMovimiento
            SQLCommand.Parameters.Add("pFechaLiquidacion", SqlDbType.Char, 10).Value = strFechaLiquidacion
            SQLCommand.Parameters.Add("pMonto", SqlDbType.Float, 20).Value = dblMonto
            SQLCommand.Parameters.Add("pDscMovCaja", SqlDbType.VarChar, 120).Value = strDescMovCaja
            SQLCommand.Parameters.Add("pMovAutomatico", SqlDbType.VarChar, 1).Value = strMovAutomatico
            SQLCommand.Parameters.Add("pConsideraOrden", SqlDbType.Char, 1).Value = strConsideraOrden
            SQLCommand.Parameters.Add("pSobregiro", SqlDbType.Char, 1).Value = strOperaConCredito
            SQLCommand.Parameters.Add("pIdOperacion", SqlDbType.Float, 10).Value = dblIdOperacion

            '...Id Solicitud de Inversion
            Dim ParametroSal2 As New SqlClient.SqlParameter("pIdMovCaja", SqlDbType.Float, 10)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            '...Resultado
            Dim ParametroSal As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If Trim(strDescError) = "OK" Then
                lngIdMovCaja = SQLCommand.Parameters("pIdMovCaja").Value.ToString.Trim
                MiTransaccionSQL.Commit()
                Return ("OK")
            Else
                lngIdMovCaja = 0
                MiTransaccionSQL.Rollback()
                Return strDescError
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error al Grabar Movimientos Caja " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            GuardarMovimientosCajaAsig = strDescError
        End Try
    End Function

    Public Function Caja_Mantenedor(ByVal strId_Cuenta As String, ByRef dstCaja As DataSet) As String
        Dim strDescError As String = "OK"
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            If dstCaja.Tables(0).Rows.Count <> Nothing Then
                For Each pRow As DataRow In dstCaja.Tables(0).Rows
                    SQLCommand = New SqlClient.SqlCommand("Rcp_Caja_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)
                    SQLCommand.CommandType = CommandType.StoredProcedure
                    SQLCommand.CommandText = "Rcp_Caja_Mantencion"
                    SQLCommand.CommandTimeout = 0
                    SQLCommand.Parameters.Clear()

                    If pRow("ID_CAJA_CUENTA").ToString = "0" Then
                        SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = "INSERTAR"
                        SQLCommand.Parameters.Add("pIdCajaCuenta", SqlDbType.Float, 10).Value = DBNull.Value
                    ElseIf pRow("ID_CAJA_CUENTA").ToString <> "0" And pRow("EST_CAJA_CUENTA").ToString = "VIG" Then
                        SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = "MODIFICAR"
                        SQLCommand.Parameters.Add("pIdCajaCuenta", SqlDbType.Float, 10).Value = pRow("ID_CAJA_CUENTA").ToString
                    ElseIf pRow("ID_CAJA_CUENTA").ToString <> "0" And pRow("EST_CAJA_CUENTA").ToString = "ELI" Then
                        SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = "ELIMINAR"
                        SQLCommand.Parameters.Add("pIdCajaCuenta", SqlDbType.Float, 10).Value = pRow("ID_CAJA_CUENTA").ToString
                    End If

                    SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = CLng("0" & strId_Cuenta)
                    SQLCommand.Parameters.Add("pCodMercado", SqlDbType.VarChar, 4).Value = pRow("COD_MERCADO").ToString
                    SQLCommand.Parameters.Add("pCodMoneda", SqlDbType.VarChar, 3).Value = pRow("COD_MONEDA").ToString
                    SQLCommand.Parameters.Add("pDscCajaCuenta", SqlDbType.VarChar, 50).Value = pRow("DSC_CAJA_CUENTA").ToString
                    SQLCommand.Parameters.Add("pEstCajaCuenta", SqlDbType.VarChar, 3).Value = pRow("EST_CAJA_CUENTA").ToString
                    SQLCommand.Parameters.Add("pCajaOperativa", SqlDbType.Bit, 1).Value = pRow("CAJA_OPERATIVA").ToString

                    '...Resultado
                    Dim pSalInstPort As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                    pSalInstPort.Direction = Data.ParameterDirection.Output
                    SQLCommand.Parameters.Add(pSalInstPort)

                    SQLCommand.ExecuteNonQuery()

                    strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
                    If Not strDescError.ToUpper.Trim = "OK" Then
                        Exit For
                    End If
                Next
            End If

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            Caja_Mantenedor = strDescError
        End Try
    End Function

    Public Function TraeDatosMonedaPago(ByVal lngId_Cuenta As Long, _
                                        ByVal strCod_MonedaPago As String, _
                                        ByVal strCod_Estado As String, _
                                        ByVal strColumnas As String, _
                                        ByRef strRetorno As String, _
                                        Optional ByVal strCodMonedaExc As String = "", _
                                        Optional ByVal strEsMonedaPagoConUf As String = "NO") As DataSet

        Dim LDS_CajaCuenta As New DataSet
        Dim LDS_CajaCuenta_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Caja_Cuenta_Buscar_MP"
        Lstr_NombreTabla = "Caja_Cuenta"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("@pIdCuenta", SqlDbType.Float, 10).Value = IIf(lngId_Cuenta = 0, DBNull.Value, lngId_Cuenta)
            SQLCommand.Parameters.Add("@pCodMonedaPago", SqlDbType.VarChar, 10).Value = IIf(strCod_MonedaPago = "", DBNull.Value, strCod_MonedaPago)
            SQLCommand.Parameters.Add("@pCodEstado", SqlDbType.VarChar, 10).Value = IIf(strCod_Estado = "", DBNull.Value, strCod_Estado)
            SQLCommand.Parameters.Add("@pCodMonedaExc", SqlDbType.VarChar, 3).Value = IIf(strCodMonedaExc = "", DBNull.Value, strCodMonedaExc)
            SQLCommand.Parameters.Add("pFlgEsMonedaPagoConUF", SqlDbType.VarChar, 2).Value = strEsMonedaPagoConUf

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_CajaCuenta, Lstr_NombreTabla)

            LDS_CajaCuenta_Aux = LDS_CajaCuenta

            If strColumnas.Trim = "" Then
                LDS_CajaCuenta_Aux = LDS_CajaCuenta
            Else
                LDS_CajaCuenta_Aux = LDS_CajaCuenta.Copy
                For Each Columna In LDS_CajaCuenta.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_CajaCuenta_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_CajaCuenta_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_CajaCuenta.Dispose()
            Return LDS_CajaCuenta_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function MovCaja_Modificar(ByVal strAccion As String, _
                                      ByVal strIdCuenta As String, _
                                      ByVal dblIdMovCaja As Double, _
                                      ByVal strIdCajaCuenta As String, _
                                      ByVal strMonto As String, _
                                      ByVal strFechaLiquidacion As String, _
                                      ByVal strFechaOperacion As String, _
                                      ByVal strObsMovCaja As String, _
                                      ByVal strCodEstado As String, _
                                      ByVal strMovAutomatico As String, _
                                      Optional ByVal strObsConcepAnu As String = "", _
                                      Optional ByVal StrMedioPagoCobro As String = "") As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_MovCaja_Modificar", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_MovCaja_Modificar"
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 10).Value = strAccion
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = strIdCuenta
            SQLCommand.Parameters.Add("pIdMovCaja", SqlDbType.Float, 10).Value = dblIdMovCaja
            SQLCommand.Parameters.Add("pIdCajaCuenta ", SqlDbType.Float, 10).Value = strIdCajaCuenta
            SQLCommand.Parameters.Add("pMonto", SqlDbType.Float, 20).Value = strMonto
            SQLCommand.Parameters.Add("pFechaLiquidacion", SqlDbType.Char, 10).Value = strFechaLiquidacion
            SQLCommand.Parameters.Add("pFechaOperacion", SqlDbType.Char, 10).Value = strFechaOperacion
            SQLCommand.Parameters.Add("pObsMovCaja", SqlDbType.VarChar, 200).Value = strObsMovCaja
            SQLCommand.Parameters.Add("pCodEstado", SqlDbType.VarChar, 3).Value = strCodEstado
            SQLCommand.Parameters.Add("pMovAutomatico", SqlDbType.VarChar, 1).Value = strMovAutomatico
            SQLCommand.Parameters.Add("pObsConcepAnu", SqlDbType.VarChar, 200).Value = strObsConcepAnu
            SQLCommand.Parameters.Add("pCodMedioPagoCobro", SqlDbType.VarChar, 20).Value = StrMedioPagoCobro

            '...Resultado
            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            MovCaja_Modificar = strDescError
        End Try
    End Function

    Public Function ConsultaMovimientosCaja(ByVal intIdCuenta As Integer, _
                                            ByVal intIdCliente As Integer, _
                                            ByVal intIdGrupoCuenta As Integer, _
                                            ByVal strFechaDesde As String, _
                                            ByVal strFechaHasta As String, _
                                            ByVal strCodMonedaConsulta As String, _
                                            ByRef strRetorno As String) As DataSet

        Dim LDS_MovimientosCaja As New DataSet
        Dim LDS_MovimientosCaja_Aux As New DataSet
        Dim Lstr_NombreTabla As String
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_CajaCuenta_ConsultaMovimientos"
        Lstr_NombreTabla = "Movimientos"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Int).Value = glngNegocioOperacion
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Int).Value = glngIdUsuario
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Int).Value = IIf(intIdCuenta = 0, DBNull.Value, intIdCuenta)
            SQLCommand.Parameters.Add("pIdCliente", SqlDbType.Int).Value = IIf(intIdCliente = 0, DBNull.Value, intIdCliente)
            SQLCommand.Parameters.Add("pIdGrupoCuenta", SqlDbType.Int).Value = IIf(intIdGrupoCuenta = 0, DBNull.Value, intIdGrupoCuenta)
            SQLCommand.Parameters.Add("pFechaDesde", SqlDbType.Char, 10).Value = IIf(strFechaDesde = "", DBNull.Value, strFechaDesde)
            SQLCommand.Parameters.Add("pFechaHasta", SqlDbType.Char, 10).Value = IIf(strFechaHasta = "", DBNull.Value, strFechaHasta)
            SQLCommand.Parameters.Add("pCodMdaCons", SqlDbType.VarChar, 3).Value = IIf(strCodMonedaConsulta = "", DBNull.Value, strCodMonedaConsulta)

            '...Resultado
            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_MovimientosCaja, Lstr_NombreTabla)

            strRetorno = "OK"
            Return LDS_MovimientosCaja

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function ConsultaDetalleMovimientosCaja(ByVal intIdCuenta As Integer, _
                                                   ByVal intIdCliente As Integer, _
                                                   ByVal intIdGrupoCuenta As Integer, _
                                                   ByVal strFechaDesde As String, _
                                                   ByVal strFechaHasta As String, _
                                                   ByVal strCodMonedaCaja As String, _
                                                   ByVal strCodMovCaja As String, _
                                                   ByRef strColumnasTotales As String, _
                                                   ByRef strColumnasDecMdaCaja As String, _
                                                   ByRef strRetorno As String, _
                                                   Optional ByVal strCargo_Abono As String = "") As DataSet

        Dim LDS_MovimientosCaja As New DataSet
        Dim LDS_MovimientosCaja_Aux As New DataSet
        Dim Lstr_NombreTabla As String
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_CajaCuenta_DetalleMovimientoCaja"
        Lstr_NombreTabla = "Movimientos"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Int).Value = glngNegocioOperacion
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Int).Value = glngIdUsuario
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Int).Value = IIf(intIdCuenta = 0, DBNull.Value, intIdCuenta)
            SQLCommand.Parameters.Add("pIdCliente", SqlDbType.Int).Value = IIf(intIdCliente = 0, DBNull.Value, intIdCliente)
            SQLCommand.Parameters.Add("pIdGrupoCuenta", SqlDbType.Int).Value = IIf(intIdGrupoCuenta = 0, DBNull.Value, intIdGrupoCuenta)
            SQLCommand.Parameters.Add("pFechaDesde", SqlDbType.Char, 10).Value = IIf(strFechaDesde = "", DBNull.Value, strFechaDesde)
            SQLCommand.Parameters.Add("pFechaHasta", SqlDbType.Char, 10).Value = IIf(strFechaHasta = "", DBNull.Value, strFechaHasta)
            SQLCommand.Parameters.Add("pCodMonedaCaja", SqlDbType.VarChar, 3).Value = IIf(strCodMonedaCaja = "", DBNull.Value, strCodMonedaCaja)
            SQLCommand.Parameters.Add("pCodMovCaja", SqlDbType.VarChar, 10).Value = IIf(strCodMovCaja = "", DBNull.Value, strCodMovCaja)
            SQLCommand.Parameters.Add("pCargoAbono", SqlDbType.VarChar, 5).Value = IIf(strCargo_Abono = "", DBNull.Value, strCargo_Abono)

            '...Columnas a totalizar
            Dim ParametroCol As New SqlClient.SqlParameter("pColTotal", SqlDbType.VarChar, 300)
            ParametroCol.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroCol)

            '...Columnas a formatear segun moneda caja
            Dim ParametroColDecMdaCaja As New SqlClient.SqlParameter("pColDecMdaCaja", SqlDbType.VarChar, 200)
            ParametroColDecMdaCaja.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroColDecMdaCaja)

            '...Resultado
            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_MovimientosCaja, Lstr_NombreTabla)
            If IsDBNull(SQLCommand.Parameters("pColTotal").Value) Then
                strColumnasTotales = ""
            Else
                strColumnasTotales = SQLCommand.Parameters("pColTotal").Value
            End If

            If IsDBNull(SQLCommand.Parameters("pColDecMdaCaja").Value) Then
                strColumnasDecMdaCaja = ""
            Else
                strColumnasDecMdaCaja = SQLCommand.Parameters("pColDecMdaCaja").Value
            End If

            strRetorno = SQLCommand.Parameters("pResultado").Value
            Return LDS_MovimientosCaja

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function ValidarCaja( _
                                ByVal strTipoOperacion As String, _
                                ByVal intIdCuenta As Integer, _
                                ByVal intIdcajaCuenta As Integer, _
                                ByVal dblIdInstrumento As Double, _
                                ByVal strCodMoneda As String, _
                                ByVal strCodMercado As String, _
                                ByVal dblMonto As Double, _
                                ByVal strFecha As String, _
                                ByVal strFormaOperarCartera As String, _
                                ByVal dblIdCartera As Double, _
                                ByRef strRetorno As String, _
                                Optional ByVal strFlgFlujoPatrimonial As String = "N" _
                                ) As DataSet


        Dim LDS_MovimientosCaja As New DataSet
        Dim LDS_MovimientosCaja_Aux As New DataSet
        Dim Lstr_NombreTabla As String
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_CajaCuenta_Validar"
        Lstr_NombreTabla = "validaIngreso"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pTipoOperacion", SqlDbType.VarChar, 20).Value = strTipoOperacion
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Int).Value = IIf(intIdCuenta = 0, DBNull.Value, intIdCuenta)
            SQLCommand.Parameters.Add("pIdCajaCuenta", SqlDbType.Int).Value = IIf(intIdcajaCuenta = 0, DBNull.Value, intIdcajaCuenta)
            SQLCommand.Parameters.Add("pIdInstrumento", SqlDbType.Float, 10).Value = IIf(dblIdInstrumento = 0, DBNull.Value, dblIdInstrumento)
            SQLCommand.Parameters.Add("pCodMoneda", SqlDbType.VarChar, 3).Value = IIf(strCodMoneda = "", DBNull.Value, strCodMoneda)
            SQLCommand.Parameters.Add("pCodMercado", SqlDbType.VarChar, 4).Value = IIf(strCodMercado = "", DBNull.Value, strCodMercado)
            SQLCommand.Parameters.Add("pMonto", SqlDbType.Float).Value = IIf(dblMonto = 0, DBNull.Value, dblMonto)
            SQLCommand.Parameters.Add("pFecha", SqlDbType.VarChar, 10).Value = IIf(strFecha = "", DBNull.Value, strFecha)
            SQLCommand.Parameters.Add("pFormaOperarCartera", SqlDbType.VarChar, 1).Value = strFormaOperarCartera
            SQLCommand.Parameters.Add("pIdCartera", SqlDbType.Int).Value = IIf(dblIdCartera = 0, DBNull.Value, dblIdCartera)
            SQLCommand.Parameters.Add("pFlgFlujoPatrimonial", SqlDbType.VarChar, 1).Value = strFlgFlujoPatrimonial
            SQLCommand.Parameters.Add("pIdNegocio ", SqlDbType.Int).Value = glngNegocioOperacion

            '...Resultado
            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)


            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_MovimientosCaja, Lstr_NombreTabla)

            strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            Return LDS_MovimientosCaja

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function SaldoCaja_Mantenedor(ByVal strAccion As String, _
                                         ByVal lngIdCajaCuenta As Long, _
                                         ByVal dblMonto_Mon_Caja As Double, _
                                         ByVal dblMonto_Mon_Cuenta As Double, _
                                         ByVal strFecha_Cierre As String, _
                                         ByVal dblMonto_x_Cobrar_Mon_Cta As Double, _
                                         ByVal dblMonto_x_Pagar_Mon_Cta As Double, _
                                         ByVal dblMonto_x_Cobrar_Mon_Caja As Double, _
                                         ByVal dblMonto_x_Pagar_Mon_Caja As Double, _
                                         ByVal strEst_Saldo_Caja As String, _
                                         ByRef lngIdSaldoCaja As String) As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction
        Dim llngIdOperacion As Long = 0
        Dim llngIdSaldoCaja As Long = 0

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            Dim intId_SaldoCaja As Integer = CLng("0" & lngIdSaldoCaja)

            SQLCommand = New SqlClient.SqlCommand("Rcp_Saldo_Caja_Mantenedor", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Saldo_Caja_Mantenedor"
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 10).Value = strAccion
            SQLCommand.Parameters.Add("pId_Caja_Cuenta", SqlDbType.Float, 10).Value = lngIdCajaCuenta
            SQLCommand.Parameters.Add("pMonto_Mon_Caja", SqlDbType.Float, 20).Value = dblMonto_Mon_Caja
            SQLCommand.Parameters.Add("pMonto_Mon_Cuenta", SqlDbType.Float, 20).Value = dblMonto_Mon_Cuenta
            SQLCommand.Parameters.Add("pFecha_Cierre", SqlDbType.Char, 10).Value = strFecha_Cierre
            SQLCommand.Parameters.Add("pMonto_x_Cobrar_Mon_Cta", SqlDbType.Float, 20).Value = dblMonto_x_Cobrar_Mon_Cta
            SQLCommand.Parameters.Add("pMonto_x_Pagar_Mon_Cta", SqlDbType.Float, 20).Value = Math.Abs(dblMonto_x_Pagar_Mon_Cta)
            SQLCommand.Parameters.Add("pMonto_x_Cobrar_Mon_Caja", SqlDbType.Float, 20).Value = dblMonto_x_Cobrar_Mon_Caja
            SQLCommand.Parameters.Add("pMonto_x_Pagar_Mon_Caja", SqlDbType.Float, 20).Value = Math.Abs(dblMonto_x_Pagar_Mon_Caja)
            SQLCommand.Parameters.Add("pEst_Saldo_Caja", SqlDbType.VarChar, 120).Value = strEst_Saldo_Caja

            'Id Saldo Caja
            Dim ParametroSal1 As New SqlClient.SqlParameter("pId_Saldo_Caja", SqlDbType.Float, 10)
            ParametroSal1.Value = intId_SaldoCaja
            ParametroSal1.Direction = Data.ParameterDirection.InputOutput
            SQLCommand.Parameters.Add(ParametroSal1)

            '...Resultado
            Dim ParametroSal As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError = "OK" Then
                MiTransaccionSQL.Commit()
                lngIdSaldoCaja = SQLCommand.Parameters("pId_Saldo_Caja").Value
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error al Grabar Saldos Caja " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            SaldoCaja_Mantenedor = strDescError
        End Try
    End Function

    Public Function CajaCuentas_CobrarPagar_Ver(ByVal intIdCuenta As Integer, _
                                        ByVal strModo As String, _
                                        ByVal strFecha As String, _
                                        ByVal strCodMonedaConsulta As String, _
                                        ByRef strRetorno As String) As DataSet

        Dim LDS_MovimientosCaja As New DataSet
        Dim LDS_MovimientosCaja_Aux As New DataSet
        Dim Lstr_NombreTabla As String
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_CajaCuentas_CobrarPagar_Buscar"
        Lstr_NombreTabla = "Caja"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Int).Value = IIf(intIdCuenta = 0, DBNull.Value, intIdCuenta)
            SQLCommand.Parameters.Add("pFecha", SqlDbType.Char, 10).Value = IIf(strFecha = "", DBNull.Value, strFecha)
            SQLCommand.Parameters.Add("pModo", SqlDbType.VarChar, 15).Value = IIf(strModo = "", DBNull.Value, strModo)
            SQLCommand.Parameters.Add("pMoneda", SqlDbType.VarChar, 3).Value = IIf(strCodMonedaConsulta = "", DBNull.Value, strCodMonedaConsulta)

            '...Resultado
            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_MovimientosCaja, Lstr_NombreTabla)

            strRetorno = "OK"
            Return LDS_MovimientosCaja

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function ConsultaMovimientosCajaParaImprimir(ByVal strLista As String, _
                                                        ByVal strTipoLista As String, _
                                                        ByVal strFechaDesde As String, _
                                                        ByVal strFechaHasta As String, _
                                                        ByVal strCodMoneda As String, _
                                                        ByVal strCodOrigenMovCaja As String, _
                                                        ByVal strCodEstado As String, _
                                                        ByVal strCodClase As String, _
                                                        ByVal strCodSubClase As String, _
                                                        ByVal intIdInstrumento As Integer, _
                                                        ByVal intIdContraparte As Integer, _
                                                        ByVal strColumnas As String, _
                                                        ByRef strRetorno As String) As DataSet
        Dim LDS_MovimientosCaja As New DataSet
        Dim LDS_MovimientosCaja_Aux As New DataSet
        Dim Lstr_NombreTabla As String
        Dim LstrProcedimiento
        Dim Remove As Boolean
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_NomCol As String

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_MovCaja_ConsultarParaImpresion"
        Lstr_NombreTabla = "Movimientos"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pLista", SqlDbType.Text).Value = strLista
            SQLCommand.Parameters.Add("pTipoLista", SqlDbType.Char, 1).Value = strTipoLista
            SQLCommand.Parameters.Add("pFechaDesde", SqlDbType.Char, 10).Value = IIf(strFechaDesde = "", DBNull.Value, strFechaDesde)
            SQLCommand.Parameters.Add("pFechaHasta", SqlDbType.Char, 10).Value = IIf(strFechaHasta = "", DBNull.Value, strFechaHasta)
            SQLCommand.Parameters.Add("pCodMoneda", SqlDbType.VarChar, 3).Value = IIf(strCodMoneda = "", DBNull.Value, strCodMoneda)
            SQLCommand.Parameters.Add("pCodOrigenMovCaja", SqlDbType.VarChar, 10).Value = IIf(strCodOrigenMovCaja = "", DBNull.Value, strCodOrigenMovCaja)
            SQLCommand.Parameters.Add("pCodEstado", SqlDbType.Char, 1).Value = IIf(strCodEstado = "", DBNull.Value, strCodEstado)
            SQLCommand.Parameters.Add("pCodClase", SqlDbType.VarChar, 15).Value = IIf(strCodClase = "", DBNull.Value, strCodClase)
            SQLCommand.Parameters.Add("pCodSubClase", SqlDbType.VarChar, 15).Value = IIf(strCodSubClase = "", DBNull.Value, strCodSubClase)
            SQLCommand.Parameters.Add("pIdInstrumento", SqlDbType.Decimal, 10).Value = IIf(intIdInstrumento = 0, DBNull.Value, intIdInstrumento)
            SQLCommand.Parameters.Add("pIdContraparte", SqlDbType.Decimal, 10).Value = IIf(intIdContraparte = 0, DBNull.Value, intIdContraparte)
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Int, 10).Value = glngNegocioOperacion
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Int, 10).Value = glngIdUsuario

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_MovimientosCaja, Lstr_NombreTabla)

            If strColumnas.Trim = "" Then
                LDS_MovimientosCaja_Aux = LDS_MovimientosCaja
            Else
                LDS_MovimientosCaja_Aux = LDS_MovimientosCaja.Copy
                For Each Columna In LDS_MovimientosCaja.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_MovimientosCaja_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_MovimientosCaja_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            Return LDS_MovimientosCaja
        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function TraerMovimientoCaja(ByVal lngIdMovCaja As Long, _
                                        ByVal lngIdComisionDetalle As Long, _
                                        ByVal strColumnas As String, _
                                        ByRef strRetorno As String) As DataSet

        Dim LDS_MovimientosCaja As New DataSet
        Dim LDS_MovimientosCaja_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_MovCaja_Trae_ParaImprimir"
        Lstr_NombreTabla = "MOV_CAJA"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdMovCaja", SqlDbType.Float, 10).Value = IIf(lngIdMovCaja = 0, DBNull.Value, lngIdMovCaja)
            SQLCommand.Parameters.Add("pIdComisionDetalle", SqlDbType.Float, 10).Value = IIf(lngIdComisionDetalle = 0, DBNull.Value, lngIdComisionDetalle)
            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_MovimientosCaja, Lstr_NombreTabla)

            LDS_MovimientosCaja.Tables(0).TableName = "MOV_CAJA"

            If strColumnas.Trim = "" Then
                LDS_MovimientosCaja_Aux = LDS_MovimientosCaja
            Else
                LDS_MovimientosCaja_Aux = LDS_MovimientosCaja.Copy
                For Each Columna In LDS_MovimientosCaja.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_MovimientosCaja_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_MovimientosCaja_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            Return LDS_MovimientosCaja_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function MovCaja_FormaPagoCobro_Guardar(ByVal strAccion As String, _
                                                   ByVal llngIdMovCaja As Long, _
                                                   ByVal dtMedioPagoCobro As DataTable) As String

        Dim strDescError As String = "OK"
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction
        Dim lstrprocedure As String

        Dim lstrResultadoCadena As String = ""

        'GENERA CADENA DE MEDIOS DE COBRO/PAGOS
        Dim lstrColumnasFormaPago As String = "ID_CUENTA © " & _
                                              "ID_TIPO_ESTADO © " & _
                                              "COD_ESTADO © " & _
                                              "BANCO_ID © " & _
                                              "COD_MEDIO_PAGO_COBRO © " & _
                                              "FECHA_MOVIMIENTO © " & _
                                              "FECHA_DOCUMENTO © " & _
                                              "NUM_DOCUMENTO © " & _
                                              "RETENCION © " & _
                                              "MONTO © " & _
                                              "CTA_CTE_BANCARIA"
        Dim lstrMediosCobroPago As String = ""
        lstrResultadoCadena = ""
        If IsNothing(dtMedioPagoCobro) OrElse dtMedioPagoCobro.Rows.Count = 0 Then
            lstrMediosCobroPago = ""
        Else
            lstrResultadoCadena = GeneraCadena(dtMedioPagoCobro, lstrColumnasFormaPago, "©", lstrMediosCobroPago)
            If lstrResultadoCadena <> "OK" Then
                Return lstrResultadoCadena & vbCrLf & "En Medios de Cobro/Pago"
            End If
        End If

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion
        lstrprocedure = "Rcp_MovCaja_FormaPagoCobro_Guardar"

        Try

            SQLCommand = New SqlClient.SqlCommand(lstrprocedure, MiTransaccionSQL.Connection, MiTransaccionSQL)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = lstrprocedure
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 3).Value = strAccion
            SQLCommand.Parameters.Add("@pid_mov_caja", SqlDbType.Int, 10).Value = llngIdMovCaja
            SQLCommand.Parameters.Add("pMediosCobroPago", SqlDbType.VarChar, 8000).Value = IIf(lstrMediosCobroPago = "", DBNull.Value, lstrMediosCobroPago)

            '...Resultado
            Dim ParametroSalComm As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSalComm.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSalComm)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If Trim(strDescError) = "OK" Then
                MiTransaccionSQL.Commit()
                Return ("OK")
            Else
                MiTransaccionSQL.Rollback()
                Return strDescError
            End If


        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error al actualizar detalle de comisiones" & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            MovCaja_FormaPagoCobro_Guardar = strDescError
        End Try
    End Function

    Public Function Consultar_Caja_Movimientos(ByVal intIdCajaCuenta As String) As String
        Dim LstrProcedimiento As String

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim strRetorno As String

        LstrProcedimiento = "Rcp_Caja_Mov_Consultar"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdCajaCuenta", SqlDbType.Int, 10).Value = intIdCajaCuenta

            '...Resultado
            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()

            strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            Return strRetorno
        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

End Class

