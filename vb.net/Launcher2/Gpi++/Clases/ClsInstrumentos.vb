﻿Imports System.Data.SqlClient

Public Class ClsInstrumentos

    Public Function TraerNemos(ByVal strCodClaseInstrumento As String, _
                             ByVal strCodSubClaseInstrumento As String, _
                             ByVal dblIdInstrumento As Double, _
                             ByVal strCodEstado As String, _
                             ByVal strNemotecnico As String, _
                             ByVal strEmisor As String, _
                             ByVal strCodMoneda As String, _
                             ByVal lngIdPerfilRiesgo As Long, _
                             ByVal strColumnas As String, _
                             ByRef strRetorno As String, _
                             Optional ByVal dblIdCuenta As Double = 0, _
                             Optional ByVal strCodExternoInstrumento As String = "", _
                             Optional ByVal strCodNormaInstrumento As String = "", _
                             Optional ByVal strOpcionDividendo As String = "", _
                             Optional ByVal strCodFamilia As String = "") As DataSet

        Dim LDS_Nemotecnico As New DataSet
        Dim LDS_Nemotecnico_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Instrumento_Buscar"
        Lstr_NombreTabla = "NEMOS"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pCodClaseInstrumento", SqlDbType.VarChar, 15).Value = IIf(strCodClaseInstrumento = "", DBNull.Value, strCodClaseInstrumento)
            SQLCommand.Parameters.Add("pCodSubClaseInstrumento", SqlDbType.VarChar, 10).Value = IIf(strCodSubClaseInstrumento = "", DBNull.Value, strCodSubClaseInstrumento)
            SQLCommand.Parameters.Add("pIdInstrumento", SqlDbType.Float, 10).Value = IIf(dblIdInstrumento = 0, DBNull.Value, dblIdInstrumento)
            SQLCommand.Parameters.Add("pCodEstado", SqlDbType.VarChar, 3).Value = IIf(strCodEstado = "", DBNull.Value, strCodEstado)
            SQLCommand.Parameters.Add("pNemotecnico", SqlDbType.VarChar, 100).Value = IIf(strNemotecnico = "", DBNull.Value, strNemotecnico)
            SQLCommand.Parameters.Add("pDscEmisor", SqlDbType.VarChar, 100).Value = IIf(strEmisor = "", DBNull.Value, strEmisor)
            SQLCommand.Parameters.Add("pCodMoneda", SqlDbType.VarChar, 3).Value = IIf(strCodMoneda = "", DBNull.Value, strCodMoneda)
            SQLCommand.Parameters.Add("pIdPerfilRiesgo", SqlDbType.Float, 3).Value = IIf(lngIdPerfilRiesgo = 0, DBNull.Value, lngIdPerfilRiesgo)
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = dblIdCuenta
            SQLCommand.Parameters.Add("pCodExternoInstrumento", SqlDbType.VarChar, 15).Value = IIf(strCodExternoInstrumento = "", DBNull.Value, strCodExternoInstrumento)
            SQLCommand.Parameters.Add("pCodNormaInstrumento", SqlDbType.VarChar, 10).Value = IIf(strCodNormaInstrumento = "", DBNull.Value, strCodNormaInstrumento)
            SQLCommand.Parameters.Add("pOpcionDividendo", SqlDbType.VarChar, 10).Value = strOpcionDividendo
            SQLCommand.Parameters.Add("pCodFamilia", SqlDbType.VarChar, 10).Value = IIf(strCodFamilia = "", DBNull.Value, strCodFamilia)
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Int).Value = glngNegocioOperacion
            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Nemotecnico, Lstr_NombreTabla)

            LDS_Nemotecnico_Aux = LDS_Nemotecnico

            If strColumnas.Trim = "" Then
                LDS_Nemotecnico_Aux = LDS_Nemotecnico
            Else
                LDS_Nemotecnico_Aux = LDS_Nemotecnico.Copy
                For Each Columna In LDS_Nemotecnico.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Nemotecnico_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Nemotecnico_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            Return LDS_Nemotecnico_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function TraerNemos_FFMM(ByVal strCodExternoInstrumento As String, _
                                    ByVal strSerie As String, _
                                    ByVal strRutParticipe As String, _
                                    ByVal strCtaExterna As String, _
                                    ByVal strColumnas As String, _
                                    ByRef strRetorno As String) As DataSet

        Dim LDS_Nemotecnico As New DataSet
        Dim LDS_Nemotecnico_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento
        Dim strDescError As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Busca_NEMO_GPI_x_FFMM"
        Lstr_NombreTabla = "NEMOS"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()


            SQLCommand.Parameters.Add("pCod_Externo", SqlDbType.VarChar, 20).Value = strCodExternoInstrumento
            SQLCommand.Parameters.Add("pSerie", SqlDbType.VarChar, 10).Value = strSerie
            SQLCommand.Parameters.Add("pRutparticipe", SqlDbType.VarChar, 12).Value = strRutParticipe
            SQLCommand.Parameters.Add("pCtaExterna", SqlDbType.VarChar, 10).Value = strCtaExterna

            Dim ParametroSal1 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 100)
            ParametroSal1.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal1)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Nemotecnico, Lstr_NombreTabla)

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim <> "OK" Then
                strRetorno = strDescError.ToUpper.Trim
            Else
                LDS_Nemotecnico_Aux = LDS_Nemotecnico
                If strColumnas.Trim = "" Then
                    LDS_Nemotecnico_Aux = LDS_Nemotecnico
                Else
                    LDS_Nemotecnico_Aux = LDS_Nemotecnico.Copy
                    For Each Columna In LDS_Nemotecnico.Tables(Lstr_NombreTabla).Columns
                        Remove = True
                        For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                            LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                            If Columna.ColumnName = LInt_NomCol Then
                                Remove = False
                            End If
                        Next
                        If Remove Then
                            LDS_Nemotecnico_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                        End If
                    Next
                End If

                For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                    LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                    For Each Columna In LDS_Nemotecnico_Aux.Tables(Lstr_NombreTabla).Columns
                        If Columna.ColumnName = LInt_NomCol Then
                            Columna.SetOrdinal(LInt_Col)
                            Exit For
                        End If
                    Next
                Next

                strRetorno = "OK"
            End If
            Return LDS_Nemotecnico_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function


    Public Function TraerNemotecnico(ByVal StrNemotecnico As String, _
                                     ByVal StrInstrumento As String, _
                                     ByVal StrSubClase As String, _
                                     ByVal StrEmisor As String, _
                                     ByVal dblIdCuenta As Double, _
                                     ByVal strFechaOperacion As String, _
                                     ByVal strColumnas As String, _
                                     ByRef strRetorno As String, _
                                     Optional ByVal strCodMoneda As String = "", _
                                     Optional ByVal strCodFamilia As String = "") As DataSet

        Dim LDS_Nemo As New DataSet
        Dim LDS_Nemo_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Instrumento_FormBuscarIngreso"
        Lstr_NombreTabla = "NEMOS"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("@pNemotecnico", SqlDbType.VarChar, 100).Value = IIf(StrNemotecnico.Trim = "", DBNull.Value, StrNemotecnico)
            SQLCommand.Parameters.Add("@pCodClaseInstrumento", SqlDbType.VarChar, 10).Value = IIf(StrInstrumento = "", DBNull.Value, StrInstrumento)
            SQLCommand.Parameters.Add("@pCodSubClaseInstrumento", SqlDbType.VarChar, 10).Value = IIf(StrSubClase.Trim = "", DBNull.Value, StrSubClase.Trim)
            SQLCommand.Parameters.Add("@pDscEmisor", SqlDbType.VarChar, 100).Value = IIf(StrEmisor.Trim = "", DBNull.Value, StrEmisor.Trim)
            SQLCommand.Parameters.Add("@pIdCuenta", SqlDbType.Float, 10).Value = IIf(dblIdCuenta = 0, DBNull.Value, dblIdCuenta)
            SQLCommand.Parameters.Add("@pFechaConsulta", SqlDbType.VarChar, 10).Value = IIf(strFechaOperacion = "", DBNull.Value, strFechaOperacion)
            SQLCommand.Parameters.Add("@pCodMoneda", SqlDbType.VarChar, 3).Value = IIf(strCodMoneda = "", DBNull.Value, strCodMoneda)
            SQLCommand.Parameters.Add("pCodFamilia", SqlDbType.VarChar, 10).Value = IIf(strCodFamilia = "", DBNull.Value, strCodFamilia)
            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Nemo, Lstr_NombreTabla)

            LDS_Nemo_Aux = LDS_Nemo

            If strColumnas.Trim = "" Then
                LDS_Nemo_Aux = LDS_Nemo
            Else
                LDS_Nemo_Aux = LDS_Nemo.Copy
                For Each Columna In LDS_Nemo.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Nemo_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Nemo_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            Return LDS_Nemo_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Sub TraerPrecioMercado(ByVal strIdNegocio As String, _
                                  ByVal strIdCuenta As String, _
                                  ByVal strIdInstrumento As String, _
                                  ByVal strCodSubClaseInstrumento As String, _
                                  ByVal strFechaConsulta As String, _
                                  ByVal strConsideraUltimoPrecioConocido As String, _
                                  ByRef dblUltimoPrecio As Double, _
                                  ByRef strDescError As String)


        Dim DS_Paso As New DataSet
        Dim lstrProcedimiento As String = ""
        Dim LstrNombreTabla As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion

        'Abre la conexion
        lstrProcedimiento = "Rcp_Instrumento_BuscarPrecio"
        LstrNombreTabla = "PASO"
        Connect.Abrir()

        Try

            SQLCommand = New SqlCommand(lstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = lstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Float, 9).Value = CLng(strIdNegocio)
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 9).Value = CLng(strIdCuenta)
            SQLCommand.Parameters.Add("pIdInstrumento", SqlDbType.Float, 9).Value = CLng(strIdInstrumento)
            SQLCommand.Parameters.Add("pCodSubClaseInstrumento", SqlDbType.VarChar, 10).Value = strCodSubClaseInstrumento
            SQLCommand.Parameters.Add("pFecha", SqlDbType.Char, 10).Value = strFechaConsulta
            SQLCommand.Parameters.Add("pConsideraUltimoPrecioConocido", SqlDbType.Char, 1).Value = strConsideraUltimoPrecioConocido

            Dim ParametroSal1 As New SqlClient.SqlParameter("pUltimoPrecioCtaNemo", SqlDbType.Float, 10)
            ParametroSal1.Direction = ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal1)

            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(DS_Paso, LstrNombreTabla)

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                dblUltimoPrecio = SQLCommand.Parameters("pUltimoPrecioCtaNemo").Value
            Else
                dblUltimoPrecio = 0
            End If

        Catch Ex As Exception
            strDescError = "Error en el Búsqueda de Precio Mercado." & vbCr & Ex.Message
        Finally
            Connect.Cerrar()
        End Try
    End Sub

    Public Function TraerEstados(ByRef strRetorno As String) As DataSet

        Dim LDS_Estados As New DataSet
        Dim LDS_Estados_Aux As New DataSet
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Estado_Buscar"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdTipoEstado", SqlDbType.Float, 10).Value = 3 ' id de tipo estado para nemotecnicos

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Estados)

            LDS_Estados_Aux = LDS_Estados

            strRetorno = "OK"
            Return LDS_Estados_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function TraeInversionRescate() As DataSet

        Dim DS_InverResc As New DataSet
        Dim dtbInverResc As New DataTable
        Dim drNewRow As DataRow

        With dtbInverResc

            .Columns.Add("DESCRIPCION", GetType(String))
            .Columns.Add("CODIGO", GetType(String))

            ' VIGENTE
            drNewRow = dtbInverResc.NewRow
            drNewRow("DESCRIPCION") = "CONOCIDO"
            drNewRow("CODIGO") = "C"
            Call dtbInverResc.Rows.Add(drNewRow)

            ' ANULADO
            drNewRow = dtbInverResc.NewRow
            drNewRow("DESCRIPCION") = "DESCONOCIDO"
            drNewRow("CODIGO") = "D"
            Call dtbInverResc.Rows.Add(drNewRow)

        End With

        DS_InverResc.Tables.Add(dtbInverResc)
        Return DS_InverResc
    End Function

    Public Function TraePeriodicidad() As DataSet

        Dim DS_InverResc As New DataSet
        Dim dtbInverResc As New DataTable
        Dim drNewRow As DataRow

        With dtbInverResc

            .Columns.Add("DESCRIPCION", GetType(String))
            .Columns.Add("CODIGO", GetType(String))

            drNewRow = dtbInverResc.NewRow
            drNewRow("DESCRIPCION") = "1"
            drNewRow("CODIGO") = "1"
            Call dtbInverResc.Rows.Add(drNewRow)

            drNewRow = dtbInverResc.NewRow
            drNewRow("DESCRIPCION") = "2"
            drNewRow("CODIGO") = "2"
            Call dtbInverResc.Rows.Add(drNewRow)

            drNewRow = dtbInverResc.NewRow
            drNewRow("DESCRIPCION") = "3"
            drNewRow("CODIGO") = "3"
            Call dtbInverResc.Rows.Add(drNewRow)

            drNewRow = dtbInverResc.NewRow
            drNewRow("DESCRIPCION") = "6"
            drNewRow("CODIGO") = "6"
            Call dtbInverResc.Rows.Add(drNewRow)

            drNewRow = dtbInverResc.NewRow
            drNewRow("DESCRIPCION") = "12"
            drNewRow("CODIGO") = "12"
            Call dtbInverResc.Rows.Add(drNewRow)

            drNewRow = dtbInverResc.NewRow
            drNewRow("DESCRIPCION") = "24"
            drNewRow("CODIGO") = "24"
            Call dtbInverResc.Rows.Add(drNewRow)

            drNewRow = dtbInverResc.NewRow
            drNewRow("DESCRIPCION") = "36"
            drNewRow("CODIGO") = "36"
            Call dtbInverResc.Rows.Add(drNewRow)


        End With

        DS_InverResc.Tables.Add(dtbInverResc)
        Return DS_InverResc
    End Function

    Public Function TraeDiasLiquidez() As DataSet

        Dim DS_InverResc As New DataSet
        Dim dtbInverResc As New DataTable
        Dim drNewRow As DataRow

        With dtbInverResc

            .Columns.Add("DESCRIPCION", GetType(String))
            .Columns.Add("CODIGO", GetType(String))

            drNewRow = dtbInverResc.NewRow
            drNewRow("DESCRIPCION") = "0"
            drNewRow("CODIGO") = "0"
            Call dtbInverResc.Rows.Add(drNewRow)

            drNewRow = dtbInverResc.NewRow
            drNewRow("DESCRIPCION") = "1"
            drNewRow("CODIGO") = "1"
            Call dtbInverResc.Rows.Add(drNewRow)

            drNewRow = dtbInverResc.NewRow
            drNewRow("DESCRIPCION") = "2"
            drNewRow("CODIGO") = "2"
            Call dtbInverResc.Rows.Add(drNewRow)

            drNewRow = dtbInverResc.NewRow
            drNewRow("DESCRIPCION") = "3"
            drNewRow("CODIGO") = "3"
            Call dtbInverResc.Rows.Add(drNewRow)

            drNewRow = dtbInverResc.NewRow
            drNewRow("DESCRIPCION") = "4"
            drNewRow("CODIGO") = "4"
            Call dtbInverResc.Rows.Add(drNewRow)

        End With

        DS_InverResc.Tables.Add(dtbInverResc)
        Return DS_InverResc
    End Function

    Public Function Instrumento_Mantenedor(ByVal strAccion As String, ByVal dblIdInstrumento As Double, ByVal strCodPais As String, _
                                          ByVal dblIdUsuarioInsert As Double, ByVal dblIdEmisorEspecifico As Double, ByVal dblIdTipoEstado As Double, _
                                          ByVal strCodEstado As String, ByVal dblCodMercadoTransaccion As Double, ByVal dblIdUsuarioUpdate As Double, _
                                          ByVal strCodMoneda As String, ByVal strCodSubClaseInstrumento As String, ByVal dblIdEmisorEspecificoOrigen As Double, _
                                          ByVal strCodMonedaTransaccion As String, ByVal strNemotecnico As String, ByVal strDscInstrumento As String, _
                                          ByVal dblTasaEmision As Double, ByVal strTipoTasa As String, ByVal strPeriodicidad As String, _
                                          ByVal strFechaVencimiento As String, ByVal dblCorteMinimoPapel As Double, ByVal dblMontoEmision As Double, _
                                          ByVal dblDiasLiquidez As Double, ByVal dblBase As Double, ByVal strFechaEmision As String, _
                                          ByVal strFlgFungible As String, ByVal strFlgTipoCuotaIngreso As String, ByVal strFlgTipoCuotaEgreso As String, _
                                          ByVal strpCodExternoInstrumento As String, ByVal strpCodNormaInstrumento As String, ByVal dblIdNormativo As Double, _
                                          ByVal strCadenaAtributosInstrumento As String, ByRef strId_Entidad As String, ByVal intTipoEndtidad As Integer, _
                                          ByVal strCodFamilia As String, _
                                          ByVal DS_Alias As DataSet, ByVal dblMinimoInversion As Double, ByVal strSerie As String, ByVal dblTipoFFMM As Double, _
                                          ByRef strDescError As String, Optional ByVal bTratarAlias As Boolean = True, Optional ByVal strIsin As String = "", _
                                          Optional ByVal strCuisip As String = "", Optional ByVal dblTasaEfectiva As Double = 0, _
                                          Optional ByVal dblNumeroCupones As Double = 0, Optional ByVal dblBaseFecha As Double = 0, _
                                          Optional ByVal strCodigoSerie As String = "", Optional ByVal strTipoInteres As String = "", Optional ByVal dblMaximoInv As Double = 0, _
                                          Optional ByVal DS_Atributos As DataSet = Nothing, _
                                          Optional ByVal pCod_Clase_Instrumento As String = "", _
                                          Optional ByVal bitdiversificado As Boolean = False, Optional ByVal bitparticipadiversificacion As Boolean = False) As String


        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_Instrumento_Mantenedor", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Instrumento_Mantenedor"
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = strAccion
            SQLCommand.Parameters.Add("pIdInstrumento", SqlDbType.Float, 9).Value = dblIdInstrumento
            SQLCommand.Parameters.Add("pCodPais", SqlDbType.VarChar, 10).Value = IIf(strCodPais = "", DBNull.Value, strCodPais)
            SQLCommand.Parameters.Add("pIdUsuarioInsert", SqlDbType.Float, 10).Value = IIf(dblIdUsuarioInsert = 0, DBNull.Value, dblIdUsuarioInsert)
            SQLCommand.Parameters.Add("pIdEmisorEspecifico", SqlDbType.Float, 10).Value = IIf(dblIdEmisorEspecifico = 0, DBNull.Value, dblIdEmisorEspecifico)
            SQLCommand.Parameters.Add("pIdTipoEstado", SqlDbType.Float, 4).Value = dblIdTipoEstado
            SQLCommand.Parameters.Add("pCodEstado", SqlDbType.VarChar, 3).Value = strCodEstado
            SQLCommand.Parameters.Add("pIdMercadoTransaccion", SqlDbType.Float, 10).Value = IIf(dblCodMercadoTransaccion = 0, DBNull.Value, dblCodMercadoTransaccion)
            SQLCommand.Parameters.Add("pIdUsuarioUpdate", SqlDbType.Float, 10).Value = IIf(strAccion = "GUARDAR", DBNull.Value, dblIdUsuarioUpdate)
            SQLCommand.Parameters.Add("pCodMoneda", SqlDbType.VarChar, 3).Value = IIf(strCodMoneda = "", DBNull.Value, strCodMoneda)
            SQLCommand.Parameters.Add("pCodSubClaseInstrumento", SqlDbType.VarChar, 10).Value = IIf(strCodSubClaseInstrumento = "", DBNull.Value, strCodSubClaseInstrumento)
            SQLCommand.Parameters.Add("pIdEmisorEspecificoOrigen", SqlDbType.Float, 10).Value = IIf(dblIdEmisorEspecificoOrigen = 0, DBNull.Value, dblIdEmisorEspecificoOrigen)
            SQLCommand.Parameters.Add("pCodMonedaTransaccion", SqlDbType.VarChar, 3).Value = IIf(strCodMonedaTransaccion = "", DBNull.Value, strCodMonedaTransaccion)
            SQLCommand.Parameters.Add("pNemotecnico", SqlDbType.VarChar, 50).Value = strNemotecnico
            SQLCommand.Parameters.Add("pDscInstrumento", SqlDbType.VarChar, 120).Value = strDscInstrumento
            SQLCommand.Parameters.Add("pTasaEmision", SqlDbType.Float, 14.1).Value = dblTasaEmision
            SQLCommand.Parameters.Add("pTipoTasa", SqlDbType.VarChar, 10).Value = IIf(strTipoTasa = "", DBNull.Value, strTipoTasa)
            SQLCommand.Parameters.Add("pPeriodicidad", SqlDbType.VarChar, 10).Value = IIf(strPeriodicidad = "", DBNull.Value, strPeriodicidad)
            If pCod_Clase_Instrumento = "FFMM_INT" Or pCod_Clase_Instrumento = "FFMM_NAC" Then
                SQLCommand.Parameters.Add("pFechaVencimiento", SqlDbType.VarChar, 10).Value = DBNull.Value
            Else
                SQLCommand.Parameters.Add("pFechaVencimiento", SqlDbType.VarChar, 10).Value = IIf(strFechaVencimiento = "", DBNull.Value, strFechaVencimiento)
            End If
            SQLCommand.Parameters.Add("pCorteMinimoPapel", SqlDbType.Float, 10).Value = IIf(dblCorteMinimoPapel = 0, DBNull.Value, dblCorteMinimoPapel)
            SQLCommand.Parameters.Add("pMontoEmision", SqlDbType.Float, 18).Value = IIf(dblMontoEmision = 0, DBNull.Value, dblMontoEmision)
            SQLCommand.Parameters.Add("pDiasLiquidez", SqlDbType.Float, 3).Value = IIf(dblDiasLiquidez = 0, DBNull.Value, dblDiasLiquidez)
            SQLCommand.Parameters.Add("pBase", SqlDbType.Float, 18).Value = dblBase
            SQLCommand.Parameters.Add("pFechaEmision", SqlDbType.VarChar, 10).Value = IIf(strFechaEmision = "", DBNull.Value, strFechaEmision)
            SQLCommand.Parameters.Add("pFlgFungible", SqlDbType.VarChar, 1).Value = IIf(strFlgFungible = "", DBNull.Value, strFlgFungible)
            SQLCommand.Parameters.Add("pFlgTipoCuotaIngreso", SqlDbType.VarChar, 1).Value = IIf(strFlgTipoCuotaIngreso = "", DBNull.Value, strFlgTipoCuotaIngreso)
            SQLCommand.Parameters.Add("pFlgTipoCuotaEgreso", SqlDbType.VarChar, 1).Value = IIf(strFlgTipoCuotaEgreso = "", DBNull.Value, strFlgTipoCuotaEgreso)
            SQLCommand.Parameters.Add("pFchInsert", SqlDbType.VarChar, 10).Value = gstrFechaSistema
            SQLCommand.Parameters.Add("pFchUpdate", SqlDbType.VarChar, 10).Value = DBNull.Value
            SQLCommand.Parameters.Add("pCodExternoInstrumento", SqlDbType.Char, 20).Value = strpCodExternoInstrumento
            SQLCommand.Parameters.Add("pCodNormaInstrumento", SqlDbType.Char, 20).Value = strpCodNormaInstrumento
            SQLCommand.Parameters.Add("pIdNormativo", SqlDbType.Float, 10).Value = IIf(dblIdNormativo = 0, DBNull.Value, dblIdNormativo)
            SQLCommand.Parameters.Add("pCadenaAtributosInstrumento", SqlDbType.VarChar, 300).Value = strCadenaAtributosInstrumento
            SQLCommand.Parameters.Add("pMinimoInversion", SqlDbType.Float, 20).Value = IIf(dblMinimoInversion = 0, DBNull.Value, dblMinimoInversion)
            SQLCommand.Parameters.Add("pIdTipoFFMM", SqlDbType.Float, 10).Value = IIf(dblTipoFFMM = 0, DBNull.Value, dblTipoFFMM)
            SQLCommand.Parameters.Add("pSerie", SqlDbType.Char, 10).Value = IIf(strSerie = "", DBNull.Value, strSerie)
            SQLCommand.Parameters.Add("pIsin", SqlDbType.VarChar, 50).Value = IIf(strIsin = "", DBNull.Value, strIsin)
            SQLCommand.Parameters.Add("pCuisip", SqlDbType.VarChar, 50).Value = IIf(strCuisip = "", DBNull.Value, strCuisip)
            SQLCommand.Parameters.Add("pTasaEfectiva", SqlDbType.Float, 14).Value = dblTasaEfectiva ' IIf(dblTasaEfectiva = 0, DBNull.Value, dblTasaEfectiva)
            SQLCommand.Parameters.Add("pNumeroCupones", SqlDbType.Int).Value = IIf(dblNumeroCupones = 0, DBNull.Value, dblNumeroCupones)
            SQLCommand.Parameters.Add("pBaseFecha", SqlDbType.Float, 18).Value = IIf(dblBaseFecha = 0, DBNull.Value, dblBaseFecha)
            SQLCommand.Parameters.Add("pCodigoSerie", SqlDbType.VarChar, 50).Value = IIf(strCodigoSerie = "", DBNull.Value, strCodigoSerie)
            SQLCommand.Parameters.Add("pTipoInteres", SqlDbType.VarChar, 50).Value = IIf(strTipoInteres = "", DBNull.Value, strTipoInteres)
            SQLCommand.Parameters.Add("pMaximoInv", SqlDbType.Float, 20).Value = IIf(dblMaximoInv = "0", DBNull.Value, dblMaximoInv)
            ' SQLCommand.Parameters.Add("pDiversificado", SqlDbType.Bit).Value = IIf(bitdiversificado = False, False, bitdiversificado)
            ' SQLCommand.Parameters.Add("pParticipaDiversificacion", SqlDbType.Bit).Value = IIf(bitparticipadiversificacion = False, False, bitparticipadiversificacion)
            'SQLCommand.Parameters.Add("pNombreSvs", SqlDbType.Char, 20).Value = IIf(strNombreSvs = "", DBNull.Value, strNombreSvs)
            SQLCommand.Parameters.Add("pCodFamilia", SqlDbType.VarChar, 10).Value = IIf(strCodFamilia = "", DBNull.Value, strCodFamilia)
            '...(Identity)
            Dim pSalInstr As New SqlClient.SqlParameter("pGenerado", SqlDbType.Float, 10)
            pSalInstr.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalInstr)

            '...Resultado
            Dim pSalResultInstr As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultInstr.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultInstr)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim <> "OK" Then
                MiTransaccionSQL.Rollback()
                Exit Function
            End If
            If strAccion = "GUARDAR" Then
                strId_Entidad = SQLCommand.Parameters("pGenerado").Value
            End If

            '___________________________________________________________________________________________________________

            '+ PARTE DE ALIAS
            If strDescError.ToUpper.Trim = "OK" Then
                If bTratarAlias Then
                    '+ ELIMINAMOS LOS ALIAS YA EXISTENTES
                    SQLCommand = New SqlClient.SqlCommand("Rcp_Alias_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)
                    SQLCommand.CommandType = CommandType.StoredProcedure
                    SQLCommand.CommandText = "Rcp_Alias_Mantencion"
                    SQLCommand.Parameters.Clear()

                    SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = "ELIMINAR"
                    SQLCommand.Parameters.Add("pIdEntidad", SqlDbType.Float, 10).Value = IIf(strId_Entidad.Trim = "", DBNull.Value, strId_Entidad.Trim)
                    SQLCommand.Parameters.Add("pIdEntidadGPI", SqlDbType.Float, 10).Value = intTipoEndtidad
                    SQLCommand.Parameters.Add("pIdEntidadContraparte", SqlDbType.VarChar, 9).Value = DBNull.Value
                    SQLCommand.Parameters.Add("pValor", SqlDbType.VarChar, 200).Value = DBNull.Value
                    SQLCommand.Parameters.Add("pCodEntidadContraparte", SqlDbType.VarChar, 50).Value = DBNull.Value

                    '...Resultado
                    Dim pSalElimIntPort As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                    pSalElimIntPort.Direction = Data.ParameterDirection.Output
                    SQLCommand.Parameters.Add(pSalElimIntPort)

                    SQLCommand.ExecuteNonQuery()

                    strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
                    If strDescError.ToUpper.Trim <> "OK" Then
                        MiTransaccionSQL.Rollback()
                        Exit Function
                    End If

                    '+ INSERTAMOS LOS NUEVOS ALIAS PARA LA ENTIDAD GPI 
                    If Not (IsNothing(DS_Alias)) Then
                        If DS_Alias.Tables(0).Rows.Count <> Nothing Then
                            For Each dtrRegristro As DataRow In DS_Alias.Tables(0).Rows
                                SQLCommand = New SqlClient.SqlCommand("Rcp_Alias_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)
                                SQLCommand.CommandType = CommandType.StoredProcedure
                                SQLCommand.CommandText = "Rcp_Alias_Mantencion"
                                SQLCommand.Parameters.Clear()

                                SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = "INSERTAR"
                                SQLCommand.Parameters.Add("pIdEntidad", SqlDbType.VarChar, 50).Value = strId_Entidad
                                SQLCommand.Parameters.Add("pIdEntidadGPI", SqlDbType.VarChar, 90).Value = intTipoEndtidad
                                SQLCommand.Parameters.Add("pIdEntidadContraparte", SqlDbType.VarChar, 9).Value = dtrRegristro("ID_ENTIDAD_CONTRAPARTE").ToString
                                SQLCommand.Parameters.Add("pValor", SqlDbType.VarChar, 200).Value = dtrRegristro("VALOR").ToString
                                SQLCommand.Parameters.Add("pCodEntidadContraparte", SqlDbType.VarChar, 50).Value = dtrRegristro("COD_ENTIDAD_CONTRAPARTE").ToString

                                '...Resultado
                                Dim pSalInstPort As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                                pSalInstPort.Direction = Data.ParameterDirection.Output
                                SQLCommand.Parameters.Add(pSalInstPort)

                                SQLCommand.ExecuteNonQuery()

                                strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
                                If strDescError.ToUpper.Trim <> "OK" Then
                                    Exit For
                                End If
                            Next
                        End If

                    End If
                End If
            End If
            If strDescError.ToUpper.Trim = "OK" Then
                Try
                    If Not (IsNothing(DS_Atributos)) Then
                        If DS_Atributos.Tables.Count > 0 And DS_Atributos.Tables(0).Rows.Count > 0 Then
                            For Each DR_Filas In DS_Atributos.Tables(0).Rows

                                SQLCommand = New SqlClient.SqlCommand("Lim_AtributoInstrumento_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)
                                SQLCommand.CommandType = CommandType.StoredProcedure
                                SQLCommand.CommandText = "Lim_AtributoInstrumento_Mantencion"
                                SQLCommand.Parameters.Clear()

                                SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 20).Value = "ELIMINAR"

                                SQLCommand.Parameters.Add("pIdAtributo", SqlDbType.Int, 10).Value = DR_Filas("ID_ATRIBUTO")
                                SQLCommand.Parameters.Add("pIdInstrumento", SqlDbType.Int, 10).Value = strId_Entidad
                                SQLCommand.Parameters.Add("pIdDatoAtributo", SqlDbType.Int, 12).Value = DR_Filas("ID_DATO_ATRIBUTO_LIM")
                                SQLCommand.Parameters.Add("pDscAtributo", SqlDbType.VarChar, 100).Value = DR_Filas("DSC_ATRIBUTO")
                                SQLCommand.Parameters.Add("pNemotecnico", SqlDbType.VarChar, 100).Value = strNemotecnico

                                '...Resultado
                                Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                                pSalResultado.Direction = Data.ParameterDirection.Output
                                SQLCommand.Parameters.Add(pSalResultado)

                                SQLCommand.ExecuteNonQuery()

                                strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim


                                If strDescError.ToUpper.Trim <> "OK" Then
                                    MiTransaccionSQL.Rollback()
                                    Exit For
                                End If


                            Next
                            For Each DR_Filas In DS_Atributos.Tables(0).Rows
                                If Not IsDBNull(DR_Filas("ID_DATO_ATRIBUTO_LIM")) Then
                                    SQLCommand = New SqlClient.SqlCommand("Lim_AtributoInstrumento_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)
                                    SQLCommand.CommandType = CommandType.StoredProcedure
                                    SQLCommand.CommandText = "Lim_AtributoInstrumento_Mantencion"
                                    SQLCommand.Parameters.Clear()

                                    SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 20).Value = "INSERTAR"

                                    SQLCommand.Parameters.Add("pIdAtributo", SqlDbType.Int, 10).Value = DR_Filas("ID_ATRIBUTO")
                                    SQLCommand.Parameters.Add("pIdInstrumento", SqlDbType.Int, 10).Value = strId_Entidad
                                    SQLCommand.Parameters.Add("pIdDatoAtributo", SqlDbType.Int, 12).Value = DR_Filas("ID_DATO_ATRIBUTO_LIM")
                                    SQLCommand.Parameters.Add("pDscAtributo", SqlDbType.VarChar, 100).Value = DR_Filas("DSC_ATRIBUTO")
                                    SQLCommand.Parameters.Add("pNemotecnico", SqlDbType.VarChar, 100).Value = strNemotecnico

                                    '...Resultado
                                    Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                                    pSalResultado.Direction = Data.ParameterDirection.Output
                                    SQLCommand.Parameters.Add(pSalResultado)

                                    SQLCommand.ExecuteNonQuery()

                                    strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

                                    If strDescError.ToUpper.Trim <> "OK" Then
                                        MiTransaccionSQL.Rollback()
                                        Exit For
                                    End If
                                End If
                            Next
                        End If
                    End If
                    ' -------------
                    If Trim(strDescError) = "OK" Then
                        MiTransaccionSQL.Commit()
                        Return ("OK")
                    Else
                        MiTransaccionSQL.Rollback()
                        Return strDescError
                    End If


                Catch Ex As Exception
                    MiTransaccionSQL.Rollback()
                    strDescError = "Error en la mantención Atrubutos/Instrumentos" & vbCr & Ex.Message
                Finally
                    SQLConnect.Cerrar()
                    Instrumento_Mantenedor = strDescError
                End Try
            End If
        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error: " & vbCr & Ex.Message

        Finally
            SQLConnect.Cerrar()
            Instrumento_Mantenedor = strDescError
        End Try
    End Function

    Public Function TraerNemotecnicoSVS(ByVal lngIdEmisor As Long, _
                                        ByVal strFechaVencimiento As String, _
                                        ByVal strCodMonedaDeposito As String, _
                                        ByVal strCodMonedaPago As String, _
                                        ByRef strNemotecnicoSVS As String, _
                                        ByRef strDescError As String) As String

        Dim DS_Paso As New DataSet
        Dim lstrProcedimiento As String = ""
        Dim LstrNombreTabla As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SqlLDataAdapter As New SqlClient.SqlDataAdapter
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion

        '...Abre la conexion
        lstrProcedimiento = "Rcp_Instrumento_GeneraNemotecnicoSvs"
        LstrNombreTabla = "PASO"
        SQLConnect.Abrir()

        Try
            SQLCommand = New SqlCommand(lstrProcedimiento, SQLConnect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = lstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdEmisor", SqlDbType.Float, 18).Value = lngIdEmisor
            SQLCommand.Parameters.Add("pFechaVencimiento", SqlDbType.Char, 10).Value = strFechaVencimiento
            SQLCommand.Parameters.Add("pCodMonedaDeposito", SqlDbType.VarChar, 3).Value = strCodMonedaDeposito
            SQLCommand.Parameters.Add("pCodMonedaPago", SqlDbType.VarChar, 3).Value = strCodMonedaPago

            '...(Valor Nemo SVS)
            Dim ParametroSal1 As New SqlClient.SqlParameter("pNemotecnico", SqlDbType.VarChar, 200)
            ParametroSal1.Direction = ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal1)

            '...Resultado
            Dim ParametroSal2 As New SqlClient.SqlParameter("pDescError", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SqlLDataAdapter.SelectCommand = SQLCommand
            SqlLDataAdapter.Fill(DS_Paso, LstrNombreTabla)

            strDescError = SQLCommand.Parameters("pDescError").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                strNemotecnicoSVS = SQLCommand.Parameters("pNemotecnico").Value.ToString.Trim
                Return strNemotecnicoSVS
            Else
                strNemotecnicoSVS = ""
            End If
            '___________________________________________________________________________________________________________

        Catch Ex As Exception
            strDescError = "Error: " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            TraerNemotecnicoSVS = strDescError
        End Try
    End Function
    Public Function TraerNemosxDatosExt(ByVal strCodExternoInstrumento As String, _
                                        ByRef strRetorno As String, _
                                        ByVal strColumnas As String, _
                                        ByVal strCodNormaInstrumento As String) As DataSet

        Dim LDS_Nemotecnico As New DataSet
        Dim LDS_Nemotecnico_Aux As New DataSet
        Dim Lstr_NombreTabla As String
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_InstrumentoExterno_Buscar"
        Lstr_NombreTabla = "NEMOS_EXTERNOS"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pCodExternoInstrumento", SqlDbType.VarChar, 15).Value = IIf(strCodExternoInstrumento = "", DBNull.Value, strCodExternoInstrumento)
            SQLCommand.Parameters.Add("pCodNormaInstrumento", SqlDbType.VarChar, 10).Value = IIf(strCodNormaInstrumento = "", DBNull.Value, strCodNormaInstrumento)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Nemotecnico, Lstr_NombreTabla)

            LDS_Nemotecnico_Aux = LDS_Nemotecnico
            If LDS_Nemotecnico.Tables(0).Rows.Count > 0 Then
                strRetorno = "OK"
            Else
                strRetorno = "No Existe Cod_Instrumento"
            End If
            Return LDS_Nemotecnico_Aux
        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function
    Public Function ArmarNemos(ByVal strSubClaseInstrumento As String, _
                                        ByRef strIdEmisor As Double, _
                                        ByVal strCodMonedaDep As String, _
                                        ByVal strFechaVcto As String, _
                                         ByRef strNemotecnico As String) As String

        Dim LDS_Nemotecnico As New DataSet
        Dim LDS_Nemotecnico_Aux As New DataSet
        Dim Lstr_NombreTabla As String
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_entrega_nemotecnico_IIF"
        Lstr_NombreTabla = "ARMA_NEMOS"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pSubClaseInstrumento", SqlDbType.VarChar, 15).Value = IIf(strSubClaseInstrumento = "", DBNull.Value, strSubClaseInstrumento)
            SQLCommand.Parameters.Add("pid_Emisor", SqlDbType.Float, 10).Value = IIf(strIdEmisor = 0, DBNull.Value, strIdEmisor)
            SQLCommand.Parameters.Add("pCod_Moneda_Deposito", SqlDbType.VarChar, 3).Value = IIf(strCodMonedaDep = "", DBNull.Value, strCodMonedaDep)
            SQLCommand.Parameters.Add("pFecha_Vencimiento", SqlDbType.VarChar, 10).Value = IIf(strFechaVcto = "", DBNull.Value, strFechaVcto)

            '...(Valor Nemo)
            Dim ParametroSal1 As New SqlClient.SqlParameter("pNemotecnico", SqlDbType.VarChar, 20)
            ParametroSal1.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal1)

            '...Resultado
            Dim ParametroSal2 As New SqlClient.SqlParameter("pError", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Nemotecnico, Lstr_NombreTabla)

            If SQLCommand.Parameters("pError").Value.ToString.Trim = "OK" Then
                strNemotecnico = SQLCommand.Parameters("pNemotecnico").Value.ToString.Trim
                Return "OK"
            Else
                strNemotecnico = ""
                Return SQLCommand.Parameters("pError").Value.ToString.Trim
            End If

        Catch ex As Exception
            Return ex.Message
        Finally
            Connect.Cerrar()
        End Try

    End Function
    Public Function TraerNemosOSA(ByVal strFechaConsulta As String, _
                                        ByRef strRetorno As String) As DataSet

        Dim LDS_NemoOSA As New DataSet
        Dim LDS_NemoOSA_Aux As New DataSet
        Dim Lstr_NombreTabla As String
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Instrumento_OSA_Buscar"
        Lstr_NombreTabla = "NEMOS_OSA"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pFechaConsulta", SqlDbType.VarChar, 10).Value = strFechaConsulta

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_NemoOSA, Lstr_NombreTabla)

            LDS_NemoOSA_Aux = LDS_NemoOSA
            If LDS_NemoOSA.Tables(0).Rows.Count > 0 Then
                strRetorno = "OK"
            Else
                strRetorno = "No Existen Instrumentos OSA para la fecha consultada"
            End If
            Return LDS_NemoOSA_Aux
        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function


    Public Function Trae_CodNemotecnico(ByVal lIntidIntruemento As Integer, _
                                   ByVal StrNemotecnico As String, _
                                   ByVal strColumnas As String, _
                                   ByRef strRetorno As String) As DataSet



        Dim LDS_CodNemo As New DataSet
        Dim LDS_CodNemo_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Nemotecnico_Buscar"
        Lstr_NombreTabla = "NEMOTECNICO"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("pIdInstrumento", SqlDbType.Float, 10).Value = IIf(lIntidIntruemento = 0, DBNull.Value, lIntidIntruemento)
            SQLCommand.Parameters.Add("pNemotecnico", SqlDbType.VarChar, 50).Value = IIf(StrNemotecnico.Trim = "", DBNull.Value, StrNemotecnico.Trim)


            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_CodNemo, Lstr_NombreTabla)

            LDS_CodNemo_Aux = LDS_CodNemo

            If strColumnas.Trim = "" Then
                LDS_CodNemo_Aux = LDS_CodNemo
            Else
                LDS_CodNemo_Aux = LDS_CodNemo.Copy
                For Each Columna In LDS_CodNemo.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_CodNemo_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_CodNemo_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_CodNemo.Dispose()
            Return LDS_CodNemo_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function Trae_Nemotecnico(ByVal idNemotecnico As Integer, ByRef strRetorno As String) As DataSet

        Dim LDS_Nemotecnico As New DataSet
        Dim LDS_Estados_Aux As New DataSet
        Dim LstrProcedimiento
        Dim Lstr_NombreTabla As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Instrumento_Buscar_X_IdNemotecnico"
        Lstr_NombreTabla = "GPI+"
        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("@pIdInstrumento", SqlDbType.Int, 10).Value = idNemotecnico

            Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultado.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Nemotecnico, Lstr_NombreTabla)

            strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            ' LDS_Estados_Aux = LDS_Estados

            ' strRetorno = "OK"
            Return LDS_Nemotecnico

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function
    Public Function InstrumentoAtributo_Consultar(ByVal intIdDatoAtributo As Integer, _
                                          ByVal intIdAtributo As Integer, _
                                          ByVal intIdTipoAtributo As Integer, _
                                          ByVal strEstado As String, _
                                          ByVal strColumnas As String, _
                                          ByRef strRetorno As String) As DataSet

        Dim LDS_DatoAtributo As New DataSet
        Dim LDS_DatoAtributo_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "InstrumentoAtributo_Consultar"
        Lstr_NombreTabla = "InstrumentoAtributo"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdDatoAtributo", SqlDbType.Int, 10).Value = IIf(intIdDatoAtributo = 0, DBNull.Value, intIdDatoAtributo)
            SQLCommand.Parameters.Add("pIdAtributo", SqlDbType.Int, 10).Value = IIf(intIdAtributo = 0, DBNull.Value, intIdAtributo)
            SQLCommand.Parameters.Add("pIdTipoAtributo", SqlDbType.Int, 10).Value = IIf(intIdTipoAtributo = 0, DBNull.Value, intIdTipoAtributo)
            SQLCommand.Parameters.Add("pCodEstado", SqlDbType.VarChar, 3).Value = IIf(strEstado = "", DBNull.Value, strEstado)
            '...Resultado
            Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultado.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_DatoAtributo, Lstr_NombreTabla)

            LDS_DatoAtributo_Aux = LDS_DatoAtributo

            If strColumnas.Trim = "" Then
                LDS_DatoAtributo_Aux = LDS_DatoAtributo
            Else
                LDS_DatoAtributo_Aux = LDS_DatoAtributo.Copy
                For Each Columna In LDS_DatoAtributo.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_DatoAtributo_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_DatoAtributo_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            LDS_DatoAtributo.Dispose()
            Return LDS_DatoAtributo_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function InstrumentoA_Consultar(ByVal intIdDatoAtributo As Integer, _
                                                   ByVal intIdAtributo As Integer, _
                                                   ByVal intIdTipoAtributo As Integer, _
                                                   ByVal strEstado As String, _
                                                   ByVal strColumnas As String, _
                                                   ByRef strRetorno As String) As DataSet

        Dim LDS_DatoAtributo As New DataSet
        Dim LDS_DatoAtributo_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_InstrumentoA_Consultar"
        Lstr_NombreTabla = "InstrumentoAtributo"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdDatoAtributo", SqlDbType.Int, 10).Value = IIf(intIdDatoAtributo = 0, DBNull.Value, intIdDatoAtributo)
            SQLCommand.Parameters.Add("pIdAtributo", SqlDbType.Int, 10).Value = IIf(intIdAtributo = 0, DBNull.Value, intIdAtributo)
            SQLCommand.Parameters.Add("pIdTipoAtributo", SqlDbType.Int, 10).Value = IIf(intIdTipoAtributo = 0, DBNull.Value, intIdTipoAtributo)
            SQLCommand.Parameters.Add("pCodEstado", SqlDbType.VarChar, 3).Value = IIf(strEstado = "", DBNull.Value, strEstado)
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Int, 10).Value = glngNegocioOperacion
            '...Resultado
            Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultado.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_DatoAtributo, Lstr_NombreTabla)

            LDS_DatoAtributo_Aux = LDS_DatoAtributo

            If strColumnas.Trim = "" Then
                LDS_DatoAtributo_Aux = LDS_DatoAtributo
            Else
                LDS_DatoAtributo_Aux = LDS_DatoAtributo.Copy
                For Each Columna In LDS_DatoAtributo.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_DatoAtributo_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_DatoAtributo_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            LDS_DatoAtributo.Dispose()
            Return LDS_DatoAtributo_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function InstrumentoAtributos_Consultar(ByVal intIdInstrumento As Integer, _
                                                  ByVal strEstado As String, _
                                                  ByVal strColumnas As String, _
                                                  ByRef strRetorno As String) As DataSet

        Dim LDS_AtributosInstrumento As New DataSet
        Dim LDS_AtributosInstrumento_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_InstrumentoAtributos_Consultar"
        Lstr_NombreTabla = "AtributosInstrumentos"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdInstrumento", SqlDbType.Int, 10).Value = IIf(intIdInstrumento = 0, DBNull.Value, intIdInstrumento)
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Int, 10).Value = glngNegocioOperacion
            'SQLCommand.Parameters.Add("pCodEstado", SqlDbType.VarChar, 3).Value = IIf(strEstado = "", DBNull.Value, strEstado)
            '...Resultado
            Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultado.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_AtributosInstrumento, Lstr_NombreTabla)

            LDS_AtributosInstrumento_Aux = LDS_AtributosInstrumento

            If strColumnas.Trim = "" Then
                LDS_AtributosInstrumento_Aux = LDS_AtributosInstrumento
            Else
                LDS_AtributosInstrumento_Aux = LDS_AtributosInstrumento.Copy
                For Each Columna In LDS_AtributosInstrumento.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_AtributosInstrumento_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_AtributosInstrumento_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            LDS_AtributosInstrumento.Dispose()
            Return LDS_AtributosInstrumento_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function TraerId_Nemotecnico(ByVal strNemotecnico As String, _
                                        ByVal lngIdEmisor As Long, _
                                        ByVal strFechaOperacion As String, _
                                        ByVal strFechaVencimiento As String, _
                                        ByVal strCodMonedaDeposito As String, _
                                        ByVal strCodMonedaPago As String, _
                                        ByVal strCodSubClase As String, _
                                        ByVal lngBase As Double, _
                                        ByRef dblIdgenerado As Double, _
                                        ByRef strDescError As String) As String

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_Trae_IdNemotecnico", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Trae_IdNemotecnico"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("@pNemotecnico", SqlDbType.VarChar, 50).Value = strNemotecnico
            SQLCommand.Parameters.Add("@pIdEmisor", SqlDbType.Float, 10).Value = lngIdEmisor
            SQLCommand.Parameters.Add("@pFechaOperacion", SqlDbType.VarChar, 10).Value = strFechaOperacion
            SQLCommand.Parameters.Add("@pFechaVencimiento", SqlDbType.VarChar, 10).Value = strFechaVencimiento
            SQLCommand.Parameters.Add("@pCodMoneda", SqlDbType.VarChar, 10).Value = strCodMonedaDeposito
            SQLCommand.Parameters.Add("@pCodMonedaTransaccion", SqlDbType.VarChar, 10).Value = strCodMonedaPago
            SQLCommand.Parameters.Add("@pSubClaseInstrumento", SqlDbType.VarChar, 50).Value = strCodSubClase
            SQLCommand.Parameters.Add("@pBase", SqlDbType.Float, 18).Value = lngBase


            '...(Identity)
            Dim pSalInstr As New SqlClient.SqlParameter("pGenerado", SqlDbType.Float, 10)
            pSalInstr.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalInstr)

            '...Resultado
            Dim pSalResultInstr As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultInstr.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultInstr)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            dblIdgenerado = SQLCommand.Parameters("pGenerado").Value

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()

            End If
            '___________________________________________________________________________________________________________

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error: " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            TraerId_Nemotecnico = strDescError

        End Try
    End Function
    Public Function Trae_CodNemotecnico_Todo(ByVal strNemotecnico As String, _
                                                  ByVal strColumnas As String, _
                                                  ByRef strRetorno As String) As DataSet

        Dim LDS_AtributosInstrumento As New DataSet
        Dim LDS_AtributosInstrumento_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Nemotecnico_Buscar_Completo"
        Lstr_NombreTabla = "INSTRUMENTO"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pNemotecnico", SqlDbType.VarChar, 10).Value = strNemotecnico

            Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultado.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)



            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_AtributosInstrumento, Lstr_NombreTabla)

            LDS_AtributosInstrumento_Aux = LDS_AtributosInstrumento

            If strColumnas.Trim = "" Then
                LDS_AtributosInstrumento_Aux = LDS_AtributosInstrumento
            Else
                LDS_AtributosInstrumento_Aux = LDS_AtributosInstrumento.Copy
                For Each Columna In LDS_AtributosInstrumento.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_AtributosInstrumento_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_AtributosInstrumento_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            LDS_AtributosInstrumento.Dispose()
            Return LDS_AtributosInstrumento_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Sub InstrumentoAtributo_Mantenedor(ByVal DR_Filas As DataRow, ByVal strId_Entidad As String, ByVal strNemotecnico As String, ByVal intValor As Integer, ByRef strDescError As String)

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            strDescError = "OK"

            SQLCommand = New SqlClient.SqlCommand("Lim_AtributoInstrumento_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Lim_AtributoInstrumento_Mantencion"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 20).Value = "ELIMINAR"

            SQLCommand.Parameters.Add("pIdAtributo", SqlDbType.Int, 10).Value = DR_Filas("ID_ATRIBUTO")
            SQLCommand.Parameters.Add("pIdInstrumento", SqlDbType.Int, 10).Value = strId_Entidad
            SQLCommand.Parameters.Add("pIdDatoAtributo", SqlDbType.Int, 12).Value = IIf(intValor = 0, DBNull.Value, intValor) 'DR_Filas("ID_DATO_ATRIBUTO_LIM")
            SQLCommand.Parameters.Add("pDscAtributo", SqlDbType.VarChar, 100).Value = DR_Filas("DSC_ATRIBUTO")
            SQLCommand.Parameters.Add("pNemotecnico", SqlDbType.VarChar, 100).Value = strNemotecnico

            '...Resultado
            Dim pSalResultado1 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultado1.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado1)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim


            If strDescError.ToUpper.Trim <> "OK" Then
                MiTransaccionSQL.Rollback()
                Exit Sub
            End If



            SQLCommand = New SqlClient.SqlCommand("Lim_AtributoInstrumento_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Lim_AtributoInstrumento_Mantencion"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 20).Value = "INSERTAR"

            SQLCommand.Parameters.Add("pIdAtributo", SqlDbType.Int, 10).Value = DR_Filas("ID_ATRIBUTO")
            SQLCommand.Parameters.Add("pIdInstrumento", SqlDbType.Int, 10).Value = strId_Entidad
            SQLCommand.Parameters.Add("pIdDatoAtributo", SqlDbType.Int, 12).Value = intValor   'DR_Filas("ID_DATO_ATRIBUTO_LIM")
            SQLCommand.Parameters.Add("pDscAtributo", SqlDbType.VarChar, 100).Value = DR_Filas("DSC_ATRIBUTO")
            SQLCommand.Parameters.Add("pNemotecnico", SqlDbType.VarChar, 100).Value = strNemotecnico

            '...Resultado
            Dim pSalResultado2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultado2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado2)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim <> "OK" Then
                MiTransaccionSQL.Rollback()
                Exit Sub
            End If

            ' -------------
            If Trim(strDescError) = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If


        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en la mantención Atributos/Instrumentos" & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
        End Try

    End Sub

    Public Function Instrumento_Verificar(ByVal dblIdInstrumento As Double, _
                                          ByVal strNemotecnico As String, _
                                          ByRef strRetorno As String) As String

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim LstrProcedimiento As String = ""

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Instrumento_Validar"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdInstrumento", SqlDbType.Float, 9).Value = dblIdInstrumento
            SQLCommand.Parameters.Add("pNemotecnico", SqlDbType.VarChar, 10).Value = strNemotecnico

            Dim pSalResultado As New SqlClient.SqlParameter("pMensajeInst", SqlDbType.VarChar, 200)
            pSalResultado.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)

            SQLCommand.ExecuteNonQuery()

            strRetorno = SQLCommand.Parameters("pMensajeInst").Value.ToString.Trim

            Return strRetorno

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function


    Public Function Instrumento_No_Clasificado(ByVal Desc_arbol_inst_tipo As String, ByVal strRetorno As String) As DataSet

        Dim LDS_Nemo As New DataSet
        Dim LDS_Nemo_Aux As New DataSet
        'Dim LInt_Col As Integer
        'Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        'Dim Columna As DataColumn
        'Dim Remove As Boolean
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "RCP_Instrumentos_No_Clasificados"
        Lstr_NombreTabla = "Instrumentos"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandTimeout = 0
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("@pDesc_arbol_inst_tipo", SqlDbType.VarChar, 20).Value = IIf(Desc_arbol_inst_tipo = "", DBNull.Value, Desc_arbol_inst_tipo)
            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Nemo, Lstr_NombreTabla)

            LDS_Nemo_Aux = LDS_Nemo

            'If strColumnas.Trim = "" Then
            '    LDS_Nemo_Aux = LDS_Nemo
            'Else
            '    LDS_Nemo_Aux = LDS_Nemo.Copy
            '    For Each Columna In LDS_Nemo.Tables(Lstr_NombreTabla).Columns
            '        Remove = True
            '        For LInt_Col = 0 To LArr_NombreColumna.Length - 1
            '            LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
            '            If Columna.ColumnName = LInt_NomCol Then
            '                Remove = False
            '            End If
            '        Next
            '        If Remove Then
            '            LDS_Nemo_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
            '        End If
            '    Next
            'End If

            'For LInt_Col = 0 To LArr_NombreColumna.Length - 1
            '    LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
            '    For Each Columna In LDS_Nemo_Aux.Tables(Lstr_NombreTabla).Columns
            '        If Columna.ColumnName = LInt_NomCol Then
            '            Columna.SetOrdinal(LInt_Col)
            '            Exit For
            '        End If
            '    Next
            'Next

            strRetorno = "OK"
            Return LDS_Nemo_Aux



        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

End Class
