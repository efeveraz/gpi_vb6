﻿Public Class ClsDireccionCliente

    Public Function DireccionCliente_Ver(ByVal strIdDireccion As String, _
                                         ByVal strIdCliente As String, _
                                         ByVal strColumnas As String, _
                                         ByRef strRetorno As String) As DataSet

        Dim LDS_DireccionCliente As New DataSet
        Dim LDS_DireccionCliente_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = "Originales"
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = "Rcp_DireccionCliente_Consultar"

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdDireccion", SqlDbType.VarChar, 9).Value = IIf(strIdDireccion.Trim = "", DBNull.Value, strIdDireccion.Trim)
            SQLCommand.Parameters.Add("pIdCliente", SqlDbType.VarChar, 9).Value = IIf(strIdCliente.Trim = "", DBNull.Value, strIdCliente.Trim)
            
            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_DireccionCliente, Lstr_NombreTabla)


            LDS_DireccionCliente_Aux = LDS_DireccionCliente

            If strColumnas.Trim = "" Then
                LDS_DireccionCliente_Aux = LDS_DireccionCliente
            Else
                LDS_DireccionCliente_Aux = LDS_DireccionCliente.Copy
                For Each Columna In LDS_DireccionCliente.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_DireccionCliente_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_DireccionCliente_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_DireccionCliente.Dispose()
            Return LDS_DireccionCliente_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function DireccionCliente_Mantenedor(ByVal strId_Cliente As String, ByRef dstDireccion As DataSet) As String

        Dim strDescError As String = "OK"
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try

            '       REALIZA LOS CAMBIOS DEL DATASET EN LA TABLA DE DIRECCIONES

            If (dstDireccion.Tables(0).Rows.Count > 0) Or (dstDireccion.Tables(1).Rows.Count > 0) Then
                For Each ptabla As DataTable In dstDireccion.Tables
                    If ptabla.Rows.Count > 0 Then
                        For Each pRow As DataRow In ptabla.Rows
                            If pRow("ESTADO").ToString <> "CONSERVAR" Then
                                SQLCommand = New SqlClient.SqlCommand("Rcp_Direccion_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)
                                SQLCommand.CommandType = CommandType.StoredProcedure
                                SQLCommand.CommandText = "Rcp_Direccion_Mantencion"
                                SQLCommand.Parameters.Clear()

                                '(pAccion varchar(10), 
                                'PId_Direccion numeric(10,0), 
                                'PId_Cliente numeric(10,0), 
                                'PTipo char(1), 
                                'PDireccion varchar(150), 
                                'PFono varchar(50), 
                                'PFax varchar(50), 
                                'PId_Comuna_Ciudad numeric(18,0), 
                                'PCod_Tipo_Direccion varchar(30),
                                'pResultado varchar(200) OUTPUT)

                                SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = pRow("ESTADO").ToString
                                SQLCommand.Parameters.Add("PId_Direccion", SqlDbType.Float, 10).Value = IIf(pRow("ID_DIRECCION").ToString = "", DBNull.Value, pRow("ID_DIRECCION").ToString)
                                SQLCommand.Parameters.Add("PId_Cliente", SqlDbType.Float, 10).Value = CLng("0" & strId_Cliente)
                                SQLCommand.Parameters.Add("PTipo", SqlDbType.VarChar, 1).Value = pRow("ID_TIPO").ToString
                                SQLCommand.Parameters.Add("PDireccion", SqlDbType.VarChar, 150).Value = pRow("DIRECCION").ToString
                                SQLCommand.Parameters.Add("PFono", SqlDbType.VarChar, 50).Value = pRow("FONO").ToString
                                SQLCommand.Parameters.Add("PFax", SqlDbType.VarChar, 50).Value = pRow("FAX").ToString
                                SQLCommand.Parameters.Add("PId_Comuna_Ciudad", SqlDbType.Float, 18).Value = pRow("ID_COMUNA").ToString
                                SQLCommand.Parameters.Add("PCod_Tipo_Direccion", SqlDbType.VarChar, 30).Value = pRow("ID_TIPO").ToString


                                '...Resultado
                                Dim pSalInstPort As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                                pSalInstPort.Direction = Data.ParameterDirection.Output
                                SQLCommand.Parameters.Add(pSalInstPort)

                                SQLCommand.ExecuteNonQuery()

                                strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
                                If Not strDescError.ToUpper.Trim = "OK" Then
                                    Exit For
                                End If

                            End If
                        Next
                    End If
                Next
            End If

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            DireccionCliente_Mantenedor = strDescError
        End Try
    End Function

    Public Function DireccionIncluidas_Ver(ByVal strIdDireccion As String, _
                                     ByVal strIdCliente As String, _
                                     ByVal strIdCuenta As String, _
                                     ByVal strColumnas As String, _
                                     ByRef strRetorno As String) As DataSet

        Dim LDS_DireccionCliente As New DataSet
        Dim LDS_DireccionCliente_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = "Incluidas"
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = "Rcp_DireccionIncluidaACuenta_Consultar"

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("pIdDireccion", SqlDbType.VarChar, 9).Value = IIf(strIdDireccion.Trim = "", DBNull.Value, strIdDireccion.Trim)
            SQLCommand.Parameters.Add("pIdCliente", SqlDbType.VarChar, 9).Value = IIf(strIdCliente.Trim = "", DBNull.Value, strIdCliente.Trim)
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.VarChar, 9).Value = IIf(strIdCuenta.Trim = "", DBNull.Value, strIdCuenta.Trim)


            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_DireccionCliente, Lstr_NombreTabla)


            LDS_DireccionCliente_Aux = LDS_DireccionCliente

            If strColumnas.Trim = "" Then
                LDS_DireccionCliente_Aux = LDS_DireccionCliente
            Else
                LDS_DireccionCliente_Aux = LDS_DireccionCliente.Copy
                For Each Columna In LDS_DireccionCliente.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_DireccionCliente_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_DireccionCliente_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_DireccionCliente.Dispose()
            Return LDS_DireccionCliente_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function DireccionIncluidaXCuenta_Mantenedor(ByVal strId_Cuenta As String, ByRef dstDireccionIncluidaXCuenta As DataSet) As String

        Dim strDescError As String = "OK"
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try

            '       ELIMINAMOS LAS DIRECCION  X CUENTA YA EXISTENTES

            SQLCommand = New SqlClient.SqlCommand("Rcp_DireccionCuenta_MantencionPorCuenta", MiTransaccionSQL.Connection, MiTransaccionSQL)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_DireccionCuenta_MantencionPorCuenta"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = "ELIMINAR"
            SQLCommand.Parameters.Add("pIdDireccion", SqlDbType.Float, 6).Value = DBNull.Value
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = strId_Cuenta
            SQLCommand.Parameters.Add("pObservacion", SqlDbType.VarChar, 180).Value = DBNull.Value
            '...Resultado
            Dim pSalElimIntPort As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalElimIntPort.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalElimIntPort)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            If Not strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Rollback()
                Exit Function
            End If

            '       INSERTAMOS LAS NUEVAS DIRECCION X CUENTA 

            If dstDireccionIncluidaXCuenta.Tables(0).Rows.Count <> Nothing Then
                For Each pRow As DataRow In dstDireccionIncluidaXCuenta.Tables(0).Rows
                    If (pRow("ASOCIADA").ToString = "1") Then
                        SQLCommand = New SqlClient.SqlCommand("Rcp_DireccionCuenta_MantencionPorCuenta", MiTransaccionSQL.Connection, MiTransaccionSQL)
                        SQLCommand.CommandType = CommandType.StoredProcedure
                        SQLCommand.CommandText = "Rcp_DireccionCuenta_MantencionPorCuenta"
                        SQLCommand.Parameters.Clear()

                        SQLCommand.Parameters.Add("pAccion", SqlDbType.Char, 10).Value = "INSERTAR"
                        SQLCommand.Parameters.Add("pIdDireccion", SqlDbType.Float, 6).Value = pRow("ID_DIRECCION").ToString
                        SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = strId_Cuenta
                        SQLCommand.Parameters.Add("pObservacion", SqlDbType.VarChar, 180).Value = pRow("OBSERVACION").ToString.ToUpper

                        '...Resultado
                        Dim pSalInstPort As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                        pSalInstPort.Direction = Data.ParameterDirection.Output
                        SQLCommand.Parameters.Add(pSalInstPort)

                        SQLCommand.ExecuteNonQuery()

                        strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
                    End If
                    If Not strDescError.ToUpper.Trim = "OK" Then
                        Exit For
                    End If
                Next
            End If

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            DireccionIncluidaXCuenta_Mantenedor = strDescError
        End Try
    End Function

End Class
