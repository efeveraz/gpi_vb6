﻿Public Class ClsPreciosInstrumento
    Public Function BuscarPrecioporFecha(ByVal intIdPublicador As Integer, _
                                         ByVal strCodSubClase As String, _
                                         ByVal strFecha As String, _
                                         ByVal strEnCartera As String, _
                                         ByVal strSinPrecio As String, _
                                         ByVal strColumnas As String, _
                                         ByRef strRetorno As String) As DataSet

        Dim LDS_PublicadorPrecio As New DataSet
        Dim LDS_PublicadorPrecio_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_PublicadorPrecio_BuscarPorFecha"
        Lstr_NombreTabla = "PublicadorPrecio"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdPublicador", SqlDbType.Int).Value = intIdPublicador
            SQLCommand.Parameters.Add("@pCodClaseInstrumento", SqlDbType.VarChar, 15).Value = strCodSubClase
            SQLCommand.Parameters.Add("@pfecha", SqlDbType.Char, 10).Value = strFecha
            SQLCommand.Parameters.Add("@pEnCartera", SqlDbType.Char, 2).Value = strEnCartera
            SQLCommand.Parameters.Add("@pSinPrecio", SqlDbType.Char, 2).Value = strSinPrecio

            Dim oParamSal As New SqlClient.SqlParameter("@pResultado", SqlDbType.Char, 200)
            oParamSal.Direction = ParameterDirection.Output
            SQLCommand.Parameters.Add(oParamSal)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_PublicadorPrecio, Lstr_NombreTabla)

            LDS_PublicadorPrecio_Aux = LDS_PublicadorPrecio

            If strColumnas.Trim = "" Then
                LDS_PublicadorPrecio_Aux = LDS_PublicadorPrecio
            Else
                LDS_PublicadorPrecio_Aux = LDS_PublicadorPrecio.Copy
                For Each Columna In LDS_PublicadorPrecio.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_PublicadorPrecio_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_PublicadorPrecio_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = SQLCommand.Parameters("@pResultado").Value.ToString.Trim

            LDS_PublicadorPrecio.Dispose()

            Return LDS_PublicadorPrecio_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function BuscarPrecioporInstrumento(ByVal intIdPublicador As Integer, _
                                               ByVal strCodSubClase As String, _
                                               ByVal intIdInstrumento As Integer, _
                                               ByVal strFechaDesde As String, _
                                               ByVal strFechaHasta As String, _
                                               ByVal strColumnas As String, _
                                               ByRef strRetorno As String) As DataSet

        Dim LDS_PublicadorPrecio As New DataSet
        Dim LDS_PublicadorPrecio_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_PublicadorPrecio_BuscarPorInstrumento"
        Lstr_NombreTabla = "PublicadorPrecio"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("@pIdPublicador", SqlDbType.Int).Value = intIdPublicador
            SQLCommand.Parameters.Add("@pCodClaseInstrumento", SqlDbType.VarChar, 15).Value = strCodSubClase
            SQLCommand.Parameters.Add("@pIdInstrumento", SqlDbType.Int).Value = intIdInstrumento
            SQLCommand.Parameters.Add("@pfechaDesde", SqlDbType.Char, 10).Value = strFechaDesde
            SQLCommand.Parameters.Add("@pfechaHasta", SqlDbType.Char, 10).Value = strFechaHasta

            Dim oParamSal As New SqlClient.SqlParameter("@pResultado", SqlDbType.Char, 200)
            oParamSal.Direction = ParameterDirection.Output
            SQLCommand.Parameters.Add(oParamSal)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_PublicadorPrecio, Lstr_NombreTabla)

            LDS_PublicadorPrecio_Aux = LDS_PublicadorPrecio

            If strColumnas.Trim = "" Then
                LDS_PublicadorPrecio_Aux = LDS_PublicadorPrecio
            Else
                LDS_PublicadorPrecio_Aux = LDS_PublicadorPrecio.Copy
                For Each Columna In LDS_PublicadorPrecio.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_PublicadorPrecio_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                If Not LDS_PublicadorPrecio_Aux.Tables(Lstr_NombreTabla) Is Nothing Then
                    For Each Columna In LDS_PublicadorPrecio_Aux.Tables(Lstr_NombreTabla).Columns
                        If Columna.ColumnName = LInt_NomCol Then
                            Columna.SetOrdinal(LInt_Col)
                            Exit For
                        End If
                    Next
                End If
            Next

            strRetorno = SQLCommand.Parameters("@pResultado").Value.ToString.Trim

            LDS_PublicadorPrecio.Dispose()

            Return LDS_PublicadorPrecio_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Sub PublicadorPreciosMantenedor(ByVal intIdPublicador As Integer, _
                                           ByVal strCodClaseInstrumento As String, _
                                           ByVal DS_PublicadorPrecio As DataSet, _
                                           ByRef strRetorno As String)
        Dim DR_Fila As DataRow = Nothing
        Dim lblnConValor As Boolean
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        'Abre la conexion
        Connect.Abrir()

        Dim MiTransaccion As SqlClient.SqlTransaction


        MiTransaccion = Connect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_PublicadorPrecio_Mantencion", MiTransaccion.Connection, MiTransaccion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_PublicadorPrecio_Mantencion"

            For Each DR_Fila In DS_PublicadorPrecio.Tables(0).Rows  '.Select("OPERACION=1")
                If Not IsDBNull(DR_Fila("OPERACION")) Then
                    SQLCommand.Parameters.Clear()
                    If strCodClaseInstrumento = "RF_NAC" Then
                        If IsDBNull(DR_Fila("TASA")) Then
                            lblnConValor = False
                        Else
                            lblnConValor = True
                        End If
                    Else
                        If IsDBNull(DR_Fila("PRECIO")) Then
                            lblnConValor = False
                        Else
                            lblnConValor = True
                        End If
                    End If

                    'INSERTA
                    If IsDBNull(DR_Fila("ID_PUBLICADOR_PRECIO")) And lblnConValor Then
                        SQLCommand.Parameters.Add("@pAccion", SqlDbType.VarChar, 10).Value = "INSERTAR"

                        SQLCommand.Parameters.Add("@pIdPublicador", SqlDbType.Int).Value = intIdPublicador
                        SQLCommand.Parameters.Add("@pIdInstrumento", SqlDbType.Int).Value = DR_Fila("ID_INSTRUMENTO")
                        SQLCommand.Parameters.Add("@pCodMoneda", SqlDbType.Char, 3).Value = DR_Fila("COD_MONEDA")
                        SQLCommand.Parameters.Add("@pfecha", SqlDbType.Char, 10).Value = DR_Fila("FECHA")
                        SQLCommand.Parameters.Add("@pPrecio", SqlDbType.Decimal, 18).Value = DR_Fila("PRECIO")
                        SQLCommand.Parameters.Add("@pTasa", SqlDbType.Float).Value = DR_Fila("TASA")
                        'SQLCommand.Parameters.Add("@pDuracion", SqlDbType.Decimal, 20).Value = DR_Fila("DURACION")
                        SQLCommand.Parameters.Add("@pNemotecnico", SqlDbType.Char, 50).Value = DR_Fila("NEMOTECNICO")
                        SQLCommand.Parameters.Add("@pIntAcumulado", SqlDbType.Decimal, 18).Value = DR_Fila("ACCRUED_INTEREST")

                        Dim ParametroIdPublicadorPrecio As New SqlClient.SqlParameter("@pIdPublicadorPrecio", SqlDbType.Int)
                        ParametroIdPublicadorPrecio.Direction = Data.ParameterDirection.Output
                        SQLCommand.Parameters.Add(ParametroIdPublicadorPrecio)

                    End If

                    'MODIFICA
                    If Not (IsDBNull(DR_Fila("ID_PUBLICADOR_PRECIO"))) And lblnConValor Then
                        SQLCommand.Parameters.Add("@pAccion", SqlDbType.VarChar, 10).Value = "MODIFICAR"

                        SQLCommand.Parameters.Add("@pIdPublicadorPrecio", SqlDbType.Int).Value = DR_Fila("ID_PUBLICADOR_PRECIO")
                        SQLCommand.Parameters.Add("@pIdPublicador", SqlDbType.Int).Value = intIdPublicador
                        SQLCommand.Parameters.Add("@pPrecio", SqlDbType.Decimal, 18).Value = DR_Fila("PRECIO")
                        SQLCommand.Parameters.Add("@pTasa", SqlDbType.Float).Value = DR_Fila("TASA")
                        ' SQLCommand.Parameters.Add("@pDuracion", SqlDbType.Decimal, 20).Value = DR_Fila("DURACION")
                        SQLCommand.Parameters.Add("@pNemotecnico", SqlDbType.Char, 50).Value = DR_Fila("NEMOTECNICO")
                        SQLCommand.Parameters.Add("@pIntAcumulado", SqlDbType.Decimal, 18).Value = DR_Fila("ACCRUED_INTEREST")
                    End If

                    'ELIMINA
                    If Not (IsDBNull(DR_Fila("ID_PUBLICADOR_PRECIO"))) And Not (lblnConValor) Then
                        SQLCommand.Parameters.Add("@pAccion", SqlDbType.VarChar, 10).Value = "ELIMINAR"
                        SQLCommand.Parameters.Add("@pIdPublicador", SqlDbType.Int).Value = DBNull.Value

                        SQLCommand.Parameters.Add("@pIdPublicadorPrecio", SqlDbType.Int).Value = DR_Fila("ID_PUBLICADOR_PRECIO")
                        SQLCommand.Parameters.Add("@pPrecio", SqlDbType.Decimal, 18).Value = DBNull.Value
                        SQLCommand.Parameters.Add("@pTasa", SqlDbType.Float).Value = DBNull.Value
                        SQLCommand.Parameters.Add("@pDuracion", SqlDbType.Decimal, 20).Value = DBNull.Value
                        SQLCommand.Parameters.Add("@pNemotecnico", SqlDbType.Char, 50).Value = DR_Fila("NEMOTECNICO")
                        SQLCommand.Parameters.Add("@pIntAcumulado", SqlDbType.Decimal, 18).Value = DR_Fila("ACCRUED_INTEREST")

                    End If

                    If IsDBNull(DR_Fila("ID_PUBLICADOR_PRECIO")) And Not (lblnConValor) Then
                        strRetorno = "OK"
                    Else
                        Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                        ParametroSal2.Direction = Data.ParameterDirection.Output
                        SQLCommand.Parameters.Add(ParametroSal2)

                        SQLCommand.ExecuteNonQuery()

                        strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim

                        If Trim(strRetorno) <> "OK" Then
                            Exit For
                        End If
                    End If
                End If
            Next
            If Trim(strRetorno) = "OK" Then
                MiTransaccion.Commit()
            Else
                MiTransaccion.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccion.Rollback()
            strRetorno = "Error al grabar Publicador de Precios." & vbCr & Ex.Message
        Finally
            Connect.Cerrar()
        End Try


    End Sub

    Public Function BuscaPreciosSaldoActivo(ByVal intIdPublicador As Integer, _
                                             ByVal strCodSubClase As String, _
                                             ByVal strFecha As String, _
                                             ByVal strColumnas As String, _
                                             ByVal strValoresNulos As String, _
                                             ByRef strRetorno As String) As DataSet

        Dim LDS_PublicadorPrecio As New DataSet
        Dim LDS_PublicadorPrecio_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_PublicadorPrecio_BuscaPreciosSaldoActivo"
        Lstr_NombreTabla = "PublicadorPrecio"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdPublicador", SqlDbType.Int).Value = intIdPublicador
            SQLCommand.Parameters.Add("@pCodProducto", SqlDbType.VarChar, 15).Value = strCodSubClase
            SQLCommand.Parameters.Add("@pValoresNulos", SqlDbType.VarChar, 1).Value = IIf(strValoresNulos = "N", DBNull.Value, strValoresNulos)
            SQLCommand.Parameters.Add("@pFecha", SqlDbType.Char, 10).Value = strFecha

            Dim oParamSal As New SqlClient.SqlParameter("@pResultado", SqlDbType.Char, 200)
            oParamSal.Direction = ParameterDirection.Output
            SQLCommand.Parameters.Add(oParamSal)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_PublicadorPrecio, Lstr_NombreTabla)

            LDS_PublicadorPrecio_Aux = LDS_PublicadorPrecio

            If strColumnas.Trim = "" Then
                LDS_PublicadorPrecio_Aux = LDS_PublicadorPrecio
            Else
                LDS_PublicadorPrecio_Aux = LDS_PublicadorPrecio.Copy
                For Each Columna In LDS_PublicadorPrecio.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_PublicadorPrecio_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_PublicadorPrecio_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = SQLCommand.Parameters("@pResultado").Value.ToString.Trim

            LDS_PublicadorPrecio.Dispose()

            Return LDS_PublicadorPrecio_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function
    Public Sub Carga_Precios_Safp(ByVal pPrecios As String, ByVal pPublicador As Integer, ByVal pFecha_datos As String, ByVal IdUsuario As Integer, ByVal TotReg As Integer, ByRef Reg_Leidos As Integer, ByRef Reg_Grabados As Integer, ByRef Reg_Malos As Integer, ByRef strRetorno As String)

        Dim LDS_PublicadorPrecio As New DataSet
        Dim LDS_Emisor As New DataSet
        Dim LDS_Emisor_Aux As New DataSet
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        Reg_Leidos = 0
        Reg_Grabados = 0
        Reg_Malos = 0
        strRetorno = ""

        LstrProcedimiento = "RCP_CARGA_PRECIOS_SAFP"
        Connect.Abrir()
        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Add("pPrecios", SqlDbType.Text).Value = pPrecios
            SQLCommand.Parameters.Add("pPublicador", SqlDbType.Int, 3).Value = pPublicador
            SQLCommand.Parameters.Add("pFecha_datos", SqlDbType.Text, 10).Value = pFecha_datos
            SQLCommand.Parameters.Add("pTotReg", SqlDbType.Int).Value = TotReg
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Int).Value = IdUsuario

            Dim pReg_Leidos As New SqlClient.SqlParameter("pReg_Leidos", SqlDbType.Int, 10)
            pReg_Leidos.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pReg_Leidos).Value = Reg_Leidos

            Dim pReg_Grabados As New SqlClient.SqlParameter("pReg_Grabados", SqlDbType.Int, 10)
            pReg_Grabados.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pReg_Grabados).Value = Reg_Grabados

            Dim pReg_Malos As New SqlClient.SqlParameter("pReg_Malos", SqlDbType.Int, 10)
            pReg_Malos.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pReg_Malos).Value = Reg_Malos

            Dim pError As New SqlClient.SqlParameter("pError", SqlDbType.VarChar, 100)
            pError.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pError).Value = strRetorno

            SQLCommand.ExecuteNonQuery()

            Reg_Leidos = SQLCommand.Parameters("pReg_Leidos").Value
            Reg_Grabados = SQLCommand.Parameters("pReg_Grabados").Value
            Reg_Malos = SQLCommand.Parameters("pReg_Malos").Value

            strRetorno = SQLCommand.Parameters("pError").Value.ToString


        Catch ex As Exception
            strRetorno = ex.Message

        Finally
            Connect.Cerrar()
        End Try
    End Sub

End Class
