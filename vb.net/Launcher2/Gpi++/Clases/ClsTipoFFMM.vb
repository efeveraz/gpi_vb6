﻿Public Class ClsTipoFFMM
    Public Function TipoFFMM_Buscar(ByVal strCodTipoFFMM As String, _
                                ByVal strColumnas As String, _
                                ByRef strRetorno As String) As DataSet

        Dim LDS_TipoFFMM As New DataSet
        Dim LDS_Nemotecnico_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_TipoFFMM_Buscar"
        Lstr_NombreTabla = "TIPOFFMM"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pCodTipoFFMM", SqlDbType.Char, 3).Value = IIf(strCodTipoFFMM = "", DBNull.Value, strCodTipoFFMM)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_TipoFFMM, Lstr_NombreTabla)

            LDS_Nemotecnico_Aux = LDS_TipoFFMM

            If strColumnas.Trim = "" Then
                LDS_Nemotecnico_Aux = LDS_TipoFFMM
            Else
                LDS_Nemotecnico_Aux = LDS_TipoFFMM.Copy
                For Each Columna In LDS_TipoFFMM.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Nemotecnico_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Nemotecnico_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            Return LDS_Nemotecnico_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function Seguimiento_de_Red_Buscar(ByVal strFechaConsulta As String, _
                                    ByRef strRetorno As String) As DataSet

        Dim lDS_SeguimientoRed As New DataSet
        Dim lDS_SeguimientoRed_Aux As New DataSet
        Dim lstr_NombreTabla As String = ""
        Dim lstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        lstrProcedimiento = "Hsb_SeguimientodeRed_Buscar"
        lstr_NombreTabla = "SEGUIMIENTODERED"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = lstrProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("pFechaConsulta", SqlDbType.Char, 10).Value = strFechaConsulta

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(lDS_SeguimientoRed, lstr_NombreTabla)

            lDS_SeguimientoRed_Aux = lDS_SeguimientoRed

            strRetorno = "OK"
            lDS_SeguimientoRed.Dispose()
            Return lDS_SeguimientoRed_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function
End Class
