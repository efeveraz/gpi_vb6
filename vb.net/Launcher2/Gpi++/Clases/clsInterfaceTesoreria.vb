﻿Imports System.Data

Public Class clsInterfaceTesoreria
#Region "============ INGRESO DE CARTERA A PLAZO ============"
  Public Function InformarIngresoOpPlazo(ByVal intIdOperacion As Integer _
                                       , ByVal strCod_Tipo_Mov_Sistema As String) As String
    '----------------------------------------------------------
    Dim lstrDescError As String = ""
    Dim lstrFlg_Tipo_Movimiento As String = ""
    '----------------------------------------------------------
    Dim blnEnviadoTesoreria As Boolean = False
    '----------------------------------------------------------

    Try
      lstrDescError = PreparaInterface_X_OpPlazo(intIdOperacion, strCod_Tipo_Mov_Sistema)

      If lstrDescError <> "OK" Then
        InformarIngresoOpPlazo = lstrDescError
        Exit Function
      End If
    Catch ex As Exception
      lstrDescError = "Problema en preparar la interface para la operación '" & intIdOperacion & "'." & vbCr & ex.Message
      InformarIngresoOpPlazo = lstrDescError
      Exit Function
    End Try

    'Try
    '  If gLogoCliente = "FOH" Then
    '    Dim lcInterfaceTesoreriaFOH As New ClsInterfaceTesoreria_FOH
    '    lstrDescError = lcInterfaceTesoreriaFOH.InformarOperacion(intIdOperacion:=intIdOperacion, strFlg_Tipo_Movimiento:=lstrFlg_Tipo_Movimiento)

    '    If lstrDescError <> "OK" Then
    '      InformarIngresoOpPlazo = lstrDescError
    '      Exit Function
    '    End If

    '    blnEnviadoTesoreria = True
    '  End If
    'Catch Ex As Exception
    '  lstrDescError = "Problema en el Informar a Tesorería la operación '" & intIdOperacion & "'." & vbCr & Ex.Message
    '  InformarIngresoOpPlazo = lstrDescError
    '  Exit Function
    'End Try

    'If blnEnviadoTesoreria Then
    '  lstrDescError = ConfirmaEnvio_X_Operacion(intIdOperacion)
    'End If

    InformarIngresoOpPlazo = lstrDescError
  End Function

  Public Function PreparaInterface_X_OpPlazo(ByVal intIdOperacion As Integer _
                                           , ByVal strCod_Tipo_Mov_Sistema As String) As String
    '---------------------------------------------------------
    Dim SQLCommand As New SqlClient.SqlCommand
    Dim SQLConnect As New Cls_Conexion
    '---------------------------------------------------------
    Dim lstrProcedimiento As String = ""
    Dim lstrDescError As String = ""
    '---------------------------------------------------------

    '...Abre la conexion
    SQLConnect.Abrir()
    lstrProcedimiento = "ITZ_CONT_PREPARA_INTERFACE_X_OPPLAZO"

    Try
      SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, SQLConnect.Conexion)
      SQLCommand.CommandType = CommandType.StoredProcedure
      SQLCommand.CommandText = lstrProcedimiento
      SQLCommand.Parameters.Clear()

      '( @PID_OPERACION          NUMERIC
      ', @PID_USUARIO            NUMERIC
      ', @PCOD_TIPO_MOV_SISTEMA  VARCHAR(100)   
      ', @PRESULTADO             VARCHAR(200) OUTPUT
      SQLCommand.Parameters.Add("PID_OPERACION", SqlDbType.Float, 3).Value = intIdOperacion
      SQLCommand.Parameters.Add("PID_USUARIO", SqlDbType.Float, 3).Value = glngIdUsuario
      SQLCommand.Parameters.Add("PCOD_TIPO_MOV_SISTEMA", SqlDbType.VarChar, 100).Value = strCod_Tipo_Mov_Sistema

      '...Resultado
      Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
      ParametroSal2.Direction = Data.ParameterDirection.Output
      SQLCommand.Parameters.Add(ParametroSal2)

      SQLCommand.ExecuteNonQuery()

      lstrDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

    Catch Ex As Exception
      lstrDescError = "Problema en el leer la operación '" & intIdOperacion & "'." & vbCr & Ex.Message
    Finally
      SQLConnect.Cerrar()
      PreparaInterface_X_OpPlazo = lstrDescError
    End Try
  End Function
#End Region

#Region "============ INGRESO DE OPERACIONES ============"
  Public Function InformarIngresoOperacion(ByVal intIdOperacion As Integer) As String
    '----------------------------------------------------------
    Dim lstrDescError As String = ""
    Dim lstrFlg_Tipo_Movimiento As String = ""
    '----------------------------------------------------------
    Dim blnEnviadoTesoreria As Boolean = False
    '----------------------------------------------------------

    InformarIngresoOperacion = ""

    Try
      lstrDescError = PreparaInterface_X_Operacion(intIdOperacion, lstrFlg_Tipo_Movimiento)

      If lstrDescError <> "OK" Then
        InformarIngresoOperacion = lstrDescError
        Exit Function
      End If
    Catch ex As Exception
      lstrDescError = "Problema en preparar la interface para la operación '" & intIdOperacion & "'." & vbCr & ex.Message
      InformarIngresoOperacion = lstrDescError
      Exit Function
    End Try

    Try
      If gLogoCliente = "FOH" Then
        Dim lcInterfaceTesoreriaFOH As New ClsInterfaceTesoreria_FOH
        lstrDescError = lcInterfaceTesoreriaFOH.InformarOperacion(intIdOperacion:=intIdOperacion, strFlg_Tipo_Movimiento:=lstrFlg_Tipo_Movimiento)

        If lstrDescError <> "OK" Then
          InformarIngresoOperacion = lstrDescError
          Exit Function
        End If

        blnEnviadoTesoreria = True
      End If
    Catch Ex As Exception
      lstrDescError = "Problema en el Informar a Tesorería la operación '" & intIdOperacion & "'." & vbCr & Ex.Message
      InformarIngresoOperacion = lstrDescError
      Exit Function
    End Try

    If blnEnviadoTesoreria Then
      lstrDescError = ConfirmaEnvio_X_Operacion(intIdOperacion)
    End If

    InformarIngresoOperacion = lstrDescError
  End Function

  Public Function PreparaInterface_X_Operacion(ByVal intIdOperacion As Integer _
                                             , ByRef strFlg_Tipo_Movimiento As String) As String
    '---------------------------------------------------------
    Dim SQLCommand As New SqlClient.SqlCommand
    Dim SQLConnect As New Cls_Conexion
    '---------------------------------------------------------
    Dim lstrProcedimiento As String = ""
    Dim lstrDescError As String = ""
    '---------------------------------------------------------

    '...Abre la conexion
    SQLConnect.Abrir()
    lstrProcedimiento = "ITZ_CONT_PREPARA_INTERFACE_X_OPERACION"

    Try
      SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, SQLConnect.Conexion)
      SQLCommand.CommandType = CommandType.StoredProcedure
      SQLCommand.CommandText = lstrProcedimiento
      SQLCommand.Parameters.Clear()

      '( @PID_OPERACION          NUMERIC
      ', @PID_USUARIO            NUMERIC
      ', @PTIPO_OPERACION        VARCHAR(1)   OUTPUT
      ', @PRESULTADO             VARCHAR(200) OUTPUT
      SQLCommand.Parameters.Add("PID_OPERACION", SqlDbType.Float, 3).Value = intIdOperacion
      SQLCommand.Parameters.Add("PID_USUARIO", SqlDbType.Float, 3).Value = glngIdUsuario

      Dim ParametroSal2 As New SqlClient.SqlParameter("PTIPO_OPERACION", SqlDbType.VarChar, 200)
      ParametroSal2.Direction = Data.ParameterDirection.Output
      SQLCommand.Parameters.Add(ParametroSal2)

      '...Resultado
      ParametroSal2 = New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
      ParametroSal2.Direction = Data.ParameterDirection.Output
      SQLCommand.Parameters.Add(ParametroSal2)

      SQLCommand.ExecuteNonQuery()

      strFlg_Tipo_Movimiento = SQLCommand.Parameters("PTIPO_OPERACION").Value.ToString.Trim

      lstrDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

    Catch Ex As Exception
      lstrDescError = "Problema en el leer la operación '" & intIdOperacion & "'." & vbCr & Ex.Message
    Finally
      SQLConnect.Cerrar()
      PreparaInterface_X_Operacion = lstrDescError
    End Try
  End Function

  Public Function ConfirmaEnvio_X_Operacion(ByVal intIdOperacion As Integer) As String
    '---------------------------------------------------------
    Dim SQLCommand As New SqlClient.SqlCommand
    Dim SQLConnect As New Cls_Conexion
    '---------------------------------------------------------
    Dim lstrProcedimiento As String = ""
    Dim lstrDescError As String = ""
    '---------------------------------------------------------

    '...Abre la conexion
    SQLConnect.Abrir()
    lstrProcedimiento = "ITZ_CONT_CONFIRMA_ENVIO_X_OPERACION"

    Try
      SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, SQLConnect.Conexion)
      SQLCommand.CommandType = CommandType.StoredProcedure
      SQLCommand.CommandText = lstrProcedimiento
      SQLCommand.Parameters.Clear()

      '( @PID_OPERACION NUMERIC
      ', @PRESULTADO VARCHAR(200) OUTPUT
      SQLCommand.Parameters.Add("PID_OPERACION", SqlDbType.Float, 3).Value = intIdOperacion

      '...Resultado
      Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
      ParametroSal2.Direction = Data.ParameterDirection.Output
      SQLCommand.Parameters.Add(ParametroSal2)

      SQLCommand.ExecuteNonQuery()

      lstrDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

    Catch Ex As Exception
      lstrDescError = "Problema en la confirmación del envio Tesoría código '" & intIdOperacion & "'." & vbCr & Ex.Message
    Finally
      SQLConnect.Cerrar()
    End Try

    Return lstrDescError
  End Function
#End Region

#Region "============ INGRESO DE CAJA ============"
  Public Function InformarIngresoCaja(ByVal intIdMovCaja As Integer) As String
    '----------------------------------------------------------
    Dim lstrDescError As String = ""
    Dim lstrFlg_Tipo_Movimiento As String = ""
    '----------------------------------------------------------
    Dim blnEnviadoTesoreria As Boolean = False
    '----------------------------------------------------------

    Try
      lstrDescError = PreparaInterface_X_Caja(intIdMovCaja, lstrFlg_Tipo_Movimiento)

      If lstrDescError <> "OK" Then
        InformarIngresoCaja = lstrDescError
        Exit Function
      End If
    Catch ex As Exception
      lstrDescError = "Problema en preparar la interface para la caja '" & intIdMovCaja & "'." & vbCr & ex.Message
      InformarIngresoCaja = lstrDescError
      Exit Function
    End Try

    Try
      If gLogoCliente = "FOH" Then
        Dim lcInterfaceTesoreriaFOH As New ClsInterfaceTesoreria_FOH
        lstrDescError = lcInterfaceTesoreriaFOH.InformarCaja(intIdMovCaja:=intIdMovCaja, strFlg_Tipo_Movimiento:=lstrFlg_Tipo_Movimiento)

        If lstrDescError <> "OK" Then
          InformarIngresoCaja = lstrDescError
          Exit Function
        End If

        blnEnviadoTesoreria = True
      End If
    Catch Ex As Exception
      lstrDescError = "Problema al confirmar el enviado a Tesorería código '" & intIdMovCaja & "'." & vbCr & Ex.Message
      InformarIngresoCaja = lstrDescError
      Exit Function
    End Try

    If blnEnviadoTesoreria Then
      lstrDescError = ConfirmaEnvio_X_Caja(intIdMovCaja)
    End If

    Return lstrDescError
  End Function

  Public Function PreparaInterface_X_Caja(ByVal intIdMovCaja As Integer _
                                        , ByRef strFlg_Tipo_Movimiento As String) As String
    '---------------------------------------------------------
    Dim SQLCommand As New SqlClient.SqlCommand
    Dim SQLConnect As New Cls_Conexion
    '---------------------------------------------------------
    Dim lstrProcedimiento As String = ""
    Dim lstrDescError As String = ""
    '---------------------------------------------------------

    '...Abre la conexion
    SQLConnect.Abrir()
    lstrProcedimiento = "ITZ_CONT_PREPARA_INTERFACE_X_CAJA"

    Try
      SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, SQLConnect.Conexion)
      SQLCommand.CommandType = CommandType.StoredProcedure
      SQLCommand.CommandText = lstrProcedimiento
      SQLCommand.Parameters.Clear()

      '( @PID_MOV_CAJA NUMERIC
      ', @PRESULTADO VARCHAR(200) OUTPUT
      SQLCommand.Parameters.Add("PID_MOV_CAJA", SqlDbType.Float, 3).Value = intIdMovCaja
      SQLCommand.Parameters.Add("PID_USUARIO", SqlDbType.Float, 3).Value = glngIdUsuario

      Dim ParametroSal2 As New SqlClient.SqlParameter("PTIPO_OPERACION", SqlDbType.VarChar, 200)
      ParametroSal2.Direction = Data.ParameterDirection.Output
      SQLCommand.Parameters.Add(ParametroSal2)

      '...Resultado
      ParametroSal2 = New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
      ParametroSal2.Direction = Data.ParameterDirection.Output
      SQLCommand.Parameters.Add(ParametroSal2)

      SQLCommand.ExecuteNonQuery()

      strFlg_Tipo_Movimiento = SQLCommand.Parameters("PTIPO_OPERACION").Value.ToString.Trim

      lstrDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

    Catch Ex As Exception
      lstrDescError = "Problema en el leer la operación '" & intIdMovCaja & "'." & vbCr & Ex.Message
    Finally
      SQLConnect.Cerrar()
      PreparaInterface_X_Caja = lstrDescError
    End Try
  End Function

  Public Function ConfirmaEnvio_X_Caja(ByVal intIdMovCaja As Integer) As String
    '---------------------------------------------------------
    Dim SQLCommand As New SqlClient.SqlCommand
    Dim SQLConnect As New Cls_Conexion
    '---------------------------------------------------------
    Dim lstrProcedimiento As String = ""
    Dim lstrDescError As String = ""
    '---------------------------------------------------------

    '...Abre la conexion
    SQLConnect.Abrir()
    lstrProcedimiento = "ITZ_CONT_CONFIRMA_ENVIO_X_CAJA"

    Try
      SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, SQLConnect.Conexion)
      SQLCommand.CommandType = CommandType.StoredProcedure
      SQLCommand.CommandText = lstrProcedimiento
      SQLCommand.Parameters.Clear()

      '( @PID_MOV_CAJA NUMERIC
      ', @PRESULTADO VARCHAR(200) OUTPUT
      SQLCommand.Parameters.Add("PID_MOV_CAJA", SqlDbType.Float, 3).Value = intIdMovCaja
      '...Resultado
      Dim ParametroSal2 = New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
      ParametroSal2.Direction = Data.ParameterDirection.Output
      SQLCommand.Parameters.Add(ParametroSal2)

      SQLCommand.ExecuteNonQuery()

      lstrDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

    Catch Ex As Exception
      lstrDescError = "Problema en la confirmacion de la caja código '" & intIdMovCaja & "'." & vbCr & Ex.Message
    Finally
      SQLConnect.Cerrar()
    End Try

    Return lstrDescError
  End Function

#End Region

#Region "============ VISOR DE EVENTOS ============"
  Public Function BuscarEventos(ByVal dtFechaDesde As Date _
                              , ByVal dtFechaHasta As Date _
                              , ByVal strNumCuenta As String _
                              , ByVal strCodEstado As String _
                              , ByVal strCodTipMov As String _
                              , ByVal strpColumnas As String _
                              , ByRef dtEventos As DataTable) As String
    '---------------------------------------------------------
    Dim SQLCommand As New SqlClient.SqlCommand
    Dim SQLConnect As New Cls_Conexion
    Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
    '---------------------------------------------------------
    Dim lstrProcedimiento As String = "ITZ_CONT_TRAER_EVENTOS"
    Dim lstrDescError As String = ""
    '---------------------------------------------------------

    '...Abre la conexion
    SQLConnect.Abrir()

    Try
      SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, SQLConnect.Conexion)
      SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = lstrProcedimiento
            SQLCommand.CommandTimeout = 5000
      SQLCommand.Parameters.Clear()

      '( @PFECHA_DESDE       VARCHAR(10) 
      ', @PFECHA_HASTA       VARCHAR(10)
      ', @PRESULTADO         VARCHAR(200) OUTPUT
      SQLCommand.Parameters.Add("PFECHA_DESDE", SqlDbType.VarChar, 200).Value = dtFechaDesde.ToString("yyyyMMdd")
      SQLCommand.Parameters.Add("PFECHA_HASTA", SqlDbType.VarChar, 200).Value = dtFechaHasta.ToString("yyyyMMdd")
      SQLCommand.Parameters.Add("PNUMCUENTA", SqlDbType.VarChar, 50).Value = IIf(strNumCuenta = "", DBNull.Value, strNumCuenta)
      SQLCommand.Parameters.Add("PCODESTADO", SqlDbType.VarChar, 12).Value = IIf(strCodEstado = "", DBNull.Value, strCodEstado)
      SQLCommand.Parameters.Add("PCODTIPOMOV_SISTEMA", SqlDbType.VarChar, 50).Value = IIf(strCodTipMov = "", DBNull.Value, strCodTipMov)

      '...Resultado
      Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
      ParametroSal2.Direction = Data.ParameterDirection.Output
      SQLCommand.Parameters.Add(ParametroSal2)

      SQLCommand.ExecuteNonQuery()

      lstrDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

      SQLDataAdapter.SelectCommand = SQLCommand
      SQLDataAdapter.Fill(dtEventos)

      gsubConfiguraDataTable(dtEventos, strpColumnas)

    Catch Ex As Exception
      lstrDescError = "Problema en el leer los eventos." & vbCr & Ex.Message
    Finally
      SQLConnect.Cerrar()
      BuscarEventos = lstrDescError
    End Try
  End Function

  Public Function AnularEventos(ByVal dblIdMovSistema As Double) As String
    '---------------------------------------------------------
    Dim SQLCommand As New SqlClient.SqlCommand
    Dim SQLConnect As New Cls_Conexion
    Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
    '---------------------------------------------------------
    Dim lstrProcedimiento As String = ""
    Dim lstrDescError As String = ""
    Dim lstrNomCol As String = ""
    Dim lstr_NombreTabla As String = ""
    Dim lblnRemove As Boolean = True
    '---------------------------------------------------------

    '...Abre la conexion
    SQLConnect.Abrir()
    lstrProcedimiento = "ITZ_CONT_ANULAR_EVENTO"
    lstr_NombreTabla = "EVENTOS"

    Try
      SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, SQLConnect.Conexion)
      SQLCommand.CommandType = CommandType.StoredProcedure
      SQLCommand.CommandText = lstrProcedimiento
      SQLCommand.Parameters.Clear()

      '( @PID_MOV_SISTEMA NUMERIC
      ', @PRESULTADO VARCHAR(200) OUTPUT
      SQLCommand.Parameters.Add("PID_MOV_SISTEMA", SqlDbType.Float, 3).Value = dblIdMovSistema

      '...Resultado
      Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
      ParametroSal2.Direction = Data.ParameterDirection.Output
      SQLCommand.Parameters.Add(ParametroSal2)

      SQLCommand.ExecuteNonQuery()

      lstrDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

    Catch Ex As Exception
      lstrDescError = "Problema al anular el evento." & vbCr & Ex.Message
    Finally
      SQLConnect.Cerrar()
      AnularEventos = lstrDescError
    End Try
  End Function

  Public Function EnviarTesoreria(ByVal dblIdMovSistema As Double) As String
    '----------------------------------------------------------
    Dim lstrDescError As String = ""
    Dim lstrFlg_Tipo_Movimiento As String = ""
    '----------------------------------------------------------
    Dim blnEnviadoTesoreria As Boolean = False
    '----------------------------------------------------------

    Select Case gLogoCliente
      Case "FOH"
        Dim objInterfaceTesoreriaFOH As New ClsInterfaceTesoreria_FOH

        Try
          lstrDescError = objInterfaceTesoreriaFOH.InformarEvento(dblIdMovSistema)
        Catch ex As Exception
          lstrDescError = "Problema en enviar el evento, codigo '" & dblIdMovSistema & "'." & vbCr & ex.Message
        End Try

        If lstrDescError = "OK" Then
          lstrDescError = ConfirmaEnvio_X_Evento(dblIdMovSistema)
        End If
      Case Else
        lstrDescError = "No existe configuración para el cliente '" & gLogoCliente & "'"
    End Select

    Return lstrDescError
  End Function

  Public Function EnviarContabilidad(ByVal dblIdCentralizacion As Double) As String
    '----------------------------------------------------------
    Dim lstrDescError As String = ""
    Dim lstrFlg_Tipo_Movimiento As String = ""
    '----------------------------------------------------------
    Dim blnEnviadoTesoreria As Boolean = False
    '----------------------------------------------------------

    Select Case gLogoCliente
      Case "FOH"
        Dim objInterfaceContabilidadFOH As New ClsInterfaceContabilidad_FOH

        Try
          lstrDescError = objInterfaceContabilidadFOH.InformarEvento(dblIdCentralizacion)
        Catch ex As Exception
          lstrDescError = "Problema en enviar el evento, codigo '" & dblIdCentralizacion & "'." & vbCr & ex.Message
        End Try

        If lstrDescError = "OK" Then
          lstrDescError = ConfirmaEnvio_X_Centralizaciones(dblIdCentralizacion)
        End If
      Case Else
        lstrDescError = "No existe configuración para el cliente '" & gLogoCliente & "'"
    End Select

    Return lstrDescError
  End Function

  Public Function ConfirmaEnvio_X_Evento(ByVal dblIdMovSistema As Double) As String
    '---------------------------------------------------------
    Dim SQLCommand As New SqlClient.SqlCommand
    Dim SQLConnect As New Cls_Conexion
    '---------------------------------------------------------
    Dim lstrProcedimiento As String = ""
    Dim lstrDescError As String = ""
    '---------------------------------------------------------

    '...Abre la conexion
    SQLConnect.Abrir()
    lstrProcedimiento = "DBO.ITZ_CONT_CONFIRMA_ENVIO_X_EVENTO"

    Try
      SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, SQLConnect.Conexion)
      SQLCommand.CommandType = CommandType.StoredProcedure
      SQLCommand.CommandText = lstrProcedimiento
      SQLCommand.Parameters.Clear()

      '( @PID_MOV_SISTEMA NUMERIC
      ', @PRESULTADO VARCHAR(200) OUTPUT
      SQLCommand.Parameters.Add("PID_MOV_SISTEMA", SqlDbType.Float, 3).Value = dblIdMovSistema

      '...Resultado
      Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
      ParametroSal2.Direction = Data.ParameterDirection.Output
      SQLCommand.Parameters.Add(ParametroSal2)

      SQLCommand.ExecuteNonQuery()

      lstrDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

    Catch Ex As Exception
      lstrDescError = "Problema en la confirmación del envio Tesoría código '" & dblIdMovSistema & "'." & vbCr & Ex.Message
    Finally
      SQLConnect.Cerrar()
    End Try

    Return lstrDescError
  End Function

  Public Function ConfirmaEnvio_X_Centralizaciones(ByVal dblIdCentralizacion As Double) As String
    '---------------------------------------------------------
    Dim SQLCommand As New SqlClient.SqlCommand
    Dim SQLConnect As New Cls_Conexion
    Dim MiTransaccionSQL As SqlClient.SqlTransaction
    '---------------------------------------------------------
    Dim lstrProcedimiento As String = ""
    Dim lstrDescError As String = ""
    '---------------------------------------------------------

    '...Abre la conexion
    SQLConnect.Abrir()
    MiTransaccionSQL = SQLConnect.Transaccion
    lstrProcedimiento = "ITZ_CONT_CENTRALIZACION_ACTUALIZA"

    Try
      SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, MiTransaccionSQL.Connection, MiTransaccionSQL)
      SQLCommand.CommandType = CommandType.StoredProcedure
      SQLCommand.CommandText = lstrProcedimiento
      SQLCommand.Parameters.Clear()

      '( @PID_MOV_SISTEMA NUMERIC
      ', @PRESULTADO VARCHAR(200) OUTPUT
      SQLCommand.Parameters.Add("PID_CENTRALIZACION", SqlDbType.Float, 3).Value = dblIdCentralizacion

      '...Resultado
      Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
      ParametroSal2.Direction = Data.ParameterDirection.Output
      SQLCommand.Parameters.Add(ParametroSal2)

      SQLCommand.ExecuteNonQuery()

      lstrDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
      If lstrDescError = "OK" Then
        MiTransaccionSQL.Commit()
      Else
        MiTransaccionSQL.Rollback()
      End If
    Catch Ex As Exception
      MiTransaccionSQL.Rollback()
      lstrDescError = "Problema al actualizar Envios ." & vbCr & Ex.Message
    Finally
      SQLConnect.Cerrar()
    End Try

    Return lstrDescError
  End Function

  Public Function AnulaCentralizaciones(ByVal dblIdCentralizacion As Double) As String
    '---------------------------------------------------------
    Dim SQLCommand As New SqlClient.SqlCommand
    Dim SQLConnect As New Cls_Conexion
    Dim MiTransaccionSQL As SqlClient.SqlTransaction
    '---------------------------------------------------------
    Dim lstrProcedimiento As String = ""
    Dim lstrDescError As String = ""
    '---------------------------------------------------------

    '...Abre la conexion
    SQLConnect.Abrir()
    MiTransaccionSQL = SQLConnect.Transaccion
    lstrProcedimiento = "ITZ_CONT_CENTRALIZACION_ANULAR"

    Try
      SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, MiTransaccionSQL.Connection, MiTransaccionSQL)
      SQLCommand.CommandType = CommandType.StoredProcedure
      SQLCommand.CommandText = lstrProcedimiento
      SQLCommand.Parameters.Clear()

      '( @PID_MOV_SISTEMA NUMERIC
      ', @PRESULTADO VARCHAR(200) OUTPUT
      SQLCommand.Parameters.Add("PID_CENTRALIZACION", SqlDbType.Float, 3).Value = dblIdCentralizacion

      '...Resultado
      Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
      ParametroSal2.Direction = Data.ParameterDirection.Output
      SQLCommand.Parameters.Add(ParametroSal2)

      SQLCommand.ExecuteNonQuery()

      lstrDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
      If lstrDescError = "OK" Then
        MiTransaccionSQL.Commit()
      Else
        MiTransaccionSQL.Rollback()
      End If
    Catch Ex As Exception
      MiTransaccionSQL.Rollback()
      lstrDescError = "Problema al anular Centralización." & vbCr & Ex.Message
    Finally
      SQLConnect.Cerrar()
    End Try

    Return lstrDescError
  End Function
#End Region

  Public Function Traer_Tipo_Movimiento(ByVal strColumnas As String _
                                      , ByVal dblVariable As Integer _
                                      , ByRef dsDataTable As DataTable) As String
    '---------------------------------------------------------
    Dim SQLCommand As New SqlClient.SqlCommand
    Dim SQLConnect As New Cls_Conexion
    '---------------------------------------------------------
    Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
    Dim ldsDataTable As New DataTable
    '---------------------------------------------------------
    Dim lstrProcedimiento As String = "ITZ_CONT_TRAER_TIPO_MOVIMIENTO"
    Dim lstrDescError As String = ""
    '---------------------------------------------------------

    '...Abre la conexion
    SQLConnect.Abrir()

    Try
      SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, SQLConnect.Conexion)
      SQLCommand.CommandType = CommandType.StoredProcedure
      SQLCommand.CommandText = lstrProcedimiento
      SQLCommand.Parameters.Clear()
      SQLCommand.Parameters.Add("pVariable", SqlDbType.Float, 10).Value = dblVariable

      SQLCommand.ExecuteNonQuery()

      SQLDataAdapter.SelectCommand = SQLCommand
      SQLDataAdapter.Fill(ldsDataTable)

      gsubConfiguraDataTable(ldsDataTable, strColumnas)
      dsDataTable = ldsDataTable

      lstrDescError = "OK"

    Catch Ex As Exception
      lstrDescError = "Problema en el leer los tipos de movimientos.'" & vbCr & Ex.Message
    Finally
      SQLConnect.Cerrar()
      Traer_Tipo_Movimiento = lstrDescError
    End Try
  End Function


#Region "PROCESOS DE GENERACION MOVIMIENTOS"
  Public Function Generar_Devengo_Depositos(ByVal strCod_Tipo_Mov_Sistema As String _
                                          , ByVal strNum_Cuentas As String _
                                          , ByVal dtFechaDesde As Date _
                                          , ByVal dtFechaHasta As Date) As String
    '---------------------------------------------------------
    Dim SQLCommand As New SqlClient.SqlCommand
    Dim SQLConnect As New Cls_Conexion
    '---------------------------------------------------------
    Dim lstrProcedimiento As String = ""
    Dim lstrDescError As String = ""
    '---------------------------------------------------------

    '...Abre la conexion
    SQLConnect.Abrir()
    lstrProcedimiento = "ITZ_CONT_GENMOV_DEVENGO_DEPOSITOS"

    Try
      SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, SQLConnect.Conexion)
      SQLCommand.CommandType = CommandType.StoredProcedure
      SQLCommand.CommandText = lstrProcedimiento

      '( @PCOD_TIPO_MOV_SISTEMA  VARCHAR(100)
      ', @PNUM_CUENTAS           VARCHAR(2000)
      ', @PCOD_MONEDA            VARCHAR(100)
      ', @PFECHA_DESDE           VARCHAR(10)
      ', @PFECHA_HASTA           VARCHAR(10)
      ', @PID_PUSUARIO           NUMERIC                                                          
      ', @PRESULTADO             VARCHAR(1000)


      SQLCommand.Parameters.Clear()
      SQLCommand.Parameters.Add("PCOD_TIPO_MOV_SISTEMA", SqlDbType.VarChar, 100).Value = strCod_Tipo_Mov_Sistema
      SQLCommand.Parameters.Add("PNUM_CUENTAS", SqlDbType.VarChar, 2000).Value = strNum_Cuentas
      SQLCommand.Parameters.Add("PFECHA_DESDE", SqlDbType.VarChar, 10).Value = Format(dtFechaDesde, "yyyyMMdd")
      SQLCommand.Parameters.Add("PFECHA_HASTA", SqlDbType.VarChar, 10).Value = Format(dtFechaHasta, "yyyyMMdd")
      SQLCommand.Parameters.Add("PID_PUSUARIO", SqlDbType.Float, 10).Value = glngIdUsuario

      '...Resultado
      Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
      ParametroSal2.Direction = Data.ParameterDirection.Output
      SQLCommand.Parameters.Add(ParametroSal2)

      SQLCommand.ExecuteNonQuery()

      lstrDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

    Catch Ex As Exception
      lstrDescError = "Problema al generar los devengos de Depositos.'" & vbCr & Ex.Message
    Finally
      SQLConnect.Cerrar()
    End Try

    Return lstrDescError
  End Function

  Public Function Generar_Devengo_Divisa(ByVal strCod_Tipo_Mov_Sistema As String _
                                       , ByVal strNum_Cuentas As String _
                                       , ByVal dtFechaDesde As Date _
                                       , ByVal dtFechaHasta As Date) As String
    '---------------------------------------------------------
    Dim SQLCommand As New SqlClient.SqlCommand
    Dim SQLConnect As New Cls_Conexion
    '---------------------------------------------------------
    Dim lstrProcedimiento As String = ""
    Dim lstrDescError As String = ""
    '---------------------------------------------------------

    '...Abre la conexion
    SQLConnect.Abrir()
    lstrProcedimiento = "ITZ_CONT_GENMOV_DEVENGO_DIVISA"

    Try
      SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, SQLConnect.Conexion)
      SQLCommand.CommandType = CommandType.StoredProcedure
      SQLCommand.CommandText = lstrProcedimiento

      '( @PCOD_TIPO_MOV_SISTEMA  VARCHAR(100)
      ', @PNUM_CUENTAS           VARCHAR(2000)
      ', @PFECHA_DESDE           VARCHAR(10)
      ', @PFECHA_HASTA           VARCHAR(10)
      ', @PID_PUSUARIO           NUMERIC                                                          
      ', @PRESULTADO             VARCHAR(1000)


      SQLCommand.Parameters.Clear()
      SQLCommand.Parameters.Add("PCOD_TIPO_MOV_SISTEMA", SqlDbType.VarChar, 100).Value = strCod_Tipo_Mov_Sistema
      SQLCommand.Parameters.Add("PNUM_CUENTAS", SqlDbType.VarChar, 2000).Value = strNum_Cuentas
      SQLCommand.Parameters.Add("PFECHA_DESDE", SqlDbType.VarChar, 10).Value = Format(dtFechaDesde, "yyyyMMdd")
      SQLCommand.Parameters.Add("PFECHA_HASTA", SqlDbType.VarChar, 10).Value = Format(dtFechaHasta, "yyyyMMdd")
      SQLCommand.Parameters.Add("PID_PUSUARIO", SqlDbType.Float, 10).Value = glngIdUsuario

      '...Resultado
      Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
      ParametroSal2.Direction = Data.ParameterDirection.Output
      SQLCommand.Parameters.Add(ParametroSal2)

      SQLCommand.ExecuteNonQuery()

      lstrDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

    Catch Ex As Exception
      lstrDescError = "Problema al generar los devengos de Divisa.'" & vbCr & Ex.Message
    Finally
      SQLConnect.Cerrar()
    End Try

    Return lstrDescError
  End Function

  Public Function Generar_Devengo_FFMM(ByVal strCod_Tipo_Mov_Sistema As String _
                                     , ByVal strNum_Cuentas As String _
                                     , ByVal dtFechaDesde As Date _
                                     , ByVal dtFechaHasta As Date) As String
    '---------------------------------------------------------
    Dim SQLCommand As New SqlClient.SqlCommand
    Dim SQLConnect As New Cls_Conexion
    '---------------------------------------------------------
    Dim lstrProcedimiento As String = ""
    Dim lstrDescError As String = ""
    '---------------------------------------------------------

    '...Abre la conexion
    SQLConnect.Abrir()
    lstrProcedimiento = "ITZ_CONT_GENMOV_MAYORVALOR_FFMM"

    Try
      SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, SQLConnect.Conexion)
      SQLCommand.CommandType = CommandType.StoredProcedure
      SQLCommand.CommandText = lstrProcedimiento

      '( @PCOD_TIPO_MOV_SISTEMA  VARCHAR(100)
      ', @PNUM_CUENTAS           VARCHAR(2000)
      ', @PFECHA_DESDE           VARCHAR(10)
      ', @PFECHA_HASTA           VARCHAR(10)
      ', @PID_PUSUARIO           NUMERIC                                                          
      ', @PRESULTADO             VARCHAR(1000)

      SQLCommand.Parameters.Clear()
      SQLCommand.Parameters.Add("PCOD_TIPO_MOV_SISTEMA", SqlDbType.VarChar, 100).Value = strCod_Tipo_Mov_Sistema
      SQLCommand.Parameters.Add("PNUM_CUENTAS", SqlDbType.VarChar, 2000).Value = strNum_Cuentas
      SQLCommand.Parameters.Add("PFECHA_DESDE", SqlDbType.VarChar, 10).Value = Format(dtFechaDesde, "yyyyMMdd")
      SQLCommand.Parameters.Add("PFECHA_HASTA", SqlDbType.VarChar, 10).Value = Format(dtFechaHasta, "yyyyMMdd")
      SQLCommand.Parameters.Add("PID_PUSUARIO", SqlDbType.Float, 10).Value = glngIdUsuario

      '...Resultado
      Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
      ParametroSal2.Direction = Data.ParameterDirection.Output
      SQLCommand.Parameters.Add(ParametroSal2)

      SQLCommand.ExecuteNonQuery()

      lstrDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

    Catch Ex As Exception
      lstrDescError = "Problema al generar los devengos de Fondos Mutuos.'" & vbCr & Ex.Message
    Finally
      SQLConnect.Cerrar()
    End Try

    Return lstrDescError
  End Function

  Public Function Generar_Devengo_Acciones(ByVal strCod_Tipo_Mov_Sistema As String _
                                         , ByVal strNum_Cuentas As String _
                                         , ByVal dtFechaDesde As Date _
                                         , ByVal dtFechaHasta As Date) As String
    '---------------------------------------------------------
    Dim SQLCommand As New SqlClient.SqlCommand
    Dim SQLConnect As New Cls_Conexion
    '---------------------------------------------------------
    Dim lstrProcedimiento As String = ""
    Dim lstrDescError As String = ""
    '---------------------------------------------------------

    '...Abre la conexion
    SQLConnect.Abrir()
    lstrProcedimiento = "ITZ_CONT_GENMOV_DEVENGO_ACCNAC"

    Try
      SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, SQLConnect.Conexion)
      SQLCommand.CommandType = CommandType.StoredProcedure
      SQLCommand.CommandText = lstrProcedimiento

      '( @PCOD_TIPO_MOV_SISTEMA  VARCHAR(100)
      ', @PNUM_CUENTAS           VARCHAR(2000)
      ', @PCOD_MONEDA            VARCHAR(100)
      ', @PFECHA_DESDE           VARCHAR(10)
      ', @PFECHA_HASTA           VARCHAR(10)
      ', @PID_PUSUARIO           NUMERIC                                                          
      ', @PRESULTADO             VARCHAR(1000) OUTPUT

      SQLCommand.Parameters.Add("PCOD_TIPO_MOV_SISTEMA", SqlDbType.VarChar, 100).Value = strCod_Tipo_Mov_Sistema
      SQLCommand.Parameters.Add("PNUM_CUENTAS", SqlDbType.VarChar, 2000).Value = strNum_Cuentas
      SQLCommand.Parameters.Add("PFECHA_DESDE", SqlDbType.VarChar, 10).Value = Format(dtFechaDesde, "yyyyMMdd")
      SQLCommand.Parameters.Add("PFECHA_HASTA", SqlDbType.VarChar, 10).Value = Format(dtFechaHasta, "yyyyMMdd")
      SQLCommand.Parameters.Add("PID_PUSUARIO", SqlDbType.Float, 10).Value = glngIdUsuario

      '...Resultado
      Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 2000)
      ParametroSal2.Direction = Data.ParameterDirection.Output
      SQLCommand.Parameters.Add(ParametroSal2)

      SQLCommand.ExecuteNonQuery()

      lstrDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

    Catch Ex As Exception
      lstrDescError = "Problema al generar los devengos de Acciones.'" & vbCr & Ex.Message
    Finally
      SQLConnect.Cerrar()
    End Try

    Return lstrDescError
  End Function

  Public Function Generar_Devengo_Bonos(ByVal strCod_Tipo_Mov_Sistema As String _
                                       , ByVal strNum_Cuentas As String _
                                       , ByVal dtFechaDesde As Date _
                                       , ByVal dtFechaHasta As Date) As String
    '---------------------------------------------------------
    Dim SQLCommand As New SqlClient.SqlCommand
    Dim SQLConnect As New Cls_Conexion
    '---------------------------------------------------------
    Dim lstrProcedimiento As String = ""
    Dim lstrDescError As String = ""
    '---------------------------------------------------------

    '...Abre la conexion
    SQLConnect.Abrir()
    lstrProcedimiento = "ITZ_CONT_GENMOV_DEVENGO_BONOS"

    Try
      SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, SQLConnect.Conexion)
      SQLCommand.CommandType = CommandType.StoredProcedure
      SQLCommand.CommandText = lstrProcedimiento

      '( @PCOD_TIPO_MOV_SISTEMA  VARCHAR(100)
      ', @PNUM_CUENTAS           VARCHAR(2000)
      ', @PCOD_MONEDA            VARCHAR(100)
      ', @PFECHA_DESDE           VARCHAR(10)
      ', @PFECHA_HASTA           VARCHAR(10)
      ', @PID_PUSUARIO           NUMERIC                                                          
      ', @PRESULTADO             VARCHAR(1000) OUTPUT

      SQLCommand.Parameters.Add("PCOD_TIPO_MOV_SISTEMA", SqlDbType.VarChar, 100).Value = strCod_Tipo_Mov_Sistema
      SQLCommand.Parameters.Add("PNUM_CUENTAS", SqlDbType.VarChar, 2000).Value = strNum_Cuentas
      SQLCommand.Parameters.Add("PFECHA_DESDE", SqlDbType.VarChar, 10).Value = Format(dtFechaDesde, "yyyyMMdd")
      SQLCommand.Parameters.Add("PFECHA_HASTA", SqlDbType.VarChar, 10).Value = Format(dtFechaHasta, "yyyyMMdd")
      SQLCommand.Parameters.Add("PID_PUSUARIO", SqlDbType.Float, 10).Value = glngIdUsuario

      '...Resultado
      Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 2000)
      ParametroSal2.Direction = Data.ParameterDirection.Output
      SQLCommand.Parameters.Add(ParametroSal2)

      SQLCommand.ExecuteNonQuery()

      lstrDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

    Catch Ex As Exception
      lstrDescError = "Problema al generar los devengos de Bonos.'" & vbCr & Ex.Message
    Finally
      SQLConnect.Cerrar()
    End Try

    Return lstrDescError
  End Function

  Public Function Generar_Operaciones(ByVal strCod_Tipo_Mov_Sistema As String _
                                    , ByVal strNum_Cuentas As String _
                                    , ByVal dtFechaDesde As Date _
                                    , ByVal dtFechaHasta As Date) As String
    '---------------------------------------------------------
    Dim SQLCommand As New SqlClient.SqlCommand
    Dim SQLConnect As New Cls_Conexion
    '---------------------------------------------------------
    Dim lstrProcedimiento As String = ""
    Dim lstrDescError As String = ""
    '---------------------------------------------------------

    '...Abre la conexion
    SQLConnect.Abrir()
    lstrProcedimiento = "ITZ_CONT_GENMOV_OPERACIONES"

    Try
      SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, SQLConnect.Conexion)
      SQLCommand.CommandType = CommandType.StoredProcedure
      SQLCommand.CommandText = lstrProcedimiento

      '( @PCOD_TIPO_MOV_SISTEMA  VARCHAR(100)
      ', @PNUM_CUENTAS           VARCHAR(2000)
      ', @PFECHA_DESDE           VARCHAR(10)
      ', @PFECHA_HASTA           VARCHAR(10)
      ', @PID_PUSUARIO           NUMERIC                                                          
      ', @PRESULTADO             VARCHAR(1000) OUTPUT

      SQLCommand.Parameters.Add("PCOD_TIPO_MOV_SISTEMA", SqlDbType.VarChar, 100).Value = strCod_Tipo_Mov_Sistema
      SQLCommand.Parameters.Add("PNUM_CUENTAS", SqlDbType.VarChar, 2000).Value = strNum_Cuentas
      SQLCommand.Parameters.Add("PFECHA_DESDE", SqlDbType.VarChar, 10).Value = Format(dtFechaDesde, "yyyyMMdd")
      SQLCommand.Parameters.Add("PFECHA_HASTA", SqlDbType.VarChar, 10).Value = Format(dtFechaHasta, "yyyyMMdd")
      SQLCommand.Parameters.Add("PID_PUSUARIO", SqlDbType.Float, 10).Value = glngIdUsuario

      '...Resultado
      Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 2000)
      ParametroSal2.Direction = Data.ParameterDirection.Output
      SQLCommand.Parameters.Add(ParametroSal2)

      SQLCommand.ExecuteNonQuery()

      lstrDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

    Catch Ex As Exception
      lstrDescError = "Problema al generar los Operaciones.'" & vbCr & Ex.Message
    Finally
      SQLConnect.Cerrar()
    End Try

    Return lstrDescError
  End Function

  Public Function Generar_MovCaja(ByVal strCod_Tipo_Mov_Sistema As String _
                                , ByVal strNum_Cuentas As String _
                                , ByVal dtFechaDesde As Date _
                                , ByVal dtFechaHasta As Date) As String
    '---------------------------------------------------------
    Dim SQLCommand As New SqlClient.SqlCommand
    Dim SQLConnect As New Cls_Conexion
    '---------------------------------------------------------
    Dim lstrProcedimiento As String = ""
    Dim lstrDescError As String = ""
    '---------------------------------------------------------

    '...Abre la conexion
    SQLConnect.Abrir()
    lstrProcedimiento = "ITZ_CONT_GENMOV_CAJA"

    Try
      SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, SQLConnect.Conexion)
      SQLCommand.CommandType = CommandType.StoredProcedure
      SQLCommand.CommandText = lstrProcedimiento

      '( @PCOD_TIPO_MOV_SISTEMA  VARCHAR(100)
      ', @PNUM_CUENTAS           VARCHAR(2000)
      ', @PFECHA_DESDE           VARCHAR(10)
      ', @PFECHA_HASTA           VARCHAR(10)
      ', @PID_PUSUARIO           NUMERIC                                                          
      ', @PRESULTADO             VARCHAR(1000) OUTPUT

      SQLCommand.Parameters.Add("PCOD_TIPO_MOV_SISTEMA", SqlDbType.VarChar, 100).Value = strCod_Tipo_Mov_Sistema
      SQLCommand.Parameters.Add("PNUM_CUENTAS", SqlDbType.VarChar, 2000).Value = strNum_Cuentas
      SQLCommand.Parameters.Add("PFECHA_DESDE", SqlDbType.VarChar, 10).Value = Format(dtFechaDesde, "yyyyMMdd")
      SQLCommand.Parameters.Add("PFECHA_HASTA", SqlDbType.VarChar, 10).Value = Format(dtFechaHasta, "yyyyMMdd")
      SQLCommand.Parameters.Add("PID_PUSUARIO", SqlDbType.Float, 10).Value = glngIdUsuario

      '...Resultado
      Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 2000)
      ParametroSal2.Direction = Data.ParameterDirection.Output
      SQLCommand.Parameters.Add(ParametroSal2)

      SQLCommand.ExecuteNonQuery()

      lstrDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

    Catch Ex As Exception
      lstrDescError = "Problema al generar los Movimientos de Caja.'" & vbCr & Ex.Message
    Finally
      SQLConnect.Cerrar()
    End Try

    Return lstrDescError
  End Function

  Public Function Buscar_Cuentas(ByVal lndId_Negocio As Long _
                               , ByRef dsDataTable As DataTable) As String
    '---------------------------------------------------------
    Dim SQLCommand As New SqlClient.SqlCommand
    Dim SQLConnect As New Cls_Conexion
    '---------------------------------------------------------
    Dim lstr_NombreTabla As String = "TABLA"
    Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
    Dim ldsDataSet As New DataSet
    '---------------------------------------------------------
    Dim lstrProcedimiento As String = ""
    Dim lstrDescError As String = ""
    '---------------------------------------------------------

    '...Abre la conexion
    SQLConnect.Abrir()
    lstrProcedimiento = "ITZ_CONT_BUSCAR_CUENTAS"

    Try
      SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, SQLConnect.Conexion)
      SQLCommand.CommandType = CommandType.StoredProcedure
      SQLCommand.CommandText = lstrProcedimiento
      SQLCommand.Parameters.Clear()

      SQLCommand.Parameters.Add("PID_NEGOCIO", SqlDbType.Float, 3).Value = lndId_Negocio

      SQLCommand.ExecuteNonQuery()

      SQLDataAdapter.SelectCommand = SQLCommand
      SQLDataAdapter.Fill(ldsDataSet, lstr_NombreTabla)

      dsDataTable = ldsDataSet.Tables(lstr_NombreTabla)

      lstrDescError = "OK"

    Catch Ex As Exception
      lstrDescError = "Problema en el leer las Cuentas.'" & vbCr & Ex.Message
    Finally
      SQLConnect.Cerrar()
    End Try

    Return lstrDescError
  End Function

  Public Function Generar_GENMOV_FWD_CONTRATO(ByVal strCod_Tipo_Mov_Sistema As String _
                                            , ByVal strNum_Cuentas As String _
                                            , ByVal dtFechaDesde As Date _
                                            , ByVal dtFechaHasta As Date) As String
    '---------------------------------------------------------
    Dim SQLCommand As New SqlClient.SqlCommand
    Dim SQLConnect As New Cls_Conexion
    '---------------------------------------------------------
    Dim lstrProcedimiento As String = ""
    Dim lstrDescError As String = ""
    '---------------------------------------------------------

    '...Abre la conexion
    SQLConnect.Abrir()
    lstrProcedimiento = "ITZ_CONT_GENMOV_FWD_CONTRATO"

    Try
      SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, SQLConnect.Conexion)
      SQLCommand.CommandType = CommandType.StoredProcedure
      SQLCommand.CommandText = lstrProcedimiento

      '( @PCOD_TIPO_MOV_SISTEMA  VARCHAR(100)
      ', @PNUM_CUENTAS           VARCHAR(2000)
      ', @PFECHA_DESDE           VARCHAR(10)
      ', @PFECHA_HASTA           VARCHAR(10)
      ', @PID_PUSUARIO           NUMERIC                                                          
      ', @PRESULTADO             VARCHAR(1000)

      SQLCommand.Parameters.Clear()
      SQLCommand.Parameters.Add("PCOD_TIPO_MOV_SISTEMA", SqlDbType.VarChar, 100).Value = strCod_Tipo_Mov_Sistema
      SQLCommand.Parameters.Add("PNUM_CUENTAS", SqlDbType.VarChar, 2000).Value = strNum_Cuentas
      SQLCommand.Parameters.Add("PFECHA_DESDE", SqlDbType.VarChar, 10).Value = Format(dtFechaDesde, "yyyyMMdd")
      SQLCommand.Parameters.Add("PFECHA_HASTA", SqlDbType.VarChar, 10).Value = Format(dtFechaHasta, "yyyyMMdd")
      SQLCommand.Parameters.Add("PID_PUSUARIO", SqlDbType.Float, 10).Value = glngIdUsuario

      '...Resultado
      Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
      ParametroSal2.Direction = Data.ParameterDirection.Output
      SQLCommand.Parameters.Add(ParametroSal2)

      SQLCommand.ExecuteNonQuery()

      lstrDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

    Catch Ex As Exception
      lstrDescError = "Problema al generar los devengos de Fondos Mutuos.'" & vbCr & Ex.Message
    Finally
      SQLConnect.Cerrar()
    End Try

    Return lstrDescError
  End Function

  Public Function Generar_GENMOV_FWD_VENCIMIENTO(ByVal strCod_Tipo_Mov_Sistema As String _
                                               , ByVal strNum_Cuentas As String _
                                               , ByVal dtFechaDesde As Date _
                                               , ByVal dtFechaHasta As Date) As String
    '---------------------------------------------------------
    Dim SQLCommand As New SqlClient.SqlCommand
    Dim SQLConnect As New Cls_Conexion
    '---------------------------------------------------------
    Dim lstrProcedimiento As String = ""
    Dim lstrDescError As String = ""
    '---------------------------------------------------------

    '...Abre la conexion
    SQLConnect.Abrir()
    lstrProcedimiento = "ITZ_CONT_GENMOV_FWD_VENCIMIENTO"

    Try
      SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, SQLConnect.Conexion)
      SQLCommand.CommandType = CommandType.StoredProcedure
      SQLCommand.CommandText = lstrProcedimiento

      '( @PCOD_TIPO_MOV_SISTEMA  VARCHAR(100)
      ', @PNUM_CUENTAS           VARCHAR(2000)
      ', @PFECHA_DESDE           VARCHAR(10)
      ', @PFECHA_HASTA           VARCHAR(10)
      ', @PID_PUSUARIO           NUMERIC                                                          
      ', @PRESULTADO             VARCHAR(1000)

      SQLCommand.Parameters.Clear()
      SQLCommand.Parameters.Add("PCOD_TIPO_MOV_SISTEMA", SqlDbType.VarChar, 100).Value = strCod_Tipo_Mov_Sistema
      SQLCommand.Parameters.Add("PNUM_CUENTAS", SqlDbType.VarChar, 2000).Value = strNum_Cuentas
      SQLCommand.Parameters.Add("PFECHA_DESDE", SqlDbType.VarChar, 10).Value = Format(dtFechaDesde, "yyyyMMdd")
      SQLCommand.Parameters.Add("PFECHA_HASTA", SqlDbType.VarChar, 10).Value = Format(dtFechaHasta, "yyyyMMdd")
      SQLCommand.Parameters.Add("PID_PUSUARIO", SqlDbType.Float, 10).Value = glngIdUsuario

      '...Resultado
      Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
      ParametroSal2.Direction = Data.ParameterDirection.Output
      SQLCommand.Parameters.Add(ParametroSal2)

      SQLCommand.ExecuteNonQuery()

      lstrDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

    Catch Ex As Exception
      lstrDescError = "Problema al generar los devengos de Fondos Mutuos.'" & vbCr & Ex.Message
    Finally
      SQLConnect.Cerrar()
    End Try

    Return lstrDescError
  End Function
#End Region

#Region "GENERA CENTRALIZACIONES"
  Public Function Generar_Centralizacion_Acciones(ByVal strCod_Tipo_Mov_Sistema As String _
                                                , ByVal strNum_Cuentas As String _
                                                , ByVal dtFechaDesde As Date _
                                                , ByVal dtFechaHasta As Date) As String
    '---------------------------------------------------------
    Dim SQLCommand As New SqlClient.SqlCommand
    Dim SQLConnect As New Cls_Conexion
    Dim MiTransaccionSQL As SqlClient.SqlTransaction
    '---------------------------------------------------------
    Dim lstrProcedimiento As String = ""
    Dim lstrDescError As String = ""
    '---------------------------------------------------------

    '...Abre la conexion
    SQLConnect.Abrir()
    MiTransaccionSQL = SQLConnect.Transaccion
    lstrProcedimiento = "ITZ_CONT_CENTRALIZA_DEVENGO_ACCNAC"

    Try
      SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, MiTransaccionSQL.Connection, MiTransaccionSQL)
      SQLCommand.CommandType = CommandType.StoredProcedure
      SQLCommand.CommandText = lstrProcedimiento

      SQLCommand.Parameters.Clear()
      SQLCommand.Parameters.Add("PCOD_TIPO_MOV_SISTEMA", SqlDbType.VarChar, 100).Value = strCod_Tipo_Mov_Sistema
      SQLCommand.Parameters.Add("PNUM_CUENTAS", SqlDbType.VarChar, 2000).Value = strNum_Cuentas
      SQLCommand.Parameters.Add("PFECHA_DESDE", SqlDbType.VarChar, 10).Value = Format(dtFechaDesde, "yyyyMMdd")
      SQLCommand.Parameters.Add("PFECHA_HASTA", SqlDbType.VarChar, 10).Value = Format(dtFechaHasta, "yyyyMMdd")
      SQLCommand.Parameters.Add("PID_PUSUARIO", SqlDbType.Float, 10).Value = glngIdUsuario

      '...Resultado
      Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
      ParametroSal2.Direction = Data.ParameterDirection.Output
      SQLCommand.Parameters.Add(ParametroSal2)

      SQLCommand.ExecuteNonQuery()

      lstrDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

      If lstrDescError = "OK" Then
        MiTransaccionSQL.Commit()
      Else
        MiTransaccionSQL.Rollback()
      End If
    Catch Ex As Exception
      MiTransaccionSQL.Rollback()
      lstrDescError = "Problema al generar Centralizacion de los devengos de Acciones.'" & vbCr & Ex.Message
    Finally
      SQLConnect.Cerrar()
    End Try

    Return lstrDescError
  End Function
  Public Function Generar_Centralizacion_DevDep(ByVal strCod_Tipo_Mov_Sistema As String _
                                                , ByVal strNum_Cuentas As String _
                                                , ByVal dtFechaDesde As Date _
                                                , ByVal dtFechaHasta As Date) As String
    '---------------------------------------------------------
    Dim SQLCommand As New SqlClient.SqlCommand
    Dim SQLConnect As New Cls_Conexion
    Dim MiTransaccionSQL As SqlClient.SqlTransaction
    '---------------------------------------------------------
    Dim lstrProcedimiento As String = ""
    Dim lstrDescError As String = ""
    '---------------------------------------------------------

    '...Abre la conexion
    SQLConnect.Abrir()
    MiTransaccionSQL = SQLConnect.Transaccion
    lstrProcedimiento = "ITZ_CONT_CENTRALIZA_DEVENGO_DEVDEP"

    Try
      SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, MiTransaccionSQL.Connection, MiTransaccionSQL)
      SQLCommand.CommandType = CommandType.StoredProcedure
      SQLCommand.CommandText = lstrProcedimiento

      SQLCommand.Parameters.Clear()
      SQLCommand.Parameters.Add("PCOD_TIPO_MOV_SISTEMA", SqlDbType.VarChar, 100).Value = strCod_Tipo_Mov_Sistema
      SQLCommand.Parameters.Add("PNUM_CUENTAS", SqlDbType.VarChar, 2000).Value = strNum_Cuentas
      SQLCommand.Parameters.Add("PFECHA_DESDE", SqlDbType.VarChar, 10).Value = Format(dtFechaDesde, "yyyyMMdd")
      SQLCommand.Parameters.Add("PFECHA_HASTA", SqlDbType.VarChar, 10).Value = Format(dtFechaHasta, "yyyyMMdd")
      SQLCommand.Parameters.Add("PID_PUSUARIO", SqlDbType.Float, 10).Value = glngIdUsuario

      '...Resultado
      Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
      ParametroSal2.Direction = Data.ParameterDirection.Output
      SQLCommand.Parameters.Add(ParametroSal2)

      SQLCommand.ExecuteNonQuery()

      lstrDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

      If lstrDescError = "OK" Then
        MiTransaccionSQL.Commit()
      Else
        MiTransaccionSQL.Rollback()
      End If
    Catch Ex As Exception
      MiTransaccionSQL.Rollback()
      lstrDescError = "Problema al generar Centralizacion de los devengos de Depósitos.'" & vbCr & Ex.Message
    Finally
      SQLConnect.Cerrar()
    End Try

    Return lstrDescError
  End Function
  Public Function Generar_Centralizacion_DevBonos(ByVal strCod_Tipo_Mov_Sistema As String _
                                                    , ByVal strNum_Cuentas As String _
                                                    , ByVal dtFechaDesde As Date _
                                                    , ByVal dtFechaHasta As Date) As String
    '---------------------------------------------------------
    Dim SQLCommand As New SqlClient.SqlCommand
    Dim SQLConnect As New Cls_Conexion
    Dim MiTransaccionSQL As SqlClient.SqlTransaction
    '---------------------------------------------------------
    Dim lstrProcedimiento As String = ""
    Dim lstrDescError As String = ""
    '---------------------------------------------------------

    '...Abre la conexion
    SQLConnect.Abrir()
    MiTransaccionSQL = SQLConnect.Transaccion
    lstrProcedimiento = "ITZ_CONT_CENTRALIZA_DEVENGO_DEVBONOS"

    Try
      SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, MiTransaccionSQL.Connection, MiTransaccionSQL)
      SQLCommand.CommandType = CommandType.StoredProcedure
      SQLCommand.CommandText = lstrProcedimiento

      SQLCommand.Parameters.Clear()
      SQLCommand.Parameters.Add("PCOD_TIPO_MOV_SISTEMA", SqlDbType.VarChar, 100).Value = strCod_Tipo_Mov_Sistema
      SQLCommand.Parameters.Add("PNUM_CUENTAS", SqlDbType.VarChar, 2000).Value = strNum_Cuentas
      SQLCommand.Parameters.Add("PFECHA_DESDE", SqlDbType.VarChar, 10).Value = Format(dtFechaDesde, "yyyyMMdd")
      SQLCommand.Parameters.Add("PFECHA_HASTA", SqlDbType.VarChar, 10).Value = Format(dtFechaHasta, "yyyyMMdd")
      SQLCommand.Parameters.Add("PID_PUSUARIO", SqlDbType.Float, 10).Value = glngIdUsuario

      '...Resultado
      Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
      ParametroSal2.Direction = Data.ParameterDirection.Output
      SQLCommand.Parameters.Add(ParametroSal2)

      SQLCommand.ExecuteNonQuery()

      lstrDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

      If lstrDescError = "OK" Then
        MiTransaccionSQL.Commit()
      Else
        MiTransaccionSQL.Rollback()
      End If
    Catch Ex As Exception
      MiTransaccionSQL.Rollback()
      lstrDescError = "Problema al generar Centralizacion de los devengos de Bonos.'" & vbCr & Ex.Message
    Finally
      SQLConnect.Cerrar()
    End Try

    Return lstrDescError
  End Function
  Public Function Generar_Centralizacion_DevFFMM(ByVal strCod_Tipo_Mov_Sistema As String _
                                                , ByVal strNum_Cuentas As String _
                                                , ByVal dtFechaDesde As Date _
                                                , ByVal dtFechaHasta As Date) As String
    '---------------------------------------------------------
    Dim SQLCommand As New SqlClient.SqlCommand
    Dim SQLConnect As New Cls_Conexion
    Dim MiTransaccionSQL As SqlClient.SqlTransaction
    '---------------------------------------------------------
    Dim lstrProcedimiento As String = ""
    Dim lstrDescError As String = ""
    '---------------------------------------------------------

    '...Abre la conexion
    SQLConnect.Abrir()
    MiTransaccionSQL = SQLConnect.Transaccion
    lstrProcedimiento = "ITZ_CONT_CENTRALIZA_DEVENGO_DEVFFMM"

    Try
      SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, MiTransaccionSQL.Connection, MiTransaccionSQL)
      SQLCommand.CommandType = CommandType.StoredProcedure
      SQLCommand.CommandText = lstrProcedimiento

      SQLCommand.Parameters.Clear()
      SQLCommand.Parameters.Add("PCOD_TIPO_MOV_SISTEMA", SqlDbType.VarChar, 100).Value = strCod_Tipo_Mov_Sistema
      SQLCommand.Parameters.Add("PNUM_CUENTAS", SqlDbType.VarChar, 2000).Value = strNum_Cuentas
      SQLCommand.Parameters.Add("PFECHA_DESDE", SqlDbType.VarChar, 10).Value = Format(dtFechaDesde, "yyyyMMdd")
      SQLCommand.Parameters.Add("PFECHA_HASTA", SqlDbType.VarChar, 10).Value = Format(dtFechaHasta, "yyyyMMdd")
      SQLCommand.Parameters.Add("PID_PUSUARIO", SqlDbType.Float, 10).Value = glngIdUsuario

      '...Resultado
      Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
      ParametroSal2.Direction = Data.ParameterDirection.Output
      SQLCommand.Parameters.Add(ParametroSal2)

      SQLCommand.ExecuteNonQuery()

      lstrDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

      If lstrDescError = "OK" Then
        MiTransaccionSQL.Commit()
      Else
        MiTransaccionSQL.Rollback()
      End If
    Catch Ex As Exception
      MiTransaccionSQL.Rollback()
      lstrDescError = "Problema al generar Centralizacion de los devengos de Fondos Mutuos.'" & vbCr & Ex.Message
    Finally
      SQLConnect.Cerrar()
    End Try

    Return lstrDescError
  End Function
  Public Function Generar_Centralizacion_DevDivisa(ByVal strCod_Tipo_Mov_Sistema As String _
                                                 , ByVal strNum_Cuentas As String _
                                                 , ByVal dtFechaDesde As Date _
                                                 , ByVal dtFechaHasta As Date) As String
    '---------------------------------------------------------
    Dim SQLCommand As New SqlClient.SqlCommand
    Dim SQLConnect As New Cls_Conexion
    Dim MiTransaccionSQL As SqlClient.SqlTransaction
    '---------------------------------------------------------
    Dim lstrProcedimiento As String = ""
    Dim lstrDescError As String = ""
    '---------------------------------------------------------

    '...Abre la conexion
    SQLConnect.Abrir()
    MiTransaccionSQL = SQLConnect.Transaccion
    lstrProcedimiento = "ITZ_CONT_CENTRALIZA_DEVENGO_DEVDIVISA"

    Try
      SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, MiTransaccionSQL.Connection, MiTransaccionSQL)
      SQLCommand.CommandType = CommandType.StoredProcedure
      SQLCommand.CommandText = lstrProcedimiento

      SQLCommand.Parameters.Clear()
      SQLCommand.Parameters.Add("PCOD_TIPO_MOV_SISTEMA", SqlDbType.VarChar, 100).Value = strCod_Tipo_Mov_Sistema
      SQLCommand.Parameters.Add("PNUM_CUENTAS", SqlDbType.VarChar, 2000).Value = strNum_Cuentas
      SQLCommand.Parameters.Add("PFECHA_DESDE", SqlDbType.VarChar, 10).Value = Format(dtFechaDesde, "yyyyMMdd")
      SQLCommand.Parameters.Add("PFECHA_HASTA", SqlDbType.VarChar, 10).Value = Format(dtFechaHasta, "yyyyMMdd")
      SQLCommand.Parameters.Add("PID_PUSUARIO", SqlDbType.Float, 10).Value = glngIdUsuario

      '...Resultado
      Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
      ParametroSal2.Direction = Data.ParameterDirection.Output
      SQLCommand.Parameters.Add(ParametroSal2)

      SQLCommand.ExecuteNonQuery()

      lstrDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

      If lstrDescError = "OK" Then
        MiTransaccionSQL.Commit()
      Else
        MiTransaccionSQL.Rollback()
      End If
    Catch Ex As Exception
      MiTransaccionSQL.Rollback()
      lstrDescError = "Problema al generar Centralizacion de los devengos de Divisas.'" & vbCr & Ex.Message
    Finally
      SQLConnect.Cerrar()
    End Try

    Return lstrDescError
  End Function
#End Region

#Region "LISTADOS"
  Public Function Traer_Centralizaciones(ByVal dtFechaDesde As Date, _
                                         ByVal dtFechaHasta As Date, _
                                         ByVal strColumnas As String, _
                                         ByRef strRetorno As String) As DataTable
    Dim SQLCommand As New SqlClient.SqlCommand
    Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
    Dim Connect As Cls_Conexion = New Cls_Conexion

    Dim ldtCartera As New DataTable
    Dim LstrProcedimiento = "ITZ_CONT_CENTRALIZA_LISTAR"

    Try
      Connect.Abrir()

      SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

      SQLCommand.CommandType = CommandType.StoredProcedure
      SQLCommand.CommandText = LstrProcedimiento
      SQLCommand.Parameters.Clear()

      SQLCommand.Parameters.Add("PFECHA_DESDE", SqlDbType.VarChar, 10).Value = dtFechaDesde.ToString("yyyyMMdd")
      SQLCommand.Parameters.Add("PFECHA_HASTA", SqlDbType.VarChar, 10).Value = dtFechaHasta.ToString("yyyyMMdd")

      SQLDataAdapter.SelectCommand = SQLCommand
      SQLDataAdapter.Fill(ldtCartera)

      gsubConfiguraDataTable(ldtCartera, strColumnas)

      strRetorno = "OK"
    Catch ex As Exception
      strRetorno = ex.Message
      ldtCartera = Nothing
    Finally
      Connect.Cerrar()
    End Try

    Return ldtCartera
  End Function
#End Region

  Public Function TipoMovSistema_Ver(ByVal strCodMovSistema As String, _
                                     ByVal strColumnas As String, _
                                     ByRef strRetorno As String) As DataSet

    Dim LDS_TipoOperacion As New DataSet
    Dim LDS_TipoOperacion_Aux As New DataSet
    Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
    Dim LInt_Col As Integer = 0
    Dim LInt_NomCol As String = ""
    Dim Lstr_NombreTabla As String = ""
    Dim Columna As DataColumn
    Dim Remove As Boolean = True
    Dim LstrProcedimiento As String = ""

    Dim SQLCommand As New SqlClient.SqlCommand
    Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

    Dim Connect As Cls_Conexion = New Cls_Conexion

    LstrProcedimiento = "Rcp_TipoMovSistema_Ver"
    Lstr_NombreTabla = "TIPO_MOV"

    Connect.Abrir()

    Try
      SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

      SQLCommand.CommandType = CommandType.StoredProcedure
      SQLCommand.CommandText = LstrProcedimiento
      SQLCommand.Parameters.Clear()

      SQLCommand.Parameters.Add("pCodTipoMovSistema", SqlDbType.VarChar, 50).Value = IIf(strCodMovSistema.Trim = "", DBNull.Value, strCodMovSistema.Trim)

      SQLDataAdapter.SelectCommand = SQLCommand
      SQLDataAdapter.Fill(LDS_TipoOperacion, Lstr_NombreTabla)

      LDS_TipoOperacion_Aux = LDS_TipoOperacion

      If strColumnas.Trim = "" Then
        LDS_TipoOperacion_Aux = LDS_TipoOperacion
      Else
        LDS_TipoOperacion_Aux = LDS_TipoOperacion.Copy
        For Each Columna In LDS_TipoOperacion.Tables(Lstr_NombreTabla).Columns
          Remove = True
          For LInt_Col = 0 To LArr_NombreColumna.Length - 1
            LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
            If Columna.ColumnName = LInt_NomCol Then
              Remove = False
            End If
          Next
          If Remove Then
            LDS_TipoOperacion_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
          End If
        Next
      End If

      For LInt_Col = 0 To LArr_NombreColumna.Length - 1
        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
        For Each Columna In LDS_TipoOperacion_Aux.Tables(Lstr_NombreTabla).Columns
          If Columna.ColumnName = LInt_NomCol Then
            Columna.SetOrdinal(LInt_Col)
            Exit For
          End If
        Next
      Next

      strRetorno = "OK"
      LDS_TipoOperacion.Dispose()
      Return LDS_TipoOperacion_Aux

    Catch ex As Exception
      strRetorno = ex.Message
      Return Nothing
    Finally
      Connect.Cerrar()
    End Try
  End Function
End Class


