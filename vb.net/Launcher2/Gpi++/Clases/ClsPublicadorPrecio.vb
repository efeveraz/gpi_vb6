﻿Public Class ClsPublicadorPrecio
    Public Function BuscarPrecioporFecha(ByVal intIdPublicador As Integer, _
                                         ByVal strCodSubClase As String, _
                                         ByVal strFecha As String, _
                                         ByVal strColumnas As String, _
                                         ByRef strRetorno As String) As DataSet

        Dim LDS_PublicadorPrecio As New DataSet
        Dim LDS_PublicadorPrecio_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_PublicadorPrecio_BuscarPorFecha"
        Lstr_NombreTabla = "PublicadorPrecio"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdPublicador", SqlDbType.Int).Value = intIdPublicador
            SQLCommand.Parameters.Add("@pCodClaseInstrumento", SqlDbType.VarChar, 15).Value = strCodSubClase
            SQLCommand.Parameters.Add("@pfecha", SqlDbType.Char, 10).Value = strFecha

            Dim oParamSal As New SqlClient.SqlParameter("@pResultado", SqlDbType.Char, 200)
            oParamSal.Direction = ParameterDirection.Output
            SQLCommand.Parameters.Add(oParamSal)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_PublicadorPrecio, Lstr_NombreTabla)

            LDS_PublicadorPrecio_Aux = LDS_PublicadorPrecio

            If strColumnas.Trim = "" Then
                LDS_PublicadorPrecio_Aux = LDS_PublicadorPrecio
            Else
                LDS_PublicadorPrecio_Aux = LDS_PublicadorPrecio.Copy
                For Each Columna In LDS_PublicadorPrecio.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_PublicadorPrecio_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_PublicadorPrecio_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = SQLCommand.Parameters("@pResultado").Value.ToString.Trim

            LDS_PublicadorPrecio.Dispose()

            Return LDS_PublicadorPrecio_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function BuscarPrecioporInstrumento(ByVal intIdPublicador As Integer, _
                                               ByVal strCodSubClase As String, _
                                               ByVal intIdInstrumento As Integer, _
                                               ByVal strFechaDesde As String, _
                                               ByVal strFechaHasta As String, _
                                               ByVal strColumnas As String, _
                                               ByRef strRetorno As String) As DataSet

        Dim LDS_PublicadorPrecio As New DataSet
        Dim LDS_PublicadorPrecio_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_PublicadorPrecio_BuscarPorInstrumento"
        Lstr_NombreTabla = "PublicadorPrecio"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("@pIdPublicador", SqlDbType.Int).Value = intIdPublicador
            SQLCommand.Parameters.Add("@pCodClaseInstrumento", SqlDbType.VarChar, 15).Value = strCodSubClase
            SQLCommand.Parameters.Add("@pIdInstrumento", SqlDbType.Int).Value = intIdInstrumento
            SQLCommand.Parameters.Add("@pfechaDesde", SqlDbType.Char, 10).Value = strFechaDesde
            SQLCommand.Parameters.Add("@pfechaHasta", SqlDbType.Char, 10).Value = strFechaHasta

            Dim oParamSal As New SqlClient.SqlParameter("@pResultado", SqlDbType.Char, 200)
            oParamSal.Direction = ParameterDirection.Output
            SQLCommand.Parameters.Add(oParamSal)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_PublicadorPrecio, Lstr_NombreTabla)

            LDS_PublicadorPrecio_Aux = LDS_PublicadorPrecio

            If strColumnas.Trim = "" Then
                LDS_PublicadorPrecio_Aux = LDS_PublicadorPrecio
            Else
                LDS_PublicadorPrecio_Aux = LDS_PublicadorPrecio.Copy
                For Each Columna In LDS_PublicadorPrecio.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_PublicadorPrecio_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_PublicadorPrecio_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = SQLCommand.Parameters("@pResultado").Value.ToString.Trim

            LDS_PublicadorPrecio.Dispose()

            Return LDS_PublicadorPrecio_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Sub PublicadorPreciosMantenedor(ByVal intIdPublicador As Integer, _
                                           ByVal strCodClaseInstrumento As String, _
                                           ByVal DS_PublicadorPrecio As DataSet, _
                                           ByRef strRetorno As String)
        Dim DR_Fila As DataRow = Nothing
        Dim lblnConValor As Boolean
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        'Abre la conexion
        Connect.Abrir()

        Dim MiTransaccion As SqlClient.SqlTransaction


        MiTransaccion = Connect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_PublicadorPrecio_Mantencion", MiTransaccion.Connection, MiTransaccion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_PublicadorPrecio_Mantencion"

            For Each DR_Fila In DS_PublicadorPrecio.Tables(0).Select("OPERACION = 1")
                SQLCommand.Parameters.Clear()
                If strCodClaseInstrumento = "RF_NAC" Or strCodClaseInstrumento = "RF_INT" Then
                    If IsDBNull(DR_Fila("TASA")) Then
                        lblnConValor = False
                    Else
                        lblnConValor = True
                    End If
                Else
                    If IsDBNull(DR_Fila("PRECIO")) Then
                        lblnConValor = False
                    Else
                        lblnConValor = True
                    End If
                End If

                'INSERTA
                If IsDBNull(DR_Fila("ID_PUBLICADOR_PRECIO")) And lblnConValor Then
                    SQLCommand.Parameters.Add("@pAccion", SqlDbType.VarChar, 10).Value = "INSERTAR"

                    SQLCommand.Parameters.Add("@pIdPublicador", SqlDbType.Int).Value = intIdPublicador
                    SQLCommand.Parameters.Add("@pIdInstrumento", SqlDbType.Int).Value = DR_Fila("ID_INSTRUMENTO")
                    SQLCommand.Parameters.Add("@pCodMoneda", SqlDbType.Char, 3).Value = DR_Fila("COD_MONEDA")
                    SQLCommand.Parameters.Add("@pfecha", SqlDbType.Char, 10).Value = DR_Fila("FECHA")
                    SQLCommand.Parameters.Add("@pPrecio", SqlDbType.Decimal, 18).Value = DR_Fila("PRECIO")
                    SQLCommand.Parameters.Add("@pTasa", SqlDbType.Float).Value = DR_Fila("TASA")
                    SQLCommand.Parameters.Add("@pDuracion", SqlDbType.Decimal, 20).Value = DR_Fila("DURACION")

                    Dim ParametroIdPublicadorPrecio As New SqlClient.SqlParameter("@pIdPublicadorPrecio", SqlDbType.Int)
                    ParametroIdPublicadorPrecio.Direction = Data.ParameterDirection.Output
                    SQLCommand.Parameters.Add(ParametroIdPublicadorPrecio)

                End If

                'MODIFICA
                If Not (IsDBNull(DR_Fila("ID_PUBLICADOR_PRECIO"))) And lblnConValor Then
                    SQLCommand.Parameters.Add("@pAccion", SqlDbType.VarChar, 10).Value = "MODIFICAR"

                    SQLCommand.Parameters.Add("@pIdPublicadorPrecio", SqlDbType.Int).Value = DR_Fila("ID_PUBLICADOR_PRECIO")
                    SQLCommand.Parameters.Add("@pPrecio", SqlDbType.Decimal, 18).Value = DR_Fila("PRECIO")
                    SQLCommand.Parameters.Add("@pTasa", SqlDbType.Float).Value = DR_Fila("TASA")
                    SQLCommand.Parameters.Add("@pDuracion", SqlDbType.Decimal, 20).Value = DR_Fila("DURACION")
                End If

                'ELIMINA
                If Not (IsDBNull(DR_Fila("ID_PUBLICADOR_PRECIO"))) And Not (lblnConValor) Then
                    SQLCommand.Parameters.Add("@pAccion", SqlDbType.VarChar, 10).Value = "ELIMINAR"

                    SQLCommand.Parameters.Add("@pIdPublicadorPrecio", SqlDbType.Int).Value = DR_Fila("ID_PUBLICADOR_PRECIO")
                    
                End If

                Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                ParametroSal2.Direction = Data.ParameterDirection.Output
                SQLCommand.Parameters.Add(ParametroSal2)

                SQLCommand.ExecuteNonQuery()

                strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim

                If Trim(strRetorno) <> "OK" Then
                    Exit For
                End If
            Next
            If Trim(strRetorno) = "OK" Then
                MiTransaccion.Commit()
            Else
                MiTransaccion.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccion.Rollback()
            strRetorno = "Error al grabar Publicador de Precios." & vbCr & Ex.Message
        Finally
            Connect.Cerrar()
        End Try


    End Sub

    Public Sub PublicadorPreciosGuardar(ByVal lngIdPublicador As Long, _
                                        ByVal dtbPublicadorPrecio As DataTable, _
                                        ByRef strRetorno As String)
        Dim DR_Fila As DataRow = Nothing
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        'Abre la conexion
        Connect.Abrir()

        Dim MiTransaccion As SqlClient.SqlTransaction


        MiTransaccion = Connect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_PublicadorPrecio_Guardar", MiTransaccion.Connection, MiTransaccion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_PublicadorPrecio_Guardar"

            For Each DR_Fila In dtbPublicadorPrecio.Select("CHECK = 1")
                SQLCommand.Parameters.Clear()

                SQLCommand.Parameters.Add("@pIdPublicador", SqlDbType.Float, 4).Value = lngIdPublicador
                SQLCommand.Parameters.Add("@pIdInstrumento", SqlDbType.Float, 10).Value = DR_Fila("ID_INSTRUMENTO")
                SQLCommand.Parameters.Add("@pNemotecnico", SqlDbType.VarChar, 50).Value = DR_Fila("NEMOTECNICO")
                SQLCommand.Parameters.Add("@pFecha", SqlDbType.Char, 10).Value = DR_Fila("FECHA")
                SQLCommand.Parameters.Add("@pTasa", SqlDbType.Float).Value = DR_Fila("TASA")
                SQLCommand.Parameters.Add("@pPrecio", SqlDbType.Float, 18).Value = DR_Fila("PRECIO")
                SQLCommand.Parameters.Add("@pDuracion", SqlDbType.Decimal, 20).Value = DR_Fila("DURACION")
                SQLCommand.Parameters.Add("@pCodMoneda", SqlDbType.VarChar, 3).Value = DR_Fila("COD_MONEDA")

                Dim ParametroPublicadorPrecio As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                ParametroPublicadorPrecio.Direction = Data.ParameterDirection.Output
                SQLCommand.Parameters.Add(ParametroPublicadorPrecio)

                SQLCommand.ExecuteNonQuery()

                strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            Next

            If Trim(strRetorno) = "OK" Then
                MiTransaccion.Commit()
            Else
                MiTransaccion.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccion.Rollback()
            strRetorno = "Error al grabar Publicador de Precios." & vbCr & Ex.Message
        Finally
            Connect.Cerrar()
        End Try


    End Sub

    Public Function InformeRentabilidadValorCuota(ByVal strFechaConsulta As String, _
                                                  ByVal strColumnas As String, _
                                                  ByRef strRetorno As String) As DataSet

        Dim LDS_InformeRentabilidad As New DataSet
        Dim LDS_InformeRentabilidad_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_PublicadorPrecio_InformeRentabilidad"
        Lstr_NombreTabla = "InformeRentabilidad"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Int, 10).Value = glngNegocioOperacion
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Float, 6).Value = IIf(glngIdUsuario = 0, DBNull.Value, glngIdUsuario)
            SQLCommand.Parameters.Add("pFechaConsulta", SqlDbType.Char, 10).Value = strFechaConsulta

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_InformeRentabilidad, Lstr_NombreTabla)

            LDS_InformeRentabilidad_Aux = LDS_InformeRentabilidad

            If strColumnas.Trim = "" Then
                LDS_InformeRentabilidad_Aux = LDS_InformeRentabilidad
            Else
                LDS_InformeRentabilidad_Aux = LDS_InformeRentabilidad.Copy
                For Each Columna In LDS_InformeRentabilidad.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_InformeRentabilidad_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_InformeRentabilidad_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"

            LDS_InformeRentabilidad.Dispose()

            Return LDS_InformeRentabilidad_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function
    Public Sub PublicadorPreciosMantenedorDir(ByVal strAccion As String, _
                                              ByVal intIdPublicadorPrecio As Integer, _
                                              ByVal intIdPublicador As Integer, _
                                              ByVal intIdInstrumento As Integer, _
                                              ByVal strCodMoneda As String, _
                                              ByVal strFecha As String, _
                                              ByVal dblPrecio As Double, _
                                              ByVal dblTasa As Double, _
                                              ByVal dblDuracion As Double, _
                                              ByVal strNemotecnico As String, _
                                              ByVal dblIntAcumulado As Double, _
                                              ByRef strRetorno As String)
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim lstrProc As String = "Rcp_PublicadorPrecio_Mantencion"

        'Abre la conexion
        Connect.Abrir()

        Dim MiTransaccion As SqlClient.SqlTransaction

        MiTransaccion = Connect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand(lstrProc, MiTransaccion.Connection, MiTransaccion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = lstrProc

            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("@pAccion", SqlDbType.VarChar, 10).Value = strAccion

            Dim ParametroIdPublicadorPrecio As New SqlClient.SqlParameter("@pIdPublicadorPrecio", SqlDbType.Int)
            ParametroIdPublicadorPrecio.Direction = Data.ParameterDirection.InputOutput
            ParametroIdPublicadorPrecio.Value = IIf(intIdPublicadorPrecio = 0, DBNull.Value, intIdPublicadorPrecio)
            SQLCommand.Parameters.Add(ParametroIdPublicadorPrecio)

            SQLCommand.Parameters.Add("pIdPublicador", SqlDbType.Int).Value = intIdPublicador
            SQLCommand.Parameters.Add("pIdInstrumento", SqlDbType.Int).Value = IIf(intIdInstrumento = 0, DBNull.Value, intIdInstrumento)
            SQLCommand.Parameters.Add("pCodMoneda", SqlDbType.Char, 3).Value = strCodMoneda
            SQLCommand.Parameters.Add("pfecha", SqlDbType.Char, 10).Value = strFecha
            SQLCommand.Parameters.Add("pPrecio", SqlDbType.Decimal, 18).Value = dblPrecio
            SQLCommand.Parameters.Add("pTasa", SqlDbType.Float).Value = dblTasa
            'SQLCommand.Parameters.Add("pDuracion", SqlDbType.Decimal, 20).Value = dblDuracion
            SQLCommand.Parameters.Add("pNemotecnico", SqlDbType.VarChar, 50).Value = strNemotecnico
            SQLCommand.Parameters.Add("pIntAcumulado", SqlDbType.Decimal, 20).Value = dblIntAcumulado

            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()

            strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If Trim(strRetorno) = "OK" Then
                MiTransaccion.Commit()
            Else
                MiTransaccion.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccion.Rollback()
            strRetorno = "Error al grabar Publicador de Precios." & vbCr & Ex.Message
        Finally
            Connect.Cerrar()
        End Try
    End Sub
    Public Sub GrabaPreciosTANMIPublicador(ByVal strPrecios As String, _
                                            ByVal intIdPublicador As Integer, _
                                             ByVal strFecha As String, _
                                             ByRef Reg_Leidos As Integer, _
                                             ByRef Reg_Grabados As Integer, _
                                             ByRef strRetorno As String)
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim lstrProc As String = "RCP_CARGA_PRECIOS_TANMI"

        'Abre la conexion
        Connect.Abrir()

        Dim MiTransaccion As SqlClient.SqlTransaction

        Reg_Leidos = 0
        Reg_Grabados = 0

        MiTransaccion = Connect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand(lstrProc, MiTransaccion.Connection, MiTransaccion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = lstrProc

            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pPrecios", SqlDbType.Text).Value = strPrecios
            SQLCommand.Parameters.Add("pIdPublicador", SqlDbType.Int).Value = intIdPublicador
            SQLCommand.Parameters.Add("pfecha", SqlDbType.Char, 10).Value = strFecha

            Dim pReg_Leidos As New SqlClient.SqlParameter("pReg_Leidos", SqlDbType.Int, 10)
            pReg_Leidos.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pReg_Leidos).Value = Reg_Leidos

            Dim pReg_Grabados As New SqlClient.SqlParameter("pReg_Grabados", SqlDbType.Int, 10)
            pReg_Grabados.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pReg_Grabados).Value = Reg_Grabados

            Dim ParametroSal2 As New SqlClient.SqlParameter("pError", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()

            Reg_Leidos = SQLCommand.Parameters("pReg_Leidos").Value
            Reg_Grabados = SQLCommand.Parameters("pReg_Grabados").Value
            strRetorno = SQLCommand.Parameters("pError").Value.ToString.Trim


            If Trim(strRetorno) = "OK" Then
                MiTransaccion.Commit()
            Else
                MiTransaccion.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccion.Rollback()
            strRetorno = "Error al grabar Publicador de Precios." & vbCr & Ex.Message
        Finally
            Connect.Cerrar()
        End Try
    End Sub

End Class
