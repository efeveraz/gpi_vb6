﻿Public Class ClsClasificacion

    Public Function Clasificacion_Consultar(ByVal intIdClasificacion As Integer, _
                                            ByVal strColumnas As String, _
                                            ByRef strRetorno As String) As DataSet

        Dim LDS_Clasificacion As New DataSet
        Dim LDS_Clasificacion_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Clasificacion_Consultar"
        Lstr_NombreTabla = "Clasificacion"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdClasificacion", SqlDbType.Int, 10).Value = IIf(intIdClasificacion = 0, DBNull.Value, intIdClasificacion)

            '...Resultado
            Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultado.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Clasificacion, Lstr_NombreTabla)

            LDS_Clasificacion_Aux = LDS_Clasificacion

            If strColumnas.Trim = "" Then
                LDS_Clasificacion_Aux = LDS_Clasificacion
            Else
                LDS_Clasificacion_Aux = LDS_Clasificacion.Copy
                For Each Columna In LDS_Clasificacion.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Clasificacion_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Clasificacion_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            LDS_Clasificacion.Dispose()
            Return LDS_Clasificacion_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function Clasificacion_Buscar(ByVal intIdClasificacion As Integer, _
                                         ByVal strClasificacion As String, _
                                         ByVal strCodPlazo As String, _
                                         ByVal dblValorClasificacion As Double, _
                                         ByVal strEstClasificacion As String, _
                                         ByVal strColumnas As String, _
                                         ByRef strRetorno As String) As DataSet

        Dim LDS_Plazo As New DataSet
        Dim LDS_Plazo_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Clasificacion_Buscar"
        Lstr_NombreTabla = "Clasificacion"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdClasificacion", SqlDbType.Int).Value = IIf(intIdClasificacion = 0, DBNull.Value, intIdClasificacion)
            SQLCommand.Parameters.Add("pClasificacion", SqlDbType.VarChar, 20).Value = IIf(strClasificacion = "", DBNull.Value, strClasificacion)
            SQLCommand.Parameters.Add("pCodPlazo", SqlDbType.VarChar, 5).Value = IIf(strCodPlazo = "", DBNull.Value, strCodPlazo)
            SQLCommand.Parameters.Add("pValorClasificacion", SqlDbType.Float).Value = IIf(dblValorClasificacion = 0, DBNull.Value, dblValorClasificacion)
            SQLCommand.Parameters.Add("pEstClasificacion", SqlDbType.VarChar, 3).Value = IIf(strEstClasificacion = "", DBNull.Value, strEstClasificacion)



            '...Resultado
            Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultado.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Plazo, Lstr_NombreTabla)

            LDS_Plazo_Aux = LDS_Plazo

            If strColumnas.Trim = "" Then
                LDS_Plazo_Aux = LDS_Plazo
            Else
                LDS_Plazo_Aux = LDS_Plazo.Copy
                For Each Columna In LDS_Plazo.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Plazo_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Plazo_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            LDS_Plazo.Dispose()
            Return LDS_Plazo_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function Clasificacion_Mantencion(ByVal strAccion As String, _
                                             ByVal intIdClasificacion As Integer, _
                                             ByVal strClasificacion As String, _
                                             ByVal strCodPlazo As String, _
                                             ByVal dblValorClasificacion As Double, _
                                             ByVal strEstClasificacion As String) As String


        Dim strDescError As String = "OK"
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try


            SQLCommand = New SqlClient.SqlCommand("Rcp_Clasificacion_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Clasificacion_Mantencion"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 20).Value = UCase(strAccion.Trim)
            '...IdClasificacion
            Dim pSalIdEmpresaClasificadora As New SqlClient.SqlParameter("pIdClasificacion", SqlDbType.Int, 10)
            pSalIdEmpresaClasificadora.Direction = Data.ParameterDirection.InputOutput
            pSalIdEmpresaClasificadora.Value = IIf(intIdClasificacion = 0, DBNull.Value, intIdClasificacion)
            SQLCommand.Parameters.Add(pSalIdEmpresaClasificadora)

            SQLCommand.Parameters.Add("pClasificacion", SqlDbType.VarChar, 20).Value = strClasificacion
            SQLCommand.Parameters.Add("pCodPlazo", SqlDbType.VarChar, 5).Value = strCodPlazo
            SQLCommand.Parameters.Add("pValorClasificacion", SqlDbType.Float).Value = dblValorClasificacion
            SQLCommand.Parameters.Add("pEstClasificacion", SqlDbType.VarChar, 3).Value = strEstClasificacion



            '...Resultado
            Dim pSalResultado As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            pSalResultado.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pSalResultado)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                If Trim(strDescError) = "OK" Then
                    MiTransaccionSQL.Commit()
                    Return ("OK")
                End If
            Else
                GoTo Salir
            End If

Salir:
            If Trim(strDescError) = "OK" Then
                MiTransaccionSQL.Commit()
                Return ("OK")
            Else
                MiTransaccionSQL.Rollback()
                Return strDescError
            End If


        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en la mantención de plazos." & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            Clasificacion_Mantencion = strDescError
        End Try
    End Function

End Class
