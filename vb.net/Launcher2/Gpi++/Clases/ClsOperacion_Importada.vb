﻿Public Class ClsOperacion_Importada
    Public Function ValidarOperacionImportar(ByVal pIdContraparte As Long, ByRef pDataRow As DataRow) As String
        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion

        '...Abre la conexion
        SQLConnect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_Operacion_ValidarImportar", SQLConnect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Operacion_ValidarImportar"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdContraparte", SqlDbType.Float, 10).Value = pIdContraparte
            SQLCommand.Parameters.Add("pFechaProceso", SqlDbType.VarChar, 10).Value = pDataRow("Fecha Proceso")
            SQLCommand.Parameters.Add("pId_Oper_Tmp", SqlDbType.VarChar, 10).Value = pDataRow("Id_Oper_Tmp")
            SQLCommand.Parameters.Add("pNum_Cuenta", SqlDbType.VarChar, 15).Value = pDataRow("Número Cuenta")
            SQLCommand.Parameters.Add("pNemotecnico", SqlDbType.VarChar, 50).Value = pDataRow("Nemotécnico")
            SQLCommand.Parameters.Add("pTipoOper", SqlDbType.VarChar, 1).Value = pDataRow("Tipo Operación")
            SQLCommand.Parameters.Add("pCodMoneda", SqlDbType.VarChar, 15).Value = pDataRow("Moneda")
            SQLCommand.Parameters.Add("pCantidad", SqlDbType.Decimal, 20).Value = pDataRow("Cantidad")
            SQLCommand.Parameters.Add("pPrecio", SqlDbType.Decimal, 20).Value = pDataRow("Precio")
            SQLCommand.Parameters.Add("pMontoNeto", SqlDbType.Decimal, 20).Value = pDataRow("Monto Neto")
            SQLCommand.Parameters.Add("pComisionIn", SqlDbType.Decimal, 20).Value = pDataRow("Comisiones")
            SQLCommand.Parameters.Add("pFechaLiquidacion", SqlDbType.VarChar, 10).Value = pDataRow("Fecha Liquidación")

            Dim ParametroSal0 As New SqlClient.SqlParameter("pCodClase", SqlDbType.VarChar, 15)
            ParametroSal0.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal0)

            Dim ParametroSal1 As New SqlClient.SqlParameter("pCodSubClase", SqlDbType.VarChar, 10)
            ParametroSal1.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal1)

            Dim ParametroSal2 As New SqlClient.SqlParameter("pId_Cuenta", SqlDbType.Float, 10)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            Dim ParametroSal3 As New SqlClient.SqlParameter("pId_Instrumento", SqlDbType.Float, 10)
            ParametroSal3.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal3)

            Dim ParametroSal4 As New SqlClient.SqlParameter("pDscTipoOper", SqlDbType.VarChar, 30)
            ParametroSal4.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal4)

            Dim ParametroSal5 As New SqlClient.SqlParameter("pComision", SqlDbType.Decimal, 20)
            ParametroSal5.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal5)

            Dim ParametroSal6 As New SqlClient.SqlParameter("pGastos", SqlDbType.Decimal, 20)
            ParametroSal6.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal6)

            Dim ParametroSal7 As New SqlClient.SqlParameter("pMonto", SqlDbType.Decimal, 20)
            ParametroSal7.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal7)

            Dim ParametroSal8 As New SqlClient.SqlParameter("pError", SqlDbType.VarChar, 200)
            ParametroSal8.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal8)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pError").Value.ToString.Trim
            pDataRow("Estado Proceso") = SQLCommand.Parameters("pError").Value.ToString.Trim
            pDataRow("CodClase") = SQLCommand.Parameters("pCodClase").Value
            pDataRow("CodSubClase") = SQLCommand.Parameters("pCodSubClase").Value
            pDataRow("Id_Cuenta") = SQLCommand.Parameters("pId_Cuenta").Value
            pDataRow("Id_Instrumento") = SQLCommand.Parameters("pId_Instrumento").Value
            pDataRow("DscTipoOper") = pDataRow("Tipo Operación")
            pDataRow("Tipo Operación") = SQLCommand.Parameters("pDscTipoOper").Value
            pDataRow("Comisiones") = SQLCommand.Parameters("pComision").Value
            pDataRow("Gastos") = SQLCommand.Parameters("pGastos").Value
            pDataRow("Monto") = SQLCommand.Parameters("pMonto").Value

            If Trim(strDescError) = "OK" Then
                Return ("OK")
            Else
                Return strDescError
            End If

        Catch Ex As Exception
            strDescError = "Error al obtener la información de las operaciones a importar." & vbCr & Ex.Message
            gFun_ResultadoMsg("1|" & strDescError, "ValidarOperacionImportar")
        Finally
            SQLConnect.Cerrar()
            ValidarOperacionImportar = strDescError
        End Try

    End Function
    Public Function GuardarOperacionImportada(ByVal dtbOperacionEncabezado As DataTable, _
                                              ByVal dtbOperacionDetalle As DataTable, _
                                              Optional ByRef dblIdGenerado As Double = 0, _
                                              Optional ByVal intIdCuentadestino As Integer = -1, _
                                              Optional ByVal strTipoOperacionDestino As String = "") As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction
        Dim lstrDetalle1 As String = ""
        Dim lstrDetalle2 As String = ""
        Dim lstrDetalle3 As String = ""
        Dim lstrComisiones As String = ""
        Dim lstrMediosCobroPago As String = ""
        Dim llngIdOperacion As Integer = 0
        Dim lstrColumnasDetalle As String = "ID_INSTRUMENTO © " & _
                                            "CANTIDAD © " & _
                                            "PRECIO © " & _
                                            "PRECIO_GESTION © " & _
                                            "PLAZO © " & _
                                            "TASA © " & _
                                            "BASE © " & _
                                            "VALOR_DIVISA © " & _
                                            "MONTO_PAGO © " & _
                                            "MONTO © " & _
                                            "MONTO_TOTAL © " & _
                                            "MONTO_BRUTO © " & _
                                            "FECHA_LIQUIDACION © " & _
                                            "FECHA_VENCIMIENTO © " & _
                                            "ID_CUSTODIO © " & _
                                            "COD_EMISOR © " & _
                                            "ID_NORMATIVO © " & _
                                            "NUMERO_DOCUMENTO © " & _
                                            "FLG_TIPO_DEPOSITO © " & _
                                            "FECHA_EMISION © " & _
                                            "ID_CARTERA © " & _
                                            "ING_PORC_VALOR_PAR © " & _
                                            "TIPO_CUSTODIA © " & _
                                            "COD_TIPO_REAJUSTE © " & _
                                            "MONTO_MONEDA_NEMO © " & _
                                            "CANTIDAD_INICIAL"

        Dim lstrResultadoCadena As String = ""

        ' GENERA CADENA(S) DE DETALLE DE OPERACIONES
        lstrResultadoCadena = GeneraCadena(dtbOperacionDetalle, lstrColumnasDetalle, "©", lstrDetalle1, lstrDetalle2, lstrDetalle3)
        If lstrResultadoCadena <> "OK" Then
            Return lstrResultadoCadena
        End If

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion
        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_Operacion_Importada_Guardar", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Operacion_Importada_Guardar"
            SQLCommand.Parameters.Clear()

            Dim dtrOperacionEncabezado As DataRow = dtbOperacionEncabezado.Rows(0)

            SQLCommand.Parameters.Add("OperacionExterna", SqlDbType.VarChar, 50).Value = dtrOperacionEncabezado("Operacion_Externa")
            SQLCommand.Parameters.Add("pCodMoneda", SqlDbType.VarChar, 3).Value = dtrOperacionEncabezado("COD_MONEDA")
            SQLCommand.Parameters.Add("pCodTipoOperacion", SqlDbType.VarChar, 10).Value = dtrOperacionEncabezado("COD_TIPO_OPERACION").trim
            SQLCommand.Parameters.Add("pIdContraparte", SqlDbType.Int, 18).Value = IIf((IsDBNull(dtrOperacionEncabezado("ID_CONTRAPARTE")) OrElse dtrOperacionEncabezado("ID_CONTRAPARTE") = 0), DBNull.Value, dtrOperacionEncabezado("ID_CONTRAPARTE"))
            SQLCommand.Parameters.Add("pFechaProceso", SqlDbType.Char, 10).Value = dtrOperacionEncabezado("FECHA_PROCESO")
            SQLCommand.Parameters.Add("pFechaOperacion", SqlDbType.Char, 10).Value = dtrOperacionEncabezado("FECHA_OPERACION")
            SQLCommand.Parameters.Add("pFechaLiquidacion", SqlDbType.Char, 10).Value = dtrOperacionEncabezado("FECHA_LIQUIDACION")
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Int, 18).Value = dtrOperacionEncabezado("ID_CUENTA")
            SQLCommand.Parameters.Add("pPorc_Comision", SqlDbType.Decimal, 24).Value = dtrOperacionEncabezado("PORC_COMISION")
            SQLCommand.Parameters.Add("pComision", SqlDbType.Decimal, 24).Value = dtrOperacionEncabezado("COMISION")
            SQLCommand.Parameters.Add("pDerechos", SqlDbType.Decimal, 24).Value = dtrOperacionEncabezado("DERECHOS")
            SQLCommand.Parameters.Add("pGastos", SqlDbType.Decimal, 24).Value = dtrOperacionEncabezado("PORC_COMISION")
            SQLCommand.Parameters.Add("pIva", SqlDbType.Decimal, 24).Value = dtrOperacionEncabezado("IVA")
            SQLCommand.Parameters.Add("sKey", SqlDbType.VarChar, 25).Value = dtrOperacionEncabezado("SKEY")
            SQLCommand.Parameters.Add("pLineas", SqlDbType.Int, 5).Value = dtbOperacionDetalle.Rows.Count
            SQLCommand.Parameters.Add("pDetalle1", SqlDbType.VarChar, 8000).Value = lstrDetalle1
            SQLCommand.Parameters.Add("pDetalle2", SqlDbType.VarChar, 8000).Value = IIf(lstrDetalle2 = "", DBNull.Value, lstrDetalle2)
            SQLCommand.Parameters.Add("pDetalle3", SqlDbType.VarChar, 8000).Value = IIf(lstrDetalle3 = "", DBNull.Value, lstrDetalle3)

            '...Id Solicitud de Inversion
            Dim ParSalOperacion As New SqlClient.SqlParameter("pIdOperacion", SqlDbType.Int, 10)
            ParSalOperacion.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParSalOperacion)

            '...Resultado
            Dim ParametroSal As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If Trim(strDescError) = "OK" Then
                dblIdGenerado = SQLCommand.Parameters("pIdOperacion").Value

                'SI intIdCuentadestino ES <> -1 y strTipoOperacionDestino <> ""
                'SE HACE EL MOV. DE DESTINO CON LA CUENTA DE DESTINO.

                llngIdOperacion = SQLCommand.Parameters("pIdOperacion").Value
                MiTransaccionSQL.Commit()

            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error al Grabar Operación Importada" & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
        End Try

        Return strDescError
    End Function
    Public Function OperacionImportadaBuscarImportar(ByVal strKey As String, _
                                                     ByVal lngIdContraparte As Long, _
                                                     ByVal strClaseInstrumento As String, _
                                                     ByVal strSubClaseInstrumento As String, _
                                                     ByRef strRetorno As String) As DataSet

        Dim LDS_OpeImportada As New DataSet
        Dim LDS_OpeImportada_Aux As New DataSet
        Dim Lstr_NombreTabla As String = ""
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Operacion_Importada_BuscarImportar"
        Lstr_NombreTabla = "OPERACIOIMPORTADAIMPORT"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("psKey", SqlDbType.VarChar, 25).Value = IIf(strKey.Trim = "", DBNull.Value, strKey.Trim)
            SQLCommand.Parameters.Add("pIdContraparte", SqlDbType.Float, 10).Value = IIf(lngIdContraparte = 0, DBNull.Value, lngIdContraparte)
            SQLCommand.Parameters.Add("pCodClaseInstrumento", SqlDbType.VarChar, 10).Value = IIf(strClaseInstrumento = "", DBNull.Value, strClaseInstrumento)
            SQLCommand.Parameters.Add("pCodSubClaseInstrumento", SqlDbType.VarChar, 15).Value = IIf(strSubClaseInstrumento = "", DBNull.Value, strSubClaseInstrumento)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_OpeImportada, Lstr_NombreTabla)

            LDS_OpeImportada_Aux = LDS_OpeImportada

            strRetorno = "OK"
            LDS_OpeImportada.Dispose()
            Return LDS_OpeImportada_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function
    Public Function OperacionBuscarImportadas(ByVal strFechaConsulta As String, _
                                              ByVal lngIdContraparte As Long, _
                                              ByVal lngIdEntidadGPI As Long, _
                                              ByVal strKey As String, _
                                              ByRef strRetorno As String) As DataSet

        Dim LDS_OpeImportada As New DataSet
        Dim LDS_OpeImportada_Aux As New DataSet
        Dim Lstr_NombreTabla As String = ""
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Operacion_BuscarImportadas"
        Lstr_NombreTabla = "OPERACIONESIMPORTADAS"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pFechaOperacion", SqlDbType.VarChar, 10).Value = strFechaConsulta.Trim
            SQLCommand.Parameters.Add("pIdContraparte", SqlDbType.Float, 10).Value = IIf(lngIdContraparte = 0, DBNull.Value, lngIdContraparte)
            SQLCommand.Parameters.Add("pIdEntidadGPI", SqlDbType.Float, 10).Value = IIf(lngIdEntidadGPI = 0, DBNull.Value, lngIdEntidadGPI)
            SQLCommand.Parameters.Add("psKey", SqlDbType.VarChar, 25).Value = IIf(strKey.Trim = "", DBNull.Value, strKey.Trim)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_OpeImportada, Lstr_NombreTabla)

            LDS_OpeImportada_Aux = LDS_OpeImportada

            strRetorno = "OK"
            LDS_OpeImportada.Dispose()
            Return LDS_OpeImportada_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function
    Public Function ValidarPreImportadorGeneral(ByVal pIdContraparte As Long, ByVal pId_M As Long, ByRef pDataRow As DataRow, ByRef pDataSet As DataSet, ByVal pDataSetFormat As DataSet, ByRef pNumCuenta As String, ByRef pId_Error As Integer) As String
        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion

        '...Abre la conexion
        SQLConnect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_ValidarPreImportadorGeneral", SQLConnect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_ValidarPreImportadorGeneral"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdContraparte", SqlDbType.Float, 10).Value = pIdContraparte
            SQLCommand.Parameters.Add("pId_M", SqlDbType.Float, 10).Value = pId_M
            SQLCommand.Parameters.Add("pFecha_Operacion", SqlDbType.VarChar, 10).Value = pDataRow("E.FECHA_OPERACION")
            SQLCommand.Parameters.Add("pFecha_Liquidacion", SqlDbType.VarChar, 10).Value = pDataRow("E.FECHA_LIQUIDACION")
            SQLCommand.Parameters.Add("pOperacion_Externa", SqlDbType.VarChar, 100).Value = pDataRow("E.OPERACION_EXTERNA")
            SQLCommand.Parameters.Add("pCuenta", SqlDbType.VarChar, 100).Value = getValorCompuestoAlias(pDataRow, "E.ID_CUENTA", pDataSetFormat)
            SQLCommand.Parameters.Add("pNemotecnico_E", SqlDbType.VarChar, 100).Value = getValorCompuestoAlias(pDataRow, "I.NEMOTECNICO", pDataSetFormat)
            SQLCommand.Parameters.Add("pTipoOper", SqlDbType.VarChar, 100).Value = getValorCompuestoAlias(pDataRow, "E.COD_TIPO_OPERACION", pDataSetFormat)
            SQLCommand.Parameters.Add("pCodMoneda", SqlDbType.VarChar, 100).Value = getValorCompuestoAlias(pDataRow, "E.COD_MONEDA", pDataSetFormat)
            SQLCommand.Parameters.Add("pCantidad", SqlDbType.Decimal, 24).Value = pDataRow("D.CANTIDAD")
            SQLCommand.Parameters.Add("pPrecio", SqlDbType.Decimal, 24).Value = pDataRow("D.PRECIO")
            SQLCommand.Parameters.Add("pTasa", SqlDbType.Decimal, 24).Value = pDataRow("D.TASA")
            SQLCommand.Parameters.Add("pBase", SqlDbType.Decimal, 24).Value = pDataRow("D.BASE")
            SQLCommand.Parameters.Add("pMonto", SqlDbType.Decimal, 24).Value = pDataRow("D.MONTO")
            SQLCommand.Parameters.Add("pMontoPago", SqlDbType.Decimal, 24).Value = pDataRow("D.MONTO_PAGO")
            SQLCommand.Parameters.Add("pComision", SqlDbType.Decimal, 24).Value = pDataRow("E.COMISION")
            SQLCommand.Parameters.Add("pDerechos", SqlDbType.Decimal, 24).Value = pDataRow("E.DERECHOS")
            SQLCommand.Parameters.Add("pGastos", SqlDbType.Decimal, 24).Value = pDataRow("E.GASTOS")
            SQLCommand.Parameters.Add("pIva", SqlDbType.Decimal, 24).Value = pDataRow("E.IVA")
            SQLCommand.Parameters.Add("pCodEmisor_E", SqlDbType.VarChar, 100).Value = getValorCompuestoAlias(pDataRow, "D.COD_EMISOR", pDataSetFormat)
            SQLCommand.Parameters.Add("pId_Cartera_E", SqlDbType.VarChar, 100).Value = getValorCompuestoAlias(pDataRow, "D.ID_CARTERA", pDataSetFormat)
            SQLCommand.Parameters.Add("pId_Normativo_E", SqlDbType.VarChar, 100).Value = getValorCompuestoAlias(pDataRow, "D.ID_NORMATIVO", pDataSetFormat)
            SQLCommand.Parameters.Add("pCodClase_E", SqlDbType.VarChar, 100).Value = getValorCompuestoAlias(pDataRow, "I.COD_CLASE_INSTRUMENTO", pDataSetFormat)
            SQLCommand.Parameters.Add("pCustodio_E", SqlDbType.VarChar, 100).Value = getValorCompuestoAlias(pDataRow, "D.ID_CUSTODIO", pDataSetFormat)

            Dim ParametroSal As New SqlClient.SqlParameter("pCodTipoOper", SqlDbType.VarChar, 10)
            ParametroSal.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal)

            Dim ParametroSal0 As New SqlClient.SqlParameter("pCodClase", SqlDbType.VarChar, 15)
            ParametroSal0.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal0)

            Dim ParametroSal1 As New SqlClient.SqlParameter("pCodSubClase", SqlDbType.VarChar, 10)
            ParametroSal1.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal1)

            Dim ParametroSal2 As New SqlClient.SqlParameter("pId_Cuenta", SqlDbType.Float, 10)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            Dim ParametroSal3 As New SqlClient.SqlParameter("pId_Instrumento", SqlDbType.Float, 10)
            ParametroSal3.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal3)

            Dim ParametroSal4 As New SqlClient.SqlParameter("pCod_Moneda", SqlDbType.VarChar, 3)
            ParametroSal4.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal4)

            Dim ParametroSal5 As New SqlClient.SqlParameter("pCod_Emisor_S", SqlDbType.VarChar, 10)
            ParametroSal5.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal5)

            Dim ParametroSal6 As New SqlClient.SqlParameter("pId_Cartera_S", SqlDbType.Float, 18)
            ParametroSal6.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal6)

            Dim ParametroSal7 As New SqlClient.SqlParameter("pId_Normativo_S", SqlDbType.Float, 10)
            ParametroSal7.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal7)

            Dim ParametroSal8 As New SqlClient.SqlParameter("pNemotecnico_S", SqlDbType.VarChar, 50)
            ParametroSal8.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal8)

            Dim ParametroSal80 As New SqlClient.SqlParameter("pId_Custodio_S", SqlDbType.Float, 10)
            ParametroSal80.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal80)

            'Se agrega el número de la cuenta para mostrar en grilla
            Dim ParametroSal9 As New SqlClient.SqlParameter("pNum_Cuenta", SqlDbType.VarChar, 100)
            ParametroSal9.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal9)

            Dim ParametroSal10 As New SqlClient.SqlParameter("pError", SqlDbType.VarChar, 200)
            ParametroSal10.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal10)

            Dim ParametroSal11 As New SqlClient.SqlParameter("pId_Error", SqlDbType.Float, 10)
            ParametroSal11.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal11)

            Dim ParametroSal12 As New SqlClient.SqlParameter("pId_Comision", SqlDbType.Float, 10)
            ParametroSal12.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal12)

            Dim ParametroSal13 As New SqlClient.SqlParameter("pFecha_Venc", SqlDbType.DateTime, 10)
            ParametroSal13.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal13)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pError").Value.ToString.Trim
            pId_Error = SQLCommand.Parameters("pId_Error").Value
            pDataRow("Estado Proceso") = IIf(IsDBNull(SQLCommand.Parameters("pError").Value.ToString.Trim), "", SQLCommand.Parameters("pError").Value.ToString.Trim)
            pDataRow("I.COD_CLASE_INSTRUMENTO") = IIf(IsDBNull(SQLCommand.Parameters("pCodClase").Value), "", SQLCommand.Parameters("pCodClase").Value)
            pDataRow("I.COD_SUB_CLASE_INSTRUMENTO") = IIf(IsDBNull(SQLCommand.Parameters("pCodSubClase").Value), "", SQLCommand.Parameters("pCodSubClase").Value)
            pDataRow("E.Id_Cuenta") = IIf(IsDBNull(SQLCommand.Parameters("pId_Cuenta").Value), 0, SQLCommand.Parameters("pId_Cuenta").Value)
            pDataRow("D.Id_Instrumento") = IIf(IsDBNull(SQLCommand.Parameters("pId_Instrumento").Value), 0, SQLCommand.Parameters("pId_Instrumento").Value)
            pDataRow("E.COD_TIPO_OPERACION") = IIf(IsDBNull(SQLCommand.Parameters("pCodTipoOper").Value), "", SQLCommand.Parameters("pCodTipoOper").Value)
            pDataRow("E.COD_MONEDA") = IIf(IsDBNull(SQLCommand.Parameters("pCod_Moneda").Value), "", SQLCommand.Parameters("pCod_Moneda").Value)
            pDataRow("D.COD_EMISOR") = IIf(IsDBNull(SQLCommand.Parameters("pCod_Emisor_S").Value), "", SQLCommand.Parameters("pCod_Emisor_S").Value)
            pDataRow("D.ID_CARTERA") = IIf(IsDBNull(SQLCommand.Parameters("pId_Cartera_S").Value), 0, SQLCommand.Parameters("pId_Cartera_S").Value)
            pDataRow("D.ID_NORMATIVO") = IIf(IsDBNull(SQLCommand.Parameters("pId_Normativo_S").Value), Nothing, SQLCommand.Parameters("pId_Normativo_S").Value)
            pDataRow("I.NEMOTECNICO") = IIf(IsDBNull(SQLCommand.Parameters("pNemotecnico_S").Value), "", SQLCommand.Parameters("pNemotecnico_S").Value)
            pDataRow("D.ID_CUSTODIO") = IIf(IsDBNull(SQLCommand.Parameters("pId_Custodio_S").Value), 0, SQLCommand.Parameters("pId_Custodio_S").Value)
            pDataRow("D.ID_COMISION") = IIf(IsDBNull(SQLCommand.Parameters("pId_Comision").Value), DBNull.Value, SQLCommand.Parameters("pId_Comision").Value)
            pDataRow("D.FECHA_VENCIMIENTO") = IIf(IsDBNull(SQLCommand.Parameters("pFecha_Venc").Value), DBNull.Value, SQLCommand.Parameters("pFecha_Venc").Value)
            pNumCuenta = IIf(IsDBNull(SQLCommand.Parameters("pNum_Cuenta").Value), 0, SQLCommand.Parameters("pNum_Cuenta").Value)
            If Trim(strDescError) = "OK" Then
                Return ("OK")
            Else
                Return strDescError
            End If

        Catch Ex As Exception
            strDescError = "Error: " & Ex.Message
            pId_Error = 2
            'gFun_ResultadoMsg("1|" & strDescError, "ValidarOperacionImportar")
        Finally
            SQLConnect.Cerrar()
            ValidarPreImportadorGeneral = strDescError
        End Try

    End Function
    Private Function getValorCompuestoAlias(ByVal pDr As DataRow, ByVal pNombreCampo As String, ByVal pDataSetFormat As DataSet) As String
        Dim lstrValor As String = ""
        Dim lstrTabla_Entidad_GPI As String = "0"

        For Each drFormat As DataRow In pDataSetFormat.Tables(0).Select("CAMPO_INTER = '" & pNombreCampo & "'")
            lstrTabla_Entidad_GPI = IIf("0" & drFormat("Tabla_Entidad_GPI") = "0", "0", drFormat("Tabla_Entidad_GPI"))
        Next

        If lstrTabla_Entidad_GPI <> "0" Then
            lstrValor = lstrTabla_Entidad_GPI & "|" & pDr(pNombreCampo & "_IN")
        Else
            lstrValor = IIf(IsDBNull(pDr(pNombreCampo)), "", pDr(pNombreCampo))
        End If
        getValorCompuestoAlias = lstrValor
    End Function
    Private Sub CreaCampoRecodSet(ByRef pDataSet As DataSet, ByVal pNombre As String, ByVal pTipoDato As Est_TipoDato)
        Select Case pTipoDato
            Case Est_TipoDato.enuNumerico
                '+ Numérico
                pDataSet.Tables(0).Columns.Add(pNombre, GetType(Double))

            Case Est_TipoDato.enuString
                '+ String
                pDataSet.Tables(0).Columns.Add(pNombre, GetType(String))

            Case Est_TipoDato.enuDate
                '+ Date
                pDataSet.Tables(0).Columns.Add(pNombre, GetType(Date))

            Case Else
                '+ String
                pDataSet.Tables(0).Columns.Add(pNombre, GetType(String))
        End Select

    End Sub
    Public Function EliminarOperacionImportada_OE(Optional ByVal pstrOperacionExterna As String = "", _
                                                  Optional ByVal pstrFechaProceso As String = "", _
                                                  Optional ByVal plngIdContraparte As Long = 0) As String
        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion
        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_Operacion_Importada_Eliminar_OE", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Operacion_Importada_Eliminar_OE"
            SQLCommand.Parameters.Clear()

            If pstrOperacionExterna <> "" Then
                SQLCommand.Parameters.Add("pOperacion_Externa", SqlDbType.VarChar, 50).Value = pstrOperacionExterna
            End If
            If pstrFechaProceso <> "" Then
                SQLCommand.Parameters.Add("pFecha_Proceso", SqlDbType.VarChar, 50).Value = pstrFechaProceso
            End If
            If plngIdContraparte <> 0 Then
                SQLCommand.Parameters.Add("pId_Contraparte", SqlDbType.Decimal, 50).Value = plngIdContraparte
            End If
            '...Resultado
            Dim ParametroSal As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If Trim(strDescError) = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error al Eliminar Operación Importada" & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
        End Try

        Return strDescError
    End Function
    Public Function OperacionImportadaBuscar(ByVal strFechaConsulta As String, _
                                             ByVal strClaseInstrumento As String, _
                                             ByVal strSubClaseInstrumento As String, _
                                             ByVal lngIdContraparte As Long, _
                                             ByRef strRetorno As String) As DataSet

        Dim LDS_OpeImportada As New DataSet
        Dim LDS_OpeImportada_Aux As New DataSet
        Dim Lstr_NombreTabla As String = ""
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Operacion_Importada_Buscar"
        Lstr_NombreTabla = "OPERACIOIMPORTADA"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pFechaOperacion", SqlDbType.Char, 10).Value = strFechaConsulta
            SQLCommand.Parameters.Add("pCodClaseInstrumento", SqlDbType.VarChar, 15).Value = IIf(strClaseInstrumento = "", DBNull.Value, strClaseInstrumento)
            SQLCommand.Parameters.Add("pCodSubClaseInstrumento", SqlDbType.VarChar, 15).Value = IIf(strSubClaseInstrumento = "", DBNull.Value, strSubClaseInstrumento)
            SQLCommand.Parameters.Add("pIdContraparte", SqlDbType.Float, 10).Value = IIf(lngIdContraparte = 0, DBNull.Value, lngIdContraparte)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_OpeImportada, Lstr_NombreTabla)

            LDS_OpeImportada_Aux = LDS_OpeImportada

            strRetorno = "OK"
            LDS_OpeImportada.Dispose()
            Return LDS_OpeImportada_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function
    Public Function ValidadorMacheo_Buscar(ByVal strFechaOper As String, _
                                           ByVal strClaseInstrumento As String, _
                                           ByVal strSubClaseInstrumento As String, _
                                           ByVal lngIdContraparte As Long, _
                                           ByVal lngIdCuenta As Long, _
                                           ByRef strRetorno As String) As DataSet

        Dim LDS_OpeImportada As New DataSet
        Dim LDS_OpeImportada_Aux As New DataSet
        Dim Lstr_NombreTabla As String = ""
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_ValidadorMapeo_Buscar"
        Lstr_NombreTabla = "ValidadorMacheo"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pFechaOper", SqlDbType.Char, 10).Value = strFechaOper
            SQLCommand.Parameters.Add("pCodClase", SqlDbType.VarChar, 15).Value = IIf(strClaseInstrumento = "", DBNull.Value, strClaseInstrumento)
            SQLCommand.Parameters.Add("pCodSubClase", SqlDbType.VarChar, 15).Value = IIf(strSubClaseInstrumento = "", DBNull.Value, strSubClaseInstrumento)
            SQLCommand.Parameters.Add("pIdContraparte", SqlDbType.Float, 10).Value = IIf(lngIdContraparte = 0, DBNull.Value, lngIdContraparte)
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = IIf(lngIdCuenta = 0, DBNull.Value, lngIdCuenta)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_OpeImportada, Lstr_NombreTabla)

            LDS_OpeImportada_Aux = LDS_OpeImportada

            strRetorno = "OK"
            LDS_OpeImportada.Dispose()
            Return LDS_OpeImportada_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function
End Class
