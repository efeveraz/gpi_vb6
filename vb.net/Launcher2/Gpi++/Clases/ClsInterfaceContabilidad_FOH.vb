﻿Imports Microsoft.VisualBasic
Imports System.Data

Imports Oracle.DataAccess.Client
Imports Oracle.DataAccess.Types

Public Class ClsInterfaceContabilidad_FOH
  Public Function InformarEvento(ByVal intIdCentralizacion As Integer) As String
    Dim strDescError As String = ""
    Dim dsTabla As New DataSet

    Try
      dsTabla = New DataSet
      strDescError = LeerEventos(intIdCentralizacion:=intIdCentralizacion, dsTabla:=dsTabla)
    Catch ex As Exception
      strDescError = "Problema en el leer la centralización '" & intIdCentralizacion & "'." & vbCr & ex.Message
    End Try

    If strDescError = "OK" Then
      If dsTabla.Tables("TABLA").Rows.Count > 0 Then
        'Select Case dsTabla.Tables("TABLA").Rows(0)("TIPO_MVTO").ToString.Trim.ToUpper
        '    Case "E", "A"
        Try
          strDescError = GuardarGL(dtTabla:=dsTabla.Tables("TABLA"))
        Catch ex As Exception
          strDescError = "Problema en el envio del evento código '" & intIdCentralizacion & "'." & vbCr & ex.Message
        End Try
        '    Case "I", "C"
        '        Try
        '            strDescError = GuardarAP(dtTabla:=dsTabla.Tables("TABLA"))
        '        Catch ex As Exception
        '            strDescError = "Problema en el guardar la operación '" & intIdMovSistema & "'." & vbCr & ex.Message
        '        End Try
        '    Case Else
        '        strDescError = "El código tipo de movimiento """ & dsTabla.Tables("TABLA").Rows(0)("TIPO_MVTO").ToString.Trim.ToUpper & """ no es valido."
        'End Select
      End If
    End If

    Return strDescError
  End Function

  Private Function LeerEventos(ByVal intIdCentralizacion As Integer _
                             , ByRef dsTabla As DataSet) As String
    '---------------------------------------------------------
    Dim SQLCommand As New SqlClient.SqlCommand
    Dim SQLConnect As New Cls_Conexion
    Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
    '---------------------------------------------------------
    Dim lstrProcedimiento As String = ""
    Dim strDescError As String = ""
    '---------------------------------------------------------

    '...Abre la conexion
    SQLConnect.Abrir()
    lstrProcedimiento = "FOH_CONTABILIDAD_LEER_EVENTO"

    Try
      SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, SQLConnect.Conexion)
      SQLCommand.CommandType = CommandType.StoredProcedure
      SQLCommand.CommandText = lstrProcedimiento
      SQLCommand.Parameters.Clear()

      '( @PID_OPERACION NUMERIC
      ', @PRESULTADO VARCHAR(200) OUTPUT
      SQLCommand.Parameters.Add("PID_CENTRALIZACION", SqlDbType.Float, 3).Value = intIdCentralizacion
      SQLCommand.Parameters.Add("PID_USUARIO", SqlDbType.Float, 3).Value = glngIdUsuario

      '...Resultado
      Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
      ParametroSal2.Direction = Data.ParameterDirection.Output
      SQLCommand.Parameters.Add(ParametroSal2)

      SQLCommand.ExecuteNonQuery()

      strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

      If strDescError = "OK" Then
        SQLDataAdapter.SelectCommand = SQLCommand
        SQLDataAdapter.Fill(dsTabla, "TABLA")
      End If

    Catch Ex As Exception
      strDescError = "Problema en el leer la centralización '" & intIdCentralizacion & "'." & vbCr & Ex.Message
    Finally
      SQLConnect.Cerrar()
    End Try

    Return strDescError
  End Function

  Private Function GuardarGL(ByRef dtTabla As DataTable) As String
    Dim lcConexion As New ClsConexionOracle
    Dim lcOracleCommand As New Oracle.DataAccess.Client.OracleCommand
    Dim lcMiTransaccion As OracleTransaction = Nothing
    '---------------------------------------------
    Dim ldrRow As DataRow
    '---------------------------------------------
    Dim strDescError As String = ""

    Try
      lcConexion.Abrir()

      lcMiTransaccion = lcConexion.Transaccion

      For Each ldrRow In dtTabla.Rows
        lcOracleCommand = New OracleCommand("Foh_tesorieria.Guardar_gl", lcConexion.Conexion)
        lcOracleCommand.CommandType = CommandType.StoredProcedure

        lcOracleCommand.Parameters.Clear()

        lcOracleCommand.Parameters.Add("PSTATUS", OracleDbType.Varchar2, Left(ldrRow("STATUS").ToString, 5), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PDATE_CREATED", OracleDbType.Char, ldrRow("DATE_CREATED").ToString, ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PACTUAL_FLAG", OracleDbType.Varchar2, Left(ldrRow("ACTUAL_FLAG").ToString, 5), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PUSER_CURRENCY_CONVERSION_TYPE", OracleDbType.Varchar2, Left(ldrRow("USER_CURRENCY_CONVERSION_TYPE").ToString, 30), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PUSER_JE_SOURCE_NAME", OracleDbType.Varchar2, Left(ldrRow("USER_JE_SOURCE_NAME").ToString, 25), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PUSER_JE_CATEGORY_NAME", OracleDbType.Varchar2, Left(ldrRow("USER_JE_CATEGORY_NAME").ToString, 25), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PSET_OF_BOOKS_ID", OracleDbType.Double, ldrRow("SET_OF_BOOKS_ID"), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PCURRENCY_CODE", OracleDbType.Varchar2, Left(ldrRow("CURRENCY_CODE").ToString, 15), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PCURRENCY_CONVERSION_DATE", OracleDbType.Char, ldrRow("CURRENCY_CONVERSION_DATE").ToString, ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PCURRENCY_CONVERSION_TYPE", OracleDbType.Varchar2, Left(ldrRow("CURRENCY_CONVERSION_TYPE").ToString, 15), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PCURRENCY_CONVERSION_RATE", OracleDbType.Double, ldrRow("CURRENCY_CONVERSION_RATE"), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PACCOUNTING_DATE", OracleDbType.Char, ldrRow("ACCOUNTING_DATE").ToString, ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PREFERENCE1", OracleDbType.Varchar2, Left(ldrRow("REFERENCE1").ToString, 10), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PREFERENCE2", OracleDbType.Varchar2, Left(ldrRow("REFERENCE2").ToString, 24), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PREFERENCE4", OracleDbType.Varchar2, Left(ldrRow("REFERENCE4").ToString, 10), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PREFERENCE5", OracleDbType.Varchar2, Left(ldrRow("REFERENCE5").ToString, 24), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PREFERENCE10", OracleDbType.Varchar2, Left(ldrRow("REFERENCE10").ToString, 24), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PSEGMENT1_CTA_ACTIVO", OracleDbType.Varchar2, Left(ldrRow("SEGMENT1_CTA_ACTIVO").ToString, 3), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PSEGMENT2_CTA_ACTIVO", OracleDbType.Varchar2, Left(ldrRow("SEGMENT2_CTA_ACTIVO").ToString, 3), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PSEGMENT3_CTA_ACTIVO", OracleDbType.Varchar2, Left(ldrRow("SEGMENT3_CTA_ACTIVO").ToString, 4), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PSEGMENT4_CTA_ACTIVO", OracleDbType.Varchar2, Left(ldrRow("SEGMENT4_CTA_ACTIVO").ToString, 3), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PSEGMENT5_CTA_ACTIVO", OracleDbType.Varchar2, Left(ldrRow("SEGMENT5_CTA_ACTIVO").ToString, 3), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PSEGMENT6_CTA_ACTIVO", OracleDbType.Varchar2, Left(ldrRow("SEGMENT6_CTA_ACTIVO").ToString, 3), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PSEGMENT7_CTA_ACTIVO", OracleDbType.Varchar2, Left(ldrRow("SEGMENT7_CTA_ACTIVO").ToString, 4), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PSEGMENT8_CTA_ACTIVO", OracleDbType.Varchar2, Left(ldrRow("SEGMENT8_CTA_ACTIVO").ToString, 3), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PACCOUNTED_DR", OracleDbType.Double, ldrRow("ACCOUNTED_DR"), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PENTERED_DR", OracleDbType.Double, ldrRow("ENTERED_DR"), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PACCOUNTED_CR", OracleDbType.Double, ldrRow("ACCOUNTED_CR"), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PENTERED_CR", OracleDbType.Double, ldrRow("ENTERED_CR"), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PORG_ID", OracleDbType.Double, ldrRow("ORG_ID"), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PATTRIBUTE2", OracleDbType.Varchar2, Left(ldrRow("ATTRIBUTE2").ToString, 1), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PCREATION_DATE", OracleDbType.Char, ldrRow("CREATION_DATE").ToString, ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PCREATED_BY", OracleDbType.Double, ldrRow("CREATED_BY"), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PLAST_UPDATE_DATE", OracleDbType.Char, ldrRow("LAST_UPDATE_DATE").ToString, ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PLAST_UPDATED_BY", OracleDbType.Double, ldrRow("LAST_UPDATED_BY"), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PLAST_UPDATE_LOGIN", OracleDbType.Double, ldrRow("LAST_UPDATE_LOGIN"), ParameterDirection.Input)

        Dim ParametroSal As New OracleParameter("PDescError", OracleDbType.Varchar2, 2000)
        ParametroSal.Direction = Data.ParameterDirection.Output
        lcOracleCommand.Parameters.Add(ParametroSal)

        lcOracleCommand.ExecuteNonQuery()

        strDescError = lcOracleCommand.Parameters("PDescError").Value.ToString.Trim

        If Not strDescError = "OK" Then
          Exit For
        End If
      Next

      If strDescError = "OK" Then
        lcMiTransaccion.Commit()
      Else
        lcMiTransaccion.Rollback()
      End If

    Catch ex As Exception
      strDescError = "Problema en el guardar en Contabilidad." & vbCr & ex.Message
      If Not IsNothing(lcMiTransaccion) Then
        lcMiTransaccion.Rollback()
      End If
    Finally
      lcConexion.Cerrar()
    End Try

    GuardarGL = strDescError
  End Function
End Class
