﻿Imports Microsoft.VisualBasic
Imports System.Data

Imports Oracle.DataAccess.Client
Imports Oracle.DataAccess.Types

Public Class ClsInterfaceTesoreria_FOH
#Region "8====== REGION OPERACIONES =======3~X_O"
  Public Function InformarOperacion(ByVal intIdOperacion As Integer _
                                  , ByVal strFlg_Tipo_Movimiento As String) As String
    Dim strDescError As String = ""
    Dim ldsOperacion As DataSet

    InformarOperacion = ""

    Select Case strFlg_Tipo_Movimiento
      Case "E"
        Try
          ldsOperacion = New DataSet
          strDescError = LeerOperacion_AR(intIdOperacion:=intIdOperacion, dsOperacion:=ldsOperacion)
          If strDescError <> "OK" Then
            InformarOperacion = strDescError
            Exit Function
          End If
        Catch ex As Exception
          strDescError = "Problema en el leer la operación '" & intIdOperacion & "'." & vbCr & ex.Message
          Exit Function
        End Try

        Try
          strDescError = GuardarAR(dtTabla:=ldsOperacion.Tables("OPERACION"))
          If strDescError <> "OK" Then
            InformarOperacion = strDescError
          End If
        Catch ex As Exception
          strDescError = "Problema en el guardar la operación '" & intIdOperacion & "'." & vbCr & ex.Message
        End Try
      Case "I"
        Try
          ldsOperacion = New DataSet
          strDescError = LeerOperacion_AP(intIdOperacion:=intIdOperacion, dsOperacion:=ldsOperacion)
          If strDescError <> "OK" Then
            InformarOperacion = strDescError
            Exit Function
          End If
        Catch ex As Exception
          strDescError = "Problema en el leer la operación '" & intIdOperacion & "'." & vbCr & ex.Message
          Exit Function
        End Try

        Try
          strDescError = GuardarAP(dtTabla:=ldsOperacion.Tables("OPERACION"))
          If strDescError <> "OK" Then
            InformarOperacion = strDescError
          End If
        Catch ex As Exception
          strDescError = "Problema en el guardar la operación '" & intIdOperacion & "'." & vbCr & ex.Message
        End Try
    End Select

    Return strDescError
  End Function

#Region "8====== LECTURAS DE OPERACIONES =======3~X_O"
  Private Function LeerOperacion_AR(ByVal intIdOperacion As Integer _
                                  , ByRef dsOperacion As DataSet) As String
    '---------------------------------------------------------
    Dim SQLCommand As New SqlClient.SqlCommand
    Dim SQLConnect As New Cls_Conexion
    Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
    '---------------------------------------------------------
    Dim lstrProcedimiento As String = ""
    Dim strDescError As String = ""
    '---------------------------------------------------------

    '...Abre la conexion
    SQLConnect.Abrir()
    lstrProcedimiento = "FOH_TESORERIA_LEER_OPERACION_AR"

    Try
      SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, SQLConnect.Conexion)
      SQLCommand.CommandType = CommandType.StoredProcedure
      SQLCommand.CommandText = lstrProcedimiento
      SQLCommand.Parameters.Clear()

      '( @PID_OPERACION NUMERIC
      ', @PRESULTADO VARCHAR(200) OUTPUT
      SQLCommand.Parameters.Add("PID_OPERACION", SqlDbType.Float, 3).Value = intIdOperacion
      SQLCommand.Parameters.Add("PID_USUARIO", SqlDbType.Float, 3).Value = glngIdUsuario

      '...Resultado
      Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
      ParametroSal2.Direction = Data.ParameterDirection.Output
      SQLCommand.Parameters.Add(ParametroSal2)

      SQLCommand.ExecuteNonQuery()

      SQLDataAdapter.SelectCommand = SQLCommand
      SQLDataAdapter.Fill(dsOperacion, "OPERACION")

      strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

    Catch Ex As Exception
      strDescError = "Problema en el leer la operación '" & intIdOperacion & "'." & vbCr & Ex.Message
    Finally
      SQLConnect.Cerrar()
      LeerOperacion_AR = strDescError
    End Try
  End Function

  Private Function LeerOperacion_AP(ByVal intIdOperacion As Integer _
                                  , ByRef dsOperacion As DataSet) As String
    '---------------------------------------------------------
    Dim SQLCommand As New SqlClient.SqlCommand
    Dim SQLConnect As New Cls_Conexion
    Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
    '---------------------------------------------------------
    Dim lstrProcedimiento As String = ""
    Dim strDescError As String = ""
    '---------------------------------------------------------

    '...Abre la conexion
    SQLConnect.Abrir()
    lstrProcedimiento = "FOH_TESORERIA_LEER_OPERACION_AP"

    Try
      SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, SQLConnect.Conexion)
      SQLCommand.CommandType = CommandType.StoredProcedure
      SQLCommand.CommandText = lstrProcedimiento
      SQLCommand.Parameters.Clear()

      '( @PID_OPERACION NUMERIC
      ', @PRESULTADO VARCHAR(200) OUTPUT
      SQLCommand.Parameters.Add("PID_OPERACION", SqlDbType.Float, 3).Value = intIdOperacion
      SQLCommand.Parameters.Add("PID_USUARIO", SqlDbType.Float, 3).Value = glngIdUsuario

      '...Resultado
      Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
      ParametroSal2.Direction = Data.ParameterDirection.Output
      SQLCommand.Parameters.Add(ParametroSal2)

      SQLCommand.ExecuteNonQuery()

      SQLDataAdapter.SelectCommand = SQLCommand
      SQLDataAdapter.Fill(dsOperacion, "OPERACION")

      strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

    Catch Ex As Exception
      strDescError = "Problema en el leer la operación '" & intIdOperacion & "'." & vbCr & Ex.Message
    Finally
      SQLConnect.Cerrar()
      LeerOperacion_AP = strDescError
    End Try
  End Function
#End Region
#End Region

#Region "8====== REGION CAJA =======3~X_O"

  Public Function InformarCaja(ByVal intIdMovCaja As Integer _
                                  , ByVal strFlg_Tipo_Movimiento As String) As String
    Dim strDescError As String = ""
    Dim ldsCaja As DataSet

    InformarCaja = ""

    Select Case strFlg_Tipo_Movimiento
      Case "A"
        Try
          ldsCaja = New DataSet
          strDescError = LeerCaja_AR(intIdMovCaja:=intIdMovCaja, dsCaja:=ldsCaja)
          If strDescError <> "OK" Then
            InformarCaja = strDescError
            Exit Function
          End If
        Catch ex As Exception
          strDescError = "Problema en el leer la caja '" & intIdMovCaja & "'." & vbCr & ex.Message
          Exit Function
        End Try

        Try
          strDescError = GuardarAR(dtTabla:=ldsCaja.Tables("CAJA"))
          If strDescError <> "OK" Then
            InformarCaja = strDescError
          End If
        Catch ex As Exception
          strDescError = "Problema en el guardar la caja '" & intIdMovCaja & "'." & vbCr & ex.Message
        End Try
      Case "C"
        Try
          ldsCaja = New DataSet
          strDescError = LeerCaja_AP(intIdMovCaja:=intIdMovCaja, dsCaja:=ldsCaja)
          If strDescError <> "OK" Then
            InformarCaja = strDescError
            Exit Function
          End If
        Catch ex As Exception
          strDescError = "Problema en el leer la caja '" & intIdMovCaja & "'." & vbCr & ex.Message
          Exit Function
        End Try

        Try
          strDescError = GuardarAP(dtTabla:=ldsCaja.Tables("CAJA"))
          If strDescError <> "OK" Then
            InformarCaja = strDescError
          End If
        Catch ex As Exception
          strDescError = "Problema en el guardar la caja '" & intIdMovCaja & "'." & vbCr & ex.Message
        End Try
    End Select

    Return strDescError
  End Function

  Private Function LeerCaja_AR(ByVal intIdMovCaja As Integer _
                             , ByRef dsCaja As DataSet) As String
    '---------------------------------------------------------
    Dim SQLCommand As New SqlClient.SqlCommand
    Dim SQLConnect As New Cls_Conexion
    Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
    '---------------------------------------------------------
    Dim lstrProcedimiento As String = ""
    Dim strDescError As String = ""
    '---------------------------------------------------------

    '...Abre la conexion
    SQLConnect.Abrir()
    lstrProcedimiento = "FOH_TESORERIA_LEER_CAJA_AR"

    Try
      SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, SQLConnect.Conexion)
      SQLCommand.CommandType = CommandType.StoredProcedure
      SQLCommand.CommandText = lstrProcedimiento
      SQLCommand.Parameters.Clear()

      '( @PID_MOV_CAJA NUMERIC
      ', @PRESULTADO VARCHAR(200) OUTPUT
      SQLCommand.Parameters.Add("PID_MOV_CAJA", SqlDbType.Float, 3).Value = intIdMovCaja

      '...Resultado
      Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
      ParametroSal2.Direction = Data.ParameterDirection.Output
      SQLCommand.Parameters.Add(ParametroSal2)

      SQLCommand.ExecuteNonQuery()

      SQLDataAdapter.SelectCommand = SQLCommand
      SQLDataAdapter.Fill(dsCaja, "CAJA")

      strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

    Catch Ex As Exception
      strDescError = "Problema en el leer la caja '" & intIdMovCaja & "'." & vbCr & Ex.Message
    Finally
      SQLConnect.Cerrar()
      LeerCaja_AR = strDescError
    End Try
  End Function

  Private Function LeerCaja_AP(ByVal intIdMovCaja As Integer _
                             , ByRef dsCaja As DataSet) As String
    '---------------------------------------------------------
    Dim SQLCommand As New SqlClient.SqlCommand
    Dim SQLConnect As New Cls_Conexion
    Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
    '---------------------------------------------------------
    Dim lstrProcedimiento As String = ""
    Dim strDescError As String = ""
    '---------------------------------------------------------

    '...Abre la conexion
    SQLConnect.Abrir()
    lstrProcedimiento = "FOH_TESORERIA_LEER_CAJA_AP"

    Try
      SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, SQLConnect.Conexion)
      SQLCommand.CommandType = CommandType.StoredProcedure
      SQLCommand.CommandText = lstrProcedimiento
      SQLCommand.Parameters.Clear()

      '( @PID_MOV_CAJA NUMERIC
      ', @PRESULTADO VARCHAR(200) OUTPUT
      SQLCommand.Parameters.Add("PID_MOV_CAJA", SqlDbType.Float, 3).Value = intIdMovCaja

      '...Resultado
      Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
      ParametroSal2.Direction = Data.ParameterDirection.Output
      SQLCommand.Parameters.Add(ParametroSal2)

      SQLCommand.ExecuteNonQuery()

      SQLDataAdapter.SelectCommand = SQLCommand
      SQLDataAdapter.Fill(dsCaja, "CAJA")

      strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

    Catch Ex As Exception
      strDescError = "Problema en el leer la caja '" & intIdMovCaja & "'." & vbCr & Ex.Message
    Finally
      SQLConnect.Cerrar()
      LeerCaja_AP = strDescError
    End Try
  End Function
#End Region

#Region "8====== CONEXION CON ORACLE FOR OLE =======3~X_O"

  'Private Function GuardarAR(ByRef dtOperacion As DataTable) As String
  '  Dim lcConexion As New ClsConexionOracle
  '  Dim lSql As String
  '  '---------------------------------------------
  '  Dim ldrRow As DataRow
  '  '---------------------------------------------
  '  Dim strDescError As String = ""
  '  '---------------------------------------------
  '  Dim Linvoice_Currency_Code As String
  '  Dim Lcustomer_Bill_Number As String
  '  Dim Lbill_Address_Line_1 As String
  '  Dim Lbatch_Source_Name As String
  '  Dim Lcust_Trx_Type_Name As String
  '  Dim Lterms_Name As String
  '  Dim Ltrx_Number As String
  '  Dim Ltrx_Date As String
  '  Dim Lamount As Double
  '  Dim Ldue_Date As String
  '  Dim Lgl_Date As Date
  '  Dim Lconversion_Type As String
  '  Dim Lconversion_Rate As Double
  '  Dim Lconversion_Date As String
  '  Dim Lattribute1 As String
  '  Dim Lline_Number As Double
  '  Dim Lline_Type As String
  '  Dim Lquantity As Double
  '  Dim Lunit_Selling_Price As Double
  '  Dim Ldescription As String
  '  '---------------------------------------------

  '  Try
  '    lcConexion.Abrir()
  '  Catch ex As Exception
  '    strDescError = "Problema al abrir la base de datos Oracle." & vbCr & ex.Message
  '    Return strDescError
  '  End Try

  '  Try
  '    lcConexion.BeginTransaccion()

  '    For Each ldrRow In dtOperacion.Rows

  '      lINVOICE_CURRENCY_CODE = ldrRow("INVOICE_CURRENCY_CODE").ToString
  '      lCUSTOMER_BILL_NUMBER = ldrRow("CUSTOMER_BILL_NUMBER").ToString
  '      lBILL_ADDRESS_LINE_1 = ldrRow("BILL_ADDRESS_LINE_1").ToString
  '      lBATCH_SOURCE_NAME = ldrRow("BATCH_SOURCE_NAME").ToString
  '      lCUST_TRX_TYPE_NAME = ldrRow("CUST_TRX_TYPE_NAME").ToString
  '      lTERMS_NAME = ldrRow("TERMS_NAME").ToString
  '      lTRX_NUMBER = ldrRow("TRX_NUMBER").ToString
  '      lTRX_DATE = ldrRow("TRX_DATE").ToString
  '      lAMOUNT = ldrRow("AMOUNT").ToString
  '      lDUE_DATE = ldrRow("DUE_DATE").ToString
  '      lGL_DATE = ldrRow("GL_DATE").ToString
  '      lCONVERSION_TYPE = ldrRow("CONVERSION_TYPE").ToString
  '      lCONVERSION_RATE = ldrRow("CONVERSION_RATE").ToString
  '      lCONVERSION_DATE = ldrRow("CONVERSION_DATE").ToString
  '      lATTRIBUTE1 = ldrRow("ATTRIBUTE1").ToString
  '      lLINE_NUMBER = ldrRow("LINE_NUMBER").ToString
  '      lLINE_TYPE = ldrRow("LINE_TYPE").ToString
  '      lQUANTITY = ldrRow("QUANTITY").ToString
  '      lUNIT_SELLING_PRICE = ldrRow("UNIT_SELLING_PRICE").ToString
  '      lDESCRIPTION = ldrRow("DESCRIPTION").ToString


  '      lcConexion.ClearParam()
  '      lcConexion.OraDatabase.Parameters.Add("PINVOICE_CURRENCY_CODE", Linvoice_Currency_Code, gORAPARM_INPUT)
  '      lcConexion.OraDatabase.Parameters("PINVOICE_CURRENCY_CODE").ServerType = gORATYPE_VARCHAR2
  '      lcConexion.OraDatabase.Parameters.Add("PCUSTOMER_BILL_NUMBER", Lcustomer_Bill_Number, gORAPARM_INPUT)
  '      lcConexion.OraDatabase.Parameters("PCUSTOMER_BILL_NUMBER").ServerType = gORATYPE_VARCHAR2
  '      lcConexion.OraDatabase.Parameters.Add("PBILL_ADDRESS_LINE_1", Lbill_Address_Line_1, gORAPARM_INPUT)
  '      lcConexion.OraDatabase.Parameters("PBILL_ADDRESS_LINE_1").ServerType = gORATYPE_VARCHAR2
  '      lcConexion.OraDatabase.Parameters.Add("PBATCH_SOURCE_NAME", Lbatch_Source_Name, gORAPARM_INPUT)
  '      lcConexion.OraDatabase.Parameters("PBATCH_SOURCE_NAME").ServerType = gORATYPE_VARCHAR2
  '      lcConexion.OraDatabase.Parameters.Add("PCUST_TRX_TYPE_NAME", Lcust_Trx_Type_Name, gORAPARM_INPUT)
  '      lcConexion.OraDatabase.Parameters("PCUST_TRX_TYPE_NAME").ServerType = gORATYPE_VARCHAR2
  '      lcConexion.OraDatabase.Parameters.Add("PTERMS_NAME", Lterms_Name, gORAPARM_INPUT)
  '      lcConexion.OraDatabase.Parameters("PTERMS_NAME").ServerType = gORATYPE_VARCHAR2
  '      lcConexion.OraDatabase.Parameters.Add("PTRX_NUMBER", Ltrx_Number, gORAPARM_INPUT)
  '      lcConexion.OraDatabase.Parameters("PTRX_NUMBER").ServerType = gORATYPE_VARCHAR2
  '      lcConexion.OraDatabase.Parameters.Add("PTRX_DATE", Ltrx_Date, gORAPARM_INPUT)
  '      lcConexion.OraDatabase.Parameters("PTRX_DATE").ServerType = gORATYPE_VARCHAR2
  '      lcConexion.OraDatabase.Parameters.Add("PAMOUNT", Lamount, gORAPARM_INPUT)
  '      lcConexion.OraDatabase.Parameters("PAMOUNT").ServerType = gORATYPE_NUMBER
  '      lcConexion.OraDatabase.Parameters.Add("PDUE_DATE", Ldue_Date, gORAPARM_INPUT)
  '      lcConexion.OraDatabase.Parameters("PDUE_DATE").ServerType = gORATYPE_VARCHAR2
  '      lcConexion.OraDatabase.Parameters.Add("PGL_DATE", Lgl_Date, gORAPARM_INPUT)
  '      lcConexion.OraDatabase.Parameters("PGL_DATE").ServerType = gORATYPE_VARCHAR2
  '      lcConexion.OraDatabase.Parameters.Add("PCONVERSION_TYPE", Lconversion_Type, gORAPARM_INPUT)
  '      lcConexion.OraDatabase.Parameters("PCONVERSION_TYPE").ServerType = gORATYPE_VARCHAR2
  '      lcConexion.OraDatabase.Parameters.Add("PCONVERSION_RATE", Lconversion_Rate, gORAPARM_INPUT)
  '      lcConexion.OraDatabase.Parameters("PCONVERSION_RATE").ServerType = gORATYPE_VARCHAR2
  '      lcConexion.OraDatabase.Parameters.Add("PCONVERSION_DATE", Lconversion_Date, gORAPARM_INPUT)
  '      lcConexion.OraDatabase.Parameters("PCONVERSION_DATE").ServerType = gORATYPE_VARCHAR2
  '      lcConexion.OraDatabase.Parameters.Add("PATTRIBUTE1", lATRIBBUTE1, gORAPARM_INPUT)
  '      lcConexion.OraDatabase.Parameters("PATTRIBUTE1").ServerType = gORATYPE_VARCHAR2
  '      lcConexion.OraDatabase.Parameters.Add("PLINE_NUMBER", Lline_Number, gORAPARM_INPUT)
  '      lcConexion.OraDatabase.Parameters("PLINE_NUMBER").ServerType = gORATYPE_VARCHAR2
  '      lcConexion.OraDatabase.Parameters.Add("PLINE_TYPE", Lline_Type, gORAPARM_INPUT)
  '      lcConexion.OraDatabase.Parameters("PLINE_TYPE").ServerType = gORATYPE_VARCHAR2
  '      lcConexion.OraDatabase.Parameters.Add("PQUANTITY", Lquantity, gORAPARM_INPUT)
  '      lcConexion.OraDatabase.Parameters("PQUANTITY").ServerType = gORATYPE_VARCHAR2
  '      lcConexion.OraDatabase.Parameters.Add("PUNIT_SELLING_PRICE", Lunit_Selling_Price, gORAPARM_INPUT)
  '      lcConexion.OraDatabase.Parameters("PUNIT_SELLING_PRICE").ServerType = gORATYPE_VARCHAR2
  '      lcConexion.OraDatabase.Parameters.Add("PDESCRIPTION", Ldescription, gORAPARM_INPUT)
  '      lcConexion.OraDatabase.Parameters("PDESCRIPTION").ServerType = gORATYPE_VARCHAR2

  '      '--Salida
  '      lcConexion.OraDatabase.Parameters.Add("PDescError", "", gORAPARM_OUTPUT)
  '      lcConexion.OraDatabase.Parameters("PDescError").ServerType = gORATYPE_VARCHAR2

  '      lSql = ""
  '      lSql = lSql & "BEGIN "
  '      lSql = lSql & " Foh_tesorieria.Guardar_ar("
  '      lSql = lSql & "  :Pinvoice_currency_code "
  '      lSql = lSql & ", :Pcustomer_bill_number  "
  '      lSql = lSql & ", :Pbill_address_line_1   "
  '      lSql = lSql & ", :Pbatch_source_name     "
  '      lSql = lSql & ", :Pcust_trx_type_name    "
  '      lSql = lSql & ", :Pterms_name            "
  '      lSql = lSql & ", :Ptrx_number            "
  '      lSql = lSql & ", :Ptrx_date              "
  '      lSql = lSql & ", :Pamount                "
  '      lSql = lSql & ", :Pdue_date              "
  '      lSql = lSql & ", :Pgl_date               "
  '      lSql = lSql & ", :Pconversion_type       "
  '      lSql = lSql & ", :Pconversion_rate       "
  '      lSql = lSql & ", :Pconversion_date       "
  '      lSql = lSql & ", :Pattribute1            "
  '      lSql = lSql & ", :Pline_number           "
  '      lSql = lSql & ", :Pline_type             "
  '      lSql = lSql & ", :Pquantity              "
  '      lSql = lSql & ", :Punit_selling_price    "
  '      lSql = lSql & ", :Pdescription           "
  '      lSql = lSql & ", :Pdescerror             "
  '      lSql = lSql & ");"
  '      lSql = lSql & "END;"

  '      lcConexion.OraDatabase.LastServerErrReset()
  '      Call lcConexion.OraDatabase.dbexecutesql(lSql)

  '      strDescError = lcConexion.OraDatabase.Parameters("PDescError").Value

  '      If Not strDescError = "OK" Then
  '        Exit For
  '      End If
  '    Next

  '    If strDescError = "OK" Then
  '      lcConexion.CommitTransaccion()
  '    Else
  '      lcConexion.RollbackTransaccion()
  '    End If

  '  Catch ex As Exception
  '    strDescError = "Problema en el guardar en Tesorería." & vbCr & ex.Message
  '    lcConexion.RollbackTransaccion()
  '  Finally
  '    lcConexion.Cerrar()
  '  End Try

  '  GuardarAR = strDescError
  'End Function

  'Private Function GuardarAP(ByRef dtOperacion As DataTable) As String
  '  Dim lcConexion As New ClsConexionOracle
  '  Dim lSql As String
  '  '---------------------------------------------
  '  Dim ldrRow As DataRow
  '  '---------------------------------------------
  '  Dim strDescError As String = ""

  '  Try
  '    lcConexion.Abrir()
  '  Catch ex As Exception
  '    strDescError = "Problema al abrir la base de datos Oracle." & vbCr & ex.Message
  '    Return strDescError
  '  End Try

  '  Try
  '    lcConexion.BeginTransaccion()

  '    For Each ldrRow In dtOperacion.Rows

  '      lcConexion.ClearParam()
  '      lcConexion.OraDatabase.Parameters.Add("pINVOICE_CURRENCY_CODE", ldrRow("INVOICE_CURRENCY_CODE").ToString, gORAPARM_INPUT)
  '      lcConexion.OraDatabase.Parameters("pINVOICE_CURRENCY_CODE").ServerType = gORATYPE_VARCHAR2
  '      lcConexion.OraDatabase.Parameters.Add("pVENDOR_NUM", ldrRow("VENDOR_NUM").ToString, gORAPARM_INPUT)
  '      lcConexion.OraDatabase.Parameters("pVENDOR_NUM").ServerType = gORATYPE_VARCHAR2
  '      lcConexion.OraDatabase.Parameters.Add("pVENDOR_STE_CODE", ldrRow("VENDOR_STE_CODE").ToString, gORAPARM_INPUT)
  '      lcConexion.OraDatabase.Parameters("pVENDOR_STE_CODE").ServerType = gORATYPE_VARCHAR2
  '      lcConexion.OraDatabase.Parameters.Add("pTERMS_NAME", ldrRow("TERMS_NAME").ToString, gORAPARM_INPUT)
  '      lcConexion.OraDatabase.Parameters("pTERMS_NAME").ServerType = gORATYPE_VARCHAR2
  '      lcConexion.OraDatabase.Parameters.Add("pDESCRIPTION", ldrRow("DESCRIPTION").ToString, gORAPARM_INPUT)
  '      lcConexion.OraDatabase.Parameters("pDESCRIPTION").ServerType = gORATYPE_VARCHAR2
  '      lcConexion.OraDatabase.Parameters.Add("pINVOICE_NUM", ldrRow("INVOICE_NUM").ToString, gORAPARM_INPUT)
  '      lcConexion.OraDatabase.Parameters("pINVOICE_NUM").ServerType = gORATYPE_VARCHAR2
  '      lcConexion.OraDatabase.Parameters.Add("pINVOICE_AMOUNT", ldrRow("INVOICE_AMOUNT").ToString, gORAPARM_INPUT)
  '      lcConexion.OraDatabase.Parameters("pINVOICE_AMOUNT").ServerType = gORATYPE_VARCHAR2
  '      lcConexion.OraDatabase.Parameters.Add("pINVOICE_DATE", ldrRow("INVOICE_DATE").ToString, gORAPARM_INPUT)
  '      lcConexion.OraDatabase.Parameters("pINVOICE_DATE").ServerType = gORATYPE_VARCHAR2
  '      lcConexion.OraDatabase.Parameters.Add("pINVOICE_TYPE_LOOKUP_CODE", ldrRow("INVOICE_TYPE_LOOKUP_CODE").ToString, gORAPARM_INPUT)
  '      lcConexion.OraDatabase.Parameters("pINVOICE_TYPE_LOOKUP_CODE").ServerType = gORATYPE_VARCHAR2
  '      lcConexion.OraDatabase.Parameters.Add("pGL_DATE", ldrRow("GL_DATE").ToString, gORAPARM_INPUT)
  '      lcConexion.OraDatabase.Parameters("pGL_DATE").ServerType = gORATYPE_VARCHAR2
  '      lcConexion.OraDatabase.Parameters.Add("pEXCHANGE_RATE_TYPE", ldrRow("EXCHANGE_RATE_TYPE").ToString, gORAPARM_INPUT)
  '      lcConexion.OraDatabase.Parameters("pEXCHANGE_RATE_TYPE").ServerType = gORATYPE_VARCHAR2
  '      lcConexion.OraDatabase.Parameters.Add("pEXCHANGE_RATE", ldrRow("EXCHANGE_RATE").ToString, gORAPARM_INPUT)
  '      lcConexion.OraDatabase.Parameters("pEXCHANGE_RATE").ServerType = gORATYPE_VARCHAR2
  '      lcConexion.OraDatabase.Parameters.Add("pEXCHANGE_DATE", ldrRow("EXCHANGE_DATE").ToString, gORAPARM_INPUT)
  '      lcConexion.OraDatabase.Parameters("pEXCHANGE_DATE").ServerType = gORATYPE_VARCHAR2
  '      lcConexion.OraDatabase.Parameters.Add("pGLOBAL_ATTRIBUTE_CATEGORY", ldrRow("GLOBAL_ATTRIBUTE_CATEGORY").ToString, gORAPARM_INPUT)
  '      lcConexion.OraDatabase.Parameters("pGLOBAL_ATTRIBUTE_CATEGORY").ServerType = gORATYPE_VARCHAR2
  '      lcConexion.OraDatabase.Parameters.Add("pGLOBAL_ATTRIBUTE19", ldrRow("GLOBAL_ATTRIBUTE19").ToString, gORAPARM_INPUT)
  '      lcConexion.OraDatabase.Parameters("pGLOBAL_ATTRIBUTE19").ServerType = gORATYPE_VARCHAR2
  '      lcConexion.OraDatabase.Parameters.Add("pSOURCE", ldrRow("SOURCE").ToString, gORAPARM_INPUT)
  '      lcConexion.OraDatabase.Parameters("pSOURCE").ServerType = gORATYPE_VARCHAR2
  '      lcConexion.OraDatabase.Parameters.Add("pSEGMENT1_CTA_PASVO", ldrRow("SEGMENT1_CTA_PASVO").ToString, gORAPARM_INPUT)
  '      lcConexion.OraDatabase.Parameters("pSEGMENT1_CTA_PASVO").ServerType = gORATYPE_VARCHAR2
  '      lcConexion.OraDatabase.Parameters.Add("pSEGMENT2_CTA_PASVO", ldrRow("SEGMENT2_CTA_PASVO").ToString, gORAPARM_INPUT)
  '      lcConexion.OraDatabase.Parameters("pSEGMENT2_CTA_PASVO").ServerType = gORATYPE_VARCHAR2
  '      lcConexion.OraDatabase.Parameters.Add("pSEGMENT3_CTA_PASVO", ldrRow("SEGMENT3_CTA_PASVO").ToString, gORAPARM_INPUT)
  '      lcConexion.OraDatabase.Parameters("pSEGMENT3_CTA_PASVO").ServerType = gORATYPE_VARCHAR2
  '      lcConexion.OraDatabase.Parameters.Add("pSEGMENT4_CTA_PASVO", ldrRow("SEGMENT4_CTA_PASVO").ToString, gORAPARM_INPUT)
  '      lcConexion.OraDatabase.Parameters("pSEGMENT4_CTA_PASVO").ServerType = gORATYPE_VARCHAR2
  '      lcConexion.OraDatabase.Parameters.Add("pSEGMENT5_CTA_PASVO", ldrRow("SEGMENT5_CTA_PASVO").ToString, gORAPARM_INPUT)
  '      lcConexion.OraDatabase.Parameters("pSEGMENT5_CTA_PASVO").ServerType = gORATYPE_VARCHAR2
  '      lcConexion.OraDatabase.Parameters.Add("pSEGMENT6_CTA_PASVO", ldrRow("SEGMENT6_CTA_PASVO").ToString, gORAPARM_INPUT)
  '      lcConexion.OraDatabase.Parameters("pSEGMENT6_CTA_PASVO").ServerType = gORATYPE_VARCHAR2
  '      lcConexion.OraDatabase.Parameters.Add("pSEGMENT7_CTA_PASVO", ldrRow("SEGMENT7_CTA_PASVO").ToString, gORAPARM_INPUT)
  '      lcConexion.OraDatabase.Parameters("pSEGMENT7_CTA_PASVO").ServerType = gORATYPE_VARCHAR2
  '      lcConexion.OraDatabase.Parameters.Add("pSEGMENT8_CTA_PASVO", ldrRow("SEGMENT8_CTA_PASVO").ToString, gORAPARM_INPUT)
  '      lcConexion.OraDatabase.Parameters("pSEGMENT8_CTA_PASVO").ServerType = gORATYPE_VARCHAR2
  '      lcConexion.OraDatabase.Parameters.Add("pATTRIBUTE_CATEGORY", ldrRow("ATTRIBUTE_CATEGORY").ToString, gORAPARM_INPUT)
  '      lcConexion.OraDatabase.Parameters("pATTRIBUTE_CATEGORY").ServerType = gORATYPE_VARCHAR2
  '      lcConexion.OraDatabase.Parameters.Add("pATTRIBUTE1", ldrRow("ATTRIBUTE1").ToString, gORAPARM_INPUT)
  '      lcConexion.OraDatabase.Parameters("pATTRIBUTE1").ServerType = gORATYPE_VARCHAR2
  '      lcConexion.OraDatabase.Parameters.Add("pLINE_NUMBER", ldrRow("LINE_NUMBER").ToString, gORAPARM_INPUT)
  '      lcConexion.OraDatabase.Parameters("pLINE_NUMBER").ServerType = gORATYPE_VARCHAR2
  '      lcConexion.OraDatabase.Parameters.Add("pLINE_TYPE_LOOKUP_CODE", ldrRow("LINE_TYPE_LOOKUP_CODE").ToString, gORAPARM_INPUT)
  '      lcConexion.OraDatabase.Parameters("pLINE_TYPE_LOOKUP_CODE").ServerType = gORATYPE_VARCHAR2
  '      lcConexion.OraDatabase.Parameters.Add("pAMOUNT", ldrRow("AMOUNT").ToString, gORAPARM_INPUT)
  '      lcConexion.OraDatabase.Parameters("pAMOUNT").ServerType = gORATYPE_VARCHAR2
  '      lcConexion.OraDatabase.Parameters.Add("pACCOUNTING_DATE", ldrRow("ACCOUNTING_DATE").ToString, gORAPARM_INPUT)
  '      lcConexion.OraDatabase.Parameters("pACCOUNTING_DATE").ServerType = gORATYPE_VARCHAR2
  '      lcConexion.OraDatabase.Parameters.Add("pSEGMENT1_CTA_ITEM", ldrRow("SEGMENT1_CTA_ITEM").ToString, gORAPARM_INPUT)
  '      lcConexion.OraDatabase.Parameters("pSEGMENT1_CTA_ITEM").ServerType = gORATYPE_VARCHAR2
  '      lcConexion.OraDatabase.Parameters.Add("pSEGMENT2_CTA_ITEM", ldrRow("SEGMENT2_CTA_ITEM").ToString, gORAPARM_INPUT)
  '      lcConexion.OraDatabase.Parameters("pSEGMENT2_CTA_ITEM").ServerType = gORATYPE_VARCHAR2
  '      lcConexion.OraDatabase.Parameters.Add("pSEGMENT3_CTA_ITEM", ldrRow("SEGMENT3_CTA_ITEM").ToString, gORAPARM_INPUT)
  '      lcConexion.OraDatabase.Parameters("pSEGMENT3_CTA_ITEM").ServerType = gORATYPE_VARCHAR2
  '      lcConexion.OraDatabase.Parameters.Add("pSEGMENT4_CTA_ITEM", ldrRow("SEGMENT4_CTA_ITEM").ToString, gORAPARM_INPUT)
  '      lcConexion.OraDatabase.Parameters("pSEGMENT4_CTA_ITEM").ServerType = gORATYPE_VARCHAR2
  '      lcConexion.OraDatabase.Parameters.Add("pSEGMENT5_CTA_ITEM", ldrRow("SEGMENT5_CTA_ITEM").ToString, gORAPARM_INPUT)
  '      lcConexion.OraDatabase.Parameters("pSEGMENT5_CTA_ITEM").ServerType = gORATYPE_VARCHAR2
  '      lcConexion.OraDatabase.Parameters.Add("pSEGMENT6_CTA_ITEM", ldrRow("SEGMENT6_CTA_ITEM").ToString, gORAPARM_INPUT)
  '      lcConexion.OraDatabase.Parameters("pSEGMENT6_CTA_ITEM").ServerType = gORATYPE_VARCHAR2
  '      lcConexion.OraDatabase.Parameters.Add("pSEGMENT7_CTA_ITEM", ldrRow("SEGMENT7_CTA_ITEM").ToString, gORAPARM_INPUT)
  '      lcConexion.OraDatabase.Parameters("pSEGMENT7_CTA_ITEM").ServerType = gORATYPE_VARCHAR2
  '      lcConexion.OraDatabase.Parameters.Add("pSEGMENT8_CTA_ITEM", ldrRow("SEGMENT8_CTA_ITEM").ToString, gORAPARM_INPUT)
  '      lcConexion.OraDatabase.Parameters("pSEGMENT8_CTA_ITEM").ServerType = gORATYPE_VARCHAR2
  '      lcConexion.OraDatabase.Parameters.Add("pTAX_CODE", ldrRow("TAX_CODE").ToString, gORAPARM_INPUT)
  '      lcConexion.OraDatabase.Parameters("pTAX_CODE").ServerType = gORATYPE_VARCHAR2
  '      lcConexion.OraDatabase.Parameters.Add("pAWT_GROUP_NAME", ldrRow("AWT_GROUP_NAME").ToString, gORAPARM_INPUT)
  '      lcConexion.OraDatabase.Parameters("pAWT_GROUP_NAME").ServerType = gORATYPE_VARCHAR2
  '      lcConexion.OraDatabase.Parameters.Add("pORG_ID", ldrRow("ORG_ID").ToString, gORAPARM_INPUT)
  '      lcConexion.OraDatabase.Parameters("pORG_ID").ServerType = gORATYPE_VARCHAR2
  '      lcConexion.OraDatabase.Parameters.Add("pATTRIBUTE2", ldrRow("ATTRIBUTE2").ToString, gORAPARM_INPUT)
  '      lcConexion.OraDatabase.Parameters("pATTRIBUTE2").ServerType = gORATYPE_VARCHAR2

  '      '--Salida
  '      lcConexion.OraDatabase.Parameters.Add("PDescError", "", gORAPARM_OUTPUT)
  '      lcConexion.OraDatabase.Parameters("PDescError").ServerType = gORATYPE_VARCHAR2

  '      lSql = ""
  '      lSql = lSql & "BEGIN"
  '      lSql = lSql & "  Xxfoh.Foh_tesorieria.Guardar_ap ("
  '      lSql = lSql & "    :Pinvoice_currency_code"
  '      lSql = lSql & "  , :Pvendor_num"
  '      lSql = lSql & "  , :Pvendor_ste_code"
  '      lSql = lSql & "  , :Pterms_name"
  '      lSql = lSql & "  , :Pdescription"
  '      lSql = lSql & "  , :Pinvoice_num"
  '      lSql = lSql & "  , :Pinvoice_amount"
  '      lSql = lSql & "  , :Pinvoice_date"
  '      lSql = lSql & "  , :Pinvoice_type_lookup_code"
  '      lSql = lSql & "  , :Pgl_date"
  '      lSql = lSql & "  , :Pexchange_rate_type"
  '      lSql = lSql & "  , :Pexchange_rate"
  '      lSql = lSql & "  , :Pexchange_date"
  '      lSql = lSql & "  , :Pglobal_attribute_category"
  '      lSql = lSql & "  , :Pglobal_attribute19"
  '      lSql = lSql & "  , :Psource"
  '      lSql = lSql & "  , :Psegment1_cta_pasvo"
  '      lSql = lSql & "  , :Psegment2_cta_pasvo"
  '      lSql = lSql & "  , :Psegment3_cta_pasvo"
  '      lSql = lSql & "  , :Psegment4_cta_pasvo"
  '      lSql = lSql & "  , :Psegment5_cta_pasvo"
  '      lSql = lSql & "  , :Psegment6_cta_pasvo"
  '      lSql = lSql & "  , :Psegment7_cta_pasvo"
  '      lSql = lSql & "  , :Psegment8_cta_pasvo"
  '      lSql = lSql & "  , :Pattribute_category"
  '      lSql = lSql & "  , :Pattribute1"
  '      lSql = lSql & "  , :Pline_number"
  '      lSql = lSql & "  , :Pline_type_lookup_code"
  '      lSql = lSql & "  , :Pamount"
  '      lSql = lSql & "  , :Paccounting_date"
  '      lSql = lSql & "  , :Psegment1_cta_item"
  '      lSql = lSql & "  , :Psegment2_cta_item"
  '      lSql = lSql & "  , :Psegment3_cta_item"
  '      lSql = lSql & "  , :Psegment4_cta_item"
  '      lSql = lSql & "  , :Psegment5_cta_item"
  '      lSql = lSql & "  , :Psegment6_cta_item"
  '      lSql = lSql & "  , :Psegment7_cta_item"
  '      lSql = lSql & "  , :Psegment8_cta_item"
  '      lSql = lSql & "  , :Ptax_code"
  '      lSql = lSql & "  , :Pawt_group_name"
  '      lSql = lSql & "  , :Porg_id"
  '      lSql = lSql & "  , :Pattribute2"
  '      lSql = lSql & "  , :Pdescerror"
  '      lSql = lSql & "   );"
  '      lSql = lSql & "END;"

  '      lcConexion.OraDatabase.LastServerErrReset()
  '      Call lcConexion.OraDatabase.dbexecutesql(lSql)

  '      strDescError = lcConexion.OraDatabase.Parameters("PDescError").Value

  '      If Not strDescError = "OK" Then
  '        Exit For
  '      End If
  '    Next

  '    If strDescError = "OK" Then
  '      lcConexion.CommitTransaccion()
  '    Else
  '      lcConexion.RollbackTransaccion()
  '    End If

  '  Catch ex As Exception
  '    strDescError = "Problema en el guardar en Tesorería." & vbCr & ex.Message
  '    lcConexion.RollbackTransaccion()
  '  Finally
  '    lcConexion.Cerrar()
  '  End Try

  '  GuardarAP = strDescError
  'End Function
#End Region

#Region "8====== CONEXION CON ODP ORACLE =======3~X_O"

  Private Function GuardarAR(ByRef dtTabla As DataTable) As String
    Dim lcConexion As New ClsConexionOracle
    Dim lcOracleCommand As New Oracle.DataAccess.Client.OracleCommand
    Dim lcMiTransaccion As OracleTransaction = Nothing
    '---------------------------------------------
    Dim ldrRow As DataRow
    '---------------------------------------------
    Dim strDescError As String = ""

    Try
      lcConexion.Abrir()

      lcMiTransaccion = lcConexion.Transaccion

      For Each ldrRow In dtTabla.Rows
        lcOracleCommand = New OracleCommand("Foh_tesorieria.Guardar_ar", lcConexion.Conexion)
        lcOracleCommand.CommandType = CommandType.StoredProcedure

        lcOracleCommand.Parameters.Clear()
        lcOracleCommand.Parameters.Add("PINVOICE_CURRENCY_CODE", OracleDbType.Varchar2, Left(ldrRow("INVOICE_CURRENCY_CODE").ToString, 15), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PCUSTOMER_BILL_NUMBER", OracleDbType.Varchar2, Left(ldrRow("CUSTOMER_BILL_NUMBER").ToString, 30), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PBILL_ADDRESS_LINE_1", OracleDbType.Varchar2, Left(ldrRow("BILL_ADDRESS_LINE_1").ToString, 240), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PBATCH_SOURCE_NAME", OracleDbType.Varchar2, Left(ldrRow("BATCH_SOURCE_NAME").ToString, 50), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("CUST_TRX_TYPE_NAME", OracleDbType.Varchar2, Left(ldrRow("CUST_TRX_TYPE_NAME").ToString, 20), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PTERMS_NAME", OracleDbType.Varchar2, Left(ldrRow("TERMS_NAME").ToString, 30), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PTRX_NUMBER", OracleDbType.Varchar2, Left(ldrRow("TRX_NUMBER").ToString, 20), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PTRX_DATE", OracleDbType.Char, ldrRow("TRX_DATE"), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PTOTAL_AMOUNT", OracleDbType.Double, ldrRow("TOTAL_AMOUNT"), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PDUE_DATE", OracleDbType.Char, ldrRow("DUE_DATE"), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PGL_DATE", OracleDbType.Char, ldrRow("GL_DATE"), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PCONVERSION_TYPE", OracleDbType.Varchar2, Left(ldrRow("CONVERSION_TYPE").ToString, 30), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PCONVERSION_RATE", OracleDbType.Double, ldrRow("CONVERSION_RATE"), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PCONVERSION_DATE", OracleDbType.Char, ldrRow("CONVERSION_DATE"), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PATRIBBUTE1", OracleDbType.Varchar2, Left(ldrRow("ATTRIBUTE1").ToString, 150), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PSEGMENT1_CTA_ACTIVO", OracleDbType.Varchar2, Left(ldrRow("SEGMENT1_CTA_ACTIVO").ToString, 3), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PSEGMENT2_CTA_ACTIVO", OracleDbType.Varchar2, Left(ldrRow("SEGMENT2_CTA_ACTIVO").ToString, 3), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PSEGMENT3_CTA_ACTIVO", OracleDbType.Varchar2, Left(ldrRow("SEGMENT3_CTA_ACTIVO").ToString, 4), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PSEGMENT4_CTA_ACTIVO", OracleDbType.Varchar2, Left(ldrRow("SEGMENT4_CTA_ACTIVO").ToString, 3), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PSEGMENT5_CTA_ACTIVO", OracleDbType.Varchar2, Left(ldrRow("SEGMENT5_CTA_ACTIVO").ToString, 3), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PSEGMENT6_CTA_ACTIVO", OracleDbType.Varchar2, Left(ldrRow("SEGMENT6_CTA_ACTIVO").ToString, 3), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PSEGMENT7_CTA_ACTIVO", OracleDbType.Varchar2, Left(ldrRow("SEGMENT7_CTA_ACTIVO").ToString, 4), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PSEGMENT8_CTA_ACTIVO", OracleDbType.Varchar2, Left(ldrRow("SEGMENT8_CTA_ACTIVO").ToString, 3), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PLINE_NUMBER", OracleDbType.Double, ldrRow("LINE_NUMBER"), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PLINE_TYPE", OracleDbType.Char, Left(ldrRow("LINE_TYPE").ToString, 25), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PQUANTITY", OracleDbType.Double, ldrRow("QUANTITY"), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PUNIT_SELLING_PRICE", OracleDbType.Double, ldrRow("UNIT_SELLING_PRICE"), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PLINE_AMOUNT", OracleDbType.Double, ldrRow("LINE_AMOUNT"), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PDESCRIPTION", OracleDbType.Varchar2, Left(ldrRow("DESCRIPTION").ToString, 240), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PSEGMENT1_CTA_ITEM", OracleDbType.Varchar2, Left(ldrRow("SEGMENT1_CTA_ITEM").ToString, 3), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PSEGMENT2_CTA_ITEM", OracleDbType.Varchar2, Left(ldrRow("SEGMENT2_CTA_ITEM").ToString, 3), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PSEGMENT3_CTA_ITEM", OracleDbType.Varchar2, Left(ldrRow("SEGMENT3_CTA_ITEM").ToString, 4), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PSEGMENT4_CTA_ITEM", OracleDbType.Varchar2, Left(ldrRow("SEGMENT4_CTA_ITEM").ToString, 3), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PSEGMENT5_CTA_ITEM", OracleDbType.Varchar2, Left(ldrRow("SEGMENT5_CTA_ITEM").ToString, 3), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PSEGMENT6_CTA_ITEM", OracleDbType.Varchar2, Left(ldrRow("SEGMENT6_CTA_ITEM").ToString, 3), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PSEGMENT7_CTA_ITEM", OracleDbType.Varchar2, Left(ldrRow("SEGMENT7_CTA_ITEM").ToString, 4), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("PSEGMENT8_CTA_ITEM", OracleDbType.Varchar2, Left(ldrRow("SEGMENT8_CTA_ITEM").ToString, 3), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("pORG_ID", OracleDbType.Double, ldrRow("ORG_ID"), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("pCREATED_BY", OracleDbType.Varchar2, ldrRow("CREATED_BY").ToString, ParameterDirection.Input)

        Dim ParametroSal As New OracleParameter("PDescError", OracleDbType.Varchar2, 2000)
        ParametroSal.Direction = Data.ParameterDirection.Output
        lcOracleCommand.Parameters.Add(ParametroSal)

        lcOracleCommand.ExecuteNonQuery()

        strDescError = lcOracleCommand.Parameters("PDescError").Value.ToString.Trim

        If Not strDescError = "OK" Then
          Exit For
        End If
      Next

      If strDescError = "OK" Then
        lcMiTransaccion.Commit()
      Else
        lcMiTransaccion.Rollback()
      End If

    Catch ex As Exception
      strDescError = "Problema en el guardar en Tesorería." & vbCr & ex.Message
      If Not IsNothing(lcMiTransaccion) Then
        lcMiTransaccion.Rollback()
      End If
    Finally
      lcConexion.Cerrar()
    End Try

    GuardarAR = strDescError
  End Function


  Private Function GuardarAP(ByRef dtTabla As DataTable) As String
    Dim lcConexion As New ClsConexionOracle
    Dim lcOracleCommand As New Oracle.DataAccess.Client.OracleCommand
    Dim lcMiTransaccion As OracleTransaction = Nothing
    '---------------------------------------------
    Dim ldrRow As DataRow
    '---------------------------------------------
    Dim strDescError As String = ""

    Try
      lcConexion.Abrir()

      lcMiTransaccion = lcConexion.Transaccion

      For Each ldrRow In dtTabla.Rows
        lcOracleCommand = New OracleCommand("Foh_tesorieria.Guardar_ap", lcConexion.Conexion)
        lcOracleCommand.CommandType = CommandType.StoredProcedure

        lcOracleCommand.Parameters.Clear()
        lcOracleCommand.Parameters.Add("INVOICE_CURRENCY_CODE", OracleDbType.Varchar2, Left(ldrRow("INVOICE_CURRENCY_CODE").ToString, 15), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("VENDOR_NUM", OracleDbType.Varchar2, Left(ldrRow("VENDOR_NUM").ToString, 30), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("VENDOR_STE_CODE", OracleDbType.Varchar2, Left(ldrRow("VENDOR_STE_CODE").ToString, 15), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("TERMS_NAME", OracleDbType.Varchar2, Left(ldrRow("TERMS_NAME").ToString, 30), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("DESCRIPTION", OracleDbType.Varchar2, Left(ldrRow("DESCRIPTION").ToString, 240), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("INVOICE_NUM", OracleDbType.Varchar2, Left(ldrRow("INVOICE_NUM").ToString, 50), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("INVOICE_AMOUNT", OracleDbType.Double, ldrRow("INVOICE_AMOUNT"), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("INVOICE_DATE", OracleDbType.Char, ldrRow("INVOICE_DATE"), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("INVOICE_TYPE_LOOKUP_CODE", OracleDbType.Varchar2, Left(ldrRow("INVOICE_TYPE_LOOKUP_CODE").ToString, 25), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("GL_DATE", OracleDbType.Char, ldrRow("GL_DATE"), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("EXCHANGE_RATE_TYPE", OracleDbType.Varchar2, Left(ldrRow("EXCHANGE_RATE_TYPE").ToString, 30), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("EXCHANGE_RATE", OracleDbType.Double, ldrRow("EXCHANGE_RATE"), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("EXCHANGE_DATE", OracleDbType.Char, ldrRow("EXCHANGE_DATE"), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("GLOBAL_ATTRIBUTE_CATEGORY", OracleDbType.Varchar2, Left(ldrRow("GLOBAL_ATTRIBUTE_CATEGORY").ToString, 150), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("GLOBAL_ATTRIBUTE19", OracleDbType.Varchar2, Left(ldrRow("GLOBAL_ATTRIBUTE19").ToString, 150), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("SOURCE", OracleDbType.Varchar2, Left(ldrRow("SOURCE").ToString, 80), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("SEGMENT1_CTA_PASVO", OracleDbType.Varchar2, Left(ldrRow("SEGMENT1_CTA_PASVO").ToString, 3), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("SEGMENT2_CTA_PASVO", OracleDbType.Varchar2, Left(ldrRow("SEGMENT2_CTA_PASVO").ToString, 3), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("SEGMENT3_CTA_PASVO", OracleDbType.Varchar2, Left(ldrRow("SEGMENT3_CTA_PASVO").ToString, 4), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("SEGMENT4_CTA_PASVO", OracleDbType.Varchar2, Left(ldrRow("SEGMENT4_CTA_PASVO").ToString, 3), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("SEGMENT5_CTA_PASVO", OracleDbType.Varchar2, Left(ldrRow("SEGMENT5_CTA_PASVO").ToString, 3), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("SEGMENT6_CTA_PASVO", OracleDbType.Varchar2, Left(ldrRow("SEGMENT6_CTA_PASVO").ToString, 3), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("SEGMENT7_CTA_PASVO", OracleDbType.Varchar2, Left(ldrRow("SEGMENT7_CTA_PASVO").ToString, 4), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("SEGMENT8_CTA_PASVO", OracleDbType.Varchar2, Left(ldrRow("SEGMENT8_CTA_PASVO").ToString, 3), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("ATTRIBUTE_CATEGORY", OracleDbType.Varchar2, Left(ldrRow("ATTRIBUTE_CATEGORY").ToString, 150), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("ATTRIBUTE1", OracleDbType.Double, ldrRow("ATTRIBUTE1"), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("LINE_NUMBER", OracleDbType.Double, ldrRow("LINE_NUMBER"), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("LINE_TYPE_LOOKUP_CODE", OracleDbType.Varchar2, Left(ldrRow("LINE_TYPE_LOOKUP_CODE").ToString, 25), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("AMOUNT", OracleDbType.Double, ldrRow("AMOUNT"), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("ACCOUNTING_DATE", OracleDbType.Char, ldrRow("ACCOUNTING_DATE"), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("SEGMENT1_CTA_ITEM", OracleDbType.Varchar2, Left(ldrRow("SEGMENT1_CTA_ITEM").ToString, 3), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("SEGMENT2_CTA_ITEM", OracleDbType.Varchar2, Left(ldrRow("SEGMENT2_CTA_ITEM").ToString, 3), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("SEGMENT3_CTA_ITEM", OracleDbType.Varchar2, Left(ldrRow("SEGMENT3_CTA_ITEM").ToString, 4), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("SEGMENT4_CTA_ITEM", OracleDbType.Varchar2, Left(ldrRow("SEGMENT4_CTA_ITEM").ToString, 3), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("SEGMENT5_CTA_ITEM", OracleDbType.Varchar2, Left(ldrRow("SEGMENT5_CTA_ITEM").ToString, 3), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("SEGMENT6_CTA_ITEM", OracleDbType.Varchar2, Left(ldrRow("SEGMENT6_CTA_ITEM").ToString, 3), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("SEGMENT7_CTA_ITEM", OracleDbType.Varchar2, Left(ldrRow("SEGMENT7_CTA_ITEM").ToString, 4), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("SEGMENT8_CTA_ITEM", OracleDbType.Varchar2, Left(ldrRow("SEGMENT8_CTA_ITEM").ToString, 3), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("TAX_CODE", OracleDbType.Varchar2, Left(ldrRow("TAX_CODE").ToString, 15), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("AWT_GROUP_NAME", OracleDbType.Varchar2, Left(ldrRow("AWT_GROUP_NAME").ToString, 25), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("ORG_ID", OracleDbType.Double, ldrRow("ORG_ID"), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("ATTRIBUTE2", OracleDbType.Varchar2, Left(ldrRow("ATTRIBUTE2").ToString, 1), ParameterDirection.Input)
        lcOracleCommand.Parameters.Add("CREATED_BY", OracleDbType.Varchar2, Left(ldrRow("CREATED_BY").ToString, 10), ParameterDirection.Input)

        Dim ParametroSal As New OracleParameter("PDescError", OracleDbType.Varchar2, 2000)
        ParametroSal.Direction = Data.ParameterDirection.Output
        lcOracleCommand.Parameters.Add(ParametroSal)

        lcOracleCommand.ExecuteNonQuery()

        strDescError = lcOracleCommand.Parameters("PDescError").Value.ToString.Trim

        If Not strDescError = "OK" Then
          Exit For
        End If
      Next

      If strDescError = "OK" Then
        lcMiTransaccion.Commit()
      Else
        lcMiTransaccion.Rollback()
      End If

    Catch ex As Exception
      strDescError = "Problema en el guardar en Tesorería." & vbCr & ex.Message
      If Not IsNothing(lcMiTransaccion) Then
        lcMiTransaccion.Rollback()
      End If
    Finally
      lcConexion.Cerrar()
    End Try

    GuardarAP = strDescError
  End Function
#End Region

  Public Function InformarEvento(ByVal intIdMovSistema As Integer ) As String
    Dim strDescError As String = ""
    Dim dsTabla As New DataSet

    Try
      dsTabla = New DataSet
      strDescError = LeerEventos(intIdMovSistema:=intIdMovSistema, dsTabla:=dsTabla)
    Catch ex As Exception
      strDescError = "Problema en el leer la operación '" & intIdMovSistema & "'." & vbCr & ex.Message
    End Try

    If strDescError = "OK" Then
      If dsTabla.Tables("TABLA").Rows.Count > 0 Then
        Select Case dsTabla.Tables("TABLA").Rows(0)("TIPO_MVTO").ToString.Trim.ToUpper
          Case "E", "A"
            Try
              strDescError = GuardarAR(dtTabla:=dsTabla.Tables("TABLA"))

              If Not strDescError = "OK" Then
                strDescError = "Código '" & intIdMovSistema.ToString & "': " & strDescError
              End If
            Catch ex As Exception
              strDescError = "Problema en el envio del evento código '" & intIdMovSistema & "'." & vbCr & ex.Message
            End Try
          Case "I", "C"
            Try
              strDescError = GuardarAP(dtTabla:=dsTabla.Tables("TABLA"))

              If Not strDescError = "OK" Then
                strDescError = "Código '" & intIdMovSistema.ToString & "': " & strDescError
              End If
            Catch ex As Exception
              strDescError = "Problema en el guardar la operación '" & intIdMovSistema & "'." & vbCr & ex.Message
            End Try
          Case Else
            strDescError = "El código tipo de movimiento """ & dsTabla.Tables("TABLA").Rows(0)("TIPO_MVTO").ToString.Trim.ToUpper & """ no es valido."
        End Select
      End If
    End If

    Return strDescError
  End Function

  Private Function LeerEventos(ByVal intIdMovSistema As Integer _
                             , ByRef dsTabla As DataSet) As String
    '---------------------------------------------------------
    Dim SQLCommand As New SqlClient.SqlCommand
    Dim SQLConnect As New Cls_Conexion
    Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
    '---------------------------------------------------------
    Dim lstrProcedimiento As String = ""
    Dim strDescError As String = ""
    '---------------------------------------------------------

    '...Abre la conexion
    SQLConnect.Abrir()
    lstrProcedimiento = "FOH_TESORERIA_LEER_EVENTO"

    Try
      SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, SQLConnect.Conexion)
      SQLCommand.CommandType = CommandType.StoredProcedure
      SQLCommand.CommandText = lstrProcedimiento
      SQLCommand.Parameters.Clear()

      '( @PID_OPERACION NUMERIC
      ', @PRESULTADO VARCHAR(200) OUTPUT
      SQLCommand.Parameters.Add("PID_MOV_SISTEMA", SqlDbType.Float, 3).Value = intIdMovSistema
      SQLCommand.Parameters.Add("PID_USUARIO", SqlDbType.Float, 3).Value = glngIdUsuario

      '...Resultado
      Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
      ParametroSal2.Direction = Data.ParameterDirection.Output
      SQLCommand.Parameters.Add(ParametroSal2)

      SQLCommand.ExecuteNonQuery()

      strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

      If strDescError = "OK" Then
        SQLDataAdapter.SelectCommand = SQLCommand
        SQLDataAdapter.Fill(dsTabla, "TABLA")
      End If

    Catch Ex As Exception
      strDescError = "Problema en el leer la operación '" & intIdMovSistema & "'." & vbCr & Ex.Message
    Finally
      SQLConnect.Cerrar()
    End Try

    Return strDescError
  End Function

End Class

