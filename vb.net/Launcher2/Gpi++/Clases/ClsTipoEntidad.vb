﻿Public Class ClsTipoEntidad
    Public Function BuscarTiposEntidades(ByVal strEstadoEntidad As String, _
                                         ByVal strColumnas As String, _
                                         ByRef strRetorno As String) As DataSet

        Dim LDS_TipoEntidad As New DataSet
        Dim LDS_TipoEntidad_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_TipoEntidad_Consultar"
        Lstr_NombreTabla = "TipoEntidad"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("pEstado", SqlDbType.VarChar, 3).Value = IIf(strEstadoEntidad.Trim = "", DBNull.Value, strEstadoEntidad.Trim)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_TipoEntidad, Lstr_NombreTabla)

            LDS_TipoEntidad_Aux = LDS_TipoEntidad

            If strColumnas.Trim = "" Then
                LDS_TipoEntidad_Aux = LDS_TipoEntidad
            Else
                LDS_TipoEntidad_Aux = LDS_TipoEntidad.Copy
                For Each Columna In LDS_TipoEntidad.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_TipoEntidad_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_TipoEntidad_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next


            strRetorno = "OK"
            LDS_TipoEntidad.Dispose()

            Return LDS_TipoEntidad_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function
    Public Function Tipo_Entidad_Mantenedor(ByVal strAccion As String, _
                                            ByVal strCodigo As String, _
                                            ByVal strDescripcion As String, _
                                            ByRef strEstado As String, _
                                            Optional ByVal intEli_Fis As Integer = 0) As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_Tipo_Entidad_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Tipo_Entidad_Mantencion"
            SQLCommand.Parameters.Clear()

            '@pAccion varchar(10)
            '@pCod_Tipo_Entidad varchar(3), 
            '@pDsc_Tipo_Entidad varchar(20), 
            '@pFlg_Es_Tipo_Entidad_Pago varchar(1), 
            '@pDecimales_mostrar numeric(5, 0), 
            '@pSimbolo varchar(3), 
            '@pEstado char(3), 
            '@pIdTipo_Entidad int=NULL OUTPUT, 
            '@pResultado varchar(200) OUTPUT)

            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 10).Value = strAccion
            SQLCommand.Parameters.Add("pCod_Tipo_Entidad", SqlDbType.VarChar, 3).Value = strCodigo
            SQLCommand.Parameters.Add("pDsc_Tipo_Entidad", SqlDbType.VarChar, 50).Value = strDescripcion
            SQLCommand.Parameters.Add("pEstado", SqlDbType.VarChar, 3).Value = strEstado
            SQLCommand.Parameters.Add("pEli_F", SqlDbType.Int, 1).Value = intEli_Fis

            ''...(Identity)
            'Dim ParametroSal1 As New SqlClient.SqlParameter("pIdTipo_Entidad", SqlDbType.Float, 10)
            'ParametroSal1.Direction = Data.ParameterDirection.Output
            'SQLCommand.Parameters.Add(ParametroSal1)

            '...Resultado
            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            Tipo_Entidad_Mantenedor = strDescError
        End Try
    End Function
End Class
