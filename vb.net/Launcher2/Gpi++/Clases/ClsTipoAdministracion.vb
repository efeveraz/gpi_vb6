﻿Public Class ClsTipoAdministracion
    Public Function TipoAdministracion_Ver(ByVal strCodTipoAdministracion As String, _
                                           ByVal strDscTipoAdministracion As String, _
                                           ByVal strColumnas As String, _
                                           ByRef strRetorno As String, _
                                           Optional ByVal strEstado As String = "VIG") As DataSet

        Dim LDS_TipoAdministracion As New DataSet
        Dim LDS_TipoAdministracion_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_TipoAdministracion_Consultar"
        Lstr_NombreTabla = "TipoAdministracion"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pCodTipoAdministracion", SqlDbType.VarChar, 5).Value = IIf(strCodTipoAdministracion = "", DBNull.Value, strCodTipoAdministracion)
            SQLCommand.Parameters.Add("pDscTipoAdministracion", SqlDbType.VarChar, 20).Value = IIf(strDscTipoAdministracion = "", DBNull.Value, strDscTipoAdministracion)
            SQLCommand.Parameters.Add("pEST_TIPO_ADMINISTRACION", SqlDbType.Char, 3).Value = IIf(strEstado = "", DBNull.Value, strEstado)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_TipoAdministracion, Lstr_NombreTabla)

            LDS_TipoAdministracion_Aux = LDS_TipoAdministracion

            If strColumnas.Trim = "" Then
                LDS_TipoAdministracion_Aux = LDS_TipoAdministracion
            Else
                LDS_TipoAdministracion_Aux = LDS_TipoAdministracion.Copy
                For Each Columna In LDS_TipoAdministracion.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_TipoAdministracion_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_TipoAdministracion_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_TipoAdministracion.Dispose()
            Return LDS_TipoAdministracion_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function
    Public Function RetornaTipoAdministracion(ByRef strRetorno As String, _
                                              ByVal strColumnas As String, _
                                              Optional ByVal strCodTipoAdministracion As String = "", _
                                              Optional ByVal strDscTipoAdministracion As String = "") As DataSet
        Dim lblnRemove As Boolean = True
        Dim larr_NombreColumna() As String = Split(strColumnas, ",")
        Dim lInt_Col As Integer = 0
        Dim lInt_NomCol As String = ""

        Dim lDS_TipoAdministracion As New DataSet
        Dim lDS_TipoAdministracion_Aux As New DataSet

        Dim Sqlcommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim lstrProcedimiento As String = ""
        Dim LstrNombreTabla As String = ""
        lstrProcedimiento = "Sis_TipoAdministracion_Ver"
        LstrNombreTabla = "TipoAdministracion"
        Connect.Abrir()

        Try

            Sqlcommand = New SqlClient.SqlCommand(lstrProcedimiento, Connect.Conexion)

            Sqlcommand.CommandType = CommandType.StoredProcedure
            Sqlcommand.CommandText = lstrProcedimiento
            Sqlcommand.Parameters.Clear()

            Sqlcommand.Parameters.Add("@pCodTipoAdministracion", SqlDbType.VarChar, 5).Value = IIf(strCodTipoAdministracion = "", DBNull.Value, strCodTipoAdministracion)
            Sqlcommand.Parameters.Add("@pDscTipoAdministracion", SqlDbType.VarChar, 20).Value = IIf(strDscTipoAdministracion = "", DBNull.Value, strDscTipoAdministracion)

            Dim oParamSal As New SqlClient.SqlParameter("@pMsjRetorno", SqlDbType.Char, 60)
            oParamSal.Direction = ParameterDirection.Output
            Sqlcommand.Parameters.Add(oParamSal)


            SQLDataAdapter.SelectCommand = Sqlcommand
            SQLDataAdapter.Fill(lDS_TipoAdministracion)


            If strColumnas.Trim = "" Then
                lDS_TipoAdministracion_Aux = lDS_TipoAdministracion
            Else
                lDS_TipoAdministracion_Aux = lDS_TipoAdministracion.Copy
                For Each lstrColumna As DataColumn In lDS_TipoAdministracion.Tables(LstrNombreTabla).Columns
                    lblnRemove = True
                    For lInt_Col = 0 To larr_NombreColumna.Length - 1
                        lInt_NomCol = larr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                        If lstrColumna.ColumnName = lInt_NomCol Then
                            lblnRemove = False
                        End If
                    Next
                    If lblnRemove Then
                        lDS_TipoAdministracion_Aux.Tables(LstrNombreTabla).Columns.Remove(lstrColumna.ColumnName)
                    End If
                Next
            End If

            For lInt_Col = 0 To larr_NombreColumna.Length - 1
                lInt_NomCol = larr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                For Each lstrColumna As DataColumn In lDS_TipoAdministracion_Aux.Tables(LstrNombreTabla).Columns
                    If lstrColumna.ColumnName = lInt_NomCol Then
                        lstrColumna.SetOrdinal(lInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = Sqlcommand.Parameters("@pMsjRetorno").Value.ToString.Trim

            Return lDS_TipoAdministracion_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function TipoAdministracion_Consultar(ByVal strCodTipoAdministracion As String, _
                                                 ByVal strColumnas As String, _
                                                 ByVal strEstado As String, _
                                                 ByRef strRetorno As String) As DataSet


        Dim LDS_TipoAdministracion As New DataSet
        Dim LDS_TipoAdministracion_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_TipoAdministracion_Ver"
        Lstr_NombreTabla = "TIPO_ADMINISTRACION"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("pCodTipoAdministracion", SqlDbType.VarChar, 5).Value = IIf(strCodTipoAdministracion.Trim = "", DBNull.Value, strCodTipoAdministracion.Trim)
            SQLCommand.Parameters.Add("pEstado", SqlDbType.VarChar, 3).Value = IIf(strEstado.Trim = "", DBNull.Value, strEstado.Trim)
            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_TipoAdministracion, Lstr_NombreTabla)

            LDS_TipoAdministracion_Aux = LDS_TipoAdministracion

            If strColumnas.Trim = "" Then
                LDS_TipoAdministracion_Aux = LDS_TipoAdministracion
            Else
                LDS_TipoAdministracion_Aux = LDS_TipoAdministracion.Copy
                For Each Columna In LDS_TipoAdministracion.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_TipoAdministracion_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_TipoAdministracion_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_TipoAdministracion.Dispose()
            Return LDS_TipoAdministracion_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function TipoAdministrador_Mantenedor(ByVal strAccion As String, _
                                                 ByVal strDescripcion As String, _
                                                 ByVal strIdTipoAdministracion As String, _
                                                 ByRef strEstado As String) As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_TipoAdministracion_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_TipoAdministracion_Mantencion"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 10).Value = strAccion
            SQLCommand.Parameters.Add("pDscTipoAdministracion", SqlDbType.VarChar, 60).Value = strDescripcion
            SQLCommand.Parameters.Add("pIdTipoAdministracion", SqlDbType.VarChar, 5).Value = strIdTipoAdministracion
            SQLCommand.Parameters.Add("pEstado", SqlDbType.Char, 3).Value = strEstado

            '...Resultado
            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            TipoAdministrador_Mantenedor = strDescError
        End Try
    End Function
End Class
