﻿Imports System.Data

Public Class ClsOperacion

    Public Function Operaciones_Ver(ByVal dblpIdOpeacion As Double, _
                                    ByVal strpFechaDesde As String, _
                                    ByVal strpFechaHasta As String, _
                                    ByVal strpColumnas As String, _
                                    ByRef strpRetorno As String, _
                                    Optional ByRef strCodEstado As String = "", _
                                    Optional ByVal StrListaCuentas As String = "", _
                                    Optional ByRef StrTipoReporte As String = "", _
                                    Optional ByVal StrNemotecnico As String = "") As DataSet

        Dim lDS_Operaciones As New DataSet
        Dim lDS_Operaciones_Aux As New DataSet
        Dim larr_NombreColumna() As String = Split(strpColumnas, ",")
        Dim lInt_Col As Integer = 0
        Dim lInt_NomCol As String = ""
        Dim lstr_NombreTabla As String = ""
        Dim lstrColumna As DataColumn
        Dim lblnRemove As Boolean = True
        Dim lstrProcedimiento As String = ""
        Dim strDescError As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        lstrProcedimiento = "Rcp_Operacion_Consultar2"
        lstr_NombreTabla = "OPERACIONES"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, Connect.Conexion)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandTimeout = 0
            SQLCommand.CommandText = lstrProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("pIdOpeacion", SqlDbType.Float, 10).Value = IIf(dblpIdOpeacion = 0, DBNull.Value, dblpIdOpeacion)
            SQLCommand.Parameters.Add("pFechaDesde", SqlDbType.Char, 10).Value = strpFechaDesde
            SQLCommand.Parameters.Add("pFechaHasta", SqlDbType.Char, 10).Value = strpFechaHasta
            SQLCommand.Parameters.Add("pCodEstado", SqlDbType.Char, 1).Value = IIf(strCodEstado = "", DBNull.Value, strCodEstado)
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Float, 10).Value = glngNegocioOperacion
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Float, 6).Value = IIf(glngIdUsuario = 0, DBNull.Value, glngIdUsuario)
            SQLCommand.Parameters.Add("pListaCuentas", SqlDbType.Text, 60000).Value = StrListaCuentas
            SQLCommand.Parameters.Add("pTipoReporte", SqlDbType.Char, 1).Value = IIf(StrTipoReporte = "", DBNull.Value, StrTipoReporte)
            SQLCommand.Parameters.Add("pNemotecnico", SqlDbType.VarChar, 25).Value = IIf(StrNemotecnico = "", DBNull.Value, StrNemotecnico)

            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)



            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(lDS_Operaciones, lstr_NombreTabla)
            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            lDS_Operaciones_Aux = lDS_Operaciones

            If strDescError.ToUpper.Trim <> "OK" Then
                gFun_ResultadoMsg("2|" & strDescError, "Buscar Cuentas")
                strpRetorno = "OK"
                lDS_Operaciones_Aux = Nothing
            Else
                If strpColumnas.Trim = "" Then
                    lDS_Operaciones_Aux = lDS_Operaciones
                Else
                    lDS_Operaciones_Aux = lDS_Operaciones.Copy
                    For Each lstrColumna In lDS_Operaciones.Tables(lstr_NombreTabla).Columns
                        lblnRemove = True
                        For lInt_Col = 0 To larr_NombreColumna.Length - 1
                            lInt_NomCol = larr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                            If lstrColumna.ColumnName = lInt_NomCol Then
                                lblnRemove = False
                            End If
                        Next
                        If lblnRemove Then
                            lDS_Operaciones_Aux.Tables(lstr_NombreTabla).Columns.Remove(lstrColumna.ColumnName)
                        End If
                    Next
                End If

                For lInt_Col = 0 To larr_NombreColumna.Length - 1
                    lInt_NomCol = larr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                    For Each lstrColumna In lDS_Operaciones_Aux.Tables(lstr_NombreTabla).Columns
                        If lstrColumna.ColumnName = lInt_NomCol Then
                            lstrColumna.SetOrdinal(lInt_Col)
                            Exit For
                        End If
                    Next
                Next

                strpRetorno = "OK"
            End If
            lDS_Operaciones.Dispose()
            Return lDS_Operaciones_Aux

        Catch ex As Exception
            strpRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function ConceptoOperaciones_Ver(ByVal strCodTipoOperacion As String, _
                                            ByVal strClaseInstrumento As String, _
                                            ByVal strFamiliaInstrumento As String, _
                                            ByVal strColumnas As String, _
                                            ByRef strRetorno As String, _
                                            Optional ByVal strFlg_Primera_Emision As String = "", _
                                            Optional ByVal strDescripcionConcepto As String = "") As DataSet

        Dim lDS_ConceptoOperaciones As New DataSet
        Dim lDS_ConceptoOperaciones_Aux As New DataSet
        Dim larr_NombreColumna() As String = Split(strColumnas, ",")
        Dim lInt_Col As Integer = 0
        Dim lInt_NomCol As String = ""
        Dim lstr_NombreTabla As String = ""
        Dim lstrColumna As DataColumn
        Dim lblnRemove As Boolean = True
        Dim lstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        lstrProcedimiento = "Rcp_Operacion_BuscarConceptos"
        lstr_NombreTabla = "CONCEPTO_OPERACION"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = lstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pCodTipoOperacion", SqlDbType.VarChar, 10).Value = strCodTipoOperacion
            SQLCommand.Parameters.Add("pClaseInstrumento", SqlDbType.Char, 15).Value = Trim(strClaseInstrumento)
            SQLCommand.Parameters.Add("pFamiliaInstrumento", SqlDbType.Char, 10).Value = Trim(strFamiliaInstrumento)
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Float, 10).Value = glngNegocioOperacion
            SQLCommand.Parameters.Add("pFlg_Primera_Emision", SqlDbType.Char, 1).Value = IIf(strFlg_Primera_Emision = "", DBNull.Value, strFlg_Primera_Emision)
            SQLCommand.Parameters.Add("pDescripcionConcepto", SqlDbType.VarChar, 100).Value = IIf(strDescripcionConcepto = "", DBNull.Value, strDescripcionConcepto)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(lDS_ConceptoOperaciones, lstr_NombreTabla)

            lDS_ConceptoOperaciones_Aux = lDS_ConceptoOperaciones

            If strColumnas.Trim = "" Then
                lDS_ConceptoOperaciones_Aux = lDS_ConceptoOperaciones
            Else
                lDS_ConceptoOperaciones_Aux = lDS_ConceptoOperaciones.Copy
                For Each lstrColumna In lDS_ConceptoOperaciones.Tables(lstr_NombreTabla).Columns
                    lblnRemove = True
                    For lInt_Col = 0 To larr_NombreColumna.Length - 1
                        lInt_NomCol = larr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                        If lstrColumna.ColumnName = lInt_NomCol Then
                            lblnRemove = False
                        End If
                    Next
                    If lblnRemove Then
                        lDS_ConceptoOperaciones_Aux.Tables(lstr_NombreTabla).Columns.Remove(lstrColumna.ColumnName)
                    End If
                Next
            End If

            For lInt_Col = 0 To larr_NombreColumna.Length - 1
                lInt_NomCol = larr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                For Each lstrColumna In lDS_ConceptoOperaciones_Aux.Tables(lstr_NombreTabla).Columns
                    If lstrColumna.ColumnName = lInt_NomCol Then
                        lstrColumna.SetOrdinal(lInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            lDS_ConceptoOperaciones.Dispose()
            Return lDS_ConceptoOperaciones_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function Operaciones_BuscarEncabezado(ByVal dblIdOperacion As Double, _
                                    ByVal strColumnas As String, _
                                    ByRef strRetorno As String) As DataSet

        Dim lDS_Operaciones As New DataSet
        Dim lDS_Operaciones_Aux As New DataSet
        Dim larr_NombreColumna() As String = Split(strColumnas, ",")
        Dim lInt_Col As Integer = 0
        Dim lInt_NomCol As String = ""
        Dim lstr_NombreTabla As String = ""
        Dim lstrColumna As DataColumn
        Dim lblnRemove As Boolean = True
        Dim lstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        lstrProcedimiento = "Rcp_Operacion_BuscarEncabezado"
        lstr_NombreTabla = "OPERACIONES"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = lstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("pIdOperacion", SqlDbType.Float, 10).Value = dblIdOperacion

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(lDS_Operaciones, lstr_NombreTabla)

            lDS_Operaciones_Aux = lDS_Operaciones

            If strColumnas.Trim = "" Then
                lDS_Operaciones_Aux = lDS_Operaciones
            Else
                lDS_Operaciones_Aux = lDS_Operaciones.Copy
                For Each lstrColumna In lDS_Operaciones.Tables(lstr_NombreTabla).Columns
                    lblnRemove = True
                    For lInt_Col = 0 To larr_NombreColumna.Length - 1
                        lInt_NomCol = larr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                        If lstrColumna.ColumnName = lInt_NomCol Then
                            lblnRemove = False
                        End If
                    Next
                    If lblnRemove Then
                        lDS_Operaciones_Aux.Tables(lstr_NombreTabla).Columns.Remove(lstrColumna.ColumnName)
                    End If
                Next
            End If

            For lInt_Col = 0 To larr_NombreColumna.Length - 1
                lInt_NomCol = larr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                For Each lstrColumna In lDS_Operaciones_Aux.Tables(lstr_NombreTabla).Columns
                    If lstrColumna.ColumnName = lInt_NomCol Then
                        lstrColumna.SetOrdinal(lInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            lDS_Operaciones.Dispose()
            Return lDS_Operaciones_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function Operaciones_BuscarDetalle(ByVal dblIdOperacion As Double, _
                                        ByVal strColumnas As String, _
                                        ByRef strRetorno As String) As DataSet

        Dim lDS_Operaciones As New DataSet
        Dim lDS_Operaciones_Aux As New DataSet
        Dim larr_NombreColumna() As String = Split(strColumnas, ",")
        Dim lInt_Col As Integer = 0
        Dim lInt_NomCol As String = ""
        Dim lstr_NombreTabla As String = ""
        Dim lstrColumna As DataColumn
        Dim lblnRemove As Boolean = True
        Dim lstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        lstrProcedimiento = "Rcp_Operacion_BuscarDetalle"
        lstr_NombreTabla = "OPERACIONES"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = lstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("pIdOperacion", SqlDbType.Float, 10).Value = dblIdOperacion

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(lDS_Operaciones, lstr_NombreTabla)

            lDS_Operaciones_Aux = lDS_Operaciones

            If strColumnas.Trim = "" Then
                lDS_Operaciones_Aux = lDS_Operaciones
            Else
                lDS_Operaciones_Aux = lDS_Operaciones.Copy
                For Each lstrColumna In lDS_Operaciones.Tables(lstr_NombreTabla).Columns
                    lblnRemove = True
                    For lInt_Col = 0 To larr_NombreColumna.Length - 1
                        lInt_NomCol = larr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                        If lstrColumna.ColumnName = lInt_NomCol Then
                            lblnRemove = False
                        End If
                    Next
                    If lblnRemove Then
                        lDS_Operaciones_Aux.Tables(lstr_NombreTabla).Columns.Remove(lstrColumna.ColumnName)
                    End If
                Next
            End If

            For lInt_Col = 0 To larr_NombreColumna.Length - 1
                lInt_NomCol = larr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                For Each lstrColumna In lDS_Operaciones_Aux.Tables(lstr_NombreTabla).Columns
                    If lstrColumna.ColumnName = lInt_NomCol Then
                        lstrColumna.SetOrdinal(lInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            lDS_Operaciones.Dispose()
            Return lDS_Operaciones_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function GuardarSaldosAsignacion(ByVal strAccion As String, _
                                                 ByVal dblIdOperacionDetalle As Double, _
                                                 ByVal dblIdOrdenConsolidada As Double, _
                                                 ByVal dblIdOrdenAsociada As Double, _
                                                 ByVal dblSaldoOperacion As Double, _
                                                 ByVal dblSaldoConsolidada As Double, _
                                                 ByVal dblSaldoOrdenAsociada As Double) As String
        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction


        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_Asignacion_Saldos", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Asignacion_Saldos"
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 20).Value = strAccion.Trim
            SQLCommand.Parameters.Add("pIdOperacionDetalle", SqlDbType.Float, 8).Value = dblIdOperacionDetalle
            SQLCommand.Parameters.Add("pIdOrdenConsolidada", SqlDbType.Float, 10).Value = dblIdOrdenConsolidada
            SQLCommand.Parameters.Add("pIdOrdenAsociada", SqlDbType.Float, 10).Value = dblIdOrdenAsociada
            SQLCommand.Parameters.Add("pSaldoOperacion", SqlDbType.Float, 20).Value = dblSaldoOperacion
            SQLCommand.Parameters.Add("pSaldoConsolidada", SqlDbType.Float, 20).Value = dblSaldoConsolidada
            SQLCommand.Parameters.Add("pSaldoOrdenAsociada", SqlDbType.Float, 20).Value = dblSaldoOrdenAsociada

            '...Resultado
            Dim ParametroSal As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If Trim(strDescError) = "OK" Then
                MiTransaccionSQL.Commit()
                Return ("OK")
            Else
                MiTransaccionSQL.Rollback()
                Return strDescError
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error al grabar saldos " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            GuardarSaldosAsignacion = strDescError
        End Try

    End Function

    Public Function Operaciones_Libro(ByVal strCodOperacionAporte As String, _
                                    ByVal strCodOperacionRescate As String, _
                                    ByVal dblIdCliente As Double, _
                                    ByVal strCodClase As String, _
                                    ByVal strCodSubClase As String, _
                                    ByVal dblIdInstrumento As Double, _
                                    ByVal strFechaDesde As String, _
                                    ByVal strFechaHasta As String, _
                                    ByVal strFiltroConsolidada As String, _
                                    ByRef strRetorno As String) As DataSet

        Dim lDS_Operaciones As New DataSet
        Dim lDS_Operaciones_Aux As New DataSet
        Dim lstr_NombreTabla As String = ""
        Dim lstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        lstrProcedimiento = "Rcp_Operacion_LibroOperaciones"
        lstr_NombreTabla = "LIBRO_OPERACIONES"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = lstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pCodTipoOperacionAporte", SqlDbType.VarChar, 50).Value = IIf(strCodOperacionAporte = "", DBNull.Value, strCodOperacionAporte)
            SQLCommand.Parameters.Add("pCodTipoOperacionRescate", SqlDbType.VarChar, 10).Value = IIf(strCodOperacionRescate = "", DBNull.Value, strCodOperacionRescate)
            SQLCommand.Parameters.Add("pIdCliente", SqlDbType.Float, 10).Value = IIf(dblIdCliente = 0, DBNull.Value, dblIdCliente)
            SQLCommand.Parameters.Add("pCodClaseInstrumento", SqlDbType.VarChar, 15).Value = IIf(strCodClase = "", DBNull.Value, strCodClase)
            SQLCommand.Parameters.Add("pCodSubClaseInstrumento", SqlDbType.VarChar, 15).Value = IIf(strCodSubClase = "", DBNull.Value, strCodSubClase)
            SQLCommand.Parameters.Add("pIdInstrumento", SqlDbType.Float, 10).Value = IIf(dblIdInstrumento = 0, DBNull.Value, dblIdInstrumento)
            SQLCommand.Parameters.Add("pFechaDesde", SqlDbType.Char, 10).Value = strFechaDesde
            SQLCommand.Parameters.Add("pFechaHasta", SqlDbType.Char, 10).Value = strFechaHasta
            SQLCommand.Parameters.Add("pFiltroConsolidada", SqlDbType.Char, 1).Value = strFiltroConsolidada

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(lDS_Operaciones, lstr_NombreTabla)

            lDS_Operaciones_Aux = lDS_Operaciones

            strRetorno = "OK"
            lDS_Operaciones.Dispose()
            Return lDS_Operaciones_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function ValOperacion_Asignada(ByVal pIdOperacionHija As Double) As Boolean
        Dim strDescError As String = ""
        Dim llngCantidad As Long
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion

        '...Abre la conexion
        SQLConnect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_Operacion_Cantidad_Asignadas", SQLConnect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Operacion_Cantidad_Asignadas"
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdOperacionHija", SqlDbType.Float, 10).Value = pIdOperacionHija

            '...pCantidad
            Dim ParametroSal As New SqlClient.SqlParameter("pCantidad", SqlDbType.Float, 10)
            ParametroSal.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal)

            '...Resultado
            Dim ParametroSal1 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal1.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal1)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError = "OK" Then
                llngCantidad = SQLCommand.Parameters("pCantidad").Value
            End If

        Catch Ex As Exception
            strDescError = "|1|Error al obtener la cantidad de ordenes asociadas a una orden consolidada." & vbCr & "[" & Ex.Message & "]|"
        Finally
            If strDescError <> "OK" Then
                If Not gFun_ResultadoMsg(strDescError, "ObtenerCantidadOrden_OrdenConsolidada") Then
                    llngCantidad = 0
                End If
            End If
            SQLConnect.Cerrar()
            ValOperacion_Asignada = IIf(llngCantidad > 0, True, False)
        End Try
    End Function

    Public Function GuardarOperacionesCompleta(ByVal dtbOperacionEncabezado As DataTable, _
                                               ByVal dtbOperacionDetalle As DataTable, _
                                               Optional ByVal dtbMedioPagoCobro As DataTable = Nothing, _
                                               Optional ByVal dtbComision As DataTable = Nothing, _
                                               Optional ByRef dblIdGenerado As Double = 0, _
                                               Optional ByVal intIdCuentadestino As Integer = -1, _
                                               Optional ByVal strTipoOperacionDestino As String = "", _
                                               Optional ByVal dblIdContraparteDestino As Double = 0, _
                                               Optional ByVal strFormaOperarCartera As String = "", _
                                               Optional ByVal strObservacion As String = "", _
                                               Optional ByVal strFlgEnRueda As String = "N", _
                                               Optional ByRef dblIdGeneradoInt As Double = 0) As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction
        Dim lstrDetalle1 As String = ""
        Dim lstrDetalle2 As String = ""
        Dim lstrDetalle3 As String = ""
        Dim lstrComisiones As String = ""
        Dim lstrMediosCobroPago As String = ""
        Dim llngIdOperacion As Double = 0
        Dim lstrColumnasDetalle As String = "ID_INSTRUMENTO_ORIGEN © " & _
                                            "ID_INSTRUMENTO_DESTINO © " & _
                                            "CANTIDAD © " & _
                                            "PRECIO © " & _
                                            "PRECIO_GESTION © " & _
                                            "PLAZO © " & _
                                            "TASA © " & _
                                            "BASE © " & _
                                            "FECHA_VENCIMIENTO © " & _
                                            "VALOR_DIVISA © " & _
                                            "INTERES © " & _
                                            "UTILIDAD © " & _
                                            "MONTO © " & _
                                            "FLG_MONTO_REFERENCIADO © " & _
                                            "FLG_TIPO_DEPOSITO © " & _
                                            "FECHA_VALUTA © " & _
                                            "FLG_VENDE_TODO © " & _
                                            "FLG_LIMITE_PRECIO © " & _
                                            "FACTOR © " & _
                                            "ID_NORMATIVO © " & _
                                            "ID_PORTAFOLIO © " & _
                                            "SALDO_POR_ASIGNAR © " & _
                                            "COD_TIPO_DOCUMENTO © " & _
                                            "NUMERO_DOCUMENTO © " & _
                                            "ID_EMISOR © " & _
                                            "ID_CUSTODIO © " & _
                                            "TIPO_CUSTODIA © " & _
                                            "ID_CARTERA © " & _
                                            "FECHA_EMISION © " & _
                                            "COD_TIPO_REAJUSTE © " & _
                                            "MONTO_MONEDA_NEMO © " & _
                                            "ING_CANTIDAD_INICIAL © " & _
                                            "ING_PORC_VALOR_PAR © " & _
                                            "ID_ORDEN © " & _
                                            "FOLIO_OPERACION © " & _
                                            "LINEA_OPERACION © " & _
                                            "FOLIO_OPERACION_COMPRA © " & _
                                            "LINEA_OPERACION_COMPRA © " & _
                                            "FECHA_COMPRA © " & _
                                            "PRECIO_TASA_COMPRA © " & _
                                            "MONTO_COMPRA"

        Dim lstrColumnasComision As String = "PORCENTAJE © " & _
                                             "MONTO © " & _
                                             "IMPUESTO © " & _
                                             "ID_COMISION © " & _
                                             "ID_NUMEROLINEA_DETALLE"


        Dim lstrColumnasFormaPago As String = "ID_TIPO_ESTADO © " & _
                                              "COD_ESTADO © " & _
                                              "BANCO_ID © " & _
                                              "COD_MEDIO_PAGO_COBRO © " & _
                                              "FECHA_MOVIMIENTO © " & _
                                              "FECHA_DOCUMENTO © " & _
                                              "NUM_DOCUMENTO © " & _
                                              "RETENCION © " & _
                                              "MONTO © " & _
                                              "CTA_CTE_BANCARIA"
        Dim lstrResultadoCadena As String = ""

        ' GENERA CADENA(S) DE DETALLE DE OPERACIONES
        lstrResultadoCadena = GeneraCadena(dtbOperacionDetalle, lstrColumnasDetalle, "©", lstrDetalle1, lstrDetalle2, lstrDetalle3)
        If lstrResultadoCadena <> "OK" Then
            Return lstrResultadoCadena
        End If

        lstrResultadoCadena = ""
        'GENERA CADENA DE COMISIONES
        If IsNothing(dtbComision) OrElse dtbComision.Rows.Count = 0 Then
            lstrComisiones = ""
        Else
            lstrResultadoCadena = GeneraCadena(dtbComision, lstrColumnasComision, "©", lstrComisiones)
            If lstrResultadoCadena <> "OK" Then
                Return lstrResultadoCadena & vbCrLf & "En Comisiones"
            End If
        End If

        'GENERA CADENA DE MEDIOS DE COBRO/PAGOS
        lstrResultadoCadena = ""
        If IsNothing(dtbMedioPagoCobro) OrElse dtbMedioPagoCobro.Rows.Count = 0 Then
            lstrMediosCobroPago = ""
        Else
            lstrResultadoCadena = GeneraCadena(dtbMedioPagoCobro, lstrColumnasFormaPago, "©", lstrMediosCobroPago)
            If lstrResultadoCadena <> "OK" Then
                Return lstrResultadoCadena & vbCrLf & "En Medios de Cobro/Pago"
            End If
        End If


        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion
        Try


            SQLCommand = New SqlClient.SqlCommand("Rcp_Operacion_Guarda_Completa", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Operacion_Guarda_Completa"
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            Dim dtrOperacionEncabezado As DataRow = dtbOperacionEncabezado.Rows(0)

            SQLCommand.Parameters.Add("pIdSistemaOrigen", SqlDbType.Int, 18).Value = dtrOperacionEncabezado("ID_SISTEMA_ORIGEN")
            SQLCommand.Parameters.Add("pCodMoneda", SqlDbType.VarChar, 3).Value = dtrOperacionEncabezado("COD_MONEDA")
            SQLCommand.Parameters.Add("pCodTipoOperacion", SqlDbType.VarChar, 10).Value = dtrOperacionEncabezado("COD_TIPO_OPERACION")
            SQLCommand.Parameters.Add("pIdRepresentante", SqlDbType.Int, 18).Value = dtrOperacionEncabezado("ID_REPRESENTANTE")
            SQLCommand.Parameters.Add("pIdSistemaConfirmacion", SqlDbType.Int, 18).Value = dtrOperacionEncabezado("ID_SISTEMA_CONFIRMACION")
            SQLCommand.Parameters.Add("pIdTipoEstado", SqlDbType.Int, 18).Value = dtrOperacionEncabezado("ID_TIPO_ESTADO")
            SQLCommand.Parameters.Add("pCodEstado", SqlDbType.VarChar, 3).Value = dtrOperacionEncabezado("COD_ESTADO")
            SQLCommand.Parameters.Add("pIdTrader", SqlDbType.Int, 18).Value = IIf((IsDBNull(dtrOperacionEncabezado("ID_TRADER")) OrElse dtrOperacionEncabezado("ID_TRADER") = 0), DBNull.Value, dtrOperacionEncabezado("ID_TRADER"))

            SQLCommand.Parameters.Add("pIdContraparte", SqlDbType.Int, 18).Value = IIf((IsDBNull(dtrOperacionEncabezado("ID_CONTRAPARTE")) OrElse dtrOperacionEncabezado("ID_CONTRAPARTE") = 0), DBNull.Value, dtrOperacionEncabezado("ID_CONTRAPARTE"))
            SQLCommand.Parameters.Add("pFechaOperacion", SqlDbType.Char, 10).Value = dtrOperacionEncabezado("FECHA_OPERACION")
            SQLCommand.Parameters.Add("pFechaVigencia", SqlDbType.Char, 10).Value = dtrOperacionEncabezado("FECHA_VIGENCIA")
            SQLCommand.Parameters.Add("pFechaLiquidacion", SqlDbType.Char, 10).Value = dtrOperacionEncabezado("FECHA_LIQUIDACION")
            SQLCommand.Parameters.Add("pMontoOperacion", SqlDbType.Decimal, 26).Value = dtrOperacionEncabezado("MONTO_OPERACION")
            SQLCommand.Parameters.Add("pFlgTipoOrigen", SqlDbType.Char, 1).Value = dtrOperacionEncabezado("FLG_TIPO_ORIGEN")
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Int, 18).Value = dtrOperacionEncabezado("ID_CUENTA")
            SQLCommand.Parameters.Add("pIdOperacionConcepto", SqlDbType.Int, 3).Value = dtrOperacionEncabezado("ID_OPERACION_CONCEPTO")
            SQLCommand.Parameters.Add("pDscOperacion", SqlDbType.VarChar, 200).Value = dtrOperacionEncabezado("DSC_OPERACION")
            SQLCommand.Parameters.Add("PCod_Mercado", SqlDbType.VarChar, 4).Value = DBNull.Value
            SQLCommand.Parameters.Add("pLineas", SqlDbType.Int, 5).Value = dtbOperacionDetalle.Rows.Count
            SQLCommand.Parameters.Add("pDetalle1", SqlDbType.VarChar, 8000).Value = lstrDetalle1
            SQLCommand.Parameters.Add("pComisiones", SqlDbType.VarChar, 8000).Value = IIf(lstrComisiones = "", DBNull.Value, lstrComisiones)
            SQLCommand.Parameters.Add("pMediosCobroPago", SqlDbType.VarChar, 8000).Value = IIf(lstrMediosCobroPago = "", DBNull.Value, lstrMediosCobroPago)
            SQLCommand.Parameters.Add("pDetalle2", SqlDbType.VarChar, 8000).Value = IIf(lstrDetalle2 = "", DBNull.Value, lstrDetalle2)
            SQLCommand.Parameters.Add("pDetalle3", SqlDbType.VarChar, 8000).Value = IIf(lstrDetalle3 = "", DBNull.Value, lstrDetalle3)
            SQLCommand.Parameters.Add("pObservacion", SqlDbType.VarChar, 200).Value = IIf(strObservacion = "", DBNull.Value, strObservacion)
            SQLCommand.Parameters.Add("pFormaOperarCartera", SqlDbType.VarChar, 15).Value = IIf(strFormaOperarCartera = "", DBNull.Value, strFormaOperarCartera)
            SQLCommand.Parameters.Add("pFlgEnRueda", SqlDbType.VarChar, 1).Value = IIf(strFlgEnRueda = "", DBNull.Value, strFlgEnRueda)

            '...Id Solicitud de Inversion
            Dim ParSalOperacion1 As New SqlClient.SqlParameter("pIdOperacion", SqlDbType.Decimal, 18)
            ParSalOperacion1.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParSalOperacion1)

            Dim ParSalOperacion2 As New SqlClient.SqlParameter("pIdOperacionInt", SqlDbType.Decimal, 18)
            ParSalOperacion2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParSalOperacion2)

            '...Resultado
            Dim ParametroSal As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If Trim(strDescError) = "OK" Then
                dblIdGenerado = SQLCommand.Parameters("pIdOperacion").Value
                dblIdGeneradoInt = SQLCommand.Parameters("pIdOperacionInt").Value

                'SI intIdCuentadestino ES <> -1 y strTipoOperacionDestino <> ""
                'SE HACE EL MOV. DE DESTINO CON LA CUENTA DE DESTINO.

                If intIdCuentadestino <> -1 And strTipoOperacionDestino <> "" Then

                    SQLCommand = New SqlClient.SqlCommand("Rcp_Operacion_Guarda_Completa", MiTransaccionSQL.Connection, MiTransaccionSQL)

                    SQLCommand.CommandType = CommandType.StoredProcedure
                    SQLCommand.CommandText = "Rcp_Operacion_Guarda_Completa"
                    SQLCommand.CommandTimeout = 0
                    SQLCommand.Parameters.Clear()

                    SQLCommand.Parameters.Add("pIdSistemaOrigen", SqlDbType.Int, 18).Value = dtrOperacionEncabezado("ID_SISTEMA_ORIGEN")
                    SQLCommand.Parameters.Add("pCodMoneda", SqlDbType.VarChar, 3).Value = dtrOperacionEncabezado("COD_MONEDA")
                    SQLCommand.Parameters.Add("pCodTipoOperacion", SqlDbType.VarChar, 10).Value = strTipoOperacionDestino.Trim
                    SQLCommand.Parameters.Add("pIdRepresentante", SqlDbType.Int, 18).Value = dtrOperacionEncabezado("ID_REPRESENTANTE")
                    SQLCommand.Parameters.Add("pIdSistemaConfirmacion", SqlDbType.Int, 18).Value = dtrOperacionEncabezado("ID_SISTEMA_CONFIRMACION")
                    SQLCommand.Parameters.Add("pIdTipoEstado", SqlDbType.Int, 18).Value = dtrOperacionEncabezado("ID_TIPO_ESTADO")
                    SQLCommand.Parameters.Add("pCodEstado", SqlDbType.VarChar, 3).Value = dtrOperacionEncabezado("COD_ESTADO")
                    SQLCommand.Parameters.Add("pIdTrader", SqlDbType.Int, 18).Value = dtrOperacionEncabezado("ID_TRADER")
                    SQLCommand.Parameters.Add("pIdContraparte", SqlDbType.Int, 18).Value = dblIdContraparteDestino 'dtrOperacionEncabezado("ID_CONTRAPARTE")
                    SQLCommand.Parameters.Add("pFechaOperacion", SqlDbType.Char, 10).Value = dtrOperacionEncabezado("FECHA_OPERACION")
                    SQLCommand.Parameters.Add("pFechaVigencia", SqlDbType.Char, 10).Value = dtrOperacionEncabezado("FECHA_VIGENCIA")
                    SQLCommand.Parameters.Add("pFechaLiquidacion", SqlDbType.Char, 10).Value = dtrOperacionEncabezado("FECHA_LIQUIDACION")
                    SQLCommand.Parameters.Add("pMontoOperacion", SqlDbType.Decimal, 26).Value = dtrOperacionEncabezado("MONTO_OPERACION")
                    SQLCommand.Parameters.Add("pFlgTipoOrigen", SqlDbType.Char, 1).Value = dtrOperacionEncabezado("FLG_TIPO_ORIGEN")
                    SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Int, 18).Value = intIdCuentadestino
                    SQLCommand.Parameters.Add("pIdOperacionConcepto", SqlDbType.Int, 3).Value = dtrOperacionEncabezado("ID_OPERACION_CONCEPTO")
                    SQLCommand.Parameters.Add("pDscOperacion", SqlDbType.VarChar, 200).Value = dtrOperacionEncabezado("DSC_OPERACION")
                    SQLCommand.Parameters.Add("PCod_Mercado", SqlDbType.VarChar, 4).Value = DBNull.Value
                    SQLCommand.Parameters.Add("pLineas", SqlDbType.Int, 5).Value = dtbOperacionDetalle.Rows.Count
                    SQLCommand.Parameters.Add("pDetalle1", SqlDbType.VarChar, 8000).Value = lstrDetalle1
                    SQLCommand.Parameters.Add("pComisiones", SqlDbType.VarChar, 8000).Value = IIf(lstrComisiones = "", DBNull.Value, lstrComisiones)
                    SQLCommand.Parameters.Add("pMediosCobroPago", SqlDbType.VarChar, 8000).Value = IIf(lstrMediosCobroPago = "", DBNull.Value, lstrMediosCobroPago)
                    SQLCommand.Parameters.Add("pDetalle2", SqlDbType.VarChar, 8000).Value = IIf(lstrDetalle2 = "", DBNull.Value, lstrDetalle2)
                    SQLCommand.Parameters.Add("pDetalle3", SqlDbType.VarChar, 8000).Value = IIf(lstrDetalle3 = "", DBNull.Value, lstrDetalle3)
                    SQLCommand.Parameters.Add("pObservacion", SqlDbType.VarChar, 200).Value = IIf(strObservacion = "", DBNull.Value, strObservacion)
                    SQLCommand.Parameters.Add("pFormaOperarCartera", SqlDbType.VarChar, 15).Value = IIf(strFormaOperarCartera = "", DBNull.Value, strFormaOperarCartera)
                    SQLCommand.Parameters.Add("pFlgEnRueda", SqlDbType.VarChar, 1).Value = IIf(strFlgEnRueda = "", DBNull.Value, strFlgEnRueda)

                    '...Id Solicitud de Inversion
                    Dim ParSalOperacionDest As New SqlClient.SqlParameter("pIdOperacion", SqlDbType.Int, 10)
                    ParSalOperacionDest.Direction = Data.ParameterDirection.Output
                    SQLCommand.Parameters.Add(ParSalOperacionDest)

                    '...Resultado
                    Dim ParametroSalDest As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                    ParametroSalDest.Direction = Data.ParameterDirection.Output
                    SQLCommand.Parameters.Add(ParametroSalDest)

                    SQLCommand.ExecuteNonQuery()
                    llngIdOperacion = SQLCommand.Parameters("pIdOperacion").Value
                    strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

                    If strDescError = "OK" Then
                        MiTransaccionSQL.Commit()
                    Else
                        MiTransaccionSQL.Rollback()
                    End If
                Else
                    llngIdOperacion = SQLCommand.Parameters("pIdOperacion").Value
                    MiTransaccionSQL.Commit()
                End If

            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error al Grabar Operación " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
        End Try

        If strDescError.Trim.ToUpper = "OK" Then
            If gobjParametro.Tesoreria_Envio.ValorParametro = "S" Then
                Dim lcInterfaceTesoreria As New clsInterfaceTesoreria
                lcInterfaceTesoreria.InformarIngresoOperacion(llngIdOperacion)
            End If
        End If

        Return strDescError
    End Function

    Public Function GuardarOperacionesCompleta2(ByVal dtbOperacionEncabezado As DataTable, _
                                           ByVal dtbOperacionDetalle As DataTable, _
                                           Optional ByVal dtbMedioPagoCobro As DataTable = Nothing, _
                                           Optional ByVal dtbComision As DataTable = Nothing, _
                                           Optional ByRef dblIdGenerado As Double = 0, _
                                           Optional ByVal intIdCuentadestino As Integer = -1, _
                                           Optional ByVal strTipoOperacionDestino As String = "", _
                                           Optional ByVal dblIdContraparteDestino As Double = 0, _
                                           Optional ByVal strFormaOperarCartera As String = "", _
                                           Optional ByVal strObservacion As String = "", _
                                           Optional ByVal strFlgEnRueda As String = "N", _
                                           Optional ByRef dblIdGeneradoInt As Double = 0, _
                                           Optional ByRef strCodMonedaComision As String = "") As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction
        Dim lstrDetalle1 As String = ""
        Dim lstrDetalle2 As String = ""
        Dim lstrDetalle3 As String = ""
        Dim lstrComisiones As String = ""
        Dim lstrMediosCobroPago As String = ""
        Dim llngIdOperacion As Double = 0
        Dim lstrColumnasDetalle As String = "ID_INSTRUMENTO_ORIGEN © " & _
                                            "ID_INSTRUMENTO_DESTINO © " & _
                                            "CANTIDAD © " & _
                                            "PRECIO © " & _
                                            "PRECIO_GESTION © " & _
                                            "PLAZO © " & _
                                            "TASA © " & _
                                            "BASE © " & _
                                            "FECHA_VENCIMIENTO © " & _
                                            "VALOR_DIVISA © " & _
                                            "INTERES © " & _
                                            "UTILIDAD © " & _
                                            "MONTO © " & _
                                            "FLG_MONTO_REFERENCIADO © " & _
                                            "FLG_TIPO_DEPOSITO © " & _
                                            "FECHA_VALUTA © " & _
                                            "FLG_VENDE_TODO © " & _
                                            "FLG_LIMITE_PRECIO © " & _
                                            "FACTOR © " & _
                                            "ID_NORMATIVO © " & _
                                            "ID_PORTAFOLIO © " & _
                                            "SALDO_POR_ASIGNAR © " & _
                                            "COD_TIPO_DOCUMENTO © " & _
                                            "NUMERO_DOCUMENTO © " & _
                                            "ID_EMISOR © " & _
                                            "ID_CUSTODIO © " & _
                                            "TIPO_CUSTODIA © " & _
                                            "ID_CARTERA © " & _
                                            "FECHA_EMISION © " & _
                                            "COD_TIPO_REAJUSTE © " & _
                                            "MONTO_MONEDA_NEMO © " & _
                                            "ING_CANTIDAD_INICIAL © " & _
                                            "ING_PORC_VALOR_PAR © " & _
                                            "ID_ORDEN © " & _
                                            "FOLIO_OPERACION © " & _
                                            "LINEA_OPERACION © " & _
                                            "FOLIO_OPERACION_COMPRA © " & _
                                            "LINEA_OPERACION_COMPRA © " & _
                                            "FECHA_COMPRA © " & _
                                            "PRECIO_TASA_COMPRA © " & _
                                            "MONTO_COMPRA"

        Dim lstrColumnasComision As String = "PORCENTAJE © " & _
                                             "MONTO © " & _
                                             "IMPUESTO © " & _
                                             "ID_COMISION © " & _
                                             "ID_NUMEROLINEA_DETALLE"


        Dim lstrColumnasFormaPago As String = "ID_TIPO_ESTADO © " & _
                                              "COD_ESTADO © " & _
                                              "BANCO_ID © " & _
                                              "COD_MEDIO_PAGO_COBRO © " & _
                                              "FECHA_MOVIMIENTO © " & _
                                              "FECHA_DOCUMENTO © " & _
                                              "NUM_DOCUMENTO © " & _
                                              "RETENCION © " & _
                                              "MONTO © " & _
                                              "CTA_CTE_BANCARIA"
        Dim lstrResultadoCadena As String = ""

        ' GENERA CADENA(S) DE DETALLE DE OPERACIONES
        lstrResultadoCadena = GeneraCadena(dtbOperacionDetalle, lstrColumnasDetalle, "©", lstrDetalle1, lstrDetalle2, lstrDetalle3)
        If lstrResultadoCadena <> "OK" Then
            Return lstrResultadoCadena
        End If

        lstrResultadoCadena = ""
        'GENERA CADENA DE COMISIONES
        If IsNothing(dtbComision) OrElse dtbComision.Rows.Count = 0 Then
            lstrComisiones = ""
        Else
            lstrResultadoCadena = GeneraCadena(dtbComision, lstrColumnasComision, "©", lstrComisiones)
            If lstrResultadoCadena <> "OK" Then
                Return lstrResultadoCadena & vbCrLf & "En Comisiones"
            End If
        End If

        'GENERA CADENA DE MEDIOS DE COBRO/PAGOS
        lstrResultadoCadena = ""
        If IsNothing(dtbMedioPagoCobro) OrElse dtbMedioPagoCobro.Rows.Count = 0 Then
            lstrMediosCobroPago = ""
        Else
            lstrResultadoCadena = GeneraCadena(dtbMedioPagoCobro, lstrColumnasFormaPago, "©", lstrMediosCobroPago)
            If lstrResultadoCadena <> "OK" Then
                Return lstrResultadoCadena & vbCrLf & "En Medios de Cobro/Pago"
            End If
        End If


        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion
        Try


            SQLCommand = New SqlClient.SqlCommand("Rcp_Operacion_Guarda_Completa", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Operacion_Guarda_Completa"
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            Dim dtrOperacionEncabezado As DataRow = dtbOperacionEncabezado.Rows(0)

            SQLCommand.Parameters.Add("pIdSistemaOrigen", SqlDbType.Int, 18).Value = dtrOperacionEncabezado("ID_SISTEMA_ORIGEN")
            SQLCommand.Parameters.Add("pCodMoneda", SqlDbType.VarChar, 3).Value = dtrOperacionEncabezado("COD_MONEDA")
            SQLCommand.Parameters.Add("pCodTipoOperacion", SqlDbType.VarChar, 10).Value = dtrOperacionEncabezado("COD_TIPO_OPERACION").trim
            SQLCommand.Parameters.Add("pIdRepresentante", SqlDbType.Int, 18).Value = dtrOperacionEncabezado("ID_REPRESENTANTE")
            SQLCommand.Parameters.Add("pIdSistemaConfirmacion", SqlDbType.Int, 18).Value = dtrOperacionEncabezado("ID_SISTEMA_CONFIRMACION")
            SQLCommand.Parameters.Add("pIdTipoEstado", SqlDbType.Int, 18).Value = dtrOperacionEncabezado("ID_TIPO_ESTADO")
            SQLCommand.Parameters.Add("pCodEstado", SqlDbType.VarChar, 3).Value = dtrOperacionEncabezado("COD_ESTADO")
            SQLCommand.Parameters.Add("pIdTrader", SqlDbType.Int, 18).Value = IIf((IsDBNull(dtrOperacionEncabezado("ID_TRADER")) OrElse dtrOperacionEncabezado("ID_TRADER") = 0), DBNull.Value, dtrOperacionEncabezado("ID_TRADER"))

            SQLCommand.Parameters.Add("pIdContraparte", SqlDbType.Int, 18).Value = IIf((IsDBNull(dtrOperacionEncabezado("ID_CONTRAPARTE")) OrElse dtrOperacionEncabezado("ID_CONTRAPARTE") = 0), DBNull.Value, dtrOperacionEncabezado("ID_CONTRAPARTE"))
            SQLCommand.Parameters.Add("pFechaOperacion", SqlDbType.Char, 10).Value = dtrOperacionEncabezado("FECHA_OPERACION")
            SQLCommand.Parameters.Add("pFechaVigencia", SqlDbType.Char, 10).Value = dtrOperacionEncabezado("FECHA_VIGENCIA")
            SQLCommand.Parameters.Add("pFechaLiquidacion", SqlDbType.Char, 10).Value = dtrOperacionEncabezado("FECHA_LIQUIDACION")
            SQLCommand.Parameters.Add("pMontoOperacion", SqlDbType.Decimal, 26).Value = dtrOperacionEncabezado("MONTO_OPERACION")
            SQLCommand.Parameters.Add("pFlgTipoOrigen", SqlDbType.Char, 1).Value = dtrOperacionEncabezado("FLG_TIPO_ORIGEN")
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Int, 18).Value = dtrOperacionEncabezado("ID_CUENTA")
            SQLCommand.Parameters.Add("pIdOperacionConcepto", SqlDbType.Int, 3).Value = dtrOperacionEncabezado("ID_OPERACION_CONCEPTO")
            SQLCommand.Parameters.Add("pDscOperacion", SqlDbType.VarChar, 200).Value = dtrOperacionEncabezado("DSC_OPERACION")
            SQLCommand.Parameters.Add("PCod_Mercado", SqlDbType.VarChar, 4).Value = DBNull.Value
            SQLCommand.Parameters.Add("pLineas", SqlDbType.Int, 5).Value = dtbOperacionDetalle.Rows.Count
            SQLCommand.Parameters.Add("pDetalle1", SqlDbType.VarChar, 8000).Value = lstrDetalle1
            SQLCommand.Parameters.Add("pComisiones2", SqlDbType.VarChar, 8000).Value = IIf(lstrComisiones = "", DBNull.Value, lstrComisiones)
            SQLCommand.Parameters.Add("pMediosCobroPago", SqlDbType.VarChar, 8000).Value = IIf(lstrMediosCobroPago = "", DBNull.Value, lstrMediosCobroPago)
            SQLCommand.Parameters.Add("pDetalle2", SqlDbType.VarChar, 8000).Value = IIf(lstrDetalle2 = "", DBNull.Value, lstrDetalle2)
            SQLCommand.Parameters.Add("pDetalle3", SqlDbType.VarChar, 8000).Value = IIf(lstrDetalle3 = "", DBNull.Value, lstrDetalle3)
            SQLCommand.Parameters.Add("pObservacion", SqlDbType.VarChar, 200).Value = IIf(strObservacion = "", DBNull.Value, strObservacion)
            SQLCommand.Parameters.Add("pFormaOperarCartera", SqlDbType.VarChar, 15).Value = IIf(strFormaOperarCartera = "", DBNull.Value, strFormaOperarCartera)
            SQLCommand.Parameters.Add("pFlgEnRueda", SqlDbType.VarChar, 1).Value = IIf(strFlgEnRueda = "", DBNull.Value, strFlgEnRueda)

            SQLCommand.Parameters.Add("pCodMonedaComision", SqlDbType.VarChar, 3).Value = IIf(strCodMonedaComision = "", "CLP", strCodMonedaComision)

            '...Id Solicitud de Inversion
            Dim ParSalOperacion1 As New SqlClient.SqlParameter("pIdOperacion", SqlDbType.Float, 18)
            ParSalOperacion1.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParSalOperacion1)

            Dim ParSalOperacion2 As New SqlClient.SqlParameter("pIdOperacionInt", SqlDbType.Float, 18)
            ParSalOperacion2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParSalOperacion2)

            '...Resultado
            Dim ParametroSal As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If Trim(strDescError) = "OK" Then
                dblIdGenerado = SQLCommand.Parameters("pIdOperacion").Value
                dblIdGeneradoInt = SQLCommand.Parameters("pIdOperacionInt").Value

                'SI intIdCuentadestino ES <> -1 y strTipoOperacionDestino <> ""
                'SE HACE EL MOV. DE DESTINO CON LA CUENTA DE DESTINO.

                If intIdCuentadestino <> -1 And strTipoOperacionDestino <> "" Then

                    SQLCommand = New SqlClient.SqlCommand("Rcp_Operacion_Guarda_Completa", MiTransaccionSQL.Connection, MiTransaccionSQL)

                    SQLCommand.CommandType = CommandType.StoredProcedure
                    SQLCommand.CommandText = "Rcp_Operacion_Guarda_Completa"
                    SQLCommand.CommandTimeout = 0
                    SQLCommand.Parameters.Clear()

                    SQLCommand.Parameters.Add("pIdSistemaOrigen", SqlDbType.Int, 18).Value = dtrOperacionEncabezado("ID_SISTEMA_ORIGEN")
                    SQLCommand.Parameters.Add("pCodMoneda", SqlDbType.VarChar, 3).Value = dtrOperacionEncabezado("COD_MONEDA")
                    SQLCommand.Parameters.Add("pCodTipoOperacion", SqlDbType.VarChar, 10).Value = strTipoOperacionDestino.Trim
                    SQLCommand.Parameters.Add("pIdRepresentante", SqlDbType.Int, 18).Value = dtrOperacionEncabezado("ID_REPRESENTANTE")
                    SQLCommand.Parameters.Add("pIdSistemaConfirmacion", SqlDbType.Int, 18).Value = dtrOperacionEncabezado("ID_SISTEMA_CONFIRMACION")
                    SQLCommand.Parameters.Add("pIdTipoEstado", SqlDbType.Int, 18).Value = dtrOperacionEncabezado("ID_TIPO_ESTADO")
                    SQLCommand.Parameters.Add("pCodEstado", SqlDbType.VarChar, 3).Value = dtrOperacionEncabezado("COD_ESTADO")
                    SQLCommand.Parameters.Add("pIdTrader", SqlDbType.Int, 18).Value = dtrOperacionEncabezado("ID_TRADER")
                    SQLCommand.Parameters.Add("pIdContraparte", SqlDbType.Int, 18).Value = dblIdContraparteDestino 'dtrOperacionEncabezado("ID_CONTRAPARTE")
                    SQLCommand.Parameters.Add("pFechaOperacion", SqlDbType.Char, 10).Value = dtrOperacionEncabezado("FECHA_OPERACION")
                    SQLCommand.Parameters.Add("pFechaVigencia", SqlDbType.Char, 10).Value = dtrOperacionEncabezado("FECHA_VIGENCIA")
                    SQLCommand.Parameters.Add("pFechaLiquidacion", SqlDbType.Char, 10).Value = dtrOperacionEncabezado("FECHA_LIQUIDACION")
                    SQLCommand.Parameters.Add("pMontoOperacion", SqlDbType.Decimal, 26).Value = dtrOperacionEncabezado("MONTO_OPERACION")
                    SQLCommand.Parameters.Add("pFlgTipoOrigen", SqlDbType.Char, 1).Value = dtrOperacionEncabezado("FLG_TIPO_ORIGEN")
                    SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Int, 18).Value = intIdCuentadestino
                    SQLCommand.Parameters.Add("pIdOperacionConcepto", SqlDbType.Int, 3).Value = dtrOperacionEncabezado("ID_OPERACION_CONCEPTO")
                    SQLCommand.Parameters.Add("pDscOperacion", SqlDbType.VarChar, 200).Value = dtrOperacionEncabezado("DSC_OPERACION")
                    SQLCommand.Parameters.Add("PCod_Mercado", SqlDbType.VarChar, 4).Value = DBNull.Value
                    SQLCommand.Parameters.Add("pLineas", SqlDbType.Int, 5).Value = dtbOperacionDetalle.Rows.Count
                    SQLCommand.Parameters.Add("pDetalle1", SqlDbType.VarChar, 8000).Value = lstrDetalle1
                    SQLCommand.Parameters.Add("pComisiones2", SqlDbType.VarChar, 8000).Value = IIf(lstrComisiones = "", DBNull.Value, lstrComisiones)
                    SQLCommand.Parameters.Add("pMediosCobroPago", SqlDbType.VarChar, 8000).Value = IIf(lstrMediosCobroPago = "", DBNull.Value, lstrMediosCobroPago)
                    SQLCommand.Parameters.Add("pDetalle2", SqlDbType.VarChar, 8000).Value = IIf(lstrDetalle2 = "", DBNull.Value, lstrDetalle2)
                    SQLCommand.Parameters.Add("pDetalle3", SqlDbType.VarChar, 8000).Value = IIf(lstrDetalle3 = "", DBNull.Value, lstrDetalle3)
                    SQLCommand.Parameters.Add("pObservacion", SqlDbType.VarChar, 200).Value = IIf(strObservacion = "", DBNull.Value, strObservacion)
                    SQLCommand.Parameters.Add("pFormaOperarCartera", SqlDbType.VarChar, 15).Value = IIf(strFormaOperarCartera = "", DBNull.Value, strFormaOperarCartera)
                    SQLCommand.Parameters.Add("pFlgEnRueda", SqlDbType.VarChar, 1).Value = IIf(strFlgEnRueda = "", DBNull.Value, strFlgEnRueda)

                    '...Id Solicitud de Inversion
                    Dim ParSalOperacionDest As New SqlClient.SqlParameter("pIdOperacion", SqlDbType.Int, 10)
                    ParSalOperacionDest.Direction = Data.ParameterDirection.Output
                    SQLCommand.Parameters.Add(ParSalOperacionDest)

                    '...Resultado
                    Dim ParametroSalDest As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                    ParametroSalDest.Direction = Data.ParameterDirection.Output
                    SQLCommand.Parameters.Add(ParametroSalDest)

                    SQLCommand.ExecuteNonQuery()
                    llngIdOperacion = SQLCommand.Parameters("pIdOperacion").Value
                    strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

                    If strDescError = "OK" Then
                        MiTransaccionSQL.Commit()
                    Else
                        MiTransaccionSQL.Rollback()
                    End If
                Else
                    llngIdOperacion = SQLCommand.Parameters("pIdOperacion").Value
                    MiTransaccionSQL.Commit()
                End If

            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error al Grabar Operación " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
        End Try

        If strDescError.Trim.ToUpper = "OK" Then
            If gobjParametro.Tesoreria_Envio.ValorParametro = "S" Then
                Dim lcInterfaceTesoreria As New clsInterfaceTesoreria
                lcInterfaceTesoreria.InformarIngresoOperacion(llngIdOperacion)
            End If
        End If

        Return strDescError
    End Function

    Public Function CrearDataTableOperacion() As DataTable
        Dim ldtbOperacion As New DataTable

        AddCampo(ldtbOperacion, "ID_SISTEMA_ORIGEN", "System.Double")
        AddCampo(ldtbOperacion, "COD_MONEDA", "System.String")
        AddCampo(ldtbOperacion, "COD_TIPO_OPERACION", "System.String")
        AddCampo(ldtbOperacion, "ID_REPRESENTANTE", "System.Double")
        AddCampo(ldtbOperacion, "ID_SISTEMA_CONFIRMACION", "System.Double")
        AddCampo(ldtbOperacion, "ID_TIPO_ESTADO", "System.Double")
        AddCampo(ldtbOperacion, "COD_ESTADO", "System.String")
        AddCampo(ldtbOperacion, "ID_TRADER", "System.Double")
        AddCampo(ldtbOperacion, "ID_CONTRAPARTE", "System.Double")
        AddCampo(ldtbOperacion, "FLG_TIPO_MOVIMIENTO", "System.String")
        AddCampo(ldtbOperacion, "FECHA_OPERACION", "System.String")
        AddCampo(ldtbOperacion, "FECHA_VIGENCIA", "System.String")
        AddCampo(ldtbOperacion, "FECHA_LIQUIDACION", "System.String")
        AddCampo(ldtbOperacion, "DSC_OPERACION", "System.String")
        AddCampo(ldtbOperacion, "FECHA_CONFIRMACION", "System.String")
        AddCampo(ldtbOperacion, "PORC_COMISION", "System.Double")
        AddCampo(ldtbOperacion, "COMISION", "System.Double")
        AddCampo(ldtbOperacion, "DERECHOS", "System.Double")
        AddCampo(ldtbOperacion, "GASTOS", "System.Double")
        AddCampo(ldtbOperacion, "IVA", "System.Double")
        AddCampo(ldtbOperacion, "MONTO_OPERACION", "System.Double")
        AddCampo(ldtbOperacion, "FLG_LIMITE_PRECIO", "System.String")
        AddCampo(ldtbOperacion, "FLG_TIPO_ORIGEN", "System.String")
        AddCampo(ldtbOperacion, "CONFIRMA_COMISION", "System.Double")
        AddCampo(ldtbOperacion, "CONFIRMA_DERECHOS", "System.Double")
        AddCampo(ldtbOperacion, "CONFIRMA_GASTOS", "System.Double")
        AddCampo(ldtbOperacion, "CONFIRMA_IVA", "System.Double")
        AddCampo(ldtbOperacion, "CONFIRMA_MONTO_OPERACION", "System.Double")
        AddCampo(ldtbOperacion, "INTERES", "System.Double")
        AddCampo(ldtbOperacion, "ID_CUENTA", "System.Double")

        AddCampo(ldtbOperacion, "AMORTIZACION", "System.Double")
        AddCampo(ldtbOperacion, "FLUJO", "System.Double")
        AddCampo(ldtbOperacion, "FCH_PROX_CORTE_CUPON", "System.String")
        AddCampo(ldtbOperacion, "NUMERO_CUPON", "System.Double")
        AddCampo(ldtbOperacion, "ID_OPERACION_CONCEPTO", "System.Double")

        AddCampo(ldtbOperacion, "ENC_MUEVE_CAJA", "System.String")
        AddCampo(ldtbOperacion, "ENC_MUEVE_CARTERA", "System.String")
        AddCampo(ldtbOperacion, "COD_SUB_CLASE_INSTRUMENTO", "System.String")

        Return ldtbOperacion
    End Function

    Public Function CrearDataTableDetalleOperacion() As DataTable
        Dim ldtbDetalleOperacion As New DataTable

        AddCampo(ldtbDetalleOperacion, "ID_NUMEROLINEA_DETALLE", "System.Double")
        AddCampo(ldtbDetalleOperacion, "ID_TIPO_ESTADO", "System.Double")
        AddCampo(ldtbDetalleOperacion, "COD_ESTADO", "System.String")
        AddCampo(ldtbDetalleOperacion, "ID_INSTRUMENTO_ORIGEN", "System.Double")
        AddCampo(ldtbDetalleOperacion, "NEMOTECNICO_ORIGEN", "System.String")
        AddCampo(ldtbDetalleOperacion, "ID_INSTRUMENTO_DESTINO", "System.Double")
        AddCampo(ldtbDetalleOperacion, "NEMOTECNICO_DESTINO", "System.String")
        AddCampo(ldtbDetalleOperacion, "COD_MONEDA", "System.String")
        AddCampo(ldtbDetalleOperacion, "ID_SOL_INVERSION", "System.Double")
        AddCampo(ldtbDetalleOperacion, "COD_MONEDA_PAGO", "System.String")
        AddCampo(ldtbDetalleOperacion, "CANTIDAD", "System.Double")
        AddCampo(ldtbDetalleOperacion, "PRECIO", "System.Double")
        AddCampo(ldtbDetalleOperacion, "PRECIO_GESTION", "System.Double")
        AddCampo(ldtbDetalleOperacion, "PRECIO_HISTORICO_COMPRA", "System.Double")
        AddCampo(ldtbDetalleOperacion, "TASA", "System.Double")
        AddCampo(ldtbDetalleOperacion, "PLAZO", "System.Double")
        AddCampo(ldtbDetalleOperacion, "BASE", "System.Double")
        AddCampo(ldtbDetalleOperacion, "VALOR_DIVISA", "System.Double")
        AddCampo(ldtbDetalleOperacion, "COMISION", "System.Double")
        AddCampo(ldtbDetalleOperacion, "INTERES", "System.Double")
        AddCampo(ldtbDetalleOperacion, "UTILIDAD", "System.Double")
        AddCampo(ldtbDetalleOperacion, "PORC_COMISION", "System.Double")
        AddCampo(ldtbDetalleOperacion, "MONTO", "System.Double")
        AddCampo(ldtbDetalleOperacion, "MONTO2", "System.Double")
        AddCampo(ldtbDetalleOperacion, "MONTO_PAGO", "System.Double")
        AddCampo(ldtbDetalleOperacion, "MONTO_BRUTO", "System.Double")
        AddCampo(ldtbDetalleOperacion, "MONTO_TOTAL", "System.Double")
        AddCampo(ldtbDetalleOperacion, "FLG_MONTO_REFERENCIADO", "System.String")
        AddCampo(ldtbDetalleOperacion, "FLG_TIPO_DEPOSITO", "System.String")
        AddCampo(ldtbDetalleOperacion, "FLG_VENDE_TODO", "System.String")
        AddCampo(ldtbDetalleOperacion, "FLG_LIMITE_PRECIO", "System.String")
        AddCampo(ldtbDetalleOperacion, "FECHA_VALUTA", "System.String")
        AddCampo(ldtbDetalleOperacion, "FECHA_LIQUIDACION", "System.String")
        AddCampo(ldtbDetalleOperacion, "FECHA_VENCIMIENTO", "System.String")
        AddCampo(ldtbDetalleOperacion, "ID_NORMATIVO", "System.Double")
        AddCampo(ldtbDetalleOperacion, "ID_PORTAFOLIO", "System.Double")
        AddCampo(ldtbDetalleOperacion, "ID_CARTERA", "System.Double")
        AddCampo(ldtbDetalleOperacion, "FACTOR", "System.Double")

        AddCampo(ldtbDetalleOperacion, "DET_MUEVE_CAJA", "System.String")
        AddCampo(ldtbDetalleOperacion, "DET_MUEVE_CARTERA", "System.String")

        AddCampo(ldtbDetalleOperacion, "ID_CUSTODIO", "System.Double")
        AddCampo(ldtbDetalleOperacion, "COD_TIPO_DOCUMENTO", "System.String")
        AddCampo(ldtbDetalleOperacion, "NUMERO_DOCUMENTO", "System.Double")

        AddCampo(ldtbDetalleOperacion, "AGREGA_DATOS_TRASPASO", "System.String")
        AddCampo(ldtbDetalleOperacion, "NUMERO_EGRESO", "System.String")
        AddCampo(ldtbDetalleOperacion, "NUMERO_CARTA_SVS", "System.String")
        AddCampo(ldtbDetalleOperacion, "FECHA_CARTA_SVS", "System.String")
        AddCampo(ldtbDetalleOperacion, "NUMERO_CARTA_REGISTRO", "System.String")
        AddCampo(ldtbDetalleOperacion, "FECHA_CARTA_REGISTRO", "System.String")
        AddCampo(ldtbDetalleOperacion, "NUMERO_DOCUMENTO_TRASPASO", "System.String")
        AddCampo(ldtbDetalleOperacion, "NUMERO_TITULO", "System.String")
        AddCampo(ldtbDetalleOperacion, "FECHA_TITULO", "System.String")
        AddCampo(ldtbDetalleOperacion, "DATOS_TRASPASO", "System.String")

        AddCampo(ldtbDetalleOperacion, "ID_CONTRAPARTE", "System.Double")
        AddCampo(ldtbDetalleOperacion, "CONTRAPARTE", "System.String")
        AddCampo(ldtbDetalleOperacion, "ID_TRADER", "System.Double")
        AddCampo(ldtbDetalleOperacion, "COD_SUB_CLASE_INSTRUMENTO", "System.String")
        AddCampo(ldtbDetalleOperacion, "IMPUESTO", "System.Double")
        AddCampo(ldtbDetalleOperacion, "TIPO_CUSTODIA", "System.String")
        AddCampo(ldtbDetalleOperacion, "ING_CANTIDAD_INICIAL", "System.String")
        AddCampo(ldtbDetalleOperacion, "ING_PORC_VALOR_PAR", "System.String")

        Return ldtbDetalleOperacion
    End Function

    Public Function OperacionesDiarias_Ver(ByVal strFechaDesde As String, _
                                            ByVal strFechaHasta As String, _
                                            ByVal dblIdCuenta As Double, _
                                            ByVal dblIdOperacion As Double, _
                                            ByVal dblIdInstrumento As Double, _
                                            ByVal dblIdAsesor As Double, _
                                            ByVal strCodTipoOperacion As String, _
                                            ByVal strFiltroIngresoEgreso As String, _
                                            ByVal dblIdGrupo As Double, _
                                            ByVal dblIdCliente As Double, _
                                            ByVal strCodClase As String, _
                                            ByVal strCodSubClase As String, _
                                            ByRef strRetorno As String) As DataSet

        Dim LDS_Portafolio As New DataSet
        Dim LDS_Portafolio_Aux As New DataSet
        Dim sProcedimiento As String
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        sProcedimiento = "Rcp_Reporte_BlotterOperacionesDiarias_Buscar"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(sProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = sProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pFechaDesde", SqlDbType.Char, 10).Value = strFechaDesde
            SQLCommand.Parameters.Add("pFechaHasta", SqlDbType.Char, 10).Value = strFechaHasta
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = IIf(dblIdCuenta = 0, 0, dblIdCuenta)
            SQLCommand.Parameters.Add("pIdOperacion", SqlDbType.Float, 10).Value = IIf(dblIdOperacion = 0, 0, dblIdOperacion)
            SQLCommand.Parameters.Add("pInstrumento", SqlDbType.Float, 10).Value = IIf(dblIdInstrumento = 0, 0, dblIdInstrumento)
            SQLCommand.Parameters.Add("pIdAsesor", SqlDbType.Float, 10).Value = IIf(dblIdAsesor = 0, 0, dblIdAsesor)
            SQLCommand.Parameters.Add("pCodTipoOperacion", SqlDbType.VarChar, 10).Value = IIf(strCodTipoOperacion = "", " ", strCodTipoOperacion)
            SQLCommand.Parameters.Add("pFiltroIngresoEgreso", SqlDbType.Char, 1).Value = strFiltroIngresoEgreso
            SQLCommand.Parameters.Add("pIdGrupo", SqlDbType.Float, 10).Value = IIf(dblIdGrupo = 0, 0, dblIdGrupo)
            SQLCommand.Parameters.Add("pIdCliente", SqlDbType.Float, 10).Value = IIf(dblIdCliente = 0, 0, dblIdCliente)
            SQLCommand.Parameters.Add("pCodClaseInstrumento", SqlDbType.VarChar, 15).Value = IIf(strCodClase = "", DBNull.Value, strCodClase)
            SQLCommand.Parameters.Add("pCodSubClaseInstrumento", SqlDbType.VarChar, 15).Value = IIf(strCodSubClase = "", DBNull.Value, strCodSubClase)
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Float, 10).Value = glngIdUsuario
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Float, 10).Value = glngNegocioOperacion


            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Portafolio)

            LDS_Portafolio_Aux = LDS_Portafolio
            strRetorno = "OK"
            Return LDS_Portafolio_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function Operaciones_CambiarCustodio(ByVal pIdOperacionDetalle As Integer, _
                                                ByVal pIdCustodio As Integer, _
                                                ByVal pTipoCustodia As String) As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion
        Try

            SQLCommand = New SqlClient.SqlCommand("Rcp_Operacion_CambiaCustodio", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Operacion_CambiaCustodio"
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdOperacionDetalle", SqlDbType.Int, 8).Value = pIdOperacionDetalle
            SQLCommand.Parameters.Add("pIdCustodio", SqlDbType.Int).Value = pIdCustodio
            SQLCommand.Parameters.Add("pTipoCustodia", SqlDbType.VarChar, 1).Value = pTipoCustodia

            '...Resultado
            Dim ParametroSal As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If Trim(strDescError) = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If
        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error al cambiar Custodio " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
        End Try

        Return strDescError
    End Function

    Public Function Operaciones_BuscarDetalleOpe(ByVal dblIdOperacionDetalle As Double, _
                                      ByVal strColumnas As String, _
                                      ByRef strRetorno As String) As DataSet

        Dim lDS_Operaciones As New DataSet
        Dim lDS_Operaciones_Aux As New DataSet
        Dim larr_NombreColumna() As String = Split(strColumnas, ",")
        Dim lInt_Col As Integer = 0
        Dim lInt_NomCol As String = ""
        Dim lstr_NombreTabla As String = ""
        Dim lstrColumna As DataColumn
        Dim lblnRemove As Boolean = True
        Dim lstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        lstrProcedimiento = "Rcp_Operacion_BuscarDetalleOpe"
        lstr_NombreTabla = "OPERACIONES"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = lstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("pIdOperacionDetalle", SqlDbType.Float, 10).Value = dblIdOperacionDetalle

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(lDS_Operaciones, lstr_NombreTabla)

            lDS_Operaciones_Aux = lDS_Operaciones

            If strColumnas.Trim = "" Then
                lDS_Operaciones_Aux = lDS_Operaciones
            Else
                lDS_Operaciones_Aux = lDS_Operaciones.Copy
                For Each lstrColumna In lDS_Operaciones.Tables(lstr_NombreTabla).Columns
                    lblnRemove = True
                    For lInt_Col = 0 To larr_NombreColumna.Length - 1
                        lInt_NomCol = larr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                        If lstrColumna.ColumnName = lInt_NomCol Then
                            lblnRemove = False
                        End If
                    Next
                    If lblnRemove Then
                        lDS_Operaciones_Aux.Tables(lstr_NombreTabla).Columns.Remove(lstrColumna.ColumnName)
                    End If
                Next
            End If

            For lInt_Col = 0 To larr_NombreColumna.Length - 1
                lInt_NomCol = larr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                For Each lstrColumna In lDS_Operaciones_Aux.Tables(lstr_NombreTabla).Columns
                    If lstrColumna.ColumnName = lInt_NomCol Then
                        lstrColumna.SetOrdinal(lInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            lDS_Operaciones.Dispose()
            Return lDS_Operaciones_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function
    Public Function TraerAnalisisOrdenesOperaciones(ByVal strFechaProceso As String, _
                                                    ByVal lngIdContraparte As Long, _
                                                    ByVal strClaseInstrumento As String, _
                                                    ByVal strSubClaseInstrumento As String, _
                                                    ByVal lngIdCuenta As Long, _
                                                    ByVal StrNemo As String, _
                                                    ByVal lngTipoRep As Long, _
                                                    ByVal strTipoOper As String, _
                                                    ByRef strRetorno As String) As DataSet

        Dim LDS_AnalisisOrdenesOperaciones As New DataSet
        Dim LDS_Aux As New DataSet
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim LstrProcedimiento As String
        Dim Lstr_NombreTabla As String

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_AnalisisOrdenesOperaciones_Buscar"
        Lstr_NombreTabla = "Analisis_Ordenes_Operaciones"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pFechaProc", SqlDbType.Char, 10).Value = strFechaProceso
            SQLCommand.Parameters.Add("pIdContraparte", SqlDbType.Float, 10).Value = IIf(lngIdContraparte = 0, DBNull.Value, lngIdContraparte)
            SQLCommand.Parameters.Add("pCodClase", SqlDbType.VarChar, 15).Value = IIf(strClaseInstrumento = "", DBNull.Value, strClaseInstrumento)
            SQLCommand.Parameters.Add("pCodSubClase", SqlDbType.VarChar, 10).Value = IIf(strSubClaseInstrumento = "", DBNull.Value, strSubClaseInstrumento)
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = IIf(lngIdCuenta = 0, DBNull.Value, lngIdCuenta)
            SQLCommand.Parameters.Add("pNemotecnico", SqlDbType.Char, 50).Value = IIf(StrNemo = "", DBNull.Value, StrNemo)
            SQLCommand.Parameters.Add("pTipoRep", SqlDbType.Float, 10).Value = IIf(lngTipoRep = 0, DBNull.Value, lngTipoRep)
            SQLCommand.Parameters.Add("pTipoOper", SqlDbType.VarChar, 10).Value = IIf(strTipoOper = "", DBNull.Value, strTipoOper)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_AnalisisOrdenesOperaciones, Lstr_NombreTabla)

            LDS_Aux = LDS_AnalisisOrdenesOperaciones

            strRetorno = "OK"
            Return LDS_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function Operaciones_Spot(ByVal strpFecha As String, _
                                    ByVal strpColumnas As String, _
                                    ByRef strpRetorno As String) As DataSet

        Dim lDS_Operaciones As New DataSet
        Dim lDS_Operaciones_Aux As New DataSet
        Dim larr_NombreColumna() As String = Split(strpColumnas, ",")
        Dim lInt_Col As Integer = 0
        Dim lInt_NomCol As String = ""
        Dim lstr_NombreTabla As String = ""
        Dim lstrColumna As DataColumn
        Dim lblnRemove As Boolean = True
        Dim lstrProcedimiento As String = ""
        Dim strDescError As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        lstrProcedimiento = "Rcp_Operacion_Consultar_Spot"
        lstr_NombreTabla = "OPERACIONES"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, Connect.Conexion)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = lstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pFecha", SqlDbType.Char, 10).Value = strpFecha
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Float, 10).Value = glngNegocioOperacion

            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()
            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(lDS_Operaciones, lstr_NombreTabla)

            lDS_Operaciones_Aux = lDS_Operaciones

            If strDescError.ToUpper.Trim <> "OK" Then
                gFun_ResultadoMsg("2|" & strDescError, "Buscar Cuentas Spot")
                strpRetorno = "OK"
                lDS_Operaciones_Aux = Nothing
            Else
                If strpColumnas.Trim = "" Then
                    lDS_Operaciones_Aux = lDS_Operaciones
                Else
                    lDS_Operaciones_Aux = lDS_Operaciones.Copy
                    For Each lstrColumna In lDS_Operaciones.Tables(lstr_NombreTabla).Columns
                        lblnRemove = True
                        For lInt_Col = 0 To larr_NombreColumna.Length - 1
                            lInt_NomCol = larr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                            If lstrColumna.ColumnName = lInt_NomCol Then
                                lblnRemove = False
                            End If
                        Next
                        If lblnRemove Then
                            lDS_Operaciones_Aux.Tables(lstr_NombreTabla).Columns.Remove(lstrColumna.ColumnName)
                        End If
                    Next
                End If

                For lInt_Col = 0 To larr_NombreColumna.Length - 1
                    lInt_NomCol = larr_NombreColumna(lInt_Col).TrimEnd.TrimStart
                    For Each lstrColumna In lDS_Operaciones_Aux.Tables(lstr_NombreTabla).Columns
                        If lstrColumna.ColumnName = lInt_NomCol Then
                            lstrColumna.SetOrdinal(lInt_Col)
                            Exit For
                        End If
                    Next
                Next

                strpRetorno = "OK"
            End If
            lDS_Operaciones.Dispose()
            Return lDS_Operaciones_Aux

        Catch ex As Exception
            strpRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function
    Public Function Guardar_Operaciones_RV_BICE(ByVal Registro As String, _
                                         ByVal pCodMoneda As String, _
                                         ByVal pCodTipoOperacion As String, _
                                         ByVal pFechaOperacion As String, _
                                         ByVal pFechaVigencia As String, _
                                         ByVal pFechaLiquidacion As String, _
                                         ByVal pCuenta_gpi As String, _
                                         ByVal pIdOperacionConcepto As Integer, _
                                         ByVal pDscOperacion As String, _
                                         ByVal Pmonto_operac As Decimal) As String

        Dim strDescError As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion

        Dim lstrProcedimiento As String
        Dim Lstr_NombreTabla As String
        Dim LDS_Rel_Entidad As New DataSet

        lstrProcedimiento = "RCP_PROCESO_GUARDA_OPERACIONES_RV"
        Lstr_NombreTabla = "OPERACIONES"
        SQLConnect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, SQLConnect.Conexion)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = lstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()




            SQLCommand.Parameters.Add("CADENA", SqlDbType.Text).Value = Registro

            SQLCommand.Parameters.Add("pCodMoneda", SqlDbType.VarChar, 3).Value = pCodMoneda
            SQLCommand.Parameters.Add("pCodTipoOperacion", SqlDbType.VarChar, 10).Value = pCodTipoOperacion
            SQLCommand.Parameters.Add("pIdRepresentante", SqlDbType.Int, 18).Value = DBNull.Value
            SQLCommand.Parameters.Add("pIdSistemaConfirmacion", SqlDbType.Int, 18).Value = DBNull.Value
            '  SQLCommand.Parameters.Add("pIdTipoEstado", SqlDbType.Int, 18).Value = 1 LO TENGO EN EL PROCEDURE
            SQLCommand.Parameters.Add("pCodEstado", SqlDbType.VarChar, 3).Value = "C"
            SQLCommand.Parameters.Add("pIdTrader", SqlDbType.Int, 18).Value = DBNull.Value
            ' SQLCommand.Parameters.Add("pIdContraparte", SqlDbType.Int, 18).Value = pId_Contraparte  'LO TENGO EN EL PROCEDURE

            SQLCommand.Parameters.Add("pFechaOperacion", SqlDbType.Char, 10).Value = pFechaOperacion.Substring(0, 10)
            SQLCommand.Parameters.Add("pFechaVigencia", SqlDbType.Char, 10).Value = pFechaVigencia.Substring(0, 10)
            SQLCommand.Parameters.Add("pFechaLiquidacion", SqlDbType.Char, 10).Value = pFechaLiquidacion.Substring(0, 10)
            SQLCommand.Parameters.Add("pFlgTipoOrigen", SqlDbType.Char, 1).Value = "A"
            SQLCommand.Parameters.Add("pCuenta_Gpi", SqlDbType.VarChar, 15).Value = pCuenta_gpi
            SQLCommand.Parameters.Add("pIdOperacionConcepto", SqlDbType.Int, 3).Value = pIdOperacionConcepto
            SQLCommand.Parameters.Add("pDscOperacion", SqlDbType.VarChar, 200).Value = pDscOperacion



            'SQLCommand.Parameters.Add("pIdSistemaOrigen", SqlDbType.Int, 18).Value = dtrOperacionEncabezado("ID_SISTEMA_ORIGEN")
            'SQLCommand.Parameters.Add("pCodMoneda", SqlDbType.VarChar, 3).Value = dtrOperacionEncabezado("COD_MONEDA")
            'SQLCommand.Parameters.Add("pCodTipoOperacion", SqlDbType.VarChar, 10).Value = strTipoOperacionDestino.Trim
            SQLCommand.Parameters.Add("pMontoOperacion", SqlDbType.Decimal, 26).Value = Pmonto_operac
            'SQLCommand.Parameters.Add("PCod_Mercado", SqlDbType.VarChar, 4).Value = DBNull.Value
            'SQLCommand.Parameters.Add("pLineas", SqlDbType.Int, 5).Value = dtbOperacionDetalle.Rows.Count
            'SQLCommand.Parameters.Add("pDetalle1", SqlDbType.VarChar, 8000).Value = lstrDetalle1
            'SQLCommand.Parameters.Add("pComisiones2", SqlDbType.VarChar, 8000).Value = IIf(lstrComisiones = "", DBNull.Value, lstrComisiones)
            'SQLCommand.Parameters.Add("pMediosCobroPago", SqlDbType.VarChar, 8000).Value = IIf(lstrMediosCobroPago = "", DBNull.Value, lstrMediosCobroPago)
            'SQLCommand.Parameters.Add("pDetalle2", SqlDbType.VarChar, 8000).Value = IIf(lstrDetalle2 = "", DBNull.Value, lstrDetalle2)
            'SQLCommand.Parameters.Add("pDetalle3", SqlDbType.VarChar, 8000).Value = IIf(lstrDetalle3 = "", DBNull.Value, lstrDetalle3)
            'SQLCommand.Parameters.Add("pObservacion", SqlDbType.VarChar, 200).Value = IIf(strObservacion = "", DBNull.Value, strObservacion)
            'SQLCommand.Parameters.Add("pFormaOperarCartera", SqlDbType.VarChar, 15).Value = IIf(strFormaOperarCartera = "", DBNull.Value, strFormaOperarCartera)
            'SQLCommand.Parameters.Add("pFlgEnRueda", SqlDbType.VarChar, 1).Value = IIf(strFlgEnRueda = "", DBNull.Value, strFlgEnRueda)


            Dim ParametroSal As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 1000)
            ParametroSal.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Rel_Entidad, Lstr_NombreTabla)

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError = "OK" Then
                Return strDescError
            Else

                Return strDescError
            End If
        Catch ex As Exception
            strDescError = ex.Message
            Return strDescError
        Finally
            SQLConnect.Cerrar()
        End Try
    End Function
    Public Function Convierte_Asignacion_En_Operaciones(ByVal Registro As String, _
                                                        ByVal Fecha_Proceso As String, _
                                                        ByRef strRetorno As String) As DataSet

        Dim strDescError As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion

        Dim lstrProcedimiento As String
        Dim Lstr_NombreTabla As String
        Dim LDS_Rel_Entidad As New DataSet


        '...Abre la conexion
        lstrProcedimiento = "Rcp_Carga_Y_Asigna_Cierres_RV_BICE"
        Lstr_NombreTabla = "OPERACIONES"
        SQLConnect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, SQLConnect.Conexion)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = lstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pTEXT_CIERRES", SqlDbType.Text).Value = Registro
            SQLCommand.Parameters.Add("pFecha_Proceso", SqlDbType.Char, 10).Value = Fecha_Proceso
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Int, 10).Value = glngNegocioOperacion
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Int, 10).Value = glngIdUsuario

            Dim ParametroSal As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 1000)
            ParametroSal.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Rel_Entidad, Lstr_NombreTabla)

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError = "OK" Or strDescError = "" Then
                strRetorno = "OK"
                Return LDS_Rel_Entidad
            Else
                strRetorno = strDescError
                Return Nothing
            End If
        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            SQLConnect.Cerrar()
        End Try
    End Function

    Public Function Comprobante_Operaciones_Ver(ByVal dblpIdOpeacion As Double, _
                                                ByRef strRetorno As String) As DataSet

        Dim lDS_Comprobante As New DataSet

        Dim lstr_NombreTabla As String = "TABLA"
        Dim lstrProcedimiento As String = "Rcp_Comprobante_Operacion"

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(lstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = lstrProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("@pIdOpeacion", SqlDbType.Float, 10).Value = dblpIdOpeacion

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(lDS_Comprobante, lstr_NombreTabla)

            'lDS_SaldoCajas_Aux = lDS_SaldoCajas

            strRetorno = "OK"
            lDS_Comprobante.Dispose()
            Return lDS_Comprobante

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function BlotterOperacionesDiarias_Ver(ByVal strFechaDesde As String, _
                                                  ByVal strFechaHasta As String, _
                                                  ByVal strLista As String, _
                                                  ByVal strTipoLista As String, _
                                                  ByVal strCodEstado As String, _
                                                  ByVal strCodTipoOperacion As String, _
                                                  ByVal strCodClase As String, _
                                                  ByVal strCodSubClase As String, _
                                                  ByVal dblIdInstrumento As Double, _
                                                  ByVal dblIdEmisor As Double, _
                                                  ByVal strFlgTipoMovimiento As String, _
                                                  ByVal strFlgEnRueda As String, _
                                                  ByRef strRetorno As String) As DataSet

        Dim LDS_Portafolio As New DataSet
        Dim LDS_Portafolio_Aux As New DataSet
        Dim sProcedimiento As String
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        sProcedimiento = "Rcp_Blotter_Operaciones_diarias_BICE"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(sProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = sProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pLista", SqlDbType.Text).Value = strLista
            SQLCommand.Parameters.Add("pTipoLista", SqlDbType.Char, 1).Value = strTipoLista
            SQLCommand.Parameters.Add("pFechaDesde", SqlDbType.Char, 10).Value = strFechaDesde
            SQLCommand.Parameters.Add("pFechaHasta", SqlDbType.Char, 10).Value = strFechaHasta
            SQLCommand.Parameters.Add("pCodEstado", SqlDbType.Char, 1).Value = IIf(strCodEstado = "", DBNull.Value, strCodEstado)
            SQLCommand.Parameters.Add("pCodTipoOperacion", SqlDbType.VarChar, 10).Value = IIf(strCodTipoOperacion = "", DBNull.Value, strCodTipoOperacion)
            SQLCommand.Parameters.Add("pCodClaseInstrumento", SqlDbType.VarChar, 15).Value = IIf(strCodClase = "", DBNull.Value, strCodClase)
            SQLCommand.Parameters.Add("pCodSubClaseInstrumento", SqlDbType.VarChar, 15).Value = IIf(strCodSubClase = "", DBNull.Value, strCodSubClase)
            SQLCommand.Parameters.Add("pIdInstrumento", SqlDbType.Float, 10).Value = IIf(dblIdInstrumento = 0, DBNull.Value, dblIdInstrumento)
            SQLCommand.Parameters.Add("pFlgTipoMovimiento", SqlDbType.Char, 1).Value = IIf(strFlgTipoMovimiento = "", DBNull.Value, strFlgTipoMovimiento)
            SQLCommand.Parameters.Add("pIdEmisor", SqlDbType.Float, 10).Value = IIf(dblIdEmisor = 0, DBNull.Value, dblIdEmisor)
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Float, 10).Value = glngIdUsuario
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Float, 10).Value = glngNegocioOperacion
            SQLCommand.Parameters.Add("pEnRueda", SqlDbType.Char, 1).Value = IIf(strFlgEnRueda = "", DBNull.Value, strFlgEnRueda)

            Dim ParametroSal As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 1000)
            ParametroSal.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal)


            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Portafolio)


            LDS_Portafolio_Aux = LDS_Portafolio
            strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            Return LDS_Portafolio_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function


    Public Function VencimientoFuturos_Ver(ByVal strTipoInforme As String, _
                                           ByVal strFechaCartera As String, _
                                           ByVal strFechaDesde As String, _
                                           ByVal strFechaHasta As String, _
                                           ByVal strCodMoneda As String, _
                                           ByVal strLista As String, _
                                           ByVal strTipoLista As String, _
                                           ByVal strCodClase As String, _
                                           ByVal strCodSubClase As String, _
                                           ByVal dblIdInstrumento As Double, _
                                           ByVal dblIdEmisor As Double, _
                                           ByVal dblIdEjecutivo As Double, _
                                           ByVal strCodMonedaUM As String, _
                                           ByRef strRetorno As String) As DataSet

        Dim LDS_Portafolio As New DataSet
        Dim LDS_Portafolio_Aux As New DataSet
        Dim sProcedimiento As String
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        sProcedimiento = "Rcp_Reporte_Proyeccion_Vencimientos"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(sProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = sProcedimiento
            SQLCommand.CommandTimeout = 0
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pTipoReporte", SqlDbType.Text).Value = strTipoInforme
            SQLCommand.Parameters.Add("pFechaCartera", SqlDbType.VarChar, 10).Value = strFechaCartera
            SQLCommand.Parameters.Add("pFechaDesde", SqlDbType.Char, 10).Value = strFechaDesde
            SQLCommand.Parameters.Add("pFechaHasta", SqlDbType.Char, 10).Value = strFechaHasta
            SQLCommand.Parameters.Add("pCodMoneda", SqlDbType.VarChar, 3).Value = IIf(strCodMoneda = "", DBNull.Value, strCodMoneda)
            SQLCommand.Parameters.Add("pLista", SqlDbType.Text).Value = strLista
            SQLCommand.Parameters.Add("pTipoLista", SqlDbType.Char, 1).Value = strTipoLista
            SQLCommand.Parameters.Add("pCodSubClaseInstrumento", SqlDbType.VarChar, 15).Value = IIf(strCodSubClase = "", DBNull.Value, strCodSubClase)
            SQLCommand.Parameters.Add("pIdInstrumento", SqlDbType.Float, 10).Value = IIf(dblIdInstrumento = 0, DBNull.Value, dblIdInstrumento)
            SQLCommand.Parameters.Add("pIdEmisor", SqlDbType.Float, 10).Value = IIf(dblIdEmisor = 0, DBNull.Value, dblIdEmisor)
            SQLCommand.Parameters.Add("pIdEjecutivo", SqlDbType.Float, 10).Value = IIf(dblIdEjecutivo = 0, DBNull.Value, dblIdEjecutivo)
            SQLCommand.Parameters.Add("pCodMonedaUM", SqlDbType.VarChar, 3).Value = IIf(strCodMoneda = "", DBNull.Value, strCodMoneda)
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.Float, 10).Value = glngNegocioOperacion
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Float, 10).Value = glngIdUsuario

            Dim ParametroSal As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 1000)
            ParametroSal.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal)


            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Portafolio)


            LDS_Portafolio_Aux = LDS_Portafolio
            strRetorno = SQLCommand.Parameters("pResultado").Value.ToString.Trim
            Return LDS_Portafolio_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

End Class
