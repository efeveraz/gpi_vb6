﻿Public Class ClsEtiquetas
    Public Function Etiquetas_Ver(ByVal strIdNegocio As String, _
                                  ByVal strIdCuenta As String, _
                                  ByVal strColumnas As String, _
                                  ByRef strRetorno As String) As DataSet

        Dim LDS_Etiquetas As New DataSet
        Dim LDS_Etiquetas_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Etiquetas_Consultar"
        Lstr_NombreTabla = "Etiquetas"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.VarChar, 10).Value = IIf(strIdNegocio.Trim = "", DBNull.Value, strIdNegocio.Trim)
            SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.VarChar, 10).Value = IIf(strIdCuenta.Trim = "", DBNull.Value, strIdCuenta.Trim)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Etiquetas, Lstr_NombreTabla)

            LDS_Etiquetas_Aux = LDS_Etiquetas

            If strColumnas.Trim = "" Then
                LDS_Etiquetas_Aux = LDS_Etiquetas
            Else
                LDS_Etiquetas_Aux = LDS_Etiquetas.Copy
                For Each Columna In LDS_Etiquetas.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Etiquetas_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Etiquetas_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_Etiquetas.Dispose()
            Return LDS_Etiquetas_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function
    Public Function Etiquetas_Mantenedor(ByVal strId_Cuenta As String, ByRef dstEtiquetasXCuenta As DataSet) As String
        Dim strDescError As String = "OK"
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction
        Dim lstrAccion As String

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try

            SQLCommand = New SqlClient.SqlCommand("Rcp_Etiquetas_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_Etiquetas_Mantencion"

            '+ SE INSERTA, MODIFICA O ELIMINA LOS LAS ETIQUETAS X CUENTA 

            If dstEtiquetasXCuenta.Tables(0).Rows.Count <> Nothing Then
                For Each pRow As DataRow In dstEtiquetasXCuenta.Tables(0).Rows
                    '+ Si hubo modificación en el valor
                    If pRow("VALOR_INI").ToString.Trim <> pRow("VALOR_END").ToString.Trim Then
                        If pRow("VALOR_INI").ToString.Trim = "" Then
                            lstrAccion = "INSERTAR"
                        Else
                            If pRow("VALOR_END").ToString.Trim = "" Then
                                lstrAccion = "ELIMINAR"
                            Else
                                lstrAccion = "MODIFICAR"
                            End If
                        End If
                        SQLCommand.Parameters.Clear()

                        SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 10).Value = lstrAccion
                        SQLCommand.Parameters.Add("pIdCuenta", SqlDbType.Float, 10).Value = strId_Cuenta.Trim
                        SQLCommand.Parameters.Add("pIdEtiqueta", SqlDbType.Float, 10).Value = pRow("ID_ETIQUETA").ToString
                        SQLCommand.Parameters.Add("pValor", SqlDbType.VarChar, 50).Value = pRow("VALOR_END").ToString

                        '...Resultado
                        Dim pSalInstPort As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
                        pSalInstPort.Direction = Data.ParameterDirection.Output
                        SQLCommand.Parameters.Add(pSalInstPort)

                        SQLCommand.ExecuteNonQuery()

                        strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim
                    End If
                    If Not strDescError.ToUpper.Trim = "OK" Then
                        Exit For
                    End If
                Next
            End If

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en la actualización de las etiquetas [" & vbCr & Ex.Message & "]"
        Finally
            SQLConnect.Cerrar()
            Etiquetas_Mantenedor = strDescError
        End Try
    End Function

End Class
