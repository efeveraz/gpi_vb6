﻿Imports System.IO
Imports Microsoft.Office.Interop
Imports System.Data
Imports System.Data.OleDb

Public Class ClsArchivo

    Public Function Archivo_Importa_M_Ver(ByVal lngIdContraparte As Long, _
                                          ByVal strColumnas As String, _
                                          ByVal llngTipo As Long, _
                                          ByRef strRetorno As String, _
                                          Optional ByVal strIngresaOperacion As String = "") As DataSet
        Dim LDS_Comuna As New DataSet
        Dim LDS_Comuna_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        ''Tipo de busqueda: 0)Normal y 1)La concatenada
        If llngTipo = 0 Then
            'Salida Normal
            LstrProcedimiento = "Sis_Archivo_Importa_M_Consultar"
        Else
            'Salida Concatenada
            LstrProcedimiento = "Sis_Archivo_Importa_M_ConsultarCon"
        End If

        Lstr_NombreTabla = "Archivo_Importa_M"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("pIdContraparte", SqlDbType.Float, 10).Value = IIf(lngIdContraparte = 0, DBNull.Value, lngIdContraparte)
            SQLCommand.Parameters.Add("pIngresa_Operaciones", SqlDbType.VarChar, 1).Value = IIf(strIngresaOperacion = "", DBNull.Value, strIngresaOperacion)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Comuna, Lstr_NombreTabla)

            LDS_Comuna_Aux = LDS_Comuna

            If strColumnas.Trim = "" Then
                LDS_Comuna_Aux = LDS_Comuna
            Else
                LDS_Comuna_Aux = LDS_Comuna.Copy
                For Each Columna In LDS_Comuna.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Comuna_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Comuna_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_Comuna.Dispose()
            Return LDS_Comuna_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function Archivo_Importa_M_Consulta_Reportes(ByVal lngIdContraparte As Long, _
                                      ByVal strNombre As String, _
                                      ByVal lngIdUsuario As Long, _
                                      ByRef strRetorno As String _
                                      ) As DataSet

        Dim LDS_Comuna As New DataSet
        Dim LDS_Comuna_Aux As New DataSet
        Dim Lstr_NombreTabla As String = ""
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Sis_Archivo_Importa_M_ConsultarPorContraparte_Usuario"

        Lstr_NombreTabla = "REPORTES"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("pIdContraparte", SqlDbType.Float, 10).Value = IIf(lngIdContraparte = 0, DBNull.Value, lngIdContraparte)
            SQLCommand.Parameters.Add("pNomMaestro", SqlDbType.VarChar, 50).Value = IIf(strNombre = "", DBNull.Value, strNombre)
            SQLCommand.Parameters.Add("pIdUsuario", SqlDbType.Float, 6).Value = glngIdUsuario

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Comuna, Lstr_NombreTabla)

            LDS_Comuna_Aux = LDS_Comuna

            strRetorno = "OK"
            LDS_Comuna.Dispose()
            Return LDS_Comuna_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function Archivo_Importa_D_Ver(ByVal lngId_M As Long, _
                                          ByVal lngId_D As Long, _
                                          ByVal strCampoArchi As String, _
                                          ByVal strCampoInter As String, _
                                          ByVal strCampoRegis As String, _
                                          ByVal lngOrden As Long, _
                                          ByVal strColumnas As String, _
                                          ByRef strRetorno As String) As DataSet

        Dim LDS_Comuna As New DataSet
        Dim LDS_Comuna_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Sis_Archivo_Importa_D_Consultar"
        Lstr_NombreTabla = "Archivo_Importa_D"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("pId_M", SqlDbType.Float, 10).Value = IIf(lngId_M = 0, DBNull.Value, lngId_M)
            SQLCommand.Parameters.Add("pId_D", SqlDbType.Float, 10).Value = IIf(lngId_D = 0, DBNull.Value, lngId_D)
            SQLCommand.Parameters.Add("pCampoArchi", SqlDbType.VarChar, 50).Value = IIf(strCampoArchi = "", DBNull.Value, strCampoArchi)
            SQLCommand.Parameters.Add("pCampoInter", SqlDbType.VarChar, 50).Value = IIf(strCampoInter = "", DBNull.Value, strCampoInter)
            SQLCommand.Parameters.Add("pCampoRegis", SqlDbType.VarChar, 50).Value = IIf(strCampoRegis = "", DBNull.Value, strCampoRegis)
            SQLCommand.Parameters.Add("pOrden", SqlDbType.Int, 5).Value = IIf(lngOrden = 0, DBNull.Value, lngOrden)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Comuna, Lstr_NombreTabla)

            LDS_Comuna_Aux = LDS_Comuna

            If strColumnas.Trim = "" Then
                LDS_Comuna_Aux = LDS_Comuna
            Else
                LDS_Comuna_Aux = LDS_Comuna.Copy
                For Each Columna In LDS_Comuna.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Comuna_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Comuna_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_Comuna.Dispose()
            Return LDS_Comuna_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Sub Archivo_Importa_D_Ver_M(ByRef pDS_Datos As DataSet, _
                                       ByVal strTabla As String, _
                                       ByVal lngId_M As Long, _
                                       ByVal lngId_D As Long, _
                                       ByVal strCampoArchi As String, _
                                       ByVal strCampoInter As String, _
                                       ByVal strCampoRegis As String, _
                                       ByVal lngOrden As Long, _
                                       ByVal strColumnas As String, _
                                       ByRef strRetorno As String)

        Dim LDS_DatosArchivo As New DataSet
        Dim LDS_DatosArchivo_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LDS_DatosArchivo = pDS_Datos

        LstrProcedimiento = "Sis_Archivo_Importa_D_Consultar"
        Lstr_NombreTabla = strTabla

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("pId_M", SqlDbType.Float, 10).Value = IIf(lngId_M = 0, DBNull.Value, lngId_M)
            SQLCommand.Parameters.Add("pId_D", SqlDbType.Float, 10).Value = IIf(lngId_D = 0, DBNull.Value, lngId_D)
            SQLCommand.Parameters.Add("pCampoArchi", SqlDbType.VarChar, 50).Value = IIf(strCampoArchi = "", DBNull.Value, strCampoArchi)
            SQLCommand.Parameters.Add("pCampoInter", SqlDbType.VarChar, 50).Value = IIf(strCampoInter = "", DBNull.Value, strCampoInter)
            SQLCommand.Parameters.Add("pCampoRegis", SqlDbType.VarChar, 50).Value = IIf(strCampoRegis = "", DBNull.Value, strCampoRegis)
            SQLCommand.Parameters.Add("pOrden", SqlDbType.Int, 5).Value = IIf(lngOrden = 0, DBNull.Value, lngOrden)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_DatosArchivo, Lstr_NombreTabla)

            LDS_DatosArchivo_Aux = LDS_DatosArchivo

            If strColumnas.Trim = "" Then
                LDS_DatosArchivo_Aux = LDS_DatosArchivo
            Else
                LDS_DatosArchivo_Aux = LDS_DatosArchivo.Copy
                For Each Columna In LDS_DatosArchivo.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_DatosArchivo_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_DatosArchivo_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            pDS_Datos = LDS_DatosArchivo_Aux
            strRetorno = "OK"
            LDS_DatosArchivo.Dispose()

        Catch ex As Exception
            strRetorno = ex.Message
        Finally
            Connect.Cerrar()
        End Try
    End Sub

    Public Function Archivo_Importa_D_ParaGrilla(ByVal strNomMaestro As String, _
                                                ByVal intIdContraparte As Integer, _
                                                 ByVal strColumnas As String, _
                                                 ByRef strRetorno As String) As DataSet
        Dim LDS_ImportaD As New DataSet
        Dim LDS_ImportaD_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Sis_Archivo_Importa_D_ConsultarPorContraparte"
        Lstr_NombreTabla = "Archivo_Importa_D"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("pIdContraparte", SqlDbType.Int, 10).Value = IIf(intIdContraparte = 0, DBNull.Value, intIdContraparte)
            SQLCommand.Parameters.Add("pNomMaestro", SqlDbType.VarChar, 50).Value = IIf(strNomMaestro = "", DBNull.Value, strNomMaestro)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_ImportaD, Lstr_NombreTabla)

            LDS_ImportaD_Aux = LDS_ImportaD

            If strColumnas.Trim = "" Then
                LDS_ImportaD_Aux = LDS_ImportaD
            Else
                LDS_ImportaD_Aux = LDS_ImportaD.Copy
                For Each Columna In LDS_ImportaD.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_ImportaD_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_ImportaD_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_ImportaD.Dispose()
            Return LDS_ImportaD_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function Archivo_Importa_D_PorPlantilla(ByVal strReporte As String, _
                                                 ByVal intIdContraparte As Integer, _
                                                 ByVal intIdUsuario As String, _
                                                 ByVal strPlantilla As String, _
                                                 ByRef strRetorno As String) As DataSet

        Dim LDS_ImportaD As New DataSet
        Dim LDS_ImportaD_Aux As New DataSet
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Sis_Archivo_Importa_D_ConsultarPorPlantilla"
        Lstr_NombreTabla = "Archivo_Importa_D"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("@pIdContraparte", SqlDbType.Int, 10).Value = IIf(intIdContraparte = 0, DBNull.Value, intIdContraparte)
            SQLCommand.Parameters.Add("@pNomMaestro", SqlDbType.VarChar, 50).Value = IIf(strReporte = "", DBNull.Value, strReporte)
            SQLCommand.Parameters.Add("@pIdUsuario", SqlDbType.Int, 6).Value = glngIdUsuario
            SQLCommand.Parameters.Add("@pPlantilla", SqlDbType.VarChar, 50).Value = IIf(strPlantilla = "", DBNull.Value, strPlantilla)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_ImportaD, Lstr_NombreTabla)

            LDS_ImportaD_Aux = LDS_ImportaD

            strRetorno = "OK"
            LDS_ImportaD.Dispose()
            Return LDS_ImportaD_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function Archivo_Importa_M_PorPlantilla(ByVal strNomMaestro As String, _
                                                 ByVal intIdContraparte As Integer, _
                                                 ByVal intIdUsuario As String, _
                                                 ByVal strPlantilla As String, _
                                                 ByVal strColumnas As String, _
                                                 ByRef strRetorno As String) As DataSet

        Dim LDS_ImportaD As New DataSet
        Dim LDS_ImportaD_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim LstrProcedimiento As String = ""

        Dim Remove As Boolean = True

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion
        'rrh
        LstrProcedimiento = "Sis_Archivo_Importa_M_ConsultarPorPlantilla"
        Lstr_NombreTabla = "Archivo_Importa_D"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("@pIdContraparte", SqlDbType.Int, 10).Value = IIf(intIdContraparte = 0, DBNull.Value, intIdContraparte)
            SQLCommand.Parameters.Add("@pNomMaestro", SqlDbType.VarChar, 50).Value = IIf(strNomMaestro = "", DBNull.Value, strNomMaestro)
            SQLCommand.Parameters.Add("@pIdUsuario", SqlDbType.Int, 6).Value = IIf(intIdUsuario = 0, DBNull.Value, intIdUsuario)
            SQLCommand.Parameters.Add("@pPlantilla", SqlDbType.VarChar, 50).Value = IIf(strPlantilla = "", DBNull.Value, strPlantilla)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_ImportaD, Lstr_NombreTabla)

            LDS_ImportaD_Aux = LDS_ImportaD

            If strColumnas.Trim = "" Then
                LDS_ImportaD_Aux = LDS_ImportaD
            Else
                LDS_ImportaD_Aux = LDS_ImportaD.Copy
                For Each Columna In LDS_ImportaD.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_ImportaD_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_ImportaD_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_ImportaD.Dispose()
            Return LDS_ImportaD_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function ContraParte_Consultar(ByVal lngIdContraparte As Integer, _
                                          ByVal strColumnas As String, _
                                          ByRef strRetorno As String) As DataSet

        Dim LDS_Contraparte As New DataSet
        Dim LDS_Contraparte_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Lstr_NombreTabla As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim LstrProcedimiento

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Sis_Archivo_Importa_M_Contraparte"
        Lstr_NombreTabla = "CONTRAPARTE"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pIdContraparte", SqlDbType.Float, 10).Value = IIf(lngIdContraparte = 0, DBNull.Value, lngIdContraparte)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Contraparte, Lstr_NombreTabla)

            LDS_Contraparte_Aux = LDS_Contraparte

            If strColumnas.Trim = "" Then
                LDS_Contraparte_Aux = LDS_Contraparte
            Else
                LDS_Contraparte_Aux = LDS_Contraparte.Copy
                For Each Columna In LDS_Contraparte.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Contraparte_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Contraparte_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            Return LDS_Contraparte_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function Archivo_Importa_M_Insertar(ByVal intIdContraparte As Integer, _
                              ByVal strTipoArchivo As String, _
                              ByVal strNombreA As String, _
                              ByVal intIdUsuario As Integer, _
                              ByVal strPlantillaReporte As String, _
                              ByVal strGlobalLocal As String) As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("SIS_ARCHIVO_IMPORTA_M_INSERTAR", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "SIS_ARCHIVO_IMPORTA_M_INSERTAR"
            SQLCommand.Parameters.Clear()

            '@pID_CONTRAPARTE    numeric(10, 0),
            '@pTIPO_ARCHIVO      varchar(3),
            '@pNOMBRE_A          varchar(50),
            '@pNOMBRE_H          varchar(50),
            '@pTABLA             varchar(40),
            '@pTIPO_OPER         int,
            '@pTIPO_REL          int,
            '@pTIPO_MOV          int,
            '@pSEPARADOR         char(1),
            '@pID_USUARIO        numeric(6, 0),
            '@pPLANTILLA_REPORTE varchar(50),
            '@pGLOBAL_LOCAL      char(1),
            '@pError             varchar(500) OUTPUT

            SQLCommand.Parameters.Add("@pID_CONTRAPARTE", SqlDbType.Int, 10).Value = intIdContraparte
            SQLCommand.Parameters.Add("@pTIPO_ARCHIVO", SqlDbType.VarChar, 3).Value = strTipoArchivo
            SQLCommand.Parameters.Add("@pNOMBRE_A", SqlDbType.VarChar, 50).Value = strNombreA
            SQLCommand.Parameters.Add("@pNOMBRE_H", SqlDbType.VarChar, 50).Value = DBNull.Value
            SQLCommand.Parameters.Add("@pTABLA", SqlDbType.VarChar, 40).Value = DBNull.Value
            SQLCommand.Parameters.Add("@pTIPO_OPER", SqlDbType.Int, 10).Value = DBNull.Value
            SQLCommand.Parameters.Add("@pTIPO_REL", SqlDbType.Int, 10).Value = DBNull.Value
            SQLCommand.Parameters.Add("@pTIPO_MOV", SqlDbType.Int, 10).Value = DBNull.Value
            SQLCommand.Parameters.Add("@pSEPARADOR", SqlDbType.Char, 1).Value = DBNull.Value
            SQLCommand.Parameters.Add("@pID_USUARIO", SqlDbType.Int, 6).Value = glngIdUsuario
            SQLCommand.Parameters.Add("@pPLANTILLA_REPORTE", SqlDbType.VarChar, 50).Value = strPlantillaReporte
            SQLCommand.Parameters.Add("@pGLOBAL_LOCAL", SqlDbType.Char, 1).Value = strGlobalLocal

            '...Resultado
            Dim ParametroSal2 As New SqlClient.SqlParameter("@pError", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("@pError").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en la insersión de " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            Archivo_Importa_M_Insertar = strDescError
        End Try
    End Function

    Public Function Archivo_Importa_D_Insertar(ByVal intIdArchivoImportarM As Integer, _
                              ByVal strCampo As String, _
                              ByVal strCampoEtiqueta As String, _
                              ByVal strFormato As String, _
                              ByVal intIncidencia As Integer, _
                              ByVal intTipoDato As Integer, _
                              ByVal intFormatoCampo As Integer, _
                              ByVal intLargo As Integer, _
                              ByVal intDecimales As Integer, _
                              ByVal intOrden As Integer) As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("SIS_ARCHIVO_IMPORTA_D_INSERTAR", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "SIS_ARCHIVO_IMPORTA_D_INSERTAR"
            SQLCommand.Parameters.Clear()

            '@pId_Sis_Archivo_Importa_M numeric(10,0), 
            '@pCampo                    varchar(50),
            '@pCampo_Inter              varchar(50),
            '@pCampo_Etiqueta           varchar(50),
            '@pFormato                  varchar(100),
            '@pIncidencia               int,
            '@pValor_Defecto            varchar(100),
            '@pTipo_Dato                int,
            '@pFORMATO_CAMPO            int,
            '@pLARGO                    int,
            '@pDECIMALES                int,
            '@pTIPO_REGISTRO_GPI        int,
            '@pCAMPO_REGISTRO_GPI       varchar(50),
            '@pTABLA_ENTIDAD_GPI        varchar(40),
            '@pORDEN                    int,
            '@pError             varchar(500) OUTPUT

            SQLCommand.Parameters.Add("@pId_Sis_Archivo_Importa_M", SqlDbType.Int, 10).Value = intIdArchivoImportarM
            SQLCommand.Parameters.Add("@pCampo", SqlDbType.VarChar, 50).Value = strCampo
            SQLCommand.Parameters.Add("@pCampo_Inter", SqlDbType.VarChar, 50).Value = DBNull.Value
            SQLCommand.Parameters.Add("@pCampo_Etiqueta", SqlDbType.VarChar, 50).Value = strCampoEtiqueta
            SQLCommand.Parameters.Add("@pFormato", SqlDbType.VarChar, 100).Value = strFormato
            SQLCommand.Parameters.Add("@pIncidencia", SqlDbType.Int, 10).Value = 1
            SQLCommand.Parameters.Add("@pValor_Defecto", SqlDbType.VarChar, 100).Value = DBNull.Value
            SQLCommand.Parameters.Add("@pTipo_Dato", SqlDbType.Int, 10).Value = intTipoDato
            SQLCommand.Parameters.Add("@pFORMATO_CAMPO", SqlDbType.Int, 10).Value = intFormatoCampo
            SQLCommand.Parameters.Add("@pLARGO", SqlDbType.Int, 10).Value = intLargo
            SQLCommand.Parameters.Add("@pDECIMALES", SqlDbType.Int, 10).Value = intDecimales
            SQLCommand.Parameters.Add("@pTIPO_REGISTRO_GPI", SqlDbType.Int, 10).Value = DBNull.Value
            SQLCommand.Parameters.Add("@pCAMPO_REGISTRO_GPI", SqlDbType.VarChar, 50).Value = DBNull.Value
            SQLCommand.Parameters.Add("@pTABLA_ENTIDAD_GPI", SqlDbType.VarChar, 40).Value = DBNull.Value
            SQLCommand.Parameters.Add("@pORDEN", SqlDbType.Int, 10).Value = intOrden

            '...Resultado
            Dim ParametroSal2 As New SqlClient.SqlParameter("@pError", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("@pError").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en la insersión de " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            Archivo_Importa_D_Insertar = strDescError
        End Try
    End Function

    Public Function Archivo_Importa_D_Actualizar(ByVal intIdArchivoImportarD As Integer, _
                          ByVal intIdArchivoImportarM As Integer, _
                          ByVal strCampo As String, _
                          ByVal strCampoEtiqueta As String, _
                          ByVal strFormato As String, _
                          ByVal intIncidencia As Integer, _
                          ByVal intTipoDato As Integer, _
                          ByVal intFormatoCampo As Integer, _
                          ByVal intLargo As Integer, _
                          ByVal intDecimales As Integer, _
                          ByVal intOrden As Integer, _
                          ByVal strGlobalLocal As String) As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("SIS_ARCHIVO_IMPORTA_D_MODIFICAR", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "SIS_ARCHIVO_IMPORTA_D_MODIFICAR"
            SQLCommand.Parameters.Clear()

            '@pId_Sis_Archivo_Importa_D numeric(10, 0)
            '@pId_Sis_Archivo_Importa_M numeric(10,0), 
            '@pCampo                    varchar(50),
            '@pCampo_Inter              varchar(50),
            '@pCampo_Etiqueta           varchar(50),
            '@pFormato                  varchar(100),
            '@pIncidencia               int,
            '@pValor_Defecto            varchar(100),
            '@pTipo_Dato                int,
            '@pFORMATO_CAMPO            int,
            '@pLARGO                    int,
            '@pDECIMALES                int,
            '@pTIPO_REGISTRO_GPI        int,
            '@pCAMPO_REGISTRO_GPI       varchar(50),
            '@pTABLA_ENTIDAD_GPI        varchar(40),
            '@pORDEN                    int,
            '@pError             varchar(500) OUTPUT

            SQLCommand.Parameters.Add("@pId_Sis_Archivo_Importa_D", SqlDbType.Int, 10).Value = intIdArchivoImportarD
            SQLCommand.Parameters.Add("@pId_Sis_Archivo_Importa_M", SqlDbType.Int, 10).Value = intIdArchivoImportarM
            SQLCommand.Parameters.Add("@pCampo", SqlDbType.VarChar, 50).Value = strCampo
            SQLCommand.Parameters.Add("@pCampo_Inter", SqlDbType.VarChar, 50).Value = DBNull.Value
            SQLCommand.Parameters.Add("@pCampo_Etiqueta", SqlDbType.VarChar, 50).Value = strCampoEtiqueta
            SQLCommand.Parameters.Add("@pFormato", SqlDbType.VarChar, 100).Value = strFormato
            SQLCommand.Parameters.Add("@pIncidencia", SqlDbType.Int, 10).Value = 1
            SQLCommand.Parameters.Add("@pValor_Defecto", SqlDbType.VarChar, 100).Value = DBNull.Value
            SQLCommand.Parameters.Add("@pTipo_Dato", SqlDbType.Int, 10).Value = intTipoDato
            SQLCommand.Parameters.Add("@pFORMATO_CAMPO", SqlDbType.Int, 10).Value = intFormatoCampo
            SQLCommand.Parameters.Add("@pLARGO", SqlDbType.Int, 10).Value = intLargo
            SQLCommand.Parameters.Add("@pDECIMALES", SqlDbType.Int, 10).Value = intDecimales
            SQLCommand.Parameters.Add("@pTIPO_REGISTRO_GPI", SqlDbType.Int, 10).Value = DBNull.Value
            SQLCommand.Parameters.Add("@pCAMPO_REGISTRO_GPI", SqlDbType.VarChar, 50).Value = DBNull.Value
            SQLCommand.Parameters.Add("@pTABLA_ENTIDAD_GPI", SqlDbType.VarChar, 40).Value = DBNull.Value
            SQLCommand.Parameters.Add("@pGlobal_Local", SqlDbType.Char, 1).Value = strGlobalLocal
            SQLCommand.Parameters.Add("@pORDEN", SqlDbType.Int, 10).Value = intOrden

            '...Resultado
            Dim ParametroSal2 As New SqlClient.SqlParameter("@pError", SqlDbType.VarChar, 500)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("@pError").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en la insersión de " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            Archivo_Importa_D_Actualizar = strDescError
        End Try
    End Function

    Public Function Carga_Grilla_Detalle_Plantilla(ByVal intIdContraparte As Integer, _
                                                   ByVal intDetallePlantilla As Integer, _
                                                   ByVal strPlantilla As String, _
                                                   ByVal strColumnas As String, _
                                                   ByRef strRetorno As String) As DataSet
        'Devuelve los datos a la Grilla
        Dim LDS_ImportaD As New DataSet
        Dim LDS_ImportaD_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Sis_Plantilla_Detalle_Consultar"
        Lstr_NombreTabla = "Plantilla_Reporte"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("pIdContraparte", SqlDbType.Int, 10).Value = IIf(intIdContraparte = 0, DBNull.Value, intIdContraparte)
            SQLCommand.Parameters.Add("pIdDetallePlantilla", SqlDbType.Int, 10).Value = IIf(intDetallePlantilla = 0, DBNull.Value, intDetallePlantilla)
            SQLCommand.Parameters.Add("pPlantilla", SqlDbType.VarChar, 50).Value = IIf(strPlantilla = "", DBNull.Value, strPlantilla)

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_ImportaD, Lstr_NombreTabla)

            LDS_ImportaD_Aux = LDS_ImportaD

            If strColumnas.Trim = "" Then
                LDS_ImportaD_Aux = LDS_ImportaD
            Else
                LDS_ImportaD_Aux = LDS_ImportaD.Copy
                For Each Columna In LDS_ImportaD.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_ImportaD_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_ImportaD_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_ImportaD.Dispose()
            Return LDS_ImportaD_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Sub GrabaPlantillasReporte(ByVal strPlantillas As String, _
                                           ByVal intIdContraparte As Integer, _
                                            ByVal intIdDetalle As Integer, _
                                            ByVal StrAccion As String, _
                                            ByRef Reg_Leidos As Integer, _
                                            ByRef Reg_Grabados As Integer, _
                                            ByRef strRetorno As String)
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Dim lstrProc As String = "RCP_PLANTILLA_REPORTE_MANTENCION"

        'Abre la conexion
        Connect.Abrir()

        Dim MiTransaccion As SqlClient.SqlTransaction

        Reg_Leidos = 0
        Reg_Grabados = 0

        MiTransaccion = Connect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand(lstrProc, MiTransaccion.Connection, MiTransaccion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = lstrProc

            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pPlantillas", SqlDbType.Text).Value = strPlantillas
            SQLCommand.Parameters.Add("pIdContraparte", SqlDbType.Int).Value = intIdContraparte
            SQLCommand.Parameters.Add("pIdDetalle", SqlDbType.Int).Value = intIdDetalle
            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 50).Value = IIf(StrAccion = "", DBNull.Value, StrAccion)

            Dim pReg_Leidos As New SqlClient.SqlParameter("pReg_Leidos", SqlDbType.Int, 10)
            pReg_Leidos.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pReg_Leidos).Value = Reg_Leidos

            Dim pReg_Grabados As New SqlClient.SqlParameter("pReg_Grabados", SqlDbType.Int, 10)
            pReg_Grabados.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(pReg_Grabados).Value = Reg_Grabados

            Dim ParametroSal2 As New SqlClient.SqlParameter("pError", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()

            Reg_Leidos = SQLCommand.Parameters("pReg_Leidos").Value
            Reg_Grabados = SQLCommand.Parameters("pReg_Grabados").Value
            strRetorno = SQLCommand.Parameters("pError").Value.ToString.Trim


            If Trim(strRetorno) = "OK" Then
                MiTransaccion.Commit()
            Else
                MiTransaccion.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccion.Rollback()
            strRetorno = "Error al grabar Publicador de Precios." & vbCr & Ex.Message
        Finally
            Connect.Cerrar()
        End Try
    End Sub

    Public Sub CargaArchivo_txt(ByVal pNombreArchivo As String, _
                                ByVal pFecha As Date, _
                                ByVal pTabla As String, _
                                ByVal pSeparador As String, _
                                ByVal pIdArchivoM As Double, _
                                ByVal pRegInicio As Integer, _
                                ByRef pDataSet As DataSet, _
                                ByVal pTitulo As String, _
                                ByRef pMensaje As String)

        '' Nombre de archivo dinamico 
        'Dim lintIni As Integer
        'Dim lIniFin As Integer
        'Dim lintLargo As Integer
        'Dim lstrFormatoFecha As String
        'Dim lstrNombreArchivo As String
        'lintIni = pNombreArchivo.IndexOf("&") + 1
        'lIniFin = pNombreArchivo.IndexOf("&", lintIni)
        'lintLargo = lIniFin - lintIni
        'lstrFormatoFecha = pNombreArchivo.Substring(lintIni, lintLargo)
        'lstrNombreArchivo = pNombreArchivo
        'lstrNombreArchivo = Replace(lstrNombreArchivo, ("&" + lstrFormatoFecha + "&"), Format(pFecha, lstrFormatoFecha))

        'Nombre de archivo dinamico 
        Dim lintIni As Integer
        Dim lIniFin As Integer
        Dim lintLargo As Integer
        Dim lstrFormatoFecha As String
        Dim lstrNombreArchivo As String
        lintIni = pNombreArchivo.IndexOf("&") + 1
        lIniFin = pNombreArchivo.IndexOf("&", lintIni)
        lintLargo = lIniFin - lintIni
        If lintLargo > 0 Then
            lstrFormatoFecha = pNombreArchivo.Substring(lintIni, lintLargo)
            lstrNombreArchivo = pNombreArchivo
            lstrNombreArchivo = Replace(lstrNombreArchivo, ("&" + lstrFormatoFecha + "&"), Format(pFecha, lstrFormatoFecha))
            pNombreArchivo = lstrNombreArchivo
        End If
        '-----
        Dim lstrMensajeResumen As String = ""
        Dim lintIndiceCol As Integer = -1
        Dim Ds_Archivo_D = New DataSet
        Dim lCountError As Integer = 0
        Dim lCountArch As Integer = 0

        pMensaje = "OK"

        Dim contador As Integer = 0
        Dim lint_cont As Integer = 0
        Dim expression As String
        expression = "campo is not null and incidencia in (0,1)"
        Dim DtbDatos As DataTable = New DataTable(pTabla)
        Dim ldtrFila As DataRow
        Dim str_Tipo_Dato As String = ""
        Dim lintTitulo As Integer = 0
        Dim Lint_tipo_dato As Integer = 0

        If pTitulo = "T" Then lintTitulo = 1
        DtbDatos.Columns.Clear()

        'If pRegInicio = 0 Then
        Call Archivo_Importa_D_Ver_M(Ds_Archivo_D, pTabla, pIdArchivoM, 0, "", "", "", 0, "", lstrMensajeResumen)
        'lintIndiceCol = -1
        For Each dr As DataRow In Ds_Archivo_D.Tables(pTabla).Select(expression)
            contador += 1
        Next
        contador = contador - 1

        Dim fieldWidths(contador) As Integer

        For Each dr As DataRow In Ds_Archivo_D.Tables(pTabla).Select(expression, "ORDEN ASC")
            If dr("CAMPO") <> "" Then
                Select Case dr("TIPO_DATO")
                    Case 0
                        str_Tipo_Dato = "System.Double"
                    Case 1
                        str_Tipo_Dato = "System.String"
                    Case 2
                        str_Tipo_Dato = "System.String"
                End Select
                CrearCampo(dr("CAMPO"), System.Type.GetType(str_Tipo_Dato), DtbDatos)
            End If
            fieldWidths(lint_cont) = dr("LARGO")
            lint_cont += 1
        Next
        'If pTabla = "OPERACION" Or pTabla = "MOVIMIENTO_CAJA" Then CrearCampo("TIPO_MOVIMIENTO", System.Type.GetType("System.String"), DtbDatos)

        'Carga archivo en un arreglo 
        Dim objReader As New StreamReader(pNombreArchivo)
        Dim arrText As New ArrayList()
        Dim sLine As String = ""
        Dim StrFechaMov As String = ""
        Dim StrFechaLiq As String = ""

        Do
            sLine = objReader.ReadLine()
            If Not sLine Is Nothing Then
                If lintTitulo = 1 Then
                    lintTitulo = 0
                Else
                    arrText.Add(sLine)
                End If
            End If

        Loop Until sLine Is Nothing
        objReader.Close()

        If pTabla = "MOVIMIENTO_CAJA" Then
            Dim lstrValores() As String
            Dim lsprimeravez As String = "SI"
            'Dim ldtrFila As DataRow
            Dim lNumColumnas As Integer = 0

            Try
                For Each sLine In arrText
                    Try
                        lstrValores = sLine.Split(pSeparador)
                        lCountArch = lCountArch + 1
                        If lNumColumnas <> 0 Then
                            If lstrValores.Length <> lNumColumnas Then
                                lCountError = lCountError + 1
                                Exit Try
                            End If
                        Else
                            lNumColumnas = lstrValores.Length
                        End If
                        ldtrFila = DtbDatos.NewRow

                        'For indice As Integer = 0 To lNumColumnas - 1 Step 1
                        For indice As Integer = 0 To contador '- 1 Step 1
                            Lint_tipo_dato = Ds_Archivo_D.Tables(0).Rows(indice).Item("TIPO_DATO")
                            Select Case Lint_tipo_dato
                                Case 0
                                    If gtxtSeparadorDecimal = "," Then
                                        ldtrFila(indice) = lstrValores(indice).ToString.Replace(".", ",")
                                    Else
                                        ldtrFila(indice) = lstrValores(indice).ToString.Replace(",", ".")
                                    End If
                                Case 1
                                    ldtrFila(indice) = lstrValores(indice)
                                Case 2
                                    ldtrFila(indice) = IIf(IsDBNull(Format(CDate(lstrValores(indice)), "yyyyMMdd")), "", Format(CDate(lstrValores(indice)), "yyyyMMdd"))
                            End Select
                        Next
                        DtbDatos.Rows.Add(ldtrFila)
                    Catch ex As Exception
                        lCountError = lCountError + 1
                    End Try
                Next
                pDataSet.Tables.Add(DtbDatos)

            Catch ex As Exception
                pMensaje = ex.Message
            End Try
        Else
            ' Pasa arreglo con los datos del archivo a un Dataset 
            Dim lstrValores() As String
            Dim lsprimeravez As String = "SI"
            'Dim ldtrFila As DataRow
            Dim lNumColumnas As Integer = 0
            Try
                For Each sLine In arrText
                    Try
                        lstrValores = sLine.Split(pSeparador)
                        lCountArch = lCountArch + 1
                        If lNumColumnas <> 0 Then
                            If lstrValores.Length <> lNumColumnas Then
                                Exit Try
                            End If
                        Else
                            lNumColumnas = lstrValores.Length
                        End If
                        ldtrFila = DtbDatos.NewRow

                        'For indice As Integer = 0 To lNumColumnas - 1 Step 1
                        For indice As Integer = 0 To contador '- 1 Step 1
                            Lint_tipo_dato = Ds_Archivo_D.Tables(0).Rows(indice).Item("TIPO_DATO")
                            Select Case Lint_tipo_dato
                                Case 0
                                    If IsDBNull(lstrValores(indice)) Or lstrValores(indice) = "" Then
                                        ldtrFila(indice) = 0
                                    Else
                                        If gtxtSeparadorDecimal = "," Then
                                            ldtrFila(indice) = CDbl(lstrValores(indice).ToString.Replace(".", ","))
                                        Else
                                            ldtrFila(indice) = CDbl(lstrValores(indice).ToString.Replace(",", "."))
                                        End If
                                    End If
                                Case 1
                                    ldtrFila(indice) = lstrValores(indice)
                                Case 2
                                    If IsDate(lstrValores(indice)) Then
                                        ldtrFila(indice) = IIf(IsDBNull(Format(CDate(lstrValores(indice)), "yyyyMMdd")), "", Format(CDate(lstrValores(indice)), "yyyyMMdd"))
                                    Else
                                        ldtrFila(indice) = lstrValores(indice)
                                    End If
                            End Select

                        Next
                        DtbDatos.Rows.Add(ldtrFila)
                    Catch ex As Exception
                        lCountError = lCountError + 1
                    End Try
                Next
                pDataSet.Tables.Add(DtbDatos)

            Catch ex As Exception
                pMensaje = ex.Message
            End Try
        End If
        If lCountError <> 0 Then
            pMensaje = "Se encontraron: " & lCountError & " Registros que no se pueden procesar, de un total de: " & lCountArch & ". porfavor revisar el archivo"
        End If

    End Sub

    Public Sub CargaArchivo_Txt_Fijo(ByVal pNombreArchivo As String, _
                                ByVal pFecha As Date, _
                                ByVal pTabla As String, _
                                ByVal pSeparador As String, _
                                ByVal pIdArchivoM As Double, _
                                ByVal pRegInicio As Integer, _
                                ByRef pDataSet As DataSet, _
                                ByRef pMensaje As String)

        '' Nombre de archivo dinamico 
        'Dim lintIni As Integer
        'Dim lIniFin As Integer
        'Dim lintLargo As Integer
        'Dim lstrFormatoFecha As String
        'Dim lstrNombreArchivo As String
        'lintIni = pNombreArchivo.IndexOf("&") + 1
        'lIniFin = pNombreArchivo.IndexOf("&", lintIni)
        'lintLargo = lIniFin - lintIni
        'lstrFormatoFecha = pNombreArchivo.Substring(lintIni, lintLargo)
        'lstrNombreArchivo = pNombreArchivo
        'lstrNombreArchivo = Replace(lstrNombreArchivo, ("&" + lstrFormatoFecha + "&"), Format(pFecha, lstrFormatoFecha))

        'Nombre de archivo dinamico 
        Dim lintIni As Integer
        Dim lIniFin As Integer
        Dim lintLargo As Integer
        Dim lstrFormatoFecha As String
        Dim lstrNombreArchivo As String
        lintIni = pNombreArchivo.IndexOf("&") + 1
        lIniFin = pNombreArchivo.IndexOf("&", lintIni)
        lintLargo = lIniFin - lintIni
        If lintLargo > 0 Then
            lstrFormatoFecha = pNombreArchivo.Substring(lintIni, lintLargo)
            lstrNombreArchivo = pNombreArchivo
            lstrNombreArchivo = Replace(lstrNombreArchivo, ("&" + lstrFormatoFecha + "&"), Format(pFecha, lstrFormatoFecha))
            pNombreArchivo = lstrNombreArchivo
        End If

        lintIni = pNombreArchivo.IndexOf("©") + 1
        lIniFin = pNombreArchivo.IndexOf("©", lintIni)
        lintLargo = lIniFin - lintIni
        If lintLargo > 0 Then
            lstrFormatoFecha = pNombreArchivo.Substring(lintIni, lintLargo)
            lstrNombreArchivo = pNombreArchivo
            lstrNombreArchivo = Replace(lstrNombreArchivo, ("©" + lstrFormatoFecha + "©"), (Mid(Format(pFecha, lstrFormatoFecha), 1, 4) + "0101"))
            pNombreArchivo = lstrNombreArchivo
        End If

        '-----
        Dim lstrMensajeResumen As String = ""
        Dim lintIndiceCol As Integer = -1
        Dim Ds_Archivo_D = New DataSet

        pMensaje = "OK"

        Dim contador As Integer = 0
        Dim lint_cont As Integer = 0
        Dim expression As String
        expression = "campo is not null and incidencia in (0,1)"
        Dim DtbDatos As DataTable = New DataTable(pTabla)
        Dim ldtrFila As DataRow
        Dim str_Tipo_Dato As String = ""

        DtbDatos.Columns.Clear()

        'If pRegInicio = 0 Then
        Call Archivo_Importa_D_Ver_M(Ds_Archivo_D, pTabla, pIdArchivoM, 0, "", "", "", 0, "", lstrMensajeResumen)
        'lintIndiceCol = -1
        For Each dr As DataRow In Ds_Archivo_D.Tables(pTabla).Select(expression)
            contador += 1
        Next
        contador = contador - 1

        Dim fieldWidths(contador) As Integer

        For Each dr As DataRow In Ds_Archivo_D.Tables(pTabla).Select(expression, "ORDEN ASC")
            If dr("CAMPO") <> "" Then
                Select Case dr("TIPO_DATO")
                    Case 0
                        str_Tipo_Dato = "System.Double"
                    Case 1
                        str_Tipo_Dato = "System.String"
                    Case 2
                        str_Tipo_Dato = "System.String"
                End Select
                CrearCampo(dr("CAMPO"), System.Type.GetType(str_Tipo_Dato), DtbDatos)
            End If
            fieldWidths(lint_cont) = dr("LARGO")
            lint_cont += 1
        Next

        'If pTabla = "OPERACION" Or pTabla = "MOVIMIENTO_CAJA" Then

        '    Using MyReader As New Microsoft.VisualBasic.FileIO.TextFieldParser(pNombreArchivo)
        '        MyReader.TextFieldType = Microsoft.VisualBasic.FileIO.FieldType.FixedWidth
        '        MyReader.SetFieldWidths(fieldWidths)

        '        Dim currentRow As String()
        '        While Not MyReader.EndOfData
        '            Try
        '                ldtrFila = DtbDatos.NewRow
        '                currentRow = MyReader.ReadFields()

        '                ldtrFila("CUENTA") = currentRow(0)
        '                ldtrFila("RUT_CLIENTE") = currentRow(1)
        '                ldtrFila("FECHA_OPERACION") = currentRow(2)
        '                ldtrFila("NUMERO_OPERACION") = currentRow(3)
        '                ldtrFila("CODIGO_TIPO_OPERACION") = currentRow(4)
        '                ldtrFila("SUBCODIGO_TIPO_OPERACION") = currentRow(5)
        '                ldtrFila("DESCRIPCION_TIPO_OPERACION") = currentRow(6)
        '                ldtrFila("MONTO_OPERACION") = currentRow(7)
        '                ldtrFila("NUMERO_CUOTAS") = currentRow(8)
        '                ldtrFila("VALOR_CUOTAS") = currentRow(9)
        '                ldtrFila("CODIGO_FFMM") = currentRow(10)
        '                ldtrFila("DESCRIPCION_FFMM") = currentRow(11)
        '                ldtrFila("MONTO_COMISION") = currentRow(12)
        '                ldtrFila("MONTO_RETENSION") = currentRow(13)
        '                ldtrFila("SALDO_CUENTA") = currentRow(14)
        '                ldtrFila("OTRA_CUENTA") = IIf(IsDBNull(currentRow(15)), "", currentRow(15))
        '                ldtrFila("CODIGO_FFMM_REINVERSION") = currentRow(16)
        '                ldtrFila("HORA_TRANSACCION") = currentRow(17)
        '                'ldtrFila("COD_ORIGEN_MOV_CAJA") = currentRow(6)

        '                DtbDatos.Rows.Add(ldtrFila)
        '            Catch ex As Microsoft.VisualBasic.FileIO.MalformedLineException
        '                pMensaje = ex.Message
        '            End Try
        '        End While
        '    End Using
        'Else
        Using MyReader As New Microsoft.VisualBasic.FileIO.TextFieldParser(pNombreArchivo)
            MyReader.TextFieldType = Microsoft.VisualBasic.FileIO.FieldType.FixedWidth
            MyReader.SetFieldWidths(fieldWidths)

            Dim currentRow As String()
            While Not MyReader.EndOfData

                Try
                    ldtrFila = DtbDatos.NewRow
                    currentRow = MyReader.ReadFields()

                    'ldtrFila("CUENTA") = currentRow(0)
                    'ldtrFila("NOMBRE_CLIENTE") = currentRow(1)
                    'ldtrFila("RUT_CLIENTE") = currentRow(2)
                    'ldtrFila("DIRECCION") = currentRow(3)
                    'ldtrFila("CIUDAD") = currentRow(4)
                    'ldtrFila("COMUNA") = currentRow(5)
                    'ldtrFila("TELEFONO") = currentRow(6)
                    'ldtrFila("RUT_AGENTE") = currentRow(7)
                    'ldtrFila("CODIGO_ESTADO_CUENTA") = currentRow(8)
                    'ldtrFila("DESCRICION_ESTADO_CUENTA") = currentRow(9)
                    'ldtrFila("CODIGO_FFMM") = currentRow(10)
                    'ldtrFila("DESCRIPCION_FFMM") = currentRow(11)
                    'ldtrFila("FECHA_SALDO") = currentRow(12)
                    'ldtrFila("SALDO_CUOTAS") = currentRow(13)
                    'ldtrFila("VALOR_CUOTA") = currentRow(14)
                    'ldtrFila("FECHA_APERTURA") = currentRow(15)
                    'ldtrFila("PERIODICIDAD") = currentRow(16)
                    'ldtrFila("RUT_COPARTICIPE") = currentRow(17)
                    'ldtrFila("NOMBRE_COPARTICIPE") = currentRow(18)
                    'ldtrFila("EFECTO57_BIS") = currentRow(19)
                    'ldtrFila("TIPO_PLAN_INVERSION") = currentRow(20)
                    'ldtrFila("CODIGO_BLOQUEO") = currentRow(21)
                    'ldtrFila("DESCRIPCION_BLOQUEO") = currentRow(22)
                    'ldtrFila("CODIGO_GRUPO_AGENTE") = currentRow(23)

                    For lcontador As Integer = 0 To contador
                        ldtrFila(lcontador) = currentRow(lcontador)
                    Next
                    DtbDatos.Rows.Add(ldtrFila)
                Catch ex As Microsoft.VisualBasic.FileIO.MalformedLineException
                    pMensaje = ex.Message
                End Try
            End While
        End Using
        'End If

        pDataSet.Tables.Add(DtbDatos)

    End Sub

    Private Sub CrearCampo(ByVal strNombreCampo As String, ByVal strTipoCampo As System.Type, ByRef DtbDatos As DataTable)
        Dim dtcColumna As DataColumn = New DataColumn(strNombreCampo)
        dtcColumna.DataType = strTipoCampo
        dtcColumna.AllowDBNull = True
        DtbDatos.Columns.Add(dtcColumna)
    End Sub

    Sub CargaArchivo_csv(ByVal pNombreArchivo As String, _
                                ByVal pFecha As Date, _
                                ByVal pTabla As String, _
                                ByVal pSeparador As String, _
                                ByVal pIdArchivoM As Double, _
                                ByVal pRegInicio As Integer, _
                                ByRef pDataSet As DataSet, _
                                ByRef pMensaje As String)

        '' Nombre de archivo dinamico 
        'Dim lintIni As Integer
        'Dim lIniFin As Integer
        'Dim lintLargo As Integer
        'Dim lstrFormatoFecha As String
        'Dim lstrNombreArchivo As String
        'lintIni = pNombreArchivo.IndexOf("&") + 1
        'lIniFin = pNombreArchivo.IndexOf("&", lintIni)
        'lintLargo = lIniFin - lintIni
        'lstrFormatoFecha = pNombreArchivo.Substring(lintIni, lintLargo)
        'lstrNombreArchivo = pNombreArchivo
        'lstrNombreArchivo = Replace(lstrNombreArchivo, ("&" + lstrFormatoFecha + "&"), Format(pFecha, lstrFormatoFecha))

        'Nombre de archivo dinamico 
        Dim lintIni As Integer
        Dim lIniFin As Integer
        Dim lintLargo As Integer
        Dim lstrFormatoFecha As String
        Dim lstrNombreArchivo As String
        lintIni = pNombreArchivo.IndexOf("&") + 1
        lIniFin = pNombreArchivo.IndexOf("&", lintIni)
        lintLargo = lIniFin - lintIni
        If lintLargo > 0 Then
            lstrFormatoFecha = pNombreArchivo.Substring(lintIni, lintLargo)
            lstrNombreArchivo = pNombreArchivo
            lstrNombreArchivo = Replace(lstrNombreArchivo, ("&" + lstrFormatoFecha + "&"), Format(pFecha, lstrFormatoFecha))
            pNombreArchivo = lstrNombreArchivo
        End If


        Dim lstrMensajeResumen As String = ""
        Dim lintIndiceCol As Integer = -1
        Dim Ds_Archivo_D = New DataSet

        pMensaje = "OK"
        Dim contador As Integer = 0
        Dim lint_cont As Integer = 0
        Dim expression As String
        expression = "campo is not null and incidencia in (0,1)"
        Dim DtbDatos As DataTable = New DataTable(pTabla)
        'Dim ldtrFila As DataRow
        Dim str_Tipo_Dato As String = ""
        'Obtenemos el delimitador que usa el Sistema
        Dim strRegedit As String = "HKEY_LOCAL_MACHINE\Software\Microsoft\Jet\4.0\Engines\Text"

        Dim valor As String = My.Computer.Registry.GetValue(strRegedit, "Format", Nothing)

        'Con esto cambias el delimitador del sistema a (,) = coma
        My.Computer.Registry.SetValue(strRegedit, "Format", "Delimited(;)")

        Dim sConnectionString As String = "Provider=Microsoft.Jet.OLEDB.4.0;" & _
                                            "Data Source=" & pNombreArchivo & _
                                            ";Extended Properties=" & Chr(34) & "text;HDR=YES;Format=(;)" & Chr(34)

        
        Dim objConn As New OleDbConnection(sConnectionString)

        objConn.Open()

        Try
            Dim objCmdSelect As New OleDbCommand("SELECT * FROM " & pNombreArchivo, objConn)
            Dim objAdapter1 As New OleDbDataAdapter()

            objAdapter1.SelectCommand = objCmdSelect

            'Dim objDataset1 As New DataSet()
            objAdapter1.Fill(pDataSet, "Test")
            'DataGridView1.DataSource = objDataset1.Tables(0) '.DefaultView

            If pRegInicio = 0 Then
                Call Archivo_Importa_D_Ver_M(Ds_Archivo_D, pTabla, pIdArchivoM, 0, "", "", "", 0, "", lstrMensajeResumen)
                lintIndiceCol = -1
                For Each dr As DataRow In Ds_Archivo_D.Tables(pTabla).Select("campo is not null and incidencia in (0,1)")
                    If dr(2) <> "" Then
                        lintIndiceCol = dr("orden") - 1
                        pDataSet.Tables(pTabla).Columns(lintIndiceCol).ColumnName = dr(2)
                    End If
                Next
            End If
        Catch ex As Exception
            pMensaje = ex.Message
        Finally
            objConn.Close()
            'Con esto restauramos el delimitador del sistema a como estaba incialmente
            My.Computer.Registry.SetValue(strRegedit, "Format", valor)
        End Try

    End Sub

End Class
