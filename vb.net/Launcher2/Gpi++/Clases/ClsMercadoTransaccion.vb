﻿Public Class ClsMercadoTransaccion

    Public Function MercadoTransaccion_Ver(ByVal intCodigo As Integer, _
                                       ByVal strColumnas As String, _
                                       ByVal strEstado As String, _
                                       ByRef strRetorno As String) As DataSet

        Dim LDS_MercadoTransaccion As New DataSet
        Dim LDS_MercadoTransaccion_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Lstr_NombreTabla As String = ""
        Dim Columna As DataColumn
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_MercadoTransaccion_Consultar"
        Lstr_NombreTabla = "MERCADO_TRANSACCION"

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("IdMercadoTransaccion", SqlDbType.Float, 5).Value = IIf(intCodigo = 0, DBNull.Value, intCodigo)
            SQLCommand.Parameters.Add("pEstado", SqlDbType.VarChar, 3).Value = IIf(strEstado.Trim = "", DBNull.Value, strEstado.Trim)
            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_MercadoTransaccion, Lstr_NombreTabla)

            LDS_MercadoTransaccion_Aux = LDS_MercadoTransaccion

            If strColumnas.Trim = "" Then
                LDS_MercadoTransaccion_Aux = LDS_MercadoTransaccion
            Else
                LDS_MercadoTransaccion_Aux = LDS_MercadoTransaccion.Copy
                For Each Columna In LDS_MercadoTransaccion.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_MercadoTransaccion_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_MercadoTransaccion_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_MercadoTransaccion.Dispose()
            Return LDS_MercadoTransaccion_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function MercadoTransaccion_Mantenedor(ByVal strAccion As String, _
                                              ByVal strDescripcion As String, _
                                              ByVal intIdMercadoTransaccion As Integer, _
                                              ByRef strEstado As String) As String

        Dim strDescError As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand("Rcp_MercadoTransaccion_Mantencion", MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Rcp_MercadoTransaccion_Mantencion"
            SQLCommand.Parameters.Clear()

            ' @pAccion varchar(10),
            ' @pDscMercadoTransaccion varchar(20),
            ' @pIdMercadoTransaccion inT output , 
            ' @pResultado varchar(200) OUTPUT)

            SQLCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 10).Value = strAccion
            SQLCommand.Parameters.Add("pDscMercadoTransaccion", SqlDbType.VarChar, 60).Value = strDescripcion
            SQLCommand.Parameters.Add("pIdMercadoTransaccion", SqlDbType.Float, 3).Value = intIdMercadoTransaccion
            SQLCommand.Parameters.Add("pEstado", SqlDbType.VarChar, 3).Value = strEstado

            '...Resultado
            Dim ParametroSal2 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal2.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal2)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en el Búsqueda de " & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            MercadoTransaccion_Mantenedor = strDescError
        End Try
    End Function
End Class

