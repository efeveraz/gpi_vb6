﻿Imports System.Threading
Imports System.Reflection

Public Class Frm_Principal
    Private mMenu As New System.Windows.Forms.MainMenu()
    Dim ImagenLocal As Image = Nothing
    Public Shared flgModoConsulta As Boolean = False
    Public Shared lstrNum_Cta As String = ""
    Public Shared llngIdGrupo As Long = 0
    Public Shared llngIdCliente As Long = 0
    Public Shared llngIdCuenta As Long = 0
    Public Shared lstrCodMoneda As String = ""
    Public Shared lstrNombreCliente As String = ""
    Public Shared lstrFechaCierreCta As String = ""
    Public Shared lintCantCtas As Integer = 0
    Public Shared lstrRut_Cliente As String = ""
    Public Shared lstrCodTipoEntidad As String = ""
    Public PrevSizeX As Integer = Me.Width
    Public PrevSizeY As Integer = Me.Height

    'variables cronometro
    Private lihora As Integer = 0
    Private liminuto As Integer = 0
    Private lisegundo As Integer = 0
    Private limilisegundo As Integer = 0

    Public Sub New()
        MyBase.New()
        '
        'El Diseñador de Windows Forms requiere esta llamada.
        InitializeComponent()
        'Agregar cualquier inicialización después de la llamada a InitializeComponent()
    End Sub

    Private Sub Frm_Principal_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If Not Fnt_Multitarea_Disponible() Then MsgBox("Accion no permitida mientras se genera reporte.", MsgBoxStyle.Critical, gtxtNombreSistema & Me.Text) : e.Cancel = True : Exit Sub
        
        Frm_Login.Show()
        Frm_Login.BringToFront()
        tmr_FechaSrv.Enabled = False
    End Sub

    Private Sub Frm_Principal_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.Text = "Gpi+ 2.0  (Back Office)        DB: " & gtxtAmbienteSistema & "       USUARIO : " & gtxtNombreUsuario & "           NEGOCIO : " & gtxtNombreNegocio
       
        'Dim CommandLineArgs As System.Collections.ObjectModel.ReadOnlyCollection(Of String) = My.Application.CommandLineArgs
        Dim sPrograma As String = ""

        'Verificamos y cargamos escritorio comercial
        If ESC_COM_SEL = True Then
            'Dim Frm_Formulario As New Frm_DetalleClientes
            'With Frm_Formulario
            '    .TopMost = False
            '    .Top = 2
            '    .Left = 2
            '    .TopLevel = False
            '    .Parent = Me.Panel1
            '    .Show()
            '    .BringToFront()
            'End With
            Exit Sub
        End If
        Try
            ImagenLocal = Drawing.Image.FromFile(gPathApp & "\" & IIf(gLogoCliente = "" Or gLogoCliente = "CREASYS", "Recursos Comunes", "Recursos_" & gLogoCliente) & "\Recursos\Img_Logo.gif")
            C1PictureBox1.BackgroundImage = ImagenLocal
        Catch EX As Exception
        End Try
        '...CARGA LA CLASE PARAMETROS DE SISTEMA Y FECHA SISTEMA
        gobjParametro = New ClsParametrosSistema

        Call Prc_Carga_Datos_Accion()
        Me.MinimumSize = Me.Size
        Dim oCuentas As New ClsCuentas
        Dim strRetorno As String
        strRetorno = oCuentas.BuscarCuentasPorUsuario(glngIdUsuario, "1|Cuenta1|2|Cuenta2|3|Cuenta3|4|Cuenta4|5|Cuenta5|6|Cuenta6|7|Cuenta7|8|Cuenta8|")
      
    End Sub

    Private Sub Sub_Muestra_Frm(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ass As System.Reflection.Assembly
        Dim sPrograma As String = CType(sender, MenuItem).Tag
        gintSubPantalla = CType(sender, MenuItem).MergeOrder
        Dim sNumeroPantalla As Integer = CType(sender, MenuItem).MergeOrder
        sPrograma = sPrograma.ToUpper
        Debug.Print("________________________________________")
        Debug.Print("Chequeando => " & vbTab & sender.text & vbTab & "  =>  " & sPrograma)

        'Carga el Formulario 
        gobjFormulario._Modal = False
        Dim s As String = ""

        ass = System.Reflection.Assembly.GetExecutingAssembly()
        For Each t1 As Type In ass.GetTypes()

            If sPrograma = t1.Name.ToUpper Then
                s = "Gpi__." + t1.Name
            End If
        Next

        Try
            Dim t As Type = ass.GetType(s)

            Dim o As Object
            o = Activator.CreateInstance(t)
            Dim Frm_Formulario As Object = CType(o, Form)

            Select Case sPrograma.ToUpper
                Case "FRM_MANTENEDORVALORTIPOCAMBIO"
                    With Frm_Formulario
                        If sender.text.ToString.ToUpper = "VALOR TIPO CAMBIO" Then
                            .gOperacion = "VALOR TIPO CAMBIO"
                        Else
                            .gOperacion = ""
                        End If
                        .TopMost = False
                        .Top = 2
                        .Left = 2
                        .TopLevel = False
                        .Parent = Me.Panel1
                        Try
                            .lint_NumeroDePantalla = gintSubPantalla
                            Try
                                .cargarformulario()
                            Catch ex As Exception
                                .show()
                            End Try
                        Catch ex1 As Exception
                            Try
                                .cargarformulario()
                            Catch ex As Exception
                                .show()
                            End Try
                        End Try
                        'Try
                        '    .cargarformulario()
                        'Catch ex As Exception
                        '    .show()
                        'End Try
                        '.CargarFormulario()
                        .BringToFront()
                    End With
                Case Is = "Frm_IngresoOpcion".ToUpper
                    'Dim Frm As New Frm_IngresoDividendos
                    'With Frm
                    '    .gOperacion = "OPCION"
                    '    .gTituloOperacion = "Ingreso de Opción"
                    '    .TopMost = False
                    '    .TopLevel = False
                    '    .Parent = Me.Panel1
                    '    Try
                    '        .lint_NumeroDePantalla = sNumeroPantalla
                    '        .Show()
                    '    Catch ex As Exception
                    '        .Show()
                    '    End Try
                    '    .BringToFront()
                    'End With
                Case Is = "Frm_Conciliador_Asignaciones".ToUpper
                    'Dim frm As New Frm_Conciliador_Asignaciones
                    Dim lstrOperacion As String = ""
                    Dim lstrTitulo As String = ""
                    Select Case sender.text.ToString.ToUpper
                        Case "Conciliador Asignaciones RF".ToUpper
                            lstrOperacion = "IRF"
                            lstrTitulo = "Conciliador Asignaciones Renta Fija"
                        Case "Conciliador Asignaciones IF".ToUpper
                            lstrOperacion = "IIF"
                            lstrTitulo = "Conciliador Asignaciones Intermediación Financiera"
                        Case "Conciliador Asignaciones RV".ToUpper
                            lstrOperacion = "RV"
                            lstrTitulo = "Conciliador Asignaciones Renta Variable"
                    End Select
                    'With frm
                    '    .gOperacion = lstrOperacion
                    '    .gTituloOperacion = lstrTitulo
                    '    .TopMost = False
                    '    .TopLevel = False
                    '    .Parent = Me.Panel1
                    '    .Show()
                    '    .BringToFront()
                    'End With

                Case Is = "Frm_Resumen_Caja".ToUpper
                    'Dim frm As New Frm_Resumen_Caja
                    Dim lstrOperacion As String = ""
                    Dim lstrTitulo As String = ""
                    Select Case sender.text.ToString.ToUpper
                        Case "RPT033-Resumen de Ing. y Egr. de Caja".ToUpper
                            lstrOperacion = "Resumen"
                            lstrTitulo = "Resumen Ingresos y Egresos de Caja Administración de Carteras"
                        Case "RPT034-1-Control Ing. y Egr. de Caja".ToUpper
                            lstrOperacion = "Reporte"
                            lstrTitulo = "Reporte Ingresos y Egresos de Caja (BICE)"
                    End Select
                    'With frm
                    '    .gOperacion = lstrOperacion
                    '    .gTituloOperacion = lstrTitulo
                    '    .TopMost = False
                    '    .TopLevel = False
                    '    .Parent = Me.Panel1             
                    '    .Show()
                    '    .BringToFront()
                    'End With


                Case Is = "Frm_CobroComisionesAdministracion".ToUpper
                    'Dim Frm As New Frm_CobroComisionesAdministracion
                    'With Frm
                    '    Select Case sender.text.ToString.ToUpper
                    '        Case "COBRO COMISIONES ADMIN.".ToUpper
                    '            .lstrModOperacion = "EDICION"
                    '        Case "CONSULTA COMISIONES ADMIN.".ToUpper
                    '            .lstrModOperacion = "CONSULTA"
                    '    End Select
                    '    .TopMost = False
                    '    .TopLevel = False
                    '    .Parent = Me.Panel1
                    '    .Show()
                    '    .BringToFront()
                    'End With
                Case Else
                    With Frm_Formulario
                        .TopMost = False
                        .Top = 2
                        .Left = 2
                        .TopLevel = False
                        .Parent = Me.Panel1
                        Try
                            .lint_NumeroDePantalla = gintSubPantalla
                            Try
                                .cargarformulario()
                            Catch ex As Exception
                                .show()
                            End Try
                        Catch ex1 As Exception
                            Try
                                .cargarformulario()
                            Catch ex As Exception
                                .show()
                            End Try
                        End Try
                        '.CargarFormulario()
                        .BringToFront()
                    End With
            End Select

            ' fin
        Catch ex As Exception
            Debug.Print(vbTab & "Integrar => " & sPrograma)
        End Try
    End Sub

    Private Sub Prc_Carga_Datos_Accion()
        Dim oDSMenu As Data.DataSet
        Dim oUsuario As New ClsUsuario()
        Dim oMenu As New ClsAcciones
        Dim sub_pantalla As Integer = 0
        Dim descripcion_item As String = ""
        Dim nombre_programa As String = ""
        Dim nombre_control As String = ""
        Dim strRetorno As String = ""
        Dim blnHabilitado As Boolean = True
        Dim lintIndice As Integer = 1

        Dim sRetorno As String = ""
        oDSMenu = oMenu.BuscarOpcionesMenu(sRetorno, 200000, glngIdUsuario, glngNegocioOperacion)
        Dim nMenu_tmp As MenuItem

        If sRetorno = "OK" Then
            For Each dr As Data.DataRow In oDSMenu.Tables(0).Rows
                nMenu_tmp = mMenu.MenuItems.Add("mnu_" & CStr(dr.Item("NUMERO_LINEA")))
                If dr.Item("CONTROL_HABILITADO") = 0 Then
                    nMenu_tmp.Enabled = False
                End If
                nMenu_tmp.Text = dr.Item("DESCRIPCION_ITEM")
                Prc_Arma_Menu_Recursivo(oDSMenu, dr.Item("SUB_PANTALLA"), nMenu_tmp)
            Next
        End If
        Me.Menu = mMenu

    End Sub

    ' RdiazG Procedimiento de Armado de Menu automatizado
    ' Invoca evento Sub_Muestra_frm con respectivo tag del nombre del frm
    Sub Prc_Arma_Menu_Recursivo(ByRef DSet As Data.DataSet, ByVal Sub_Linea As Double, ByRef ObjRaiz As MenuItem)
        Dim oUsuario As New ClsUsuario()
        Dim oMenu As New ClsAcciones
        Dim sub_pantalla As Integer = 0
        Dim descripcion_item As String = ""
        Dim nombre_programa As String = ""
        Dim nombre_control As String = ""
        Dim strRetorno As String = ""
        Dim blnHabilitado As Boolean = True
        Dim lintIndice As Integer = 1
        Dim sRetorno As String = ""
        Dim nMenu_tmp As MenuItem

        DSet = oMenu.BuscarOpcionesMenu(sRetorno, Sub_Linea, glngIdUsuario, glngNegocioOperacion)

        If sRetorno = "OK" Then
            For Each dr As Data.DataRow In DSet.Tables(0).Rows
                If dr.Item("CONCEPTO") = "MENU" Then
                    nMenu_tmp = ObjRaiz.MenuItems.Add("mnu_" & CStr(dr.Item("NUMERO_LINEA")))
                    If dr.Item("CONTROL_HABILITADO") = 0 Then
                        nMenu_tmp.Enabled = False
                    End If
                    nMenu_tmp.Text = dr.Item("DESCRIPCION_ITEM")
                    Prc_Arma_Menu_Recursivo(DSet, dr.Item("SUB_PANTALLA"), nMenu_tmp)
                ElseIf dr.Item("CONCEPTO") = "SUBMENU" Then
                    nMenu_tmp = ObjRaiz.MenuItems.Add("mnu_" & CStr(dr.Item("NUMERO_LINEA")), New EventHandler(AddressOf Sub_Muestra_Frm))
                    nMenu_tmp.Tag = dr.Item("NOMBRE_CONTROL")
                    nMenu_tmp.MergeOrder = dr.Item("SUB_PANTALLA")
                    If dr.Item("CONTROL_HABILITADO") = 0 Then
                        nMenu_tmp.Enabled = False
                    End If
                    nMenu_tmp.Text = dr.Item("DESCRIPCION_ITEM")
                End If
            Next
        End If

    End Sub

    Private Sub Frm_Principal_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
        Fnt_Resize_Controls(Me, Me)
    End Sub

    Private Sub Tmr_fechaSrv_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmr_FechaSrv.Tick
        Dim LTxt_ResultadoFecha As String = ""
        limilisegundo += 1
        If limilisegundo = 9 Then
            limilisegundo = 0
            lisegundo += 1
            If lisegundo = 60 Then
                lisegundo = 0
                liminuto += 1
                If liminuto = 60 Then
                    liminuto = 0
                    lihora += 1
                    LTxt_ResultadoFecha = gFun_BuscarFechaHoraSistema(gstrFechaSistema, gstrHoraSistema)
                End If
            End If
        End If
    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub
End Class