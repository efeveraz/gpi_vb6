﻿'------------------------------------------------------------------------------
' <auto-generated>
'     Este código fue generado por una herramienta.
'     Versión de runtime:4.0.30319.42000
'
'     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
'     se vuelve a generar el código.
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict Off
Option Explicit On

Imports System
Imports System.ComponentModel
Imports System.Diagnostics
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Xml.Serialization

'
'Microsoft.VSDesigner generó automáticamente este código fuente, versión=4.0.30319.42000.
'
Namespace WsBiceCMA_ConsultaSaldo_Desa
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.3056.0"),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code"),  _
     System.Web.Services.WebServiceBindingAttribute(Name:="TEL23300PortBinding", [Namespace]:="http://tel23300.ws.bice.cl/")>  _
    Partial Public Class TEL23300Service
        Inherits System.Web.Services.Protocols.SoapHttpClientProtocol
        
        Private ExecuteOperationCompleted As System.Threading.SendOrPostCallback
        
        Private useDefaultCredentialsSetExplicitly As Boolean
        
        '''<remarks/>
        Public Sub New()
            MyBase.New
            Me.Url = Global.Gpi__.My.MySettings.Default.Gpi_20_WsBiceCMA_ConsultaSaldo_Desa_TEL23300Service
            If (Me.IsLocalFileSystemWebService(Me.Url) = true) Then
                Me.UseDefaultCredentials = true
                Me.useDefaultCredentialsSetExplicitly = false
            Else
                Me.useDefaultCredentialsSetExplicitly = true
            End If
        End Sub
        
        Public Shadows Property Url() As String
            Get
                Return MyBase.Url
            End Get
            Set
                If (((Me.IsLocalFileSystemWebService(MyBase.Url) = true)  _
                            AndAlso (Me.useDefaultCredentialsSetExplicitly = false))  _
                            AndAlso (Me.IsLocalFileSystemWebService(value) = false)) Then
                    MyBase.UseDefaultCredentials = false
                End If
                MyBase.Url = value
            End Set
        End Property
        
        Public Shadows Property UseDefaultCredentials() As Boolean
            Get
                Return MyBase.UseDefaultCredentials
            End Get
            Set
                MyBase.UseDefaultCredentials = value
                Me.useDefaultCredentialsSetExplicitly = true
            End Set
        End Property
        
        '''<remarks/>
        Public Event ExecuteCompleted As ExecuteCompletedEventHandler
        
        '''<remarks/>
        <System.Web.Services.Protocols.SoapDocumentMethodAttribute("", RequestNamespace:="http://tel23300.ws.bice.cl/", ResponseNamespace:="http://tel23300.ws.bice.cl/", Use:=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle:=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)>  _
        Public Function Execute(<System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)> ByVal CANAL As String, <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)> ByVal MONEDA As String, <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)> ByVal CUENTA As String) As <System.Xml.Serialization.XmlElementAttribute("return", Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)> tel23300Out
            Dim results() As Object = Me.Invoke("Execute", New Object() {CANAL, MONEDA, CUENTA})
            Return CType(results(0),tel23300Out)
        End Function
        
        '''<remarks/>
        Public Overloads Sub ExecuteAsync(ByVal CANAL As String, ByVal MONEDA As String, ByVal CUENTA As String)
            Me.ExecuteAsync(CANAL, MONEDA, CUENTA, Nothing)
        End Sub
        
        '''<remarks/>
        Public Overloads Sub ExecuteAsync(ByVal CANAL As String, ByVal MONEDA As String, ByVal CUENTA As String, ByVal userState As Object)
            If (Me.ExecuteOperationCompleted Is Nothing) Then
                Me.ExecuteOperationCompleted = AddressOf Me.OnExecuteOperationCompleted
            End If
            Me.InvokeAsync("Execute", New Object() {CANAL, MONEDA, CUENTA}, Me.ExecuteOperationCompleted, userState)
        End Sub
        
        Private Sub OnExecuteOperationCompleted(ByVal arg As Object)
            If (Not (Me.ExecuteCompletedEvent) Is Nothing) Then
                Dim invokeArgs As System.Web.Services.Protocols.InvokeCompletedEventArgs = CType(arg,System.Web.Services.Protocols.InvokeCompletedEventArgs)
                RaiseEvent ExecuteCompleted(Me, New ExecuteCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState))
            End If
        End Sub
        
        '''<remarks/>
        Public Shadows Sub CancelAsync(ByVal userState As Object)
            MyBase.CancelAsync(userState)
        End Sub
        
        Private Function IsLocalFileSystemWebService(ByVal url As String) As Boolean
            If ((url Is Nothing)  _
                        OrElse (url Is String.Empty)) Then
                Return false
            End If
            Dim wsUri As System.Uri = New System.Uri(url)
            If ((wsUri.Port >= 1024)  _
                        AndAlso (String.Compare(wsUri.Host, "localHost", System.StringComparison.OrdinalIgnoreCase) = 0)) Then
                Return true
            End If
            Return false
        End Function
    End Class
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.3056.0"),  _
     System.SerializableAttribute(),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code"),  _
     System.Xml.Serialization.XmlTypeAttribute([Namespace]:="http://tel23300.ws.bice.cl/")>  _
    Partial Public Class tel23300Out
        
        Private fechaUltimaFacturacionField As String
        
        Private fechaVencimientoField As String
        
        Private mENSAJEField As String
        
        Private montoAprobadoLineaSobregiroField As String
        
        Private montoDesconocidoField As String
        
        Private montoDisponibleLineaSobregiroField As String
        
        Private montoFacturadoField As String
        
        Private montoLBTRField As String
        
        Private montoUtilizadoLineaSobregiroField As String
        
        Private otroMontoField As String
        
        Private rETCODEField As Integer
        
        Private rETCODEFieldSpecified As Boolean
        
        Private retencionesMismaPlazaField As String
        
        Private retencionesOtrasPlazasField As String
        
        Private saldoContableField As String
        
        Private saldoDisponibleField As String
        
        Private statusField As Integer
        
        Private statusFieldSpecified As Boolean
        
        Private ultimosAbonosField() As movimiento
        
        Private ultimosChequesField() As cheque
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property fechaUltimaFacturacion() As String
            Get
                Return Me.fechaUltimaFacturacionField
            End Get
            Set
                Me.fechaUltimaFacturacionField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property fechaVencimiento() As String
            Get
                Return Me.fechaVencimientoField
            End Get
            Set
                Me.fechaVencimientoField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property MENSAJE() As String
            Get
                Return Me.mENSAJEField
            End Get
            Set
                Me.mENSAJEField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property montoAprobadoLineaSobregiro() As String
            Get
                Return Me.montoAprobadoLineaSobregiroField
            End Get
            Set
                Me.montoAprobadoLineaSobregiroField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property montoDesconocido() As String
            Get
                Return Me.montoDesconocidoField
            End Get
            Set
                Me.montoDesconocidoField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property montoDisponibleLineaSobregiro() As String
            Get
                Return Me.montoDisponibleLineaSobregiroField
            End Get
            Set
                Me.montoDisponibleLineaSobregiroField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property montoFacturado() As String
            Get
                Return Me.montoFacturadoField
            End Get
            Set
                Me.montoFacturadoField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property montoLBTR() As String
            Get
                Return Me.montoLBTRField
            End Get
            Set
                Me.montoLBTRField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property montoUtilizadoLineaSobregiro() As String
            Get
                Return Me.montoUtilizadoLineaSobregiroField
            End Get
            Set
                Me.montoUtilizadoLineaSobregiroField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property otroMonto() As String
            Get
                Return Me.otroMontoField
            End Get
            Set
                Me.otroMontoField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property RETCODE() As Integer
            Get
                Return Me.rETCODEField
            End Get
            Set
                Me.rETCODEField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlIgnoreAttribute()>  _
        Public Property RETCODESpecified() As Boolean
            Get
                Return Me.rETCODEFieldSpecified
            End Get
            Set
                Me.rETCODEFieldSpecified = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property retencionesMismaPlaza() As String
            Get
                Return Me.retencionesMismaPlazaField
            End Get
            Set
                Me.retencionesMismaPlazaField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property retencionesOtrasPlazas() As String
            Get
                Return Me.retencionesOtrasPlazasField
            End Get
            Set
                Me.retencionesOtrasPlazasField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property saldoContable() As String
            Get
                Return Me.saldoContableField
            End Get
            Set
                Me.saldoContableField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property saldoDisponible() As String
            Get
                Return Me.saldoDisponibleField
            End Get
            Set
                Me.saldoDisponibleField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property status() As Integer
            Get
                Return Me.statusField
            End Get
            Set
                Me.statusField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlIgnoreAttribute()>  _
        Public Property statusSpecified() As Boolean
            Get
                Return Me.statusFieldSpecified
            End Get
            Set
                Me.statusFieldSpecified = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute("ultimosAbonos", Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true)>  _
        Public Property ultimosAbonos() As movimiento()
            Get
                Return Me.ultimosAbonosField
            End Get
            Set
                Me.ultimosAbonosField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute("ultimosCheques", Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true)>  _
        Public Property ultimosCheques() As cheque()
            Get
                Return Me.ultimosChequesField
            End Get
            Set
                Me.ultimosChequesField = value
            End Set
        End Property
    End Class
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.3056.0"),  _
     System.SerializableAttribute(),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code"),  _
     System.Xml.Serialization.XmlTypeAttribute([Namespace]:="http://tel23300.ws.bice.cl/")>  _
    Partial Public Class movimiento
        
        Private abonoField As String
        
        Private cargoField As String
        
        Private descripcionField As String
        
        Private documentoField As String
        
        Private fechaField As String
        
        Private fechaMovimientoField As String
        
        Private lineaSobregiroField As String
        
        Private otroP1Field As String
        
        Private otroP2Field As String
        
        Private otroP3Field As String
        
        Private saldoField As String
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property abono() As String
            Get
                Return Me.abonoField
            End Get
            Set
                Me.abonoField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property cargo() As String
            Get
                Return Me.cargoField
            End Get
            Set
                Me.cargoField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property descripcion() As String
            Get
                Return Me.descripcionField
            End Get
            Set
                Me.descripcionField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property documento() As String
            Get
                Return Me.documentoField
            End Get
            Set
                Me.documentoField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property fecha() As String
            Get
                Return Me.fechaField
            End Get
            Set
                Me.fechaField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property fechaMovimiento() As String
            Get
                Return Me.fechaMovimientoField
            End Get
            Set
                Me.fechaMovimientoField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property lineaSobregiro() As String
            Get
                Return Me.lineaSobregiroField
            End Get
            Set
                Me.lineaSobregiroField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property otroP1() As String
            Get
                Return Me.otroP1Field
            End Get
            Set
                Me.otroP1Field = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property otroP2() As String
            Get
                Return Me.otroP2Field
            End Get
            Set
                Me.otroP2Field = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property otroP3() As String
            Get
                Return Me.otroP3Field
            End Get
            Set
                Me.otroP3Field = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property saldo() As String
            Get
                Return Me.saldoField
            End Get
            Set
                Me.saldoField = value
            End Set
        End Property
    End Class
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.3056.0"),  _
     System.SerializableAttribute(),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code"),  _
     System.Xml.Serialization.XmlTypeAttribute([Namespace]:="http://tel23300.ws.bice.cl/")>  _
    Partial Public Class cheque
        
        Private fechaField As String
        
        Private montoField As Single
        
        Private montoFieldSpecified As Boolean
        
        Private numeroField As Long
        
        Private numeroFieldSpecified As Boolean
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property fecha() As String
            Get
                Return Me.fechaField
            End Get
            Set
                Me.fechaField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property monto() As Single
            Get
                Return Me.montoField
            End Get
            Set
                Me.montoField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlIgnoreAttribute()>  _
        Public Property montoSpecified() As Boolean
            Get
                Return Me.montoFieldSpecified
            End Get
            Set
                Me.montoFieldSpecified = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property numero() As Long
            Get
                Return Me.numeroField
            End Get
            Set
                Me.numeroField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlIgnoreAttribute()>  _
        Public Property numeroSpecified() As Boolean
            Get
                Return Me.numeroFieldSpecified
            End Get
            Set
                Me.numeroFieldSpecified = value
            End Set
        End Property
    End Class
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.3056.0")>  _
    Public Delegate Sub ExecuteCompletedEventHandler(ByVal sender As Object, ByVal e As ExecuteCompletedEventArgs)
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.3056.0"),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code")>  _
    Partial Public Class ExecuteCompletedEventArgs
        Inherits System.ComponentModel.AsyncCompletedEventArgs
        
        Private results() As Object
        
        Friend Sub New(ByVal results() As Object, ByVal exception As System.Exception, ByVal cancelled As Boolean, ByVal userState As Object)
            MyBase.New(exception, cancelled, userState)
            Me.results = results
        End Sub
        
        '''<remarks/>
        Public ReadOnly Property Result() As tel23300Out
            Get
                Me.RaiseExceptionIfNecessary
                Return CType(Me.results(0),tel23300Out)
            End Get
        End Property
    End Class
End Namespace
