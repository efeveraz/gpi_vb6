﻿Public Class Frm_PopUp

    Private Sub Frm_PopUp_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Left = Frm_Principal.Width - Me.Width
        Me.Top = Frm_Principal.Height - Me.Height
    End Sub

    Public Sub Actualiza(ByVal ppTitulo As String, ByVal ppTexto As String, Optional ByVal Accion As String = "N")
        'Me.Left = Frm_Principal.Width - Me.Width
        'Me.Top = Frm_Principal.Height - Me.Height
        'If Not Me.ContainsFocus Then
        'Me.Activate()
        'End If
        Group_Titulo.Text = ppTitulo
        Texto.Text = ppTexto
        If Accion = "LISTO" Then
            Me.Close()
            Me.Dispose()
        End If

    End Sub

    Private Sub Btn_Cancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_Cancelar.Click
        If bw.IsBusy Then 'Solo Entra si se esta ejecutando el Hilo Multitarea
            Texto.Text = "Cancelando..."
            bw.CancelAsync() 'Solicita Cancelacion de Hilo Multitarea
            Btn_Cancelar.Enabled = False
        End If
        gBolReporte = False
        Me.Close()
        Me.Dispose()
    End Sub
End Class