﻿Public Class ClsEstados

    Public Function Estados_Ver(ByVal strCodigoEstados As String, _
                               ByVal strCodigoTipo As String, _
                               ByVal strDefault As String, _
                               ByVal strColumnas As String, _
                               ByRef strRetorno As String) As DataSet

        Dim LDS_Estados As New DataSet
        Dim LDS_Estados_Aux As New DataSet
        Dim Lstr_NombreTabla As String
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Estados_Consultar"
        Lstr_NombreTabla = "Estados"
        Connect.Abrir()
        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("pIdEstado", SqlDbType.VarChar, 9).Value = IIf(strCodigoEstados.Trim = "", DBNull.Value, strCodigoEstados.Trim)
            SQLCommand.Parameters.Add("pIdTipoEstado", SqlDbType.VarChar, 9).Value = IIf(strCodigoTipo.Trim = "", DBNull.Value, strCodigoTipo.Trim)
            SQLCommand.Parameters.Add("pDefault", SqlDbType.VarChar, 1).Value = IIf(strDefault.Trim = "", DBNull.Value, strDefault.Trim)
            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Estados, Lstr_NombreTabla)
            strRetorno = "OK"
            LDS_Estados_Aux = LDS_Estados

            If strColumnas.Trim = "" Then
                LDS_Estados_Aux = LDS_Estados
            Else
                LDS_Estados_Aux = LDS_Estados.Copy
                For Each Columna In LDS_Estados.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Estados_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Estados_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_Estados.Dispose()
            Return LDS_Estados_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function Tipo_Estado(ByVal strDscTipoEstado As String, ByRef strRetorno As String) As DataSet

        Dim LDS_Estados As New DataSet
        Dim LDS_Estados_Aux As New DataSet
        Dim Lstr_NombreTabla As String
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_Tipo_Estado"
        Lstr_NombreTabla = "TIPO_ESTADO"
        Connect.Abrir()
        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("pDscTipoEstado", SqlDbType.VarChar, 30).Value = IIf(strDscTipoEstado.Trim = "", DBNull.Value, strDscTipoEstado.Trim)
            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Estados, Lstr_NombreTabla)
            strRetorno = "OK"
          
            LDS_Estados.Dispose()
            Return LDS_Estados

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function


    Public Function EstadosChecklist_Ver(ByVal strCodigoEstados As String, _
                                         ByVal strCodigoNegocio As String, _
                                         ByVal strDefault As String, _
                                         ByVal strColumnas As String, _
                                         ByRef strRetorno As String) As DataSet

        Dim LDS_Estados As New DataSet
        Dim LDS_Estados_Aux As New DataSet
        Dim Lstr_NombreTabla As String
        Dim LArr_NombreColumna() As String = Split(strColumnas, ",")
        Dim LInt_Col As Integer = 0
        Dim LInt_NomCol As String = ""
        Dim Remove As Boolean = True
        Dim LstrProcedimiento As String = ""
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion

        LstrProcedimiento = "Rcp_ChecklistEstados_Consultar"
        Lstr_NombreTabla = "Estados"
        Connect.Abrir()
        Try
            SQLCommand = New SqlClient.SqlCommand(LstrProcedimiento, Connect.Conexion)
            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = LstrProcedimiento
            SQLCommand.Parameters.Clear()
            SQLCommand.Parameters.Add("pIdEstado", SqlDbType.VarChar, 9).Value = IIf(strCodigoEstados.Trim = "", DBNull.Value, strCodigoEstados.Trim)
            SQLCommand.Parameters.Add("pIdNegocio", SqlDbType.VarChar, 9).Value = IIf(strCodigoNegocio.Trim = "", DBNull.Value, strCodigoNegocio.Trim)
            SQLCommand.Parameters.Add("pDefault", SqlDbType.VarChar, 1).Value = IIf(strDefault.Trim = "", DBNull.Value, strDefault.Trim)
            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_Estados, Lstr_NombreTabla)
            strRetorno = "OK"
            LDS_Estados_Aux = LDS_Estados

            If strColumnas.Trim = "" Then
                LDS_Estados_Aux = LDS_Estados
            Else
                LDS_Estados_Aux = LDS_Estados.Copy
                For Each Columna In LDS_Estados.Tables(Lstr_NombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_Estados_Aux.Tables(Lstr_NombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_Estados_Aux.Tables(Lstr_NombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            strRetorno = "OK"
            LDS_Estados.Dispose()
            Return LDS_Estados_Aux
            Return LDS_Estados
        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try
    End Function

End Class
