﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports Microsoft.VisualBasic


Public Class ClsAcciones
    Dim lstrProcedimiento As String = ""
    Dim LstrNombreTabla As String = ""

    Public Function BuscarAcciones(ByRef strRetorno As String, _
                                   ByVal intNumeroPantalla As Integer, _
                                   ByVal intNumeroLinea As Integer) As DataSet

        Dim DS_Acciones As New DataSet
        Dim DS_Acciones_Aux As New DataSet

        Dim Sqlcommand As New SqlClient.SqlCommand
        Dim SqlLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion

        lstrProcedimiento = "Sis_Accion_Consultar"
        LstrNombreTabla = "ACCIONES"
        Connect.Abrir()

        Try
            Sqlcommand = New SqlCommand(lstrProcedimiento, Connect.Conexion)

            Sqlcommand.CommandType = CommandType.StoredProcedure
            Sqlcommand.CommandText = lstrProcedimiento
            Sqlcommand.Parameters.Clear()

            Sqlcommand.Parameters.Add("@pCodigoOperacion", SqlDbType.Char, 15).Value = "BUSCAR"

            Sqlcommand.Parameters.Add("@pNumeroPantalla", SqlDbType.Int).Value = intNumeroPantalla

            Sqlcommand.Parameters.Add("@pNumeroLinea", SqlDbType.Int).Value = IIf(intNumeroLinea = 0, DBNull.Value, intNumeroLinea)


            Dim oParamSalida2 As New SqlClient.SqlParameter("@pMsjRetorno", SqlDbType.Char, 60)
            oParamSalida2.Direction = ParameterDirection.Output
            Sqlcommand.Parameters.Add(oParamSalida2)

            SqlLDataAdapter.SelectCommand = Sqlcommand
            SqlLDataAdapter.Fill(DS_Acciones, LstrNombreTabla)


            DS_Acciones_Aux = DS_Acciones

            strRetorno = Sqlcommand.Parameters("@pMsjRetorno").Value.ToString.Trim

            Return DS_Acciones_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function BuscarOpcionesMenu(ByRef strRetorno As String, _
                               ByVal intNumeroPantalla As Integer, _
                               ByVal intIdUsuario As Integer, _
                               ByVal intIdNegocio As Integer) As DataSet

        Dim DS_Acciones As New DataSet
        Dim DS_Acciones_Aux As New DataSet

        Dim Sqlcommand As New SqlClient.SqlCommand
        Dim SqlLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion

        lstrProcedimiento = "Sis_OpcionMenu_Consultar"
        LstrNombreTabla = "ACCIONES"
        Connect.Abrir()

        Try
            Sqlcommand = New SqlCommand(lstrProcedimiento, Connect.Conexion)

            Sqlcommand.CommandType = CommandType.StoredProcedure
            Sqlcommand.CommandText = lstrProcedimiento
            Sqlcommand.Parameters.Clear()

            Sqlcommand.Parameters.Add("@pCodigoOperacion", SqlDbType.Char, 15).Value = "BUSCARMENU"
            Sqlcommand.Parameters.Add("@pNumeroPantalla", SqlDbType.Float).Value = intNumeroPantalla
            Sqlcommand.Parameters.Add("@pIdUsuario", SqlDbType.Float).Value = intIdUsuario
            Sqlcommand.Parameters.Add("@pIdNegocio", SqlDbType.Float).Value = intIdNegocio

            Dim oParamSalida2 As New SqlClient.SqlParameter("@pMsjRetorno", SqlDbType.Char, 60)
            oParamSalida2.Direction = ParameterDirection.Output
            Sqlcommand.Parameters.Add(oParamSalida2)

            SqlLDataAdapter.SelectCommand = Sqlcommand
            SqlLDataAdapter.Fill(DS_Acciones, LstrNombreTabla)


            DS_Acciones_Aux = DS_Acciones

            strRetorno = Sqlcommand.Parameters("@pMsjRetorno").Value.ToString.Trim

            Return DS_Acciones_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function

    Public Function Buscar(ByRef strRetorno As String) As DataSet

        Dim DS_Acciones As New DataSet
        Dim DS_Acciones_Aux As New DataSet

        Dim Sqlcommand As New SqlClient.SqlCommand
        Dim SqlLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion

        lstrProcedimiento = "Sis_Accion_Consultar"
        LstrNombreTabla = "ACCIONES"
        Connect.Abrir()

        Try
            Sqlcommand = New SqlCommand(lstrProcedimiento, Connect.Conexion)

            Sqlcommand.CommandType = CommandType.StoredProcedure
            Sqlcommand.CommandText = lstrProcedimiento
            Sqlcommand.Parameters.Clear()

            Sqlcommand.Parameters.Add("@pCodigoOperacion", SqlDbType.Char, 15).Value = "TODOS"

            Sqlcommand.Parameters.Add("@pNumeroPantalla", SqlDbType.Int).Value = DBNull.Value

            Sqlcommand.Parameters.Add("@pNumeroLinea", SqlDbType.Int).Value = DBNull.Value


            Dim oParamSalida2 As New SqlClient.SqlParameter("@pMsjRetorno", SqlDbType.Char, 60)
            oParamSalida2.Direction = ParameterDirection.Output
            Sqlcommand.Parameters.Add(oParamSalida2)

            SqlLDataAdapter.SelectCommand = Sqlcommand
            SqlLDataAdapter.Fill(DS_Acciones, LstrNombreTabla)


            DS_Acciones_Aux = DS_Acciones

            strRetorno = Sqlcommand.Parameters("@pMsjRetorno").Value.ToString.Trim

            Return DS_Acciones_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function
    Public Function GetPantalla_FormularioControl(ByVal pTipoMenu As String, ByVal pNombreFormulario As String, Optional ByVal pNombreControl As String = "") As Long
        Dim strDescError As String = ""
        Dim llngPantalla As Long
        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As Cls_Conexion = New Cls_Conexion

        '...Abre la conexion
        SQLConnect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand("Sis_Accion_Pantalla", SQLConnect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = "Sis_Accion_Pantalla"
            SQLCommand.Parameters.Clear()

            SQLCommand.Parameters.Add("pTipoMenu", SqlDbType.VarChar, 20).Value = pTipoMenu
            SQLCommand.Parameters.Add("pNombreFormulario", SqlDbType.VarChar, 40).Value = pNombreFormulario
            SQLCommand.Parameters.Add("pNombreControl", SqlDbType.VarChar, 100).Value = IIf(pNombreControl.Trim = "", DBNull.Value, pNombreControl.Trim)

            '...pCantidad
            Dim ParametroSal As New SqlClient.SqlParameter("pPantalla", SqlDbType.Float, 10)
            ParametroSal.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal)

            '...Resultado
            Dim ParametroSal1 As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSal1.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSal1)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError = "OK" Then
                llngPantalla = SQLCommand.Parameters("pPantalla").Value
            End If

        Catch Ex As Exception
            strDescError = "|1|Error al obtener la pantalla del formulario a tratar." & vbCr & "[" & Ex.Message & "]|"
        Finally
            If strDescError <> "OK" Then
                If Not gFun_ResultadoMsg(strDescError, "GetPantalla_FormularioControl") Then
                    llngPantalla = 0
                End If
            End If
            SQLConnect.Cerrar()
            GetPantalla_FormularioControl = llngPantalla
        End Try
    End Function

    Public Function BuscarAccionesId(ByVal intIdAcccion As Integer, _
                                     ByRef strRetorno As String) As DataSet

        Dim DS_Acciones As New DataSet
        Dim DS_Acciones_Aux As New DataSet

        Dim Sqlcommand As New SqlClient.SqlCommand
        Dim SqlLDataAdapter As New SqlClient.SqlDataAdapter
        Dim Connect As Cls_Conexion = New Cls_Conexion

        lstrProcedimiento = "Sis_Accion_Consultar_ID"
        LstrNombreTabla = "ACCIONESID"
        Connect.Abrir()

        Try
            Sqlcommand = New SqlCommand(lstrProcedimiento, Connect.Conexion)

            Sqlcommand.CommandType = CommandType.StoredProcedure
            Sqlcommand.CommandText = lstrProcedimiento
            Sqlcommand.Parameters.Clear()

            Sqlcommand.Parameters.Add("@pIdAccion", SqlDbType.Int).Value = intIdAcccion

            Dim oParamSalida2 As New SqlClient.SqlParameter("@pRetorno", SqlDbType.Char, 60)
            oParamSalida2.Direction = ParameterDirection.Output
            Sqlcommand.Parameters.Add(oParamSalida2)

            SqlLDataAdapter.SelectCommand = Sqlcommand
            SqlLDataAdapter.Fill(DS_Acciones, LstrNombreTabla)


            DS_Acciones_Aux = DS_Acciones

            strRetorno = Sqlcommand.Parameters("@pRetorno").Value.ToString.Trim

            Return DS_Acciones_Aux

        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            Connect.Cerrar()
        End Try

    End Function
End Class
