﻿Public Class ClsParametrosSistema

    ' NOTA
    ' SI AGREGA UN NUEVO PARAMETRO EN LA TABLA SIS_PARAMETROS SE DEBE:
    ' 1º AGREGAR UN ELEMENTO EN LA NUMERACION "nParametro" QUE IDENTIFIQUE  EL NUEVO PARAMETRO ASIGNANDO COMO VALOR EL ID_PARAMETRO DE LA TABLA DEL NUEVO PARAMETRO 
    ' 2º AGREGAR UNA VARIABLE PUBLICA DEL TIPO "Parametros" QUE IDENTIFIQUE EL NUEVO PARAMETRO
    ' 3º AGREGAR EN EL SUB "RetornarParametros" DENTRO DEL SELECT CASE EL CASE QUE CORRESPONDA

    '+ NUMERACION DE LOS PARAMETROS, ESTO SEGUN ID_PARAMETRO DE LA TABLA DE PARAMETROS

    Enum nParametro As Integer
        Negocio = 1
        PorcentajeIva = 2
        DiasVigencia = 4
        PorcentajeComision = 5
        Sucursal = 6
        Dias_Exp_Clave = 7
        Cod_Pais = 8
        Correo_Cumplimiento = 22
        Ruta_Archivos = 23
        Servidor_Correo = 24
        Id_Cuenta_Consolida = 25
        Contraparte_Obligado = 26
        Forma_Operar_Cartera = 27
        Hora_Fin_Oper = 28
        Imp_Aut = 29
        Encabezado_Unico = 30
        MovCaja_SubTot_NConc = 31
        MovCaja_SubTot_TConc = 32
        MovCaja_Total = 33
        Tesoreria_Envio = 35 'HAF 20100624
        Evalua_En_Linea = 47
        Act_Datos_CM = 49
        Fecha_Cierre_Minimo = 60
        Cierra_Todas_Cuenta = 61
        Monto_Maximo_Reajuste = 62
        Porc_Aju_Conc = 73
        COD_OPEBOL_1 = 78
        COMI_GENERICA = 101
    End Enum

    '+ ESTRUCTURA DE LOS PARAMETROS. SEGUN ESTRUCTURA DE LA TABLA DE PARAMETROS

    Public Structure Parametros
        Private _IdParametro As String
        Private _CodigoParametro As String
        Private _NombreParametro As String
        Private _TipoDato As String
        Private _CaracterDecimal As String
        Private _FormatoFecha As String
        Private _FormatoNumerico As String
        Private _EsPorcentaje As String
        Private _CaracteresNoPermitidos As String
        Private _CantidadDecimales As String
        Private _ValorParametro As String
        Private _EstadoParametro As String
        Private _EsRango As String
        Private _RangoInicial As String
        Private _RangoFinal As String
        Private _EsPersistente As String

        Public Property IdParametro() As String
            Get
                Return _IdParametro
            End Get
            Set(ByVal value As String)
                _IdParametro = value
            End Set
        End Property

        Public Property CodigoParametro() As String
            Get
                Return _CodigoParametro
            End Get
            Set(ByVal value As String)
                _CodigoParametro = value
            End Set
        End Property

        Public Property NombreParametro() As String
            Get
                Return _NombreParametro
            End Get
            Set(ByVal value As String)
                _NombreParametro = value
            End Set
        End Property

        Public Property TipoDato() As String
            Get
                Return _TipoDato
            End Get

            Set(ByVal value As String)
                _TipoDato = value
            End Set
        End Property

        Public Property CaracterDecimal() As String
            Get
                Return _CaracterDecimal
            End Get

            Set(ByVal value As String)
                _CaracterDecimal = value
            End Set
        End Property

        Public Property FormatoFecha() As String
            Get
                Return _FormatoFecha
            End Get

            Set(ByVal value As String)
                _FormatoFecha = value
            End Set
        End Property

        Public Property FormatoNumerico() As String
            Get
                Return _FormatoNumerico
            End Get

            Set(ByVal value As String)
                _FormatoNumerico = value
            End Set
        End Property

        Public Property EsPorcentaje() As String
            Get
                Return _EsPorcentaje
            End Get

            Set(ByVal value As String)
                _EsPorcentaje = value
            End Set
        End Property

        Public Property CaracteresNoPermitidos() As String
            Get
                Return _CaracteresNoPermitidos
            End Get

            Set(ByVal value As String)
                _CaracteresNoPermitidos = value
            End Set
        End Property

        Public Property CantidadDecimales() As String
            Get
                Return _CantidadDecimales
            End Get

            Set(ByVal value As String)
                _CantidadDecimales = value
            End Set
        End Property

        'Public Property ValorParametro() As String
        '    Get
        '        If Me.EsPersistente = "NO" Then
        '            Dim objGeneral As ClsGeneral = New ClsGeneral
        '            Dim DS_Parametros As New DataSet
        '            Dim dtrParametro As DataRow
        '            Dim strResultado As String = ""
        '            For intIntentos As Integer = 0 To 2
        '                DS_Parametros = objGeneral.RetornarParametrosSistemas(Me.IdParametro, "", "", strResultado)
        '                If Not (IsNothing(DS_Parametros)) Then
        '                    Exit For
        '                End If
        '            Next

        '            If DS_Parametros.Tables("ParametrosSistema").Rows.Count > 0 Then
        '                dtrParametro = DS_Parametros.Tables("ParametrosSistema").Rows(0)
        '                If dtrParametro("TIPO_DATO").ToString.ToUpper.Trim = "NUMERICO" Then
        '                    dtrParametro("VALOR_PARAMETRO") = Replace(dtrParametro("VALOR_PARAMETRO"), dtrParametro("CARACTER_DECIMAL"), gtxtSeparadorDecimal.ToString)
        '                End If
        '                _ValorParametro = dtrParametro("VALOR_PARAMETRO")

        '                If Trim(dtrParametro("CODIGO_PARAMETRO")) = "CUENTA_CONSOLIDA" Then
        '                    Dim objCuenta As New ClsCuentas
        '                    Dim strRetorno As String = ""
        '                    Dim DS_Cuentas As DataSet

        '                    DS_Cuentas = objCuenta.TraerCuentas(glngNegocioOperacion, "", "", _ValorParametro, "", "", "", "", "ID_CUENTA", strRetorno, "")
        '                    If strRetorno = "OK" And DS_Cuentas.Tables("CUENTAS").Rows.Count > 0 Then
        '                        _ValorParametro = DS_Cuentas.Tables("CUENTAS").Rows(0).Item("ID_CUENTA").ToString.Trim
        '                    Else
        '                        _ValorParametro = ""
        '                    End If
        '                End If
        '            End If

        '        End If

        '        Return _ValorParametro
        '    End Get

        '    Set(ByVal value As String)
        '        _ValorParametro = value
        '    End Set
        'End Property

        Public Property EstadoParametro() As String
            Get
                Return _EstadoParametro
            End Get

            Set(ByVal value As String)
                _EstadoParametro = value
            End Set
        End Property

        Public Property EsRango() As String
            Get
                Return _EsRango
            End Get

            Set(ByVal value As String)
                _EsRango = value
            End Set
        End Property

        'Public Property RangoInicial() As String
        '    Get
        '        If EsPersistente = "NO" Then
        '            Dim objGeneral As ClsGeneral = New ClsGeneral
        '            Dim DS_Parametros As New DataSet
        '            Dim dtrParametro As DataRow
        '            Dim lstrResultado As String = ""

        '            DS_Parametros = objGeneral.RetornarParametrosSistemas(Me.IdParametro, "", "", lstrResultado)
        '            If DS_Parametros.Tables("ParametrosSistema").Rows.Count > 0 Then
        '                dtrParametro = DS_Parametros.Tables("ParametrosSistema").Rows(0)
        '                _RangoInicial = dtrParametro("RANGO_INICIAL")
        '            End If
        '        End If
        '        Return _RangoInicial
        '    End Get

        '    Set(ByVal value As String)
        '        _RangoInicial = value
        '    End Set
        'End Property

        'Public Property RangoFinal() As String
        '    Get
        '        If EsPersistente = "NO" Then
        '            Dim objGeneral As ClsGeneral = New ClsGeneral
        '            Dim DS_Parametros As New DataSet
        '            Dim strParametro As DataRow
        '            Dim lstrResultado As String = ""

        '            DS_Parametros = objGeneral.RetornarParametrosSistemas(Me.IdParametro, "", "", lstrResultado)
        '            If DS_Parametros.Tables("ParametrosSistema").Rows.Count > 0 Then
        '                strParametro = DS_Parametros.Tables("ParametrosSistema").Rows(0)
        '                _RangoFinal = strParametro("RANGO_FINAL")
        '            End If
        '        End If
        '        Return _RangoFinal
        '    End Get

        '    Set(ByVal value As String)
        '        _RangoFinal = value
        '    End Set
        'End Property

        Public Property EsPersistente() As String
            Get
                Return _EsPersistente
            End Get

            Set(ByVal value As String)
                _EsPersistente = value
            End Set

        End Property

        Public Sub New(ByVal a, ByVal b, ByVal c, ByVal d)

        End Sub

    End Structure

    '+ SE DECLARAN CADA UNO DE LOS PARAMETROS DEL TIPO DE LA ESTRUCTURA.
    '+ SE DEBEN AGREGAR TODOS LOS QUE ESTEN EN LA TABLA DE PARAMETROS

    Public Negocio As Parametros
    Public PorcentajeIva As Parametros
    Public PorcentajeComision As Parametros
    Public DiasVigencia As Parametros
    Public Sucursal As Parametros
    Public Dias_Exp_Clave As Parametros
    Public Cod_Pais As Parametros
    Public Cod_Suc_Gi As Parametros
    Public Limite_Correos As Parametros
    Public Correo_Cumplimiento As Parametros
    Public Ruta_Archivos As Parametros
    Public Servidor_Correo As Parametros
    Public Id_Cuenta_Consolida As Parametros
    Public Contraparte_Obligado As Parametros
    Public Forma_Operar_Cartera As Parametros
    Public Hora_Fin_Oper As Parametros
    Public Imp_Aut As Parametros
    Public Encabezado_Unico As Parametros
    Public MovCaja_SubTot_NConc As Parametros
    Public MovCaja_SubTot_TConc As Parametros
    Public MovCaja_Total As Parametros
    Public Tesoreria_Envio As Parametros
    Public Evalua_En_Linea As Parametros
    Public Act_Datos_CM As Parametros
    Public Fecha_Cierre_Minimo As Parametros
    Public Cierra_Todas_Cuenta As Parametros
    Public Monto_Maximo_Reajuste As Parametros
    Public Porc_Aju_Conc As Parametros
    Public COD_OPEBOL_1 As Parametros
    Public COMI_GENERICA As Parametros

    '+ CONSTRUCTOR DE LA CLASE
    '+ INICIALIZA CADA UNO DE LOS PARAMETROS QUE ESTEN EN LA TABLA Y ESTEN DECLARADOS
    'Public Sub New()
    '    Dim lstrIdParametro As String = ""
    '    Dim lstrEstadoParametro As String = ""
    '    RetornarParametros(lstrIdParametro, lstrEstadoParametro)
    'End Sub

    ''+ METODO QUE REFRESCA EL VALOR DE UN PARAMETRO ID_PARAMETRO INDICADO EN LTXT_IDPARAMETRO
    'Public Sub RefrescaValor(ByVal strIdParametro As String)
    '    Dim lstrEstadoParametro As String = ""
    '    RetornarParametros(strIdParametro, lstrEstadoParametro)
    'End Sub

    '+ RUTINA QUE REALIZA EL LLAMADO AL WEBSERVICE PARA PODER CARGAR LAS ESTRUCTURAS DE LOS PARAMETROS

    'Private Sub RetornarParametros(ByVal strIdParametro As String, ByVal strEstadoParametro As String)
    '    Dim objParametros As ClsGeneral = New ClsGeneral
    '    Dim DS_Parametros As New DataSet
    '    Dim dtrParametro As DataRow
    '    Dim LTxt_Resultado As String = ""

    '    For LInt_Intentos As Integer = 0 To 2
    '        DS_Parametros = objParametros.RetornarParametrosSistemas(strIdParametro, strEstadoParametro, "", LTxt_Resultado)
    '        If Not IsNothing(DS_Parametros) Then
    '            Exit For
    '        End If
    '    Next
    '    If LTxt_Resultado.ToUpper.Trim <> "OK" Then
    '        '    MsgBox(LTxt_Resultado, MsgBoxStyle.Critical, My.Application.Info.Title)
    '        Exit Sub
    '    End If
    '    For Each dtrParametro In DS_Parametros.Tables("ParametrosSistema").Rows
    '        Dim parametro As New Parametros With {
    '            .IdParametro = IIf(IsDBNull(dtrParametro("ID_PARAMETRO")), "", dtrParametro("ID_PARAMETRO")),
    '            .CodigoParametro = IIf(IsDBNull(dtrParametro("CODIGO_PARAMETRO")), "", dtrParametro("CODIGO_PARAMETRO")),
    '            .NombreParametro = IIf(IsDBNull(dtrParametro("NOMBRE_PARAMETRO")), "", dtrParametro("NOMBRE_PARAMETRO")),
    '            .TipoDato = IIf(IsDBNull(dtrParametro("TIPO_DATO")), "", dtrParametro("TIPO_DATO")),
    '            .CaracterDecimal = IIf(IsDBNull(dtrParametro("CARACTER_DECIMAL")), "", dtrParametro("CARACTER_DECIMAL")),
    '            .FormatoFecha = IIf(IsDBNull(dtrParametro("FORMATO_FECHA")), "", dtrParametro("FORMATO_FECHA")),
    '            .FormatoNumerico = IIf(IsDBNull(dtrParametro("FORMATO_NUMERICO")), "", dtrParametro("FORMATO_NUMERICO")),
    '            .EsPorcentaje = IIf(IsDBNull(dtrParametro("ES_PORCENTAJE_BASE_100")), "", dtrParametro("ES_PORCENTAJE_BASE_100")),
    '            .CaracteresNoPermitidos = IIf(IsDBNull(dtrParametro("CARACTERES_NO_PERMITIDOS")), "", dtrParametro("CARACTERES_NO_PERMITIDOS")),
    '            .CantidadDecimales = IIf(IsDBNull(dtrParametro("CANTIDAD_DECIMALES")), "", dtrParametro("CANTIDAD_DECIMALES")),
    '            .ValorParametro = IIf(IsDBNull(dtrParametro("VALOR_PARAMETRO")), "", dtrParametro("VALOR_PARAMETRO")),
    '            .EstadoParametro = IIf(IsDBNull(dtrParametro("ESTADO_PARAMETRO")), "", dtrParametro("ESTADO_PARAMETRO")),
    '            .EsRango = IIf(IsDBNull(dtrParametro("ESRANGO")), "", dtrParametro("ESRANGO")),
    '            .RangoInicial = IIf(IsDBNull(dtrParametro("RANGO_INICIAL")), "", dtrParametro("RANGO_INICIAL")),
    '            .RangoFinal = IIf(IsDBNull(dtrParametro("RANGO_FINAL")), "", dtrParametro("RANGO_FINAL")),
    '            .EsPersistente = IIf(IsDBNull(dtrParametro("EsPersistente")), "", dtrParametro("EsPersistente"))
    '        }
    '        If parametro.TipoDato.ToUpper.Trim = "NUMERICO" Then
    '            parametro.ValorParametro = Replace(parametro.ValorParametro, parametro.CaracterDecimal, gtxtSeparadorDecimal.ToString)
    '        End If

    '        Select Case dtrParametro("ID_PARAMETRO")
    '            Case nParametro.Negocio
    '                Negocio = parametro
    '            Case nParametro.PorcentajeIva
    '                PorcentajeIva = parametro
    '            Case nParametro.DiasVigencia
    '                DiasVigencia = parametro
    '            Case nParametro.PorcentajeComision
    '                PorcentajeComision = parametro
    '            Case nParametro.Sucursal
    '                Sucursal = parametro
    '            Case nParametro.Correo_Cumplimiento
    '                Correo_Cumplimiento = parametro
    '            Case nParametro.Ruta_Archivos
    '                Ruta_Archivos = parametro
    '            Case nParametro.Servidor_Correo
    '                Servidor_Correo = parametro
    '            Case nParametro.Id_Cuenta_Consolida
    '                Id_Cuenta_Consolida = parametro
    '            Case nParametro.Contraparte_Obligado
    '                Contraparte_Obligado = parametro
    '            Case nParametro.Forma_Operar_Cartera
    '                Forma_Operar_Cartera = parametro
    '            Case nParametro.Dias_Exp_Clave
    '                Dias_Exp_Clave = parametro
    '            Case nParametro.Cod_Pais
    '                Cod_Pais = parametro
    '            Case nParametro.Hora_Fin_Oper
    '                Hora_Fin_Oper = parametro
    '            Case nParametro.Imp_Aut
    '                Imp_Aut = parametro
    '            Case nParametro.Encabezado_Unico
    '                Encabezado_Unico = parametro
    '            Case nParametro.MovCaja_SubTot_NConc
    '                MovCaja_SubTot_NConc = parametro
    '            Case nParametro.MovCaja_SubTot_TConc
    '                MovCaja_SubTot_TConc = parametro
    '            Case nParametro.MovCaja_Total
    '                MovCaja_Total = parametro
    '            Case nParametro.Tesoreria_Envio
    '                Tesoreria_Envio = parametro
    '            Case nParametro.Tesoreria_Envio
    '                Tesoreria_Envio = parametro
    '            Case nParametro.Evalua_En_Linea
    '                Evalua_En_Linea = parametro
    '            Case nParametro.Act_Datos_CM
    '                Act_Datos_CM = parametro
    '            Case nParametro.Fecha_Cierre_Minimo
    '                Fecha_Cierre_Minimo = parametro
    '            Case nParametro.Cierra_Todas_Cuenta
    '                Cierra_Todas_Cuenta = parametro
    '            Case nParametro.Monto_Maximo_Reajuste
    '                Monto_Maximo_Reajuste = parametro
    '            Case nParametro.Porc_Aju_Conc
    '                Porc_Aju_Conc = parametro
    '            Case nParametro.COD_OPEBOL_1
    '                COD_OPEBOL_1 = parametro
    '            Case nParametro.COMI_GENERICA
    '                COMI_GENERICA = parametro
    '        End Select
    '    Next
    'End Sub

    Public Function RetornarValorParametro(ByVal strpCodigoParametro As String, ByRef strRetorno As String) As DataSet
        Dim dsAuxiliar As New DataSet
        Dim dsAuxiliarAux As New DataSet
        Dim lstrProcedimiento = "Sis_ParametrosConsulta"
        Dim sqlCommand As New SqlClient.SqlCommand
        Dim sqlDataAdapter As New SqlClient.SqlDataAdapter
        Dim connect As New Cls_Conexion
        connect.Abrir()
        Try
            sqlCommand = New SqlClient.SqlCommand(lstrProcedimiento, connect.Conexion) With {
                .CommandType = CommandType.StoredProcedure,
                .CommandText = lstrProcedimiento
            }
            sqlCommand.Parameters.Clear()
            sqlCommand.Parameters.Add("pIdNegocio", SqlDbType.Int, 20).Value = 0
            sqlCommand.Parameters.Add("pCodigo", SqlDbType.VarChar, 20).Value = strpCodigoParametro.Trim
            sqlCommand.Parameters.Add("pAccion", SqlDbType.VarChar, 20).Value = ""
            sqlCommand.Parameters.Add("@pResultado", SqlDbType.VarChar, 20).Value = ""

            sqlCommand.ExecuteNonQuery()
            sqlDataAdapter.SelectCommand = sqlCommand
            sqlDataAdapter.Fill(dsAuxiliar)
            dsAuxiliarAux = dsAuxiliar
            dsAuxiliar.Dispose()
            strRetorno = "OK"
            Return dsAuxiliarAux
        Catch ex As Exception
            strRetorno = ex.Message
            Return Nothing
        Finally
            connect.Cerrar()
        End Try
    End Function
End Class
