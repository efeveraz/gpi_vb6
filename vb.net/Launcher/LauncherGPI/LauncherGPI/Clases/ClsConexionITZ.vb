﻿Imports Microsoft.VisualBasic
Imports System
'Imports System.Data
Imports System.Data.SqlClient

Public Class ClsConexionITZ
    Shared _conConexion As SqlConnection
    Private blnIsOpen As Boolean = False
    Public _strServerName As String
    Public _strDatabaseName As String
    Public _strUserID As String
    Public _strPassword As String

    Public Property ServerName() As String
        Get
            Return _strServerName
        End Get
        Set(ByVal valor As String)
            _strServerName = valor
        End Set
    End Property
    Public Property DatabaseName() As String
        Get
            Return _strDatabaseName
        End Get
        Set(ByVal valor As String)
            _strDatabaseName = valor
        End Set
    End Property
    Public Property UserID() As String
        Get
            Return _strUserID
        End Get
        Set(ByVal valor As String)
            _strUserID = valor
        End Set
    End Property
    Public Property Password() As String
        Get
            Return _strPassword
        End Get
        Set(ByVal valor As String)
            _strPassword = valor
        End Set
    End Property
    Public Property Conexion() As SqlConnection
        Get
            Return _conConexion
        End Get
        Set(ByVal valor As SqlConnection)
            _conConexion = valor
        End Set
    End Property
    Public Sub Abrir()
        Try
            If Not blnIsOpen Then
                Me.Conexion = New SqlConnection(gstrCadenaConexion_ITZ)
                ' Conexion.ConnectionTimeout
                ' Abre la conección.
                Conexion.Open()
                blnIsOpen = True
            End If

        Catch ex As Exception
            'gFun_ResultadoMsg("1|" & ex.Message, "ClsConexionITZ.Abrir")
        End Try
    End Sub

    Public Sub Cerrar()
        If blnIsOpen Then
            Conexion.Close()
            blnIsOpen = False
        End If
    End Sub

    Public Function GetConnectionString_ITZ(ByVal strPathArchivoIni) As String
        Dim lintIndice As Integer
        Dim lstrTipoConexion_ITZ As String
        Dim lstrClavesConexion() As String
        Dim lstrConexion As String = ""
        Dim lstrAmbiente_ITZ As String = ""
        Dim lobjUsuario As ClsUsuario = New ClsUsuario

        Try

            lstrTipoConexion_ITZ = ObtieneValor_ArchivoIni(strPathArchivoIni, "CONEXIONBD_ITZ", "Tipo_Conexion_ITZ", "CONEXION_ESTANDAR_ITZ")
            lstrAmbiente_ITZ = ObtieneValor_ArchivoIni(strPathArchivoIni, "AMBIENTEBD_ITZ", "Ambiente_Itz", "")
            lstrClavesConexion = ObtieneSeccion_ArchivoIni(strPathArchivoIni, lstrTipoConexion_ITZ & " " & lstrAmbiente_ITZ)

            For lintIndice = 0 To UBound(lstrClavesConexion)
                If gModoAutenticacion = "SQL SERVER" Or _
                (gModoAutenticacion = "WINDOWS" And (lstrClavesConexion(lintIndice).Contains("Data Source") Or lstrClavesConexion(lintIndice).Contains("Initial Catalog"))) Then
                    If lstrClavesConexion(lintIndice).Contains("Password") Then
                        lstrConexion = lstrConexion & lstrClavesConexion(lintIndice).Substring(0, 9)
                        If gLogoCliente = "HSBC" Then
                            lstrConexion = lstrConexion & gFunEncripta(lstrClavesConexion(lintIndice).Substring(9), False) & "; "
                        Else
                            lstrConexion = lstrConexion & lobjUsuario.DesEncriptar(lstrClavesConexion(lintIndice).Substring(9)) & "; "
                        End If

                    Else
                        lstrConexion = lstrConexion & lstrClavesConexion(lintIndice) & "; "
                    End If
                End If
            Next
            If gModoAutenticacion = "WINDOWS" Then
                lstrConexion = lstrConexion & "Integrated Security = True"
            End If
            Return lstrConexion

        Catch ex As Exception
            Return lstrConexion
        End Try

    End Function

    Public Function Transaccion() As SqlTransaction
        Return _conConexion.BeginTransaction()
    End Function

    Public Sub New()
        blnIsOpen = False
    End Sub

    Public Sub Itz_ExecProcDataSet(ByRef LDS_CargaMasiva As DataSet, _
                                   ByVal strNombreProc As String, _
                                   ByVal strNombreTabla As String, _
                                   ByVal ArrstrColumnas As String, _
                                   ByVal ArrstrParametros As String, _
                                   ByRef strRetorno As String)

        Dim LDS_CargaMasiva_Aux As New DataSet
        Dim LArr_NombreColumna() As String = Split(ArrstrColumnas, ",")
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String
        Dim Columna As DataColumn
        Dim Remove As Boolean
        Dim lArrRegistros() As String
        Dim lArrCampos() As String
        Dim lSepLinea As String = "©"
        Dim lSepCampos As String = "|"
        Dim lstrParamOutSal As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLDataAdapter As New SqlClient.SqlDataAdapter

        Dim Connect As ClsConexionITZ = New ClsConexionITZ

        Connect.Abrir()

        Try
            SQLCommand = New SqlClient.SqlCommand(strNombreProc, Connect.Conexion)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = strNombreProc
            SQLCommand.Parameters.Clear()

            If ArrstrParametros.Trim <> "" Then
                '+ Si viene el separador de línea al principio se elimina porque no debe estar
                If Mid(ArrstrParametros, 1, 1) = lSepLinea Then
                    ArrstrParametros = Mid(ArrstrParametros, 2, Len(ArrstrParametros))
                End If

                '+ Si viene el separador de línea al final se elimina porque no debe estar
                If Mid(ArrstrParametros, Len(ArrstrParametros), 1) = lSepLinea Then
                    ArrstrParametros = Mid(ArrstrParametros, 1, Len(ArrstrParametros) - 1)
                End If


                lArrRegistros = ArrstrParametros.Split(lSepLinea)
                For lintIndiceR = 0 To lArrRegistros.Length - 1
                    lArrCampos = lArrRegistros(lintIndiceR).Split(lSepCampos)
                    '************************************************************************
                    '* Estructura de los campos: |NombreParam|TipoDato|Largo|TipoParam|Valor|
                    '+                           |     0     |   1    |  2  |    3    |  4  |
                    '************************************************************************
                    '+ NombreParam: Es el nombre del parámetro en el sp
                    '+ TipoDato: Es el tipo de dato asociado en el sp -> 0)Char, 1)Varchar, 2)Int, 3)Float y 4)Date
                    '+ Largo: Es la longitud del parámetro
                    '+ TipoParam: Es el tipo de parámetro a tratar: "OUT" de salida de lo contrario de entrada
                    '+ Valor: Es el valor a asignar al parámetro

                    '+ Si existe información de los campos para procesar
                    If lArrCampos.Length > 0 Then
                        '+ Si el tipo de parámetro es de salida (OUT)
                        If lArrCampos(3).ToUpper = "OUT" Then

                        Else
                            Select Case lArrCampos(1)
                                Case Est_TipoDatoParam.enuChar
                                    SQLCommand.Parameters.Add(lArrCampos(0), SqlDbType.Char, lArrCampos(2)).Value = IIf(lArrCampos(4) = "NULL", DBNull.Value, lArrCampos(4))

                                Case Est_TipoDatoParam.enuVarChar
                                    SQLCommand.Parameters.Add(lArrCampos(0), SqlDbType.VarChar, lArrCampos(2)).Value = IIf(lArrCampos(4) = "NULL", DBNull.Value, lArrCampos(4))

                                Case Est_TipoDatoParam.enuInt
                                    SQLCommand.Parameters.Add(lArrCampos(0), SqlDbType.Int, lArrCampos(2)).Value = IIf(lArrCampos(4) = "NULL", DBNull.Value, lArrCampos(4))

                                Case Est_TipoDatoParam.enuFloat
                                    SQLCommand.Parameters.Add(lArrCampos(0), SqlDbType.Float, lArrCampos(2)).Value = IIf(lArrCampos(4) = "NULL", DBNull.Value, lArrCampos(4))

                                Case Est_TipoDatoParam.enuDate
                                    SQLCommand.Parameters.Add(lArrCampos(0), SqlDbType.DateTime, lArrCampos(2)).Value = IIf(lArrCampos(4) = "NULL", DBNull.Value, lArrCampos(4))

                            End Select
                        End If
                    End If
                Next
            End If

            SQLDataAdapter.SelectCommand = SQLCommand
            SQLDataAdapter.Fill(LDS_CargaMasiva, strNombreTabla)

            LDS_CargaMasiva_Aux = LDS_CargaMasiva

            If ArrstrColumnas.Trim = "" Then
                LDS_CargaMasiva_Aux = LDS_CargaMasiva
            Else
                LDS_CargaMasiva_Aux = LDS_CargaMasiva.Copy
                For Each Columna In LDS_CargaMasiva.Tables(strNombreTabla).Columns
                    Remove = True
                    For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                        LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                        If Columna.ColumnName = LInt_NomCol Then
                            Remove = False
                        End If
                    Next
                    If Remove Then
                        LDS_CargaMasiva_Aux.Tables(strNombreTabla).Columns.Remove(Columna.ColumnName)
                    End If
                Next
            End If

            For LInt_Col = 0 To LArr_NombreColumna.Length - 1
                LInt_NomCol = LArr_NombreColumna(LInt_Col).TrimEnd.TrimStart
                For Each Columna In LDS_CargaMasiva_Aux.Tables(strNombreTabla).Columns
                    If Columna.ColumnName = LInt_NomCol Then
                        Columna.SetOrdinal(LInt_Col)
                        Exit For
                    End If
                Next
            Next

            LDS_CargaMasiva = LDS_CargaMasiva_Aux

        Catch ex As Exception
            strRetorno = ex.Message
        Finally
            Connect.Cerrar()
        End Try
    End Sub
    Public Function Itz_ExecProcUpd(ByVal strNombreProc As String, _
                                    ByVal ArrstrParametros As String) As String

        Dim lArrRegistros() As String
        Dim lArrCampos() As String
        Dim lSepLinea As String = "©"
        Dim lSepCampos As String = "|"
        Dim strDescError As String = ""

        Dim SQLCommand As New SqlClient.SqlCommand
        Dim SQLConnect As ClsConexionITZ = New ClsConexionITZ
        Dim MiTransaccionSQL As SqlClient.SqlTransaction

        '...Abre la conexion
        SQLConnect.Abrir()
        MiTransaccionSQL = SQLConnect.Transaccion

        Try
            SQLCommand = New SqlClient.SqlCommand(strNombreProc, MiTransaccionSQL.Connection, MiTransaccionSQL)

            SQLCommand.CommandType = CommandType.StoredProcedure
            SQLCommand.CommandText = strNombreProc
            SQLCommand.Parameters.Clear()

            If ArrstrParametros.Trim <> "" Then
                '+ Si viene el separador de línea al principio se elimina porque no debe estar
                If Mid(ArrstrParametros, 1, 1) = lSepLinea Then
                    ArrstrParametros = Mid(ArrstrParametros, 2, Len(ArrstrParametros))
                End If

                '+ Si viene el separador de línea al final se elimina porque no debe estar
                If Mid(ArrstrParametros, Len(ArrstrParametros), 1) = lSepLinea Then
                    ArrstrParametros = Mid(ArrstrParametros, 1, Len(ArrstrParametros) - 1)
                End If

                lArrRegistros = ArrstrParametros.Split(lSepLinea)
                For lintIndiceR = 0 To lArrRegistros.Length - 1
                    lArrCampos = lArrRegistros(lintIndiceR).Split(lSepCampos)
                    '************************************************************************
                    '* Estructura de los campos: |NombreParam|TipoDato|Largo|TipoParam|Valor|
                    '+                           |     0     |   1    |  2  |    3    |  4  |
                    '************************************************************************
                    '+ NombreParam: Es el nombre del parámetro en el sp
                    '+ TipoDato: Es el tipo de dato asociado en el sp -> 0)Char, 1)Varchar, 2)Int, 3)Float y 4)Date
                    '+ Largo: Es la longitud del parámetro
                    '+ TipoParam: Es el tipo de parámetro a tratar: "OUT" de salida de lo contrario de entrada
                    '+ Valor: Es el valor a asignar al parámetro

                    '+ Si existe información de los campos para procesar
                    If lArrCampos.Length > 0 Then
                        '+ Si el tipo de parámetro es de salida (OUT)
                        If lArrCampos(3).ToUpper = "OUT" Then

                        Else
                            Select Case lArrCampos(1)
                                Case Est_TipoDatoParam.enuChar
                                    SQLCommand.Parameters.Add(lArrCampos(0), SqlDbType.Char, lArrCampos(2)).Value = IIf(lArrCampos(4) = "NULL", DBNull.Value, lArrCampos(4))

                                Case Est_TipoDatoParam.enuVarChar
                                    SQLCommand.Parameters.Add(lArrCampos(0), SqlDbType.VarChar, lArrCampos(2)).Value = IIf(lArrCampos(4) = "NULL", DBNull.Value, lArrCampos(4))

                                Case Est_TipoDatoParam.enuInt
                                    SQLCommand.Parameters.Add(lArrCampos(0), SqlDbType.Int, lArrCampos(2)).Value = IIf(lArrCampos(4) = "NULL", DBNull.Value, lArrCampos(4))

                                Case Est_TipoDatoParam.enuFloat
                                    SQLCommand.Parameters.Add(lArrCampos(0), SqlDbType.Float, lArrCampos(2)).Value = IIf(lArrCampos(4) = "NULL", DBNull.Value, lArrCampos(4))

                                Case Est_TipoDatoParam.enuDate
                                    SQLCommand.Parameters.Add(lArrCampos(0), SqlDbType.DateTime, lArrCampos(2)).Value = IIf(lArrCampos(4) = "NULL", DBNull.Value, lArrCampos(4))

                            End Select
                        End If
                    End If
                Next
            End If

            Dim ParametroSalFijo As New SqlClient.SqlParameter("pResultado", SqlDbType.VarChar, 200)
            ParametroSalFijo.Direction = Data.ParameterDirection.Output
            SQLCommand.Parameters.Add(ParametroSalFijo)

            SQLCommand.ExecuteNonQuery()

            strDescError = SQLCommand.Parameters("pResultado").Value.ToString.Trim

            If strDescError.ToUpper.Trim = "OK" Then
                MiTransaccionSQL.Commit()
            Else
                MiTransaccionSQL.Rollback()
            End If

        Catch Ex As Exception
            MiTransaccionSQL.Rollback()
            strDescError = "Error en la actualización [Itz_ExecProcUpd -" & strNombreProc & "]." & vbCr & Ex.Message
        Finally
            SQLConnect.Cerrar()
            Itz_ExecProcUpd = strDescError
        End Try
    End Function
End Class
