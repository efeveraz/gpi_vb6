﻿Module gDeclaracionesEsc

    ' MANEJO DE LA CLASE CUENTAS
    'Public gobjCuentas As New Cls_Cuentas
    'Public gobjVistaActivos As New ClsVistaActivos


    ' VISTA CONSOLIDADA DE ACTIVOS
    Public blnVistaConsolActivoOpen As Boolean = False
    Public strTituloPantallaCnslActvs As String
    Public strCartolaActivoCnslActvs As String

    ' VISTA DETALLE DE ACTIVOS
    Public colFrmArrayDetActvs As Collection
    Public strTituloPantallaDetActvs As String
    Public strTipoDetActvs As String
    Public strCodProducto_DetAct As String
    Public strCodSubPproducto_DetAct As String
    Public strRangoVencimiento As String

    ' VISTA DETALLE OPERACIONES
    Public strTituloPantallaDetOpe As String
    Public strTipoDetOpe As String
    Public strNemoDetOper As String ' Nemotecnico enviado como parametro para el detalle de operaciones

    ' VISTA DATOS OPERACION
    Public dblNumeroOperacion As Double
    Public dblCorrelativo As Double

    ' CONTROL CIERRE DE PANTALLAS
    Public blnCerrando As Boolean = False

End Module
