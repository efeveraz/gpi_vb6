Imports System.IO
Imports C1.Win.C1List
Imports Microsoft.Office.Interop
Imports System.ComponentModel
Imports System.Threading
Imports System.Net.Mail
Imports System.Net
Imports System.Net.Mime

Public Module gFuncionesRecursosComunes

    'Dim lobjConcepto As ClsBloqueo = New ClsBloqueo

#Region " Funciones Victor Caro "

    '...Declaraci�n de la API
    Private Declare Auto Function SetProcessWorkingSetSize Lib "kernel32.dll" (ByVal procHandle As IntPtr, ByVal min As Int32, ByVal max As Int32) As Boolean

    Public Function gFunExisteAmbienteConexion() As String
        gFunExisteAmbienteConexion = "OK"
        Dim Connect As Cls_Conexion = New Cls_Conexion
        Try
            Connect.Abrir()
        Catch ex As Exception
            gFunExisteAmbienteConexion = vbCrLf & "__ NO SE PUDO ESTABLECER CONEXI�N CON LA BASE DE DATOS __" & vbCrLf & vbCrLf
            gFunExisteAmbienteConexion = gFunExisteAmbienteConexion & "AMBIENTE: [ " & gtxtAmbienteSistema & " ]"
        Finally
            Connect.Cerrar()
        End Try
    End Function

    Public Function gFunControlTienePermiso(ByVal strNombreControl As String,
                                            ByVal ArregloDePermisos(,) As String) As Boolean
        Dim lblnTienePermiso As Boolean = False
        ' --+ validacion valida que traiga al menos un permiso
        If ArregloDePermisos.Length <= 1 Then
            gFunControlTienePermiso = lblnTienePermiso
            Exit Function
        End If

        For lintFor As Integer = 0 To ArregloDePermisos.GetLength(0) - 1
            If strNombreControl.ToUpper.Trim = ArregloDePermisos(lintFor, 1).ToUpper.Trim Then
                lblnTienePermiso = True
                Exit For
            End If
        Next
        gFunControlTienePermiso = lblnTienePermiso
    End Function

    Public Function gFunFechaFormatoConfiguracionRegional(ByVal str_Fecha_DD_MM_AAAA As String) As String
        Try
            '  "26-06-2008"
            '   0123456789
            Dim MiFecha As String = str_Fecha_DD_MM_AAAA.Trim
            If MiFecha.Trim <> "" Then
                Dim FormatoFechaActual = System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern
                FormatoFechaActual = FormatoFechaActual.ToString.ToUpper.Replace("Y", "A")
                FormatoFechaActual = FormatoFechaActual.ToString.Replace(".", "/")
                FormatoFechaActual = FormatoFechaActual.ToString.Replace("-", "/")
                FormatoFechaActual = FormatoFechaActual.ToString.ToUpper
                Select Case FormatoFechaActual
                    Case "DD/MM/AAAA"
                        MiFecha = MiFecha.Trim
                    Case "AAAA/MM/DD"
                        MiFecha = MiFecha.Substring(6, 4) & "/" & MiFecha.Substring(3, 2) & "/" & MiFecha.Substring(0, 2)
                    Case "DD/MM/AA"
                        MiFecha = MiFecha.Substring(0, 2) & "/" & MiFecha.Substring(3, 2) & "/" & MiFecha.Substring(8, 2)
                    Case "AA/MM/DD"
                        MiFecha = MiFecha.Substring(8, 2) & "/" & MiFecha.Substring(3, 2) & "/" & MiFecha.Substring(0, 2)
                    Case "M/D/AAAA"
                        MiFecha = MiFecha.Substring(3, 2).PadRight(2, "0") & "/" & MiFecha.Substring(0, 2).PadRight(2, "0") & "/" & MiFecha.Substring(6, 4)
                    Case "AAAA/D/M"
                        MiFecha = MiFecha.Substring(6, 4) & "/" & MiFecha.Substring(0, 2).PadRight(2, "0") & "/" & MiFecha.Substring(3, 2)
                    Case "AAAA/M/D"
                        MiFecha = MiFecha.Substring(6, 4) & "/" & MiFecha.Substring(3, 2).PadRight(2, "0") & "/" & MiFecha.Substring(0, 2)
                    Case "D/M/AAAA"
                        MiFecha = MiFecha.Substring(0, 2).PadRight(2, "0") & "/" & MiFecha.Substring(3, 2).PadRight(2, "0") & "/" & MiFecha.Substring(6, 4)
                    Case "D/M/AA"
                        MiFecha = MiFecha.Substring(0, 2).PadRight(2, "0") & "/" & MiFecha.Substring(3, 2).PadRight(2, "0") & "/" & MiFecha.Substring(6, 4)
                    Case "M/D/AA"
                        MiFecha = MiFecha.Substring(3, 2).PadRight(2, "0") & "/" & MiFecha.Substring(0, 2).PadRight(2, "0") & "/" & MiFecha.Substring(8, 2)
                    Case "AA/D/M"
                        MiFecha = MiFecha.Substring(8, 2) & "/" & MiFecha.Substring(0, 2).PadRight(2, "0") & "/" & MiFecha.Substring(3, 2).PadRight(2, "0")
                    Case Else
                        MiFecha = MiFecha
                End Select
            End If
            gFunFechaFormatoConfiguracionRegional = MiFecha.Trim
        Catch ex As Exception
            gFunFechaFormatoConfiguracionRegional = ""
        End Try
    End Function

    Public Sub gSubGuardaAgrupacionGrilla(ByVal MyGrilla As C1.Win.C1TrueDBGrid.C1TrueDBGrid)
        '...Salva las agrupaciones de una grilla
        Try
            If MyGrilla.RowCount > 0 Then
                Dim int_Index As Integer
                Dim arr_GroupBy(MyGrilla.GroupedColumns.Count - 1) As String

                For int_Index = 0 To MyGrilla.GroupedColumns.Count - 1
                    arr_GroupBy(int_Index) = MyGrilla.GroupedColumns.Item(int_Index).DataField.ToString() & "|"

                    If MyGrilla.GroupedColumns.Item(int_Index).SortDirection.ToString.Trim() = "Ascending" Then
                        arr_GroupBy(int_Index) = arr_GroupBy(int_Index) & "1"
                    Else
                        arr_GroupBy(int_Index) = arr_GroupBy(int_Index) & "2"
                    End If
                Next
                garrColumnaAgrupacion = arr_GroupBy
            End If
        Catch ex As Exception
            MsgBox("Error al Salvar Agrupaci�n.", vbCritical, My.Application.Info.Title & " : GSub_GuardaAgrupacionGrilla")
        End Try
    End Sub

    Public Sub gSubRecuperaAgrupacionGrilla(ByRef MyGrilla As C1.Win.C1TrueDBGrid.C1TrueDBGrid, ByVal MyKeyGrilla As String)
        '...Recupera las agrupaciones de una grilla
        Try
            Dim int_Index As Integer
            For int_Index = 0 To garrColumnaAgrupacion.Length - 1
                Dim ColOrder() = garrColumnaAgrupacion(int_Index).Split("|")
                MyGrilla.GroupedColumns.Add(MyGrilla.Columns(ColOrder(0).ToString.Trim))
                MyGrilla.GroupedColumns.Item(ColOrder(0).ToString.Trim).SortDirection = CInt(ColOrder(1))
            Next
            '...Genera otra agrupacion y la elimina ( para que la grilla agrupe en forma correcta )
            MyGrilla.GroupedColumns.Add(MyGrilla.Columns(MyKeyGrilla))
            MyGrilla.GroupedColumns.Item(MyKeyGrilla).SortDirection = C1.Win.C1TrueDBGrid.SortDirEnum.None
            MyGrilla.GroupedColumns.RemoveAt(MyGrilla.GroupedColumns.Count - 1)
        Catch ex As Exception
            Exit Sub
        End Try
    End Sub

    Public Sub gSubGuardaFiltrosGrilla(ByVal MyGrilla As C1.Win.C1TrueDBGrid.C1TrueDBGrid, ByRef G_arr_ColFiltros() As String)
        Try
            Dim int_Index As Integer
            Dim arr_Filtros(MyGrilla.Columns.Count - 1) As String
            For int_Index = 0 To MyGrilla.Columns.Count - 1
                arr_Filtros(int_Index) = MyGrilla.Columns.Item(int_Index).FilterText.Trim
            Next
            G_arr_ColFiltros = arr_Filtros
        Catch ex As Exception
            MsgBox("Error al guardar filtros de la lista.", vbCritical, My.Application.Info.Title & " : gSubGuardaFiltrosGrilla")
        End Try
    End Sub

    Public Sub gSubRecuperaFiltrosGrilla(ByRef MyGrilla As C1.Win.C1TrueDBGrid.C1TrueDBGrid, ByVal G_arr_ColFiltros() As String)
        Try
            Dim int_Index As Integer
            For int_Index = 0 To G_arr_ColFiltros.Length - 1
                If G_arr_ColFiltros(int_Index).Trim <> "" Then
                    MyGrilla.Columns.Item(int_Index).FilterText = G_arr_ColFiltros(int_Index).Trim
                End If
            Next
        Catch ex As Exception
            Exit Sub
        End Try
    End Sub

    Public Function gFunValidaCaracterAlfaNumerico(
                    ByVal KeyChar As Char,
                    Optional ByVal blnSoloMayusculas As Boolean = True,
                    Optional ByVal Beper As Boolean = True) As Char
        '...Valida en keypress que solo permita los caracteres en los string "strMayuscula" y "strMinuscula"
        Try
            Dim strMayuscula As String
            Dim strMinuscula As String
            Dim strCompara As String
            strMayuscula = " ABCDEFGHIJKLMN�OPQRSTUVWXYZ_-+*=/1234567890,.:@#$()?�"
            strMinuscula = " abcdefghijklmn�opqrstuvwxyz_-+*=/1234567890,.:@#$()?�"
            If blnSoloMayusculas Then
                gFunValidaCaracterAlfaNumerico = CStr(KeyChar).ToUpper : strCompara = strMayuscula
            Else
                gFunValidaCaracterAlfaNumerico = KeyChar : strCompara = strMayuscula & strMinuscula
            End If
            If Asc(KeyChar) <> Asc(vbBack) Then
                If (InStr(strCompara, gFunValidaCaracterAlfaNumerico)) <= 0 Then
                    gFunValidaCaracterAlfaNumerico = ""
                    If (Beper) Then Beep()
                End If
            End If
        Catch ex As Exception
            MsgBox("Error de validaci�n Alfanum�rico.", vbCritical, My.Application.Info.Title & " : gFunValidaCaracterAlfaNumerico")
        End Try
    End Function

    Public Function gFunValidaValorControl(
                    ByVal strMemsajeError As String,
                    ByVal objetoControl As Object,
                    ByRef StrMensajeResumen As String,
                    ByRef SeteaFoco As Boolean,
                    Optional ByVal QueTengaValor As Boolean = True,
                    Optional ByRef DockingTabControl As C1.Win.C1Command.C1DockingTab = Nothing,
                    Optional ByVal DockingTabSelectedIndex As Integer = -1,
                    Optional ByVal MuestraError As Boolean = True) As Boolean
        '...Valida que un control Tenga o NO Tenga un valor, si hay error posiciona el "Focus" en el control.
        '   Si el control est� en un control "DockingTab", cambia el SelectedIndex del tabulador, pasado
        '   a trav�s del par�metro "DockingTabSelectedIndex"
        Try
            gFunValidaValorControl = False
            '...TextBox, C1.Win.C1Input.C1TextBox y C1.Win.C1Input.C1DateEdit
            If (TypeOf objetoControl Is TextBox) Or
               (TypeOf objetoControl Is C1.Win.C1Input.C1TextBox) Or
               (TypeOf objetoControl Is C1.Win.C1Input.C1DateEdit) Then
                If (objetoControl.Text.Trim = "" And QueTengaValor) Or
                   (objetoControl.text.ToString.Replace("0", "").Trim = objetoControl.nulltext.ToString.Replace("0", "").Trim And QueTengaValor And objetoControl.numericinput) Or
                   (objetoControl.value.ToString.Replace("0", "").Trim = objetoControl.nulltext.ToString.Replace("0", "").Trim And QueTengaValor And objetoControl.numericinput) Or
                   (objetoControl.Text.Trim <> "" And Not QueTengaValor) Then
                    If MuestraError Then
                        MsgBox(strMemsajeError, vbCritical, My.Application.Info.Title)
                    Else
                        StrMensajeResumen = StrMensajeResumen & strMemsajeError & vbCr
                    End If
                    If SeteaFoco Then
                        If (Not DockingTabSelectedIndex = -1) Then DockingTabControl.SelectedIndex = DockingTabSelectedIndex
                        objetoControl.Focus()
                        SeteaFoco = False
                    End If
                    Exit Function
                End If
            Else
                '...C1.Win.C1List.C1Combo
                If (TypeOf objetoControl Is C1.Win.C1List.C1Combo) Then
                    If (objetoControl.Text.Trim = "" And QueTengaValor) Or
                       (objetoControl.Text.Trim <> "" And Not QueTengaValor) Then
                        If MuestraError Then
                            MsgBox(strMemsajeError, vbCritical, My.Application.Info.Title)
                        Else
                            StrMensajeResumen = StrMensajeResumen & strMemsajeError & vbCr
                        End If
                        If SeteaFoco Then
                            If (Not DockingTabSelectedIndex = -1) Then DockingTabControl.SelectedIndex = DockingTabSelectedIndex
                            objetoControl.Focus()
                            SeteaFoco = False
                        End If
                        Exit Function
                    End If
                End If
            End If
            gFunValidaValorControl = True
        Catch ex As Exception
            MsgBox("Error de validaci�n del Control:" & vbCr & objetoControl.name, vbCritical, My.Application.Info.Title & " : GFun_ValidaValorControl")
            gFunValidaValorControl = False
        End Try
    End Function

    Public Sub gSubHabilitaControl(
                    ByRef objetoControl As Object,
                    ByVal blnReadOnly As Boolean,
                    ByVal LColorHabilitado As Color,
                    ByVal LColorDeshabilitado As Color,
                    Optional ByVal blnSetColorControl As Boolean = True)
        Try
            If (TypeOf objetoControl Is Label) Then Exit Sub
            '...Color
            If (blnReadOnly) And (blnSetColorControl) Then
                If (TypeOf objetoControl Is C1.Win.C1List.C1Combo) Then
                    objetoControl.EditorBackColor = gColorBackGroundDesabled
                Else
                    If (TypeOf objetoControl Is System.Windows.Forms.RadioButton) Or
                       (TypeOf objetoControl Is System.Windows.Forms.CheckBox) Then
                        objetoControl.BackColor = Color.Transparent
                    Else
                        If Not (TypeOf objetoControl Is System.Windows.Forms.Panel) Then
                            objetoControl.BackColor = LColorDeshabilitado
                        End If
                    End If
                End If
            Else
                If (TypeOf objetoControl Is C1.Win.C1List.C1Combo) Then
                    objetoControl.EditorBackColor = gColorBackGroundEnabled
                Else
                    If (TypeOf objetoControl Is System.Windows.Forms.RadioButton) Or
                       (TypeOf objetoControl Is System.Windows.Forms.CheckBox) Then
                        objetoControl.BackColor = Color.Transparent
                    Else
                        If Not (TypeOf objetoControl Is System.Windows.Forms.Panel) Then
                            objetoControl.BackColor = LColorHabilitado
                        End If
                    End If
                End If
            End If
            '...Enable / TabStop
            If (TypeOf objetoControl Is C1.Win.C1Input.C1TextBox) Or
               (TypeOf objetoControl Is C1.Win.C1Input.C1DateEdit) Or
               (TypeOf objetoControl Is C1.Win.C1List.C1Combo) Then
                objetoControl.ReadOnly = blnReadOnly
                objetoControl.TabStop = (Not blnReadOnly)
            Else
                If (TypeOf objetoControl Is System.Windows.Forms.RadioButton) Or
                   (TypeOf objetoControl Is System.Windows.Forms.CheckBox) Or
                   (TypeOf objetoControl Is System.Windows.Forms.Button) Or
                   (TypeOf objetoControl Is System.Windows.Forms.Panel) Then
                    objetoControl.Enabled = (Not blnReadOnly)
                    objetoControl.TabStop = (Not blnReadOnly)
                Else
                    objetoControl.Locked = blnReadOnly
                    objetoControl.TabStop = (Not blnReadOnly)
                End If
            End If
        Catch ex As Exception
            MsgBox("Error al Habilitar Control :" & vbCr & objetoControl.name, vbCritical, My.Application.Info.Title & " : gSubHabilitaControl")
        End Try
    End Sub

    Public Function gFunSeteaItemComboBox(
                    ByVal ComboDondeBuscar As C1.Win.C1List.C1Combo,
                    ByVal QueBusca As String,
                    Optional ByVal NumeroColumnaDondeBusca As Integer = 1) As Integer

        Dim intIndex As Integer
        gFunSeteaItemComboBox = -1
        Try
            For intIndex = 0 To ComboDondeBuscar.ListCount - 1
                If ComboDondeBuscar.Columns(NumeroColumnaDondeBusca).CellValue(intIndex).ToString.Trim.ToUpper = QueBusca.ToString.Trim.ToUpper Then
                    gFunSeteaItemComboBox = intIndex
                    Exit Function
                End If
            Next
        Catch ex As Exception
            MsgBox("Error al setear indice Combo Box :" & vbCr & ComboDondeBuscar.Name, vbCritical, My.Application.Info.Title & " : gFunSeteaItemComboBox")
        End Try
    End Function

    'Public Function gFunBloqueaRegistros(ByVal LTxt_Operacion As String, ByVal LTxt_Negocio As String, ByVal LTxt_Concepto As String,
    '                                      ByVal LTxt_Clave1 As String, ByVal LTxt_Clave2 As String, ByVal LTxt_Clave3 As String,
    '                                      ByVal LTxt_Clave4 As String, ByVal LTxt_Clave5 As String, ByVal LTxt_Clave6 As String,
    '                                      ByRef PUsuario As String, ByRef PCod_Usuario As String) As String

    '    Dim ClsGeneral As ClsGeneral = New ClsGeneral

    '    Dim EstadoRegistro As String = ""
    '    'EstadoRegistro = ClsGeneral.BloqueaRegistros(LTxt_Operacion, LTxt_Negocio, LTxt_Concepto, _
    '    '                                                LTxt_Clave1, LTxt_Clave2, LTxt_Clave3, _
    '    '                                                LTxt_Clave4, LTxt_Clave5, LTxt_Clave6, _
    '    '                                                "", "", _
    '    '                                                PUsuario, PCod_Usuario)
    '    'Date.Today.ToShortDateString, Date.Today.ToShortTimeString, _
    '    Return EstadoRegistro
    'End Function

    Public Function gFunFiltroValido(ByRef FilterText As String) As Boolean
        If IsNumeric(FilterText.Trim) Then
            FilterText = " = " + FilterText
            Return True
        Else
            If FilterText.Length = 1 Then
                If FilterText.Trim.Substring(0, 1) = ">" Or
                   FilterText.Trim.Substring(0, 1) = "<" Or
                   FilterText.Trim.Substring(0, 1) = "=" Or
                   FilterText.Trim.Substring(0, 1) = "-" Then
                    FilterText = "*"
                    Return True
                Else
                    If IsNumeric(FilterText.Trim.Substring(0)) Then
                        FilterText = FilterText
                        Return True
                    ElseIf FilterText.Trim.Substring(0) = "" Then
                        FilterText = "*"
                        Return True
                    Else
                        Return False
                    End If
                End If
            End If
            If FilterText.Length = 2 Then
                If FilterText.Substring(0, 2) = "<>" Or
                   FilterText.Substring(0, 2) = "<=" Or
                   FilterText.Substring(0, 2) = ">=" Or
                   FilterText.Substring(0, 2) = "=<" Or
                   FilterText.Substring(0, 2) = "=>" Or
                   FilterText.Substring(0, 2) = ">-" Or
                   FilterText.Substring(0, 2) = "<-" Or
                   FilterText.Substring(0, 2) = "=-" Then

                    FilterText = "*"
                    Return True
                Else
                    If IsNumeric(FilterText.Trim.Substring(1)) Then
                        FilterText = FilterText
                        Return True
                    ElseIf FilterText.Trim.Substring(1) = "" Then
                        FilterText = "*"
                        Return True
                    Else
                        Return False
                    End If
                End If
            End If
            If FilterText.Length > 2 Then
                If FilterText.Substring(0, 3) = "<>-" Or
                   FilterText.Substring(0, 3) = "<=-" Or
                   FilterText.Substring(0, 3) = ">=-" Or
                   FilterText.Substring(0, 3) = "=<-" Or
                   FilterText.Substring(0, 3) = "=>-" Then
                    FilterText = "*"
                    Return True
                Else
                    If IsNumeric(FilterText.Trim.Substring(2)) Then
                        FilterText = FilterText
                        Return True
                    ElseIf FilterText.Trim.Substring(2) = "" Then
                        FilterText = "*"
                        Return True
                    Else
                        Return False
                    End If
                End If
            End If
        End If
    End Function

    Public Sub gSubGuardarOrdenGrilla(ByRef Orden As SortOrder, ByRef Columna As Integer, ByVal Grilla As C1.Win.C1TrueDBGrid.C1TrueDBGrid)
        Try
            Dim Col As C1.Win.C1TrueDBGrid.C1DataColumn
            Dim Index As Integer = 0

            If Grilla.RowCount > 0 Then
                For Index = 0 To Grilla.Columns.Count - 1
                    Col = Grilla.Columns(Index)
                    If Col.SortDirection <> SortOrder.None Then
                        Columna = Index
                        Orden = Col.SortDirection
                        Exit For
                    End If
                Next
            End If
        Catch ex As Exception
            Exit Sub
        End Try
    End Sub

    Public Sub gSubRecuperarOrdenGrilla(ByVal Orden As SortOrder, ByVal Columna As Integer, ByVal Grilla As C1.Win.C1TrueDBGrid.C1TrueDBGrid, ByRef DatasetGrilla As DataSet)
        Try
            gSubRecuperarOrdenGrilla(Orden, Columna, Grilla, DatasetGrilla.Tables(0))
        Catch ex As Exception
            Exit Sub
        End Try
    End Sub

    Public Sub gSubRecuperarOrdenGrilla(ByVal Orden As SortOrder, ByVal Columna As Integer, ByVal Grilla As C1.Win.C1TrueDBGrid.C1TrueDBGrid, ByRef DataTableGrilla As DataTable)
        Try
            Dim sortCondition As String = Grilla.Splits(0).DisplayColumns(Columna).DataColumn.DataField + " "
            sortCondition += IIf(Orden = SortOrder.Descending, "DESC", "")

            DataTableGrilla.DefaultView.Sort = sortCondition
            Grilla.Columns(Columna).SortDirection = Orden
        Catch ex As Exception
            Exit Sub
        End Try
    End Sub


#End Region '....

#Region " Funciones Alexis Gonzalez "

    Public Function GfunValidar_Email(ByVal Email As String, ByRef MensajeError As String) As Boolean
        'Dim MensajeError As String
        Dim strTmp As String
        Dim n As Long
        Dim sEXT As String
        GfunValidar_Email = True
        sEXT = Email
        Do While InStr(1, sEXT, ".") <> 0
            sEXT = Microsoft.VisualBasic.Right(sEXT, Len(sEXT) - InStr(1, sEXT, "."))
        Loop
        If Email = "" Then
            GfunValidar_Email = False
            MensajeError = MensajeError & "No se indic� ninguna direcci�n de " & "mail para verificar!" & vbNewLine
        ElseIf InStr(1, Email, "@") = 0 Then
            GfunValidar_Email = False
            MensajeError = MensajeError & "La direcci�n de email debe contener el signo @" & vbNewLine
        ElseIf InStr(1, Email, "@") = 1 Then
            GfunValidar_Email = False
            MensajeError = MensajeError & "El @ No puede estar al principio" & vbNewLine
        ElseIf InStr(1, Email, "@") = Len(Email) Then
            GfunValidar_Email = False
            MensajeError = MensajeError & "El @ no puede estar al final de la direcci�n" & vbNewLine
        ElseIf EXTisOK(sEXT) = False Or sEXT = "" Then
            GfunValidar_Email = False
            MensajeError = MensajeError & "LA direcci�n no tiene un dominio v�lido, "
            MensajeError = MensajeError & "por ejemplo : "
            MensajeError = MensajeError & ".com, .net, .gov, .org, .edu, .biz, .tv etc.. " & vbNewLine
        ElseIf Len(Email) < 6 Then
            GfunValidar_Email = False
            MensajeError = MensajeError & "La direcci�n no puede ser menor a 6 caracteres." & vbNewLine
        End If
        strTmp = Email
        Do While InStr(1, strTmp, "@") <> 0
            n = 1
            strTmp = Microsoft.VisualBasic.Right(strTmp, Len(strTmp) - InStr(1, strTmp, "@"))
        Loop
        If n > 1 Then
            GfunValidar_Email = False
            MensajeError = MensajeError & "Solo puede haber un @ en la direcci�n de email" & vbNewLine
        End If
        Dim pos As Integer
        pos = InStr(1, Email, "@")
        If Mid(Email, pos + 1, 1) = "." Then
            GfunValidar_Email = False
            MensajeError = MensajeError & "El punto no puede estar seguido del @" & vbNewLine
        End If
        'If MensajeError <> "" Then
        'MsgBox(MensajeError, vbCritical)
        'End If
    End Function

    Public Function EXTisOK(ByVal sEXT As String) As Boolean
        Dim EXT As String = "", X As Long = 0
        EXTisOK = False
        If Microsoft.VisualBasic.Left(sEXT, 1) <> "." Then sEXT = "." & sEXT
        sEXT = UCase(sEXT) 'just to avoid errors
        EXT = EXT & ".COM.EDU.GOV.NET.BIZ.ORG.TV"
        EXT = EXT & ".AF.AL.DZ.As.AD.AO.AI.AQ.AG.AP.AR.AM.AW.AU.AT.AZ.BS.BH.BD.BB.BY"
        EXT = EXT & ".BE.BZ.BJ.BM.BT.BO.BA.BW.BV.BR.IO.BN.BG.BF.MM.BI.KH.CM.CA.CV.KY"
        EXT = EXT & ".CF.TD.CL.CN.CX.CC.CO.KM.CG.CD.CK.CR.CI.HR.CU.CY.CZ.DK.DJ.DM.DO"
        EXT = EXT & ".TP.EC.EG.SV.GQ.ER.EE.ET.FK.FO.FJ.FI.CS.SU.FR.FX.GF.PF.TF.GA.GM.GE.DE"
        EXT = EXT & ".GH.GI.GB.GR.GL.GD.GP.GU.GT.GN.GW.GY.HT.HM.HN.HK.HU.IS.IN.ID.IR.IQ"
        EXT = EXT & ".IE.IL.IT.JM.JP.JO.KZ.KE.KI.KW.KG.LA.LV.LB.LS.LR.LY.LI.LT.LU.MO.MK.MG"
        EXT = EXT & ".MW.MY.MV.ML.MT.MH.MQ.MR.MU.YT.MX.FM.MD.MC.MN.MS.MA.MZ.NA"
        EXT = EXT & ".NR.NP.NL.AN.NT.NC.NZ.NI.NE.NG.NU.NF.KP.MP.NO.OM.PK.PW.PA.PG.PY"
        EXT = EXT & ".PE.PH.PN.PL.PT.PR.QA.RE.RO.RU.RW.GS.SH.KN.LC.PM.ST.VC.SM.SA.SN.SC"
        EXT = EXT & ".SL.SG.SK.SI.SB.SO.ZA.KR.ES.LK.SD.SR.SJ.SZ.SE.CH.SY.TJ.TW.TZ.TH.TG.TK"
        EXT = EXT & ".TO.TT.TN.TR.TM.TC.TV.UG.UA.AE.UK.US.UY.UM.UZ.VU.VA.VE.VN.VG.VI"
        EXT = EXT & ".WF.WS.EH.YE.YU.ZR.ZM.ZW"
        EXT = UCase(EXT) 'just to avoid errors
        If InStr(1, EXT, sEXT, 0) <> 0 Then
            EXTisOK = True
        End If
    End Function

    Public Function EntrgaRutDigito(ByVal Rut As Long) As String
        Dim Digito As Integer
        Dim Contador As Integer
        Dim Multiplo As Integer
        Dim Acumulador As Integer

        Contador = 2
        Acumulador = 0
        While Rut <> 0
            Multiplo = (Rut Mod 10) * Contador
            Acumulador = Acumulador + Multiplo
            Rut = Rut \ 10
            Contador = Contador + 1
            If Contador = 8 Then
                Contador = 2
            End If
        End While
        Digito = 11 - (Acumulador Mod 11)
        EntrgaRutDigito = CStr(Digito)
        If Digito = 10 Then EntrgaRutDigito = "K"
        If Digito = 11 Then EntrgaRutDigito = "0"
    End Function

    Public Function ValidaRut(ByVal Rut As String, ByRef StrMensajeResumen As String, ByVal strMemsajeError As String) As Boolean
        Dim i As Integer, L As Integer, Valor As Integer, Suma As Integer, Resultado As Integer
        Dim Resto As String

        ValidaRut = False

        If Not Trim(Rut) = "" Then
            If InStr(Rut, "-") > 0 Then
                Rut = Mid(Rut, 1, InStr(Rut, "-") - 1) & Mid(Rut, InStr(Rut, "-") + 1, 1)
            End If

            If Not IsNumeric(Mid(Rut, Len(Rut), 1)) Then
                Rut = Mid(Rut, 1, Len(Rut) - 1) & UCase(Mid(Rut, Len(Rut), 1))
            End If

            Valor = 2
            L = Len(Rut) - 1
            For i = L To 1 Step -1
                Suma = Suma + Val(Mid(Rut, i, 1)) * Valor
                Valor = Valor + 1
                If Valor > 7 Then
                    Valor = 2
                End If
            Next i

            Resultado = Suma Mod 11
            Resto = 11 - Resultado
            If Resto = 11 Then
                Resto = "0"
            End If
            If Resto = 10 Then
                Resto = "K"
            End If

            If Not UCase(Resto) = UCase(Mid(Rut, Len(Rut), 1)) Then
                StrMensajeResumen = StrMensajeResumen & strMemsajeError & vbCr
                ValidaRut = False
            Else
                ValidaRut = True
            End If
        Else
            ValidaRut = True
        End If
    End Function

    Public Sub gSub_BuscarMonedas(ByVal strCodigo As String,
                                  ByVal strColumnas As String,
                                  ByRef strRetorno As String,
                                  ByRef DS_Moneda As DataSet,
                                  Optional ByVal strEstado As String = "TOD",
                                  Optional ByVal strEsMonedaPago As String = "",
                                  Optional ByVal strEsDual As String = "D",
                                  Optional ByVal strMonedaIndice As String = "M",
                                  Optional ByVal strEsMonedaPagoConUf As String = "NO")
        Dim objMoneda As ClsMoneda = New ClsMoneda
        DS_Moneda = objMoneda.Moneda_Ver(strCodigo:=strCodigo,
                                         strColumnas:=strColumnas,
                                         strEstado:=strEstado,
                                         strRetorno:=strRetorno,
                                         strEsMonedaPago:=strEsMonedaPago,
                                         strEsDual:=strEsDual,
                                         strMonedaIndice:=strMonedaIndice,
                                         strEsMonedaPagoConUf:=strEsMonedaPagoConUf)
    End Sub
    Public Function gSub_BuscarSimboloMoneda(ByVal strCodigo As String) As String
        Dim objMoneda As ClsMoneda = New ClsMoneda
        Dim strRetorno As String = "OK"
        Dim DS_Moneda As DataSet
        DS_Moneda = objMoneda.Moneda_Ver(strCodigo:=strCodigo,
                                         strColumnas:="MONEDA_SIMBOLO",
                                         strEstado:="TOD",
                                         strRetorno:=strRetorno,
                                         strEsMonedaPago:="",
                                         strEsDual:="D",
                                         strMonedaIndice:="M",
                                         strEsMonedaPagoConUf:="NO")
        If strRetorno = "OK" Then
            If DS_Moneda.Tables.Count > 0 Then
                gSub_BuscarSimboloMoneda = DS_Moneda.Tables(0).Rows(0)("MONEDA_SIMBOLO").ToString
            Else
                gSub_BuscarSimboloMoneda = "$"
            End If
        Else
            gSub_BuscarSimboloMoneda = "$"
        End If
    End Function

#End Region '....


#Region " Funciones Fabi�n Bonilla "

    Public Sub CargaComboMonedas(ByRef meComboBox As C1.Win.C1List.C1Combo,
                                Optional ByVal strTransaccion As String = "",
                                Optional ByVal strCodigoMoneda As String = "",
                                Optional ByVal strEstadoMoneda As String = "",
                                Optional ByVal bIfBlank As Boolean = True,
                                Optional ByVal strEsMonedaPago As String = "",
                                Optional ByVal strEsDual As String = "D",
                                Optional ByVal strMonedaIndice As String = "M",
                                Optional ByVal strSoloMonedaMX As String = "NO",
                                Optional ByVal strFlgConUF As String = "NO")
        Dim lobjMoneda As ClsMoneda = New ClsMoneda
        Dim DS_General As New DataSet
        Dim MonedaTemp As DataRow
        Dim ltxtColumnas As String
        Dim ltxtResultadoTransaccion As String = Nothing

        Try
            ltxtColumnas = "MONEDA_CODIGO,MONEDA_DESCRIPCION,MONEDA_DECIMALES"
            '...Limpia Combo
            meComboBox.ClearItems()
            meComboBox.Text = ""
            '...Inicializa Data Set Combo
            DS_General = Nothing
            DS_General = lobjMoneda.Moneda_Ver(strCodigoMoneda, ltxtColumnas, strEstadoMoneda, ltxtResultadoTransaccion, strEsMonedaPago, strEsDual, strMonedaIndice, strSoloMonedaMX, strFlgConUF)

            If ltxtResultadoTransaccion.ToUpper = "OK" Then
                '+ Si se indico que acepta blanco se incluye un registro en blanco en el combo
                If bIfBlank Then
                    MonedaTemp = DS_General.Tables("Moneda").NewRow()
                    MonedaTemp("MONEDA_DESCRIPCION") = ""
                    MonedaTemp("MONEDA_CODIGO") = ""
                    MonedaTemp("MONEDA_DECIMALES") = "0"
                    DS_General.Tables("Moneda").Rows.Add(MonedaTemp)
                    DS_General.AcceptChanges()
                End If

                '...Asigna Data Source
                meComboBox.AddItem(";")
                meComboBox.DataSource = DS_General.Tables("Moneda")
                meComboBox.Sort(0, C1.Win.C1List.SortDirEnum.ASC)
                '...Ordena y visibilidad de columnas del combo
                meComboBox.Columns(0).DataField = "MONEDA_DESCRIPCION"
                meComboBox.Splits(0).DisplayColumns(0).Visible = True

                meComboBox.Columns(1).DataField = "MONEDA_CODIGO"
                meComboBox.Splits(0).DisplayColumns(1).Visible = False

                meComboBox.Columns(2).DataField = "MONEDA_DECIMALES"
                meComboBox.Splits(0).DisplayColumns(2).Visible = False
            End If
        Catch ex As Exception
            ltxtResultadoTransaccion = ex.Message
        Finally
            If ltxtResultadoTransaccion <> "OK" Then
                MsgBox("Error al cargar combo monedas : " & ltxtResultadoTransaccion, MsgBoxStyle.Information, gtxtNombreSistema & strTransaccion)
            End If
        End Try
    End Sub



    'Public Sub RecuperaAccesoDeControles(ByVal lint_NumeroDePantalla As Integer, ByRef larr_AccesoDeControles(,) As String)
    '    Dim objAccesoDeControles As ClsGeneral = New ClsGeneral
    '    Try
    '        If Not (objAccesoDeControles.TraerControles(lint_NumeroDePantalla, larr_AccesoDeControles)) Then
    '            Exit Sub
    '        End If
    '    Catch ex As Exception
    '        larr_AccesoDeControles = Nothing
    '        objAccesoDeControles = Nothing
    '    End Try
    'End Sub

    Public Sub Accion_NoPermitida(ByVal lstrMensajeAccionNoPermitida As String, Optional ByVal lstrAccion As String = "ejecutar", Optional ByVal lstrTransaccion As String = "")
        If lstrMensajeAccionNoPermitida.Trim = "" Then
            lstrMensajeAccionNoPermitida = "Ud. no est� autorizado para realizar esta acci�n."
        Else
            lstrMensajeAccionNoPermitida = "Ud. no est� autorizado para " & lstrAccion & " " & lstrMensajeAccionNoPermitida.Replace("&", "") & "."
        End If
        MsgBox(lstrMensajeAccionNoPermitida, MsgBoxStyle.Information + MsgBoxStyle.OkOnly, gtxtNombreSistema & lstrTransaccion)
    End Sub

    Public Function gFun_TipoEstadoDesc(ByVal strCodigoTipo As String, ByVal strCodigoEstado As String, ByVal strDefault As String, ByVal strTransaccion As String) As String
        Dim DS_Estado As DataSet
        Dim lobjEstados As ClsEstados = New ClsEstados
        Dim lstrResultadoTransaccion As String = ""
        Dim lstrMsg As String = ""

        DS_Estado = lobjEstados.Estados_Ver(strCodigoEstado, strCodigoTipo, strDefault, "", lstrResultadoTransaccion)
        Try
            If lstrResultadoTransaccion.ToUpper <> "OK" Then
                lstrMsg = lstrResultadoTransaccion
                MsgBox("Error al obtener la descripci�n del Estados : " & strCodigoEstado, MsgBoxStyle.Information, gtxtNombreSistema)
            Else

            End If
        Catch ex As Exception
            lstrMsg = ex.Message
        Finally
            gFun_TipoEstadoDesc = DS_Estado.Tables(0).Rows(0)("DSC_ESTADO")
            If lstrMsg <> "" Then
                MsgBox("Error al obtenr la descripci�n del estado: " & strCodigoEstado & vbCr & "[" & lstrMsg & "]", MsgBoxStyle.Information, gtxtNombreSistema & strTransaccion)
            End If
        End Try
    End Function

    Public Function gFun_ResultadoMsg(ByVal strMsg As String, ByVal strTransaccion As String, Optional ByVal blnSoloValidar As Boolean = False, Optional ByVal strSepColumna As String = "|") As Boolean
        Dim arr_Columnas() As String
        Dim lblnSimple As Boolean
        Dim lstrMensaje As String
        Dim lstrTipoError As String
        Dim msrResultadoMensaje As MsgBoxResult

        If strMsg = "" Then
            gFun_ResultadoMsg = True
            Exit Function
        End If

        '+ Se verifica si el mensaje viene con m�s de una columna; es decir, es compuesto no simple
        lblnSimple = IIf(InStr(strMsg, strSepColumna) > 0, False, True)

        If lblnSimple Then
            lstrMensaje = "OK"
            If strMsg = "OK" Then
                lstrTipoError = "0"
            Else
                lstrTipoError = "1"
                lstrMensaje = strMsg
            End If
        Else
            If Mid(strMsg, 1, 1) <> strSepColumna Then
                strMsg = strSepColumna & strMsg
            End If
            arr_Columnas = strMsg.Split(strSepColumna)
            lstrTipoError = arr_Columnas(1)
            lstrMensaje = arr_Columnas(2)
        End If

        '+ Si el tipo de error es: 1)Error
        If lstrTipoError = "1" Then
            gFun_ResultadoMsg = False
        Else
            gFun_ResultadoMsg = True
        End If

        '+ Si solamente se utiliza para validar y no mostrar el mensaje
        If Not blnSoloValidar Then
            '+ Si hay que desplegar un mensaje
            If lstrTipoError <> "0" Then
                Select Case lstrTipoError
                    Case "1"
                        '+ Mensaje de Error
                        MsgBox(lstrMensaje, MsgBoxStyle.Critical, gtxtNombreSistema & strTransaccion)
                    Case "2"
                        '+ Mensaje de Advertencia
                        MsgBox(lstrMensaje, MsgBoxStyle.Exclamation, gtxtNombreSistema & strTransaccion)
                    Case "3"
                        '+ Mensaje de Informaci�n
                        MsgBox(lstrMensaje, MsgBoxStyle.Information, gtxtNombreSistema & strTransaccion)
                    Case "4"
                        '+ Mensaje de Pregunta
                        MsgBox(lstrMensaje, MsgBoxStyle.Question, gtxtNombreSistema & strTransaccion)
                    Case "5"
                        '+ Mensaje de Advertencia con pregunta con el segundo bot�n habilitado (bot�n del no).
                        msrResultadoMensaje = MsgBox(lstrMensaje, MsgBoxStyle.DefaultButton2 + MsgBoxStyle.YesNo + MsgBoxStyle.Exclamation, gtxtNombreSistema & strTransaccion)
                        If msrResultadoMensaje = MsgBoxResult.No Then
                            gFun_ResultadoMsg = False
                        End If
                    Case "6"
                        '+ Mensaje de Advertencia con pregunta con el primer bot�n habilitado (bot�n del si).
                        msrResultadoMensaje = MsgBox(lstrMensaje, MsgBoxStyle.DefaultButton1 + MsgBoxStyle.YesNo + MsgBoxStyle.Exclamation, gtxtNombreSistema & strTransaccion)
                        If msrResultadoMensaje = MsgBoxResult.No Then
                            gFun_ResultadoMsg = False
                        End If
                    Case Else
                        MsgBox(lstrMensaje, , gtxtNombreSistema & strTransaccion)
                End Select
            End If
        End If
    End Function

    Public Sub gSubCargaComboMonedaPermitidas(
            ByRef objetoControl As C1.Win.C1List.C1Combo)
        Try
            Dim ldtbEstados As New DataTable
            Dim ldrNewRowEstado As DataRow

            With ldtbEstados
                .Columns.Clear()
                .Rows.Clear()
                .Columns.Add("MONEDA_DESCRIPCION", GetType(String))
                .Columns.Add("MONEDA_CODIGO", GetType(String))

                ldrNewRowEstado = .NewRow
                ldrNewRowEstado("MONEDA_DESCRIPCION") = "PESO"
                ldrNewRowEstado("MONEDA_CODIGO") = "CLP"
                Call .Rows.Add(ldrNewRowEstado)

                ldrNewRowEstado = .NewRow
                ldrNewRowEstado("MONEDA_DESCRIPCION") = "DOLAR"
                ldrNewRowEstado("MONEDA_CODIGO") = "USD"
                Call .Rows.Add(ldrNewRowEstado)
            End With

            With objetoControl
                .DataSource = ldtbEstados
                .Columns(0).DataField = "MONEDA_DESCRIPCION"
                .Columns(1).DataField = "MONEDA_CODIGO"
                .Splits(0).DisplayColumns(1).Visible = False
                .Text = ""
                .Sort(0, C1.Win.C1List.SortDirEnum.ASC)
                .SelectedIndex = -1
            End With
        Catch ex As Exception

        End Try
    End Sub

    Public Sub CargaComboPaises(ByRef meComboBox As C1.Win.C1List.C1Combo, Optional ByVal strTransaccion As String = "", Optional ByVal strCodigoPais As String = "", Optional ByVal bIfBlank As Boolean = True)
        Dim lobjPais As ClsPais = New ClsPais
        Dim DS_General As New DataSet
        Dim DataTemp As DataRow
        Dim ltxtColumnas As String
        Dim ltxtResultadoTransaccion As String = Nothing

        Try
            ltxtColumnas = "CODIGO_PAIS,DESCRIPCION_PAIS"
            '...Limpia Combo
            meComboBox.ClearItems()
            meComboBox.Text = ""
            '...Inicializa Data Set Combo
            DS_General = Nothing
            DS_General = lobjPais.Pais_Ver(strCodigoPais, ltxtColumnas, ltxtResultadoTransaccion)

            If ltxtResultadoTransaccion.ToUpper = "OK" Then
                '+ Si se indico que acepta blanco se incluye un registro en blanco en el combo
                If bIfBlank Then
                    DataTemp = DS_General.Tables("PAIS").NewRow()
                    DataTemp("DESCRIPCION_PAIS") = ""
                    DataTemp("CODIGO_PAIS") = ""
                    DS_General.Tables("PAIS").Rows.Add(DataTemp)
                    DS_General.AcceptChanges()
                End If

                '...Asigna Data Source
                meComboBox.AddItem(";")
                meComboBox.DataSource = DS_General.Tables("PAIS")
                meComboBox.Sort(0, C1.Win.C1List.SortDirEnum.ASC)
                '...Ordena y visibilidad de columnas del combo
                meComboBox.Columns(0).DataField = "DESCRIPCION_PAIS"
                meComboBox.Splits(0).DisplayColumns(0).Visible = True

                meComboBox.Columns(1).DataField = "CODIGO_PAIS"
                meComboBox.Splits(0).DisplayColumns(1).Visible = False
            End If
        Catch ex As Exception
            ltxtResultadoTransaccion = ex.Message
        Finally
            If ltxtResultadoTransaccion <> "OK" Then
                MsgBox("Error al cargar combo pa�ses : " & ltxtResultadoTransaccion, MsgBoxStyle.Information, gtxtNombreSistema & strTransaccion)
            End If
        End Try
    End Sub

    Public Function gSubstrSoloNumerosLetras(ByVal pstrCadena As String) As String
        Dim strValor As String = ""
        Dim strCaracteres = " ABCDEFGHIJKLMN�OPQRSTUVWXYZ1234567890"
        Dim strCaracter As String

        Try
            For lIndex = 1 To pstrCadena.Length
                strCaracter = Mid(pstrCadena, lIndex, 1)
                If InStr(strCaracteres, strCaracter.ToUpper) > 0 Then
                    strValor = strValor & strCaracter
                End If
            Next
        Catch ex As Exception
            MsgBox("Error al obtener la cadena de s�lo numeros y letras.", vbCritical, My.Application.Info.Title & " : gSubstrSoloNumerosLetras")
        End Try
        gSubstrSoloNumerosLetras = strValor
    End Function

    Public Function gGetSKeyUser(Optional ByVal plngUsuario As Long = 0, Optional ByVal pintModo As Integer = 0) As String
        Dim strValor As String = ""
        Dim strFecha As String = Mid(Now.ToString, 7, 4) & Mid(Now, 4, 2) & Mid(Now, 1, 2)
        Dim strHora As String = Mid(String.Format("{0:T}", TimeString), 1, 2) & Mid(String.Format("{0:T}", TimeString), 4, 2) & Mid(String.Format("{0:T}", TimeString), 7, 2)

        'Mid(ldrRegistro("Fecha Proceso"), 4, 2) & Mid(ldrRegistro("Fecha Proceso"), 1, 2) & Mid(String.Format("{0:T}", TimeString), 1, 2) & Mid(String.Format("{0:T}", TimeString), 4, 2)
        Try
            Select Case pintModo
                '+ Skey con formato: <id_usuario>_<yyyymmdd_hhmiss>. Primer valor c�digo de usuario y segundo valor fecha y hora.
                Case 0
                    strValor = plngUsuario.ToString & "_" & strFecha & "_" & strHora
                    '+ Skey con formato: <id_usuario>_<hhmiss>. Primer valor c�digo de usuario y segundo valor s�lo la hora.
                Case 1
                    strValor = plngUsuario.ToString & "_" & strHora
                Case Else

            End Select

            '+ Se rellenan con ceros a la izquierda hasta completar los 25 caracteres.
            strValor = strValor.PadLeft(25, "0")
        Catch ex As Exception
            gFun_ResultadoMsg("1|Error al obtener el sKey.", My.Application.Info.Title)
        End Try
        gGetSKeyUser = strValor
    End Function

    Public Function getValorFormatoXLS(ByVal pstrFormato As String,
                                       ByVal pTipoFormato As Est_Formato_XLS) As String
        Dim arrValores() As String
        Dim lstrValor As String

        If Mid(pstrFormato, 1, 1) = "|" Then
            pstrFormato = Mid(pstrFormato, 2, Len(pstrFormato))
        End If

        If Mid(pstrFormato, Len(pstrFormato), 1) = "|" Then
            pstrFormato = Mid(pstrFormato, 1, Len(pstrFormato) - 1)
        End If

        arrValores = pstrFormato.Split("|")

        lstrValor = arrValores(pTipoFormato)
        If Not IsNumeric(lstrValor) Then
            Select Case lstrValor
                Case "T"
                    lstrValor = "True"
                Case "F"
                    lstrValor = "False"
                Case Else
                    lstrValor = ""
            End Select
        End If
        getValorFormatoXLS = lstrValor
    End Function

    Public Function gEntregaLetraXLS(ByVal pNumero As Long) As String
        Dim lstrColLetrasCE As String = "A B C D E F G H I J K L M N O P Q R S T U V W X Y Z"
        Dim lstrColLetrasSE As String = lstrColLetrasCE.Replace(" ", "")
        Dim lstrColLetras() As String = lstrColLetrasCE.Split(" ")
        Dim llngRes As Long = pNumero \ Len(lstrColLetrasSE)
        Dim llngMod As Long = (pNumero Mod Len(lstrColLetrasSE))

        gEntregaLetraXLS = ""
        If llngRes = 0 Then
            gEntregaLetraXLS = lstrColLetras(llngMod - 1)
        Else
            If llngMod = 0 Then
                If llngRes > 1 Then
                    gEntregaLetraXLS = lstrColLetras(llngRes - 2)
                End If
                gEntregaLetraXLS = gEntregaLetraXLS & lstrColLetras(Len(lstrColLetrasSE) - 1)
            Else
                gEntregaLetraXLS = lstrColLetras(llngRes - 1)
                gEntregaLetraXLS = gEntregaLetraXLS & lstrColLetras(llngMod - 1)
            End If
        End If
    End Function

    Public Function getValorFormatoArr(ByVal pstrFormato As String,
                                       ByVal pTipoFormato As Est_Formato_XLS,
                                       Optional ByVal pSep As String = "|") As String
        Dim arrValores() As String
        Dim lstrValor As String

        If Mid(pstrFormato, 1, 1) = pSep Then
            pstrFormato = Mid(pstrFormato, 2, Len(pstrFormato))
        End If

        If Mid(pstrFormato, Len(pstrFormato), 1) = pSep Then
            pstrFormato = Mid(pstrFormato, 1, Len(pstrFormato) - 1)
        End If

        arrValores = pstrFormato.Split(pSep)

        lstrValor = arrValores(pTipoFormato)
        If Not IsNumeric(lstrValor) Then
            Select Case lstrValor
                Case "T"
                    lstrValor = "True"
                Case "F"
                    lstrValor = "False"
                Case Else
                    lstrValor = ""
            End Select
        End If
        getValorFormatoArr = lstrValor
    End Function

    Public Function getNumberFormat(ByVal pLargo As Integer,
                                    ByVal pDecimales As Integer,
                                    Optional ByVal pFormatSimbol As Boolean = True) As String
        Dim lstrFormat As String = ""
        Dim lintParteEntera As Integer = 0
        '"###,###,###,###,##0.0000"
        If pLargo > 0 Then
            If Not pFormatSimbol Then
                lstrFormat = StrDup(pLargo, "#")
            Else
                '+ Nota: La parte decimal est� contenida en el Largo
                lintParteEntera = pLargo - pDecimales

                For lintPos = 1 To lintParteEntera
                    lstrFormat = "#" & lstrFormat
                    If lintPos Mod 3 = 0 Then
                        lstrFormat = "," & lstrFormat
                    End If
                Next

                If Mid(lstrFormat, 1, 1) = "," Then
                    lstrFormat = Mid(lstrFormat, 2, Len(lstrFormat))
                End If

                lstrFormat = Mid(lstrFormat, 1, Len(lstrFormat) - 1) & "0"

                If pDecimales > 0 Then
                    lstrFormat = lstrFormat & "." & StrDup(pDecimales, "0")
                End If
            End If
        End If
        getNumberFormat = lstrFormat
    End Function

    'Objetivo: Crear campo para un DataTable pasado como par�metro.
    Public Sub CrearCampoDataTableGeneral(ByRef dtbTable As DataTable, ByVal strNombreCampo As String, ByVal strTipoCampo As System.Type)
        Dim dtcColumna As DataColumn = New DataColumn(strNombreCampo)
        dtcColumna.DataType = strTipoCampo
        dtcColumna.AllowDBNull = True
        dtbTable.Columns.Add(dtcColumna)
    End Sub

    'Objetivo: Crear un DataTable pasado como par�metro de acuerdo a una cadena que contedr� los campos a crear en formato <NumbreCampo><,><Tipo><|>. Con separador de l�nea el "|".
    Public Sub CrearDataTableGeneral(ByRef pDataTable As DataTable, ByVal pstrCampos As String, Optional ByVal pSepElemento As String = ",", Optional ByVal pSepLinea As String = "|")
        Dim lArrCampos() As String
        Dim lArrElementos() As String
        Try
            '+ Si viene el separador de l�nea al principio se elimina porque no debe estar
            If Mid(pstrCampos, 1, 1) = pSepLinea Then
                pstrCampos = Mid(pstrCampos, 2, Len(pstrCampos))
            End If

            '+ Si viene el separador de l�nea al final se elimina porque no debe estar
            If Mid(pstrCampos, Len(pstrCampos), 1) = pSepLinea Then
                pstrCampos = Mid(pstrCampos, 1, Len(pstrCampos) - 1)
            End If

            lArrCampos = pstrCampos.Split(pSepLinea)

            pDataTable.Columns.Clear()
            For lintIndice = 0 To lArrCampos.Length - 1
                lArrElementos = lArrCampos(lintIndice).Split(pSepElemento)
                Select Case lArrElementos(1)
                    Case 0
                        '+ Num�rico
                        CrearCampoDataTableGeneral(pDataTable, lArrElementos(0), System.Type.GetType("System.Double"))
                    Case 1
                        '+ String
                        CrearCampoDataTableGeneral(pDataTable, lArrElementos(0), System.Type.GetType("System.String"))
                    Case 2
                        '+ Date
                        CrearCampoDataTableGeneral(pDataTable, lArrElementos(0), System.Type.GetType("System.String"))
                    Case Else
                        '+ String
                        CrearCampoDataTableGeneral(pDataTable, lArrElementos(0), System.Type.GetType("System.String"))
                End Select
            Next
        Catch ex As Exception
            gFun_ResultadoMsg("1|" & ex.Message, "CrearDataTableGeneral")
        End Try
    End Sub

    Public Function AgregarEspaciosIntermedios(ByVal pCadena As String) As String
        Dim lstrCadena As String = ""

        For lIndex = 0 To Len(pCadena) - 1
            lstrCadena = lstrCadena + pCadena(lIndex).ToString + " "
        Next
        AgregarEspaciosIntermedios = lstrCadena
    End Function

    Public Function getNombreCampoValido(ByVal pDataRow As DataRow, ByVal pNombreCampo As String) As String
        Dim lstrCampoIn As String = pNombreCampo & "_IN"
        getNombreCampoValido = ""
        Try
            If Not pDataRow(lstrCampoIn) Is Nothing Then
                getNombreCampoValido = lstrCampoIn
            End If
        Catch ex As Exception
            Try
                If Not pDataRow(pNombreCampo) Is Nothing Then
                    getNombreCampoValido = pNombreCampo
                End If
            Catch ex1 As Exception
                getNombreCampoValido = ""
            End Try
        End Try
    End Function

#End Region

#Region "Funciones Juan Mazo"

    Public Sub gSubCargaComboEstado(
                ByRef objetoControl As C1.Win.C1List.C1Combo)
        Try
            objetoControl.ComboStyle = ComboStyleEnum.DropdownList
            objetoControl.DataMode = DataModeEnum.AddItem
            objetoControl.ClearItems()
            objetoControl.AddItem("VIGENTE;VIG")
            objetoControl.AddItem("ELIMINADO;ELI")
            objetoControl.Splits(0).DisplayColumns(0).Visible = True
            objetoControl.Splits(0).DisplayColumns(1).Visible = False
            objetoControl.SelectedIndex = -1
        Catch ex As Exception
        End Try
    End Sub

    Public Sub gSubCargaCombo_FOC(ByRef objetoControl As C1.Win.C1List.C1Combo)
        Try
            objetoControl.ComboStyle = ComboStyleEnum.DropdownList
            objetoControl.DataMode = DataModeEnum.AddItem
            objetoControl.ClearItems()
            objetoControl.AddItem("TASA;TASA")
            objetoControl.AddItem("FIFO;FIFO")
            objetoControl.AddItem("HPR;HPR")
            objetoControl.Splits(0).DisplayColumns(0).Visible = True
            objetoControl.Splits(0).DisplayColumns(1).Visible = False
            objetoControl.SelectedIndex = -1
        Catch ex As Exception
        End Try
    End Sub

    Public Sub CargaMeses(ByRef objetoControl As C1.Win.C1List.C1Combo)
        Try
            objetoControl.ComboStyle = ComboStyleEnum.DropdownList
            objetoControl.DataMode = DataModeEnum.AddItem
            objetoControl.ClearItems()
            objetoControl.AddItem("ENERO;01")
            objetoControl.AddItem("FEBRERO;02")
            objetoControl.AddItem("MARZO;03")
            objetoControl.AddItem("ABRIL;04")
            objetoControl.AddItem("MAYO;05")
            objetoControl.AddItem("JUNIO;06")
            objetoControl.AddItem("JULIO;07")
            objetoControl.AddItem("AGOSTO;08")
            objetoControl.AddItem("SEPTIEMBRE;09")
            objetoControl.AddItem("OCTUBRE;10")
            objetoControl.AddItem("NOVIEMBRE;11")
            objetoControl.AddItem("DICIEMBRE;12")
            objetoControl.Splits(0).DisplayColumns(0).Visible = True
            objetoControl.Splits(0).DisplayColumns(1).Visible = False
            objetoControl.SelectedIndex = -1
        Catch ex As Exception
        End Try
    End Sub

    Public Sub gSubCargaComboTipoBusqueda(
        ByRef objetoControl As C1.Win.C1List.C1Combo)
        Try
            objetoControl.ComboStyle = ComboStyleEnum.DropdownList
            objetoControl.DataMode = DataModeEnum.AddItem
            objetoControl.ClearItems()
            objetoControl.AddItem("CUENTAS")
            objetoControl.AddItem("CLIENTES")
            objetoControl.AddItem("GRUPOS")
            objetoControl.Splits(0).DisplayColumns(0).Visible = True
            'objetoControl.Splits(0).DisplayColumns(1).Visible = False
            objetoControl.SelectedIndex = -1
        Catch ex As Exception
        End Try
    End Sub

    ''' <summary>
    ''' Metodo creado especialmente para cargar estados para security
    ''' </summary>
    ''' <param name="objetoControl"></param>
    ''' <param name="strTipoEstado"></param>
    ''' <remarks></remarks>

    Public Sub Carga_Combos_estado_Security(ByRef objetoControl As C1.Win.C1List.C1Combo, ByVal strTipoEstado As String, Optional ByVal bIsBlank As Boolean = False)
        Dim lobjClaseEstados As ClsEstados = New ClsEstados
        Dim DS_Estados As DataSet = New DataSet
        Dim strRetorno As String = ""
        Dim EstadoTemp As DataRow

        Try
            objetoControl.ClearItems()
            DS_Estados = lobjClaseEstados.Estados_Ver("", strTipoEstado, "", "DSC_ESTADO,COD_ESTADO", strRetorno)
            If DS_Estados.Tables(0).Rows.Count > 0 Then
                '+ Si se indico que acepta blanco se incluye un registro en blanco en el combo
                If bIsBlank Then
                    EstadoTemp = DS_Estados.Tables("Estados").NewRow()
                    EstadoTemp("DSC_ESTADO") = ""
                    EstadoTemp("COD_ESTADO") = ""
                    DS_Estados.Tables("Estados").Rows.Add(EstadoTemp)
                    DS_Estados.AcceptChanges()
                End If

                objetoControl.ComboStyle = ComboStyleEnum.DropdownList
                objetoControl.DataMode = DataModeEnum.AddItem
                For i As Integer = 0 To DS_Estados.Tables(0).Rows.Count - 1
                    If DS_Estados.Tables(0).Rows(i).Item("COD_ESTADO") = "P" Or DS_Estados.Tables(0).Rows(i).Item("COD_ESTADO") = "A" Or DS_Estados.Tables(0).Rows(i).Item("COD_ESTADO") = "" Then
                        Dim lstrDescripcion As String = DS_Estados.Tables(0).Rows(i).Item("DSC_ESTADO")
                        Dim lstrCod As String = DS_Estados.Tables(0).Rows(i).Item("COD_ESTADO")
                        objetoControl.AddItem(lstrDescripcion & ";" & lstrCod)
                    End If
                Next i

                objetoControl.Sort(0, C1.Win.C1List.SortDirEnum.ASC)
                objetoControl.Splits(0).DisplayColumns(0).Visible = True
                objetoControl.Splits(0).DisplayColumns(1).Visible = False
                objetoControl.SelectedIndex = -1
            End If
        Catch ex As Exception

        End Try
    End Sub

    Public Sub Carga_Combos_estado(ByRef objetoControl As C1.Win.C1List.C1Combo, ByVal strTipoEstado As String, Optional ByVal bIsBlank As Boolean = True)
        Dim lobjClaseEstados As ClsEstados = New ClsEstados
        Dim DS_Estados As DataSet = New DataSet
        Dim strRetorno As String = ""
        Try
            objetoControl.ClearItems()
            DS_Estados = lobjClaseEstados.Estados_Ver("", strTipoEstado, "", "DSC_ESTADO,COD_ESTADO", strRetorno)
            If DS_Estados.Tables(0).Rows.Count > 0 Then
                objetoControl.ComboStyle = ComboStyleEnum.DropdownList
                objetoControl.DataMode = DataModeEnum.AddItem
                If bIsBlank Then
                    objetoControl.AddItem("" & ";" & "")
                End If
                For i As Integer = 0 To DS_Estados.Tables(0).Rows.Count - 1
                    Dim lstrDescripcion As String = DS_Estados.Tables(0).Rows(i).Item("DSC_ESTADO")
                    Dim lstrCod As String = DS_Estados.Tables(0).Rows(i).Item("COD_ESTADO")
                    objetoControl.AddItem(lstrDescripcion & ";" & lstrCod)
                Next i

                objetoControl.Splits(0).DisplayColumns(0).Visible = True
                objetoControl.Splits(0).DisplayColumns(1).Visible = False
                objetoControl.SelectedIndex = -1
            End If
        Catch ex As Exception

        End Try
    End Sub



    Public Sub gSubCargaComboTipoCheckList(
            ByRef objetoControl As C1.Win.C1List.C1Combo,
            Optional ByVal bIfBlank As Boolean = False)
        Try
            objetoControl.ComboStyle = ComboStyleEnum.DropdownList
            objetoControl.DataMode = DataModeEnum.AddItem
            objetoControl.ClearItems()
            If bIfBlank Then
                objetoControl.AddItem(";0")
            End If
            objetoControl.AddItem("DOCUMENTACI�N;1")
            objetoControl.AddItem("TIPO DE APORTE;2")
            objetoControl.Sort(0, C1.Win.C1List.SortDirEnum.ASC)
            objetoControl.Splits(0).DisplayColumns(0).Visible = True
            objetoControl.Splits(0).DisplayColumns(1).Visible = False
            objetoControl.SelectedIndex = -1
        Catch ex As Exception

        End Try
    End Sub

    Public Sub gSubCargaComboEstadoSolicitud(
        ByRef objetoControl As C1.Win.C1List.C1Combo)
        Try
            objetoControl.ComboStyle = ComboStyleEnum.DropdownList
            objetoControl.DataMode = DataModeEnum.AddItem
            objetoControl.ClearItems()
            objetoControl.AddItem("PENDIENTE;PEN")
            objetoControl.AddItem("CONFIRMADA;CON")
            objetoControl.AddItem("RECHAZADA;RCH")
            objetoControl.Splits(0).DisplayColumns(0).Visible = True
            objetoControl.Splits(0).DisplayColumns(1).Visible = False
            objetoControl.SelectedIndex = -1
        Catch ex As Exception

        End Try
    End Sub

    Public Sub gSubTraspasoGrillas(ByRef TDG_No_Inc As C1.Win.C1TrueDBGrid.C1TrueDBGrid, ByRef TDG_Inc As C1.Win.C1TrueDBGrid.C1TrueDBGrid, ByRef DS1 As DataSet, ByVal accion As String, Optional ByVal Rama As String = "", Optional ByRef DS_Instrumento As DataSet = Nothing, Optional ByVal UPD As String = "")
        Dim intCantidad As Integer
        Select Case accion
            Case "INCLUIR"
                If TDG_No_Inc.RowCount > 0 Then

                    For intCantidad = 0 To TDG_No_Inc.SelectedRows.Count - 1
                        Dim DT_Temp As DataTable = Nothing
                        Dim row As DataRow = DS1.Tables(0).NewRow
                        row.ItemArray = DS1.Tables(1).Rows(TDG_No_Inc.SelectedRows.Item(intCantidad)).ItemArray
                        If Rama = "" Then
                            DS1.Tables(0).Rows.Add(row)
                        Else
                            Dim DATAROW As DataRow() = DS_Instrumento.Tables(0).Select("ID_INSTRUMENTO = " + row.Item(1).ToString)
                            DATAROW(0).BeginEdit()
                            DATAROW(0).Item(2) = CInt(Rama)
                            DATAROW(0).Item(3) = CInt(UPD)
                            DATAROW(0).EndEdit()
                            DS1.Tables(0).Rows.Add(row)
                        End If
                    Next
                    intCantidad = TDG_No_Inc.SelectedRows.Count - 1
                    Dim ArrIndices(intCantidad) As Integer
                    Dim i As Integer
                    For i = 0 To intCantidad
                        ArrIndices(i) = TDG_No_Inc.SelectedRows.Item(i)
                    Next i
                    Array.Sort(ArrIndices)
                    While intCantidad >= 0
                        '+ Borrar de la Lista el traspasado
                        TDG_No_Inc.Delete(ArrIndices(intCantidad))
                        intCantidad = intCantidad - 1
                    End While
                End If
            Case "NO INCLUIR"
                If TDG_Inc.RowCount > 0 Then

                    For intCantidad = 0 To TDG_Inc.SelectedRows.Count - 1
                        Dim DT_Temp As DataTable = Nothing
                        Dim row As DataRow = DS1.Tables(1).NewRow
                        row.ItemArray = DS1.Tables(0).Rows(TDG_Inc.SelectedRows.Item(intCantidad)).ItemArray
                        If Rama = "" Then
                            DS1.Tables(1).Rows.Add(row)
                        Else
                            Dim DATAROW As DataRow() = DS_Instrumento.Tables(0).Select("ID_INSTRUMENTO = " + row.Item(1).ToString)
                            DATAROW(0).BeginEdit()
                            DATAROW(0).Item(2) = CInt(Rama)
                            DATAROW(0).Item(3) = CInt(UPD)
                            DATAROW(0).EndEdit()
                            DS1.Tables(1).Rows.Add(row)
                        End If
                    Next

                    intCantidad = TDG_Inc.SelectedRows.Count - 1
                    Dim ArrIndices(intCantidad) As Integer
                    Dim i As Integer
                    For i = 0 To intCantidad
                        ArrIndices(i) = TDG_Inc.SelectedRows.Item(i)
                    Next i
                    Array.Sort(ArrIndices)
                    While intCantidad >= 0
                        '+ Borrar de la Lista el traspasado
                        TDG_Inc.Delete(ArrIndices(intCantidad))
                        intCantidad = intCantidad - 1
                    End While
                End If
        End Select
    End Sub

    Public Sub ConvierteColor(ByRef color As System.Drawing.Color, ByVal strColor As String)
        Select Case UCase(strColor)
            Case "BLUE"
                color = Drawing.Color.Blue
            Case "RED"
                color = Drawing.Color.Red
            Case "GREEN"
                color = Drawing.Color.Green
            Case "WHITE"
                color = Drawing.Color.White
            Case "YELLOW"
                color = Drawing.Color.Yellow
            Case "BROWN"
                color = Drawing.Color.Brown
            Case "DIMGRAY"
                color = Drawing.Color.DimGray
            Case "GRAY"
                color = Drawing.Color.Gray
            Case "PURPLE"
                color = Drawing.Color.Purple
            Case "BICE.TITULO.FORE"
                color = Color.FromArgb(&H9, &H3F, &H72)
            Case "BICE.TITULO.BACK"
                color = Color.FromArgb(&HDF, &HDF, &HDF)
            Case "BICE.DATA.FORE"
                color = Drawing.Color.Black
            Case "BICE.DATA.BACK"
                color = Color.FromArgb(&HDF, &HDF, &HDF)
            Case "BICE.SUBTOTAL.FORE"
                color = Drawing.Color.Black
            Case "BICE.SUBTOTAL.BACK"
                color = Color.FromArgb(&HDF, &HDF, &HDF)
            Case "BICE.TOTAL.FORE"
                color = Color.FromArgb(&H6, &H3D, &H70)
            Case "BICE.TOTAL.BACK"
                color = Color.FromArgb(&HCA, &HDA, &HE8)
            Case Else
                color = Drawing.Color.Black
        End Select
    End Sub


#End Region

#Region "Funciones - Edo"


    'Public Sub gSubGrabarEstadoColumnasGrilla(ByVal strCodConfiguracion As String, ByVal strConfiguracion As String)
    '    Dim LTxt_Resultado As String = ""
    '    Dim ClsGeneral As ClsGeneral = New ClsGeneral

    '    Try
    '        LTxt_Resultado = ClsGeneral.GuardarConfiguracionUsuario(glngIdUsuario, strCodConfiguracion, strConfiguracion)

    '    Catch ex As Exception
    '        Exit Sub
    '    End Try
    'End Sub

    Public Function gFunIndiceElemento(ByVal Arreglo() As String, ByVal strElemento As String, Optional ByVal intIndiceDondeBusca As Integer = 0) As Integer
        Dim i As Int16
        Dim Retorna As Int16 = -1

        For i = 0 To Arreglo.Length - 1
            If (InStr(Arreglo(i).Split("|")(intIndiceDondeBusca), strElemento) > 0) And _
               (Arreglo(i).Split("|")(intIndiceDondeBusca).Trim.Length = strElemento.Trim.Length) Then
                Retorna = i
                Exit For
            End If
        Next
        Return Retorna
    End Function

    Public Function gFunEsColumnaVisible(ByVal ArregloColumnas() As String, ByVal strTituloColumna As String) As Boolean
        Dim intColIndex As Int16 = -1
        Dim strColEstado() As String
        Dim Retorno As Boolean = True

        If ArregloColumnas.Length > 0 Then
            intColIndex = gFunIndiceElemento(ArregloColumnas, strTituloColumna, 1)

            If intColIndex <> -1 Then
                strColEstado = ArregloColumnas(intColIndex).Split("|")
                If strColEstado(2) = "0" Then
                    Retorno = False
                End If
            End If
        End If
        Return Retorno
    End Function

    Public Function gFunEsColumnaFija(ByVal ArregloColumnas() As String, ByVal strTituloColumna As String) As Boolean
        Dim intColIndex As Int16 = -1
        Dim strColEstado() As String
        Dim Retorno As Boolean = True

        If ArregloColumnas.Length > 0 Then
            intColIndex = gFunIndiceElemento(ArregloColumnas, strTituloColumna, 1)

            If intColIndex <> -1 Then
                strColEstado = ArregloColumnas(intColIndex).Split("|")
                If strColEstado(3) = "0" Then
                    Retorno = False
                End If
            End If
        End If

        Return Retorno
    End Function

    Public Function gFunEsColumnaConfigurable(ByVal ArregloColumnas() As String, ByVal strTituloColumna As String) As Boolean
        Dim intColIndex As Int16 = -1
        Dim strColEstado() As String
        Dim Retorno As Boolean = True

        If ArregloColumnas.Length > 0 Then
            intColIndex = gFunIndiceElemento(ArregloColumnas, strTituloColumna, 1)

            If intColIndex <> -1 Then
                strColEstado = ArregloColumnas(intColIndex).Split("|")
                If strColEstado(4) = "N" Then
                    Retorno = False
                End If
            End If
        End If

        Return Retorno
    End Function

#End Region 'Eduardo - Funciones

#Region "Funciones Victoria Cortes"

    Private sBuffer As String ' Para usarla en las funciones GetSection(s)
    Private Declare Function GetPrivateProfileSection Lib "kernel32" Alias "GetPrivateProfileSectionA" (ByVal lpAppName As String, ByVal lpReturnedString As String, ByVal nSize As Integer, ByVal lpFileName As String) As Integer
    Private Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Integer, ByVal lpFileName As String) As Integer
    Private Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Integer, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Integer, ByVal lpFileName As String) As Integer

    Public Function ObtieneValor_ArchivoIni(ByVal sFileName As String, ByVal sSection As String, ByVal sKeyName As String, Optional ByVal sDefault As String = "") As String
        '--------------------------------------------------------------------------
        ' Devuelve el valor de una clave de un fichero INI
        ' Los par�metros son:
        '   sFileName   El fichero INI
        '   sSection    La secci�n de la que se quiere leer
        '   sKeyName    Clave
        '   sDefault    Valor opcional que devolver� si no se encuentra la clave
        '--------------------------------------------------------------------------
        Dim ret As Integer
        Dim sRetVal As String
        '
        sRetVal = New String(Chr(0), 255)
        '
        ret = GetPrivateProfileString(sSection, sKeyName, sDefault, sRetVal, Len(sRetVal), sFileName)
        If ret = 0 Then
            Return sDefault
        Else
            Return Microsoft.VisualBasic.Strings.Left(sRetVal, ret)
        End If
    End Function

    Public Function ObtieneSeccion_ArchivoIni(ByVal sFileName As String, ByVal sSection As String) As String()
        '--------------------------------------------------------------------------
        ' Lee una secci�n entera de un fichero INI                      (27/Feb/99)
        ' Adaptada para devolver un array de string                     (04/Abr/01)
        '
        ' Esta funci�n devolver� un array de �ndice cero
        ' con las claves y valores de la secci�n
        '
        ' Par�metros de entrada:
        '   sFileName   Nombre del fichero INI
        '   sSection    Nombre de la secci�n a leer
        ' Devuelve:
        '   Un array con el nombre de la clave y el valor
        '   Para leer los datos:
        '       For i = 0 To UBound(elArray) -1 Step 2
        '           sClave = elArray(i)
        '           sValor = elArray(i+1)
        '       Next
        '
        Dim aSeccion() As String
        Dim n As Integer
        '
        ReDim aSeccion(0)
        '
        ' El tama�o m�ximo para Windows 95
        sBuffer = New String(ChrW(0), 32767)
        '
        n = GetPrivateProfileSection(sSection, sBuffer, sBuffer.Length, sFileName)
        '
        If n > 0 Then
            sBuffer = sBuffer.Substring(0, n - 1).TrimEnd()
            ' Cada elemento estar� separado por un Chr(0)
            ' y cada valor estar� en la forma: clave = valor
            'aSeccion = sBuffer.Split(New Char() {ChrW(0), "="c})
            aSeccion = sBuffer.Split(ChrW(0))
        End If
        ' Devolver el array
        Return aSeccion
    End Function

    Function gFunEncripta(ByVal strCadena As String, ByVal blnModo As Boolean) As String
        Dim X As Single
        Dim xPdw As String
        Dim Caracteres As String
        Dim Codificacion As String

        Caracteres = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890abcdefghijklmnopqrstuvwxyz"
        Codificacion = "<?>zTcklop}IZiqLmatr@uxAOW[{nMGKXSysDBCP_h\EvNRw`]QHfbdeJgVFYU"

        xPdw = ""
        gFunEncripta = ""

        For X = 1 To Len(strCadena)
            If blnModo Then
                xPdw = xPdw + Chr((Asc(Mid(Codificacion, InStr(1, Caracteres, Mid(strCadena, X, 1)), 1)) - X))
            Else
                xPdw = xPdw + Mid(Caracteres, InStr(1, Codificacion, Chr(Asc(Mid(strCadena, X, 1)) + X)), 1)
            End If
        Next
        gFunEncripta = xPdw
    End Function

    Public Function gFunInitCapital(ByVal strTexto As String) As String
        Dim lstrTextoIniCapital As String = ""
        If strTexto.Length = 1 Then
            lstrTextoIniCapital = UCase(strTexto)
        Else
            Dim lstrTexto() As String = strTexto.Split(" ")
            Dim lintIndice As Integer = 0
            For lintIndice = 0 To lstrTexto.Length - 1
                If lstrTexto(lintIndice) <> "" Then
                    lstrTextoIniCapital = lstrTextoIniCapital & " " & UCase(lstrTexto(lintIndice).Substring(0, 1)) & LCase(lstrTexto(lintIndice).Substring(1, lstrTexto(lintIndice).Length - 1))
                End If
            Next
        End If

        Return lstrTextoIniCapital
    End Function

#End Region

#Region "Funciones - HAF: 8==3~X_O!!"

    Public Sub gsubFormatTextBox(ByRef Textbox As C1.Win.C1Input.C1TextBox, ByVal Formato As String)
        Textbox.CustomFormat = Formato
        Textbox.DisplayFormat.CustomFormat = Formato
        Textbox.EditFormat.CustomFormat = Formato
    End Sub

    Public Sub AddCampo(ByRef dtbTabla As DataTable, ByVal strNombreCampo As String, ByVal strTipoCampo As String)
        Dim dtcColumna As DataColumn = New DataColumn(strNombreCampo)
        dtcColumna.DataType = System.Type.GetType(strTipoCampo)
        dtcColumna.AllowDBNull = True
        dtbTabla.Columns.Add(dtcColumna)
    End Sub

    Public Sub gsubLimpiar_FiltroGrillas(ByRef Grilla As C1.Win.C1TrueDBGrid.C1TrueDBGrid)
        Dim intColumna As Integer
        Grilla.FilterActive = False
        For intColumna = 0 To Grilla.Columns.Count - 1
            If Grilla.Splits(0).DisplayColumns(intColumna).Visible Then
                Grilla.Columns(intColumna).FilterText = ""
            End If
        Next
        Grilla.FilterActive = True
    End Sub

    Public Sub gsubConfiguraDataTable(ByRef dtbTabla As DataTable, ByVal strColumnas As String)
        Dim lArr_NombreColumna() As String
        Dim ldtbTabla_Aux As New DataTable
        Dim Remove As Boolean
        Dim LInt_Col As Integer
        Dim LInt_NomCol As String

        lArr_NombreColumna = Strings.Split(strColumnas, ",")

        ldtbTabla_Aux = dtbTabla

        If strColumnas.Trim = "" Then
            ldtbTabla_Aux = dtbTabla
        Else
            ldtbTabla_Aux = dtbTabla.Copy
            For Each Columna As DataColumn In dtbTabla.Columns
                Remove = True
                For LInt_Col = 0 To lArr_NombreColumna.Length - 1
                    LInt_NomCol = lArr_NombreColumna(LInt_Col).Trim
                    If Columna.ColumnName = LInt_NomCol Then
                        Remove = False
                        Exit For
                    End If
                Next
                If Remove Then
                    ldtbTabla_Aux.Columns.Remove(Columna.ColumnName)
                End If
            Next
        End If

        For LInt_Col = 0 To lArr_NombreColumna.Length - 1
            LInt_NomCol = lArr_NombreColumna(LInt_Col).Trim
            For Each Columna In ldtbTabla_Aux.Columns
                If Columna.ColumnName = LInt_NomCol Then
                    Columna.SetOrdinal(LInt_Col)
                    Exit For
                End If
            Next
        Next
        dtbTabla = ldtbTabla_Aux
    End Sub

    'Public Sub NotifyTextPopUp(ByVal pTexto As String _
    '                         , ByVal pTitulo As String _
    '                         , Optional ByVal pIcono As ToolTipIcon = ToolTipIcon.None)
    '    If Not goNotifyIcon Is Nothing Then
    '        If Not goNotifyIcon.Visible Then
    '            Try
    '                goNotifyIcon.Visible = True
    '            Catch ex As Exception
    '            End Try
    '        End If

    '        'If goNotifyIcon.Tag Then
    '        'goNotifyIcon.BalloonTipText = pTexto
    '        'goNotifyIcon.BalloonTipTitle = pTitulo
    '        'goNotifyIcon.BalloonTipIcon = pIcono
    '        'goNotifyIcon.ShowBalloonTip(50)
    '        'Else
    '        goNotifyIcon.ShowBalloonTip(100, pTitulo, pTexto, pIcono)
    '        'End If
    '    End If

    '    With Frm_PopUp
    '        .TopMost = True
    '        .Show()
    '        .Actualiza(pTitulo, pTexto)
    '        .Refresh()
    '    End With
    'End Sub

    Public Function NVL(ByVal varValor, ByVal varIsNull)
        If IsDBNull(varValor) Then
            Return varIsNull
        Else
            Return varValor
        End If
    End Function



    Public Function NTL(ByVal varValor, ByVal varIsNothing)
        If varValor Is Nothing Then
            Return varIsNothing
        Else
            Return varValor
        End If
    End Function

#End Region


    Public Function crearCsvDesdeDatatable(ByVal nombreArchivo As String, _
                                       ByVal dt As DataTable, _
                                       ByVal separatorChar As Char, _
                                       ByVal primeraFilaContieneNombres As Boolean, _
                                       ByVal textDelimiter As Boolean, _
                                       ByVal strDirectorio As String) As String
        'validaciones

        If dt.Rows.Count = 0 Then
            crearCsvDesdeDatatable = "Error al generar csv,sin filas en set de datos.- "
            Exit Function
        End If

        If separatorChar = "" Then
            crearCsvDesdeDatatable = "Error al generar csv,sin separador definido.- "
            Exit Function
        End If

        If nombreArchivo = "" Then
            crearCsvDesdeDatatable = "Error al generar csv,falta definir nombre archivo.- "
            Exit Function
        End If
        If strDirectorio = "" Then
            crearCsvDesdeDatatable = "Error al generar csv,falta ruta archivo.- "
            Exit Function
        End If
        ' Si no se ha especificado un nombre de archivo,
        ' o el objeto DataTable no es v�lido, provocamos
        ' una excepci�n de argumentos no v�lidos.
        If (nombreArchivo = String.Empty) Or (dt Is Nothing) Then
            crearCsvDesdeDatatable = "Argumentos no v�lidos."
            Exit Function
        End If
        ' Si el archivo existe, solicito confirmaci�n para sobreescribirlo.

        If (File.Exists(strDirectorio & "\" + nombreArchivo)) Then
            If (MessageBox.Show("Ya existe un archivo de texto con el mismo nombre." & Environment.NewLine & _
                               "�Desea sobrescribirlo?", _
                               "Crear archivo de texto delimitado", _
                               MessageBoxButtons.YesNo, _
                               MessageBoxIcon.Information) = DialogResult.No) Then Return False
        End If


        Dim sw As System.IO.StreamWriter

        Try
            Dim col As Integer = 0
            Dim value As String = String.Empty

            ' Creamos el archivo de texto con la codificaci�n por defecto.
            '
            sw = New StreamWriter(strDirectorio & "\" + nombreArchivo, False, System.Text.Encoding.Default)

            If (primeraFilaContieneNombres) Then
                ' La primera l�nea del archivo de texto contiene
                ' el nombre de los campos.
                For Each dc As DataColumn In dt.Columns

                    If (textDelimiter) Then
                        ' Incluimos el nombre del campo entre el caracter
                        ' delimitador de texto especificado.
                        '
                        value &= """" & dc.ColumnName & """" & separatorChar

                    Else
                        ' No se incluye caracter delimitador de texto alguno.
                        '
                        value &= dc.ColumnName & separatorChar

                    End If

                Next

                sw.WriteLine(value.Remove(value.Length - 1, 1))
                value = String.Empty

            End If

            ' Recorremos todas las filas del objeto DataTable
            ' incluido en el conjunto de datos.
            '
            For Each dr As DataRow In dt.Rows

                For Each dc As DataColumn In dt.Columns

                    If ((dc.DataType Is System.Type.GetType("System.String")) And _
                       (textDelimiter = True)) Then

                        ' Incluimos el dato alfanum�rico entre el caracter
                        ' delimitador de texto especificado.
                        '
                        value &= """" & dr.Item(col).ToString & """" & separatorChar

                    Else
                        ' No se incluye caracter delimitador de texto alguno
                        '
                        value &= dr.Item(col).ToString & separatorChar

                    End If

                    ' Siguiente columna
                    col += 1

                Next

                ' Al escribir los datos en el archivo, elimino el
                ' �ltimo car�cter delimitador de la fila.
                '
                sw.WriteLine(value.Remove(value.Length - 1, 1))
                value = String.Empty
                col = 0

            Next ' Siguiente fila

            ' Nos aseguramos de cerrar el archivo
            '
            sw.Close()

            crearCsvDesdeDatatable = "OK"

        Catch ex As Exception
            crearCsvDesdeDatatable = ex.Message

        Finally
            sw = Nothing

        End Try

    End Function
    Public Function GeneraCadena(ByVal dtTabla As DataTable, _
                                 ByVal strColumnas As String, _
                                 ByVal strSeparador As String, _
                                 ByRef strCadena1 As String, _
                                 Optional ByRef strCadena2 As String = "*", _
                                 Optional ByRef strCadena3 As String = "*") As String
        Dim lstrResultado As String = "OK"

        If strColumnas = "" Then
            lstrResultado = "No se han definido las columnas"
            Return lstrResultado
        End If

        Dim larr_NombreColumna() As String = strColumnas.Split(strSeparador)
        Dim lstrColumna As String = ""
        Dim lstrCadena As String = ""
        Dim lintNumeroCadena As Integer = 1
        strCadena1 = ""
        Dim lstrvalor As String = ""
        Dim DfechaPaso As Date
        Dim lstrCadena_paso As String = ""

        For Each DtFila As DataRow In dtTabla.Rows
            lstrCadena_paso = ""
            For lintindice As Integer = 0 To larr_NombreColumna.Length - 1
                lstrColumna = Trim(larr_NombreColumna(lintindice))

                If dtTabla.Columns.IndexOf(lstrColumna) = -1 Then
                    lstrvalor = "null"
                Else
                    If IsDBNull(DtFila(lstrColumna)) OrElse DtFila(lstrColumna).ToString.Trim = "" Then
                        lstrvalor = "null"
                    Else
                        'TypeOf(value) Is DateTime
                        If TypeOf (DtFila(lstrColumna)) Is DateTime Or InStr(lstrColumna, "FECHA") Or InStr(lstrColumna, "FCH_") Then
                            DfechaPaso = DtFila(lstrColumna)
                            lstrvalor = DfechaPaso.ToString.Substring(0, 10)
                        Else
                            lstrvalor = DtFila(lstrColumna).ToString.Trim
                        End If
                        'lstrvalor = DtFila(lstrColumna).ToString.Trim
                    End If
                End If
                lstrCadena_paso = lstrCadena_paso & lstrvalor & strSeparador
            Next

            If (lstrCadena & lstrCadena_paso).ToString.Length > 8000 Then
                If strCadena2 = "*" And strCadena3 = "*" Then
                    lstrResultado = "Detalle es demasiado largo"
                    Return lstrResultado
                End If
                Select Case lintNumeroCadena
                    Case 1
                        strCadena1 = lstrCadena
                        lintNumeroCadena = lintNumeroCadena + 1
                    Case 2
                        strCadena2 = lstrCadena
                        lintNumeroCadena = lintNumeroCadena + 1
                    Case 3
                        strCadena3 = lstrCadena
                        lstrResultado = "Detalle es demasiado largo"
                        Return lstrResultado
                End Select
                lstrCadena = ""
            End If
            lstrCadena = lstrCadena & lstrCadena_paso
        Next

        Select Case lintNumeroCadena
            Case 1
                strCadena1 = lstrCadena
            Case 2
                strCadena2 = lstrCadena
            Case 3
                strCadena3 = lstrCadena
        End Select

        Return lstrResultado
    End Function

    Public Function GeneraCadenaText(ByVal dtTabla As DataTable, _
                             ByVal strColumnas As String, _
                             ByVal strSeparador As String, _
                             ByRef strCadena As String) As String
        Dim lstrResultado As String = "OK"

        If strColumnas = "" Then
            lstrResultado = "No se han definido las columnas"
            Return lstrResultado
        End If

        Dim larr_NombreColumna() As String = strColumnas.Split(strSeparador)
        Dim lstrColumna As String = ""
        Dim lstrCadena As String = ""
        Dim lintNumeroCadena As Integer = 1
        strCadena = ""
        Dim lstrvalor As String = ""
        Dim DfechaPaso As Date

        For Each DtFila As DataRow In dtTabla.Rows
            For lintindice As Integer = 0 To larr_NombreColumna.Length - 1
                lstrColumna = Trim(larr_NombreColumna(lintindice))
                If dtTabla.Columns.IndexOf(lstrColumna) = -1 Then
                    lstrvalor = "null"
                Else
                    If IsDBNull(DtFila(lstrColumna)) OrElse DtFila(lstrColumna).ToString.Trim = "" Then
                        lstrvalor = "null"
                    Else
                        'TypeOf(value) Is DateTime
                        If TypeOf (DtFila(lstrColumna)) Is DateTime Or InStr(lstrColumna, "FECHA") Or InStr(lstrColumna, "FCH_") Then
                            DfechaPaso = DtFila(lstrColumna)
                            lstrvalor = DfechaPaso.ToString.Substring(0, 10)
                        Else
                            lstrvalor = DtFila(lstrColumna).ToString.Trim
                        End If
                        'lstrvalor = DtFila(lstrColumna).ToString.Trim
                    End If
                End If

                lstrCadena = lstrCadena & lstrvalor.ToString.Trim & strSeparador
            Next
            lstrCadena = lstrCadena & strSeparador
        Next
        strCadena = lstrCadena
        Return lstrResultado
    End Function

    ''Function EsFecha(ByVal sFecha As String) As Boolean
    ''    Dim sCodigoFecha As String = ""
    ''    Dim pos1 As Integer

    ''    EsFecha = True

    ''    'If CDate(sFecha) Then Exit Function
    ''    'MsgBox(sFecha)
    ''    If Len(sFecha) > 10 Then
    ''        pos1 = InStr(sFecha, "12:00:00")
    ''        sCodigoFecha = sFecha.ToString.Substring(pos1 - 1, (Len(sFecha) + 1 - pos1))
    ''        If sCodigoFecha = "12:00:00 AM" Or sCodigoFecha = "12:00:00 PM" Then
    ''            Exit Function
    ''        End If
    ''    End If
    ''    EsFecha = False
    ''End Function

    ''' <summary>
    ''' Funcion para enviar correos a N personas, con los nombres separados por coma (,).
    ''' </summary>
    ''' <param name="para">Parametro que integra a quien se Dirige el correo en PARA (se separan por coma , )</param>
    ''' <param name="cc">Parametro que integra a quien se Dirige el correo en COPIA (se separan por coma , )</param>
    ''' <param name="bcc">Parametro que integra a quien se Dirige el correo con COPIA OCULTA (se separan por coma , )</param>
    ''' <param name="asunto">Asunto del Correo</param>
    ''' <param name="contenido">El Cuerpo del correo</param>
    ''' <param name="filePath">Parametro que integra un archivo para enviar</param>
    ''' <param name="EsHtml">True or False si el contenido del correo es en HTML</param>
    ''' <returns>Si el envio resulto OK</returns>
    ''' <remarks></remarks>
    Public Function EnviarCorreo(ByVal para As String,
                                 ByVal cc As String,
                                 ByVal bcc As String,
                                 ByVal asunto As String,
                                 ByVal contenido As String,
                                 ByVal filePath As Dictionary(Of String, String),
                                 ByVal EsHtml As Boolean) As String
        Dim respuesta = String.Empty
        Try
            Dim smtpServer = ObtenerValorParametro("SERVIDOR_CORREO")
            Dim puerto = ObtenerValorParametro("SERVIDOR_CORREO_PORT")
            Dim correoWebMaster = ObtenerValorParametro("ENVIO_MAIL_CORREO")
            Dim correoUserWebMaster = ObtenerValorParametro("ENVIO_USER_CORREO")
            Dim validaPassword = ObtenerValorParametro("ENVIO_PASS_CORREO_SN")
            Dim enableSsl = ObtenerValorParametro("ENABLE_SSL_SERVIDOR")
            Dim correoPasswordWebMaster = String.Empty
            Dim correoDisplayName = String.Empty
            If validaPassword.Trim = "S" Then
                Try
                    correoPasswordWebMaster = ObtenerValorParametro("ENVIO_PASS_CORREO")
                    correoPasswordWebMaster = ClsEncriptacion.DesEncriptar(correoPasswordWebMaster)
                Catch
                    correoPasswordWebMaster = String.Empty
                End Try
            End If
            correoDisplayName = ObtenerValorParametro("ENVIO_DISPLAY_CORREO")

            Dim mail As New MailMessage With {
                .From = New MailAddress(correoWebMaster.Trim(), correoDisplayName.Trim()),
                .SubjectEncoding = Text.Encoding.UTF8,
                .Subject = asunto,
                .Priority = MailPriority.Normal,
                .IsBodyHtml = EsHtml,
                .Body = contenido
            }

            Dim altView = AlternateView.CreateAlternateViewFromString(
                contenido,
                Nothing,
                MediaTypeNames.Text.Html)

            For Each path In filePath
                Dim img = New LinkedResource(path.Value, MediaTypeNames.Image.Jpeg) With {
                    .ContentId = path.Key,
                    .TransferEncoding = TransferEncoding.Base64
                }
                altView.LinkedResources.Add(img)
            Next

            mail.AlternateViews.Add(altView)

            mail.To.Add(para)
            If Not String.IsNullOrEmpty(cc) Then
                mail.CC.Add(cc)
            End If

            If Not String.IsNullOrEmpty(bcc) Then
                mail.Bcc.Add(bcc)
            End If

            Dim smtp As New SmtpClient With {
                .Host = smtpServer,
                .Port = puerto,
                .DeliveryMethod = SmtpDeliveryMethod.Network,
                .EnableSsl = enableSsl.Equals("S")
            }

            If validaPassword.Trim = "S" Then
                smtp.UseDefaultCredentials = False
                smtp.Credentials = New NetworkCredential(correoUserWebMaster.Trim(), correoPasswordWebMaster.Trim())
            Else
                smtp.Credentials = CredentialCache.DefaultNetworkCredentials
            End If

            smtp.Send(mail)

            respuesta = "OK"
        Catch ex As SmtpException
            respuesta = ex.Message.ToString
            respuesta = String.Format("{0}{1}Source: {2}", respuesta, vbCrLf, ex.Source)
            respuesta = String.Format("{0}{1}StatusCode: {2}", respuesta, vbCrLf, ex.StatusCode)
            If Not IsNothing(ex.InnerException) Then
                respuesta = String.Format("{0}{1}InnerException: {2}", respuesta, vbCrLf, ex.InnerException.Message)
                respuesta = String.Format("{0}{1}TargetSite: {2}", respuesta, vbCrLf, ex.InnerException.Source)
                respuesta = String.Format("{0}{1}StackTrace: {2}{1}", respuesta, vbCrLf, ex.InnerException.StackTrace)
            End If
        Catch ex As Exception
            respuesta = ex.Message.ToString
        End Try
        Return respuesta
    End Function

#Region "Funciones Esteban Reinoso"



#End Region


#Region "Funciones GPI++ Rodolfo Diaz"

    ' Obejto Multitarea para exportacion Excel
    Public bw As BackgroundWorker = New BackgroundWorker
    Public Sh_Thread As Thread
    Public Led_Thread As Thread
    Public Db_Led As Form

    Public Function Fnt_Resize_Controls(ByRef FrmCtrl As Object, ByRef Frm As Object) As Boolean
        ' ---------------------------------------------------------------------------------------------------------
        ' Auto-Ajuste de controles Resize
        ' RDiazG
        '
        ' Requerimientos : La funcion Requiere la declaracion de estos 2 elementos publicos de formulario
        ' Public Class {Formulario}
        '     Public PrevSizeX As Integer = Me.Width
        '     Public PrevSizeY As Integer = Me.Height
        '
        ' En el Form_Load Aplicar Tama�o Minimo del form:
        '
        '   Me.MinimumSize = Me.Size
        '
        '   
        ' Parametros : 
        ' Frm_Ctrl      Objeto Control de recorrido raiz, inicialmente siempre sera el form(Me) al cual se desea autoajustar
        '               La Propia Funcion crea la recursividad necesaria para recorrer los demas controles del set
        ' Frm           Objeto Formulario al cual se desea auto-ajustar
        '
        ' Ej Comun de uso: 
        '   
        '   Event Form_Resize
        '       Fnt_Resize_Controls(Me,Me)
        '   
        ' Nota: No modificar la propiedad anchor de los controles del formulario, dejar en default : top, Left
        '       Provoca Conflictos en el evento Resize
        ' -----------------------------------------------------------------------------------------------------------
        ' Versiones :
        '   RdiazG 1.0      : Version Inicial
        '   RdiazG 1.1      : Se Corrige descuadre al evitar recalcular controles al minimizar
        '   RdiazG 1.2      : Se a�ade C1DockingTab y C1DockingTabPage a Controles Contenedor
        '   RdiazG 1.3      : Se Elimina Ajuste de Formularios dependientes, no se reajustaran mas Forms dentro de Forms
        '   RdiazG 1.4      : Se Agrega a las Bases el Tipo TabControl
        '
        '
        ' -----------------------------------------------------------------------------------------------------------
        'Verificar Estado de formulario, si esta minimizado no recalcular controles
        If Frm.windowstate = FormWindowState.Minimized Then
            Frm.bringtofront()
            Exit Function
        End If

        Dim xCalc_Temp As Double = Frm.Width / Frm.PrevSizeX
        Dim yCalc_Temp As Double = Frm.Height / Frm.PrevSizeY

        ' Recorre Controles en su base
        For Each tmpControl In FrmCtrl.Controls

            ' Verifica Controles Contenedores y aplica recursividad
            If TypeOf (tmpControl) Is GroupBox Then Fnt_Resize_Controls(tmpControl, Frm)
            If TypeOf (tmpControl) Is TableLayoutPanel Then Fnt_Resize_Controls(tmpControl, Frm)
            If TypeOf (tmpControl) Is Panel Then Fnt_Resize_Controls(tmpControl, Frm)
            If TypeOf (tmpControl) Is TabControl Then Fnt_Resize_Controls(tmpControl, Frm)
            If TypeOf (tmpControl) Is TableLayoutPanel Then Fnt_Resize_Controls(tmpControl, Frm)
            If TypeOf (tmpControl) Is C1.Win.C1Command.C1DockingTab Then Fnt_Resize_Controls(tmpControl, Frm)
            If TypeOf (tmpControl) Is C1.Win.C1Command.C1DockingTabPage Then Fnt_Resize_Controls(tmpControl, Frm)
            If TypeOf (tmpControl) Is Form Then Exit Function
            ' Configuracion especifica por control Agregar mas segun Necesidad
            If TypeOf (tmpControl) Is C1.Win.C1List.C1Combo Then
                tmpControl.Left = tmpControl.Left * xCalc_Temp
                tmpControl.Top = tmpControl.Top * yCalc_Temp
                tmpControl.Width = tmpControl.Width * xCalc_Temp
            ElseIf TypeOf (tmpControl) Is C1.Win.C1Input.C1DateEdit Then
                tmpControl.Left = tmpControl.Left * xCalc_Temp
                tmpControl.Top = tmpControl.Top * yCalc_Temp
                tmpControl.Width = tmpControl.Width * xCalc_Temp
            ElseIf TypeOf (tmpControl) Is C1.Win.C1Input.C1TextBox Then
                tmpControl.Left = tmpControl.Left * xCalc_Temp
                tmpControl.Top = tmpControl.Top * yCalc_Temp
                tmpControl.Width = tmpControl.Width * xCalc_Temp
            ElseIf TypeOf (tmpControl) Is C1.Win.C1Input.C1Button Then
                tmpControl.Left = tmpControl.Left * xCalc_Temp
                tmpControl.Top = tmpControl.Top * yCalc_Temp
            ElseIf TypeOf (tmpControl) Is C1.Win.C1SuperTooltip.C1SuperLabel Then
                tmpControl.Left = tmpControl.Left * xCalc_Temp
                tmpControl.Top = tmpControl.Top * yCalc_Temp
            ElseIf TypeOf (tmpControl) Is ToolStrip Then
                tmpControl.Left = tmpControl.Left * xCalc_Temp
                tmpControl.Top = tmpControl.Top * yCalc_Temp
            Else
                tmpControl.Left = tmpControl.Left * xCalc_Temp
                tmpControl.Top = tmpControl.Top * yCalc_Temp
                tmpControl.Width = tmpControl.Width * xCalc_Temp
                tmpControl.Height = tmpControl.Height * yCalc_Temp
            End If
        Next tmpControl

        ' Se guarda ultimo tama�o de frm conocido, para siguiente cambio y calculo
        If FrmCtrl Is Frm Then
            Frm.PrevSizeX = Frm.Width
            Frm.PrevSizeY = Frm.Height
        End If

    End Function



    Public Function Zoom_Control(ByRef Frm_Origen As Form, ByRef obj As Object, ByVal Ox As Integer, ByVal Oy As Integer, ByVal Ow As Integer, ByVal Oh As Integer, ByVal Zoom_Factor As Long) As Boolean
        Dim mw As Integer
        Dim mh As Integer
        Dim mpcx As Integer = Frm_Origen.Width / 2 ' Centro del form
        Dim mpcy As Integer = Frm_Origen.Height / 2 ' Centro del Form

        If Ox = obj.Left Then
            mw = ((obj.Width * Zoom_Factor)) ' ancho de control aumentado al factor
            mh = ((obj.Height * Zoom_Factor)) ' alto de control aumentado al factor 

            obj.Left = (mpcx - (mw / 2))
            obj.Top = (mpcy - (mh / 2)) - 50 ' - pixeles de reajuste con las barras
            obj.Width = mw
            obj.Height = mh
        Else
            obj.Left = Ox
            obj.Top = Oy
            obj.Width = Ow
            obj.Height = Oh
        End If
    End Function

#End Region

#Region "Funciones Enrique Valenzuela"
    'Public Function Consultar_ConsumirWsConsultaSaldo(ByVal strCtaCte As String, _
    '                                                   ByRef strRetorno As String) As DataRowView
    '    Dim dv As DataView = Nothing
    '    Dim drv As DataRowView = Nothing
    '    Dim dsParametro As New DataSet
    '    Dim dsConsultaSaldo As New DataSet
    '    Try
    '        Dim strOrigenWs As String = ""
    '        dsParametro = gobjParametro.RetornarValorParametro("WS_BICE", strRetorno)
    '        If dsParametro Is Nothing Or dsParametro.Tables(0).Rows.Count = 0 Then
    '            Throw New System.Exception("Error al leer Parametro WS_BICE.")
    '        Else
    '            strOrigenWs = dsParametro.Tables(0).Rows(0).Item("VALOR_PARAMETRO")
    '            If strOrigenWs.Trim = "" Then
    '                Throw New System.Exception("Par�metro WS_BICE no esta ingresado.")
    '            End If
    '        End If
    '        If strOrigenWs.ToUpper = "BICE" Then
    '            strRetorno = ConsumirWsConsultaSaldoBice(strCtaCte, dsConsultaSaldo)
    '        ElseIf strOrigenWs.ToUpper = "LOCAL" Then
    '            strRetorno = ConsumirWsConsultaSaldo(strCtaCte, dsConsultaSaldo)
    '        End If
    '        If strRetorno = "OK" Then
    '            If dsConsultaSaldo.Tables(0).Rows.Count > 0 Then
    '                dv = dsConsultaSaldo.Tables(0).DefaultView
    '                ' ''para filtrar en caso de que sean varias filas
    '                ''Dim strNombreColumna As String = ""
    '                ''Dim strValor As String = ""
    '                ''dv.RowFilter = strNombreColumna & " = '" & strValor & "'"
    '                ''For Each drvFor As DataRowView In dv
    '                ''    drv = drvFor
    '                ''Next
    '                Return dv(0)
    '            Else
    '                Throw New System.Exception("WsConsultaSaldo no ha devuelto datos")
    '            End If
    '        Else
    '            Throw New System.Exception(strRetorno)
    '        End If
    '    Catch ex As Exception
    '        strRetorno = ex.Message
    '    End Try
    '    Return drv
    'End Function


#End Region

#Region "Funciones Varias GPI++"

    Public Function gstrFiltro_X_TipoColumna(ByRef pdcColuma As C1.Win.C1TrueDBGrid.C1DataColumn) As String
        Dim lstrFiltro As String = ""
        Dim lstrVarFiltro As String = ""
        Try
            Select Case pdcColuma.DataType.ToString.ToUpper
                Case "SYSTEM.STRING"
                    If pdcColuma.FilterText.ToString = "True" Then
                        lstrVarFiltro = "1"
                    ElseIf pdcColuma.FilterText.ToString = "False" Then
                        lstrVarFiltro = "0"
                    Else
                        lstrVarFiltro = pdcColuma.FilterText.ToString()
                    End If
                    'lstrFiltro = (pdcColuma.DataField & " like '%" & pdcColuma.FilterText & "%'")
                    lstrFiltro = ("[" & pdcColuma.DataField & "] like '%" & lstrVarFiltro & "%'")
                Case "SYSTEM.DATETIME"
                    lstrFiltro = ("CONVERT([" & pdcColuma.DataField & "], 'System.String') like '%" & pdcColuma.FilterText & "%'")
                Case "SYSTEM.DECIMAL", "SYSTEM.INT32"
                    If pdcColuma.NumberFormat.Length = 0 Then
                        'ES UN NUMERO SIN FORMATO PERO QUE LO COMPARAMOS COMO STRING
                        'lstrFiltro = ("CONVERT(" & pdcColuma.DataField & ", 'System.String') like '%" & pdcColuma.FilterText. & "%'")
                        If pdcColuma.FilterText.ToString = "True" Then
                            lstrVarFiltro = "1"
                        ElseIf pdcColuma.FilterText.ToString = "False" Then
                            lstrVarFiltro = "0"
                        Else
                            lstrVarFiltro = pdcColuma.FilterText.ToString()
                        End If
                        lstrFiltro = ("CONVERT([" & pdcColuma.DataField & "], 'System.String') like '%" & lstrVarFiltro.Replace(",", ".") & "%'")
                    Else
                        'lstrFiltro = pdcColuma.FilterText
                        lstrFiltro = pdcColuma.FilterText.ToString.Replace(",", ".")
                        If gFunFiltroValido(lstrFiltro) Then
                            lstrFiltro = pdcColuma.DataField & lstrFiltro
                        Else
                            lstrFiltro = ""
                        End If
                    End If
                Case "SYSTEM.DOUBLE"
                    If pdcColuma.NumberFormat.Length = 0 Then
                        'ES UN NUMERO SIN FORMATO PERO QUE LO COMPARAMOS COMO STRING
                        'lstrFiltro = ("CONVERT(" & pdcColuma.DataField & ", 'System.String') like '%" & pdcColuma.FilterText & "%'")
                        If pdcColuma.FilterText.ToString = "True" Then
                            lstrVarFiltro = "1"
                        ElseIf pdcColuma.FilterText.ToString = "False" Then
                            lstrVarFiltro = "0"
                        Else
                            lstrVarFiltro = pdcColuma.FilterText.ToString()
                        End If
                        lstrFiltro = ("CONVERT([" & pdcColuma.DataField & "], 'System.String') like '%" & lstrVarFiltro.Replace(",", ".") & "%'")
                    Else
                        'lstrFiltro = pdcColuma.FilterText
                        lstrFiltro = pdcColuma.FilterText.ToString.Replace(",", ".")
                        If gFunFiltroValido(lstrFiltro) Then
                            lstrFiltro = pdcColuma.DataField & lstrFiltro
                        Else
                            lstrFiltro = ""
                        End If
                    End If
                Case "SYSTEM.BOOLEAN"
                    If pdcColuma.FilterText.ToString = "True" Then
                        lstrVarFiltro = "True"
                    ElseIf pdcColuma.FilterText.ToString = "False" Then
                        lstrVarFiltro = "False"
                    Else
                        lstrVarFiltro = pdcColuma.FilterText.ToString()
                    End If
                    lstrFiltro = ("CONVERT([" & pdcColuma.DataField & "],'System.String') like '%" & lstrVarFiltro & "%'")
                Case Else
                    lstrFiltro = (pdcColuma.DataField & " like '%" & pdcColuma.FilterText & "%'")
            End Select
        Catch ex As Exception
        End Try

        Return lstrFiltro
    End Function

    Public Function CuentaFiltro(ByVal strFiltro As String) As Integer
        Dim arrFiltro() As String

        If strFiltro = "" Then
            Return 0
        Else
            arrFiltro = strFiltro.Split("�")
            Return arrFiltro.Count - 1
        End If
    End Function

#End Region

#Region "Funciones GPI++ Eugenio Roco"

    Public Sub LlenaComboPlantillaReporte(ByRef cmb As ComboBox, ByRef DS As DataSet)
        Try
            If DS.Tables(0).Rows.Count > 0 Then
                cmb.Items.Clear()
                For Each DrColumna In DS.Tables(0).Rows
                    cmb.Items.Add(DrColumna.Item(4))
                Next
                cmb.SelectedIndex = 0
            End If
        Catch ex As Exception

        End Try
    End Sub

    Public Sub LlenaComboPlantillaReporte(ByRef cmb As ToolStripComboBox, ByRef DS As DataSet)
        Try
            If DS.Tables(0).Rows.Count > 0 Then
                cmb.Items.Clear()
                For Each DrColumna In DS.Tables(0).Rows
                    cmb.Items.Add(DrColumna.Item(4))
                Next
                cmb.SelectedIndex = 0
            End If
        Catch ex As Exception

        End Try
    End Sub

    Public Sub gSubCargaComboAlineacion( _
                ByRef objetoControl As C1.Win.C1List.C1Combo)
        Try
            objetoControl.ComboStyle = ComboStyleEnum.DropdownList
            objetoControl.DataMode = DataModeEnum.AddItem
            objetoControl.ClearItems()
            objetoControl.AddItem("General;0")
            objetoControl.AddItem("Izquierda;1")
            objetoControl.AddItem("Centrado;2")
            objetoControl.AddItem("Derecha;3")
            objetoControl.AddItem("Justificado;4")
            objetoControl.Splits(0).DisplayColumns(0).Visible = True
            objetoControl.Splits(0).DisplayColumns(1).Visible = False
            objetoControl.SelectedIndex = 2
        Catch ex As Exception
        End Try
    End Sub

    Public Sub gSubCargaComboTipoDato( _
            ByRef objetoControl As C1.Win.C1List.C1Combo)
        Try
            objetoControl.ComboStyle = ComboStyleEnum.DropdownList
            objetoControl.DataMode = DataModeEnum.AddItem
            objetoControl.ClearItems()
            objetoControl.AddItem("Numerico;0")
            objetoControl.AddItem("Texto;1")
            objetoControl.AddItem("Fecha;2")
            objetoControl.AddItem("Monto;3")
            objetoControl.Splits(0).DisplayColumns(0).Visible = True
            objetoControl.Splits(0).DisplayColumns(1).Visible = False
            objetoControl.SelectedIndex = 2
        Catch ex As Exception
        End Try
    End Sub

#End Region



#Region "Funciones Comunes Juan Pablo Antillanca M."
    ''' <summary>
    ''' Configura el formato de entrada y visualizaci�n de TextBox de ComponentOne
    ''' Ej:
    '''   Para N�meros enteros;
    '''     SetC1TextBox(Txt_LimiteInicial, C1.Win.C1Input.FormatTypeEnum.Integer, "#,##0", 6, 0, 999999)
    '''   Para porcentajes %
    '''     SetC1TextBox(Txt_LimiteFinal, C1.Win.C1Input.FormatTypeEnum.CustomFormat, "##0.00", 6, 0, 100)
    ''' </summary>
    ''' <param name="textBox">TextBox de ComponentOne</param>
    ''' <param name="type">Tipo del valor, Entero, Decimal, etc.</param>
    ''' <param name="format">Formato de visualizacion, ej: #,###, #.##, etc</param>
    ''' <param name="maxLength">M�xima cantidad de caracteres</param>
    ''' <param name="minValue">Valor M�nimo a validar</param>
    ''' <param name="maxValue">Valor M�ximo a validar</param>
    ''' <remarks></remarks>
    Public Sub SetC1TextBox(ByRef textBox As C1.Win.C1Input.C1TextBox, _
                                ByRef type As C1.Win.C1Input.FormatTypeEnum, _
                                ByVal format As String, _
                                ByVal maxLength As Integer, _
                                ByVal minValue As Double, _
                                ByVal maxValue As Double, _
                                Optional ByVal flg As Boolean = False)
        If flg = True Then Exit Sub
        textBox.DataType = GetType(System.Decimal)
        textBox.EditFormat.FormatType = type
        textBox.EditFormat.CustomFormat = format
        textBox.CustomFormat = format
        textBox.DisplayFormat.CustomFormat = format
        textBox.MaxLength = maxLength
        textBox.PostValidation.Intervals.Add(New C1.Win.C1Input.ValueInterval(minValue, maxValue, True, True))
        textBox.PostValidation.ErrorMessage = String.Format("El valor debe estar entre {0} y {1}.", minValue, maxValue)
    End Sub

    ''' <summary>
    ''' Genera el Footer para la TrueDBGrid y columna dada.
    ''' </summary>
    ''' <param name="tdgGrilla">Grilla en la que se genera el Footer</param>
    ''' <param name="nombreColumna">Nombre de la Columna a agregar Footer</param>
    ''' <remarks></remarks>
    Public Sub GeneraFooter(ByVal tdgGrilla As C1.Win.C1TrueDBGrid.C1TrueDBGrid, ByVal nombreColumna As String)
        tdgGrilla.ColumnFooters = True
        Dim total As Object = DBNull.Value
        If Not tdgGrilla.DataSource Is Nothing AndAlso tdgGrilla.RowCount > 0 Then
            total = tdgGrilla.DataSource.compute("SUM(" & nombreColumna & ")", "")
        Else
            total = 0
        End If
        tdgGrilla.Columns(nombreColumna).FooterText = IIf(Not total Is DBNull.Value, Format(total, "#,##0.00"), 0)
        With tdgGrilla.Splits(0).DisplayColumns(nombreColumna)
            .AutoSize()
            If .Width <= 120 Then
                .Width = 120
            End If
            .Style.Padding.Right = 2
            .Style.Padding.Left = 2
            .FooterStyle.HorizontalAlignment = C1.Win.C1TrueDBGrid.AlignHorzEnum.Far
        End With
    End Sub




    Public Function TerminaProcesosActivos(ByVal procesoBuscado As Thread) As Boolean
        Dim nombreProcesos As New List(Of String)()
        If Not procesoBuscado Is Nothing AndAlso gListaProcesos.Exists(Function(proceso As Thread) proceso.ManagedThreadId = procesoBuscado.ManagedThreadId) Then
            If procesoBuscado.IsAlive Then
                nombreProcesos.Add(procesoBuscado.Name)
            Else
                gListaProcesos.Remove(procesoBuscado)
            End If
        Else
            For Each proceso As Thread In gListaProcesos.ToList()
                If proceso.IsAlive Then
                    nombreProcesos.Add(proceso.Name)
                End If
                If Not proceso.IsAlive Then
                    Dim procesoMuerto As Thread = proceso
                    gListaProcesos.Remove(procesoMuerto)
                End If
            Next
        End If

        Dim texto As String = "El(Los) siguiente(s) proceso(s) est�(n) en ejecuci�n:" & vbNewLine & vbNewLine
        If nombreProcesos.Count = 0 Then
            Return True
        Else
            For Each nombre As String In nombreProcesos
                texto = texto & nombre & vbNewLine
            Next
        End If
        texto = texto & vbNewLine
        texto = texto & "�Desea terminar los procesos en ejecuci�n?"

        If MessageBox.Show(texto, _
                           "Gpi+ L�mites", _
                           MessageBoxButtons.YesNo, _
                           MessageBoxIcon.Warning, _
                           MessageBoxDefaultButton.Button2) = DialogResult.No Then
            Return False
        Else
            If Not procesoBuscado Is Nothing AndAlso gListaProcesos.Exists(Function(proceso As Thread) proceso.ManagedThreadId = procesoBuscado.ManagedThreadId) Then
                If procesoBuscado.IsAlive Then
                    procesoBuscado.Abort()
                    procesoBuscado.Join()
                    gListaProcesos.Remove(procesoBuscado)
                End If
            Else
                For Each proceso As Thread In gListaProcesos.ToList()
                    If proceso.IsAlive Then
                        proceso.Abort()
                        proceso.Join()
                    End If
                Next
                gListaProcesos.Clear()
            End If
            Return True
        End If

    End Function

    ''' <summary>
    ''' Recupera solo el valor de un par�metro dado el c�digo, en caso de que haya 
    ''' un error o no venga el valor del par�metro se lanzara una excepci�n.
    ''' </summary>
    ''' <param name="codigo">c�digo del par�metro.</param>
    ''' <returns></returns>
    Public Function ObtenerValorParametro(codigo As String) As String
        Dim parametros As New ClsParametrosSistema
        Dim resultado = String.Empty
        Dim valorParametro = String.Empty
        Dim ds = parametros.RetornarValorParametro(codigo, resultado)
        If resultado <> "OK" Then
            Throw New Exception(resultado)
        End If
        If ds Is Nothing Or ds.Tables(0).Rows.Count = 0 Then
            Throw New Exception(String.Format("El Par�metro {0} NO fue encontrado en el sistema, comun�quese con el administrador.", codigo))
        Else
            valorParametro = ds.Tables(0).Rows(0).Item("VALOR")
            If String.IsNullOrEmpty(valorParametro.Trim) Then
                Throw New Exception(String.Format("El Par�metro {0} NO tiene valor asignado, comun�quese con el administrador.", codigo))
            End If
        End If
        Return valorParametro
    End Function

    ''' <summary>
    ''' Guarda una imagen desde los recursos de la aplicaci�n.
    ''' </summary>
    ''' <param name="imageName"></param>
    ''' <returns></returns>
    Public Function SaveImage(imageName As String) As String
        Dim pathImage = String.Format("{0}\{1}", Application.StartupPath, imageName)
        If Not File.Exists(pathImage) Then
            Dim nameResource = imageName.
                Replace(".png", "").
                Replace(".bmp", "").
                Replace(".jpg", "")
            Dim image = My.Resources.ResourceManager.GetObject(nameResource)
            If Not image Is Nothing Then
                image.Save(pathImage, Imaging.ImageFormat.Png)
            End If
        End If
        Return pathImage
    End Function

    ''' <summary>
    ''' Reemplaza caracteres especiales para envio de correos hacia outlook.
    ''' </summary>
    ''' <param name="text"></param>
    ''' <returns></returns>
    Public Function ReplaceSpecialCharacter(text As String) As String
        Return text.
            Replace("<", "&lt;").
            Replace(">", "&gt;").
            Replace("@", "&#64;")
    End Function
#End Region

#Region "Filtros Genericos Grillas"
    Public Sub FiltroGrilla(ByVal Grilla As C1.Win.C1TrueDBGrid.C1TrueDBGrid, ByVal sender As C1.Win.C1TrueDBGrid.C1TrueDBGrid, ByVal e As System.EventArgs)
        Dim sb As New System.Text.StringBuilder
        Dim dc As C1.Win.C1TrueDBGrid.C1DataColumn

        Try
            sender.SelectedRows.Clear()
            For Each dc In sender.Columns
                If dc.FilterText.Trim.Length > 0 Then
                    If sb.Length > 0 Then
                        sb.Append(" AND ")
                    End If
                    sb.Append(gstrFiltro_X_TipoColumna(dc))
                End If
            Next dc
            '... Realiza el filtro sobre el dataset
            sender.DataSource.DefaultView.RowFilter = sb.ToString
            sender.Refresh()

            If sender.RowCount > 0 Then
                sender.SelectedRows.Add(0)
            End If
        Catch ex As Exception
            Debug.Print(ex.Message)
        End Try
    End Sub

    Public Sub FiltroChangeGrillaGenerico(ByVal sender As C1.Win.C1TrueDBGrid.C1TrueDBGrid, ByRef dc As C1.Win.C1TrueDBGrid.C1DataColumn, ByRef sb As System.Text.StringBuilder, ByRef Filtro As String)
        Select Case dc.DataType.ToString.ToUpper
            'Case ""
            '    sb.Append((dc.DataField + " like " + "'%" + dc.FilterText + "%'"))
            'Case ""
            '    sb.Append((dc.DataField + " like " + "'" + dc.FilterText + "%'"))
            Case "SYSTEM.STRING", "SYSTEM.DATETIME", "SYSTEM.BOOLEAN"
                sb.Append(gstrFiltro_X_TipoColumna(dc))
            Case "SYSTEM.DECIMAL", "SYSTEM.INT32", "SYSTEM.DOUBLE" 'n�meros
                Filtro = dc.FilterText
                If gFunFiltroValido(Filtro) Then
                    '  sb.Append((dc.DataField + Filtro))
                    ' sb.Append((dc.DataField + " like " + "'" + Filtro + "%'"))
                    sb.Append(gstrFiltro_X_TipoColumna(dc))
                Else
                    MsgBox("Ingrese solo valores num�ricos para filtrar la columna" + dc.Caption, MsgBoxStyle.Information, gtxtNombreSistema & " ")
                    Exit Sub
                End If
            Case Else
                sb.Append((dc.DataField + " like " + "'%" + dc.FilterText + "%'"))
        End Select
    End Sub

    Public Sub NRegistros(ByVal dtArchivos As DataTable)
        Dim strItems() As String = {"Registros de Cartera", "Cantidad de Archivos"}
        Dim strExtra As String = ""
        Dim intOk As Integer = 0
        Dim intErr As Integer = 0

        '...>variable filtro grillas generico
        'Dim dtArchivos As New DataTable

        Try
            For i As Integer = 0 To dtArchivos.Rows.Count - 1
                If dtArchivos(i)(0) = "OK" Then
                    intOk += 1
                Else
                    intErr += 1
                End If
            Next
            strExtra = " de los que son v�lidos: " & intOk.ToString & " y tienen errores: " & intErr.ToString
        Catch ex As Exception
            Debug.Print("Tdg_Archivos aun no tiene datos: " & ex.Message)
        End Try

        'NRegistros(strItems, strExtra)
    End Sub

    Function ConvertToLetter(ByVal iCol As Integer) As String
        Dim iAlpha As Integer
        Dim iRemainder As Integer
        Dim letra As String = ""
        iAlpha = Int(iCol / 27)
        iRemainder = iCol - (iAlpha * 26)
        If iAlpha > 0 Then
            letra = Chr(iAlpha + 64)
        End If
        If iRemainder > 0 Then
            letra = letra & Chr(iRemainder + 64)
        End If
        Return letra
    End Function

#End Region
End Module
