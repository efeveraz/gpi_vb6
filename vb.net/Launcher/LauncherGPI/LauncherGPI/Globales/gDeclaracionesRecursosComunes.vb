Imports System.Threading

Public Module gDeclaracionesRecursosComunes

#Region "Declaracion GPI+"
    Public goNotifyIcon As NotifyIcon = Nothing

    '...Sistema
    Public gtxtNombreSistema As String = ".: GPI+ "

    Public gtxtNombreUsuario As String = "Felipe Catrian."
    Public glngIdUsuario As Long = 0

    Public glngTodasCuentas As String = ""

    Public gtxtNombreNegocio As String = "..."
    Public glngNegocioOperacion As Long = 0
    Public lstrIdNegocio As Long
    Public gStrAbrNegocio As String = ""

    Public gtxtVersionSistema As String = "1.0.0.73 30-09-2020"
    Public gtxtAmbienteSistema As String = "DESARROLLO2"
    Public gtxtPath_Excel As String = ""
    Public gtxtPath_Txt As String = ""

    Public gblnBackOffice As Boolean = False
    Public gblnEscritorioRemoto As Boolean = False
    Public gblnLimites As Boolean = False
    Public gblnConta As Boolean = False
    Public gModoAutenticacion As String = Nothing

    '...Sistema
    'Public gobjFormulario As New ClsParametrosFormulario
    Public garrColumnaAgrupacion() As String
    Public gtxtSeparadorDecimal = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator
    Public gtxtSeparadorMiles = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberGroupSeparator

    '...Colores
    Public gColorBackGroundDesabled As Color = Color.Lavender
    Public gColorBackGroundEnabled As Color = Color.White

    '...Logo Ckliente
    'Public Const gLogoCliente As String = "HSBC"  ' "FOH" , "MONEDA", "HSBC", "SECURITY"
    'Public Const gLogoCliente1 As String = "MONEDA"  ' "FOH" , "MONEDA", "HSBC"
    Public gPathApp As String
    Public gLogoCliente As String
    Public gDB As String
    Public gServer As String

    '...Parametros Generales (Luego saldran de una clase)
    Public Const gstrAgrupadoporPrecio As String = "N"

    Public gstrFechaSistema As String
    Public gdteFechaSistema As Date
    Public gstrHoraSistema As String

    'Public oApp As New clsApp
    'Public gobjParametro As ClsParametrosSistema
    Public gstrAmbienteBD As String

    Public gstrCadenaConexion As String
    Public gstrCadenaConexionMoneda As String
    Public gstrCadenaConexion_ITZ As String
    Public gstrCadenaConexion_GRL As String
    Public gstrPathArchivoIni As String
    Public gstrCadenaConexionGF As String
    Public gstrCadenaConexionCLTE As String
    Public gstrCadenaConexionConc As String

    Public Const gstrTipoComisionTransancional As String = "TRANS"
    Public Const gstrTipoComisionExito As String = "EXITO"
    Public Const gstrTipoComisionAdministracion As String = "ADMIN"

    Public Const gstrFormatoFecha103 As String = "dd/MM/yyyy"
    Public Const gstrFormatoFecha112 As String = "yyyyMMdd"

    'Variable DCV
    Public strValorTipoCustodia As String

    Public Enum Est_Formato_XLS
        enuWidth = 0
        enuAlignment
        enuLocked
        enuVisible
        enuAllowSizing
        enuFetchStyle
    End Enum

    Public Enum Est_TipoTabla
        enuEncabezado = 0
        enuDetalle
        enuInterno
    End Enum

    Public Enum Est_TipoDato
        enuNumerico = 0
        enuString
        enuDate
    End Enum

    Public Enum Est_TipoDatoParam
        enuChar = 0
        enuVarChar
        enuInt
        enuFloat
        enuDate
    End Enum

    Public Enum enuEntidadGPI
        ASESOR
        CAJA_CUENTA
        CARTERA
        CLIENTE
        CLIENTE_SEGMENTO
        CUENTA
        EMISOR
        ESTADO_CIVIL
        ETIQUETAS
        INSTRUMENTO
        MERCADO_TRANSACCION
        MONEDA
        OPERACION
        OPERACION_DETALLE
        PAIS
        PERFIL_RIESGO
        PUBLICADOR
        PUBLICADOR_PRECIO
        SECTOR_ECONOMICO
        SIS_USUARIO
        SUB_CLASE_INSTRUMENTO
        SUCURSAL
        EJECUTIVO_SUCURSAL
        TIPO_ADMINISTRACION
        TIPO_CAMBIO
        TIPO_CUENTA
        TIPO_ENTIDAD
        TIPO_EMISOR
        TIPO_OPERACION
        TIPO_REFERENCIADO
        TIPO_DIRECCION
        EMISORES_ESPECIFICO
        EMISORES_GENERALES
        PRODUCTOS
        REL_CONTRAPARTE_GPI
        CLASE_INSTRUMENTO
        CONTRATO_MATRIMONIAL
        TIPO_CAMBIO_VALOR
        MOVIMIENTO_CAJA
        DIRECCION_CLIENTE
        DIRECCION_PERSONA
        LIM_DATO_ATRIBUTO
        REGION
        COMUNA
        CUSTODIO
        REL_NEGOCIO_SCINSTRUMENTO_PUBLICADOR
        GRUPO_ECONOMICO
        SALDO_CAJA
        CS_TB_CUPONES
        DIVIDENDO
    End Enum

    Public gblnErrorRep As Boolean

    Public gblnCancel As Boolean

    'Lista de procesos background del sistema
    Public gListaProcesos As New List(Of Thread)()

#End Region

#Region "Constantes para Entidad Contraparte"
    Public Const gEntidad_Contraparte_LARRA = "LARRA_FFMM"
#End Region

#Region "Constantes para ORACLE FOR OLE"
    'Parameter Types (ServerType)
    Public Const gORATYPE_VARCHAR2 = 1
    Public Const gORATYPE_NUMBER = 2
    Public Const gORATYPE_SINT = 3
    Public Const gORATYPE_FLOAT = 4
    Public Const gORATYPE_STRING = 5
    Public Const gORATYPE_VARCHAR = 9
    Public Const gORATYPE_DATE = 12
    Public Const gORATYPE_UINT = 68
    Public Const gORATYPE_CHAR = 96
    Public Const gORATYPE_CHARZ = 97
    Public Const gORATYPE_CURSOR = 102

    'Parameter Types
    Public Const gORAPARM_INPUT = 1
    Public Const gORAPARM_OUTPUT = 2
    Public Const gORAPARM_BOTH = 3
#End Region

#Region "Eduardo Berenguel - Variables"
    Public objParametros_Formulario As New ClsParametrosFormulario
    Public arr_ColVisible_Ordenes() As String
    Public arr_ColVisible_OrdenesConsolidadas() As String
    Public arr_ColVisible_Operaciones() As String
#End Region

#Region "Variables GPI++"
    Public gstrLista As String = ""             'Lista con Separador '�'

    Public gStrListaC As String = ""            'Lista para cuentas
    Public gStrListaR As String = ""            'Lista para Rut
    Public gStrListaG As String = ""            'Lista para Grupos

    Public gintCantidadLista As Integer = 0     'Cantidad Elementos Lista
    Public gstrTipoLista As String = ""         'C - Cuentas, R - Clientes, G - Grupo

    Public gBolReporte As Boolean = False       ' Indicador Cancelacion genereacion de reportes

    Public ESC_COM_SEL As Boolean = False       ' Indica si se carga el escritorio comercial

    Public gstrContabFechaCierre As String = "" 'Fecha de Cierre contable
    Public glngContabIdEmpr As Long             'Id Empresa Contable
    Public gdblContabCuotasCirc As Double = 0   'Cuotas Circulantes de empresa contable 
    Public gstrContabNombreEmpr As String = ""  'Nombre empresa
    Public gintSubPantalla As Integer = 0       ' Numero SUB PANtalla

#End Region

End Module
