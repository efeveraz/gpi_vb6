Imports System.Text
Imports System.DirectoryServices

Public Class ClsSeguridad

    Const NumClavesRecordar = "N_CLAVE_REC"

    Const NumClavesIntento = "N_INTENTO_CLAVE"

    Const NumLargoMinimoClave = "N_CLAVE_LARGO"

    Const NumDiasVencimientoClave = "N_CLAVE_VEN_DIA"

    Const Caracteres = "abcdefghijklmnopqrstuvwxyz"

    Const CaracteresEspeciales = "!#$%&+-.<=>?@_|~"

    Const LetraMayuscula = 1

    Const LetraMinuscula = 2

    Const CaracterEspecial = 3

    Const Numero = 4

    ReadOnly path As String

    ReadOnly filterAttribute As String

    Public Sub New(ByVal path As String)
        Me.path = path
    End Sub

    Public Function IsAuthenticated(ByVal domain As String, ByVal username As String, ByVal pwd As String) As Boolean

        Dim domainAndUsername As String = domain & "\" & username
        Dim objDirectoryEntry As DirectoryEntry = New DirectoryEntry(path, domainAndUsername, pwd)
        Dim objUser As DirectoryEntry

        Dim objDirectorySearcher As New DirectorySearcher(objDirectoryEntry)
        Dim objSearchResult As SearchResult

        Try
            objDirectorySearcher.Filter = "(SAMAccountName=" & username & ")"
            objSearchResult = objDirectorySearcher.FindOne()
            objUser = objSearchResult.GetDirectoryEntry()

            ' "Usuario aceptado."

            Return True
        Catch ex As System.Exception
            Throw New Exception("Error. " & ex.Message)
            Return False
        End Try

    End Function

    Public Function GetGroups() As String
        Dim search As DirectorySearcher = New DirectorySearcher(path)
        search.Filter = "(cn=" & filterAttribute & ")"
        search.PropertiesToLoad.Add("memberOf")
        Dim groupNames As StringBuilder = New StringBuilder()

        Try
            Dim result As SearchResult = search.FindOne()
            Dim propertyCount As Integer = result.Properties("memberOf").Count

            Dim dn As String
            Dim equalsIndex, commaIndex

            Dim propertyCounter As Integer

            For propertyCounter = 0 To propertyCount - 1
                dn = CType(result.Properties("memberOf")(propertyCounter), String)

                equalsIndex = dn.IndexOf("=", 1)
                commaIndex = dn.IndexOf(",", 1)
                If (equalsIndex = -1) Then
                    Return Nothing
                End If

                groupNames.Append(dn.Substring((equalsIndex + 1), (commaIndex - equalsIndex) - 1))
                groupNames.Append("|")
            Next

        Catch ex As Exception
            Throw New Exception("Error obtaining group names. " & ex.Message)
        End Try

        Return groupNames.ToString()
    End Function

    ''' <summary>
    ''' N�mero de claves a recordar
    ''' </summary>
    ''' <returns></returns>
    Public Shared Function GetNumeroClavesRecordar() As Integer
        Dim valorParametro = ObtenerValorParametro(NumClavesRecordar)
        Return Convert.ToInt16(valorParametro)
    End Function

    ''' <summary>
    ''' N�mero de intentos fallidos
    ''' </summary>
    ''' <returns></returns>
    Public Shared Function GetNumeroIntentosFallidos() As Integer
        Dim valorParametro = ObtenerValorParametro(NumClavesIntento)
        Return Convert.ToInt16(valorParametro)
    End Function

    ''' <summary>
    ''' Largo m�nimo de la clave
    ''' </summary>
    ''' <returns></returns>
    Public Shared Function GetLargoMinimoClave() As Integer
        Dim valorParametro = ObtenerValorParametro(NumLargoMinimoClave)
        Return Convert.ToInt16(valorParametro)
    End Function

    ''' <summary>
    ''' D�as vencimiento de la clave
    ''' </summary>
    ''' <returns></returns>
    Public Shared Function GetDiasVencimientoClave() As Integer
        Dim valorParametro = ObtenerValorParametro(NumDiasVencimientoClave)
        Return Convert.ToInt16(valorParametro)
    End Function

    ''' <summary>
    ''' Indica si la password contiene al menos una letra mayuscula.
    ''' </summary>
    ''' <param name="password"></param>
    ''' <returns></returns>
    Private Shared Function ContieneLetraMayuscula(password As String) As Boolean
        Return password.Where(Function(c) Char.IsUpper(c)).Count() > 0
    End Function

    ''' <summary>
    ''' Indica si la password contiene al menos una letra minuscula.
    ''' </summary>
    ''' <param name="password"></param>
    ''' <returns></returns>
    Private Shared Function ContieneLetraMinuscula(password As String) As Boolean
        Return password.Where(Function(c) Char.IsLower(c)).Count() > 0
    End Function

    ''' <summary>
    ''' Indica si la password contiene al menos un n�mero.
    ''' </summary>
    ''' <param name="password"></param>
    ''' <returns></returns>
    Private Shared Function ContieneNumeros(password As String) As Boolean
        Return password.Where(Function(c) Char.IsDigit(c)).Count() > 0
    End Function

    ''' <summary>
    ''' Indica si la password contiene al menos un caracter especial.
    ''' </summary>
    ''' <param name="password"></param>
    ''' <returns></returns>
    Private Shared Function ContieneCaracteresEspeciales(password As String) As Boolean
        Return password.Where(Function(c) CaracteresEspeciales.Contains(c)).Count() > 0
    End Function

    ''' <summary>
    ''' Indica si la password se encuentra dentro de las ultimas claves repetidas.
    ''' </summary>
    ''' <param name="idUsuario"></param>
    ''' <param name="password"></param>
    ''' <param name="numeroClavesRecordar"></param>
    ''' <returns></returns>
    Private Shared Function EsClaveRepetida(idUsuario As Long, password As String, numeroClavesRecordar As Integer) As Boolean
        Dim seguridadLogService As New ClsSeguridadLog
        Dim dsUltimasClaves = seguridadLogService.RegistroClavesConsultar(idUsuario, numeroClavesRecordar)
        If Not IsNothing(dsUltimasClaves) AndAlso dsUltimasClaves.Rows.Count > 0 Then
            Return dsUltimasClaves.
                AsEnumerable().
                Where(Function(row)
                          Return ClsEncriptacion.DesEncriptar(row.Item("CLAVE")) = password
                      End Function).Count() > 0
        End If
        Return False
    End Function

    ''' <summary>
    ''' Valida que la password cumpla con las reglas exigidas.
    ''' </summary>
    ''' <param name="idUsuario"></param>
    ''' <param name="password"></param>
    ''' <returns></returns>
    Public Shared Function ValidarPassword(idUsuario As Long, password As String) As String
        Dim reglasNoCumplidas As New StringBuilder()
        Dim largoMinimo = GetLargoMinimoClave()
        Dim numeroClavesRecordar = GetNumeroClavesRecordar()

        If password.Length < largoMinimo Then
            reglasNoCumplidas.Append(String.Format("- Debe tener al menos 8 car�cteres.{0}", Environment.NewLine))
        End If

        If (Not ContieneNumeros(password)) Then
            reglasNoCumplidas.Append(String.Format("- Debe contener al menos un d�gito.{0}", Environment.NewLine))
        End If

        If (Not ContieneLetraMinuscula(password)) Then
            reglasNoCumplidas.Append(String.Format("- Debe contener al menos un car�cter en min�scula.{0}", Environment.NewLine))
        End If

        If (Not ContieneLetraMayuscula(password)) Then
            reglasNoCumplidas.Append(String.Format("- Debe contener al menos un car�cter en may�scula.{0}", Environment.NewLine))
        End If

        If (Not ContieneCaracteresEspeciales(password)) Then
            reglasNoCumplidas.Append(String.Format("- Debe contener al menos un car�cter especial (( ! # $ % & + - . < = > ? @ _ | ~ )).{0}", Environment.NewLine))
        End If

        If (numeroClavesRecordar >= 1 AndAlso EsClaveRepetida(idUsuario, password, numeroClavesRecordar)) Then
            If numeroClavesRecordar = 1 Then
                reglasNoCumplidas.Append(String.Format("- NO debe ser igual la �ltima contrase�a.{0}", Environment.NewLine))
            Else
                reglasNoCumplidas.Append(String.Format("- NO debe ser igual a una de las {0} �ltimas contrase�as.{1}", numeroClavesRecordar, Environment.NewLine))
            End If
        End If

        Return reglasNoCumplidas.ToString()
    End Function

    ''' <summary>
    ''' Genera una password al azar con las reglas exigidas, esta para usuarios nuevos 
    ''' o cuando se resetea su password.
    ''' </summary>
    ''' <returns></returns>
    Public Shared Function GenerarPasswordRandom() As String
        Dim largoMinimo = GetLargoMinimoClave()
        Dim password(largoMinimo) As String
        Dim rnd = New Random
        Dim indiceValidacion = largoMinimo - 4, tipoCaracter = 0, indiceCaracter
        Dim letra, passwordValidacion As String

        For i As Integer = 0 To password.Length - 1

            If i >= indiceValidacion Then
                passwordValidacion = String.Join("", password)
                If (Not ContieneLetraMayuscula(passwordValidacion)) Then
                    tipoCaracter = LetraMayuscula
                ElseIf (Not ContieneLetraMinuscula(passwordValidacion)) Then
                    tipoCaracter = LetraMinuscula
                ElseIf (Not ContieneNumeros(passwordValidacion)) Then
                    tipoCaracter = Numero
                ElseIf (Not ContieneCaracteresEspeciales(passwordValidacion)) Then
                    tipoCaracter = CaracterEspecial
                End If
            Else
                tipoCaracter = rnd.Next(1, 5)
            End If

            indiceCaracter = rnd.Next(0, Caracteres.Length)
            letra = Caracteres.ElementAt(indiceCaracter).ToString()

            If tipoCaracter = LetraMayuscula Then
                password(i) = letra.ToUpper()
            ElseIf tipoCaracter = LetraMinuscula Then
                password(i) = letra.ToLower()
            ElseIf tipoCaracter = CaracterEspecial Then
                indiceCaracter = rnd.Next(0, CaracteresEspeciales.Length)
                password(i) = CaracteresEspeciales.ElementAt(indiceCaracter).ToString()
            ElseIf tipoCaracter = Numero Then
                password(i) = rnd.Next(0, 10).ToString()
            End If
        Next
        Return String.Join("", password)
    End Function

    ''' <summary>
    ''' Registra la �ltima clave asignada con el fin de recordarla.
    ''' </summary>
    ''' <param name="idUsuario"></param>
    ''' <param name="password"></param>
    ''' <returns></returns>
    Public Shared Function RegistraClaveNueva(idUsuario As Long, password As String) As String
        Dim tipoRegistroService As New ClsTipoRegistro
        Dim lobjConceptoRegistro As New ClsConceptoRegistro
        Dim seguridadLogService As New ClsSeguridadLog
        Dim resultado = String.Empty
        Dim idTipoRegistro = tipoRegistroService.GetIdTipoRegistro("SEGURIDAD")
        Dim idConceptoRegistro = lobjConceptoRegistro.GetIdConceptoRegistro(idTipoRegistro, "CAMBIO_CLAVE")
        resultado = seguridadLogService.Registra_Log_Basico(idTipoRegistro,
                                                            idConceptoRegistro,
                                                            idUsuario,
                                                            ClsEncriptacion.Encriptar(password))
        Return resultado
    End Function

    ''' <summary>
    ''' Genera el cuerpo del email con la password al azar al usuario.
    ''' </summary>
    ''' <param name="nombre"></param>
    ''' <param name="usuario"></param>
    ''' <param name="correo"></param>
    ''' <param name="password"></param>
    ''' <returns></returns>
    Public Shared Function EnviarMailPassword(nombre As String, usuario As String, correo As String, password As String)
        Dim bodyHtml As String
        Dim pathImage As New Dictionary(Of String, String) From {
            {"creasys_logo", SaveImage("creasys_logo.png")},
            {"paswword_titulo", SaveImage("paswword_titulo.png")},
            {"cliente_logo", SaveImage("cliente_logo.png")}
        }
        bodyHtml = My.Resources.email_template
        bodyHtml = bodyHtml.Replace("[NOMBRE]", nombre.Trim)
        bodyHtml = bodyHtml.Replace("[USUARIO]", usuario.Trim)
        bodyHtml = bodyHtml.Replace("[PASSWORD]", ReplaceSpecialCharacter(password))
        Dim resultado = EnviarCorreo(
            correo,
            String.Empty,
            String.Empty,
            "Cambio de contrase�a",
            bodyHtml,
            pathImage,
            True)
        Return resultado
    End Function
End Class
