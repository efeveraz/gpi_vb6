'Imports GPIMAS.My.Resources

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Frm_Login


    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Frm_Login))
        Me.Lbl_NombreUsuario = New C1.Win.C1SuperTooltip.C1SuperLabel()
        Me.Lbl_Login = New C1.Win.C1SuperTooltip.C1SuperLabel()
        Me.Pnl_Login = New System.Windows.Forms.Panel()
        Me.PnlUsuarioLogeado = New System.Windows.Forms.Panel()
        Me.C1SuperLabel2 = New C1.Win.C1SuperTooltip.C1SuperLabel()
        Me.C1SuperLabel3 = New C1.Win.C1SuperTooltip.C1SuperLabel()
        Me.Cmb_Negocio = New C1.Win.C1List.C1Combo()
        Me.C1SuperLabel1 = New C1.Win.C1SuperTooltip.C1SuperLabel()
        Me.Txt_Contraseña = New C1.Win.C1Input.C1TextBox()
        Me.Lbl_ClaveUser = New C1.Win.C1SuperTooltip.C1SuperLabel()
        Me.Txt_NombreUsuario = New C1.Win.C1Input.C1TextBox()
        Me.Lbl_LoginUser = New C1.Win.C1SuperTooltip.C1SuperLabel()
        Me.Pnl_Modulos = New System.Windows.Forms.Panel()
        Me.Btn_Aceptar = New C1.Win.C1Input.C1Button()
        Me.Btn_CambiarUsuario = New C1.Win.C1Input.C1Button()
        Me.Btn_CambiarPasswd = New C1.Win.C1Input.C1Button()
        Me.Pnl_CambioPwd = New System.Windows.Forms.Panel()
        Me.C1SuperLabel4 = New C1.Win.C1SuperTooltip.C1SuperLabel()
        Me.txtPwdAnterior = New C1.Win.C1Input.C1TextBox()
        Me.label5 = New C1.Win.C1SuperTooltip.C1SuperLabel()
        Me.txtPwdNueva = New C1.Win.C1Input.C1TextBox()
        Me.label4 = New C1.Win.C1SuperTooltip.C1SuperLabel()
        Me.txtPwdRetipeada = New C1.Win.C1Input.C1TextBox()
        Me.Btn_CambiarClave = New C1.Win.C1Input.C1Button()
        Me.Btn_Volver = New C1.Win.C1Input.C1Button()
        Me.Lbl_CambioClave = New C1.Win.C1SuperTooltip.C1SuperLabel()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.tslbl_InfoServer = New System.Windows.Forms.ToolStripLabel()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.tslbl_InfoDB = New System.Windows.Forms.ToolStripLabel()
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator()
        Me.tslbl_InfoVersion = New System.Windows.Forms.ToolStripLabel()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.tslbl_InfoFecha = New System.Windows.Forms.ToolStripLabel()
        Me.Pnl_Login.SuspendLayout()
        Me.PnlUsuarioLogeado.SuspendLayout()
        CType(Me.Cmb_Negocio, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Txt_Contraseña, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Txt_NombreUsuario, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Pnl_CambioPwd.SuspendLayout()
        CType(Me.txtPwdAnterior, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPwdNueva, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPwdRetipeada, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ToolStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Lbl_NombreUsuario
        '
        Me.Lbl_NombreUsuario.BackColor = System.Drawing.Color.Transparent
        Me.Lbl_NombreUsuario.Font = New System.Drawing.Font("Verdana", 8.5!, System.Drawing.FontStyle.Bold)
        Me.Lbl_NombreUsuario.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.Lbl_NombreUsuario.Location = New System.Drawing.Point(21, 19)
        Me.Lbl_NombreUsuario.Name = "Lbl_NombreUsuario"
        Me.Lbl_NombreUsuario.Size = New System.Drawing.Size(193, 18)
        Me.Lbl_NombreUsuario.TabIndex = 14
        Me.Lbl_NombreUsuario.UseMnemonic = True
        '
        'Lbl_Login
        '
        Me.Lbl_Login.BackColor = System.Drawing.Color.Transparent
        Me.Lbl_Login.Font = New System.Drawing.Font("Verdana", 8.5!, System.Drawing.FontStyle.Bold)
        Me.Lbl_Login.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.Lbl_Login.Location = New System.Drawing.Point(21, 53)
        Me.Lbl_Login.Name = "Lbl_Login"
        Me.Lbl_Login.Size = New System.Drawing.Size(195, 18)
        Me.Lbl_Login.TabIndex = 11
        Me.Lbl_Login.UseMnemonic = True
        '
        'Pnl_Login
        '
        Me.Pnl_Login.BackColor = System.Drawing.Color.Transparent
        Me.Pnl_Login.BackgroundImage = CType(resources.GetObject("Pnl_Login.BackgroundImage"), System.Drawing.Image)
        Me.Pnl_Login.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.Pnl_Login.Controls.Add(Me.PnlUsuarioLogeado)
        Me.Pnl_Login.Controls.Add(Me.Cmb_Negocio)
        Me.Pnl_Login.Controls.Add(Me.C1SuperLabel1)
        Me.Pnl_Login.Controls.Add(Me.Txt_Contraseña)
        Me.Pnl_Login.Controls.Add(Me.Lbl_ClaveUser)
        Me.Pnl_Login.Controls.Add(Me.Txt_NombreUsuario)
        Me.Pnl_Login.Controls.Add(Me.Lbl_LoginUser)
        Me.Pnl_Login.Controls.Add(Me.Pnl_Modulos)
        Me.Pnl_Login.Controls.Add(Me.Btn_Aceptar)
        Me.Pnl_Login.Controls.Add(Me.Btn_CambiarUsuario)
        Me.Pnl_Login.Controls.Add(Me.Btn_CambiarPasswd)
        Me.Pnl_Login.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Pnl_Login.ForeColor = System.Drawing.Color.MediumBlue
        Me.Pnl_Login.Location = New System.Drawing.Point(1, 2)
        Me.Pnl_Login.Name = "Pnl_Login"
        Me.Pnl_Login.Size = New System.Drawing.Size(398, 177)
        Me.Pnl_Login.TabIndex = 0
        Me.Pnl_Login.TabStop = True
        '
        'PnlUsuarioLogeado
        '
        Me.PnlUsuarioLogeado.BackColor = System.Drawing.Color.Transparent
        Me.PnlUsuarioLogeado.Controls.Add(Me.Lbl_Login)
        Me.PnlUsuarioLogeado.Controls.Add(Me.Lbl_NombreUsuario)
        Me.PnlUsuarioLogeado.Controls.Add(Me.C1SuperLabel2)
        Me.PnlUsuarioLogeado.Controls.Add(Me.C1SuperLabel3)
        Me.PnlUsuarioLogeado.Location = New System.Drawing.Point(79, 3)
        Me.PnlUsuarioLogeado.Name = "PnlUsuarioLogeado"
        Me.PnlUsuarioLogeado.Size = New System.Drawing.Size(241, 77)
        Me.PnlUsuarioLogeado.TabIndex = 40
        Me.PnlUsuarioLogeado.Visible = False
        '
        'C1SuperLabel2
        '
        Me.C1SuperLabel2.BackColor = System.Drawing.Color.Transparent
        Me.C1SuperLabel2.Font = New System.Drawing.Font("Verdana", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.C1SuperLabel2.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.C1SuperLabel2.Location = New System.Drawing.Point(21, 5)
        Me.C1SuperLabel2.Name = "C1SuperLabel2"
        Me.C1SuperLabel2.Size = New System.Drawing.Size(48, 18)
        Me.C1SuperLabel2.TabIndex = 35
        Me.C1SuperLabel2.Text = "Usuario"
        Me.C1SuperLabel2.UseMnemonic = True
        '
        'C1SuperLabel3
        '
        Me.C1SuperLabel3.BackColor = System.Drawing.Color.Transparent
        Me.C1SuperLabel3.Font = New System.Drawing.Font("Verdana", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.C1SuperLabel3.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.C1SuperLabel3.Location = New System.Drawing.Point(21, 39)
        Me.C1SuperLabel3.Name = "C1SuperLabel3"
        Me.C1SuperLabel3.Size = New System.Drawing.Size(73, 18)
        Me.C1SuperLabel3.TabIndex = 36
        Me.C1SuperLabel3.Text = "Contraseña"
        Me.C1SuperLabel3.UseMnemonic = True
        '
        'Cmb_Negocio
        '
        Me.Cmb_Negocio.AddItemSeparator = Global.Microsoft.VisualBasic.ChrW(59)
        Me.Cmb_Negocio.AllowColMove = False
        Me.Cmb_Negocio.AllowSort = False
        Me.Cmb_Negocio.AutoDropDown = True
        Me.Cmb_Negocio.AutoSelect = True
        Me.Cmb_Negocio.AutoSize = False
        Me.Cmb_Negocio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Cmb_Negocio.Caption = ""
        Me.Cmb_Negocio.CaptionHeight = 18
        Me.Cmb_Negocio.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.Cmb_Negocio.ColumnCaptionHeight = 18
        Me.Cmb_Negocio.ColumnFooterHeight = 18
        Me.Cmb_Negocio.ColumnHeaders = False
        Me.Cmb_Negocio.ComboStyle = C1.Win.C1List.ComboStyleEnum.DropdownList
        Me.Cmb_Negocio.ContentHeight = 12
        Me.Cmb_Negocio.DeadAreaBackColor = System.Drawing.Color.White
        Me.Cmb_Negocio.EditorBackColor = System.Drawing.Color.White
        Me.Cmb_Negocio.EditorFont = New System.Drawing.Font("Arial", 7.0!)
        Me.Cmb_Negocio.EditorForeColor = System.Drawing.Color.SteelBlue
        Me.Cmb_Negocio.EditorHeight = 12
        Me.Cmb_Negocio.ExtendRightColumn = True
        Me.Cmb_Negocio.FlatStyle = C1.Win.C1List.FlatModeEnum.Flat
        Me.Cmb_Negocio.Font = New System.Drawing.Font("Verdana", 7.0!, System.Drawing.FontStyle.Bold)
        Me.Cmb_Negocio.Images.Add(CType(resources.GetObject("Cmb_Negocio.Images"), System.Drawing.Image))
        Me.Cmb_Negocio.ItemHeight = 18
        Me.Cmb_Negocio.Location = New System.Drawing.Point(106, 93)
        Me.Cmb_Negocio.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Cmb_Negocio.MatchEntry = C1.Win.C1List.MatchEntryEnum.Extended
        Me.Cmb_Negocio.MatchEntryTimeout = CType(500, Long)
        Me.Cmb_Negocio.MaxDropDownItems = CType(5, Short)
        Me.Cmb_Negocio.MaxLength = 0
        Me.Cmb_Negocio.MinimumSize = New System.Drawing.Size(10, 16)
        Me.Cmb_Negocio.MouseCursor = System.Windows.Forms.Cursors.Default
        Me.Cmb_Negocio.Name = "Cmb_Negocio"
        Me.Cmb_Negocio.Padding = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.Cmb_Negocio.RowSubDividerColor = System.Drawing.Color.DarkGray
        Me.Cmb_Negocio.Size = New System.Drawing.Size(190, 16)
        Me.Cmb_Negocio.TabIndex = 2
        Me.Cmb_Negocio.VisualStyle = C1.Win.C1List.VisualStyle.Office2007Silver
        Me.Cmb_Negocio.PropBag = resources.GetString("Cmb_Negocio.PropBag")
        '
        'C1SuperLabel1
        '
        Me.C1SuperLabel1.AutoSize = True
        Me.C1SuperLabel1.BackColor = System.Drawing.Color.Transparent
        Me.C1SuperLabel1.Font = New System.Drawing.Font("Verdana", 7.0!)
        Me.C1SuperLabel1.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.C1SuperLabel1.Location = New System.Drawing.Point(104, 77)
        Me.C1SuperLabel1.Name = "C1SuperLabel1"
        Me.C1SuperLabel1.Size = New System.Drawing.Size(49, 18)
        Me.C1SuperLabel1.TabIndex = 47
        Me.C1SuperLabel1.Text = "Negocio"
        Me.C1SuperLabel1.UseMnemonic = True
        '
        'Txt_Contraseña
        '
        Me.Txt_Contraseña.AutoSize = False
        Me.Txt_Contraseña.BackColor = System.Drawing.Color.Transparent
        Me.Txt_Contraseña.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Txt_Contraseña.Font = New System.Drawing.Font("Verdana", 7.0!, System.Drawing.FontStyle.Bold)
        Me.Txt_Contraseña.ForeColor = System.Drawing.Color.SteelBlue
        Me.Txt_Contraseña.Location = New System.Drawing.Point(106, 60)
        Me.Txt_Contraseña.Margin = New System.Windows.Forms.Padding(0)
        Me.Txt_Contraseña.Name = "Txt_Contraseña"
        Me.Txt_Contraseña.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.Txt_Contraseña.Size = New System.Drawing.Size(190, 16)
        Me.Txt_Contraseña.TabIndex = 1
        Me.Txt_Contraseña.Tag = Nothing
        Me.Txt_Contraseña.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Black
        '
        'Lbl_ClaveUser
        '
        Me.Lbl_ClaveUser.BackColor = System.Drawing.Color.Transparent
        Me.Lbl_ClaveUser.Font = New System.Drawing.Font("Verdana", 7.0!)
        Me.Lbl_ClaveUser.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.Lbl_ClaveUser.Location = New System.Drawing.Point(103, 44)
        Me.Lbl_ClaveUser.Name = "Lbl_ClaveUser"
        Me.Lbl_ClaveUser.Size = New System.Drawing.Size(37, 18)
        Me.Lbl_ClaveUser.TabIndex = 45
        Me.Lbl_ClaveUser.Text = "Clave"
        Me.Lbl_ClaveUser.UseMnemonic = True
        '
        'Txt_NombreUsuario
        '
        Me.Txt_NombreUsuario.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Txt_NombreUsuario.AutoSize = False
        Me.Txt_NombreUsuario.BackColor = System.Drawing.Color.Transparent
        Me.Txt_NombreUsuario.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Txt_NombreUsuario.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.Txt_NombreUsuario.Font = New System.Drawing.Font("Verdana", 7.0!, System.Drawing.FontStyle.Bold)
        Me.Txt_NombreUsuario.ForeColor = System.Drawing.Color.SteelBlue
        Me.Txt_NombreUsuario.Location = New System.Drawing.Point(106, 28)
        Me.Txt_NombreUsuario.Margin = New System.Windows.Forms.Padding(0)
        Me.Txt_NombreUsuario.Name = "Txt_NombreUsuario"
        Me.Txt_NombreUsuario.Size = New System.Drawing.Size(190, 16)
        Me.Txt_NombreUsuario.TabIndex = 0
        Me.Txt_NombreUsuario.Tag = Nothing
        Me.Txt_NombreUsuario.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Silver
        '
        'Lbl_LoginUser
        '
        Me.Lbl_LoginUser.BackColor = System.Drawing.Color.Transparent
        Me.Lbl_LoginUser.Font = New System.Drawing.Font("Verdana", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lbl_LoginUser.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.Lbl_LoginUser.Location = New System.Drawing.Point(103, 12)
        Me.Lbl_LoginUser.Name = "Lbl_LoginUser"
        Me.Lbl_LoginUser.Size = New System.Drawing.Size(48, 18)
        Me.Lbl_LoginUser.TabIndex = 46
        Me.Lbl_LoginUser.Text = "Usuario"
        Me.Lbl_LoginUser.UseMnemonic = True
        '
        'Pnl_Modulos
        '
        Me.Pnl_Modulos.Location = New System.Drawing.Point(1, 150)
        Me.Pnl_Modulos.Name = "Pnl_Modulos"
        Me.Pnl_Modulos.Size = New System.Drawing.Size(397, 26)
        Me.Pnl_Modulos.TabIndex = 41
        '
        'Btn_Aceptar
        '
        Me.Btn_Aceptar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.Btn_Aceptar.Location = New System.Drawing.Point(159, 124)
        Me.Btn_Aceptar.Margin = New System.Windows.Forms.Padding(2)
        Me.Btn_Aceptar.Name = "Btn_Aceptar"
        Me.Btn_Aceptar.Size = New System.Drawing.Size(80, 19)
        Me.Btn_Aceptar.TabIndex = 3
        Me.Btn_Aceptar.Text = "&Ingresar"
        Me.Btn_Aceptar.UseVisualStyleBackColor = True
        Me.Btn_Aceptar.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Silver
        '
        'Btn_CambiarUsuario
        '
        Me.Btn_CambiarUsuario.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.Btn_CambiarUsuario.Location = New System.Drawing.Point(102, 124)
        Me.Btn_CambiarUsuario.Margin = New System.Windows.Forms.Padding(2)
        Me.Btn_CambiarUsuario.Name = "Btn_CambiarUsuario"
        Me.Btn_CambiarUsuario.Size = New System.Drawing.Size(96, 19)
        Me.Btn_CambiarUsuario.TabIndex = 4
        Me.Btn_CambiarUsuario.Text = "Cambiar &Usuario"
        Me.Btn_CambiarUsuario.UseVisualStyleBackColor = True
        Me.Btn_CambiarUsuario.Visible = False
        Me.Btn_CambiarUsuario.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Silver
        '
        'Btn_CambiarPasswd
        '
        Me.Btn_CambiarPasswd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.Btn_CambiarPasswd.Location = New System.Drawing.Point(201, 124)
        Me.Btn_CambiarPasswd.Margin = New System.Windows.Forms.Padding(2)
        Me.Btn_CambiarPasswd.Name = "Btn_CambiarPasswd"
        Me.Btn_CambiarPasswd.Size = New System.Drawing.Size(96, 19)
        Me.Btn_CambiarPasswd.TabIndex = 6
        Me.Btn_CambiarPasswd.Text = "&Cambio de Clave"
        Me.Btn_CambiarPasswd.UseVisualStyleBackColor = True
        Me.Btn_CambiarPasswd.Visible = False
        Me.Btn_CambiarPasswd.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Silver
        '
        'Pnl_CambioPwd
        '
        Me.Pnl_CambioPwd.BackColor = System.Drawing.Color.Transparent
        Me.Pnl_CambioPwd.BackgroundImage = CType(resources.GetObject("Pnl_CambioPwd.BackgroundImage"), System.Drawing.Image)
        Me.Pnl_CambioPwd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.Pnl_CambioPwd.Controls.Add(Me.C1SuperLabel4)
        Me.Pnl_CambioPwd.Controls.Add(Me.txtPwdAnterior)
        Me.Pnl_CambioPwd.Controls.Add(Me.label5)
        Me.Pnl_CambioPwd.Controls.Add(Me.txtPwdNueva)
        Me.Pnl_CambioPwd.Controls.Add(Me.label4)
        Me.Pnl_CambioPwd.Controls.Add(Me.txtPwdRetipeada)
        Me.Pnl_CambioPwd.Controls.Add(Me.Btn_CambiarClave)
        Me.Pnl_CambioPwd.Controls.Add(Me.Btn_Volver)
        Me.Pnl_CambioPwd.Controls.Add(Me.Lbl_CambioClave)
        Me.Pnl_CambioPwd.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.Pnl_CambioPwd.Location = New System.Drawing.Point(0, 23)
        Me.Pnl_CambioPwd.Name = "Pnl_CambioPwd"
        Me.Pnl_CambioPwd.Size = New System.Drawing.Size(398, 155)
        Me.Pnl_CambioPwd.TabIndex = 11
        Me.Pnl_CambioPwd.Visible = False
        '
        'C1SuperLabel4
        '
        Me.C1SuperLabel4.BackColor = System.Drawing.Color.Transparent
        Me.C1SuperLabel4.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.C1SuperLabel4.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.C1SuperLabel4.Location = New System.Drawing.Point(63, 53)
        Me.C1SuperLabel4.Name = "C1SuperLabel4"
        Me.C1SuperLabel4.Size = New System.Drawing.Size(125, 20)
        Me.C1SuperLabel4.TabIndex = 1
        Me.C1SuperLabel4.Text = "Contraseña Anterior"
        Me.C1SuperLabel4.UseMnemonic = True
        '
        'txtPwdAnterior
        '
        Me.txtPwdAnterior.AutoSize = False
        Me.txtPwdAnterior.BackColor = System.Drawing.Color.Transparent
        Me.txtPwdAnterior.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtPwdAnterior.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPwdAnterior.ForeColor = System.Drawing.Color.SteelBlue
        Me.txtPwdAnterior.Location = New System.Drawing.Point(194, 53)
        Me.txtPwdAnterior.MaxLength = 15
        Me.txtPwdAnterior.Name = "txtPwdAnterior"
        Me.txtPwdAnterior.Padding = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.txtPwdAnterior.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtPwdAnterior.Size = New System.Drawing.Size(130, 16)
        Me.txtPwdAnterior.TabIndex = 0
        Me.txtPwdAnterior.Tag = Nothing
        Me.txtPwdAnterior.VerticalAlign = C1.Win.C1Input.VerticalAlignEnum.Middle
        Me.txtPwdAnterior.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Silver
        '
        'label5
        '
        Me.label5.BackColor = System.Drawing.Color.Transparent
        Me.label5.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label5.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.label5.Location = New System.Drawing.Point(63, 78)
        Me.label5.Name = "label5"
        Me.label5.Size = New System.Drawing.Size(124, 20)
        Me.label5.TabIndex = 3
        Me.label5.Text = "Contraseña Nueva"
        Me.label5.UseMnemonic = True
        '
        'txtPwdNueva
        '
        Me.txtPwdNueva.AutoSize = False
        Me.txtPwdNueva.BackColor = System.Drawing.Color.Transparent
        Me.txtPwdNueva.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtPwdNueva.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPwdNueva.ForeColor = System.Drawing.Color.SteelBlue
        Me.txtPwdNueva.Location = New System.Drawing.Point(194, 78)
        Me.txtPwdNueva.MaxLength = 15
        Me.txtPwdNueva.Name = "txtPwdNueva"
        Me.txtPwdNueva.Padding = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.txtPwdNueva.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtPwdNueva.Size = New System.Drawing.Size(130, 16)
        Me.txtPwdNueva.TabIndex = 1
        Me.txtPwdNueva.Tag = Nothing
        Me.txtPwdNueva.VerticalAlign = C1.Win.C1Input.VerticalAlignEnum.Middle
        Me.txtPwdNueva.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Silver
        '
        'label4
        '
        Me.label4.BackColor = System.Drawing.Color.Transparent
        Me.label4.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label4.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.label4.Location = New System.Drawing.Point(63, 104)
        Me.label4.Name = "label4"
        Me.label4.Size = New System.Drawing.Size(124, 19)
        Me.label4.TabIndex = 9
        Me.label4.Text = "Reescribir Contraseña"
        Me.label4.UseMnemonic = True
        '
        'txtPwdRetipeada
        '
        Me.txtPwdRetipeada.AutoSize = False
        Me.txtPwdRetipeada.BackColor = System.Drawing.Color.Transparent
        Me.txtPwdRetipeada.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtPwdRetipeada.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPwdRetipeada.ForeColor = System.Drawing.Color.SteelBlue
        Me.txtPwdRetipeada.Location = New System.Drawing.Point(194, 105)
        Me.txtPwdRetipeada.MaxLength = 15
        Me.txtPwdRetipeada.Name = "txtPwdRetipeada"
        Me.txtPwdRetipeada.Padding = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.txtPwdRetipeada.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtPwdRetipeada.Size = New System.Drawing.Size(130, 16)
        Me.txtPwdRetipeada.TabIndex = 2
        Me.txtPwdRetipeada.Tag = Nothing
        Me.txtPwdRetipeada.VerticalAlign = C1.Win.C1Input.VerticalAlignEnum.Middle
        Me.txtPwdRetipeada.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Silver
        '
        'Btn_CambiarClave
        '
        Me.Btn_CambiarClave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.Btn_CambiarClave.Location = New System.Drawing.Point(124, 153)
        Me.Btn_CambiarClave.Margin = New System.Windows.Forms.Padding(2)
        Me.Btn_CambiarClave.Name = "Btn_CambiarClave"
        Me.Btn_CambiarClave.Size = New System.Drawing.Size(72, 19)
        Me.Btn_CambiarClave.TabIndex = 11
        Me.Btn_CambiarClave.TabStop = False
        Me.Btn_CambiarClave.Text = "&Cambiar"
        Me.Btn_CambiarClave.UseVisualStyleBackColor = True
        Me.Btn_CambiarClave.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Silver
        '
        'Btn_Volver
        '
        Me.Btn_Volver.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.Btn_Volver.Location = New System.Drawing.Point(198, 153)
        Me.Btn_Volver.Margin = New System.Windows.Forms.Padding(2)
        Me.Btn_Volver.Name = "Btn_Volver"
        Me.Btn_Volver.Size = New System.Drawing.Size(72, 19)
        Me.Btn_Volver.TabIndex = 12
        Me.Btn_Volver.TabStop = False
        Me.Btn_Volver.Text = "&Volver"
        Me.Btn_Volver.UseVisualStyleBackColor = True
        Me.Btn_Volver.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Silver
        '
        'Lbl_CambioClave
        '
        Me.Lbl_CambioClave.BackColor = System.Drawing.Color.Transparent
        Me.Lbl_CambioClave.BackgroundImageLayout = C1.Win.C1SuperTooltip.BackgroundImageLayout.Stretch
        Me.Lbl_CambioClave.Font = New System.Drawing.Font("Verdana", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lbl_CambioClave.ForeColor = System.Drawing.Color.SlateGray
        Me.Lbl_CambioClave.Location = New System.Drawing.Point(45, 16)
        Me.Lbl_CambioClave.Name = "Lbl_CambioClave"
        Me.Lbl_CambioClave.Size = New System.Drawing.Size(309, 37)
        Me.Lbl_CambioClave.TabIndex = 10
        Me.Lbl_CambioClave.Text = "Cambio de Contraseña"
        Me.Lbl_CambioClave.UseMnemonic = True
        '
        'ToolStrip1
        '
        Me.ToolStrip1.AutoSize = False
        Me.ToolStrip1.BackColor = System.Drawing.Color.Silver
        Me.ToolStrip1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.ToolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tslbl_InfoServer, Me.ToolStripSeparator1, Me.tslbl_InfoDB, Me.ToolStripSeparator4, Me.tslbl_InfoVersion, Me.ToolStripSeparator3, Me.tslbl_InfoFecha})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 179)
        Me.ToolStrip1.Margin = New System.Windows.Forms.Padding(2)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.ToolStrip1.Size = New System.Drawing.Size(399, 21)
        Me.ToolStrip1.TabIndex = 1010
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'tslbl_InfoServer
        '
        Me.tslbl_InfoServer.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.tslbl_InfoServer.Name = "tslbl_InfoServer"
        Me.tslbl_InfoServer.Size = New System.Drawing.Size(87, 18)
        Me.tslbl_InfoServer.Text = "tslbl_InfoServer"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 21)
        '
        'tslbl_InfoDB
        '
        Me.tslbl_InfoDB.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.tslbl_InfoDB.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.tslbl_InfoDB.Name = "tslbl_InfoDB"
        Me.tslbl_InfoDB.Size = New System.Drawing.Size(70, 18)
        Me.tslbl_InfoDB.Text = "tslbl_InfoDB"
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(6, 21)
        '
        'tslbl_InfoVersion
        '
        Me.tslbl_InfoVersion.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.tslbl_InfoVersion.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.tslbl_InfoVersion.Name = "tslbl_InfoVersion"
        Me.tslbl_InfoVersion.Size = New System.Drawing.Size(93, 18)
        Me.tslbl_InfoVersion.Text = "tslbl_InfoVersion"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(6, 21)
        '
        'tslbl_InfoFecha
        '
        Me.tslbl_InfoFecha.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.tslbl_InfoFecha.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.tslbl_InfoFecha.Name = "tslbl_InfoFecha"
        Me.tslbl_InfoFecha.Size = New System.Drawing.Size(86, 18)
        Me.tslbl_InfoFecha.Text = "tslbl_InfoFecha"
        '
        'Frm_Login
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.DimGray
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.ClientSize = New System.Drawing.Size(399, 200)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.Pnl_Login)
        Me.Controls.Add(Me.Pnl_CambioPwd)
        Me.DoubleBuffered = True
        Me.ForeColor = System.Drawing.Color.DarkRed
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Frm_Login"
        Me.Opacity = 0.9R
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Gpi+  Identificación"
        Me.Pnl_Login.ResumeLayout(False)
        Me.Pnl_Login.PerformLayout()
        Me.PnlUsuarioLogeado.ResumeLayout(False)
        CType(Me.Cmb_Negocio, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Txt_Contraseña, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Txt_NombreUsuario, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Pnl_CambioPwd.ResumeLayout(False)
        CType(Me.txtPwdAnterior, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPwdNueva, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPwdRetipeada, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Public Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub
    Friend WithEvents Lbl_NombreUsuario As C1.Win.C1SuperTooltip.C1SuperLabel
    Friend WithEvents Lbl_Login As C1.Win.C1SuperTooltip.C1SuperLabel
    Friend WithEvents Pnl_Login As System.Windows.Forms.Panel
    Friend WithEvents Pnl_CambioPwd As System.Windows.Forms.Panel
    Friend WithEvents Lbl_CambioClave As C1.Win.C1SuperTooltip.C1SuperLabel
    Friend WithEvents label4 As C1.Win.C1SuperTooltip.C1SuperLabel
    Friend WithEvents txtPwdRetipeada As C1.Win.C1Input.C1TextBox
    Friend WithEvents label5 As C1.Win.C1SuperTooltip.C1SuperLabel
    Friend WithEvents txtPwdNueva As C1.Win.C1Input.C1TextBox
    Friend WithEvents C1SuperLabel4 As C1.Win.C1SuperTooltip.C1SuperLabel
    Friend WithEvents txtPwdAnterior As C1.Win.C1Input.C1TextBox
    Friend WithEvents PnlUsuarioLogeado As System.Windows.Forms.Panel
    Friend WithEvents Pnl_Modulos As System.Windows.Forms.Panel
    Friend WithEvents Btn_Volver As C1.Win.C1Input.C1Button
    Friend WithEvents Btn_CambiarClave As C1.Win.C1Input.C1Button
    Friend WithEvents Btn_Aceptar As C1.Win.C1Input.C1Button
    Friend WithEvents Btn_CambiarPasswd As C1.Win.C1Input.C1Button
    Friend WithEvents Btn_CambiarUsuario As C1.Win.C1Input.C1Button
    Friend WithEvents C1SuperLabel3 As C1.Win.C1SuperTooltip.C1SuperLabel
    Friend WithEvents C1SuperLabel2 As C1.Win.C1SuperTooltip.C1SuperLabel
    Friend WithEvents Txt_NombreUsuario As C1.Win.C1Input.C1TextBox
    Friend WithEvents Txt_Contraseña As C1.Win.C1Input.C1TextBox
    Friend WithEvents Cmb_Negocio As C1.Win.C1List.C1Combo
    Friend WithEvents Lbl_LoginUser As C1.Win.C1SuperTooltip.C1SuperLabel
    Friend WithEvents Lbl_ClaveUser As C1.Win.C1SuperTooltip.C1SuperLabel
    Friend WithEvents C1SuperLabel1 As C1.Win.C1SuperTooltip.C1SuperLabel
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents tslbl_InfoVersion As System.Windows.Forms.ToolStripLabel
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tslbl_InfoFecha As System.Windows.Forms.ToolStripLabel
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tslbl_InfoServer As System.Windows.Forms.ToolStripLabel
    Friend WithEvents tslbl_InfoDB As System.Windows.Forms.ToolStripLabel
    Friend WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
End Class
